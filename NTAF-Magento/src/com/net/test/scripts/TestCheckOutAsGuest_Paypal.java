package com.net.test.scripts;


import com.net.test.data.CheckoutTestData;
import com.net.test.pageObject.CheckOutProcess;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;
import org.testng.annotations.*;

public class TestCheckOutAsGuest_Paypal extends TestBase
{
	CheckOutProcess ckProcess = new CheckOutProcess();
	TestCommonMethods common = new TestCommonMethods();
	CheckoutTestData checkoutTestData = new CheckoutTestData();

	@BeforeClass
	public void setUp() throws Exception
	{	
		common.initializeExecutedClass("Checkout Process As","Guest-Paypal");		
	}
	@Test (priority = 1)
	public void addProductIntoCart() throws Exception 
	{	
		ckProcess.addProductIntoCart();
		
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 2)
	public void verifyTitleInShoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Title In Shopping Cart Page"))
		{
			ckProcess.titleInshoppingCartPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyTitleInShoppingCartPage" } ,priority = 3)
	public void verifyProductPriceInshoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Price In Shopping Cart Page"))
		{
			ckProcess.productPriceInshoppingCartPageForGuest();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyProductPriceInshoppingCartPage" } ,priority = 4)
	public void verifyProductQTYInshoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"QTY In Shopping Cart Page"))
		{
			//ckProcess.productQTYInshoppingCartPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyProductQTYInshoppingCartPage" } ,priority = 5)
	public void verifyProductNameInshoppingCartPage() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Name In Shopping Cart Page"))
		{
			ckProcess.productNameInshoppingCartPage();
		}
		

	}
	@Test(dependsOnMethods = { "verifyProductNameInshoppingCartPage" } ,priority = 6)
	public void verifyProceedToCheckoutButton() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Proceed To Checkout Button In Shoppping Cart"))
		{
			ckProcess.proceedToCheckoutButton();
		}
	

	}
	@Test(dependsOnMethods = { "verifyProceedToCheckoutButton" },priority = 7)
	public void verifyCheckoutPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Checkout As Guest With Email"))
		{
			ckProcess.CheckoutAsGuest_withEmail();	
		}
		else if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
		{
			ckProcess.CheckoutAsGuest_withoutEmail();	
		}
		
	}
	
	
	@Test(dependsOnMethods = { "verifyCheckoutPage" },priority = 8)
	public void validateBillingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			ckProcess.validateBillingInforPage();
		}	
		if(isExecuted(getMethodIsExecuted,"Create An Account Later"))
		{
			ckProcess.createAccountLater();
		}	
		
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.billingInfoConButton();	
		}
		
		
	
	}
	@Test(dependsOnMethods = { "validateBillingInfoPage" } ,priority = 9)
	public void verifyBillingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			ckProcess.fillGuestUserDetails();
			
			ckProcess.billingInfoPage();
		}	
		if(isExecuted(getMethodIsExecuted,"Create An Account Later"))
		{
			ckProcess.createAccountLater();
		}	
		
	
	}
	@Test(dependsOnMethods = { "verifyBillingInfoPage" }, priority = 10)
	public void verifyBillingInfoConButton() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.billingInfoConButton();	
			
		}
	}
	
	/*--------------------------------------*/
	
	@Test(dependsOnMethods = { "verifyBillingInfoConButton" } ,priority = 11)
	public void validateDeliveryInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Delivery Info Page"))
		{
			ckProcess.validateDeliveryInforPage();
		}	
		
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.DeliveryInfoConButton();	
		}
		
		
	
	}
	
	@Test(dependsOnMethods = { "validateDeliveryInfoPage" } , priority = 12)
	public void verifyDeliveryInfoConButton() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Use same billing address"))
		{
			ckProcess.chkSameAddress();		
			
		}
		
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.DeliveryInfoConButton();		
			
		}
	}
	

	@Test(dependsOnMethods = { "verifyDeliveryInfoConButton" } , priority = 13)
	public void continueShoppingMethod() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Method Page"))
		{
			ckProcess.shippingMethodConButton();
		}

	}
	
	@Test(dependsOnMethods = { "continueShoppingMethod" }, priority = 14)
	public void verifySelectPaymentMethod() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is Paypal"))
		{
			ckProcess.paymentMethodIsPaypal();
		}
		
	}

	@Test(dependsOnMethods = { "verifySelectPaymentMethod" } ,priority = 15)
	public void payment_submitButton() throws Exception 
	{
		ckProcess.payment_submitButton();
		
		ckProcess.verifyErrorMsgInOrderConfirmPage();
		
	}
//2	
	@Test(dependsOnMethods = { "payment_submitButton" } ,priority = 16)
	public void verifyPaypalAccount() throws Exception 
	{
		ckProcess.verifyPaypalAccount();
	}
	
	@Test(dependsOnMethods = { "payment_submitButton" },priority = 17)
	public void verifyPaypalReviewPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Paypal Review"))
		{
			ckProcess.verifyProductPriceInPaypalReview();
			
			ckProcess.verifyProductNameInPaypalReview();
			
			ckProcess.continuePaypalReview();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
		}					
	}
	

	@Test(dependsOnMethods = { "verifyPaypalReviewPage" }, alwaysRun = true, priority = 18 )
	public void verifyTotalTime() throws Exception 
	{	
		
		ckProcess.totalTime(startTime);

	}
	@AfterClass
	public void stopSelenium() throws Exception
	{
			common.closedBrowser();
		
	}

}