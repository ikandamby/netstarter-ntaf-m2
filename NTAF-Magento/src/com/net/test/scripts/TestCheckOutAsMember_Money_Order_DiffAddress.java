package com.net.test.scripts;


import com.net.test.data.CheckoutTestData;
import com.net.test.pageObject.CheckOutProcess;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;
import org.testng.annotations.*;

public class TestCheckOutAsMember_Money_Order_DiffAddress extends TestBase
{
	CheckOutProcess ckProcess = new CheckOutProcess();
	TestCommonMethods common = new TestCommonMethods();
	CheckoutTestData checkoutTestData = new CheckoutTestData();

	@BeforeClass
	public void setUp() throws Exception
	{
		common.initializeExecutedClass("Checkout Process As","Reg-MO-Different Address");	
		
	}
	@Test public void loginToMyAccount() throws Exception 
	{	
		ckProcess.loginToMyAccount();
		
		common.clearMyCart();
		
		ckProcess.logout();
		
		common.clickLogo();
						
	}
	

	@Test(dependsOnMethods = { "loginToMyAccount" })
	public void addProductIntoCart() throws Exception 
	{	
		ckProcess.addProductIntoCart();
		
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" })
	public void verifyTitleInShoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Title In Shopping Cart Page"))
		{
			ckProcess.titleInshoppingCartPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyTitleInShoppingCartPage" })
	public void verifyProductPriceInshoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Price In Shopping Cart Page"))
		{
			ckProcess.productPriceInshoppingCartPageForMember();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyProductPriceInshoppingCartPage" })
	public void verifyProductQTYInshoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"QTY In Shopping Cart Page"))
		{
			//ckProcess.productQTYInshoppingCartPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyProductQTYInshoppingCartPage" } )
	public void verifyProductNameInshoppingCartPage() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Name In Shopping Cart Page"))
		{
			ckProcess.productNameInshoppingCartPage();
		}
		

	}
	@Test(dependsOnMethods = { "verifyProductNameInshoppingCartPage" })
	public void verifyProceedToCheckoutButton() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Proceed To Checkout Button In Shoppping Cart"))
		{
			ckProcess.proceedToCheckoutButton();
		}
	

	}
	
	@Test(dependsOnMethods = { "verifyProceedToCheckoutButton" })
	public void validateLoginDetails() throws Exception 
	{	
		ckProcess.memberCheckoutLoginWithEmail();	
	
	}
	@Test(dependsOnMethods = { "validateLoginDetails" })
	public void validateBillingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			ckProcess.memberNewBillingAddressValidation();
		}
		/*if(isExecuted(getMethodIsExecuted,"Select Different Address"))
		{
			ckProcess.chkDifferentAddress();				
			
		}*/
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.shippingToDifferentAddress();
			
			ckProcess.billingInfoConButton();	
			
		
		}
	
	}
	@Test(dependsOnMethods = { "validateBillingInfoPage" })
	public void verifyBillingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			ckProcess.selectNewAddress();
			
			ckProcess.billingInfoNewAddress();
		}	
		
	
	}
	
	@Test(dependsOnMethods = { "verifyBillingInfoPage" })
	public void selectDifferntAddress() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Select Different Address"))
		{
			ckProcess.chkDifferentAddress();				
			
		}
		if(isExecuted(getMethodIsExecuted,"Continue Billing Button For Different Address"))
		{
			ckProcess.billingInfoConButton();				
		}
	}
	
	@Test(dependsOnMethods = { "selectDifferntAddress" })
	public void validateShippingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Info Page"))
		{
			ckProcess.memberNewShippingAddressValidation();
		}
		
		if(isExecuted(getMethodIsExecuted,"Shipping Info Continue Button"))
		{
			ckProcess.shippingInfoConButton();	
		
		}
	
	}
	@Test(dependsOnMethods = { "validateShippingInfoPage" })
	public void verifyShippingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Info Page"))
		{
			ckProcess.memberNewShippingAddress();				
			
		}
		
	}
	@Test(dependsOnMethods = { "verifyShippingInfoPage" })
	public void verifyShippingInfoConButton() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Info Continue Button"))
		{
			ckProcess.shippingInfoConButton();				
			
		}
	}
	@Test(dependsOnMethods = { "verifyShippingInfoConButton" })
	public void shippingMethodValidation() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
		{
			ckProcess.shippingMethodValidation();
		
		}

	}
	
	@Test(dependsOnMethods = { "shippingMethodValidation" })
	public void selectShoppingMethod() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
		{
			ckProcess.selectShippingMethod();
		
		}

	}
	
	@Test(dependsOnMethods = { "selectShoppingMethod" })
	public void continueShoppingMethod() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Method Page"))
		{
			ckProcess.shippingMethodConButton();
		}

	}
	
	@Test(dependsOnMethods = { "continueShoppingMethod" })
	public void verifySelectPaymentMethod() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is Money Order"))
		{
			ckProcess.paymentMethodIsMoneyOrder();
		}
		
	}
	
	@Test(dependsOnMethods = { "verifySelectPaymentMethod" })
	public void selectPaymentTeamsAndCondition() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Payment - Terms Condition"))
		{
			ckProcess.selectPaymentTeamsAndCondition();
		}
		
	}
	
	@Test(dependsOnMethods = { "selectPaymentTeamsAndCondition" })	
	public void moneyOrderSubmitButton() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Money Order"))
		{
			ckProcess.moneyOrderSubmitButton();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
		}
		
	}
	
	@Test(dependsOnMethods = { "moneyOrderSubmitButton" })
	public void verifyOrderReviewPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Order Review"))
		{
			ckProcess.verifyProductPriceInOrderReview();
			
			ckProcess.verifyProductNameInOrderReview();
			
			ckProcess.continueOrderReview();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
		}
		
	}
	
	@Test(dependsOnMethods = { "verifyOrderReviewPage" })
	public void verifyConfirmationPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Confimation Page"))
		{
			ckProcess.orderIdInConfirmationPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" },priority = 1)
	public void verifyContinueShopping() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Continue Shopping"))
		{
			ckProcess.continueShopping();
		}	

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" },priority = 2)
	public void verifyConfirmationMail() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Confimation Mail Reader"))
		{
			ckProcess.ConfirmationMaiReader();
		}

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" },priority = 3)
	public void verifyOrderDisplayedInMyAccount() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Order In My Account"))
		{
			ckProcess.verifyOrderDisplayedInMyAccount();
		}		

	}
	@Test(dependsOnMethods = { "verifyOrderDisplayedInMyAccount" })
	public void verifyOrderInMyOrderSection() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Order In My Account"))
		{						
			ckProcess.verifyOrderInMyOrderSection();
		}
	}
	
	@Test(dependsOnMethods = { "verifyOrderInMyOrderSection" }, alwaysRun = true )	
	public void verifyTotalTime() throws Exception 
	{	
		ckProcess.totalTime(startTime);
	}
	
	@AfterClass
	public void stopSelenium() throws Exception
	{
			common.closedBrowser();	
	}	
}