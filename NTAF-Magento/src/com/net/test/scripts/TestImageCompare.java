package com.net.test.scripts;


import com.net.test.data.CheckoutTestData;
import com.net.test.pageObject.CheckOutProcess;
import com.net.test.pageObject.UIImageCompare;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;
import org.testng.annotations.*;

public class TestImageCompare extends TestBase
{
	UIImageCompare imCompare = new UIImageCompare();
	TestCommonMethods common = new TestCommonMethods();
	CheckoutTestData checkoutTestData = new CheckoutTestData();

	@BeforeClass
	public void setUp() throws Exception
	{
		common.initializeExecutedClass("Verify UI","");			
	}
	
	@Test public void compareHomePageScreenShot() throws Exception 
	{	
		imCompare.compareHomePageScreenShot("Home Page");						
	}
	
	@Test(dependsOnMethods = { "compareHomePageScreenShot" })
	public void compareCategoryPageScreenShot() throws Exception 
	{	
		imCompare.compareCategoryPageScreenShot("Category Page");		
	}
	
	@Test(dependsOnMethods = { "compareCategoryPageScreenShot" })
	/*public void compareProductPageScreenShot() throws Exception 
	{	
		imCompare.compareProductPageScreenShot("Product Page");		
	}
	
	@Test(dependsOnMethods = { "compareProductPageScreenShot" })
	public void compareHeaderCartScreenShot() throws Exception 
	{	
		imCompare.compareHeaderCartScreenShot("Header Cart");		
	}
	
	@Test(dependsOnMethods = { "compareHeaderCartScreenShot" })
	public void compareShoppingCartScreenShot() throws Exception 
	{	
		imCompare.compareShoppingCartScreenShot("Shopping Cart");		
	}
	
	@Test(dependsOnMethods = { "compareShoppingCartScreenShot" })
	public void compareCheckoutUserTypeScreenShot() throws Exception 
	{	
		imCompare.compareCheckoutUserTypeScreenShot("User Type");		
	}
	
	@Test(dependsOnMethods = { "compareCheckoutUserTypeScreenShot" })
	public void compareCheckouBillingScreenShot() throws Exception 
	{	
		imCompare.compareCheckouBillingScreenShot("Billing Infor");		
	}
	
	@Test(dependsOnMethods = { "compareCheckouBillingScreenShot" })
	public void compareCheckoutShipplingScreenShot() throws Exception 
	{	
		imCompare.compareCheckoutShipplingScreenShot("Shipping Infor");		
	}
	
	@Test(dependsOnMethods = { "compareCheckoutShipplingScreenShot" })
	public void compareCheckoutShippngMethodScreenShot() throws Exception 
	{	
		imCompare.compareCheckoutShippngMethodScreenShot("Shipping Method");		
	}
	
	@Test(dependsOnMethods = { "compareCheckoutShippngMethodScreenShot" })
	public void compareCheckoutpaymentScreenShot() throws Exception 
	{	
		imCompare.compareCheckoutpaymentScreenShot("Payment Screen");		
	}
	
	@Test(dependsOnMethods = { "compareCheckoutpaymentScreenShot" })
	public void compareCheckoutOrderReviewScreenShot() throws Exception 
	{	
		imCompare.compareCheckoutOrderReviewScreenShot("Order review Screen");		
	}
	
	@Test(dependsOnMethods = { "compareCheckoutpaymentScreenShot" })
	public void compareCheckoutConfirmScreenShot() throws Exception 
	{
		imCompare.compareCheckoutConfirmScreenShot("Order confirm Page");		
	}
	
	@Test(dependsOnMethods = { "compareCheckoutConfirmScreenShot" }, alwaysRun = true)*/
	public void compareUserCreationScreenShot() throws Exception 
	{		
		imCompare.compareUserCreationScreenShot("User creation Page");		
	}
	
	@Test(dependsOnMethods = { "compareUserCreationScreenShot" }, alwaysRun = true)
	public void compareLoginPageScreenShot() throws Exception 
	{	
		imCompare.compareLoginPageScreenShot("Login page");	
	}
	
	@Test(dependsOnMethods = { "compareLoginPageScreenShot" }, alwaysRun = true)
	public void compareForgotPwPageScreenShot() throws Exception 
	{	
	//	imCompare.compareForgotPwPageScreenShot("Forgot password page");	
	}
	
	@Test(dependsOnMethods = { "compareForgotPwPageScreenShot" }, alwaysRun = true)
	public void compareHomePageLoggedUserScreenShot() throws Exception 
	{	
		imCompare.compareHomePageLoggedUserScreenShot("Logged-Home Page");	
	}
	
	@Test(dependsOnMethods = { "compareHomePageLoggedUserScreenShot" }, alwaysRun = true)
	public void compareCategoryPageLoggedUserScreenShot() throws Exception 
	{	
		imCompare.compareCategoryPageLoggedUserScreenShot("Logged-Category Page");	
	}
	
	@Test(dependsOnMethods = { "compareCategoryPageLoggedUserScreenShot" }, alwaysRun = true)
/*	public void compareProductPageLoggedUserScreenShot() throws Exception 
	{	
		imCompare.compareProductPageLoggedUserScreenShot("Logged-Product Page");	
	}
	
	@Test(dependsOnMethods = { "compareProductPageLoggedUserScreenShot" }, alwaysRun = true)
	public void compareHeaderCartLoggedUserScreenShot() throws Exception 
	{	
		imCompare.compareHeaderCartLoggedUserScreenShot("Logged-Header Cart");	
	}
	
	@Test(dependsOnMethods = { "compareHeaderCartLoggedUserScreenShot" }, alwaysRun = true)
	public void compareHeaderShoppingCartUserScreenShot() throws Exception 
	{	
		imCompare.compareShoppingCartLoggedUserScreenShot("Logged-Shopping Cart");	
	}
	
	@Test(dependsOnMethods = { "compareHeaderShoppingCartUserScreenShot" }, alwaysRun = true)*/
	public void verifyTotalTime() throws Exception 
	{	
		
		imCompare.totalTime(startTime);

	}
	@AfterClass
	public void stopSelenium() throws Exception
	{
			common.closedBrowser();
		
	}


}