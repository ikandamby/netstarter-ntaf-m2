package com.net.test.scripts;

import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.FileSystemOptions;
import org.apache.commons.vfs.Selectors;
import org.apache.commons.vfs.VFS;
import org.apache.commons.vfs.impl.DefaultFileSystemConfigBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.net.test.util.BitbucketIssue;
import com.net.test.util.ReadXl;
import com.net.test.util.SendTextMessage;
import com.net.test.util.TestBase;
import com.net.test.util.WriteResults;


public class TestSendResultsToHtml extends TestBase
{
	static DecimalFormat df = new DecimalFormat("#####0");
	public ArrayList<String> saveScreenShots = new ArrayList<String>();
	public static ArrayList<String> fileSet = new ArrayList<String>();
	static String ftpHost = "192.168.10.97"; //"59.163.248.228";//"192.168.10.97";
	static int ftpPort = 22;     
    static String ftpUserName = "ntaf";
    static String ftpPassword = "M@k3PuY!1";
    static String ftpRemoteDirectory = "/var/www/dashboard/web/";
    static String fileToTransmit = "";      
    static String strScreenshotPath = "";
    
    @Test
	public void convertXlToXml() throws Exception
	{
    	mainData();
    	
    	String executedTime = fileReadTime;
    	
    	System.out.println("Executed Time - "+executedTime);
    	
        FileWriter fostream;
        PrintWriter out = null;
        String strOutputPath = readXL.htmlResultFolder;
        String strInputPath = readXL.resultFolder;
        strScreenshotPath = readXL.screenShot;
        
        String strOutFile  ="";
        try {
        	String failUrl ="";
        	String executionTime = "";
        	
        	executionTime = executedTime.substring(0,10)+' '+executedTime.substring(11);
        	executionTime = executionTime.substring(0,13)+':'+executionTime.substring(14);
			
            InputStream inputStream = new FileInputStream(new File(strInputPath+wResult.wFileName));
            Workbook wb = WorkbookFactory.create(inputStream);
            strOutFile = strOutputPath+wResult.wFileName.replaceAll("xlsx", "xml");
            fostream = new FileWriter(strOutFile);
            out = new PrintWriter(new BufferedWriter(fostream));

            out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            out.println("<?xml-stylesheet type=\"text/xsl\" href=\"../css/results.xsl\"?>");
            
            
           out.println("<test_results>");
            
            
            out.println(formatElement("\t", "executed_time", executionTime));
            int noOFSheets = wb.getNumberOfSheets();        
            
          //  summary(out,wb);
            
           
            for(int i=0;i<noOFSheets;i++)
            {
            	if(wb.getSheetName(i).equalsIgnoreCase("Summary Results"))
            	{
            		break;
            	}
            	if(i == (noOFSheets-1))
            	{
            		failedSummary(out,wb,executedTime);
            	}
            	
            }
            for(int i=0;i<noOFSheets;i++)
            {
            	Sheet sheet = wb.getSheetAt(i);

            	if(wb.getSheetName(i).equalsIgnoreCase("Summary Results"))
            	{
            		summary(out,wb,executedTime);
            		
            		// summary(out,wb);
            	}
            	
            	else if(wb.getSheetName(i).equalsIgnoreCase("Skip Results"))
            	{
            		 summarySkipTC(out,wb);
            	}
            	
            	else
            	{
            		out.println("<test_scenario id=\""+wb.getSheetName(i)+"\">");
                    
                    boolean firstRow = true;
                    for (Row row : sheet) {
                        if (firstRow == true) {
                            firstRow = false;
                            continue;
                        }
                        
                       // System.out.println("row"+row);
                        out.println("\t<test_case>");
                        
                                 
                        out.println(formatElement("\t\t", "case", charReplace(formatCell(row.getCell(0)))));
                        out.println(formatElement("\t\t", "expected",charReplace(formatCell(row.getCell(1)))));
                        out.println(formatElement("\t\t", "actual", charReplace(formatCell(row.getCell(2)))));
                        out.println(formatElement("\t\t", "status", charReplace(formatCell(row.getCell(3)))));
                        if(formatCell(row.getCell(3)).equalsIgnoreCase("FAIL"))
                        {
                        	String failTc1 = formatCell(row.getCell(0));
                        	String failTc ="";
                        	
                        	String resultsSheet = wb.getSheetName(i);
                        	failTc = resultsSheet+"-"+failTc1;
                        	
                        	System.out.println(executedTime);
                        	System.out.println(resultsSheet);
                        	System.out.println(failTc1);
                        	
                        	if(failTc.length() > 100)
                			{                        		
                        	
                        		failTc = failTc.substring(0, 100).replaceAll("([^a-zA-Z]|\\s)+","_");
                			} 
                        	else
                        	{
                        		failTc = failTc.replaceAll("([^a-zA-Z]|\\s)+","_");
                        	}
                        	
                        	failUrl = executedTime+"-"+failTc+".jpg";
                        	 
                        	System.out.println("failTc"+failTc);
                        	
                        	out.println("\t\t<failUrl>"+failUrl+"</failUrl>");
                        	
                        	
                        	saveScreenShots.add(failUrl);
                            
                        	failUrl = "";
                        	failTc1="";
                            
                        	
                        }
                        
                        out.println("\t</test_case>");
                    }
                    
                    out.println("</test_scenario>");
                    out.println("\t");       
	
            	}
            	
            }
            			
            out.println("</test_results>");
            out.flush();
            out.close();
            
            converToHtml(strOutputPath,strOutFile);
          
            fileSet.add(strOutFile.replaceAll("xml","html"));
            fileSet.add(strOutFile);
            
                      
            pushFailScreenShots(saveScreenShots);
            
            pushFileToServer(fileSet);
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static String charReplace(String text)
    {    
    	text = text.replaceAll("[^\\p{Print}\\t\\n]", "");
    	return text.replaceAll("&", "&amp;").replaceAll(" \"", "&quot;").replaceAll("'", "&apos;")
    			.replaceAll(" <", " &lt;").replaceAll(" >", "&gt;");
    }
    
    private static void failedSummary(PrintWriter out,Workbook wb,String executedTime)
    {    	
    	out.println("<test_results_summary id=\""+"Summary Results"+"\">");
    	    	
    	out.println("\t<test_summary>");
        out.println(formatElement("\t\t", "scenario", "Test Failed"));
        out.println(formatElement("\t\t", "case", "Test Failed"));
        out.println(formatElement("\t\t", "skip", "0"));
        out.println(formatElement("\t\t", "passed", "0"));
        out.println(formatElement("\t\t", "failed", "0"));
        out.println(formatElement("\t\t", "time", "0"));
        out.println(formatElement("\t\t", "rate", "0"));
        out.println("\t</test_summary>");
        out.println("</test_results_summary>");
             
        
        out.println("\t<total_summary>");
        out.println(formatElement("\t\t", "scenario", "Total"));
        out.println(formatElement("\t\t", "case", "Test Failed"));
        out.println(formatElement("\t\t", "skip", "0"));
        out.println(formatElement("\t\t", "passed", "0"));
        out.println(formatElement("\t\t", "failed", "0"));
        out.println(formatElement("\t\t", "time", "0"));
        out.println(formatElement("\t\t", "rate", "0"));
       // out.println("\t</Test_scenario>");
    
        out.println("\t</total_summary>");


    }
    private static void convertToJPG(String screenshotName)
    {
    	BufferedImage bufferedImage;
    	 
    	try {
     
    	  //read image file
    		
    		//System.out.println(failScreenshotsFolder+screenshotName);
    	  bufferedImage = ImageIO.read(new File(failScreenshotsFolder+screenshotName.replaceAll("jpg", "png")));
     
    	  // create a blank, RGB, same width and height, and a white background
    	  BufferedImage newBufferedImage = new BufferedImage(bufferedImage.getWidth(),
    			bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
    	  newBufferedImage.createGraphics().drawImage(bufferedImage, 0, 0, Color.WHITE, null);
     
    	  // write to jpeg file
    	  ImageIO.write(newBufferedImage, "jpg", new File(failScreenshotsFolder+screenshotName));
     
    	  //System.out.println("Done");
    	  
    	  File f=new File(failScreenshotsFolder+screenshotName.replaceAll("jpg", "png")); //Takes the default path, else, you can specify the required path
    	  
      	if(f.exists())
          {
              f.delete();
          }
     
    	} catch (IOException e) {
     
    	  e.printStackTrace();
     
    	}
     
     }
    
    private static void pushDataToServerSameServer(String screenshotName) throws IOException
    {  	
    	
    	String remoteFilePath = failScreenshotsFolder+screenshotName;
    	
    	
    	File f=new File("C:/wamp/www/dashboard/web/screenshots/"+screenshotName); //Takes the default path, else, you can specify the required path
    	  
    	if(f.exists())
        {
            f.delete();
        }
        f.createNewFile();
        
        FileObject destn=VFS.getManager().resolveFile(f.getAbsolutePath());

        FileSystemOptions opts=new FileSystemOptions();
       // DefaultFileSystemConfigBuilder.getInstance().setUserAuthenticator(opts, auth);
        DefaultFileSystemConfigBuilder.getInstance();


        FileObject fo=VFS.getManager().resolveFile(remoteFilePath,opts);

        //System.out.println(fo.exists());

        //fo.createFile();

        destn.copyFrom(fo,Selectors.SELECT_SELF);
        destn.close();
    }
    
    private static void pushFailScreenShots(ArrayList<String> saveScreenShots) throws IOException
    {    
    	//System.out.println("ssss");
    	//System.out.println(saveScreenShots.size());
    	for(int i=0;i<saveScreenShots.size();i++)
    	{
    		convertToJPG(saveScreenShots.get(i));
    		fileSet.add(strScreenshotPath+saveScreenShots.get(i));
    		
    		//System.out.println("fileSet"+fileSet.get(i));
    		
    	}    	
    }
    
    private static void pushFileXmlSameServer(String strOutFile) throws IOException
    {
    	String remoteFilePath=strOutFile;
    	
    	
    	File f=new File("C:/wamp/www/dashboard/web/project_results/BBQ/"+wResult.wFileName.replaceAll("xlsx", "xml")); //Takes the default path, else, you can specify the required path
    	  
    	if(f.exists())
        {
            f.delete();
        }
        f.createNewFile();
        
        FileObject destn=VFS.getManager().resolveFile(f.getAbsolutePath());

        FileSystemOptions opts=new FileSystemOptions();
       // DefaultFileSystemConfigBuilder.getInstance().setUserAuthenticator(opts, auth);
        DefaultFileSystemConfigBuilder.getInstance();


        FileObject fo=VFS.getManager().resolveFile(remoteFilePath,opts);

        //System.out.println(fo.exists());

        //fo.createFile();

        destn.copyFrom(fo,Selectors.SELECT_SELF);
        destn.close();
    }
   
    
    private static void pushFileHtmlSameServer(String strOutFile) throws IOException
    {
    	String remoteFilePath=strOutFile;
    	File f=new File("C:/wamp/www/dashboard/web/project_results/BBQ/"+wResult.wFileName.replaceAll("xlsx", "html")); //Takes the default path, else, you can specify the required path
    	  
    	if(f.exists())
        {
            f.delete();
        }
        f.createNewFile(); 
        FileObject destn=VFS.getManager().resolveFile(f.getAbsolutePath());

        FileSystemOptions opts=new FileSystemOptions();
       // DefaultFileSystemConfigBuilder.getInstance().setUserAuthenticator(opts, auth);
        DefaultFileSystemConfigBuilder.getInstance();


        FileObject fo=VFS.getManager().resolveFile(remoteFilePath.replaceAll("xml","html"),opts);

        //System.out.println(fo.exists());

        //fo.createFile();

        destn.copyFrom(fo,Selectors.SELECT_SELF);
        destn.close();
    }
    
    private static void converToHtml(String strOutputPath,String strOutFile)
    {
    	 try
    	    {
    	        TransformerFactory tFactory = TransformerFactory.newInstance();

    	        Source xslDoc = new StreamSource(strOutputPath+"/results.xsl");
    	        Source xmlDoc = new StreamSource(strOutFile);

    	        String outputFileName = strOutFile.replaceAll("xml", "html");
    	        OutputStream htmlFile = new FileOutputStream(outputFileName);

    	        Transformer transformer = tFactory.newTransformer(xslDoc);
    	        transformer.transform(xmlDoc, new StreamResult(htmlFile));
    	    }
    	    catch(Exception e)
    	    {
    	        e.printStackTrace();
    	    }
    }
   
   
    private static void summary(PrintWriter out,Workbook wb,String executedTime)
    {
    	double totalExecutedTC = 0;
    	double totalFT = 0;
    	double totalPT = 0;
    	double totalSK = 0;
    	double totalTC = 0;
    	
    	
    	out.println("<test_results_summary id=\""+"Summary Results"+"\">");
    	
    	Sheet sheet =wb.getSheet("Summary Results");
       
    	boolean firstRow = true;
        for (Row row : sheet) 
        { 	
        	
            if (firstRow == true) 
            {
            	
                firstRow = false;
                continue;
            }
            out.println("\t<test_summary>");
            out.println(formatElement("\t\t", "scenario", formatCell(row.getCell(0))));
            out.println(formatElement("\t\t", "case", formatCell(row.getCell(1))));
            out.println(formatElement("\t\t", "skip", formatCell(row.getCell(2))));
            out.println(formatElement("\t\t", "passed", formatCell(row.getCell(3))));
            out.println(formatElement("\t\t", "failed", formatCell(row.getCell(4))));
            out.println(formatElement("\t\t", "time", formatCell(row.getCell(5))));
            out.println(formatElement("\t\t", "rate", formatCell(row.getCell(6))));
            out.println("\t</test_summary>");
            
            totalPT = totalPT+row.getCell(3).getNumericCellValue();
        	totalFT = totalFT+row.getCell(4).getNumericCellValue();
        	totalSK = totalSK+row.getCell(2).getNumericCellValue();
        }
        out.println("</test_results_summary>");
        totalExecutedTC = totalPT+totalFT;
        totalTC = totalPT+totalFT+totalSK;
        
       int rate = ((int)totalPT*100/(int)(totalPT+totalFT));
        
        //System.out.println("rate"+rate);
       
       
        
        out.println("\t<total_summary>");
        out.println(formatElement("\t\t", "scenario", "Total"));
        out.println(formatElement("\t\t", "case", String.valueOf( (int)totalTC)));
        out.println(formatElement("\t\t", "skip", String.valueOf( (int)totalSK)));
        out.println(formatElement("\t\t", "passed", String.valueOf( (int)totalPT)));
        out.println(formatElement("\t\t", "failed", String.valueOf( (int)totalFT)));
        out.println(formatElement("\t\t", "time", "0"));
        out.println(formatElement("\t\t", "rate", Integer.toString(rate)+"%"));
       // out.println("\t</Test_scenario>");
    
        out.println("\t</total_summary>");
        
        
        String failUrl="";
        
        boolean firstRow1 = true;
        for (Row row1 : sheet) 
        { 	
        	
            if (firstRow1 == true) 
            {
            	
                firstRow1 = false;
                continue;
            }
            //System.out.println(row1.getCell(4).getNumericCellValue());
            
            if(row1.getCell(4).getNumericCellValue() > 0)
            {
            	
            	Sheet sheet1 = wb.getSheet(row1.getCell(0).getStringCellValue());
                out.println("<test_failed_scenario id=\""+row1.getCell(0).getStringCellValue()+"\">");
                
                boolean firstRow2 = true;
                for (Row row2 : sheet1) {
                    if (firstRow2 == true) {
                        firstRow2= false;
                        continue;
                    }
                    
                   // //System.out.println("row"+row);
                   
                    
                    //System.out.println(formatCell(row2.getCell(3)));
                    if(formatCell(row2.getCell(3)).equalsIgnoreCase("FAIL"))
                    {
                    	 out.println("\t<test_case>");
                    	 
                    	out.println(formatElement("\t\t", "case", charReplace(formatCell(row2.getCell(0)))));
                        out.println(formatElement("\t\t", "expected",charReplace(formatCell(row2.getCell(1)))));
                        out.println(formatElement("\t\t", "actual", charReplace(formatCell(row2.getCell(2)))));
                        out.println(formatElement("\t\t", "status", charReplace(formatCell(row2.getCell(3)))));
                        
                        
                        String failTc1 = formatCell(row2.getCell(0));
                    	String failTc ="";
                    	
                    	String resultsSheet = row1.getCell(0).getStringCellValue();
                    	
                    	//System.out.println("resultsSheet"+resultsSheet);
                    	failTc = resultsSheet+"-"+failTc1;
                    	
                    	//System.out.println(executedTime);
                    	//System.out.println(resultsSheet);
                    	//System.out.println(failTc1);
                    	
                    	if(failTc.length() > 100)
            			{                        		
                    	
                    		failTc = failTc.substring(0, 100).replaceAll("([^a-zA-Z]|\\s)+","_");
            			} 
                    	else
                    	{
                    		failTc = failTc.replaceAll("([^a-zA-Z]|\\s)+","_");
                    	}
                    	
                    	failUrl = executedTime+"-"+failTc+".jpg";
                    	 
                    	//System.out.println("failTc"+failTc);
                    	
                    	out.println("\t\t<failUrl>"+failUrl+"</failUrl>");
                    	                   	                        
                    	failUrl = "";
                    	failTc1="";
                       
   
                    	 out.println("\t</test_case>");
                    	 
                    	 
                    	 
                    }
                    
                    
                   
                    
                   
                }
                
                out.println("</test_failed_scenario>");
                out.println("\t");
            }
           
        }

    }
    
    
    private static void summarySkipTC(PrintWriter out,Workbook wb)
    {    	
    	out.println("<test_results_skip id=\""+"Skip Results"+"\">");
    	
    	Sheet sheet =wb.getSheet("Skip Results");
       
    	boolean firstRow = true;
        for (Row row : sheet) 
        {        	
            if (firstRow == true) 
            {
            	
                firstRow = false;
                continue;
            }
            out.println("\t<test_skip>");
            out.println(formatElement("\t\t", "main_senario", formatCell(row.getCell(0))));
            out.println(formatElement("\t\t", "scenario", formatCell(row.getCell(1))));
            out.println(formatElement("\t\t", "status", formatCell(row.getCell(2))));
                      
            out.println("\t</test_skip>");
            
            
        }
        out.println("</test_results_skip>");
     }
    
    private static String formatCell(Cell cell)
    {
        if (cell == null) {
            return "";
        }
        switch(cell.getCellType()) {
            case Cell.CELL_TYPE_BLANK:
                return "";
            case Cell.CELL_TYPE_BOOLEAN:
                return Boolean.toString(cell.getBooleanCellValue());
            case Cell.CELL_TYPE_ERROR:
                return "*error*";
            case Cell.CELL_TYPE_NUMERIC:
                return TestSendResultsToHtml.df.format(cell.getNumericCellValue());
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();
            default:
                return "<unknown value>";
        }
    }

    private static String formatElement(String prefix, String tag, String value) 
    {
        StringBuilder sb = new StringBuilder(prefix);
        sb.append("<");
        sb.append(tag);
        if (value != null && value.length() > 0) {
            sb.append(">");
            sb.append(value);
            sb.append("</");
            sb.append(tag);
            sb.append(">");
        } else {
            sb.append("/>");
        }
        return sb.toString();
    }
    
    private static void pushFileToServer(ArrayList<String> fileSet) 
    {
    try {
    	
    	File file = new File(fileSet.get(0));
    	
    	System.out.println("File Size "+file.length());
    	if(file.length()/1024 > 1)
    	{
        	String folderName ="";
        	// JSch.setLogger(new MyLogger());
            //System.out.println("Creating session.");
            JSch jsch = new JSch();

            Session session = null;
            Channel channel = null;
            ChannelSftp c = null;

            //
            // Now connect and SFTP to the SFTP Server
            //
            try {
                // Create a session sending through our username and password
                session = jsch.getSession(ftpUserName, ftpHost, ftpPort);
                System.out.println("Session created.");
                session.setPassword(ftpPassword);

                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                session.setConfig(config);
                System.out.println("Session connected before.");
                session.connect();
                System.out.println("Session connected.");

                System.out.println("OPEN SFTP CHANNEL");
                //
                // Open the SFTP channel
                //
                System.out.println("Opening Channel.");
                channel = session.openChannel("sftp");
                channel.connect();
                c = (ChannelSftp) channel;
                System.out.println("Now checing status");
            } catch (Exception e) {
                System.err.println("Unable to connect to FTP server."
                        + e.toString());
                throw e;
            }
            for(int i=0;i<fileSet.size();i++)        	
            {
            	fileToTransmit = fileSet.get(i);       	
            	
            	System.out.println("fileToTransmit"+fileToTransmit);
            	
            	if(fileSet.get(i).indexOf(".xml") >= 0)
            	{
            		folderName = ftpRemoteDirectory +"project_results/BBQ/";
            	}
            	else if(fileSet.get(i).indexOf(".html") >= 0)
            	{
            		folderName = ftpRemoteDirectory +"project_results/BBQ/";
            	} 
            	else if(fileSet.get(i).indexOf(".jpg") >= 0)
            	{
            		folderName = ftpRemoteDirectory +"screenshots/BBQ/";
            	}
            	
            	 //
                // Change to the remote directory
                //
                System.out.println("Now performing operations");
                //ftpRemoteDirectory="/var/ftp";
                System.out.println("Changing to FTP remote dir: "
                        + folderName);
                c.cd(folderName);

                //
                // Send the file we generated
                //
            	
                try {
                    File f = new File(fileToTransmit);
                    System.out.println("Storing file as remote filename: "
                            + f.getName());
                    c.put(new FileInputStream(f), f.getName());
                } catch (Exception e) {
                    System.err
                            .println("Storing remote file failed." + e.toString());
                    throw e;
                }
            	
            }
       

            //
            // Disconnect from the FTP server
            //
            try {
                c.quit();
            } catch (Exception exc) {
                System.err.println("Unable to disconnect from FTPserver. "
                        + exc.toString());
            }
    		
    	}
    	


    } catch (Exception e) {
        System.err.println("Error: " + e.toString());
    }

    System.out.println("Process Complete.");
    //System.exit(0);
    
    
    
    }
}