package com.net.test.scripts;


import com.net.test.pageObject.SeoGoogleTracking;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

public class TestSeoGoogleTracking extends TestBase
{
	TestCommonMethods common = new TestCommonMethods();
	SeoGoogleTracking seoData = new SeoGoogleTracking();

	WebDriver driver = null;

	@BeforeClass
	public void setUp() throws Exception
	{
		
		common.initializeExecutedClass("SEO Tracking","");
	}
	
	@Test public void verifyHomePageProducts() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Verify Home Page Product"))
		{
			seoData.homePageTrackingCode();
		}
		
	}
	
	@Test(dependsOnMethods = { "verifyHomePageProducts" }, alwaysRun = true )
	public void selectUserBrand() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Select Brand"))
		{
			seoData.selectBrand();
		}
		
	}
	
	@Test(dependsOnMethods = { "selectUserBrand" })
	public void selectProductFromLeftCategory() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Select Left Category"))
		{
			seoData.selectProductFromLeftCategory();
		}
		
	}
	
	
	@Test(dependsOnMethods = { "selectProductFromLeftCategory" })
	public void verifyproductGrid() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"View Product Grid"))
		{
			seoData.productGrid();
		}
		
	}
	
	@Test(dependsOnMethods = { "verifyproductGrid" })
	public void verifyProductAvailabilityInProductDetail() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Product Availability"))
		{
			seoData.productAvailability();
		}
	}
	@Test(dependsOnMethods = { "verifyProductAvailabilityInProductDetail" })
	/*public void selectProductInProductDetail() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Product Size By Click"))
		{
			seoData.selectProductSizeByClick();
		}
	}
	
	
	@Test(dependsOnMethods = { "selectProductInProductDetail" })*/
	public void verifyProductNameInProductDetail() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Product Name In Product Detail Page"))
		{
			seoData.productNameInProductDetail();
			//seoData.productNameCompareInProductDetail();
		}	
		

	}
	@Test(dependsOnMethods = { "verifyProductNameInProductDetail"})
	public void verifyProductPriceInProductDetail() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Product Price In Product Detail Page"))
		{
			seoData.productPriceInProductDetail();
			//seoData.productPriceCompareInProductDetail();
		}
		
		
	}
	@Test(dependsOnMethods = { "verifyProductPriceInProductDetail" })
	public void verifyProducQTYInProductDetail() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Product QTY In Product Detail Page"))
		{
			//ckProcess.productQTYInProductDetail();
			//ckProcess.productQTYCompareInProductDetail();
		}
		//ckProcess.productQTYInProductDetail();
	//	ckProcess.productQTYCompareInProductDetail();
		
		/*if(readFile.indexOf("Made For School")>-1||readFile.indexOf("Atdec")>-1||readFile.indexOf("UBT")>-1||readFile.indexOf("Dick Smith")>-1){
				
			ckProcess.productQTYInProductDetail(selenium);
			ckProcess.productQTYCompareInProductDetail(selenium);
		}*/
		

	}
	@Test(dependsOnMethods = { "verifyProducQTYInProductDetail" })
	public void wishList() throws Exception 
	{	
		seoData.wishList();
	}
	
	
	@Test(dependsOnMethods = { "wishList" }, alwaysRun = true )
	public void emailToFriends() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Email To Friends in Product Details"))
		{
			seoData.emailToFriends();
		}
		
	}
	@Test(dependsOnMethods = { "emailToFriends" }, alwaysRun = true )
	public void headerCheckout() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Header Checkout in Product Details"))
		{
			seoData.headerCheckout();
		}
		
	}
	
	@Test(dependsOnMethods = { "headerCheckout" })
	public void verifyaddToCartButton() throws Exception 
	{	
		seoData.addToCart();
		
	}
	
	@Test(dependsOnMethods = { "verifyaddToCartButton" })
	public void verifyViewCart() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"View Cart"))
		{
			seoData.viewCart();
		}
		
		
	}
	@Test(dependsOnMethods = { "verifyViewCart" })
	public void verifypaypalCheckoutButton() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Paypal Button In Shoppping Cart"))
		{
			seoData.paypalInShoppingCart();
		}
	

	}
	
	@Test(dependsOnMethods = { "verifypaypalCheckoutButton" }, alwaysRun = true)
	public void verifyProceedToCheckoutButton() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Proceed To Checkout Button In Shoppping Cart"))
		{
			seoData.proceedToCheckoutButton();
		}
	

	}
	@Test(dependsOnMethods = { "verifyProceedToCheckoutButton" })
	public void verifyCheckoutPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Checkout As Guest With Email"))
		{
			seoData.CheckoutAsGuest_withEmail();	
		}
		else if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
		{
			seoData.CheckoutAsGuest_withoutEmail();	
		}
		
	}
	/*@Test(dependsOnMethods = { "verifyCheckoutPage" })
	public void verifyBillingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			seoData.billingInfoPage();
		}	
		if(isExecuted(getMethodIsExecuted,"Create An Account Later"))
		{
			seoData.createAccountLater();
		}	
		
	
	}
	@Test(dependsOnMethods = { "verifyBillingInfoPage" })
	public void selectDifferntAddress() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Select Different Address"))
		{
			seoData.chkDifferentAddress();				
		}
		if(isExecuted(getMethodIsExecuted,"Continue Billing Button For Different Address"))
		{
			seoData.billingInfoConButton();				
		}
	}
	@Test(dependsOnMethods = { "selectDifferntAddress" })
	public void verifyShippingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Info Page"))
		{
			seoData.shippingInfoPage();				
			
		}
	}
	@Test(dependsOnMethods = { "verifyShippingInfoPage" })
	public void verifyShippingInfoConButton() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Info Continue Button"))
		{
			seoData.shippingInfoConButton();				
			
		}
	}
/*	@Test(dependsOnMethods = { "verifyShippingInfoConButton" })
	public void selectShoppingMethod() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
		{
			ckProcess.selectShippingMethod();
		
		}

	}
	
	@Test(dependsOnMethods = { "selectShoppingMethod" })
	public void continueShoppingMethod() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Method Page"))
		{
			ckProcess.shippingMethodConButton();
		}

	}
	
	@Test(dependsOnMethods = { "continueShoppingMethod" })
	public void verifySelectPaymentMethod() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is CC"))
		{
			ckProcess.paymentMethodIsCC();
		}
		
	}
	@Test(dependsOnMethods = { "verifySelectPaymentMethod" })
	public void fillCreditCardDetails() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is CC"))
		{
			ckProcess.fillCreditCardDetails();
		}
		
	}
	
	@Test(dependsOnMethods = { "fillCreditCardDetails" })
	public void selectPaymentTeamsAndCondition() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Payment - Terms Condition"))
		{
			ckProcess.selectPaymentTeamsAndCondition();
		}
		
	}
	
	@Test(dependsOnMethods = { "selectPaymentTeamsAndCondition" })
	public void payment_submitButton() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is CC"))
		{
			ckProcess.credit_card_payment_submitButton();
			
			//ckProcess.creditCardError();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
		}
		
	}
	
	@Test(dependsOnMethods = { "payment_submitButton" })
	public void verifyOrderReviewPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Order Review"))
		{
			ckProcess.verifyProductPriceInOrderReview();
			
			ckProcess.verifyProductNameInOrderReview();
			
			ckProcess.continueOrderReview();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
		}
			
	}

	@Test(dependsOnMethods = { "verifyOrderReviewPage" })
	public void verifyConfirmationPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Confimation Page"))
		{
			ckProcess.orderIdInConfirmationPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" })
	public void verifyContinueShopping() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Continue Shopping"))
		{
			ckProcess.continueShopping();
		}
		

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" })
	public void verifyConfirmationMail() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Confimation Mail Reader"))
		{
			ckProcess.ConfirmationMaiReader();
		}
		
		

	}*/
	@Test(dependsOnMethods = { "verifyCheckoutPage" }, alwaysRun = true )
	public void verifyTotalTime() throws Exception 
	{	
		
		seoData.totalTime(startTime);

	}
	@AfterClass
	public void stopSelenium() throws Exception
	{
		common.closedBrowser();
		
	}

}