package com.net.test.scripts;

import com.net.test.data.ShoppingCartTestData;
import com.net.test.pageObject.ShoppingCartPage;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;
import org.testng.annotations.*;

public class TestShoppingCartPage extends TestBase
{
	TestCommonMethods common = new TestCommonMethods();
	ShoppingCartPage shoppingCart = new ShoppingCartPage();
	
	
	@BeforeClass
	public void setUp() throws Exception
	{			
		common.initializeExecutedClass("Shopping Cart Page","");		
	}
	
	@Test(priority =1)
	public void verifyMiniCart() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Mini Cart"))
		{
			if(isExecuted(getMethodIsExecuted,"Mini cart has dropdown"))
			{
				shoppingCart.verifyMiniCart();
			}		
		}			
	}
	
@Test(dependsOnMethods = { "verifyMiniCart" } , priority =2)
	public void addProductIntoCart() throws Exception 
	{	
		shoppingCart.addProductIntoCart();		
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 4)
	public void verifyTitleInShoppingCartPage() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Shopping Page Title"))
		{
			shoppingCart.verifyTitle();
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				shoppingCart.shoppingCartProfilerResults("Shopping Cart Page");			
			}
			else
			{
				skipMethods("Shopping Cart Page","Magento Profiler","");

			}	
		}
		else
		{
			skipMethods("Shopping Cart Page","Verify Product Details","");
			throw new AssertionError("No need to run Shopping Cart And Product Page functionalities");

		}
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 3)
	public void verifyProductImageLink() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Product Price/Name/QTY"))
		{
			shoppingCart.verifyProductImageLink();
		}
		else
		{
			skipMethods("Shopping Cart And Product Page","Verify Product Price/Name/QTY","");
			throw new AssertionError("No need to run Verify Product Price/Name/QTY functionalities");
		}
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 5)
	public void verifyProductPriceInshoppingCartPage() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Product Price/Name/QTY"))
		{
			shoppingCart.verifyProductPriceForGuest();
		}
	}
	
//	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 4)
//	public void verifyProductQTYInshoppingCartPage() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Product Price/Name/QTY"))
//		{
//			//shoppingCart.productQTYInshoppingCartPage();
//		}
//	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 5)
	public void verifyProductNameInshoppingCartPage() throws Exception 
	{			
		if(isExecuted(isScenarioExecuted,"Verify Product Price/Name/QTY"))
		{
			shoppingCart.verifyProductName();
		}
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 7)
	public void validateCart() throws Exception 
	{		
		if(isExecuted(isScenarioExecuted,"Verify Shopping Cart Update"))
		{
			shoppingCart.validateCart();
		}
		else
		{
			skipMethods("Shopping Cart And Product Page","Verify Shopping Cart Update","");
			throw new AssertionError("No need to run Verify Shopping Cart Update functionalities");
		}
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 8)
	public void verifyUpdateCart() throws Exception 
	{		
		if(isExecuted(isScenarioExecuted,"Verify Shopping Cart Update"))
		{
			shoppingCart.updateCart();
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				shoppingCart.shoppingCartProfilerResults("Shopping Cart Page - Update Cart");			
			}
		}
	}

	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 9)
	public void verifyContinueShopping() throws Exception 
	{			
		if(isExecuted(isScenarioExecuted,"Verify Shopping Cart Update") && isExecuted(getMethodIsExecuted,"Has continue shopping button"))
		{
			shoppingCart.continueShopping();
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				shoppingCart.shoppingCartProfilerResults("Shopping Cart Page - Continue Shopping");			
			}
		}
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 10)
	public void verifyNoItemInCart() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Product Remove And Empty Cart"))
		{
			shoppingCart.emptyCart();
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				shoppingCart.shoppingCartProfilerResults("Empty Cart Page");			
			}
		}
		else
		{
			skipMethods("Shopping Cart And Product Page","Verify Product Remove And Empty Cart","");
			throw new AssertionError("No need to run Verify Product Remove And Empty Cart functionalities");
		}
	}
	
//	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 10)
//	public void validateEmptyGiftCard() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Gift Card"))
//		{
//			shoppingCart.validateEmptyGiftCard();
//		}
//		else
//		{
//			skipMethods("Shopping Cart And Product Page","Verify Gift Card","");
//			throw new AssertionError("No need to run Verify Gift Card functionalities");
//		}
//	}
	
//	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 11)
//	public void validateInvalidGiftCard() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Gift Card"))
//		{
//			shoppingCart.validateInvalidGiftCard();
//		}
//	}
//	
//	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 12)
//	public void verifyValidGiftCardForPartRedeem() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Gift Card"))
//		{
//			shoppingCart.verifyValidGiftCardForPartRedeem();
//			
//			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
//			{
//				shoppingCart.shoppingCartProfilerResults("Shopping Cart Page - Gift Card Reddem");			
//			}
//		}
//	}
	
//	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 13)
//	public void verifyValidGiftCardForFullRedeem() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Gift Card"))
//		{
//			shoppingCart.verifyValidGiftCardForFullRedeem();
//		}
//	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 11)
	public void validateEmptyCouponCode() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Coupon Code"))
		{
			shoppingCart.validateEmptyCouponCode();
		}
		else
		{
			skipMethods("Shopping Cart And Product Page","Verify Coupon Code","");
			throw new AssertionError("No need to run Verify Coupon Code functionalities");
		}
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 12)
	public void validateInvalidCouponCode() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Coupon Code"))
		{
			shoppingCart.validateInvalidCouponCode();
		}
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 13)
	public void verifyValidCouponCodeWithFixedValue() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Coupon Code"))
		{
			shoppingCart.verifyValidCouponCodeWithFixedValue();
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				shoppingCart.shoppingCartProfilerResults("Shopping Cart Page - Coupon with fixed value");			
			}
		}
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 14)
	public void verifyValidCouponCodeWithPercentOfPriceValue() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Coupon Code"))
		{
			shoppingCart.verifyValidCouponCodeWithPercentOfPriceValue();
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				shoppingCart.shoppingCartProfilerResults("Shopping Cart Page -  Coupon with percent value");			
			}
		}
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" },priority = 15)
	public void verifyProceedToCheckoutButton() throws Exception 
	{			
		if(isExecuted(getMethodIsExecuted,"Proceed To Checkout Button In Shoppping Cart"))
		{
			shoppingCart.proceedToCheckoutButton();
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				shoppingCart.shoppingCartProfilerResults("Shopping Cart Page - Proceed to checkout page");			
			}
		}
	}
	
	@Test(dependsOnMethods = { "verifyProceedToCheckoutButton" }, alwaysRun = true, priority = 17)
	public void verifyTotalTime() throws Exception 
	{			
		shoppingCart.totalTime(startTime);

	}
	
	@AfterClass
	public void stopSelenium() throws Exception
	{
		common.closedBrowser();		
	}
}