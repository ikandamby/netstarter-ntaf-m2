package com.net.test.scripts;


import com.net.test.data.CheckoutTestData;
import com.net.test.pageObject.CheckOutProcess;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;

import org.testng.annotations.*;

public class TestCheckOutAsMember_CreditCard extends TestBase
{
	CheckOutProcess ckProcess = new CheckOutProcess();
	TestCommonMethods common = new TestCommonMethods();
	CheckoutTestData checkoutTestData = new CheckoutTestData();

	@BeforeClass
	public void setUp() throws Exception
	{
		common.initializeExecutedClass("Checkout Process As","Registered-Credit Card");	
		
	}
	@Test public void loginToMyAccount() throws Exception 
	{	
		ckProcess.loginToMyAccount();
		
		common.clearMyCart();
		
		ckProcess.logout();
	
		common.clickLogo();
						
	}
	

	@Test(dependsOnMethods = { "loginToMyAccount" })
	public void addProductIntoCart() throws Exception 
	{	
		ckProcess.addProductIntoCart();
		
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" })
	public void verifyTitleInShoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Title In Shopping Cart Page"))
		{
			ckProcess.titleInshoppingCartPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyTitleInShoppingCartPage" })
	public void verifyProductPriceInshoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Price In Shopping Cart Page"))
		{
			ckProcess.productPriceInshoppingCartPageForMember();
		}
		
		

	}
//	@Test(dependsOnMethods = { "verifyProductPriceInshoppingCartPage" })
//	public void verifyProductQTYInshoppingCartPage() throws Exception 
//	{	
//		if(isExecuted(getMethodIsExecuted,"QTY In Shopping Cart Page"))
//		{
//			//ckProcess.productQTYInshoppingCartPage();
//		}
//		
//		
//
//	}
	@Test(dependsOnMethods = { "verifyProductPriceInshoppingCartPage" } )
	public void verifyProductNameInshoppingCartPage() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Name In Shopping Cart Page"))
		{
			ckProcess.productNameInshoppingCartPage();
		}
		

	}
	@Test(dependsOnMethods = { "verifyProductNameInshoppingCartPage" })
	public void verifyProceedToCheckoutButton() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Proceed To Checkout Button In Shoppping Cart"))
		{
			ckProcess.proceedToCheckoutButton();
		}
	

	}
	@Test(dependsOnMethods = { "verifyProceedToCheckoutButton" })
	public void validateLoginDetails() throws Exception 
	{			
		ckProcess.memberCheckoutLoginWithEmail();
	}
	
	@Test(dependsOnMethods = { "validateLoginDetails" })
	public void validateBillingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			ckProcess.memberNewBillingAddressValidation();
		}
//		if(isExecuted(getMethodIsExecuted,"Select Different Address"))
//		{
//			//ckProcess.chkDifferentAddress();				
//			
//		}
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.billingInfoConButton();	
		
		}
	
	}
	@Test(dependsOnMethods = { "validateBillingInfoPage" })
	public void verifyBillingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			ckProcess.selectNewAddress();
			
			ckProcess.billingInfoPage();		
			
		}	
		
	
	}
	@Test(dependsOnMethods = { "verifyBillingInfoPage" })
	public void verifyBillingInfoConButton() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.billingInfoConButton();
			
		}
	}

	
	
	
/*--------------------------------------*/
	
	@Test(dependsOnMethods = { "verifyBillingInfoConButton" } ,priority = 11)
	public void validateDeliveryInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Delivery Info Page"))
		{
			ckProcess.validateDeliveryInforPage();
		}	
		
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.DeliveryInfoConButton();	
		}
		
		
	
	}
	
	@Test(dependsOnMethods = { "validateDeliveryInfoPage" } , priority = 12)
	public void verifyDeliveryInfoConButton() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Use same billing address"))
		{
			ckProcess.chkSameAddress();		
			
		}
		
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.DeliveryInfoConButton();		
			
		}
	}
	
	/*------------------------------------------------------*/
	
	
	
	
	
	
	
	
	
	
//	@Test(dependsOnMethods = { "verifyBillingInfoConButton" })
//	public void shippingMethodValidation() throws Exception 
//	{	
//		if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
//		{
//			ckProcess.shippingMethodValidation();
//		
//		}
//
//	}
//	
//	@Test(dependsOnMethods = { "shippingMethodValidation" })
//	public void selectShoppingMethod() throws Exception 
//	{	
//		if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
//		{
//			ckProcess.selectShippingMethod();
//		
//		}
//
//	}
	
	@Test(dependsOnMethods = { "verifyDeliveryInfoConButton" })
	public void continueShoppingMethod() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Method Page"))
		{
			ckProcess.shippingMethodConButton();
		}

	}
	
	@Test(dependsOnMethods = { "continueShoppingMethod" })
	public void verifySelectPaymentMethod() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is CC"))
		{
			ckProcess.paymentMethodIsCC();
		}
		
	}
	@Test(dependsOnMethods = { "verifySelectPaymentMethod" })
	public void fillCreditCardDetails() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is CC"))
		{
			ckProcess.fillCreditCardDetails();
		}
		
	}

//	@Test(dependsOnMethods = { "fillCreditCardDetails" })
//	public void selectPaymentTeamsAndCondition() throws Exception 
//	{
//		if(isExecuted(getMethodIsExecuted,"Payment - Terms Condition"))
//		{
//			ckProcess.selectPaymentTeamsAndCondition();
//		}
//		
//	}
	
	@Test(dependsOnMethods = { "fillCreditCardDetails" })
	public void payment_submitButton() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is CC"))
		{
			ckProcess.credit_card_payment_submitButton();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
			
			ckProcess.creditCardError();
		}
		
	}
	
	@Test(dependsOnMethods = { "payment_submitButton" },priority = 1)
	public void verifyOrderReviewPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Order Review"))
		{
			ckProcess.verifyProductPriceInOrderReview();
			
			ckProcess.verifyProductNameInOrderReview();
			
			ckProcess.continueOrderReview();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
		}
		
			
		
	}	
	@Test(dependsOnMethods = { "payment_submitButton" },priority = 2)
	public void verifyConfirmationPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Confimation Page"))
		{
			ckProcess.orderIdInConfirmationPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" },priority = 1)
	public void verifyContinueShopping() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Continue Shopping"))
		{
			ckProcess.continueShopping();
		}	

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" },priority = 2)
	public void verifyConfirmationMail() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Confimation Mail Reader"))
		{
			ckProcess.ConfirmationMaiReader();
		}

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" },priority = 3)
	public void verifyOrderDisplayedInMyAccount() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Order In My Account"))
		{
			ckProcess.verifyOrderDisplayedInMyAccount();
		}		

	}
	@Test(dependsOnMethods = { "verifyOrderDisplayedInMyAccount" })
	public void verifyOrderInMyOrderSection() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Order In My Account"))
		{
						
			ckProcess.verifyOrderInMyOrderSection();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyOrderInMyOrderSection" }, alwaysRun = true )
	public void verifyTotalTime() throws Exception 
	{	
		
		ckProcess.totalTime(startTime);

	}
	@AfterClass
	public void stopSelenium() throws Exception
	{
			common.closedBrowser();
		
	}


}