package com.net.test.scripts;

import com.net.test.data.HomeTestData;
import com.net.test.pageObject.HomePage;
import com.net.test.util.TestBase;
import org.testng.annotations.*;

public class TestHomePage extends TestBase
{
	
	public HomePage home = new HomePage();
	
	
	
	public HomeTestData homeTestData = new HomeTestData();
	String executedClassName = "Home Page";
	
	@BeforeClass
	@Parameters("Test_Suite_Type")
	public void setUp(String Test_Suite_Type) throws Exception
	{		
		getTestSuiteType(Test_Suite_Type);		
		
		common.readMainParameters();		
								
		wResult.createFile(); //Create new results file
		
		if(isExecuted(getMethodIsExecuted,"has update UJ document"))
		{
			getUJDocument();	
		}
		
		
		
		common.initializeExecutedClass(executedClassName,"");		
	}
	
	
	
	@Test(priority = 1 )
	public void homePageMagentoProfilerResults() throws Exception 
	{
		common.magentoProfilerResults("Home Page");		  	
	}
	

//	@Test(dependsOnMethods = { "homePageMagentoProfilerResults" }, priority = 25)
//	public void verifyHomePageTitle() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Home Page Title"))
//		{
//			home.appLoadResults();			
//		}
//		else
//		{
//			skipMethods(executedClassName,"Verify Home Page Title","");			
//		}
//	}	
//	
//	
//
//	@Test(dependsOnMethods = { "verifyHomePageTitle" })
//	public void verifyMainLogo() throws Exception 
//	{			
//		if(isExecuted(isScenarioExecuted,"Verify Main Logo"))
//		{
//			home.mainLogo();			
//		}		
//		else
//		{
//			skipMethods(executedClassName,"Verify Main Logo","");			
//		}  	
//	}
//	
//
//	
//	@Test(dependsOnMethods = { "verifyMainLogo" })
//	public void verifyMainLogoToolTip() throws Exception 
//	{			
//		if(isExecuted(isScenarioExecuted,"Verify Main Logo"))
//		{
//			home.mainLogoToolTip();			
//		}	  	
//	}
//	
//
//
	@Test(dependsOnMethods = { "verifyMainLogoToolTip" })
	public void verifyMainNavigation() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Main Navigation"))
		{
			home.appLink("Main Navigation");			
		}
		else
			
		{
			skipMethods(executedClassName,"Verify Main Navigation","");				
		}				
	}
//	
//	@Test(dependsOnMethods = { "verifyMainNavigation" })
//	public void verifySearchTerm() throws Exception 
//	{			
//		if(isExecuted(isScenarioExecuted,"Verify Basic Search"))
//		{
//			home.searchTerm();	
//			
//			home.searchResults();
//		}
//		else
//		{
//			skipMethods(executedClassName,"Verify Basic Search","");								
//		}
//	}
//	
//	@Test(dependsOnMethods = { "verifySearchTerm" })
//	public void verifyFooterLinks() throws Exception 
//	{		
//		if(isExecuted(isScenarioExecuted,"Verify Footer Links"))
//		{
//			
//			home.appLink("Footer Links");			
//		}
//		else
//		{
//			skipMethods(executedClassName,"Verify Footer Links","");			
//		}				
//	}
//
//	@Test(dependsOnMethods = { "verifyFooterLinks" })
//	public void verifyHeaderLinks() throws Exception 
//	{			
//		if(isExecuted(isScenarioExecuted,"Verify Header Links"))
//		{
//			home.appLink("Header Links");			
//		}
//		else
//		{
//			skipMethods(executedClassName,"Verify Header Links","");			
//		}				
//	}
//	
//
//	@Test(dependsOnMethods = { "verifyHeaderLinks" })
//	public void verifySocialMediaLinks() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Social Media Links"))
//		{
//			home.verifySocialMediaLinks();			
//		}
//		else
//		{
//			skipMethods(executedClassName,"Verify Main Navigation","");				
//		}				
//	}
//	
//	
//	
//	
//	@Test(dependsOnMethods = { "verifySocialMediaLinks" })
//	public void verifyNoOfItemsInMiniCartItemCount0() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Mini Cart"))
//		{			
//			home.noOfItemsInMiniCartItemCountIsZero();						
//		}	
//		else
//		{
//			skipMethods(executedClassName,"Verify Mini Cart","");			
//		}		
//	}
//
//	@Test(dependsOnMethods = { "verifyNoOfItemsInMiniCartItemCount0" } )
//	public void verifyNoOfItemsInMiniCartItemCount1() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Mini Cart"))
//		{			
//			home.noOfItemsInMiniCartItemCountIsOne();						
//		}			
//	}
//
//	@Test(dependsOnMethods = { "verifyNoOfItemsInMiniCartItemCount1" })	
//	public void verifyAddToWishlistGuestUser() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Wishlist"))
//		{
//			if(isExecuted(getMethodIsExecuted,"Wishlist In Product Detail Page"))
//			{
//				home.wistlistGuestPrDetails();
//			}			
//			if(isExecuted(getMethodIsExecuted,"Wishlist In Product Listing Page"))
//			{
//				home.wistlistGuestPrListing();
//			}			
//		}
//		else
//		{
//			skipMethods(executedClassName,"Verify Wishlist","");								
//		}		
//	}
//	
//	@Test(dependsOnMethods = { "verifyAddToWishlistGuestUser" } )
//	public void verifyAddToWishlistLoggedUser() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify Wishlist"))
//		{
//			if(isExecuted(getMethodIsExecuted,"Wishlist In Product Detail Page"))
//			{
//				home.addToWishlistLoggedUserPrDetails();
//			}			
//			if(isExecuted(getMethodIsExecuted,"Wishlist In Product Listing Page"))
//			{
//				home.addToWishlistLoggedUserPrListing();
//			}			
//		}	
//	}	
//
//	
//	@Test(dependsOnMethods = { "verifyAddToWishlistLoggedUser" } )
//	public void verifyCopyrightMessage() throws Exception 
//	{			
//		if(isExecuted(isScenarioExecuted,"Verify Copyright Message"))
//		{
//			home.copyrightMessage();			
//		}
//		else
//		{
//			skipMethods(executedClassName,"Verify Copyright Message","");			
//		}
//				
//	}
//	
//	@Test(dependsOnMethods = { "verifyCopyrightMessage" })
//	public void verifyTotalTime() throws Exception 
//	{			
//		home.totalTime(startTime);
//	}	
	
	@AfterClass
	public void stopSelenium() throws Exception
	{		
		common.closedBrowser();		
	}
	
}