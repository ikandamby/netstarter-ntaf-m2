package com.net.test.scripts;

import com.net.test.pageObject.MyAccount;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;
import org.testng.annotations.*;

public class TestMyAccount extends TestBase
{
	public MyAccount account = new MyAccount();	
	public TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{		
		common.initializeExecutedClass("My Account","");			
	}
	
	@Test
	public void verifyLoginPage() throws Exception 
	{	
		account.navigateToLoginPage();	
	}
	
// /*
	
	@Test(dependsOnMethods = { "verifyLoginPage" }, alwaysRun = true)
	public void loginWithoutUserNameAndpw() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the login field validation"))
		{
			account.loginWithoutUserNameAndpw();			
		}
		else
		{
			skipMethods("Verify My Account","Verify the login field validation","");
			throw new AssertionError("No need to run verify the login field validation functionalities");
		}				
	}
	
	@Test(dependsOnMethods = { "verifyLoginPage" },priority = 1)
	public void verifyLoginWithoutUserName() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the login field validation"))
		{
			account.loginWithoutUserName();			
		}
		else
		{
			skipMethods("Verify My Account","Verify the login field validation","");
			throw new AssertionError("No need to run verify the login field validation functionalities");
		}				
	}
	
	@Test(dependsOnMethods = { "verifyLoginWithoutUserName" },priority = 2)
	public void verifyLoginWithoutPw() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the login field validation"))
		{
			account.loginWithoutPW();
			
		}			
	}
	
	@Test(dependsOnMethods = { "verifyLoginWithoutPw" },priority = 3)
	public void verifyLoginInvalidUserName() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the login field validation"))
		{
			account.loginInvalidUserName();			
		}
			
	}
	@Test(dependsOnMethods = { "verifyLoginInvalidUserName" },priority = 4)	
	public void verifyLoginInvalidPW() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the login field validation"))
		{
			account.loginInvalidPW();			
		}			
	}
	
	@Test(dependsOnMethods = { "verifyLoginInvalidPW" },priority = 5)
	//@Test(dependsOnMethods = { "verifyLoginInvalidUserName" }, alwaysRun = true)
	public void verifyLoginInvalidEmail1() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the login field validation"))
		{
			account.loginInvalidEmail1();			
		}			
	}
	
	@Test(dependsOnMethods = { "verifyLoginInvalidEmail1" },priority = 6)
	//@Test(dependsOnMethods = { "verifyLoginInvalidUserName" }, alwaysRun = true)
	public void verifyLoginInvalidEmail2() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the login field validation"))
		{
			account.loginInvalidEmail2();			
		}			
	}
	
	@Test(dependsOnMethods = { "verifyLoginInvalidEmail2" },priority = 7)
	//@Test(dependsOnMethods = { "verifyLoginInvalidUserName" }, alwaysRun = true)
	public void verifyloginPasswordless() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the login field validation"))
		{
			account.loginPasswordless();			
		}
	}
	
	@Test(dependsOnMethods = { "verifyloginPasswordless" },priority = 8)
	public void verifyInvalidLogin() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the login field validation"))
		{
			account.loginInvalidUserNameAndPw();			
		}
	}
	
	//@Test(dependsOnMethods = { "verifyLoginPage" },priority = 7)
	@Test(dependsOnMethods = { "verifyInvalidLogin" }, alwaysRun = true)
	public void verifyForgotPwPage() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the Forgotten Password"))
		{
			account.navigateToForgetPasswordPage();				
			//common.magentoProfilerResults("Forgot password Page");	
		}
		else
		{
			skipMethods("Verify My Account","Verify the Forgotten Password","");
			throw new AssertionError("No need to run Verify the Forgotten Password");
		}
			
	}
	@Test(dependsOnMethods = { "verifyForgotPwPage" })
	public void verifyForgotPWInvalidEmail1() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the Forgotten Password"))
		{
			account.forgotPWInvalidEmail1();						
		}		
	}
	
	@Test(dependsOnMethods = { "verifyForgotPWInvalidEmail1" },priority = 1)
	//@Test(dependsOnMethods = { "verifyForgotPWInvalidEmail1" }, alwaysRun = true)
	public void verifyForgotPWInvalidEmail2() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the Forgotten Password"))
		{			
			account.forgotPWInvalidEmail2();			
		}			
	}
	
	@Test(dependsOnMethods = { "verifyForgotPWInvalidEmail2" },priority = 2)
	//@Test(dependsOnMethods = { "verifyForgotPWInvalidEmail2" }, alwaysRun = true)
	public void verifyForgotPWWithoutEmail() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the Forgotten Password"))
		{
			account.forgotPWWithoutEmail();				
		}			
	}
	
	@Test(dependsOnMethods = { "verifyForgotPWWithoutEmail" },priority = 3)
	//@Test(dependsOnMethods = { "verifyForgotPWWithoutEmail" }, alwaysRun = true)
	public void verifyForgotPWWithInvalidEmail() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the Forgotten Password"))
		{
			account.forgotPWWithInvalidEmail();					
		}			
	}
	
	@Test(dependsOnMethods = { "verifyForgotPWWithInvalidEmail" },priority = 4)
	//@Test(dependsOnMethods = { "verifyForgotPWWithInvalidEmail" }, alwaysRun = true)
	public void verifySendForgotPwToEmail() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the Forgotten Password"))
		{
			account.forgotPWWithValidEmail();						
		}			
	}
	
	@Test(dependsOnMethods = { "verifySendForgotPwToEmail" },priority = 5)
	//@Test(dependsOnMethods = { "verifySendForgotPwToEmail" })
	public void verifyConfirmationMaiReader() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the Forgotten Password"))
		{
			account.ConfirmationMaiReader();
			
		}
			
	}
	
//*/

	@Test(dependsOnMethods = { "verifyConfirmationMaiReader" }, alwaysRun = true)
	public void loginToMyAccount() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the user can successfully login to the system"))
		{		
			account.login();			
		}
		else
		{
			skipMethods("Verify My Account","Verify the user can successfully login to the system","");
			throw new AssertionError("No need to run Verify the user can successfully login to the system functionalities");

		}
			
	}
	
	
	/*@Test(dependsOnMethods = { "loginToMyAccount" },priority = 1)
	public void myAccountmagentoProfilerResults() throws Exception 
	{	
		common.magentoProfilerResults("My Account Page");			
				
	}
	*/
	
	@Test(dependsOnMethods = { "verifyConfirmationMaiReader" },priority = 1)
	public void verifyLoginPageTitle() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify My Account Page Title"))
		{			
			account.myAccountPageTitle();			
		}
		else
		{
			skipMethods("Verify My Account","Verify My Account Page Title","");
			throw new AssertionError("No need to run User_Creation functionalities");		

		}
				
	}
	
	
	
	
	@Test(dependsOnMethods = { "verifyLoginPageTitle" },priority = 2)
	//@Test(dependsOnMethods = { "loginToMyAccount" })
	public void verifyUserName() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify User Name"))
		{
			account.userName();
			
		}
		else
		{
			skipMethods("Verify My Account","Verify User Name","");
			throw new AssertionError("No need to run User_Creation functionalities");

		}
				
	}
	
	@Test(dependsOnMethods = { "verifyUserName" },priority = 3)
	//@Test(dependsOnMethods = { "loginToMyAccount" })
	public void verifyMyAccountLinks() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify My Account links"))
		{
			account.myAccountLinks1("My Account");
			
		}
		else
		{				
			skipMethods("Verify My Account","Verify My Account links","");
			throw new AssertionError("No need to run User_Creation functionalities");
					
		}
				
	}
	
	@Test(dependsOnMethods = { "verifyMyAccountLinks" },priority = 4)
	//@Test(dependsOnMethods = { "verifyMyAccountLinks" })
	public void verifyAccountInformation() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Account Information"))
		{
			account.verifyAccountInformations();
			
		}
		else
		{				
			skipMethods("Verify My Account","Verify Account Information","");
			throw new AssertionError("No need to run Verify Account Information");
					
		}
				
	}
	

	@Test(dependsOnMethods = { "verifyAccountInformation" },priority = 6)
	//@Test(dependsOnMethods = { "editAccountInformation" }, alwaysRun = true)
	public void editPassword() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Edit Account Information"))
		{			
			account.editPassword();			
			
		}	
	}
	
	@Test(dependsOnMethods = { "editPassword" },priority = 7)
	//@Test(dependsOnMethods = { "editPassword" }, alwaysRun = true)
	public void FieldValidationWithoutFirstname() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Edit Account Information"))
		{			
			account.FieldValidationWithoutFirstname();
			
		}	
	}
	
	@Test(dependsOnMethods = { "FieldValidationWithoutFirstname" },priority = 8)
	//@Test(dependsOnMethods = { "FieldValidationWithoutFirstname" }, alwaysRun = true)
	public void FieldValidationWithoutlastname() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Edit Account Information"))
		{			
			account.FieldValidationWithoutlastname();			
			
		}	
	}
	

	
	
	@Test(dependsOnMethods = { "FieldValidationWithoutlastname" },priority =9)
	//@Test(dependsOnMethods = { "passwordValidation2" }, alwaysRun = true)
	public void addNewAddress() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Edit Address Information"))
		{
			account.addNewAddress();
			
			account.editExistAddress();
			
			account.deleteCreatedAddress();
			
		}	
		else
		{				
			skipMethods("Verify My Account","Edit Address Information","");
			throw new AssertionError("No need to run Edit Address Information");
					
		}
	}
	

	@Test(dependsOnMethods = { "addNewAddress" },priority = 10)
	public void wishList() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Wishlist Information"))
		{						
			if(isExecuted(getMethodIsExecuted,"Wishlist In Product Listing Page"))
			{
				account.addToWishlistLoggedUserPrListing();
			}
			if(isExecuted(getMethodIsExecuted,"Wishlist In Product Detail Page"))
			{
				account.addToWishlistLoggedUserPrDetails();
			}			
			
		}	
		else
		{				
			skipMethods("Verify My Account","Wishlist Information","");
			throw new AssertionError("No need to run Wishlist Information");
					
		}
	}	
	
	@Test(dependsOnMethods = { "wishList" },priority = 11)
	//@Test(dependsOnMethods = { "wishList" }, alwaysRun = true)
	public void addAllProductsToCart() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Wishlist Information"))
		{						
			if(isExecuted(getMethodIsExecuted,"Wishlist In Product Listing Page"))
			{
				account.addAllProductsInWishlistListingPage();
			}
			if(isExecuted(getMethodIsExecuted,"Wishlist In Product Detail Page"))
			{
				account.addAllProductsInWishlistDetailsPage();
			}
			
			
		}	
	}

	@Test(dependsOnMethods = { "wishList" },priority = 17, alwaysRun = true)
	public void verifyLogOutLink() throws Exception 
	{	
		account.logOutLink();
	}
	@Test(dependsOnMethods = { "verifyLogOutLink" },priority = 18, alwaysRun = true)
	//@Test(dependsOnMethods = { "addAllProductsToCart" }, alwaysRun = true)
	public void verifyTotalTime() throws Exception 
	{	
		
		account.totalTime(startTime);

	}
	
	@AfterClass
	public void stopSelenium() throws Exception
	{
			common.closedBrowser();

		
	}
		
}