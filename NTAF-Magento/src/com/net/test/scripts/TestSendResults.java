package com.net.test.scripts;

import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.FileSystemOptions;
import org.apache.commons.vfs.Selectors;
import org.apache.commons.vfs.VFS;
import org.apache.commons.vfs.impl.DefaultFileSystemConfigBuilder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import com.net.test.util.BitbucketIssue;
import com.net.test.util.ReadXl;
import com.net.test.util.SendTextMessage;
import com.net.test.util.TestBase;
import com.net.test.util.WriteResults;


public class TestSendResults extends TestBase
{

	ReadXl readXL  = new ReadXl();
	WriteResults wResult =  new WriteResults();
	SendTextMessage sendSMS = new SendTextMessage();
	BitbucketIssue bitBk = new BitbucketIssue();
	TestSendResultsToHtml conHtml = new TestSendResultsToHtml();
		
	public String resultFolder = "";
	public String mailTo;
	public String[] mailList;
	public String[] to;
	
	public int totalExecutedTC = 0;
	public int totalPassedTC = 0;
	public int totalFailedTC = 0;
	public int totalFailedResponse = 0;
	public int totalSkipedTC = 0;
	public String uJDocumentName ="";
	
	@BeforeClass
	public void setUJDocument() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"has update UJ document"))
		{
			uJDocumentName = findElementInXLSheet(getParameterXpath,"user journey document name");
			String remoteFilePath=ReadXl.uJFolder+uJDocumentName ;
			File f=new File(findElementInXLSheet(getParameterXpath,"save user journey document")+uJDocumentName); //Takes the default path, else, you can specify the required path
			  
			if(f.exists())
		    {
		        f.delete();
		    }
		    f.createNewFile(); 
		    FileObject destn=VFS.getManager().resolveFile(f.getAbsolutePath());

		    FileSystemOptions opts=new FileSystemOptions();
		   // DefaultFileSystemConfigBuilder.getInstance().setUserAuthenticator(opts, auth);
		    DefaultFileSystemConfigBuilder.getInstance();


		    FileObject fo=VFS.getManager().resolveFile(remoteFilePath,opts);

		    System.out.println(fo.exists());

		    //fo.createFile();

		    destn.copyFrom(fo,Selectors.SELECT_SELF);
		    destn.close();
		}
		

	    //InputStream is=new FileInputStream(f);
	}
	
	@SuppressWarnings({ "static-access" })
	@Test
	public void testResults() throws Exception
	{	
		try
		{
			sendMail();
		}
		 catch(Exception exc1)
		 {
			 Thread.sleep(5000);
			 sendMail();
			 System.out.println("execption1"+exc1);
		 }
		
		
								        
	}

	public void sendMail()throws Exception
	{
		wResult.getWriteFileName();
		resultFolder = readXL.resultFolder;
		
		final String username = "testautomation084";//change accordingly
	    final String password = "1234@qwerA";//change accordingly

	      // Assuming you are sending email through relay.jangosmtp.net
	      String host = "smtp.gmail.com";

	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.starttls.enable", "true");
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", "587");
	      props.put("mail.smtp.ssl.trust", "smtp.gmail.com");



	      // Get the Session object.
	      Session session = Session.getInstance(props,
	      new javax.mail.Authenticator() {
	         protected PasswordAuthentication getPasswordAuthentication() {
	            return new PasswordAuthentication(username, password);
	         }
	      });

	      
        String from = "Automated Test Results - Netstarter";
        MimeMessage msg = new MimeMessage(session);
        
        String getMailList= "";
        
        String content  = resultsSummary1();
        if(suitType.equalsIgnoreCase("Mini"))
        {
        	if(totalFailedTC > 0 || totalExecutedTC == 0)			
    		{
            	getMailList = "gharshana@netstarter.com;psureshvasan@netstarter.com";     	
            	
    		}
        	else
        	{
        		getMailList= getParameters.get("Email Address List For Mini Suite").toString().trim();
        	}        	
        }
        else if(suitType.equalsIgnoreCase("Full"))
        {
        	if(totalFailedTC > 0 )			
    		{
            	getMailList = "gharshana@netstarter.com";     	
            	
    		}
        	else
        	{
        		getMailList= getParameters.get("Email Address List For Full Suite").toString().trim();
        	}
        	
        }
        else if(suitType.equalsIgnoreCase("UI"))
        {
        	if(totalFailedTC > 0)			
    		{
            	getMailList = "gharshana@netstarter.com";     	
            	
    		}
        	else
        	{
        		getMailList= getParameters.get("Email Address List For UI Suite").toString().trim();
        	}        	
        }
        
        System.out.println("Email address list - "+getMailList);
        
        InternetAddress[] addressTo = null;
        
        String subjectName = "";
        if(suitType.equalsIgnoreCase("Mini"))
        {
        	subjectName = "Smoke";
        }
        else
        {
        	subjectName = suitType;
        }
       
        String subject = "";
        
        if(suitType.equalsIgnoreCase("UI"))
        {
        	subject = "Test Automation Results for "+findElementInXLSheet(getParameterXpath,"save results file name")+" ("+subjectName+")"+" - "+findElementInXLSheet(getParameterXpath,"browser");
        }
        else
        {
        	subject = "Test Automation Results for "+findElementInXLSheet(getParameterXpath,"save results file name")+" ("+subjectName+")";
        }
       
        
     // set the from and to address 
        //InternetAddress addressFrom = new InternetAddress(from); 
       // msg.setFrom(addressFrom); 
        msg.setFrom(new InternetAddress("testautomation084@gmail.com", from));
       // msg.setFrom(new InternetAddress("Sender Name" + "<" + "no-reply@test.com" + ">"));
         if(!getMailList.isEmpty())
	     {
        	//msg.setFrom(new InternetAddress(from));
        	
	      	if(getMailList.indexOf(",")<0 && getMailList.indexOf(";")<0)
	        {
	      		  
			    msg.setRecipients(RecipientType.TO, getMailList);
	        }
	        else 
	        {
	        	if(getMailList.indexOf(",")>-1 )
		       	{
			       	mailList = getMailList.split(",");
			     }
			     else if(getMailList.indexOf(";")>-1)
			     {
			       	mailList = getMailList.split(";");
			     }
	        	 for(int i=0;i<mailList.length;i++)
	        	 {
	        		if(mailList[i].indexOf("@")==-1||mailList[i].indexOf(".")==-1) {
							System.out.println("Following email address is invalid"+mailList[i]);
							wResult.writeTestResult("Verify send email","Following email address is invalid '"+mailList[i]+"' We weren't able to find the recipient domain." +
									"Please check for any spelling errors, and make sure you didn't enter any spaces","", false, writeFileName);
					}
					else
					{
						to =mailList;
					}
						
	        	 }	
	        	    addressTo = new InternetAddress[to.length];
			        for (int i = 0; i < to.length; i++)
			        {
			             addressTo[i] = new InternetAddress(to[i]);
			         }
			         // msg.setFrom(new InternetAddress(from));
				           
			          msg.setRecipients(RecipientType.TO, addressTo);
	        	}
	        												
				 try 
				 {							 
					 //msg.setSubject(subject);
					 //String content  = resultsSummary1();
					 BodyPart messageBodyPart = new MimeBodyPart();
					 BodyPart messageBodyPart1 = new MimeBodyPart();
					 // Create a multipar message
					 Multipart multipart = new MimeMultipart("related");
					 // Fill the message
					// String content  = resultsSummary();
					 
					 
					// MimeMultipart multipart = new MimeMultipart("related");		        
			         

					 if(!content.isEmpty())
					 {
						 
						 messageBodyPart.setContent(content, "text/html; charset=utf-8");									 
						 // add it
				         multipart.addBodyPart(messageBodyPart);
				         
				         // second part (the image)
				         messageBodyPart = new MimeBodyPart();
				         DataSource fds1 = new FileDataSource(readXL.imageFolder+"/netstarter-logo.png");

				         messageBodyPart.setDataHandler(new DataHandler(fds1));
				         messageBodyPart.setHeader("Content-ID", "<image1>");

				         // add image to the multipart
				         multipart.addBodyPart(messageBodyPart);
				         
				         messageBodyPart = new MimeBodyPart();
				         
				         DataSource fds2;
				         if(findElementInXLSheet(getParameterXpath,"site logo").isEmpty())
				         {
				        	  fds2 = new FileDataSource(readXL.imageFolder+"/netstarter-logo.png"); 
				         }
				         else
				         {
				        	
				        	 fds2 = new FileDataSource(readXL.imageFolder+"/"+findElementInXLSheet(getParameterXpath,"site logo"));
				         }
				        
				         
				        // DataSource fds2 = new FileDataSource(readXL.imageFolder+"/taf.png");

				         messageBodyPart.setDataHandler(new DataHandler(fds2));
				         messageBodyPart.setHeader("Content-ID", "<image2>");

				         // add image to the multipart
				         multipart.addBodyPart(messageBodyPart);
					 }
					 else
					 {
						 messageBodyPart.setText("Dear All,\n\nPlease find the attached Automation test report for "+findElementInXLSheet(getParameterXpath,"Save results file name")+" project."+
								 "\n\n\nNetstarter Pty Ltd\n\nP:+61 2 9273 7777 | F:+61 2 8458 0737" +
								 "\nW: www.netstarter.com.au | gharshana@netstarter.com" +
								 "\n\nLevel 2, 26-28 Wentworth Avenue, Surry Hills, NSW 2010");
					 }
					 

					 msg.setSubject(subject+" - "+totalFailedTC+" Tests Failed out of "+ (totalFailedTC+totalPassedTC));

					 //	Set text message part
					
					 // Part two is attachment
					 
					// MimeBodyPart messageAttachment = new MimeBodyPart();  
					 messageBodyPart = new MimeBodyPart();
					 String filename = resultFolder+wResult.WriteResult();
					 DataSource source = new FileDataSource(filename);
					 messageBodyPart.setDataHandler(new DataHandler(source));
					 messageBodyPart.setFileName(wResult.WriteResult());
					 multipart.addBodyPart(messageBodyPart); 
					 
					
					 if(isExecuted(getMethodIsExecuted,"has update UJ document"))
					 {
						 messageBodyPart1 = new MimeBodyPart();
						 String filename1 = ReadXl.uJFolder+findElementInXLSheet(getParameterXpath,"user journey document name");
						 DataSource source1 = new FileDataSource(filename1);
						 messageBodyPart1.setDataHandler(new DataHandler(source1));
						 messageBodyPart1.setFileName(findElementInXLSheet(getParameterXpath,"user journey document name"));;
						 multipart.addBodyPart(messageBodyPart1); 
					 }
					 
					
					 // Send the complete message parts
					 msg.setContent(multipart);
					 
					// System.out.println("totalFailedResponse1"+totalFailedResponse);
					// System.out.println("totalFailedTC"+totalFailedTC);
					// System.out.println("totalExecutedTC"+totalExecutedTC);
					 
					/*Transport transport = session.getTransport("smtp");
						transport.send(msg);
						System.out.println("E-mail sent1 !");
					 */
					 
					// System.out.println("totalFailedResponse"+totalFailedResponse);
					// System.out.println("totalFailedTC"+totalFailedTC);
					 
					/* Transport transport = session.getTransport("smtp");
						transport.send(msg);
						System.out.println("E-mail sent1 !");
					*/
					 
						 if((totalFailedResponse > 0 || totalFailedTC > 0)  && suitType.equalsIgnoreCase("Mini"))
						 {
							 reRunFile("Total"+totalFailedTC +"totalFailedResponse"+totalFailedResponse);
							 System.out.println("send the sms");
							 Transport transport = session.getTransport("smtp");
							  transport.send(msg);
							  System.out.println("E-mail sent2 !"); 
							// sendSMS.sendSMS(totalFailedTC+" Tests Failed out of "+ totalExecutedTC );
							
						 }
						 else
						 {
							 Transport transport = session.getTransport("smtp");
							 transport.send(msg);
							 System.out.println("E-mail sent1 !"); 
						 }
				
				 }
				 catch(Exception exc)
				 {
					 System.out.println(exc);
				 }
				 
	     }
        
         else
         {
        	 wResult.writeTestResult("Verify send email","Email address are not in the data execl sheet.please check and resend", "", false, "CCC");	
         }
        

		
	}
	public static String findElementInXLSheet(HashMap<String, String> mapName,String parameterName)throws Exception
	{
		String parameterValue = "";
		if(mapName.containsKey(parameterName))
		{
			parameterValue =  mapName.get(parameterName).trim(); 
			
		}
		
		return parameterValue ;	
	}
		
	public String resultsSummary1()throws Exception
	{
		String summary = headerContent()+messageContent()+footerContent();
		
		return  summary;
	}
	public String messageContent()throws Exception
	{
		String tableContent = "";
		String tableContentAfterAddedRows = "";
		String senario = "";
		String totalTC = "";
		String passedTC = "";
		String failTC= "";
		String successRate= "";
		String skipTC= "";
		String totalTime= "";
		String backgroundColor = "";
		String statusColor = "";
		String skipColor = "#C0C0C0";
		int responseStatus = 0;
		String responseError = "";
		

		//System.out.println("wResult.senarios.size()"+WriteResults.senarios.size());
		//for(int i = 1;i < 5; i++)
		for(int i = 0;i < WriteResults.senarios.size(); i++)
		{
			
			if ( i % 2 == 0 )
			{
				backgroundColor = "#f4f4f4";
			}
			else
			{
				backgroundColor = "#dddbdb";
			}
			
			/*if(Integer.parseInt(WriteResults.SuccessRate.get(i)) >= 75)
			{
				statusColor = "#00B62C";
			}
			else
			{
				statusColor = "#D80000";
			}
				*/
			tableContent = bodyContent();	
			
			
			
			
			//System.out.println("totalExecutedTC"+totalExecutedTC);
			//System.out.println("totalPassedTC"+totalPassedTC);
			//System.out.println("totalFailedTC"+totalFailedTC);
			
			
			senario = WriteResults.senarios.get(i);
			totalTC = WriteResults.executedTC.get(i);
			passedTC = WriteResults.passedTC.get(i);
			failTC= WriteResults.failedTC.get(i);
			successRate = WriteResults.SuccessRate.get(i);
			skipTC = WriteResults.skipedTC.get(i);
			totalTime = WriteResults.SenarioExecutionTime.get(i);
			
			
			totalExecutedTC = totalExecutedTC+Integer.parseInt(totalTC);
			totalPassedTC = totalPassedTC+Integer.parseInt(passedTC); 
			totalFailedTC = totalFailedTC+Integer.parseInt(failTC); 
			totalSkipedTC = totalSkipedTC+Integer.parseInt(skipTC); 
			
			if(suitType.equalsIgnoreCase("Mini"))
			{
				senario = "Smoke Test";
				totalTC = String.valueOf(totalFailedTC+totalPassedTC);
				skipTC = "0";
				
			}
			
			//System.out.println("totalFailedTC"+totalFailedTC);
			if(senario.indexOf("Credit Card") >= 0)
			{
				senario = "Checkout-"+senario;
			}
			else if(senario.indexOf("CC") >= 0)
			{
				senario = "Checkout-"+senario.replaceAll("CC", "Credit Card");
			}
			else if(senario.indexOf("Paypal") >= 0)
			{
				senario = "Checkout-"+senario;
			}
			else if(senario.indexOf("PP") >= 0)
			{
				senario = "Checkout-"+senario.replaceAll("PP", "PayPal");
			}
			
			//System.out.println("senario"+senario);
			//System.out.println("totalTC"+totalTC);
			//System.out.println("passedTC"+passedTC);
			//System.out.println("failTC"+failTC);
			//System.out.println("successRate"+successRate);
			tableContent = tableContent.replaceAll("senario", senario)
							.replaceAll("totalTC", totalTC)
							.replaceAll("passedTC", passedTC)
							.replaceAll("failedTC", failTC)
							.replaceAll("SkipTC", skipTC)
							.replaceAll("SuccessRate", successRate+"%")
							.replaceAll("TotalTime", totalTime)
							.replaceAll("setBGColor", backgroundColor);
			
			//System.out.println("tableContent"+tableContent);
			tableContentAfterAddedRows =  tableContentAfterAddedRows + tableContent;
			
			//System.out.println(tableContentAfterAddedRows+"tableContentAfterAddedRows");
		}
		
		for(int i = 0;i < WriteResults.faildResponsesenarios.size(); i++)
		{
			
			if ( i % 2 == 0 )
			{
				backgroundColor = "#f4f4f4";
			}
			else
			{
				backgroundColor = "#dddbdb";
			}
			
			/*if(Integer.parseInt(WriteResults.SuccessRate.get(i)) >= 75)
			{
				statusColor = "#00B62C";
			}
			else
			{
				statusColor = "#D80000";
			}
				*/
			tableContent = bodyContent();				
			
			
			senario = WriteResults.faildResponsesenarios.get(i);
			totalTC = WriteResults.faildResponsesenariosDesc.get(i);
			
			totalFailedTC++;
			responseStatus =  Integer.parseInt(totalTC.substring(totalTC.indexOf("response got it:")+16, totalTC.indexOf("-")).trim());
			responseError = totalTC.substring(totalTC.indexOf("-")+1).trim();
			
			//System.out.println("responseStatus"+responseStatus);
			//System.out.println("responseError"+responseError);
			
			
			if(responseStatus > 0)
			{
				totalFailedResponse++;	
			}
			else if(!responseError.isEmpty() && !responseError.equalsIgnoreCase("Connection timed out: connect"))
			{
				totalFailedResponse++;
			}
			
			
			tableContent = tableContent.replaceAll("senario", senario)
							.replaceAll("totalTC", totalTC)
							.replaceAll("passedTC", "")
							.replaceAll("failedTC", "")
							.replaceAll("SkipTC", "")
							.replaceAll("SuccessRate", "")
							.replaceAll("TotalTime", "")
							.replaceAll("setBGColor", backgroundColor);
			
			
			tableContentAfterAddedRows =  tableContentAfterAddedRows + tableContent;
			
			//System.out.println(tableContentAfterAddedRows+"tableContentAfterAddedRows");
		}
		
		
		//System.out.println("totalExecutedTC"+totalExecutedTC);
		//System.out.println("totalPassedTC"+totalPassedTC);
		//System.out.println("totalFailedTC"+totalFailedTC);
		
		return tableContentAfterAddedRows;					
	}
		
	public String headerContent() throws Exception{		

		BufferedReader in = new BufferedReader(new FileReader(readXL.imageFolder+"/header.txt"));
		
		String line;
		String headerTxt ="";
		while((line = in.readLine()) != null)
		{
			headerTxt = headerTxt+"\n"+line;			
		}
		in.close();	
		
		String tableContent = headerTxt;
		String mailTitle = "";
		if(suitType.equalsIgnoreCase("Mini"))
        {
			mailTitle =findElementInXLSheet(getParameterXpath,"send mail title for mini suite");
        }
		else
		{
			mailTitle =findElementInXLSheet(getParameterXpath,"send mail title for full suite");
		}
		
		//System.out.println("Project Name"+wResult.fileName);
		tableContent = tableContent.replaceAll("ResultDate", wResult.readTime1)
						.replaceAll("ProjectName", wResult.fileName)
						.replaceAll("MailTitle", mailTitle);
		return tableContent;
	}
	
	public String bodyContent() throws Exception{
		
		BufferedReader in = new BufferedReader(new FileReader(readXL.imageFolder+"/body.txt"));
		
		String line;
		String bodyTxt ="";
		while((line = in.readLine()) != null)
		{
			bodyTxt = bodyTxt+"\n"+line;
		}
		in.close();
		
		return bodyTxt;
	}
	
	public String footerContent() throws Exception
	{	
		BufferedReader in = new BufferedReader(new FileReader(readXL.imageFolder+"/footer.txt"));
		
		String line;
		String footerTxt ="";
		while((line = in.readLine()) != null)
		{
			footerTxt = footerTxt+"\n"+line;
		}
		in.close();
		if(suitType.equalsIgnoreCase("Mini"))
        {
			bitBk.getBitBucketData();
			
			String notMergeCommit = "";
			
			notMergeCommit = bitBk.notMergeCommit.toString();
			
			if(notMergeCommit.equalsIgnoreCase(""))
			{		
				notMergeCommit = "All the tasks are merged from 'develop' into release-Staging"; 
				footerTxt = footerTxt.replaceAll("lastMergeTime", "Last Merge branch 'develop' into release-Staging is - "+bitBk.converTime +" - by: "+bitBk.lastMergeUser)
						.replaceAll("notMergeItem", notMergeCommit);
			}
			else
			{
				notMergeCommit = "Following task(s) not merge to this deployment"  +notMergeCommit;
				
				footerTxt = footerTxt.replaceAll("lastMergeTime", "Last Merge branch 'develop' into release-Staging is - "+bitBk.converTime +" - by: "+bitBk.lastMergeUser)
						.replaceAll("notMergeItem", notMergeCommit);	
			}			
        }
		else
		{
			footerTxt = footerTxt.replaceAll("lastMergeTime", "")
					.replaceAll("notMergeItem", "");
		}
		
		return footerTxt;
	}
	
	
}
