package com.net.test.scripts;


import com.net.test.data.CheckoutTestData;
import com.net.test.pageObject.CheckOutProcess;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;
import org.testng.annotations.*;

public class TestCheckOutAsGuest_Money_Order_WithCoupon extends TestBase
{
	CheckOutProcess ckProcess = new CheckOutProcess();
	TestCommonMethods common = new TestCommonMethods();
	CheckoutTestData checkoutTestData = new CheckoutTestData();
	
	@BeforeClass
	public void setUp() throws Exception
	{
		common.initializeExecutedClass("Checkout Process As","Guest-Money Order With Coupon");			
	}
	@Test public void addProductIntoCart() throws Exception 
	{	
		ckProcess.addProductIntoCart();
		
	}
	
	@Test(dependsOnMethods = { "addProductIntoCart" })
	public void verifyTitleInShoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Title In Shopping Cart Page"))
		{
			ckProcess.titleInshoppingCartPage();
		}		

	}
	@Test(dependsOnMethods = { "verifyTitleInShoppingCartPage" })
	public void verifyProductPriceInshoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Price In Shopping Cart Page"))
		{
			ckProcess.productPriceInshoppingCartPageForGuest();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyProductPriceInshoppingCartPage" })
	public void verifyProductQTYInshoppingCartPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"QTY In Shopping Cart Page"))
		{
			//ckProcess.productQTYInshoppingCartPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyProductQTYInshoppingCartPage" } )
	public void verifyProductNameInshoppingCartPage() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Name In Shopping Cart Page"))
		{
			ckProcess.productNameInshoppingCartPage();
		}
		

	}
	@Test(dependsOnMethods = { "verifyProductNameInshoppingCartPage" })
	public void redeemCouponInshoppingCartPage() throws Exception 
	{	
		ckProcess.redeemCouponInShoppingcart();
	}
	
	@Test(dependsOnMethods = { "redeemCouponInshoppingCartPage" })
	public void verifyProceedToCheckoutButton() throws Exception 
	{	
		
		if(isExecuted(getMethodIsExecuted,"Proceed To Checkout Button In Shoppping Cart"))
		{
			ckProcess.proceedToCheckoutButton();
		}
	

	}
	@Test(dependsOnMethods = { "verifyProceedToCheckoutButton" })
	public void verifyCheckoutPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Checkout As Guest With Email"))
		{
			ckProcess.CheckoutAsGuest_withEmail();	
		}
		else if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
		{
			ckProcess.CheckoutAsGuest_withoutEmail();	
		}
		
	}
	@Test(dependsOnMethods = { "verifyCheckoutPage" })
	public void validateBillingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			ckProcess.validateBillingInforPage();
		}	
		if(isExecuted(getMethodIsExecuted,"Create An Account Later"))
		{
			//ckProcess.createAccountLater();
		}	
		
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.billingInfoConButton();	
		
		}
		
		
	
	}
	@Test(dependsOnMethods = { "validateBillingInfoPage" })
	public void verifyBillingInfoPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			ckProcess.fillGuestUserDetails();
			
			ckProcess.billingInfoPage();
		}	
		if(isExecuted(getMethodIsExecuted,"Create An Account Later"))
		{
			ckProcess.createAccountLater();
		}	
		
	
	}
	@Test(dependsOnMethods = { "verifyBillingInfoPage" })
	public void verifyBillingInfoConButton() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Billing Info Continue Button"))
		{
			ckProcess.billingInfoConButton();	
			
			ckProcess.verifyErrorMsgIBillingInfor();
			
		}
	}
	@Test(dependsOnMethods = { "verifyBillingInfoConButton" })
	public void shippingMethodValidation() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
		{
			ckProcess.shippingMethodValidation();
		
		}

	}
	
	@Test(dependsOnMethods = { "shippingMethodValidation" })
	public void selectShoppingMethod() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
		{
			ckProcess.selectShippingMethod();
		
		}

	}
	
	@Test(dependsOnMethods = { "selectShoppingMethod" })
	public void continueShoppingMethod() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Shipping Method Page"))
		{
			ckProcess.shippingMethodConButton();
		}

	}
	
	@Test(dependsOnMethods = { "continueShoppingMethod" })
	public void verifySelectPaymentMethod() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is Money Order"))
		{
			ckProcess.paymentMethodIsMoneyOrder();
		}
		
	}
	
	@Test(dependsOnMethods = { "verifySelectPaymentMethod" })
	public void selectPaymentTeamsAndCondition() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Payment - Terms Condition"))
		{
			ckProcess.selectPaymentTeamsAndCondition();
		}
		
	}
	
	@Test(dependsOnMethods = { "selectPaymentTeamsAndCondition" })	
	public void moneyOrderSubmitButton() throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Money Order"))
		{
			ckProcess.moneyOrderSubmitButton();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
		}
		
	}
	
	@Test(dependsOnMethods = { "moneyOrderSubmitButton" })
	public void verifyOrderReviewPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Order Review"))
		{
			ckProcess.verifyProductPriceInOrderReview();
			
			ckProcess.verifyProductNameInOrderReview();
			
			//ckProcess.billingAddressInReviewPage();
			
			//ckProcess.shippingAddressInReviewPage();
			
			ckProcess.continueOrderReview();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
		}
		
			
		
	}
	
	@Test(dependsOnMethods = { "verifyOrderReviewPage" })
	public void verifyConfirmationPage() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Confimation Page"))
		{
			ckProcess.orderIdInConfirmationPage();
		}
		
		

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" },priority = 1)
	public void verifyContinueShopping() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Continue Shopping"))
		{
			ckProcess.continueShopping();
		}	

	}
	@Test(dependsOnMethods = { "verifyConfirmationPage" },priority = 2)
	public void verifyConfirmationMail() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Confimation Mail Reader"))
		{
			ckProcess.ConfirmationMaiReader();
		}

	}

	@Test(dependsOnMethods = { "verifyConfirmationMail" }, alwaysRun = true )
	public void verifyTotalTime() throws Exception 
	{	
		
		ckProcess.totalTime(startTime);

	}
	@AfterClass
	public void stopSelenium() throws Exception
	{
			common.closedBrowser();
		
	}
	
}