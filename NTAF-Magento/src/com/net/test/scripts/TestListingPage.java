package com.net.test.scripts;

import com.net.test.pageObject.ListingPage;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;
import org.testng.annotations.*;

public class TestListingPage extends TestBase
{
	TestCommonMethods common = new TestCommonMethods();
	ListingPage listing = new ListingPage();
	
	@BeforeClass
	public void setUp() throws Exception
	{	
		common.initializeExecutedClass("Product Listing And Details","");		
	}
	
	@Test
	public void addToCartValidation() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Add To Cart Validation"))
		{
			listing.addToCartValidation();
		}		
	}
	
	@Test(dependsOnMethods = { "addToCartValidation" })
	public void navigateToSearchResults() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Product Search Functionality"))
		{
			listing.searchTerm();
			listing.searchResults();			
		}
		if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
		{
			listing.productSearchResultsProfilerResults("Product Search Results Page");			
		}			
	}	
	
	@Test(dependsOnMethods = { "navigateToSearchResults" },priority = 1)
	public void verifyProductDisplayTypeIsGridInProductSearch() throws Exception 
	{
		if(isExecuted(isScenarioExecuted,"Product Search Functionality"))
		{
			listing.verifyProductDisplayTypeIsGridInProductSearch();
		}		
	}

	@Test(dependsOnMethods = { "verifyProductDisplayTypeIsGridInProductSearch" },priority = 2)
	public void searchResultsSortByPriceAsc() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Product Search Functionality") && isExecuted(getMethodIsExecuted,"search results sort by price asc"))
		{
			listing.sortByPriceAsc("product search results");
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				listing.productSearchResultsProfilerResults("Product Search Page - Sort by price asc");			
			}
		}			
	}	
	
	@Test(dependsOnMethods = { "searchResultsSortByPriceAsc" },priority = 3)
	public void searchResultsSortByPriceDesc() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Product Search Functionality") && isExecuted(getMethodIsExecuted,"search results sort by price desc"))
		{
			listing.sortByPriceDesc("product search results");
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				listing.productSearchResultsProfilerResults("Product Search Page - Sort by name desc");			
			}
		}
		
	}	
	
	@Test(dependsOnMethods = { "searchResultsSortByPriceDesc" },priority = 4)
	public void verifySearchResultsContent() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Product Search Functionality") && isExecuted(getMethodIsExecuted,"search results content"))
		{
			listing.verifySearchResultsContent();
		}
		
	
	}	
	
	@Test(dependsOnMethods = { "verifySearchResultsContent" }, alwaysRun = true)
	public void navigateToCategoryPage() throws Exception 
	{	
		listing.navigateToCategoryPage();
		
	}	
	
	@Test(dependsOnMethods = { "navigateToCategoryPage" })
	public void verifyProductDisplayType() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Default product view is grid"))
		{
			listing.verifyProductDisplayTypeIsGrid();
		}
	}	
		
	
	
//	@Test(dependsOnMethods = { "verifyProductDisplayType" }, alwaysRun = true)
//	public void getTotalProductCount() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify no of pages/products count"))
//		{
//			listing.getTotalProductCount();	
//		}		
//		
//	}	
	
//	@Test(dependsOnMethods = { "getTotalProductCount" }, alwaysRun = true)
//	public void getTotalNoOfPages() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Verify no of pages/products count"))
//		{
//			listing.getTotalNoOfPages();
//		}		
//					
//	}	
	
//	@Test(dependsOnMethods = { "getTotalNoOfPages" }, alwaysRun = true)
//	public void navigateToNextPreviousPages() throws Exception 
//	{
//		if(isExecuted(isScenarioExecuted,"Verify next/previous functionality"))
//		{
//			listing.navigateToNextPreviousPages();
//		}
//		else
//		{
//			skipMethods("Product Listing","Verify next/previous functionality","");
//			
//		}	
//		
//	}	
	
//	@Test(dependsOnMethods = { "navigateToNextPreviousPages" }, alwaysRun = true)	
//	public void sortByNameAsc() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Product Listing Sort Functionality") && isExecuted(getMethodIsExecuted,"search results sort by name asc"))
//		{
//			listing.sortByNameAsc("product listing");
//			
//			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
//			{
//				listing.productSearchResultsProfilerResults("Product listing Page - Sort by name asc");			
//			}
//		}
//		else
//		{
//			skipMethods("Product Listing","Product Listing Sort Functionality","");
//			
//		}
//		
//		
//	}	
//	
//	@Test(dependsOnMethods = { "sortByNameAsc" }, alwaysRun = true)	
//	public void sortByNameDesc() throws Exception 
//	{	
//		if(isExecuted(isScenarioExecuted,"Product Listing Sort Functionality") && isExecuted(getMethodIsExecuted,"search results sort by name desc"))
//		{
//			listing.sortByNameDesc("product listing");
//			
//			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
//			{
//				listing.productSearchResultsProfilerResults("Product listing Page - Sort by name desc");			
//			}
//		}	
//		
//	}	
//	
	@Test(dependsOnMethods = { "verifyProductDisplayType" }, alwaysRun = true)
	public void sortByPriceAsc() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Product Listing Sort Functionality") && isExecuted(getMethodIsExecuted,"search results sort by price asc"))
		{
			listing.sortByPriceAsc("product listing");
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				listing.productSearchResultsProfilerResults("Product listing Page - Sort by price asc");			
			}
			
		}
		
	}	
	
	@Test(dependsOnMethods = { "sortByPriceAsc" }, alwaysRun = true)	
	public void sortByPriceDesc() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Product Listing Sort Functionality") && isExecuted(getMethodIsExecuted,"search results sort by price desc"))
		{
			listing.sortByPriceDesc("product listing");
			
			if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
			{
				listing.productSearchResultsProfilerResults("Product listing Page - Sort by price desc");			
			}
		}	
		
	}	
	
	@Test(dependsOnMethods = { "sortByPriceDesc" }, alwaysRun = true)
	public void verifyTotalTime() throws Exception 
	{	
		
		listing.totalTime(startTime);

	}
	
	@AfterClass
	public void stopSelenium() throws Exception
	{
		common.closedBrowser();
		
	}

}