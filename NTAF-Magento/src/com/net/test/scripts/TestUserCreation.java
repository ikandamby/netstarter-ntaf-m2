package com.net.test.scripts;

import com.net.test.data.UserCreationTestData;
import com.net.test.pageObject.UserCreation;
import com.net.test.util.TestBase;
import com.net.test.util.TestCommonMethods;
import org.testng.annotations.*;

public class TestUserCreation extends TestBase
{
	UserCreation user = new UserCreation();
	TestCommonMethods common = new TestCommonMethods();
	
	@BeforeClass
	public void setUp() throws Exception
	{	
		common.initializeExecutedClass("User Registration Page","");		
	}
	
	@Test public void verifyuserRegisterPage() throws Exception 
	{			
		user.navigateToUserRegisterPage();	   				
	}
	
	@Test(dependsOnMethods = { "verifyuserRegisterPage" },priority = 1)
	public void userCreationMagentoProfilerResults() throws Exception 
	{	
	common.magentoProfilerResults("User Creation Page");			
	}
	

	
	@Test(dependsOnMethods = { "verifyuserRegisterPage" },priority = 02)
	public void verifyUserRegisterPageResults() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify User Register Page Title"))
		{
			user.userRegisterPageResults();			
		}
		else
		{
			skipMethods("User Register","Verify User Register Page Title","");
		}
				
	}
	@Test(dependsOnMethods = { "verifyuserRegisterPage" },priority = 3)
	public void verifyNewUserCreation() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Create New User"))
		{			
			user.createNewUser();			
		}
		else
		{		
			skipMethods("User Register","Verify Create New User","");			
		}
				
	}
	@Test(dependsOnMethods = { "verifyNewUserCreation" },priority = 1)
	public void verifyAccountDashboard() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Create New User"))
		{			
			user.accountDashboard();
			//user.ConfirmationMaiReader();
			
		}
				
	}
	@Test(dependsOnMethods = { "verifyNewUserCreation" },priority = 2)
	public void verifydisplayLogOutLink() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify Create New User"))
		{
			user.displayLogOutLink();	
		}
				
	}
	@Test(dependsOnMethods = { "verifyNewUserCreation" },priority = 3 )
	public void verifyCratreAlreadyExistsUser() throws Exception 
	{
		
		if(isExecuted(isScenarioExecuted,"Verify User Register with Already Exists User"))
		{
			user.loadMainPage();
			user.createAlreadyExistsUser();
		}
		else
		{
			skipMethods("User Register","Verify User Register with Already Exists User","");			
						
		}
	}
		
	@Test(dependsOnMethods = { "verifyCratreAlreadyExistsUser" }, alwaysRun = true)
	public void navigateToRegister() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{		
			user.loadRegisterPage();			
		}
		else
		{
			skipMethods("User Register","Verify the field validations","");		
		}
				
	}
	
	
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 1 )
	public void verifyFieldValidationWithoutData() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{		
			user.fieldValidationWithoutData();			
		}
			
	}
	
	
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 2 )
	public void verifyFieldValidationWithoutFirstname() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{		
			user.fieldValidationWithoutFirstname();			
		}		
				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 3 )	
	public void verifyFieldValidationWithoutlastname() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.fieldValidationWithoutlastname();
		}
				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 4 )
	public void verifyFieldValidationWithoutEmail_address() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.fieldValidationWithoutEmail_address();
		}				
	}
	
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 5 )
	public void verifyFieldValidationWithoutConfirmation() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.fieldValidationWithoutConfirmationPassword();
		}				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 6 )
	public void verifyFieldValidationWithoutPassword() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.fieldValidationWithoutPassword();			
		}				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 7 )
	public void verifyPasswordValidation1() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.passwordValidation1();			
		}				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 8 )
	public void verifyPasswordValidation2() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.passwordValidation2();			
		}				
	}
	//edit ------------------------------------------------------------------------------------------
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 9)
		public void verifyPostcodeValidation() throws Exception 
		{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
		
	user.postcodeValidation();			
}				
}
	
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 10 )
	public void verifyEmailValidation2() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.emailValidation2();			
		}				
	}
	
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 12 )
	public void verifyConfirmationEmailValidation1() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.confirmEmailValidation1();			
		}				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 13 )
	public void verifyConfirmationEmailValidation2() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.confirmEmailValidation2();			
		}				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 14 )
	public void verifyCompareEmailAndconfirmEmail() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.compareEmailAndconfirmEmail();			
		}				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 15 )
	public void verifyValidationMsgInBirthdayDay() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations"))
		{
			user.validationMsgInBirthdayDay();			
		}				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 16 )
	public void verifyValidationMsgInBirthdayMonth() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations") && isExecuted(getMethodIsExecuted,"Validation Birthday"))
		{
			user.validationMsgInBirthdayMonth();			
		}				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 17 )
	public void verifyValidationMsgInBirthdayYear() throws Exception 
	{	
		if(isExecuted(isScenarioExecuted,"Verify the field validations") && isExecuted(getMethodIsExecuted,"Validation Birthday"))
		{
			user.validationMsgInBirthdayYear();			
		}				
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 18 )
	public void verifyTermsConditionsLink() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Verify Terms Conditions Link"))
		{
			user.termsConditionsLink();
		}					
	}
	@Test(dependsOnMethods = { "navigateToRegister" },priority = 19 )
	public void verifyPrivacyPolicyLink() throws Exception 
	{	
		if(isExecuted(getMethodIsExecuted,"Verify Privacy Policy Link"))
		{
			user.privacyPolicyLink();
		}					
	}
		
	@Test(dependsOnMethods = { "verifyPrivacyPolicyLink" }, alwaysRun = true )
	public void verifyTotalTime() throws Exception 
	{			
		user.totalTime(startTime);

	}
	
	@AfterClass
	public void stopSelenium() throws Exception
	{
		common.closedBrowser();		
	}	
}