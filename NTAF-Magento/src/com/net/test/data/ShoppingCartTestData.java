package com.net.test.data;

import com.net.test.util.TestBase;

public class ShoppingCartTestData extends TestBase
{
	
	//Get location(xpath)
	public static String shoppingCartTable;
	public static String rmvItemFromCart;
	
	protected static String updateProductQTY;
	protected static String updateButton;
	protected static String shoppingCartImgeLink;

	protected static String productQTYInShoppingCart;
	protected static String proceedToCheckoutButtonInShoppingCart;
	
	protected static String shoppingCartEmptyXpath;
	protected static String msgShoppingCartEmpty;
	protected static String shoppingCartEmptyContinueButton;
	protected static String outOFStockInShoppingCart;
	protected static String continueShoppingButton ;
	
	protected static String lbEnableGiftCardCode;
	protected static String txtGiftCardXoath ;
	protected static String btnGiftCardCode ;
	
	protected static String lbEnableCouponCode;
	protected static String txtCouponCodeXpath;
	protected static String btnCouponCode ;

	protected static String txtGiftCardFullRedeem ;
	protected static String txtGiftCardPartRedeem ;
	protected static String txtCouponCodeFixed ;
	protected static String txtCouponCodePrecent ;
	
	
	protected static String msgSuccessxpath ;
	protected static String msgErrorxpath ;
	
	protected static String msgSuccessForGiftCardFullRedeem ;
	protected static String msgSuccessForGiftCardPartRedeem ;
	protected static String msgErrorGiftCard ;
	
	protected static String msgSuccessForCouponFixed ;
	protected static String msgSuccessForCouponPrecent ;
	protected static String msgErrorCoupon ;
	
	protected static String validationWithoutCouponCodeXpath ;
	protected static String validationWithoutGiftCardXpath ;
	protected static String msgValidationWithoutCouponCode ;
	protected static String msgValidationWithoutGiftCard ;
	
	public static String grandTotalXpathBeforeDiscount ;
	public static String grandTotalXpathAfterDiscount ;
	protected static String grandTotalXpathFullRedeemDiscount ;
	protected static String discountXpath ;
	protected static String couponValueFixed ;
	protected static String couponValuePrecent ;
	protected static String giftCardValuePart;
	
	protected static String linkEditMiniCart;
	protected static String linkRemoveItem;
	protected static String linkGotoShoppingCart;
	protected static String btnUpdateCart;
	protected static String linkCheckout;
	
	public static void getLocation()throws Exception
	{
		shoppingCartTable = findElementInXLSheet(getParameterXpath,"shopping cart table");
		rmvItemFromCart = findElementInXLSheet(getParameterXpath,"remove item from shopping cart");		
	
		updateProductQTY = findElementInXLSheet(getParameterXpath,"qty field in shopping cart page");
		updateButton = findElementInXLSheet(getParameterXpath,"update button");
		
		
		shoppingCartImgeLink = findElementInXLSheet(getParameterXpath,"shopping cart product image");
		
		productQTYInShoppingCart = findElementInXLSheet(getParameterXpath,"qty in shopping cart page");
		proceedToCheckoutButtonInShoppingCart = findElementInXLSheet(getParameterXpath,"proceed to checkout button");
		
		shoppingCartEmptyXpath = findElementInXLSheet(getParameterXpath,"empty cart page title");
		msgShoppingCartEmpty = findElementInXLSheet(getMessages,"msg shopping cart empty");
		shoppingCartEmptyContinueButton = findElementInXLSheet(getParameterXpath,"empty cart continue button");
				
		outOFStockInShoppingCart = findElementInXLSheet(getParameterXpath,"out of stock in shopping cart");
		continueShoppingButton = findElementInXLSheet(getParameterXpath,"continue shopping button");
				
		txtGiftCardXoath = findElementInXLSheet(getParameterXpath,"txt gift card xpath");
		lbEnableGiftCardCode = findElementInXLSheet(getParameterXpath,"enable gift card txt box");
		btnGiftCardCode = findElementInXLSheet(getParameterXpath,"btn gift card");
		
		lbEnableCouponCode = findElementInXLSheet(getParameterXpath,"enable coupon code txt box");
		txtCouponCodeXpath = findElementInXLSheet(getParameterXpath,"txt coupon code xpath");
		btnCouponCode = findElementInXLSheet(getParameterXpath,"btn coupon code");	
		
		msgSuccessxpath = findElementInXLSheet(getParameterXpath,"shopping cart success xpath");
		msgErrorxpath = findElementInXLSheet(getParameterXpath,"shopping cart error xpath");
		
		msgSuccessForGiftCardFullRedeem = findElementInXLSheet(getMessages,"gift card success for full redeem");
		msgSuccessForGiftCardPartRedeem = findElementInXLSheet(getMessages,"gift card success for part redeem");
		msgErrorGiftCard = findElementInXLSheet(getMessages,"gift card error");		
		msgSuccessForCouponFixed = findElementInXLSheet(getMessages,"coupon code success for fixed");
		msgSuccessForCouponPrecent = findElementInXLSheet(getMessages,"coupon code success for precent");
		msgErrorCoupon = findElementInXLSheet(getMessages,"coupon code error");
		
		validationWithoutCouponCodeXpath = findElementInXLSheet(getParameterXpath,"validation without coupon code");
		validationWithoutGiftCardXpath = findElementInXLSheet(getParameterXpath,"validation without gift card");
		
		msgValidationWithoutCouponCode = findElementInXLSheet(getMessages,"validation without coupon code");
		msgValidationWithoutGiftCard = findElementInXLSheet(getMessages,"validation without gift card");
		
		grandTotalXpathBeforeDiscount = findElementInXLSheet(getParameterXpath,"grand total xpath before discount");
		grandTotalXpathAfterDiscount = findElementInXLSheet(getParameterXpath,"grand total xpath after discount");
		grandTotalXpathFullRedeemDiscount = findElementInXLSheet(getParameterXpath,"grand total xpath after full redeem discount");
		discountXpath = findElementInXLSheet(getParameterXpath,"discount xpath");
		couponValueFixed = findElementInXLSheet(getParameterXpath,"coupon code amount");
		couponValuePrecent = findElementInXLSheet(getParameterXpath,"coupon code amount for precent");
		couponValueFixed = findElementInXLSheet(getParameterXpath,"coupon code amount for fixed");
		
		txtGiftCardFullRedeem = findElementInXLSheet(getParameterXpath,"gift card name for full redeem");
		txtGiftCardPartRedeem = findElementInXLSheet(getParameterXpath,"gift card name for part redeem");
		giftCardValuePart = findElementInXLSheet(getParameterXpath,"gift card amount for part part redeem");
		txtCouponCodeFixed = findElementInXLSheet(getParameterXpath,"coupon code name for fixed");
		txtCouponCodePrecent = findElementInXLSheet(getParameterXpath,"coupon code name for precent");
		
		
		linkEditMiniCart  = findElementInXLSheet(getParameterXpath,"edit cart link");
		linkRemoveItem = findElementInXLSheet(getParameterXpath,"remove cart link");
		linkGotoShoppingCart = findElementInXLSheet(getParameterXpath,"goto shopping cart link");
		btnUpdateCart = findElementInXLSheet(getParameterXpath,"update cart button");
		linkCheckout = findElementInXLSheet(getParameterXpath,"goto checkout link");
		
		

	
	}
	
	public static void getDataFromXLSheet()throws Exception
	{

	}
	
	
		
}
