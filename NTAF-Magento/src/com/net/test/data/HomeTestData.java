package com.net.test.data;

import com.net.test.util.TestBase;

public class HomeTestData extends TestBase
{
	
	//Get location(xpath)
	public static String searchTextBox;
	public static String searchTerm;
	public static String searchButton;
	public static String mainLogo;
	protected static String mainLogoToolTip;
	protected static String mainLogoToolTipText;
	
	protected static String wishListLink;
	protected static String wishListErrorXpath;
	protected static String wishListErrorMsg;
	protected static String wishListTable;
	protected static String rmvItemFromWishList;
	protected static String copyright;
	
	public static String siteTitle;
	protected static String copyrightMessage;
	
	protected static String errorMsg;
	
	protected static String newsletterTxtBox;
	protected static String newsletterSubmitButton;
	protected static String newsletterValidationXpath;
	protected static String newsletterValidationMsg;
	protected static String newsletterSuccessXpath;
	protected static String newsletterSuccessMsg;
	protected static String newsletterEmailForGuest;
	protected static String newsletterEmailForMember;
	
	protected static String newsletterPopupFName;
	protected static String newsletterPopupLName;
	protected static String newsletterPopupCEmail;
	protected static String newsletterPopupFNameXpath;
	protected static String newsletterPopupLNameXpath;
	protected static String newsletterPopupCEmailXpath;
	protected static String newsletterPopupFNameErrorXpath;
	protected static String newsletterPopupLNameErrorXpath;
	protected static String newsletterPopupCEmailErrorXpath;
	protected static String newsletterPopupSubmit;
	
	protected static String newsletterAlreadyXpath;
	protected static String newsletterAlreadyMsg;
	protected static String newsletterAlreadyEmail;
	
	protected static String txtPopUp;
	protected static String btnPopUpSubmit;
	protected static String newsLetterpopUpEmptyValidation;
	protected static String newsLetterpopUpInvalidEmail;
	protected static String newsLetterpopUpSuccess;
	protected static String newsLetterpopUpAlreadyExist;
	
	protected static String lnfacebook;
	protected static String lninstagram;
	protected static String lnyoutube;
	
	protected static String facebookTitle;
	protected static String instagramTitle;
	protected static String youtubeTitle;
	
	public void getLocation()throws Exception
	{
	
		//siteUrl 		= findElementInXLSheet(getParameterXpath,"Site URL");
		//secureUrl 		= findElementInXLSheet(getParameterXpath,"Secure Site URL");
		searchTextBox 	= findElementInXLSheet(getParameterXpath,"search text box");
		searchButton	= findElementInXLSheet(getParameterXpath,"search button");
		mainLogo 		= findElementInXLSheet(getParameterXpath,"main logo");
		mainLogoToolTip = findElementInXLSheet(getParameterXpath,"main logo tool tip");
		mainLogoToolTipText = findElementInXLSheet(getParameterXpath,"main logo tool tip text");
		
		wishListLink 	= findElementInXLSheet(getParameterXpath,"wishlist link");
		wishListErrorXpath = findElementInXLSheet(getParameterXpath,"wishlist error xpath");
		wishListTable	= findElementInXLSheet(getParameterXpath,"wishList table");
		rmvItemFromWishList = findElementInXLSheet(getParameterXpath,"remove item from wishlist");
		
		copyright 	= findElementInXLSheet(getParameterXpath,"copyright xpath");
		
		errorMsg = findElementInXLSheet(getParameterXpath,"ErrorMsg");	
		
		newsletterTxtBox = findElementInXLSheet(getParameterXpath,"newsletter subscription txt box");
		newsletterSubmitButton = findElementInXLSheet(getParameterXpath,"newsletter subscription submit button"); 
		newsletterValidationXpath = findElementInXLSheet(getParameterXpath,"newsletter subscription validation xpath");
		newsletterValidationMsg = findElementInXLSheet(getMessages,"newsletter subscription validation msg");
		newsletterSuccessXpath = findElementInXLSheet(getParameterXpath,"newsletter subscription success msg xpath");
		newsletterSuccessMsg = findElementInXLSheet(getMessages,"newsletter subscription success msg");
		
		newsletterAlreadyXpath = findElementInXLSheet(getParameterXpath,"newsletter subscription already msg xpath");
		newsletterAlreadyMsg = findElementInXLSheet(getMessages,"newsletter subscription already msg");
		newsletterAlreadyEmail	= findElementInXLSheet(getParameters,"Newsletter Subscription Already subscrib Email");
		
		
		newsletterPopupFNameXpath = findElementInXLSheet(getParameterXpath,"newsletter subscription popup first name");
		newsletterPopupLNameXpath = findElementInXLSheet(getParameterXpath,"newsletter subscription popup last name");
		newsletterPopupCEmailXpath = findElementInXLSheet(getParameterXpath,"newsletter subscription popup confirm email");
		newsletterPopupFNameErrorXpath = findElementInXLSheet(getParameterXpath,"newsletter subscription popup error first name");
		newsletterPopupLNameErrorXpath = findElementInXLSheet(getParameterXpath,"newsletter subscription popup error last name");
		newsletterPopupCEmailErrorXpath = findElementInXLSheet(getParameterXpath,"newsletter subscription popup error confirm email");
		
		
		newsletterPopupSubmit = findElementInXLSheet(getParameterXpath,"newsletter subscription popup submit button");
		
		txtPopUp = findElementInXLSheet(getParameterXpath,"newsletter popup txt box");
		btnPopUpSubmit = findElementInXLSheet(getParameterXpath,"newsletter popup submit button");
		newsLetterpopUpEmptyValidation = findElementInXLSheet(getParameterXpath,"newsletter popup empty validation xpath");
		newsLetterpopUpSuccess = findElementInXLSheet(getParameterXpath,"newsletter popup success xpath");
		newsLetterpopUpAlreadyExist = findElementInXLSheet(getParameterXpath,"newsletter popup error xpath");
		newsLetterpopUpInvalidEmail = findElementInXLSheet(getParameterXpath,"newsletter popup invalid validation xpath");
		
		lnfacebook = findElementInXLSheet(getParameterXpath,"facebook link");
		lninstagram = findElementInXLSheet(getParameterXpath,"instagram link");
		lnyoutube = findElementInXLSheet(getParameterXpath,"youtube link");
		
	}
	
	public void getDataFromXLSheet()throws Exception
	{
		siteTitle = findElementInXLSheet(getParameters,"Site Title");
		searchTerm =  findElementInXLSheet(getParameters,"Search Term");
		copyrightMessage = findElementInXLSheet(getParameters,"Copyright Message");
		
		newsletterEmailForGuest	 = findElementInXLSheet(getParameters,"Newsletter Subscription Email With Guest");
		newsletterEmailForMember = findElementInXLSheet(getParameters,"Newsletter Subscription With Member User");
		newsletterPopupFName = findElementInXLSheet(getParameters,"Newsletter Subscription First Name");
		newsletterPopupLName = findElementInXLSheet(getParameters,"Newsletter Subscription Last Name");
		
		facebookTitle = findElementInXLSheet(getSocialMediaDetails,"facebook link");
		instagramTitle = findElementInXLSheet(getSocialMediaDetails,"instagram link");
		youtubeTitle = findElementInXLSheet(getSocialMediaDetails,"youtube link");			
		
	}

}
