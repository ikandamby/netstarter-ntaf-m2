package com.net.test.data;

import com.net.test.util.TestBase;

public class SeoGoogleTrackingTestData extends TestBase
{
	
	//Get location(xpath)
	protected static String searchTextBox;
	protected static String searchButton;
	protected static String mainLogo;
	protected static String mainLogoToolTip;
	protected static String wishListLink;
	protected static String wishListTable;
	protected static String copyright;
	protected static String loginLink;
	protected static String linkLogOut;
	protected static String productGrid;
	protected static String leftCategory;
	protected static String addToCartButton;
	
	protected static String outOfStockProductMessage;
	
	protected static String productSizeBlock;
	protected static String productSizeClassName;
	protected static String productSizeAttributeValue;
	protected static String productSizeclassOutofStockValue;
	

	
	protected static String siteTitle;
	protected static String copyrightMessage;
	
	protected static String loginEmailAddress;
	protected static String loginPassword;
	protected static String forgotPw;
	
	protected static String myCartXpath;
	protected static String viewCartId;
	protected static String viewCart;
	protected static String shoppingCartTable;
	protected static String rmvItemFromCart;
	
	protected String productItem;
	protected String productPrice;
	protected String productName;
	protected static String productNameXpath;
	protected static String productPriceXpath;
	protected static String productQTYInDetailPage;
	protected static String productQTYXpath;
	protected static String updateProductQTY;
	protected static String updateButton;
	protected static String shoppingCartTitle;
	protected static String productPriceInShoppingCart;
	protected static String productNameInShoppingCart;
	protected static String productQTYInShoppingCart;
	protected static String proceedToCheckoutButtonInShoppingCart;
	protected static String PaypalCheckoutButtonInShoppingCart;
	
	
	
	
	protected static String productNameXpathInProductDetailPage;
	protected static String productPriceXpathInProductDetailPage;
	protected static String productNameXpathOrderReviewPage;
	protected static String productPriceXpathInOrderReviewPage;
	
	protected static String productNameXpathPaypalReviewPage;
	protected static String productPriceXpathInPaypalReviewPage;
	protected static String btnsubmitPaypalReviewPage;
	
	protected static String getProductAttributes ;
	protected static String getProductName;
	protected static String getProductPrice ;
	protected static String setProductName ;
	
	protected static String btnCheckoutAsGuestUser;	
	protected static String chkCheckoutAsGuestUser;
	protected static String btnBillingInfoContinue;
	protected static String btnShippingInfoContinue;
	protected static String btnShippingMethodContinue;
	protected static String CheckoutAsRegisteredUser;
	protected static String billingLoginEmail;
	protected static String billingEmail;
	protected static String billingFirstName;
	protected static String billingLastName;
	protected static String billingTelephone;
	protected static String billingstreet1;
	protected static String billingCity;
	protected static String billingRegion_Id;
	protected static String billingPostcode;
	protected static String linkaddressHint;
	protected static String billingCountry_Id;
	protected static String billingCreateAccount;
	
	protected static String shippingFirstName;
	protected static String shippingLastName;
	protected static String shippingTelephone;
	protected static String shippingstreet1;
	protected static String shippingCity;
	protected static String shippingRegion_Id;
	protected static String shippingPostcode;
	protected static String shippingCountry_Id;
	
	protected static String getTotalShippingMethods;
	protected static String getShippingMethodTag;
	protected static String selectShippingMethod;
	
	protected static String chkPaymentIsCC;
	protected static String chkPaymentIsPaypal;
	protected static String chkPaymentIsMoneyOrder;
	
	protected static String cCName;
	protected static String cCNumber;
	protected static String cCCvs;
	protected static String cCMonth;
	protected static String cCYear;
	protected static String cCType;
	
	protected static String selectNewAddress_DropDown;
	
	protected static String txtBillingLoginEmail;
	protected static String txtBillingEmail;
	protected static String txtBillingFirstName;
	protected static String txtBillingLastName;
	protected static String txtBillingTelephone;
	protected static String txtBillingstreet1;
	protected static String txtBillingCity;
	protected static String selectBillingRegion_Id;
	protected static String selectBillingRegion_Id_DropDown;
	protected static String selectMemberBillingRegion_Id_DropDown;
	protected static String txtBillingPostcode;
	protected static String selectBillingCountry_Id;
	
	protected static String txtShippingFirstName;						 
	protected static String txtShippingLastName;
	protected static String txtShippingTelephone;
	protected static String txtShippingstreet1;
	protected static String txtShippingCity;
	protected static String selectShippingRegion_Id;
	protected static String selectShippingRegion_Id_DropDown;
	protected static String selectMemberShippingRegion_Id_DropDown;
	protected static String txtShippingPostcode;
	protected static String selectShippingCountry_Id;
	
	
	protected static String txtCCNumber;
	protected static String txtCCName;
	protected static String txtCCCvs;
	protected static String selectCCType;
	protected static String selectCCMonth;
	protected static String selectCCYear;
	protected static String selectCCTypeDropDown;
	protected static String selectCCMonthDropDown;
	protected static String selectCCYearDropDown;
	
	protected static String txtPaypalEmail;
	protected static String txtPaypalPassword;
	protected static String btnPaypalSubmit;
	protected static String btnPaypalContinue;
	
	protected static String paypalEmail;
	protected static String paypalPassword;
	
	protected static String chkTeamsAndCondtion;
	protected static String btnContinuePaymentScreen;	
	protected static String chkDifferentShipAddress;

	
	protected static String validationBillingGuestEmailRequired;
	protected static String validationBillingGuestEmailInvalid;
	protected static String validationBillingFirstName;
	protected static String validationBillingLastName;
	protected static String validationBillingTelephone;
	protected static String validationBillingPostcode;
	protected static String validationBillingCountry_Id;
	protected static String validationBillingEmail;
	protected static String validationBillingRegion;
	
	protected static String validationMsgBillingGuestEmailRequired;
	protected static String validationMsgBillingGuestEmailInvalid;
	protected static String validationMsgBillingFirstName;
	protected static String validationMsgBillingLastName;
	protected static String validationMsgBillingTelephone;
	protected static String validationMsgBillingPostcode;
	protected static String validationMsgBillingCountry_Id;
	protected static String validationMsgBillingEmail;
	protected static String validationMsgBillingRegion;
	
	protected static String validationMsgShippingFirstName;
	protected static String validationMsgShippingLastName;
	protected static String validationMsgShippingTelephone;
	protected static String validationMsgShippingPostcode;
	protected static String validationMsgShippingCountry_Id;
	protected static String validationShippingRegion ;
	protected static String validationMsgShippingRegion ;
	
	protected static String validationShippingFirstName;
	protected static String validationShippingLastName;
	protected static String validationShippingTelephone;
	protected static String validationShippingPostcode;
	protected static String validationShippingCountry_Id;
	
	protected static String orderReviewContinueButton;
	protected static String moneyOrderContinueButton;
	protected static String errorMsg;
	protected static String billingAddressInReviewPage;
	protected static String shippingAddressInReviewPage;
	protected static String getBillingAddressInReviewPage;
	protected static String getShippingAddressInReviewPage;
	
	
	protected static String orderIdMsgInConfirmationPage;
	protected static String orderIdXpath;
	protected static String loggedUserOrderIdXpath;
	protected static String orderNumber; 
	protected static String continueShoppingButton;
	
	protected static String myAccountLink;
	
	protected static String chkoutAjaxLoader; 
	protected static String ajaxLoader;
	
	
	protected static String chkoutAjaxUserType; 
	protected static String chkoutAjaxBilling; 
	protected static String chkoutAjaxShipping; 
	protected static String chkoutAjaxGiftCard; 
	protected static String chkoutAjaxPayment; 
	protected static String chkoutAjaxClassType; 
	protected static String chkoutAjaxClassValue; 
	protected static String HasCheckoutAjaxRequest ; 
	
	
	protected static String homeProductXpath; 
	protected static String homeProductNameXpath; 
	protected static String categoryProductAddToCartXpath; 
	protected static String ProductDetailsAddToCartXpath; 
	protected static String ProductDetailsEmailToFriendsXpath; 
	protected static String ProductDetailsHeaderCheckoutXpath; 
	
	protected static String TagManagerContentTag;
	
	protected static String homeProductGACode; 
	
	protected static String CategoryProductGACode;
	
	protected static String productDetailAddToCartGACode;
	protected static String productDetailWishlistGACode;
	protected static String productDetailEmailToFriendsGACode;
	protected static String productDetailHeaderCheckoutGACode;
	
	protected static String shoppingProceedToCheckoutGACode;
	protected static String shoppingPaypalCheckoutGACode;
	
	protected static String checkoutAsGuestUserAndHasFunctionInSelectUserType;
	protected static String checkoutAsGuestSelectUserTypeFunctionValue;
	protected static String checkoutAsGuestSelectUserTypeGACode;
	
	protected static String checkoutAsMemberUserAndHasFunctionInSelectUserType;
	protected static String checkoutAsMemberSelectUserTypeFunctionValue;
	protected static String checkoutAsMemberSelectUserTypeGACode;
	
	protected static String checkoutAsCreateUserAndHasFunctionInSelectUserType;
	protected static String checkoutAsCreateSelectUserTypeFunctionValue;
	protected static String checkoutAsCreateSelectUserTypeGACode;
	
	protected static String checkoutSameAddressHasFunction;
	protected static String checkoutSameAddressFunctionValue;
	protected static String checkoutSameAddressGACode;
	
	protected static String checkoutDifferentAddressHasFunction;
	protected static String checkoutDifferentAddressFunctionValue;
	protected static String checkoutDifferentAddressGACode;
	
	protected static String checkoutPickUpInStoreHasFunction;
	protected static String checkoutPickUpInStoreFunctionValue;
	protected static String checkoutPickUpInStoreGACode;
	
	
	protected static String newsLetterHasFunction;
	protected static String newsLetterFunctionValue;
	protected static String newsLetterGACode;
	

	public void getLocation()throws Exception
	{
		searchTextBox = findElementInXLSheet(getParameterXpath,"Search Text Box");
		searchButton = findElementInXLSheet(getParameterXpath,"Search Button");
		mainLogo = findElementInXLSheet(getParameterXpath,"Main Logo");
		mainLogoToolTip = findElementInXLSheet(getParameterXpath,"Main Logo Tool Tip");
		
		//Chkout
		wishListLink = findElementInXLSheet(getParameterXpath,"Wishlist Link");
		wishListTable = findElementInXLSheet(getParameterXpath,"WishList Table");
		copyright = findElementInXLSheet(getParameterXpath,"Copyright xpath");
		loginLink = findElementInXLSheet(getParameterXpath,"login link");
		linkLogOut = findElementInXLSheet(getParameterXpath,"Logout link");
		productGrid = findElementInXLSheet(getParameterXpath,"Results Grid");
		
		leftCategory  = findElementInXLSheet(getParameterXpath,"Left Category Xpath");
		productSizeBlock = findElementInXLSheet(getParameterXpath,"Select Size");
		productSizeClassName = findElementInXLSheet(getParameterXpath,"Product Size Class Name");
		productSizeAttributeValue = findElementInXLSheet(getParameterXpath,"Product Size Attribute Value");
		productSizeclassOutofStockValue = findElementInXLSheet(getParameterXpath,"Product Size class Out of Stock Value");
		
		addToCartButton = findElementInXLSheet(getParameterXpath,"Add to cart button");
		
		ajaxLoader = findElementInXLSheet(getParameterXpath,"Ajax loader");
		
		myCartXpath = findElementInXLSheet(getParameterXpath,"My Cart Xpath");
		viewCartId = findElementInXLSheet(getParameterXpath,"View Cart Id");
		viewCart = findElementInXLSheet(getParameterXpath,"View Cart");
		shoppingCartTable = findElementInXLSheet(getParameterXpath,"Shopping Cart Table");
		rmvItemFromCart = findElementInXLSheet(getParameterXpath,"Remove Item From Shopping Cart");		
		
		productNameXpathInProductDetailPage = findElementInXLSheet(getParameterXpath,"Product Name in product detail page");
		productPriceXpathInProductDetailPage = findElementInXLSheet(getParameterXpath,"Product Price in product detail page");
		outOfStockProductMessage = findElementInXLSheet(getParameterXpath,"Out of stock message");
		
		productQTYInDetailPage = findElementInXLSheet(getCheckoutDetails,"QTY");		
		productQTYXpath  = findElementInXLSheet(getParameterXpath,"QTY");
		updateProductQTY = findElementInXLSheet(getParameterXpath,"QTY Field in Shopping Cart page");
		
		updateButton = findElementInXLSheet(getParameterXpath,"Update Button");
		productPriceXpath = findElementInXLSheet(getParameterXpath,"Product Price in Shopping Cart price table");
		shoppingCartTitle = findElementInXLSheet(getParameterXpath,"Shopping Cart Page Title");
		productPriceInShoppingCart = findElementInXLSheet(getParameterXpath,"Product Price in Shopping Cart page");
		productNameInShoppingCart = findElementInXLSheet(getParameterXpath,"Product Name in Shopping Cart page");
		productQTYInShoppingCart = findElementInXLSheet(getParameterXpath,"QTY in Shopping Cart page");
		proceedToCheckoutButtonInShoppingCart = findElementInXLSheet(getParameterXpath,"Proceed to Checkout button");
		PaypalCheckoutButtonInShoppingCart = findElementInXLSheet(getParameterXpath,"Paypal Express Checkout button");
		
		getProductAttributes = findElementInXLSheet(getParameterXpath,"Product attributes");
		getProductName = findElementInXLSheet(getParameterXpath,"Product Name in Category page");
		getProductPrice = findElementInXLSheet(getParameterXpath,"Product Price in Category page"); 

		validationBillingGuestEmailRequired = findElementInXLSheet(getParameterXpath,"Msg Billing Login Email Required");		
		validationBillingGuestEmailInvalid = findElementInXLSheet(getParameterXpath,"Msg Billing Login Email Invalid");	
		validationMsgBillingGuestEmailRequired = findElementInXLSheet(getMessages,"Msg Billing Login Email Required");		
		validationMsgBillingGuestEmailInvalid = findElementInXLSheet(getMessages,"Msg Billing Login Email Invalid");	
		
		
		//Billing Information
		
		chkoutAjaxLoader = findElementInXLSheet(getParameterXpath,"Checkout ajax loader");
		
		txtBillingLoginEmail = findElementInXLSheet(getParameterXpath,"txtBillingLoginEmail");
		
		btnCheckoutAsGuestUser = findElementInXLSheet(getParameterXpath,"Checkout as guest user continue");

		txtBillingEmail = findElementInXLSheet(getParameterXpath,"txtBillingEmail");

		selectNewAddress_DropDown = findElementInXLSheet(getParameterXpath,"Member-Select New Billing Address");
		
		txtBillingFirstName = findElementInXLSheet(getParameterXpath,"txtBillingFirstName");
		txtBillingLastName = findElementInXLSheet(getParameterXpath,"txtBillingLastName");
		txtBillingTelephone = findElementInXLSheet(getParameterXpath,"txtBillingTelephone");
		txtBillingstreet1 = findElementInXLSheet(getParameterXpath,"txtBillingstreet1");
		txtBillingCity = findElementInXLSheet(getParameterXpath,"txtBillingCity");
		selectBillingRegion_Id = findElementInXLSheet(getParameterXpath,"selectBillingRegion_Id");
		
		selectBillingRegion_Id_DropDown = findElementInXLSheet(getParameterXpath,"selectBillingRegion_Id_DropDown");
		selectMemberBillingRegion_Id_DropDown = findElementInXLSheet(getParameterXpath,"selectMemberBillingRegion_Id_DropDown");
		txtBillingPostcode = findElementInXLSheet(getParameterXpath,"txtBillingPostcode");
		selectBillingCountry_Id = findElementInXLSheet(getParameterXpath,"selectBillingCountry_id");
		
		billingCreateAccount = findElementInXLSheet(getParameterXpath,"Create an account during checkout");
		
		chkCheckoutAsGuestUser = findElementInXLSheet(getParameterXpath,"Checkout as guest user");
		btnBillingInfoContinue = findElementInXLSheet(getParameterXpath,"Billing info continue button");
		
		linkaddressHint = findElementInXLSheet(getParameterXpath,"linkAddressHint");
		
		
		//Shipping Address
		
		chkDifferentShipAddress = findElementInXLSheet(getParameterXpath,"Chk Different Ship Address");		
		txtShippingFirstName = findElementInXLSheet(getParameterXpath,"txtShippingFirstName");
		txtShippingLastName = findElementInXLSheet(getParameterXpath,"txtShippingLastName");
		txtShippingTelephone = findElementInXLSheet(getParameterXpath,"txtShippingTelephone");
		txtShippingstreet1 = findElementInXLSheet(getParameterXpath,"txtShippingstreet1");
		txtShippingCity = findElementInXLSheet(getParameterXpath,"txtShippingCity");
		selectShippingRegion_Id = findElementInXLSheet(getParameterXpath,"selectShippingRegion_Id");
		txtShippingPostcode = findElementInXLSheet(getParameterXpath,"txtShippingPostcode");
		selectShippingCountry_Id = findElementInXLSheet(getParameterXpath,"selectShippingCountry_id");
		selectShippingRegion_Id_DropDown = findElementInXLSheet(getParameterXpath,"selectShippingRegion_Id_DropDown");
		selectMemberShippingRegion_Id_DropDown = findElementInXLSheet(getParameterXpath,"selectMemberShippingRegion_Id_DropDown");
		
		btnShippingInfoContinue =  findElementInXLSheet(getParameterXpath,"Shipping info continue button");
				
		getTotalShippingMethods = findElementInXLSheet(getParameterXpath,"Total shipping method");
		getShippingMethodTag = findElementInXLSheet(getParameterXpath,"Shipping method tag");
		selectShippingMethod = findElementInXLSheet(getParameterXpath,"Select shipping method");
		btnShippingMethodContinue = findElementInXLSheet(getParameterXpath,"Shipping method continue button");
		
		selectCCType 		 = findElementInXLSheet(getParameterXpath,"Credit Card Type");
		cCType 		 = findElementInXLSheet(getCheckoutDetails,"Credit Card Type");
		
		
		//Credit Card
		
		selectCCTypeDropDown 		 = findElementInXLSheet(getParameterXpath,"Credit Card Type Drop Down");
		selectCCYearDropDown		 = findElementInXLSheet(getParameterXpath,"Expiration Year Drop Down");
		selectCCMonthDropDown 		 = findElementInXLSheet(getParameterXpath,"Expiration Month Drop Down");
		
		
		chkPaymentIsCC = findElementInXLSheet(getParameterXpath,"Payment Type Is CC");
		chkPaymentIsMoneyOrder = findElementInXLSheet(getParameterXpath,"Payment Type Is MoneyOrder");
		chkPaymentIsPaypal = findElementInXLSheet(getParameterXpath,"Payment Type Is Paypal");
		
		if(findElementInXLSheet(getParameterXpath,"Credit Card Name") != null || findElementInXLSheet(getCheckoutDetails,"Credit Card Name") != null)
		{
			txtCCName = findElementInXLSheet(getParameterXpath,"Credit Card Name");
			cCName = findElementInXLSheet(getCheckoutDetails,"Credit Card Name");
			
		}
		if(findElementInXLSheet(getParameterXpath,"Credit Card Number") != null || findElementInXLSheet(getCheckoutDetails,"Credit Card Number") != null)
		{
			txtCCNumber = findElementInXLSheet(getParameterXpath,"Credit Card Number");
			cCNumber = findElementInXLSheet(getCheckoutDetails,"Credit Card Number");
		}
		if(findElementInXLSheet(getParameterXpath,"Expiration Year") != null || findElementInXLSheet(getCheckoutDetails,"Expiration Year") != null)
		{
			selectCCYear = findElementInXLSheet(getParameterXpath,"Expiration Year");
			cCYear = findElementInXLSheet(getCheckoutDetails,"Expiration Year");
		}
		if(findElementInXLSheet(getParameterXpath,"Card Verification Number") != null || findElementInXLSheet(getCheckoutDetails,"Card Verification Number") != null)
		{
			txtCCCvs = findElementInXLSheet(getParameterXpath,"Card Verification Number");
			cCCvs = findElementInXLSheet(getCheckoutDetails,"Card Verification Number");
		}
		
		if(findElementInXLSheet(getParameterXpath,"Expiration Month") != null || findElementInXLSheet(getCheckoutDetails,"Expiration Month") != null)
		{
			selectCCMonth = findElementInXLSheet(getParameterXpath,"Expiration Month");
			cCMonth = findElementInXLSheet(getCheckoutDetails,"Expiration Month");
		}
		
		chkTeamsAndCondtion = findElementInXLSheet(getParameterXpath,"Payment Teams And Condition");

		
		btnContinuePaymentScreen = findElementInXLSheet(getParameterXpath,"Payment continue button");
		
		
		txtPaypalEmail = findElementInXLSheet(getParameterXpath,"Paypal Email Address");
		txtPaypalPassword = findElementInXLSheet(getParameterXpath,"Paypal Password");
		btnPaypalSubmit = findElementInXLSheet(getParameterXpath,"Paypal Submit Button");
		btnPaypalContinue = findElementInXLSheet(getParameterXpath,"Paypal Continue Button");
		
		paypalEmail = findElementInXLSheet(getCheckoutDetails,"Paypal Email Address");
		paypalPassword = findElementInXLSheet(getCheckoutDetails,"Paypal Password");
		//Order Review		
		
		if(findElementInXLSheet(getParameterXpath,"Product Name in Order Review page") != null || findElementInXLSheet(getParameterXpath,"Product Price in Order Review page") != null)
		{
			productNameXpathOrderReviewPage = findElementInXLSheet(getParameterXpath,"Product Name in Order Review page");
			productPriceXpathInOrderReviewPage = findElementInXLSheet(getParameterXpath,"Product Price in Order Review page");

			
		}
		
		if(findElementInXLSheet(getParameterXpath,"Order Review Continue Button") != null)
		{
			orderReviewContinueButton = findElementInXLSheet(getParameterXpath,"Order Review Continue Button");
		}
		if(findElementInXLSheet(getParameterXpath,"ErrorMsg") != null)
		{
			errorMsg = findElementInXLSheet(getParameterXpath,"ErrorMsg");
		}
		
		moneyOrderContinueButton = findElementInXLSheet(getParameterXpath,"Money Order Continue Button");
		
		billingAddressInReviewPage = findElementInXLSheet(getParameterXpath,"Billing Address In order Review");
		
		shippingAddressInReviewPage = findElementInXLSheet(getParameterXpath,"Shipping Address In order Review");
		
		productNameXpathPaypalReviewPage = findElementInXLSheet(getParameterXpath,"Product Name in Paypal Review page");
		productPriceXpathInPaypalReviewPage = findElementInXLSheet(getParameterXpath,"Product Price in Paypal Review page");
		btnsubmitPaypalReviewPage = findElementInXLSheet(getParameterXpath,"Paypal Review Continue Button");
		
		//Confirmation Page
		
		orderIdMsgInConfirmationPage = findElementInXLSheet(getParameterXpath,"Order Id Msg in Confirmation page");
		orderIdXpath = findElementInXLSheet(getParameterXpath,"Order Id Xpath");
		loggedUserOrderIdXpath = findElementInXLSheet(getParameterXpath,"Logged User Order Id Xpath");
		continueShoppingButton = findElementInXLSheet(getParameterXpath,"Continue Shopping button");
		
		myAccountLink = findElementInXLSheet(getParameterXpath,"My Account Link");
		
	
		billingAddressInReviewPage = findElementInXLSheet(getParameterXpath,"Billing Address In order Review");
		
		
		//Ajax
		
		chkoutAjaxUserType = findElementInXLSheet(getParameterXpath,"User type ajax request");
		chkoutAjaxBilling = findElementInXLSheet(getParameterXpath,"Billing infor ajax request");
		chkoutAjaxShipping = findElementInXLSheet(getParameterXpath,"Shopping info ajax request");
		chkoutAjaxGiftCard = findElementInXLSheet(getParameterXpath,"Gift card info ajax request"); 
		chkoutAjaxPayment = findElementInXLSheet(getParameterXpath,"Payament infor ajax request");
		chkoutAjaxClassType = findElementInXLSheet(getParameterXpath,"Checkout ajax request calss style type");
		chkoutAjaxClassValue = findElementInXLSheet(getParameterXpath,"Checkout ajax request calss style value");
		HasCheckoutAjaxRequest = findElementInXLSheet(getMethodIsExecuted,"Has checkout ajax request");
				
	}
	
	public void getDataFromXLSheet()throws Exception
	{

		siteTitle = findElementInXLSheet(getParameters,"Site Title");
		copyrightMessage = findElementInXLSheet(getParameters,"Copyright Message");
		loginPassword = findElementInXLSheet(getAccountDetails,"Password");
		loginEmailAddress = findElementInXLSheet(getAccountDetails,"Email Address");		
		forgotPw = findElementInXLSheet(getAccountDetails,"Forgot Password");
				
		validationBillingGuestEmailRequired = findElementInXLSheet(getMessages,"Msg Billing Login Email Required");	
		validationBillingGuestEmailInvalid = findElementInXLSheet(getMessages,"Msg Billing Login Email Invalid");	
		
		billingLoginEmail = findElementInXLSheet(getCheckoutDetails,"Billing Login Email");
		billingFirstName = findElementInXLSheet(getCheckoutDetails,"Billing First Name");
		billingLastName = findElementInXLSheet(getCheckoutDetails,"Billing Last Name");
		billingTelephone = findElementInXLSheet(getCheckoutDetails,"Billing Telephone");
		billingstreet1 = findElementInXLSheet(getCheckoutDetails,"Billing street1");
		billingCity = findElementInXLSheet(getCheckoutDetails,"Billing City");
		billingRegion_Id = findElementInXLSheet(getCheckoutDetails,"Billing Region_Id");
		billingPostcode = findElementInXLSheet(getCheckoutDetails,"Billing Postcode");
		billingCountry_Id = findElementInXLSheet(getCheckoutDetails,"Billing Country");		
		
		validationMsgBillingFirstName = findElementInXLSheet(getMessages,"Msg Billing First Name");
		validationMsgBillingLastName = findElementInXLSheet(getMessages,"Msg Billing Last Name");
		validationMsgBillingTelephone = findElementInXLSheet(getMessages,"Msg Billing Telephone");
		validationMsgBillingPostcode = findElementInXLSheet(getMessages,"Msg Billing Postcode");
		validationMsgBillingCountry_Id = findElementInXLSheet(getMessages,"Msg Billing Country");
		
		validationBillingFirstName = findElementInXLSheet(getParameterXpath,"Msg Billing First Name");
		validationBillingLastName = findElementInXLSheet(getParameterXpath,"Msg Billing Last Name");
		validationBillingTelephone = findElementInXLSheet(getParameterXpath,"Msg Billing Telephone");
		validationBillingPostcode = findElementInXLSheet(getParameterXpath,"Msg Billing Postcode");
		validationBillingCountry_Id = findElementInXLSheet(getParameterXpath,"Msg Billing Country");

		validationMsgBillingEmail = findElementInXLSheet(getMessages,"Msg Billing Email");
		validationBillingEmail = findElementInXLSheet(getParameterXpath,"Msg Billing Email");


		shippingFirstName = findElementInXLSheet(getCheckoutDetails,"Shipping First Name");
		shippingLastName = findElementInXLSheet(getCheckoutDetails,"Shipping Last Name");
		shippingTelephone = findElementInXLSheet(getCheckoutDetails,"Shipping Telephone");
		shippingstreet1 = findElementInXLSheet(getCheckoutDetails,"Shipping street1");
		shippingCity = findElementInXLSheet(getCheckoutDetails,"Shipping City");
		shippingRegion_Id = findElementInXLSheet(getCheckoutDetails,"Shipping Region_Id");
		shippingPostcode = findElementInXLSheet(getCheckoutDetails,"Shipping Postcode");
		shippingCountry_Id = findElementInXLSheet(getCheckoutDetails,"Shipping Country");		
		
		validationShippingRegion = findElementInXLSheet(getParameterXpath,"Msg Shipping Region");
		validationMsgShippingRegion = findElementInXLSheet(getMessages,"Msg Shipping Region");
		
		validationMsgShippingFirstName = findElementInXLSheet(getCheckoutDetails,"Msg Shipping First Name");
		validationMsgShippingLastName = findElementInXLSheet(getCheckoutDetails,"Msg Shipping Last Name");
		validationMsgShippingTelephone = findElementInXLSheet(getCheckoutDetails,"Msg Shipping Telephone");
		validationMsgShippingPostcode = findElementInXLSheet(getCheckoutDetails,"Msg Shipping Postcode");
		validationMsgShippingCountry_Id = findElementInXLSheet(getCheckoutDetails,"Msg Shipping Country");
		
		validationShippingFirstName = findElementInXLSheet(getParameterXpath,"Msg Shipping First Name");
		validationShippingLastName = findElementInXLSheet(getParameterXpath,"Msg Shipping Last Name");
		validationShippingTelephone = findElementInXLSheet(getParameterXpath,"Msg Shipping Telephone");
		validationShippingPostcode = findElementInXLSheet(getParameterXpath,"Msg Shipping Postcode");
		validationShippingCountry_Id = findElementInXLSheet(getParameterXpath,"Msg Shipping Country");
		
		
	}
	
	
	public void getSeoData()throws Exception
	{
		TagManagerContentTag = findElementInXLSheet(getParameterXpath,"Tag manager content tag");
		
		homeProductXpath = findElementInXLSheet(getParameterXpath,"Home Page Product Xpath");
		homeProductGACode = findElementInXLSheet(getParameterXpath,"Home Page Product GA code");
		homeProductNameXpath = findElementInXLSheet(getParameterXpath,"Home Page Product Name Xpath");
		ProductDetailsAddToCartXpath = findElementInXLSheet(getParameterXpath,"Product Details Add To Cart Xpath");
		ProductDetailsEmailToFriendsXpath = findElementInXLSheet(getParameterXpath,"Product Details Email To Friends Xpath");
		ProductDetailsHeaderCheckoutXpath = findElementInXLSheet(getParameterXpath,"Product Details Header Checkout Xpath");
		
		categoryProductAddToCartXpath = findElementInXLSheet(getParameterXpath,"Category Page Product Add To Cart Xpath");		
		CategoryProductGACode = findElementInXLSheet(getParameterXpath,"Category Product GA Code");	
		productDetailAddToCartGACode = findElementInXLSheet(getParameterXpath,"Product Details Add To Cart GA Code");	
		productDetailWishlistGACode = findElementInXLSheet(getParameterXpath,"Product Details Wishlist GA Code");	
		productDetailEmailToFriendsGACode = findElementInXLSheet(getParameterXpath,"Product Details Email To Friends GA Code");
		productDetailHeaderCheckoutGACode = findElementInXLSheet(getParameterXpath,"Product Details Header Checkout GA Code");
		
		shoppingProceedToCheckoutGACode = findElementInXLSheet(getParameterXpath,"Shopping Cart Proceed To Checkout Button GA Code");
		shoppingPaypalCheckoutGACode = findElementInXLSheet(getParameterXpath,"Shopping Cart Paypal Checkout Button GA Code");
		
		checkoutAsGuestUserAndHasFunctionInSelectUserType = findElementInXLSheet(getParameterXpath,"Checkout as guest user and has function in select user type");
		checkoutAsGuestSelectUserTypeFunctionValue = findElementInXLSheet(getParameterXpath,"Checkout as guest - select user type function value");
		checkoutAsGuestSelectUserTypeGACode = findElementInXLSheet(getParameterXpath,"Checkout as guest - select user type GA Code");
		
		checkoutAsMemberUserAndHasFunctionInSelectUserType = findElementInXLSheet(getParameterXpath,"Checkout as member user and has function in select user type continue button");
		checkoutAsMemberSelectUserTypeFunctionValue = findElementInXLSheet(getParameterXpath,"Checkout as member - select user type function value");
		checkoutAsMemberSelectUserTypeGACode = findElementInXLSheet(getParameterXpath,"Checkout as member - select user type GA Code");
		
		checkoutAsCreateUserAndHasFunctionInSelectUserType = findElementInXLSheet(getParameterXpath,"Checkout as create user and has function in select user type continue button");
		checkoutAsCreateSelectUserTypeFunctionValue = findElementInXLSheet(getParameterXpath,"Checkout as create user - select user type function value");
		checkoutAsCreateSelectUserTypeGACode = findElementInXLSheet(getParameterXpath,"Checkout as create user - select user type GA Code");
		
		checkoutSameAddressHasFunction = findElementInXLSheet(getParameterXpath,"Checkout as guest and has function in select deliver to same address");
		checkoutSameAddressFunctionValue = findElementInXLSheet(getParameterXpath,"Checkout as guest - select deliver to same address function value");
		checkoutSameAddressGACode = findElementInXLSheet(getParameterXpath,"Checkout as guest - select deliver to same address type GA Code");
		
		checkoutDifferentAddressHasFunction = findElementInXLSheet(getParameterXpath,"Checkout as guest and has function in select deliver to different address");
		checkoutDifferentAddressFunctionValue = findElementInXLSheet(getParameterXpath,"Checkout as guest - select deliver to different address function value");
		checkoutDifferentAddressGACode = findElementInXLSheet(getParameterXpath,"Checkout as guest - select deliver to different address type GA Code");
		
		checkoutPickUpInStoreHasFunction = findElementInXLSheet(getParameterXpath,"Checkout as guest and has function in select deliver to store");
		checkoutPickUpInStoreFunctionValue = findElementInXLSheet(getParameterXpath,"Checkout as guest - select deliver to store function value");
		checkoutPickUpInStoreGACode = findElementInXLSheet(getParameterXpath,"Checkout as guest - select deliver to store GA Code");
		
		newsLetterHasFunction = findElementInXLSheet(getParameterXpath,"Checkout as guest and has function in select deliver to store");
		newsLetterFunctionValue = findElementInXLSheet(getParameterXpath,"Checkout as guest - select deliver to store function value");
		newsLetterGACode = findElementInXLSheet(getParameterXpath,"Checkout as guest - select deliver to store GA Code");
		
		
		
	}


	
		
}
