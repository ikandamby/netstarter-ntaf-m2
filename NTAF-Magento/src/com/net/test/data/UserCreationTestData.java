package com.net.test.data;

import com.net.test.util.TestBase;

public class UserCreationTestData extends TestBase
{
	//property file - user creation
	
	protected static String registerLink;
	protected static String loginLink;
	protected static String myAccountLinkXpath;
	protected static String registerURL;
	protected static String registerPageTitle;
	public static String txtFirstName;
	public static String txtLastName;
	public static String txtEmail1;
	protected static String txtEmail2;
	protected static String txtPassword;
	protected static String txtConfirmPassword;
	protected static String isAgree;
	protected static String isMale;
	protected static String isFemale;
	protected static String txtDOB;
	
	protected static String btnUserCreation;
	protected static String logoutlink;
	protected static String txtBirthdayDay;
	protected static String txtBirthdayMonth;
	protected static String txtBirthdayYear;
	
	protected static String selectPostCode_DropDown;
	protected static String txtUserCreationPostcode;
	protected static String txtstor;
	protected static String userCreationPostcode;
	protected static String userCreationstor;
	protected static String txtTelephone;
	protected static String telephone;
	protected static String txtStreet;
	protected static String street;
	protected static String linkLogout ;
	protected static String txtPostcodeChangeStore ;
	
	protected static String myAccountWelcomeMsgxpath;
	
	//User creation - validation msg xpath
	protected static String registerWithoutFirstName;
	protected static String registerWithoutLastName;
	protected static String registerWithoutEmail;
	protected static String registerWithoutConfirmEmail;
	protected static String registerWithoutPassword;
	protected static String registerWithoutConfirmPassword;
	protected static String registerLessPassword;
	protected static String registerLessConfirmPassword;
	protected static String registerMissMatchPassword;
	protected static String registerInvalidEmail;
	protected static String registerInvalidConfirmEmail;
	protected static String registerAlreadyRegistedUser;
	protected static String registerMissmatchEmail;
	protected static String registerMissmatchConfirmEmail;
	protected static String registerWithoutPostcode;
	protected static String registerInvalidBirthday;
	protected static String registerWithoutTelephone;
	protected static String registerInvalidTelephone;
	protected static String registeWitoutPostcode;
	protected static String registeWitoutStreet;
	protected static String registeWitoutTerms;
	
	
	protected static String linkTermsAndConditions ;
	protected static String linkPrivacyPolicy ;
	protected static String registerWithoutDOB;
	
	
	//User creation - validation msg 
	
	protected static String registerMsgWithoutFirstName;
	protected static String registerMsgWithoutLastName;
	protected static String registerMsgWithoutEmail;
	protected static String registerMsgWithoutConfirmEmail;
	protected static String registerMsgWithoutPassword;
	protected static String registerMsgWithoutConfirmPassword;
	protected static String registerMsgLessPassword;
	protected static String registerMsgLessConfirmPassword;
	protected static String registerMsgMissMatchPassword;
	protected static String registerMsgInvalidEmail;
	protected static String registerMsgInvalidConfirmEmail;
	protected static String registerMsgAlreadyRegistedUser;
	protected static String registerMsgMissmatchEmail;
	protected static String registerMsgMissmatchConfirmEmail;
	protected static String registerMsgInvalidBirthday;
	protected static String registerMsgWithoutTelephone;
	protected static String registerMsgInvalidTelephone;
	protected static String registeMsgWitoutPostcode;
	protected static String registeMsgWitoutStreet;
	protected static String registeMsgWitoutTerms;
	protected static String titleTermsAndConditions;
	protected static String titlePrivacyPolicy ;
	protected static String registerMsgWithoutDOB ;
	protected static String registerMsgWithoutPostcode;
	
	//Data file - user creation
	public static String userCreationFirstName;
	public static String userCreationLastName;
	protected static String userCreationEmailAddress;
	protected static String userCreationConfirmEmailAddress;
	protected static String userCreationPassword;
	protected static String userCreationConfirmPassword;
	protected static String userCreationThankYouMessage;
	protected static String userCreationExistsEmailAddress;
	protected static String userCreationGender;
	protected static String userCreationGenderDropdown;
	public static String userCreationDobDayDropdown;
	public static String userCreationDobMonthDropdown;
	public static String userCreationDobYearDropdown; 
	public static String userCreationBirthdayDay;
	public static String userCreationBirthdayMonth;
	public static String userCreationBirthdayYear;
	protected static String userCreationAlreadyExixtsUser;
	protected static String userCreationDOB;
	
	public static String userCreationMailSubject;
	public static String myAccWlcomeMsg;
	

	public void getLocation()throws Exception
	{	
		myAccWlcomeMsg = findElementInXLSheet(getParameterXpath,"wellcome massage");
		registerLink = findElementInXLSheet(getParameterXpath,"register link");
		registerURL = findElementInXLSheet(getParameterXpath,"register URL");
		registerPageTitle = findElementInXLSheet(getParameterXpath,"register page title");	
		linkLogout = findElementInXLSheet(getParameterXpath,"logout link");
		myAccountWelcomeMsgxpath = findElementInXLSheet(getParameterXpath,"thank you message after registerd");
		loginLink = findElementInXLSheet(getParameterXpath,"login link");
		myAccountLinkXpath = findElementInXLSheet(getParameterXpath,"user cration link under my account link");
		txtFirstName = findElementInXLSheet(getParameterXpath,"txtFirstName");
		txtLastName = findElementInXLSheet(getParameterXpath,"txtLastName");
		txtEmail1 = findElementInXLSheet(getParameterXpath,"txtEmail1");
		txtEmail2 = findElementInXLSheet(getParameterXpath,"txtEmail2");
		txtPassword = findElementInXLSheet(getParameterXpath,"txtPasswordUser");
		txtConfirmPassword = findElementInXLSheet(getParameterXpath,"txtConfirmPasswordUser");
		isAgree = findElementInXLSheet(getParameterXpath,"is agree");
		isMale = findElementInXLSheet(getParameterXpath,"gender_male_xpath");
		userCreationGenderDropdown = findElementInXLSheet(getParameterXpath,"gender dropdown value");
		isFemale = findElementInXLSheet(getParameterXpath,"gender_female_xpath");
		
		txtPostcodeChangeStore = findElementInXLSheet(getParameterXpath,"postcode change Store");
		
		txtBirthdayDay = findElementInXLSheet(getParameterXpath,"txtBirthday-day");
		txtBirthdayMonth = findElementInXLSheet(getParameterXpath,"txtBirthday-month");
		txtBirthdayYear = findElementInXLSheet(getParameterXpath,"txtBirthday-year");	
		txtDOB = findElementInXLSheet(getParameterXpath,"txtDOB");
		
		userCreationDobDayDropdown = findElementInXLSheet(getParameterXpath,"dob-day dropdown");
		userCreationDobMonthDropdown = findElementInXLSheet(getParameterXpath,"dob-month dropdown");
		userCreationDobYearDropdown = findElementInXLSheet(getParameterXpath,"dob-year dropdown");
		
		selectPostCode_DropDown = findElementInXLSheet(getParameterXpath,"select user creation post dropdown");	
		txtUserCreationPostcode = findElementInXLSheet(getParameterXpath,"txt user cration postcode");
		txtstor = findElementInXLSheet(getParameterXpath,"txt user cration stor");
		txtTelephone = findElementInXLSheet(getParameterXpath,"txt telephone");	
		txtStreet = findElementInXLSheet(getParameterXpath,"txt street");	
		
		btnUserCreation = findElementInXLSheet(getParameterXpath,"user create button");
		registerWithoutFirstName = findElementInXLSheet(getParameterXpath,"user creation without first name");
		registerWithoutLastName = findElementInXLSheet(getParameterXpath,"user creation without last name");
		registerWithoutEmail = findElementInXLSheet(getParameterXpath,"user creation without email");
		registerWithoutConfirmEmail = findElementInXLSheet(getParameterXpath,"user creation without confirm email");
		registerWithoutConfirmPassword = findElementInXLSheet(getParameterXpath,"user creation without confirm pw");
		registerLessPassword = findElementInXLSheet(getParameterXpath,"user creation less pw");
		registerLessConfirmPassword = findElementInXLSheet(getParameterXpath,"user creation less confirm");
		registerMissMatchPassword = findElementInXLSheet(getParameterXpath,"user creation mismatch pw");
		registerMissmatchEmail = findElementInXLSheet(getParameterXpath,"user creation mismatch email");
		registerInvalidEmail = findElementInXLSheet(getParameterXpath,"user creation invalid email");
		registerMissmatchConfirmEmail = findElementInXLSheet(getParameterXpath,"user creation invalid confirm email");
		registeMsgWitoutPostcode = findElementInXLSheet(getParameterXpath,"user creation without postcode");
		registerInvalidBirthday = findElementInXLSheet(getParameterXpath,"user creation invalid Birthday");
		registerAlreadyRegistedUser = findElementInXLSheet(getParameterXpath,"already registered member");		
		
		registerWithoutTelephone = findElementInXLSheet(getParameterXpath,"user creation witout telephone");
		registerInvalidTelephone = findElementInXLSheet(getParameterXpath,"user creation invalid telephone");
		registeWitoutPostcode = findElementInXLSheet(getParameterXpath,"user creation without postcode");
		registeWitoutStreet = findElementInXLSheet(getParameterXpath,"user creation without street");
		registeWitoutTerms = findElementInXLSheet(getParameterXpath,"user creation without street");
		
		linkPrivacyPolicy = findElementInXLSheet(getParameterXpath,"privacy policy");
		linkTermsAndConditions = findElementInXLSheet(getParameterXpath,"terms and conditions");			
		registerWithoutDOB = findElementInXLSheet(getParameterXpath,"user creation without DOB");
		userCreationMailSubject = findElementInXLSheet(getParameterXpath,"user creation mail subject");
		
		registerMsgWithoutFirstName = findElementInXLSheet(getMessages,"user creation without first name");
		registerMsgWithoutLastName = findElementInXLSheet(getMessages,"user creation without last name");
		registerMsgWithoutEmail = findElementInXLSheet(getMessages,"user creation without email");
		registerMsgWithoutConfirmEmail = findElementInXLSheet(getMessages,"user creation without confirm email");
		registerWithoutPassword = findElementInXLSheet(getParameterXpath,"user creation without pw");
		registerMsgWithoutConfirmPassword = findElementInXLSheet(getMessages,"user creation without confirm pw");
		registerMsgLessPassword = findElementInXLSheet(getMessages,"user creation less pw");
		registerMsgLessConfirmPassword = findElementInXLSheet(getMessages,"user creation less confirm");
		registerMsgMissMatchPassword = findElementInXLSheet(getMessages,"user creation mismatch pw");
		registerMsgMissmatchEmail = findElementInXLSheet(getMessages,"user creation mismatch email");			
		registerMsgInvalidEmail = findElementInXLSheet(getMessages,"user creation invalid email");
		registerMsgInvalidConfirmEmail = findElementInXLSheet(getMessages,"user creation invalid confirm email");
		registerMsgInvalidBirthday = findElementInXLSheet(getMessages,"user creation invalid Birthday");
		registerMsgAlreadyRegistedUser = findElementInXLSheet(getMessages,"already registered member");
		registerMsgWithoutPassword = findElementInXLSheet(getMessages,"user creation without pw");
		registerMsgWithoutPostcode = findElementInXLSheet(getMessages,"user creation without postcode");
		
		registerMsgWithoutTelephone = findElementInXLSheet(getMessages,"user creation witout telephone");
		registerMsgInvalidTelephone = findElementInXLSheet(getMessages,"user creation invalid telephone");		
		registeMsgWitoutPostcode = findElementInXLSheet(getMessages,"user creation without postcode");
		registeMsgWitoutStreet = findElementInXLSheet(getMessages,"user creation without street");
		registeMsgWitoutTerms = findElementInXLSheet(getMessages,"user creation without terms");
		registerMsgWithoutDOB = findElementInXLSheet(getMessages,"user creation without DOB");
		
	}
	
		
	public void getDataFromXLSheet()throws Exception
	{
		userCreationFirstName  = findElementInXLSheet(getUserDetails,"First Name");
		userCreationLastName  = findElementInXLSheet(getUserDetails,"Last Name");
		userCreationEmailAddress = findElementInXLSheet(getUserDetails,"Email Address");
		userCreationConfirmEmailAddress = findElementInXLSheet(getUserDetails,"Confirm Email Address");
		userCreationConfirmPassword = findElementInXLSheet(getUserDetails,"Confirm Password");			
		userCreationGender = findElementInXLSheet(getUserDetails,"Gender");
		userCreationPassword = findElementInXLSheet(getUserDetails,"Password");			
		userCreationAlreadyExixtsUser = findElementInXLSheet(getUserDetails,"Exists Email Address");
		userCreationBirthdayDay = findElementInXLSheet(getUserDetails,"Birthday-Day");
		userCreationBirthdayMonth = findElementInXLSheet(getUserDetails,"Birthday-Month");
		userCreationBirthdayYear = findElementInXLSheet(getUserDetails,"Birthday-Year");
		userCreationDOB  = findElementInXLSheet(getUserDetails,"Date Of Birth");
		userCreationPostcode = findElementInXLSheet(getUserDetails,"Post code");
		selectPostCode_DropDown = findElementInXLSheet(getParameterXpath,"select user creation post dropdown");	
		userCreationstor = findElementInXLSheet(getParameterXpath,"txt user cration stor");
		telephone = findElementInXLSheet(getUserDetails,"Telephone");
		street = findElementInXLSheet(getUserDetails,"Street");
		titlePrivacyPolicy = findElementInXLSheet(getUserDetails,"privacy policy title");
		titleTermsAndConditions = findElementInXLSheet(getUserDetails,"terms and conditions title");
		txtPostcodeChangeStore = findElementInXLSheet(getParameterXpath,"postcode change Store");
		
		
		
			
	}

		
}
