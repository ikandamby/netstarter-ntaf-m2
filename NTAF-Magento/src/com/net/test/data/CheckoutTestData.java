package com.net.test.data;

import com.net.test.util.TestBase;

public class CheckoutTestData extends TestBase
{
	//Get location(xpath)
	public static String productGrid;
	public static String mainBrandName;
	public static String leftCategory;
	public static String addToCartButton;
	public static String SelectProductSize;
	
	public static String HasAddToCartAjaxRequest;
	public static String addToCartAjax; 
	public static String addToCartAjaxClassType; 
	public static String addToCartAjaxClassValue;
	
	public static String noOfProductsPerColoum;
	public static String categoryViewXpath;
	
	public static String outOfStockProductMessage;	
	public static String productSizeBlock;
	public static String productSizeClassName;
	public static String productSizeAttributeValue;
	public static String productSizeclassOutofStockValue;	
	
	//user can enter product qty
	protected static String simpleProductQtyTextBox;
	protected static String getDefaultQtyAttribute;
	protected static String groupProductQtyTextBox;
	protected static String configProductQtyTextBox;

	public static String productSizeDropdownXpath;
	public static String productDropdownOptionsValue;
	public static String withoutSelectingSizeErrorXpath;
	
	public static String productColorDropdownXpath;
	public static String withoutSelectingColorErrorXpath;
	
	public static String groupProductQty;
	public static String groupProductPrice;
	public static String groupProductName;
	public static String groupProductTable;
	
	public static String getProductAttributes ;
	public static String getProductName;
	public static String getProductPrice ;
	public static String hasSpecialPriceTagInCategoryPage;
	public static String tagNameForSpecialPrice;
	public static String hasOriginalPriceTagInCategoryPage;
	public static String hasSpecialPriceTagInDetailsPage ;
	public static String hasPriceTagInDetailsPage ;
	public static String setProductName ;
	
	/**
	 * This variables are use for gift card
	 */
	
	public static String giftCardSenderName;
	public static String giftCardSenderEmail;
	public static String giftCardRecipentName;
	public static String giftCardRecipentEmail;	
	
	public static String giftCardSenderNameXpath;
	public static String giftCardSenderEmailXpath;
	public static String giftCardRecipentNameXpath;
	public static String giftCardRecipentEmailXpath;
	public static String giftCardAmountDropdownXpath;
	public static String giftCardAmountTxtXpath;
	
	public static String giftCardWithoutSenderName;
	public static String giftCardWithoutSenderEmail;
	public static String giftCardWithoutRecipentName;
	public static String giftCardWithoutRecipentEmail;
	public static String giftCardWithoutDropdownAmount;
	public static String giftCardWithoutTxtAmount;
	
	public static String validationMsgGiftCardWithoutSenderName;
	public static String validationMsgGiftCardWithoutSenderEmail;
	public static String validationMsgGiftCardWithoutRecipentName;
	public static String validationMsgGiftCardWithoutRecipentEmail;
	public static String validationMsgGiftCardWithoutAmount;
	
	public static String myCartXpath;
	public static String viewCartId;
	public static String viewCart;
	public static String shoppingCartTable;
	public static String rmvItemFromCart;
	
	protected static String setQtyInDetailPage;
	public static String productQTYXpath;
	protected static String updateProductQTY;
	protected static String updateButton;
	public static String shoppingCartTitle;
	public static String productPriceInShoppingCartForGuest;
	public static String productPriceInShoppingCartForMember;
	public static String productSubTotalInShoppingCart;
	
	public static String productNameInShoppingCart;
	public static String productQTYInShoppingCart;
	public static String proceedToCheckoutButtonInShoppingCart;
	
	public static String productNameXpathInProductDetailPage;
	public static String productPriceXpathInProductDetailPage;
	public static String productNameXpathOrderReview;
	public static String productPriceXpathInOrderReview;
	public static String productTotalPriceXpathInOrderReview;
	public static String productTotalPriceBeforeShippingInOrderReview;
	public static String productTotalPriceAfterShippingInOrderReview;

	
	protected static String productNameXpathPaypalReviewPage;
	protected static String productPriceXpathInPaypalReviewPage;
	protected static String btnsubmitPaypalReviewPage;	
	
	protected static String txtCheckoutMemberEmail;
	protected static String txtCheckoutMemberPassword;
	protected static String btnCheckoutMemberSubmitButton;
	protected static String btnCheckoutMemberSeparateSubmitButtonForPw;
	protected static String checkoutMemberInvalidEmailXpath;
	protected static String checkoutMemberInvalidPasswordXpath;
	protected static String msgCheckoutMemberInvalidEmail;
	protected static String msgCheckoutMemberInvalidPassword;
	protected static String checkoutMemberEmptyEmailXpath;
	protected static String checkoutMemberEmptyPasswordXpath;
	protected static String msgCheckoutMemberEmptyEmail;
	protected static String msgCheckoutMemberEmptyPassword;	
	protected static String myAccountEditPw;
	public static String modelClosePopup ;
	
	public static String btnCheckoutAsGuestUser;	
	public static String chkCheckoutAsGuestUser;
	protected static String btnBillingInfoContinue;
	protected static String btnShippingInfoContinue;
	protected static String btnShippingMethodContinue;
	protected static String btnDeliveryMethodContinue;
	protected static String CheckoutAsRegisteredUser;
	protected static String billingLoginEmail;
	protected static String billingLoginPassword;
	
	protected static String billingEmail;
	protected static String billingFirstName;
	protected static String billingLastName;
	protected static String billingTelephone;
	protected static String billingsuburb;
	protected static String billingstreet1;
	protected static String billingCity;
	
	protected static String billingPassword;
	protected static String billingConfirmPassword;
	
	protected static String billingRegion_Id;
	protected static String billingPostcode;
	protected static String linkaddressHint;
	protected static String billingCountry_Id;
	protected static String billingCreateAccount;
	
	protected static String shippingFirstName;
	protected static String shippingLastName;
	protected static String shippingTelephone;
	protected static String shippingstreet1;
	protected static String shippingCity;
	protected static String shippingRegion_Id;
	protected static String shippingPostcode;
	protected static String shippingCountry_Id;
	
	protected static String getTotalShippingMethods;
	protected static String getShippingMethodTag;	
	
	protected static String chkPaymentIsCC;
	protected static String chkPaymentIsPaypal;
	protected static String chkPaymentIsMoneyOrder;
	
	protected static String cCName;
	protected static String cCNumber;
	protected static String cCCvs;
	protected static String cCMonth;
	protected static String cCYear;
	protected static String cCType;
	
	protected static String selectNewBillingAddressDropDown;
	protected static String newBillingAddressdropdownlabel;
	protected static String selectNewShippingAddressDropDown;
	protected static String newShippingAddressdropdownlabel;
	
	protected static String txtBillingLoginEmail;
	protected static String txtBillingLoginPassword;
	protected static String btnBillingContinue;
	
	protected static String lnkSelectNewAddress;
	protected static String lnkNewAddress;
	
	protected static String btnBillingSubmitPassword;
	protected static String txtBillingEmail;
	protected static String txtBillingFirstName;
	protected static String txtBillingLastName;
	protected static String txtBillingTelephone;
	protected static String txtBillingSuburb;
	protected static String txtBillingstreet1;
	protected static String txtBillingCity;
	
	protected static String txtDeliveryFirstName;
	protected static String txtDeliveryLastName;
	protected static String txtDeliveryTelephone;
	protected static String txtDeliverySuburb;
	protected static String txtDeliverystreet1;

	
	protected static String txtPassword;
	protected static String txtConfirmPassword;
	protected static String selectBillingRegion_Id;
	protected static String txtBillingRegion ;
	protected static String txtBillingCustPw;
	protected static String txtBillingCustConfirmPw;
	protected static String billingCustPw;
	protected static String billingJoinUserEmail;
	protected static String userCreationMailSubject;
	
	protected static String billingBirthdayDay;
	protected static String billingBirthdayMonth;
	protected static String billingBirthdayYear;

	protected static String selectBillingRegion_Id_DropDown;
	protected static String selectMemberBillingRegion_Id_DropDown;
	protected static String selectBillingPostCodeDropDown;
	protected static String txtBillingPostcode;
	protected static String txtPostalCode;
	protected static String selectBillingCountry_Id;
	
	protected static String txtShippingFirstName;						 
	protected static String txtShippingLastName;
	protected static String txtShippingTelephone;
	protected static String txtShippingstreet1;
	protected static String txtShippingCity;
	protected static String selectShippingRegion_Id;
	protected static String selectShippingRegion_Id_DropDown;
	protected static String selectMemberShippingRegion_Id_DropDown;
	protected static String txtShippingPostcode;
	protected static String selectShippingCountry_Id;
	protected static String txtShippingRegion ;	
	protected static String selectShippingPostCodeDropDown;
	
	protected static String txtCCNumber;
	protected static String txtCCName;
	protected static String txtCCCvs;
	protected static String selectCCType;
	protected static String selectCCMonth;
	protected static String selectCCYear;
	protected static String selectCCTypeDropDown;
	protected static String selectCCMonthDropDown;
	protected static String selectCCYearDropDown;
	protected static String selectCCTypeDropDownValue;
	protected static String selectCCMonthDropDownValue;
	protected static String selectCCYearDropDownValue;
	
	protected static String txtPaypalEmail;
	protected static String txtPaypalPassword;
	protected static String btnPaypalSubmit;
	protected static String btnPaypalContinue;
	
	
	protected static String billingGuestPassword;
	protected static String billingGusetEmail;

	
	protected static String paypalEmail;
	protected static String paypalPassword;
	
	protected static String chkTeamsAndCondtion;
	protected static String btnContinuePaymentScreen;	
	protected static String chkDifferentShipAddress;
	protected static String chkSameShipAddress;
	
	protected static String validationBillingGuestEmailRequired;
	protected static String validationBillingGuestEmailInvalid;
	protected static String validationBillingFirstName;
	protected static String validationBillingLastName;
	protected static String validationBillingTelephone;
	protected static String validationBillingPostcode;
	protected static String validationBillingCountry_Id;
	protected static String validationBillingEmail;
	
	
	protected static String validationDeliveryFirstName;
	protected static String validationDeliveryLastName;
	protected static String validationDeliveryAddress1;
	protected static String validationDeliveryCity;
	protected static String validationDeliveryTelephone;

	//protected static String validationBillingRegion;
	
	protected static String validationMsgBillingGuestEmailRequired;
	protected static String validationMsgBillingGuestEmailInvalid;
	
	protected static String validationMsgDeliveryFirstName;
	protected static String validationMsgDeliveryLastName;
	protected static String validationMsgDeliveryAddress1;
	protected static String validationMsgDeliveryCity;
	protected static String validationMsgDeliveryTelephone;
	
	
	
	
	protected static String validationMsgBillingFirstName;
	protected static String validationMsgBillingLastName;
	protected static String validationMsgBillingTelephone;
	protected static String validationMsgBillingPostcode;
	protected static String validationMsgBillingCountry_Id;
	protected static String validationMsgBillingEmail;
	protected static String validationMsgBillingRegion;
	
	protected static String validationMsgShippingFirstName;
	protected static String validationMsgShippingLastName;
	protected static String validationMsgShippingTelephone;
	protected static String validationMsgShippingPostcode;
	protected static String validationMsgShippingCountry_Id;
	protected static String validationShippingRegion ;
	protected static String validationMsgShippingRegion ;
	
	protected static String validationShippingFirstName;
	protected static String validationShippingLastName;
	protected static String validationShippingTelephone;
	protected static String validationShippingPostcode;
	protected static String validationShippingCountry_Id;
	
	protected static String orderReviewContinueButton;
	public static String errorMsg;
	public static String productDetailsErrorMsg;
	protected static String billingAddressInReviewPage;
	protected static String shippingAddressInReviewPage;
	protected static String getBillingAddressInReviewPage;
	protected static String getShippingAddressInReviewPage;
	protected static String getTotalShippingMethodsXpath ;
	protected static String getShippingMethodType;
	protected static String selectShippingMethodInput;
	protected static String selectShippingMethod;
	protected static String getShiipingValue;
	protected static String selectedShippingMethodLocatorType;	
	
	protected static String orderIdMsgInConfirmationPage;
	protected static String orderIdXpath;
	protected static String loggedUserOrderIdXpath;
	protected static String orderNumber; 
	protected static String continueShoppingButton;
	
	protected static String myAccountLink;
	protected static String myAccountDropLink;
	protected static String myAccountUrl;
	protected static String myOrderLink;
	protected static String orderTable;
	protected static String orderPrice;
	protected static String orderName;
	protected static String orderTotalPrice;
	protected static String btnReOrder;
	protected static String btPrinteOrder;
	protected static String lbOrderViewHeader;
	
	public static String chkoutAjaxLoader; 
	protected static String ajaxLoader;	
	
	protected static String chkoutAjaxUserType; 
	protected static String chkoutAjaxBilling; 
	protected static String chkoutAjaxShipping; 
	protected static String chkoutAjaxShippingMethod; 
	protected static String chkoutAjaxorderReview;
	protected static String chkoutAjaxGiftCard;
	protected static String chkoutAjaxPayment; 
	protected static String chkoutAjaxClassType; 
	protected static String chkoutAjaxClassValue; 
	protected static String HasCheckoutAjaxRequest;	
	
	protected static String ccEmptyNameXpath;
	protected static String ccEmptyCardTypeXpath;
	protected static String ccEmptyCardNumberXpath;
	protected static String ccEmptyExpMonthXpath;
	protected static String ccEmptyCvcXpath;
	protected static String ccEmptyExpYearXpath;
	protected static String ccInvalidCardNumberXpath;
	protected static String ccInvalidCvcXpath;
	
	protected static String msgCcEmptyName;
	protected static String msgCcEmptyCardType;
	protected static String msgCcEmptyCardNumber;
	protected static String msgCcEmptyExpMonth;
	protected static String msgCcEmptyCvc;
	protected static String msgCcEmptyExpYear;
	protected static String msgCcInvalidCardNumber;
	protected static String msgCcInvalidCvc;
	
	protected static String txtCouponCodeXpath;
	protected static String btnCouponCode;
	protected static String msgSuccessxpath;
	protected static String msgErrorxpath;
	protected static String txtCouponCodeFixed;
	
	protected static String msgBillingWithoutPw;
	protected static String msgBillingWithoutConfirmPw;
	protected static String msgBillingLessPw;
	protected static String msgBillingMisMatchPw;
	

	public static void getLocation()throws Exception
	{
		
		leftCategory  = findElementInXLSheet(getParameterXpath,"left category xpath");		
		noOfProductsPerColoum = findElementInXLSheet(getParameterXpath,"no of products per coloum");
		categoryViewXpath = findElementInXLSheet(getParameterXpath,"category view");
		productSizeBlock = findElementInXLSheet(getParameterXpath,"select size");
		productSizeClassName = findElementInXLSheet(getParameterXpath,"product size class name");
		productSizeAttributeValue = findElementInXLSheet(getParameterXpath,"product size attribute value");
		productSizeclassOutofStockValue = findElementInXLSheet(getParameterXpath,"product size class out of stock value");
		
		simpleProductQtyTextBox = findElementInXLSheet(getParameterXpath,"simple product qty text box");
		groupProductQtyTextBox = findElementInXLSheet(getParameterXpath,"group product qty text box");
		configProductQtyTextBox = findElementInXLSheet(getParameterXpath,"config product qty text box");
		getDefaultQtyAttribute = findElementInXLSheet(getParameterXpath,"get default qty attribute count");
		getProductAttributes = findElementInXLSheet(getParameterXpath,"product attributes");
		getProductName = findElementInXLSheet(getParameterXpath,"product name in category page");
		getProductPrice =findElementInXLSheet(getParameterXpath,"product price in category page"); 
		hasSpecialPriceTagInCategoryPage = findElementInXLSheet(getParameterXpath,"has product special price tag in category page");
		hasOriginalPriceTagInCategoryPage = findElementInXLSheet(getParameterXpath,"has product original price tag in category page");
		tagNameForSpecialPrice = findElementInXLSheet(getParameterXpath,"tag name for special price");
		
		productSizeDropdownXpath =findElementInXLSheet(getParameterXpath,"size dropdown xpath");
		productDropdownOptionsValue = findElementInXLSheet(getParameterXpath,"dropdown options value");
		productColorDropdownXpath = findElementInXLSheet(getParameterXpath,"color dropdown xpath");
		withoutSelectingSizeErrorXpath = findElementInXLSheet(getParameterXpath,"add to cart without selecting size");
		
		groupProductQty = findElementInXLSheet(getParameterXpath,"group product qty text box");
		groupProductPrice = findElementInXLSheet(getParameterXpath,"group product product price");
		groupProductName = findElementInXLSheet(getParameterXpath,"group product product name");
		groupProductTable = findElementInXLSheet(getParameterXpath,"group product table");
		/**
		 * This is use for gift card
		 */
		giftCardSenderNameXpath = findElementInXLSheet(getParameterXpath,"gift card sender name");
		giftCardSenderEmailXpath = findElementInXLSheet(getParameterXpath,"gift card sender email");
		giftCardRecipentNameXpath = findElementInXLSheet(getParameterXpath,"gift card recipient name");
		giftCardRecipentEmailXpath = findElementInXLSheet(getParameterXpath,"gift card recipient email");
		giftCardAmountDropdownXpath = findElementInXLSheet(getParameterXpath,"gift card amount has dropdown");
		giftCardAmountTxtXpath = findElementInXLSheet(getParameterXpath,"gift card amount has txt field");
		
		giftCardWithoutDropdownAmount = findElementInXLSheet(getParameterXpath,"gift card without amount has dropdown");
		giftCardWithoutTxtAmount = findElementInXLSheet(getParameterXpath,"gift card without amount has txt");
		giftCardWithoutSenderName = findElementInXLSheet(getParameterXpath,"gift card without sender name");
		giftCardWithoutSenderEmail = findElementInXLSheet(getParameterXpath,"gift card without sender email");
		giftCardWithoutRecipentName = findElementInXLSheet(getParameterXpath,"gift card without recipient name");
		giftCardWithoutRecipentEmail = findElementInXLSheet(getParameterXpath,"card without recipient email");
		
		
		addToCartButton = findElementInXLSheet(getParameterXpath,"add to cart button");
		SelectProductSize = findElementInXLSheet(getParameterXpath,"Select Product Size");
		
		ajaxLoader = findElementInXLSheet(getParameterXpath,"ajax loader");
		
		HasAddToCartAjaxRequest = findElementInXLSheet(getMethodIsExecuted,"has checkout ajax request");
		addToCartAjax = findElementInXLSheet(getParameterXpath,"add to cart ajax request");
		addToCartAjaxClassType = findElementInXLSheet(getParameterXpath,"add to cart ajax request calss style type");
		addToCartAjaxClassValue = findElementInXLSheet(getParameterXpath,"add to cart ajax request calss style value");
				
		myCartXpath = findElementInXLSheet(getParameterXpath,"my cart xpath");
		viewCartId = findElementInXLSheet(getParameterXpath,"cart id");
		viewCart = findElementInXLSheet(getParameterXpath,"view cart");
		shoppingCartTable = findElementInXLSheet(getParameterXpath,"shopping cart table");
		rmvItemFromCart = findElementInXLSheet(getParameterXpath,"remove item from shopping cart");		
		
		
		productNameXpathInProductDetailPage =  findElementInXLSheet(getParameterXpath,"product name in product detail page");
		productPriceXpathInProductDetailPage = findElementInXLSheet(getParameterXpath,"product price in product detail page");
		hasSpecialPriceTagInDetailsPage = findElementInXLSheet(getParameterXpath,"has product special price tag in detail page"); 
		hasPriceTagInDetailsPage = findElementInXLSheet(getParameterXpath,"has product price tag in details page"); 
		outOfStockProductMessage = findElementInXLSheet(getParameterXpath,"out of stock message");
		productQTYXpath  = findElementInXLSheet(getParameterXpath,"qty");
		updateProductQTY = findElementInXLSheet(getParameterXpath,"qty field in shopping cart page");
		updateButton = findElementInXLSheet(getParameterXpath,"update button");
		
		shoppingCartTitle = findElementInXLSheet(getParameterXpath,"shopping cart page title");
		productPriceInShoppingCartForGuest = findElementInXLSheet(getParameterXpath,"product price in shopping cart page for guest");
		productPriceInShoppingCartForMember = findElementInXLSheet(getParameterXpath,"product price in shopping cart page for member");
		productSubTotalInShoppingCart = findElementInXLSheet(getParameterXpath,"product sub total in shopping cart page");
		productNameInShoppingCart = findElementInXLSheet(getParameterXpath,"product name in cart");
		productQTYInShoppingCart = findElementInXLSheet(getParameterXpath,"qty in shopping cart page");
		proceedToCheckoutButtonInShoppingCart = findElementInXLSheet(getParameterXpath,"proceed to checkout button");
		
		getProductAttributes = findElementInXLSheet(getParameterXpath,"product attributes");
		getProductName = findElementInXLSheet(getParameterXpath,"product name in category page");
		getProductPrice = findElementInXLSheet(getParameterXpath,"product price in category page");
		modelClosePopup= findElementInXLSheet(getParameterXpath,"model close popup");
		
		validationBillingGuestEmailRequired = findElementInXLSheet(getParameterXpath,"msg billing login email required");
		validationBillingGuestEmailInvalid = findElementInXLSheet(getParameterXpath,"msg billing login email invalid");
		txtCheckoutMemberEmail = findElementInXLSheet(getParameterXpath,"checkout member email");
		txtCheckoutMemberPassword = findElementInXLSheet(getParameterXpath,"checkout member password");
		btnCheckoutMemberSubmitButton = findElementInXLSheet(getParameterXpath,"checkout member submit button");
		btnCheckoutMemberSeparateSubmitButtonForPw = findElementInXLSheet(getParameterXpath,"checkout member separate submit button for pw");
		checkoutMemberInvalidEmailXpath = findElementInXLSheet(getParameterXpath,"checkout member invalid email xpath");
		checkoutMemberInvalidPasswordXpath = findElementInXLSheet(getParameterXpath,"checkout member invalid password xpath");
		checkoutMemberEmptyEmailXpath = findElementInXLSheet(getParameterXpath,"checkout member empty email xpath");
		checkoutMemberEmptyPasswordXpath = findElementInXLSheet(getParameterXpath,"checkout member empty password xpath");
				
		//Billing Information		
		chkoutAjaxLoader = findElementInXLSheet(getParameterXpath,"checkout ajax loader");
		txtBillingLoginEmail = findElementInXLSheet(getParameterXpath,"txtBillingLoginEmail");
		txtBillingLoginPassword = findElementInXLSheet(getParameterXpath,"txtBillingLoginPassword");
		btnBillingContinue = findElementInXLSheet(getParameterXpath,"btnBillingContinue");
		btnBillingSubmitPassword = findElementInXLSheet(getParameterXpath,"btnBillingSubmitPassword");
		lnkSelectNewAddress = findElementInXLSheet(getParameterXpath,"selectAddreddMenu");
		lnkNewAddress = findElementInXLSheet(getParameterXpath,"lnkNewAddress");
		
		
		
		btnCheckoutAsGuestUser = findElementInXLSheet(getParameterXpath,"checkout as guest user continue");
		txtBillingEmail = findElementInXLSheet(getParameterXpath,"txtBillingEmail");

		selectNewBillingAddressDropDown = findElementInXLSheet(getParameterXpath,"member select new billing address");
		newBillingAddressdropdownlabel = findElementInXLSheet(getParameterXpath,"member new billing address label");
		selectNewShippingAddressDropDown = findElementInXLSheet(getParameterXpath,"member select new shipping address");
		newShippingAddressdropdownlabel = findElementInXLSheet(getParameterXpath,"member new shipping address label");
		
		txtBillingFirstName = findElementInXLSheet(getParameterXpath,"txtBillingFirstName");
		txtBillingLastName = findElementInXLSheet(getParameterXpath,"txtBillingLastName");
		txtBillingTelephone = findElementInXLSheet(getParameterXpath,"txtBillingTelephone");
		txtBillingSuburb = findElementInXLSheet(getParameterXpath,"txtBillingSuburb");
		txtBillingstreet1 = findElementInXLSheet(getParameterXpath,"txtBillingstreet1");
		txtBillingCity = findElementInXLSheet(getParameterXpath,"txtBillingCity");
		
		
		txtDeliveryFirstName = findElementInXLSheet(getParameterXpath,"txtDeliveryFirstName");
		txtDeliveryLastName = findElementInXLSheet(getParameterXpath,"txtDeliveryLastName");
		txtDeliveryTelephone = findElementInXLSheet(getParameterXpath,"txtDeliveryTelephone");
		txtDeliverySuburb = findElementInXLSheet(getParameterXpath,"txtDeliveryPostCode");
		txtDeliverystreet1 = findElementInXLSheet(getParameterXpath,"txtDeliveryStreet1");
	
		
		txtPassword = findElementInXLSheet(getParameterXpath,"txtPassword");
		txtConfirmPassword = findElementInXLSheet(getParameterXpath,"txtConfirmPassword");
		selectBillingRegion_Id = findElementInXLSheet(getParameterXpath,"selectBillingRegion_Id");
		
		txtBillingRegion = findElementInXLSheet(getParameterXpath,"txtBillingRegion");
		txtBillingCustPw = findElementInXLSheet(getParameterXpath,"txtBillingCustPw");
		txtBillingCustConfirmPw = findElementInXLSheet(getParameterXpath,"txtBillingCustConfirmPw");
		billingBirthdayDay = findElementInXLSheet(getParameterXpath,"billing dob-day dropdown");
		billingBirthdayMonth = findElementInXLSheet(getParameterXpath,"billing dob-month dropdown");
		billingBirthdayYear = findElementInXLSheet(getParameterXpath,"billing dob-year dropdown");
		
		
		selectBillingRegion_Id_DropDown = findElementInXLSheet(getParameterXpath,"selectBillingRegion_Id_DropDown");
		selectMemberBillingRegion_Id_DropDown = findElementInXLSheet(getParameterXpath,"selectMemberBillingRegion_Id_DropDown");
		selectBillingPostCodeDropDown = findElementInXLSheet(getParameterXpath,"select billing postcode dropdown");
		txtBillingPostcode = findElementInXLSheet(getParameterXpath,"txtBillingPostcode");
		txtPostalCode = findElementInXLSheet(getParameterXpath,"SelectPostcode");
		
		selectBillingCountry_Id = findElementInXLSheet(getParameterXpath,"selectBillingCountry_id");
		
		billingCreateAccount = findElementInXLSheet(getParameterXpath,"create an account during checkout");
		
		chkCheckoutAsGuestUser = findElementInXLSheet(getParameterXpath,"checkout as guest user");
		btnBillingInfoContinue = findElementInXLSheet(getParameterXpath,"billing info continue button");		
		
		//Shipping Address		
		chkDifferentShipAddress = findElementInXLSheet(getParameterXpath,"chk different ship address");
		chkSameShipAddress = findElementInXLSheet(getParameterXpath,"chk same ship address");
		txtShippingFirstName = findElementInXLSheet(getParameterXpath,"txtShippingFirstName");
		txtShippingLastName = findElementInXLSheet(getParameterXpath,"txtShippingLastName");
		txtShippingTelephone = findElementInXLSheet(getParameterXpath,"txtShippingTelephone");
		txtShippingstreet1 = findElementInXLSheet(getParameterXpath,"txtShippingstreet1");
		txtShippingCity = findElementInXLSheet(getParameterXpath,"txtShippingCity");
		selectShippingRegion_Id = findElementInXLSheet(getParameterXpath,"selectShippingRegion_Id");
		txtShippingPostcode = findElementInXLSheet(getParameterXpath,"txtShippingPostcode");
		selectShippingCountry_Id = findElementInXLSheet(getParameterXpath,"selectShippingCountry_id");
		selectShippingRegion_Id_DropDown = findElementInXLSheet(getParameterXpath,"selectShippingRegion_Id_DropDown");
		selectMemberShippingRegion_Id_DropDown = findElementInXLSheet(getParameterXpath,"selectMemberShippingRegion_Id_DropDown");
		selectShippingPostCodeDropDown = findElementInXLSheet(getParameterXpath,"select shipping postcode dropdown");
		txtShippingRegion = findElementInXLSheet(getParameterXpath,"txtShippingRegion");
		
		btnShippingInfoContinue =  findElementInXLSheet(getParameterXpath,"shipping info continue button");
				
		getTotalShippingMethodsXpath = findElementInXLSheet(getParameterXpath,"total shipping methods table xpath");
		getShippingMethodTag = findElementInXLSheet(getParameterXpath,"shipping method tag");
		getShippingMethodType = findElementInXLSheet(getParameterXpath,"shipping method type");
		selectShippingMethodInput = findElementInXLSheet(getParameterXpath,"shipping method input type");
		getShiipingValue = findElementInXLSheet(getParameterXpath,"shipping value");
		selectedShippingMethodLocatorType = findElementInXLSheet(getParameterXpath,"selected shipping method locator type");
		
		selectShippingMethod = findElementInXLSheet(getParameterXpath,"select shipping method"); 
		
		btnShippingMethodContinue = findElementInXLSheet(getParameterXpath,"shipping method continue button");
		btnDeliveryMethodContinue = findElementInXLSheet(getParameterXpath,"Delivery information continue button");
		
		
		
		selectCCType= findElementInXLSheet(getParameterXpath,"select credit card type");
					
		//Credit Card		
		selectCCTypeDropDown = findElementInXLSheet(getParameterXpath,"credit card type dropdown");
		selectCCYearDropDown = findElementInXLSheet(getParameterXpath,"expiration year dropdown");
		selectCCMonthDropDown = findElementInXLSheet(getParameterXpath,"expiration month dropdown");
		
		selectCCTypeDropDownValue = findElementInXLSheet(getParameterXpath,"credit card type dropdown value");
		selectCCYearDropDownValue = findElementInXLSheet(getParameterXpath,"expiration year dropdown value");
		selectCCMonthDropDownValue = findElementInXLSheet(getParameterXpath,"expiration month dropdown value");
		chkPaymentIsCC = findElementInXLSheet(getParameterXpath,"payment type is cc");
		chkPaymentIsMoneyOrder = findElementInXLSheet(getParameterXpath,"payment type is money order");
		chkPaymentIsPaypal = findElementInXLSheet(getParameterXpath,"payment type is paypal");
		txtCCName = findElementInXLSheet(getParameterXpath,"credit card name");
		txtCCNumber = findElementInXLSheet(getParameterXpath,"credit card number");
		selectCCYear = findElementInXLSheet(getParameterXpath,"select expiration year");
		txtCCCvs = findElementInXLSheet(getParameterXpath,"card verification number");
		selectCCMonth = findElementInXLSheet(getParameterXpath,"select expiration month");
		chkTeamsAndCondtion = findElementInXLSheet(getParameterXpath,"payment teams and condition");
		btnContinuePaymentScreen = findElementInXLSheet(getParameterXpath,"payment continue button");
		
		//paypal
		txtPaypalEmail = findElementInXLSheet(getParameterXpath,"paypal email address");
		txtPaypalPassword = findElementInXLSheet(getParameterXpath,"paypal password");
		btnPaypalSubmit = findElementInXLSheet(getParameterXpath,"paypal submit button");
		btnPaypalContinue = findElementInXLSheet(getParameterXpath,"paypal continue button");
		
		//Order Review		
		productNameXpathOrderReview = findElementInXLSheet(getParameterXpath,"product name in order review page");
		productPriceXpathInOrderReview = findElementInXLSheet(getParameterXpath,"product price in order review page");
		productTotalPriceXpathInOrderReview = findElementInXLSheet(getParameterXpath,"grand total order review page");
		productTotalPriceXpathInOrderReview = findElementInXLSheet(getParameterXpath,"grand total order review page");
		productTotalPriceBeforeShippingInOrderReview = findElementInXLSheet(getParameterXpath,"grand total before shipping in order review page");
		productTotalPriceAfterShippingInOrderReview = findElementInXLSheet(getParameterXpath,"grand total after shipping in order review page");
		
		orderReviewContinueButton = findElementInXLSheet(getParameterXpath,"order review continue button");
		errorMsg = findElementInXLSheet(getParameterXpath,"errorMsg");
		productDetailsErrorMsg = findElementInXLSheet(getParameterXpath,"product details error msg");
		//moneyOrderContinueButton = findElementInXLSheet(getParameterXpath,"money order continue button");
		
		billingAddressInReviewPage = findElementInXLSheet(getParameterXpath,"billing address in order review");
		shippingAddressInReviewPage = findElementInXLSheet(getParameterXpath,"shipping address in order review");
		productNameXpathPaypalReviewPage = findElementInXLSheet(getParameterXpath,"Product name in paypal review page");
		
		productPriceXpathInPaypalReviewPage = findElementInXLSheet(getParameterXpath,"Product price in paypal review page");
		
		btnsubmitPaypalReviewPage = findElementInXLSheet(getParameterXpath,"paypal review continue button");
		
		//Confirmation Page
		
		orderIdMsgInConfirmationPage = findElementInXLSheet(getParameterXpath,"order id msg in confirmation page");
		orderIdXpath = findElementInXLSheet(getParameterXpath,"order id xpath");
		loggedUserOrderIdXpath = findElementInXLSheet(getParameterXpath,"logged user order id xpath");
		continueShoppingButton = findElementInXLSheet(getParameterXpath,"continue shopping button");
		myAccountLink = findElementInXLSheet(getParameterXpath,"my account link");
		myAccountDropLink= findElementInXLSheet(getParameterXpath,"my account drop link");
		myAccountUrl = findElementInXLSheet(getParameterXpath,"my account url");
		billingAddressInReviewPage = findElementInXLSheet(getParameterXpath,"billing address in order review");
		chkoutAjaxUserType = findElementInXLSheet(getParameterXpath,"user type ajax request");
		chkoutAjaxBilling = findElementInXLSheet(getParameterXpath,"billing infor ajax request");
		chkoutAjaxShipping = findElementInXLSheet(getParameterXpath,"shipping info ajax request");
		chkoutAjaxorderReview = findElementInXLSheet(getParameterXpath,"order review infor ajax request");
		chkoutAjaxShippingMethod = findElementInXLSheet(getParameterXpath,"shipping method info ajax request");
		chkoutAjaxGiftCard = findElementInXLSheet(getParameterXpath,"gift card infor ajax request");
		chkoutAjaxPayment = findElementInXLSheet(getParameterXpath,"payament infor ajax request");
		chkoutAjaxClassType = findElementInXLSheet(getParameterXpath,"checkout ajax request calss style type");
		HasCheckoutAjaxRequest = findElementInXLSheet(getMethodIsExecuted,"has checkout ajax request");
		chkoutAjaxClassValue = findElementInXLSheet(getParameterXpath,"checkout ajax request calss style value");
		myOrderLink = findElementInXLSheet(getParameterXpath,"my order link");
		orderTable = findElementInXLSheet(getParameterXpath,"get order table in my account");
		orderName = findElementInXLSheet(getParameterXpath,"get order name");
		orderPrice = findElementInXLSheet(getParameterXpath,"get order price");
		orderTotalPrice = findElementInXLSheet(getParameterXpath,"get order total price");
		btnReOrder = findElementInXLSheet(getParameterXpath,"reorder button");
		btPrinteOrder = findElementInXLSheet(getParameterXpath,"print order button");
		lbOrderViewHeader = findElementInXLSheet(getParameterXpath,"order view page header");
		txtCouponCodeXpath = findElementInXLSheet(getParameterXpath,"txt coupon code xpath");
		btnCouponCode = findElementInXLSheet(getParameterXpath,"btn coupon code");
		msgSuccessxpath = findElementInXLSheet(getParameterXpath,"shopping cart success xpath");
		msgErrorxpath = findElementInXLSheet(getParameterXpath,"shopping cart error xpath");
		txtCouponCodeFixed = findElementInXLSheet(getParameterXpath,"coupon code name for fixed");
		msgBillingWithoutPw = findElementInXLSheet(getParameterXpath,"msg billing without pw");
		msgBillingWithoutConfirmPw = findElementInXLSheet(getParameterXpath,"msg billing without confirm pw");
		msgBillingLessPw = findElementInXLSheet(getParameterXpath,"msg billing less pw");
		
		msgBillingMisMatchPw = findElementInXLSheet(getParameterXpath,"msg billing mismatch pw");
		validationBillingFirstName = findElementInXLSheet(getParameterXpath,"msg billing first name");
		validationBillingLastName = findElementInXLSheet(getParameterXpath,"msg billing last name");
		validationBillingTelephone = findElementInXLSheet(getParameterXpath,"msg billing telephone");
		validationBillingPostcode = findElementInXLSheet(getParameterXpath,"msg billing postcode");
		validationBillingCountry_Id = findElementInXLSheet(getParameterXpath,"msg billing country");
		
		validationDeliveryFirstName = findElementInXLSheet(getParameterXpath,"msg delivery first name");
		validationDeliveryLastName = findElementInXLSheet(getParameterXpath,"msg delivery last name");
		validationDeliveryAddress1 = findElementInXLSheet(getParameterXpath,"msg delivery address1");
		validationDeliveryCity = findElementInXLSheet(getParameterXpath,"msg delivery city");
		validationDeliveryTelephone = findElementInXLSheet(getParameterXpath,"msg delivery telephone");
		
		
		
		validationBillingEmail = findElementInXLSheet(getParameterXpath,"msg billing email");
		validationShippingRegion = findElementInXLSheet(getParameterXpath,"msg shipping region");
		validationShippingFirstName = findElementInXLSheet(getParameterXpath,"msg shipping first name");
		validationShippingLastName = findElementInXLSheet(getParameterXpath,"msg shipping last name");
		validationShippingTelephone = findElementInXLSheet(getParameterXpath,"msg shipping telephone");
		validationShippingPostcode = findElementInXLSheet(getParameterXpath,"msg shipping postcode");
		validationShippingCountry_Id = findElementInXLSheet(getParameterXpath,"msg shipping country");
		ccEmptyNameXpath = findElementInXLSheet(getParameterXpath,"cc empty name xpath");
		ccEmptyCardTypeXpath = findElementInXLSheet(getParameterXpath,"cc empty card type xpath");
		ccEmptyCardNumberXpath = findElementInXLSheet(getParameterXpath,"cc empty card number xpath");
		ccEmptyExpMonthXpath = findElementInXLSheet(getParameterXpath,"cc empty exp month xpath");
		ccEmptyCvcXpath = findElementInXLSheet(getParameterXpath,"cc empty cvc xpath");
		ccEmptyExpYearXpath = findElementInXLSheet(getParameterXpath,"cc empty exp year xpath");
		ccInvalidCardNumberXpath = findElementInXLSheet(getParameterXpath,"cc invalid card number xpath");
		ccInvalidCvcXpath = findElementInXLSheet(getParameterXpath,"cc invalid cvc xpath");
		
		//messages
		
		
		validationBillingGuestEmailRequired = findElementInXLSheet(getMessages,"msg billing login email required");	
		validationBillingGuestEmailInvalid = findElementInXLSheet(getMessages,"msg billing login email invalid");
		
		
		validationMsgDeliveryFirstName = findElementInXLSheet(getMessages,"msg delivery first name");
		validationMsgDeliveryLastName = findElementInXLSheet(getMessages,"msg delivery last name");
		validationMsgDeliveryAddress1 = findElementInXLSheet(getMessages,"msg delivery address");
		validationMsgDeliveryCity = findElementInXLSheet(getMessages,"msg delivery city");
		validationMsgDeliveryTelephone = findElementInXLSheet(getMessages,"msg delivery telephone");
		
		
		
		validationMsgBillingFirstName = findElementInXLSheet(getMessages,"msg billing first name");
		validationMsgBillingLastName = findElementInXLSheet(getMessages,"msg billing last name");
		validationMsgBillingTelephone = findElementInXLSheet(getMessages,"msg billing telephone");
		validationMsgBillingPostcode = findElementInXLSheet(getMessages,"msg billing postcode");
		validationMsgBillingCountry_Id = findElementInXLSheet(getMessages,"msg billing country");
		validationMsgBillingEmail = findElementInXLSheet(getMessages,"msg billing email");
		validationMsgShippingRegion = findElementInXLSheet(getMessages,"msg shipping region");
		msgCcEmptyName = findElementInXLSheet(getMessages,"msg cc empty name");
		msgCcEmptyCardType = findElementInXLSheet(getMessages,"msg cc empty card type");
		msgCcEmptyCardNumber = findElementInXLSheet(getMessages,"msg cc empty card number");
		msgCcEmptyExpMonth = findElementInXLSheet(getMessages,"msg cc empty exp month");
		msgCcEmptyCvc = findElementInXLSheet(getMessages,"msg cc empty cvc");
		msgCcEmptyExpYear = findElementInXLSheet(getMessages,"msg cc empty exp year");
		msgCcInvalidCardNumber = findElementInXLSheet(getMessages,"msg cc invalid card number");
		msgCcInvalidCvc = findElementInXLSheet(getMessages,"msg cc invalid cvc");
		validationMsgBillingGuestEmailRequired = findElementInXLSheet(getMessages,"msg billing login email required");
		validationMsgBillingGuestEmailInvalid = findElementInXLSheet(getMessages,"msg billing login email invalid");
		validationMsgGiftCardWithoutAmount = findElementInXLSheet(getMessages,"msg gift card without amount");
		validationMsgGiftCardWithoutSenderName = findElementInXLSheet(getMessages,"msg gift card without sender name");
		validationMsgGiftCardWithoutSenderEmail = findElementInXLSheet(getMessages,"msg gift card without sender email");
		validationMsgGiftCardWithoutRecipentName = findElementInXLSheet(getMessages,"msg gift card without recipient name");
		validationMsgGiftCardWithoutRecipentEmail = findElementInXLSheet(getMessages,"msg gift card without recipient email");
		msgCheckoutMemberInvalidEmail = findElementInXLSheet(getMessages,"msg checkout member invalid email");
		msgCheckoutMemberInvalidPassword = findElementInXLSheet(getMessages,"msg checkout member invalid password");
		msgCheckoutMemberEmptyEmail = findElementInXLSheet(getMessages,"msg checkout member empty email");
		msgCheckoutMemberEmptyPassword = findElementInXLSheet(getMessages,"msg checkout member empty password");
		
				

		
	}
	
	public static void getDataFromXLSheet()throws Exception
	{	
		mainBrandName = findElementInXLSheet(getCheckoutDetails,"Product Main Brand Type");
		giftCardSenderName = findElementInXLSheet(getCheckoutDetails,"gift card sender name");
		giftCardSenderEmail = findElementInXLSheet(getCheckoutDetails,"gift card sender email");
		giftCardRecipentName = findElementInXLSheet(getCheckoutDetails,"gift card recipient name");
		giftCardRecipentEmail = findElementInXLSheet(getCheckoutDetails,"gift card recipient email");				
		
		billingLoginEmail = findElementInXLSheet(getCheckoutDetails,"Billing Login Email");
		billingLoginPassword = findElementInXLSheet(getCheckoutDetails,"Billing Login Password");
		billingFirstName = findElementInXLSheet(getCheckoutDetails,"Billing First Name");
		billingLastName = findElementInXLSheet(getCheckoutDetails,"Billing Last Name");
		billingTelephone = findElementInXLSheet(getCheckoutDetails,"Billing Telephone");
		billingsuburb = findElementInXLSheet(getCheckoutDetails,"Billing Suburb");
		billingstreet1 = findElementInXLSheet(getCheckoutDetails,"Billing street1");
		billingCity = findElementInXLSheet(getCheckoutDetails,"Billing City");
		billingPassword = findElementInXLSheet(getCheckoutDetails,"Billing Password");
		billingConfirmPassword = findElementInXLSheet(getCheckoutDetails,"Billing Confirm Password");
		billingRegion_Id = findElementInXLSheet(getCheckoutDetails,"Billing Region_Id");
		billingPostcode = findElementInXLSheet(getCheckoutDetails,"Billing Postcode");
		billingCountry_Id = findElementInXLSheet(getCheckoutDetails,"Billing Country");	
		billingGuestPassword = findElementInXLSheet(getCheckoutDetails,"Guest Password");
		billingGusetEmail = findElementInXLSheet(getCheckoutDetails,"Guest Email Address");
		
		shippingFirstName = findElementInXLSheet(getCheckoutDetails,"Shipping First Name");
		shippingLastName = findElementInXLSheet(getCheckoutDetails,"Shipping Last Name");
		shippingTelephone = findElementInXLSheet(getCheckoutDetails,"Shipping Telephone");
		shippingstreet1 = findElementInXLSheet(getCheckoutDetails,"Shipping street1");
		shippingCity = findElementInXLSheet(getCheckoutDetails,"Shipping City");
		shippingRegion_Id = findElementInXLSheet(getCheckoutDetails,"Shipping Region_Id");
		shippingPostcode = findElementInXLSheet(getCheckoutDetails,"Shipping Postcode");
		shippingCountry_Id = findElementInXLSheet(getCheckoutDetails,"Shipping Country");			
		
		validationMsgShippingFirstName = findElementInXLSheet(getCheckoutDetails,"Msg Shipping First Name");
		validationMsgShippingLastName = findElementInXLSheet(getCheckoutDetails,"Msg Shipping Last Name");
		validationMsgShippingTelephone = findElementInXLSheet(getCheckoutDetails,"Msg Shipping Telephone");
		validationMsgShippingPostcode = findElementInXLSheet(getCheckoutDetails,"Msg Shipping Postcode");
		validationMsgShippingCountry_Id = findElementInXLSheet(getCheckoutDetails,"Msg Shipping Country");	
		myAccountEditPw =findElementInXLSheet(getAccountDetails,"Edit Password");
		
		billingCustPw = findElementInXLSheet(getUserDetails,"Password");
		billingJoinUserEmail = findElementInXLSheet(getUserDetails,"Email Address");
		
		
		
		
		setQtyInDetailPage = findElementInXLSheet(getCheckoutDetails,"QTY");
		cCType = findElementInXLSheet(getCheckoutDetails,"Credit Card Type");
		cCName = findElementInXLSheet(getCheckoutDetails,"Credit Card Name");
		cCNumber = findElementInXLSheet(getCheckoutDetails,"Credit Card Number");
		cCYear = findElementInXLSheet(getCheckoutDetails,"Expiration Year");
		cCCvs = findElementInXLSheet(getCheckoutDetails,"Card Verification Number");
		cCMonth = findElementInXLSheet(getCheckoutDetails,"Expiration Month");
		paypalEmail = findElementInXLSheet(getCheckoutDetails,"Paypal Email Address");
		paypalPassword = findElementInXLSheet(getCheckoutDetails,"Paypal Password");
		
	}		
}
