package com.net.test.data;

import com.net.test.util.TestBase;

public class MyAccountTestData extends TestBase{
	
	public static String loginLink;
	public static String loginUrl;
	public static String myAccountLinkXpath;
	public static String txtLoginEmailAddress;
	public static String txtLoginPassword;
	public static String btnLoginSubmitButton;
	public static String msgXpathInvalidAccountEmailAddress;
	public static String msgXpathAccountPwLess;
	public static String msgXpathEmailAddress;
	
	public static String msgXpathPassword;
	public static String msgXpathInvalidLogin;
	protected static String linkForgotPw;
	protected static String txtForgotPwEmail;
	protected static String msgXpathForgotPwEmailValidation;
	protected static String msgXpathForgotPwEmailRequired;
	protected static String msgXpathForgotPwFail;
	protected static String msgXpathForgotPwSuccess;
	protected static String txtForgotPw;
	protected static String txtForgotPwConfirm;
	protected static String btnForgotPwSubmit;
	public static String linkLogOut;
	public static String titleMyAccount;
	public static String titleUserLogin;
	protected static String lbAccountUserName;
	protected static String lbAccountBreadcrumb;
	
	protected static String msgEmailAddress;
	protected static String msgPasswordRequired;		
	protected static String msgInvalidLoginDetails;	
	protected static String msgInvalidUserName;	
	protected static String msgInvalidPw;	
	protected static String msgForgotPwEmailValidation;		
	protected static String msgForgotPwEmailRequired;		
	protected static String msgForgotPwFail;
	protected static String msgForgotPwSuccess;
	protected static String msgInvalidAccountEmailAddress;
	protected static String msgAccountPwLess;		
	public static String loginPassword;		
	public static String loginEmailAddress;		
	protected static String forgotPw;		
	protected static String forgotConfirmPw;
	protected static String forgotPwEmailAddress;
	protected static String forgotPwFirstName;		
	protected static String myAccountFirstName;
	protected static String myAccountLastName;
	protected static String myAccountEditFirstName;
	protected static String myAccountEditLastName;
	public static String myAccountEditPw;
	protected static String myAccountEditConfirmPw;
	
	protected static String myAccountInfoLink;
	protected static String myAccountFirstNameXpath;
	protected static String myAccountLastNameXpath;
	protected static String myAccountEmailXpath;
	protected static String myAccountPasswordXpath;
	protected static String myAccountConfirmPasswordXpath;
	protected static String myAccountChangePwXpath;
	protected static String myAccountCurrentPasswordXpath;
	
	protected static String editInforSaveButton ;	
	protected static String forgotPwMailSubject ;	
	
	protected static String editAccountWithoutFirstName;
	protected static String editAccountWithoutLastName;
	protected static String editAccountWithoutEmail;
	protected static String editAccountWithoutConfirmEmail;
	protected static String editAccountWithoutPassword;
	protected static String editAccountWithoutConfirmPassword;
	protected static String editAccountLessPassword;
	protected static String editAccountLessConfirmPassword;
	protected static String editAccountMissMatchPassword;
	protected static String editAccountInvalidEmail;
	protected static String editAccountInvalidConfirmEmail;
	protected static String editAccountAlreadyRegistedUser;
	protected static String editAccountMissmatchEmail;
	protected static String editAccountMissmatchConfirmEmail;
	protected static String editAccountInvalidBirthday ;
	protected static String linkTermsAndConditions ;
	protected static String linkPrivacyPolicy ;
	protected static String editAccountWithoutDOB ;
	
	protected static String editAccountMsgWithoutFirstName;
	protected static String editAccountMsgWithoutLastName;
	protected static String editAccountMsgWithoutEmail;
	protected static String editAccountMsgWithoutConfirmEmail;
	protected static String editAccountMsgWithoutPassword;
	protected static String editAccountMsgWithoutConfirmPassword;
	protected static String editAccountMsgLessPassword;
	protected static String editAccountMsgLessConfirmPassword;
	protected static String editAccountMsgMissMatchPassword;
	protected static String editAccountMsgInvalidEmail;
	protected static String editAccountMsgInvalidConfirmEmail;
	protected static String editAccountMsgAlreadyRegistedUser;
	protected static String editAccountMsgMissmatchEmail;
	protected static String editAccountMsgMissmatchConfirmEmail;
	protected static String editAccountMsgInvalidBirthday ;
	protected static String titleTermsAndConditions;
	protected static String titlePrivacyPolicy ;
	protected static String editAccountMsgWithoutDOB ;
	protected static String editAccountSaveMsg;
	
	protected static String linkAddressBook;
	protected static String btnNewaddress;
	protected static String lbGetAllSavedAddresses;
	protected static String btnEditAddress;
	protected static String btnGoBack;

	protected static String txtNewAddressFirstName;
	protected static String txtNewAddressLastName;
	protected static String txtNewAddressTelephone;
	protected static String txtNewAddressStreet;
	protected static String txtNewAddressCity;
	protected static String txtNewAddressPostCode;
	protected static String txtNewAddressZip;
	protected static String txtNewAddressPhone;	
	protected static String selectNewAddressPostCode;
	protected static String txtNewAddressState;
	protected static String selectNewAddressState;
	protected static String selectNewAddressCountry;
	protected static String btnNewAddresssave;
	
	protected static String btnDeleteAddress;
	
	protected static String newAddressWithoutFirstName;
	protected static String newAddressWithoutLastName;
	protected static String newAddressWithoutTelephone;
	protected static String newAddressWithoutStreet;
	protected static String newAddressWithoutCity;
	protected static String newAddressWithoutPostCode;
	protected static String newAddressWithoutState;
	protected static String newAddressWithoutCountry;
	protected static String newAddressCreateSuccesXpath;
	
	protected static String msgNewAddressWithoutFirstName;
	protected static String msgNewAddressWithoutLastName;
	protected static String msgNewAddressWithoutTelephone;
	protected static String msgNewAddressWithoutStreet;
	protected static String msgNewAddressWithoutCity;
	protected static String msgNewAddressWithoutPostCode;
	protected static String msgNewAddressWithoutState;
	protected static String msgNewAddressWithoutCountry;
	protected static String msgNewAddressCreateSucces;
	
	protected static String newAddressFirstName;
	protected static String newAddressLastName;
	protected static String newAddressTelephone;
	protected static String newAddressStreet;
	protected static String newAddressZip;
	protected static String newAddressCity;
	protected static String newAddressPostCode;
	protected static String newAddressState;
	protected static String newAddressCountry;
	//protected static String txtNewAddressFirstName;	
	
	public static String mouseMoveToMyAccount ;
	
	protected static String wishListLink;
	protected static String wishListTable;
	protected static String rmvItemFromWishList;
	protected static String myWishlistLink;
	protected static String wishListProdcutName;
	protected static String wishListProdcutPrice;
	protected static String myAccountUrl;
	public static String myAccountLink;
	protected static String txtWishListCommentBoxXpath;
	protected static String btnWishListUpdate;
	protected static String btnAddToCart;
	protected static String wishListCommentBox;
	protected static String btnShareWishlist;
	protected static String txtShareWishlistEmailXpath;
	protected static String txtShareWishlistMessageXpath;
	protected static String btnshareWishSave;
	protected static String shareWishlistSuccessXpath;
	protected static String shareWishlistValidationXpath;
	protected static String msgShareWishlistSuccess;
	protected static String msgShareWishlistValidation;
	protected static String shareWishlistEmail;
	protected static String shareWishlistMessage;
	
	protected static String myAccountErrorMsg;
	
	protected static String hasMyAccountAjaxRequest;
	protected static String myAccountAjax;
	protected static String myAccountAjaxClassType;
	protected static String myAccountAjaxClassValue;
	
	
	protected static String myAccountLinksLogOut;
	
	public void getLocation()throws Exception
	{
		loginLink = findElementInXLSheet(getParameterXpath,"login link");
		loginUrl = findElementInXLSheet(getParameterXpath,"login URL");	
		myAccountLinkXpath = findElementInXLSheet(getParameterXpath,"account links under my account section");
		
		txtLoginEmailAddress = findElementInXLSheet(getParameterXpath,"email address");
		txtLoginPassword = findElementInXLSheet(getParameterXpath,"password");
		btnLoginSubmitButton = findElementInXLSheet(getParameterXpath,"login button");
		
		myAccountLinksLogOut = findElementInXLSheet(getParameterXpath,"my account links xpath log out");
		
		msgXpathEmailAddress = findElementInXLSheet(getParameterXpath,"email address required");		
		msgXpathPassword = findElementInXLSheet(getParameterXpath,"password required");
		msgXpathInvalidLogin = findElementInXLSheet(getParameterXpath,"invalid login details");
		
		
		linkForgotPw = findElementInXLSheet(getParameterXpath,"forgot password link");
		txtForgotPwEmail = findElementInXLSheet(getParameterXpath,"forgot password email");
		
		msgXpathInvalidAccountEmailAddress = findElementInXLSheet(getParameterXpath,"invalid my account email address");
		msgXpathAccountPwLess = findElementInXLSheet(getParameterXpath,"my account pw less");
		
		msgXpathForgotPwEmailValidation = findElementInXLSheet(getParameterXpath,"forgot pw email validation");
		msgXpathForgotPwEmailRequired = findElementInXLSheet(getParameterXpath,"forgot pw email required");
		msgXpathForgotPwFail = findElementInXLSheet(getParameterXpath,"forgot pw fail msg");
		msgXpathForgotPwSuccess = findElementInXLSheet(getParameterXpath,"forgot pw success msg");
		txtForgotPw = findElementInXLSheet(getParameterXpath,"forgot password id");
		txtForgotPwConfirm = findElementInXLSheet(getParameterXpath,"forgot confirm password id");
		btnForgotPwSubmit = findElementInXLSheet(getParameterXpath,"forgot password submit");
		linkLogOut = findElementInXLSheet(getParameterXpath,"logout link");
		titleMyAccount = findElementInXLSheet(getParameterXpath,"my account page title");
		titleUserLogin = findElementInXLSheet(getParameterXpath,"user login title");
		lbAccountUserName = findElementInXLSheet(getParameterXpath,"account user name");
		lbAccountBreadcrumb = findElementInXLSheet(getParameterXpath,"account breadcrumb");
		
		msgEmailAddress =findElementInXLSheet(getMessages,"email address required");
		msgPasswordRequired = findElementInXLSheet(getMessages,"password required");		
		msgInvalidLoginDetails = findElementInXLSheet(getMessages,"invalid login details");	
		msgInvalidUserName = findElementInXLSheet(getMessages,"invalid user name");
		msgInvalidPw = findElementInXLSheet(getMessages,"invalid password");
		msgForgotPwEmailValidation = findElementInXLSheet(getMessages,"forgot pw email validation");		
		msgForgotPwEmailRequired = findElementInXLSheet(getMessages,"forgot pw email required");		
		msgForgotPwFail = findElementInXLSheet(getMessages,"forgot pw fail msg");
		msgForgotPwSuccess = findElementInXLSheet(getMessages,"forgot pw success msg");
		msgInvalidAccountEmailAddress = findElementInXLSheet(getMessages,"invalid my account email address");
		msgAccountPwLess = findElementInXLSheet(getMessages,"my account pw less");
		forgotPwMailSubject = findElementInXLSheet(getParameterXpath,"forgot password mail subject"); 	
		
		myAccountInfoLink = findElementInXLSheet(getParameterXpath,"account information link");
		myAccountFirstNameXpath= findElementInXLSheet(getParameterXpath,"my account first name xpath");
		myAccountLastNameXpath = findElementInXLSheet(getParameterXpath,"my account last name xpath");
		myAccountEmailXpath = findElementInXLSheet(getParameterXpath,"my account email xpath");
		myAccountPasswordXpath = findElementInXLSheet(getParameterXpath,"my account password xpath");
		myAccountConfirmPasswordXpath = findElementInXLSheet(getParameterXpath,"my account confirm password xpath");
		myAccountChangePwXpath = findElementInXLSheet(getParameterXpath,"my account change pw xpath");
		myAccountCurrentPasswordXpath = findElementInXLSheet(getParameterXpath,"my account current password xpath");
		
		editInforSaveButton = findElementInXLSheet(getParameterXpath,"edit infor save button");

		editAccountWithoutFirstName = findElementInXLSheet(getParameterXpath,"edit account without first name");
		editAccountMsgWithoutFirstName = findElementInXLSheet(getMessages,"edit account without first name");
		editAccountWithoutLastName = findElementInXLSheet(getParameterXpath,"edit account without last name");
		editAccountMsgWithoutLastName = findElementInXLSheet(getMessages,"edit account without last name");
		editAccountWithoutEmail = findElementInXLSheet(getParameterXpath,"edit account without email");
		editAccountMsgWithoutEmail = findElementInXLSheet(getMessages,"edit account without email");
		editAccountWithoutConfirmEmail = findElementInXLSheet(getParameterXpath,"edit account without confirm email");
		editAccountMsgWithoutConfirmEmail = findElementInXLSheet(getMessages,"edit account without confirm email");
		editAccountWithoutPassword = findElementInXLSheet(getParameterXpath,"edit account without pw");
		editAccountMsgWithoutPassword = findElementInXLSheet(getMessages,"edit account without pw");
		editAccountWithoutConfirmPassword = findElementInXLSheet(getParameterXpath,"edit account without confirm pw");
		editAccountMsgWithoutConfirmPassword = findElementInXLSheet(getMessages,"edit account without confirm pw");
		editAccountLessPassword = findElementInXLSheet(getParameterXpath,"edit account less pw");
		editAccountMsgLessPassword = findElementInXLSheet(getMessages,"edit account less pw");
		editAccountLessConfirmPassword = findElementInXLSheet(getParameterXpath,"edit account less confirm");
		editAccountMsgLessConfirmPassword = findElementInXLSheet(getMessages,"edit account less confirm");
		editAccountMissMatchPassword = findElementInXLSheet(getParameterXpath,"edit account mismatch pw");
		editAccountMsgMissMatchPassword = findElementInXLSheet(getMessages,"edit account mismatch pw");
		editAccountSaveMsg = findElementInXLSheet(getMessages,"edit account save message");
		
		mouseMoveToMyAccount =  findElementInXLSheet(getParameterXpath,"mouse move to my account");
		
		linkAddressBook =  findElementInXLSheet(getParameterXpath,"address book link");
		btnNewaddress =  findElementInXLSheet(getParameterXpath,"new address button");
		lbGetAllSavedAddresses = findElementInXLSheet(getParameterXpath,"get all saved addresses");
		btnDeleteAddress = findElementInXLSheet(getParameterXpath,"address delete button");
		btnEditAddress = findElementInXLSheet(getParameterXpath,"edit address button");
		
		txtNewAddressFirstName =  findElementInXLSheet(getParameterXpath,"txt new address first name");
		txtNewAddressLastName =  findElementInXLSheet(getParameterXpath,"txt new address last name");
		txtNewAddressTelephone =  findElementInXLSheet(getParameterXpath,"txt new address telephone");
		txtNewAddressStreet =  findElementInXLSheet(getParameterXpath,"txt new address street");
		txtNewAddressCity =  findElementInXLSheet(getParameterXpath,"txt new address city");
		txtNewAddressPostCode =  findElementInXLSheet(getParameterXpath,"txt new address postcode");
		txtNewAddressZip =  findElementInXLSheet(getParameterXpath,"txt new address zip");
		selectNewAddressPostCode =  findElementInXLSheet(getParameterXpath,"select new address postcode dropdown");
		txtNewAddressPhone = findElementInXLSheet(getParameterXpath,"txt new address telephone");
		
		txtNewAddressState = findElementInXLSheet(getParameterXpath,"txt new address state");
		selectNewAddressState = findElementInXLSheet(getParameterXpath,"select new address state");
		selectNewAddressCountry = findElementInXLSheet(getParameterXpath,"select new address country");
		btnNewAddresssave =  findElementInXLSheet(getParameterXpath,"new address save button");
		btnGoBack = findElementInXLSheet(getParameterXpath,"go back button in address info");
		
		newAddressWithoutFirstName = findElementInXLSheet(getParameterXpath,"new address witout first name xpath");
		newAddressWithoutLastName = findElementInXLSheet(getParameterXpath,"new address witout last name xpath");
		newAddressWithoutTelephone = findElementInXLSheet(getParameterXpath,"new address witout telephone xpath");
		newAddressWithoutStreet = findElementInXLSheet(getParameterXpath,"new address witout street xpath");
		newAddressWithoutCity = findElementInXLSheet(getParameterXpath,"new address witout city xpath");
		newAddressWithoutPostCode = findElementInXLSheet(getParameterXpath,"new address witout postcode xpath");
		newAddressWithoutState = findElementInXLSheet(getParameterXpath,"new address witout state xpath");
		newAddressWithoutCountry = findElementInXLSheet(getParameterXpath,"new address witout country xpath");
		newAddressCreateSuccesXpath = findElementInXLSheet(getParameterXpath,"new address create success xpath");
		
		msgNewAddressWithoutFirstName = findElementInXLSheet(getMessages,"msg new address witout first name");
		msgNewAddressWithoutLastName = findElementInXLSheet(getMessages,"msg new address witout last name");
		msgNewAddressWithoutTelephone = findElementInXLSheet(getMessages,"msg new address witout telephone");
		msgNewAddressWithoutStreet = findElementInXLSheet(getMessages,"msg new address witout street");
		msgNewAddressWithoutCity = findElementInXLSheet(getMessages,"msg new address witout city");
		msgNewAddressWithoutPostCode = findElementInXLSheet(getMessages,"msg new address witout postcode");
		msgNewAddressWithoutState = findElementInXLSheet(getMessages,"msg new address witout state");
		msgNewAddressCreateSucces = findElementInXLSheet(getMessages,"msg new address create success");
		
		wishListLink 	= findElementInXLSheet(getParameterXpath,"wishlist link");
		wishListTable	= findElementInXLSheet(getParameterXpath,"wishList table");
		rmvItemFromWishList = findElementInXLSheet(getParameterXpath,"remove item from wishlist");
		myWishlistLink = findElementInXLSheet(getParameterXpath,"my wishlist link");
		wishListProdcutName = findElementInXLSheet(getParameterXpath,"wishlist product name xpath");
		wishListProdcutPrice = findElementInXLSheet(getParameterXpath,"wishlist product price xpath");
		
		myAccountUrl = findElementInXLSheet(getParameterXpath,"my account URL");
		myAccountLink= findElementInXLSheet(getParameterXpath,"my account link");

		txtWishListCommentBoxXpath = findElementInXLSheet(getParameterXpath,"wishlist comment box xpath");
		btnWishListUpdate = findElementInXLSheet(getParameterXpath,"update wishlist button");
		
		btnShareWishlist = findElementInXLSheet(getParameterXpath,"share wishlist button");
		txtShareWishlistEmailXpath = findElementInXLSheet(getParameterXpath,"share wishlist email address text box xpath");
		txtShareWishlistMessageXpath = findElementInXLSheet(getParameterXpath,"share wishlist message box xpath");
		btnshareWishSave = findElementInXLSheet(getParameterXpath,"share wishlist save button");
		shareWishlistSuccessXpath = findElementInXLSheet(getParameterXpath,"share wishlist success msg xpath");
		shareWishlistValidationXpath = findElementInXLSheet(getParameterXpath,"share wishlist validation xpath");
		msgShareWishlistSuccess = findElementInXLSheet(getMessages,"msg share wishlist success");
		msgShareWishlistValidation = findElementInXLSheet(getMessages,"msg share wishlist validation");
		
		
		btnAddToCart = findElementInXLSheet(getParameterXpath,"wishlist add to cart button");
		myAccountErrorMsg = findElementInXLSheet(getParameterXpath,"my account error msg");
				
		myAccountAjaxClassType = findElementInXLSheet(getParameterXpath,"my account ajax request calss style type");
		myAccountAjaxClassValue = findElementInXLSheet(getParameterXpath,"my account ajax request calss style value");
		hasMyAccountAjaxRequest = findElementInXLSheet(getMethodIsExecuted,"has my account ajax request");
		myAccountAjax	= findElementInXLSheet(getParameterXpath,"my account ajax request");		
		
	}
	
	
	public void getDataFromXLSheet()throws Exception
	{
		loginPassword = findElementInXLSheet(getAccountDetails,"Password");
		loginEmailAddress = findElementInXLSheet(getAccountDetails,"Email Address");
		forgotPw = findElementInXLSheet(getAccountDetails,"Forgot Password");
		forgotConfirmPw = findElementInXLSheet(getAccountDetails,"Forgot Confirm Password");
		forgotPwEmailAddress = findElementInXLSheet(getAccountDetails,"Forgot Password Email Address");
		forgotPwFirstName = findElementInXLSheet(getAccountDetails,"forgot Password First Name");
		myAccountFirstName = findElementInXLSheet(getAccountDetails,"First Name");
		myAccountLastName = findElementInXLSheet(getAccountDetails,"Last Name");		
		myAccountEditFirstName =findElementInXLSheet(getAccountDetails,"Edit First Name");
		myAccountEditLastName = findElementInXLSheet(getAccountDetails,"Edit Last Name");
		myAccountEditPw =findElementInXLSheet(getAccountDetails,"Edit Password");
		myAccountEditConfirmPw = findElementInXLSheet(getAccountDetails,"Edit Confirm Password");
		
		newAddressFirstName = findElementInXLSheet(getAccountDetails,"New Address First Name");
		newAddressLastName = findElementInXLSheet(getAccountDetails,"New Address Last Name");
		newAddressTelephone = findElementInXLSheet(getAccountDetails,"New Address Telephone");
		newAddressStreet = findElementInXLSheet(getAccountDetails,"New Address Street");
		newAddressZip  = findElementInXLSheet(getAccountDetails,"New Address Zip");
		newAddressCity = findElementInXLSheet(getAccountDetails,"New Address City");
		newAddressPostCode = findElementInXLSheet(getAccountDetails,"New Address Postcode");
		newAddressState = findElementInXLSheet(getAccountDetails,"New Address State");
		newAddressCountry = findElementInXLSheet(getAccountDetails,"New Address Country");
		
		wishListCommentBox = findElementInXLSheet(getAccountDetails,"Wishlist Comment");
		
		shareWishlistEmail = findElementInXLSheet(getAccountDetails,"Share Wishlist Email List");
		shareWishlistMessage = findElementInXLSheet(getAccountDetails,"Share Wishlist Message");
	}
	
	
}
