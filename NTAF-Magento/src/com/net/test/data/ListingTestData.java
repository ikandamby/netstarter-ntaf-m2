package com.net.test.data;

import com.net.test.util.TestBase;

public class ListingTestData extends TestBase
{
	
	//Get location(xpath)
	protected static String dropItemShowXpath;
	protected static String lbTotalItems;
	protected static String getNoofPagesXpath;
	protected static String getPagesCountTagName;
	protected static String nextButton;
	protected static String previuosButton;
	protected static String categoryViewXpath;
	protected static String noOfProductsPerColoum;
	protected static String getSortedProductName;
	protected static String getSortedProductPrice;
	protected static String dropdownSort;
	protected static String sortByNameAsc;
	protected static String sortByPriceAsc;
	protected static String sortByPriceDesc;
	protected static String sortByNameDesc;
	protected static String productSizeClassName;
	protected static String withoutSelectingSizeErrorXpath ;
	protected static String withoutSelectingQTY ;

	protected static String ajaxLoader;
	protected static String productSizeDropdownXpath;
	protected static String productDropdownOptionsValue;
	


	public static void getLocation()throws Exception
	{		
		dropItemShowXpath = findElementInXLSheet(getParameterXpath,"show item per page dropdown");
		lbTotalItems = findElementInXLSheet(getParameterXpath,"get items per page");
		getNoofPagesXpath = findElementInXLSheet(getParameterXpath,"get total no of pages xpath");
		getPagesCountTagName = findElementInXLSheet(getParameterXpath,"get pages count tag name");
		nextButton = findElementInXLSheet(getParameterXpath,"next button");
		previuosButton = findElementInXLSheet(getParameterXpath,"previous button");
		categoryViewXpath = findElementInXLSheet(getParameterXpath,"category view");
		noOfProductsPerColoum = findElementInXLSheet(getParameterXpath,"no of products per coloum");
		getSortedProductName = findElementInXLSheet(getParameterXpath,"get sorted product name");
		getSortedProductPrice = findElementInXLSheet(getParameterXpath,"get sorted product price");
		dropdownSort = findElementInXLSheet(getParameterXpath,"select sort dropdown");
		sortByNameAsc = findElementInXLSheet(getParameterXpath,"sort by name asc");
		sortByNameDesc = findElementInXLSheet(getParameterXpath,"sort by name desc");
		sortByPriceAsc = findElementInXLSheet(getParameterXpath,"sort by price asc");
		sortByPriceDesc = findElementInXLSheet(getParameterXpath,"sort by price desc");
		productSizeClassName = findElementInXLSheet(getParameterXpath,"Product Size Class Name");
		withoutSelectingQTY = findElementInXLSheet(getParameterXpath,"add to cart without selecting QTY");
		withoutSelectingSizeErrorXpath = findElementInXLSheet(getParameterXpath,"add to cart without selecting size");
		productSizeDropdownXpath =findElementInXLSheet(getParameterXpath,"size dropdown xpath");
		productDropdownOptionsValue = findElementInXLSheet(getParameterXpath,"dropdown options value");
		
		

	}
	
	public static void getDataFromXLSheet()throws Exception
	{

	}
	
	
		
}
