package com.net.test.util;

import java.sql.DriverManager;
import java.util.HashMap;

import org.testng.Assert;

//import com.net.test.pageObject.ReadCommonData;
//import com.net.test.pageObject.UserCreation;


public class DBConnectionUtill {
	
	public java.sql.Connection con = null;
	
	ReadXl readParametrs  = new ReadXl();
	WriteResults wResult =  new WriteResults();

	public HashMap<String, String> getParameterXpath = new HashMap<String, String>();
    public String serverName;
	public String portNumber;
	public String dbName;
	public String driverName = "com.mysql.jdbc.Driver";
	public String userName; 
	public String password;

	public void mainParameters()throws Exception
	{
		getParameterXpath=readParametrs.getParametersXpath();
			
	}
	 
  	 public java.sql.Connection getConnection() throws Exception 
  	 {
  		  mainParameters();
  		  serverName = getParameterXpath.get("Server Name");
  		  portNumber = getParameterXpath.get("Port Number");
  		  dbName 	 = getParameterXpath.get("DB Name");
  		  userName	 = getParameterXpath.get("User Name");
  		  password	 = getParameterXpath.get("DB Password"); 
  		 
  		  System.out.println("serverName"+serverName);
  		  System.out.println("portNumber"+portNumber);
  		  System.out.println("dbName"+dbName);
  		  System.out.println("userName"+userName);
  		  System.out.println("password"+password);
  		
         try {
                 Class.forName(driverName);
                 con = DriverManager.getConnection("jdbc:mysql://"+serverName+":"+portNumber+"/"+dbName, userName, password);

                 if (con != null)
                         System.out.println("Connection Successful!");
         } catch (Exception e) {
        	
        	 wResult.writeTestResult("Verify Database connection", "Connection should be successfully", "Error Trace in getConnection() : "+ e.getMessage(), false, "Genaral Error");
 			 Assert.assertEquals("Connection should be successfully", "Error Trace in getConnection() : "+ e.getMessage());
 		
                 e.printStackTrace();
                 System.out.println("Error Trace in getConnection() : "
                                 + e.getMessage());
                 
         }
         return con;
  	 }

}         




 
