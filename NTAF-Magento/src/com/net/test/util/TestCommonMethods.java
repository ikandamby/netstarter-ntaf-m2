package com.net.test.util;

import java.net.HttpURLConnection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.net.test.data.CheckoutTestData;
import com.net.test.data.HomeTestData;
import com.net.test.data.ListingTestData;
import com.net.test.data.MyAccountTestData;
import com.net.test.data.ShoppingCartTestData;
import com.net.test.data.UserCreationTestData;



public class TestCommonMethods extends TestBase
{
	/**Define Test data object **/
	public static CheckoutTestData checkoutTestData = new CheckoutTestData();
	public static MyAccountTestData myAccountTestData = new MyAccountTestData();
	public static HomeTestData homeTestData = new HomeTestData();
	public static ListingTestData listingTestData = new ListingTestData();
	public static UserCreationTestData userCreationTestData = new UserCreationTestData();
	public static ShoppingCartTestData shoppingTestData = new ShoppingCartTestData();
	public static DBConnectionUtill db = new DBConnectionUtill();
	
	/** Define product price and name in detail and category page **/
	public static String productPrice = "";
	public static String productName = "";
	public static String productNameInDetailPage = "";
	public static String productPriceInDetailPage = "";
	public static String categoryName = "";
	
	//Get geneal properties location(xpath)
	
	public static String siteUrl;
	public static String secureUrl;
	public static String appUserName;	
	public static String appPassword;
	public static String errorMsg;
	
	public static String login;
	public static String base64login;
	public static boolean status;
	public static int getResponseStatus; 
	public static String responseError = "";
	
	public static String notFoundElement = "";
	public static String setProductName ;
	public static String productTitle ;
	public static String getProductSpPrice ="";
	
	protected static int i = 0;

	public static double grandTotalInShoppingcart = 0.0; 
	public static String productPriceInShoppingCartPage = "";	
	public static String productSubTotalInShoppingCartPage = "";
	protected static String productNameInShoppingCartPage;	
	
	protected static String adminUrl;
	protected static String adminUserName;
	protected static String adminPw;
	protected static String customerLink;
	protected static String navCustomerLink;
	protected static String navNewsLetterLink;
	protected static String newsLetterLink;
	
	public void readMainParameters()throws Exception
	{		
		getParameters=readXL.getMainParameters();
		getParameterXpath=readXL.getParametersXpath();
		getCheckoutDetails=readXL.getMainChkOutDetails();
		getAccountDetails=readXL.getMyAccountDetails();
		getUserDetails=readXL.getMainUserDetails();
		isclassExecuted =readXL.isClassExecuted();
		isScenarioExecuted = readXL.isScenarioExecuted();
		getSiteXLName =readXL.getReadFile();
		getMethodIsExecuted = readXL.getExecutedMethods();
		getMessages=readXL.getMessages();
		
		CheckoutTestData.getLocation();		
		CheckoutTestData.getDataFromXLSheet();
		
		homeTestData.getLocation();				
		homeTestData.getDataFromXLSheet();
		
		myAccountTestData.getLocation();		
		myAccountTestData.getDataFromXLSheet();	
		
		ListingTestData.getLocation();
		ListingTestData.getDataFromXLSheet();	

		ShoppingCartTestData.getLocation();
		ShoppingCartTestData.getDataFromXLSheet();
		
		userCreationTestData.getLocation();
		userCreationTestData.getDataFromXLSheet();
					
		siteUrl = findElementInXLSheet(getParameterXpath,"site URL");
		secureUrl = findElementInXLSheet(getParameterXpath,"secure Site URL");
		appUserName = findElementInXLSheet(getParameterXpath,"app user name");
		appPassword = findElementInXLSheet(getParameterXpath,"app password");
		errorMsg =findElementInXLSheet(getParameterXpath,"errorMsg");
		
		adminUrl = findElementInXLSheet(getParameterXpath,"admin url");
		adminUserName = findElementInXLSheet(getParameterXpath,"admin user name");
		adminPw = findElementInXLSheet(getParameterXpath,"admin pw");
		customerLink = findElementInXLSheet(getParameterXpath,"admin customer link");
		navCustomerLink = findElementInXLSheet(getParameterXpath,"admin customer link xpath");
		navNewsLetterLink = findElementInXLSheet(getParameterXpath,"admin newsletter link xpath");
		newsLetterLink = findElementInXLSheet(getParameterXpath,"admin newsletter link");	
		
	}
	
	public void initializeExecutedClass(String executedClassName,String executedMainSenarios)throws Exception
	{
		if(isExecuted(isclassExecuted,executedClassName))
		{	
			if(executedMainSenarios.isEmpty())
			{
				executedMainSenarios = executedClassName;
				
				resultSheetName(executedClassName);
				
				//if(common.appStatus())
				if(true)
				{				
					executedBrowser(); //get selenium instance				
				}
				else
				{
                    skipMethods(executedClassName,"Following response got it:"+getResponseStatus+" - "+responseError,"Found fail Response");
					throw new AssertionError("Home page is not displayed.Following response got it:"+responseError);
				}
			}
			
			else if(isExecuted(isScenarioExecuted,executedMainSenarios))
			{
				resultSheetName(executedMainSenarios);
				
				if(common.appStatus())
				{				
					executedBrowser(); //get selenium instance					 
				}
				else
				{
					skipMethods(executedClassName+"- "+executedMainSenarios,"Following response got it:"+getResponseStatus+" - "+responseError,"Found fail Response");
					throw new AssertionError("Home page is not displayed.Following response got it:"+responseError);
				}	
			}
			else
			{
				skipMethods(executedClassName,executedMainSenarios,"");
				throw new AssertionError("No need to runs"+executedClassName+"- "+executedMainSenarios);
			}			
		}
		else
		{
			skipMethods(executedClassName,executedMainSenarios,"");
			throw new AssertionError("No need to runs "+executedClassName+" "+executedMainSenarios);
		}
	}	
	
	public boolean appStatus()throws Exception
	{
		boolean hasSuccessResponse = true;
		
		login = appUserName + ":" + appPassword;	
		if(isExecuted(getMethodIsExecuted,"checked app status"))
		{
			if(isExecuted(getParameterXpath,"enabled proxy settings"))
			{
				System.setProperty("http.proxyHost", "192.168.10.4");
				System.setProperty("http.proxyPort", "3128");
			}
					
			if(!appUserName.isEmpty() || !appPassword.isEmpty())
			{
				base64login = new String(Base64.encodeBase64(login.getBytes()));
				
				try
				{
					getResponseStatus = Jsoup.connect(siteUrl).header("Authorization", "Basic " + base64login).timeout(1800*1000).ignoreHttpErrors(true).userAgent("Mozilla").followRedirects(true).execute().statusCode();
				}
				catch(Exception e)
				{
					
					responseError = e.getMessage();
					System.out.println(responseError);
					hasSuccessResponse = false;		
					
				}
				if(getResponseStatus == 0 || getResponseStatus != 200)
				{
					int i= 0;
					while((getResponseStatus == 0 || getResponseStatus != 200) && i < 1)
					{
						try
						{
							getResponseStatus = Jsoup.connect(siteUrl).header("Authorization", "Basic " + base64login).timeout(1800*1000).ignoreHttpErrors(true).userAgent("Mozilla").followRedirects(true).execute().statusCode();
						}
						catch(Exception e1)
						{					
							responseError = e1.getMessage();
							System.out.println(responseError);
							hasSuccessResponse = false;
						}	
						i++;
						System.out.println(siteUrl+ " - has not respond in -"+ i*5 +" Sec");
					}
					
					if(getResponseStatus == 0 || getResponseStatus != 200)
					{
						//reRunFile("Response"+getResponseStatus);
					}
					
				}					
			}
			else
			{
				try
				{
					getResponseStatus = Jsoup.connect(siteUrl).maxBodySize(100000000).timeout(1800*1000).ignoreHttpErrors(true).userAgent("Mozilla").followRedirects(true).execute().statusCode();
				}
				catch(Exception e)
				{
					responseError = e.getMessage();
					System.out.println(responseError);
					hasSuccessResponse = false;
				}
				
				if(getResponseStatus == 0 || getResponseStatus != 200)
				{
					int i= 0;
					while((getResponseStatus == 0 || getResponseStatus != 200) && i < 10)
					{			
						try
						{
							getResponseStatus = Jsoup.connect(siteUrl).maxBodySize(100000000).timeout(1800*1000).ignoreHttpErrors(true).userAgent("Mozilla").followRedirects(true).execute().statusCode();
						}
						catch(Exception e1)
						{					
							responseError = e1.getMessage();
							System.out.println(responseError);
							hasSuccessResponse = false;
						}
						i++;
						Thread.sleep(5000);
						System.out.println("iteration count-"+i);
					}
				}
			}
				
			
				
			if (getResponseStatus == HttpURLConnection.HTTP_MOVED_TEMP || getResponseStatus == HttpURLConnection.HTTP_MOVED_PERM || getResponseStatus == HttpURLConnection.HTTP_OK 
					|| getResponseStatus == HttpURLConnection.HTTP_ACCEPTED)
		    {
				hasSuccessResponse = true;
		    }
			else
			{
				System.out.println("Home page is not displayed and response code is "+getResponseStatus);
				hasSuccessResponse = false;				
			}		
		}
		
		
		return hasSuccessResponse;		
	}
	
	public boolean navigateToHomePage()throws Exception
	{
		//readMainParameters();
		
		String concatUrl = siteUrl;
		String secureConcatUrl = secureUrl;	
		boolean homePageDisplayed = true;
		
		if(!appUserName.isEmpty() && !appPassword.isEmpty())
		{
			concatUrl = siteUrl.replaceAll("http://","http://"+appUserName+":"+appPassword+"@");		
			
			if(secureUrl.indexOf("https://") >= 0)
			{
				secureConcatUrl = secureUrl.replaceAll("https://","https://"+appUserName+":"+appPassword+"@");
			System.out.println(secureConcatUrl);
			}
			else
			{
				secureConcatUrl = secureUrl.replaceAll("http://","http://"+appUserName+":"+appPassword+"@");				
				System.out.println(secureConcatUrl);
			}			
		}
		
		try
		{
			if(secureConcatUrl.equalsIgnoreCase(concatUrl))
			{
				openPage(concatUrl);
				System.out.println(secureConcatUrl);
			}
			else
			{
				System.out.println(secureConcatUrl);
			//	openPage(secureConcatUrl);
				openPage(concatUrl);			
			}				
			
			closedHomePopUp();	
			
			System.out.println("Navigate to home page");
			
			if((getTitle() == "") || getTitle().indexOf("Problem loading page") >= 0)
			{
				System.out.println("Home page is not displayed follwing page title found -: "+getTitle());
				reRunFile("Home page is not displayed follwing page title found -: "+getTitle());
				writeTestResults("Verify Home page","Home page should be displayed","Home page is not displayed follwing page title found -: "+getTitle(),"",false,true);
				
			}
		}
		catch (Exception e) 
		{
			reRunFile("Following respose found "+e.getMessage());
			writeTestResults("Verify Home page","Home page should be displayed","Following respose found "+e.getMessage(),"",false,true);
			
			homePageDisplayed = false;			
		}		
		return homePageDisplayed;			
	}
	
	private void closedHomePopUp()throws Exception
	{	
		if(isExecuted(getMethodIsExecuted,"has home page popup"))
		{
			if(isElementPresent(findElementInXLSheet(getParameterXpath,"home signup popup id")))
			{
				click(findElementInXLSheet(getParameterXpath,"home signup popup id"));
			}
		}	
	}
	
	public void closedBrowser()throws Exception
	{	
		driver.close();	
		try
        {
            Thread.sleep(5000);
            driver.quit();
        }
        catch(Exception e)
        {
        }
	}
	
	public boolean homePageLoad()throws Exception
	{	
		boolean homePageDisplayed = true;
		try
		{
			openPage(siteUrl);							
		}
		catch (Exception e) 
		{
			homePageDisplayed = false;			
		}			
		return homePageDisplayed;			
	}
	
	@SuppressWarnings("static-access")
	public boolean navigateToLoginPage()throws Exception
	{	
		boolean found = false;
		String beforeClickUrl;
				
		if(!isElementPresent(myAccountTestData.loginLink))
		{		
			if(!myAccountTestData.mouseMoveToMyAccount.isEmpty())
			{
				mouseMove(myAccountTestData.mouseMoveToMyAccount);			
			}
			
			else
			{
				accountLinksInUnderMyAcc();
			}				
			
			beforeClickUrl = getCurrentUrl();
			
			clickAndWait(myAccountTestData.loginLink);		
					
			if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
			{
				if(!myAccountTestData.loginUrl.isEmpty())
				{
					openPage(siteUrl+myAccountTestData.loginUrl);
				}				
			}	
			
			found = true;				
		}
		else
		{
			beforeClickUrl = getCurrentUrl();
			
			clickAndWait(myAccountTestData.loginLink);		
					
			if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
			{
				if(!myAccountTestData.loginUrl.isEmpty())
				{
					openPage(siteUrl+myAccountTestData.loginUrl);
				}				
			}	
			
			found = true;			
		}
		return found;		
	}
			
	public void goBackToHomePage()throws Exception 
	{
		if(siteUrl.indexOf("@")>-1)
		{
			siteUrl = siteUrl.substring(siteUrl.indexOf("@")+1).trim();
			siteUrl = "http://".concat(siteUrl);
		}
		
		if(!siteUrl.equalsIgnoreCase(getCurrentUrl()))
		{
			openPage(siteUrl);
		}
	}
	
	public void clickLogo() throws Exception
	{
		if(!siteUrl.equalsIgnoreCase(getCurrentUrl()))
		{
			if(!isElementPresent(homeTestData.mainLogo))			
			{			
				driver.get(siteUrl);				
			}
			else
			{
				clickAndWait(homeTestData.mainLogo);				
			}
		}		
		closedHomePopUp();
	}
	
	public String login()throws Exception
	{	
		notFoundElement = "Element present";		
		
		if(!isElementPresent(myAccountTestData.txtLoginEmailAddress))
		{
			notFoundElement = myAccountTestData.txtLoginEmailAddress;
		}
		else if(!isElementPresent(myAccountTestData.txtLoginPassword))
		{
			notFoundElement = myAccountTestData.txtLoginEmailAddress;
		}
		else if(!isElementPresent(myAccountTestData.btnLoginSubmitButton))
		{
			notFoundElement = myAccountTestData.btnLoginSubmitButton;
		}				
		else
		{
			sendKeys(myAccountTestData.txtLoginEmailAddress,myAccountTestData.loginEmailAddress);
			
			sendKeys(myAccountTestData.txtLoginPassword,myAccountTestData.loginPassword);
			
			clickAndWait(myAccountTestData.btnLoginSubmitButton);			
		}				
		return notFoundElement;		
	}
	
	public String loginAfterPassChange()throws Exception
	{	
		String notFoundElement1 = "Element present";		
		
		if(!isElementPresent(myAccountTestData.txtLoginEmailAddress))
		{
			notFoundElement = myAccountTestData.txtLoginEmailAddress;
		}
		else if(!isElementPresent(myAccountTestData.txtLoginPassword))
		{
			notFoundElement = myAccountTestData.txtLoginEmailAddress;
		}
		else if(!isElementPresent(myAccountTestData.btnLoginSubmitButton))
		{
			notFoundElement = myAccountTestData.btnLoginSubmitButton;
		}				
		else
		{
			sendKeys(myAccountTestData.txtLoginEmailAddress,myAccountTestData.loginEmailAddress);
			
			sendKeys(myAccountTestData.txtLoginPassword,myAccountTestData.myAccountEditPw);
			
			clickAndWait(myAccountTestData.btnLoginSubmitButton);	
			writeTestResultsToUJ("42725c8c",true);
			
			writeTestResults("Verify user can login to my account after change the password","My Account should be displayed", "My Account page is displayed","426f169e",true,false);
		
		}				
		return notFoundElement1;		
	}
	public String loginForOthreUser(String loginEmailAddress,String loginPassword)throws Exception
	{			
		String notFoundElement = "";
		
		if(!isElementPresent(myAccountTestData.txtLoginEmailAddress))
		{
			notFoundElement = myAccountTestData.txtLoginEmailAddress;
		}
		else if(!isElementPresent(myAccountTestData.txtLoginPassword))
		{
			notFoundElement = myAccountTestData.txtLoginEmailAddress;
		}
		else if(!isElementPresent(myAccountTestData.btnLoginSubmitButton))
		{
			notFoundElement = myAccountTestData.btnLoginSubmitButton;
		}				
		else
		{
			sendKeys(myAccountTestData.txtLoginEmailAddress,loginEmailAddress);
			
			sendKeys(myAccountTestData.txtLoginPassword,loginPassword);
			
			clickAndWait(myAccountTestData.btnLoginSubmitButton);			
		}
				
		return notFoundElement;		
	}
	
	public String verifyPasswordError()throws Exception
	{
		String errorMessage = "";
				
		if(isDisplayed(myAccountTestData.msgXpathEmailAddress))
		{
			errorMessage = getText(myAccountTestData.msgXpathEmailAddress);
			
		}			
		else if(isDisplayed(myAccountTestData.msgXpathInvalidAccountEmailAddress))
		{
			errorMessage = getText(myAccountTestData.msgXpathInvalidAccountEmailAddress);
		}
		
		else if(isDisplayed(myAccountTestData.msgXpathAccountPwLess))
		{
			errorMessage = getText(myAccountTestData.msgXpathAccountPwLess);
		}
		
		else if(isDisplayed(myAccountTestData.msgXpathPassword))
		{
			errorMessage = getText(myAccountTestData.msgXpathPassword);
		}	
		
		else if(isDisplayed(myAccountTestData.msgXpathInvalidLogin))
		{
			errorMessage = getText(myAccountTestData.msgXpathInvalidLogin);
			
		}			
		else if(isDisplayed(myAccountTestData.msgXpathInvalidLogin))
		{
			errorMessage = getText(myAccountTestData.msgXpathInvalidLogin);
		
		}
		else if(getText("/html/body/pre").indexOf("Mage_Core")>-1)
		{
			errorMessage = getText("/html/body/pre");
		}
			
		
		
		return errorMessage;		
	}	
	
	public String passwordMatch()throws Exception
	{			
		String errorMessage = "";		
		
	/*	if(!myAccountTestData.mouseMoveToMyAccount.isEmpty())
		{
			mouseMove(myAccountTestData.mouseMoveToMyAccount);			
		}	
		else 
		{
			accountLinksInUnderMyAcc();
		}
		*/
		if(myAccountTestData.titleUserLogin.equalsIgnoreCase(getTitle()))
		{
			errorMessage = "";
		}
		
		else if(!isDisplayed(myAccountTestData.linkLogOut))
		{
			errorMessage = verifyPasswordError();				
		}		
		return errorMessage;			
	}
	
	public void accountLinksInUnderMyAcc()throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"Login and account links are under my account"))
		{
			if(isElementPresent(myAccountTestData.myAccountLink))
			{
				click(MyAccountTestData.myAccountLink);
			}
			
			if(!MyAccountTestData.myAccountLinkXpath.isEmpty())
			{
				clickAndWait(MyAccountTestData.myAccountLinkXpath);
			}
		}
	}
	
	public void logingWithEditPw()throws Exception
	{
		if(!isDisplayed(myAccountTestData.txtLoginEmailAddress))
		{
			navigateToLoginPage();
		}
		
		sendKeys(MyAccountTestData.txtLoginEmailAddress,myAccountTestData.loginEmailAddress);
		
		sendKeys(MyAccountTestData.txtLoginPassword,myAccountTestData.myAccountEditPw);
		
		clickAndWait(MyAccountTestData.btnLoginSubmitButton);
		
	}
		
	public void userLogOut()throws Exception
	{		
		if(!myAccountTestData.mouseMoveToMyAccount.isEmpty())
		{
			mouseMove(myAccountTestData.mouseMoveToMyAccount);			
		}
		
		if(isElementPresent("//a[@class='logout']"))
		{
			
			clickAndWait("//a[@class='logout']");	
			System.out.println("Successfully Logged out");
			
		
		}
		else
		{
			System.out.println("Logout link not present");
		}		
	}
	
	public boolean selectBrand() throws Exception
	{
		//readMainParameters();
		boolean foundCategory =true;
		
		String categoryXpath = "";
		
		String beforeClickUrl ="";	
		
		if(isExecuted(getMethodIsExecuted,"Mouse Hover In Main Navigation") && !findElementInXLSheet(getCheckoutDetails,"Product Brand Type").isEmpty())
		{			
			categoryXpath = readXL.getBrandXpath();
			
			mainNavigationMouseMove();
		}
		else
		{
			categoryXpath = readXL.getMainBrandXpath(CheckoutTestData.mainBrandName);
		}						
		
		if(categoryXpath.isEmpty()||categoryXpath.equalsIgnoreCase(null))
		{
			foundCategory =false;
		}
		else if(isElementPresent(categoryXpath))
		{
			beforeClickUrl = getCurrentUrl();	
			
			clickAndWait(categoryXpath);
			
			if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
			{	
				openPage(getAttribute(categoryXpath,"href"));
					
				foundCategory =true;
				categoryName = readXL.brandName;
			}		
		}
		
		
		else
		{
			foundCategory =false;
		}
		categoryName = readXL.brandName;
		
		return foundCategory;
	}
	
	private boolean mainNavigationMouseMove()  throws Exception
	{
		boolean foundCategory = true;
		
		if(isExecuted(getMethodIsExecuted,"Mouse Hover In Main Navigation"))
		{
			String mainCategoryXpath = readXL.getMainBrandXpath(CheckoutTestData.mainBrandName);
			
			if(mainCategoryXpath.isEmpty()||mainCategoryXpath.equalsIgnoreCase(null))
			{
				foundCategory = false;
			}
			else if(isElementPresent(mainCategoryXpath))
			{
				//click(mainCategoryXpath);
				mouseMove(mainCategoryXpath);
			}					
		}	
		return foundCategory;
	}
	
	public boolean productGrid() throws Exception
	{
		boolean foundProductGrid = true;
		if(isElementPresent(checkoutTestData.productGrid))
		{
			click(checkoutTestData.productGrid);
		}
		else
		{
			foundProductGrid =false;
		}
		return foundProductGrid;	
	}
	
	public void resultSheetName(String writeFileName1) throws Exception
	{
		writeFileName = writeFileName1;		
	}
	
	@SuppressWarnings("static-access")
	public void selectProductFromLeftCategory() throws Exception
	{
		if(isElementPresent(checkoutTestData.leftCategory))
		{
			click(checkoutTestData.leftCategory);
		}		
	}
	
	public int totalItemInHeaderCart() throws Exception
	{ 
		int items = 0;
		
		if(isElementPresent(checkoutTestData.myCartXpath))
		{
			String totalCartItems = getText(checkoutTestData.myCartXpath);	
			
			System.out.println("totalCartItems"+totalCartItems);
			
			if(totalCartItems.indexOf("Items")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("")+0,totalCartItems.indexOf("Items")).trim());
			}
			else if(totalCartItems.indexOf("items")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("")+0,totalCartItems.indexOf("items")).trim());
			}
			else if(totalCartItems.indexOf("My Cart (")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("My Cart (")+9,totalCartItems.indexOf(")")).trim());
			}
			else if(totalCartItems.indexOf("My Cart")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("My Cart")+7).trim());
			}
			else if(totalCartItems.indexOf("MY CART")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("MY CART (")+9,totalCartItems.indexOf(")")).trim());
			}
			else if(totalCartItems.indexOf("SHOPPING CART")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("")+0,totalCartItems.indexOf("SHOPPING CART")).trim());
			}
			else if(totalCartItems.indexOf("SHOPPING BAG")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("(")+1,totalCartItems.indexOf(")")).trim());
			}
			else if(totalCartItems.indexOf("CART (")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("CART (")+6,totalCartItems.indexOf(")")).trim());				
			}
			else if(totalCartItems.length() == 1)
			{
				items = Integer.parseInt(totalCartItems);				
			}
			
		}		
		
		System.out.println("items"+items);
		return items;		
	}
	
	public void clearMyCart() throws Exception
	{ 
		if(totalItemInHeaderCart() > 0)
		{
			if(!isDisplayed(CheckoutTestData.viewCart))
			{
				click(CheckoutTestData.viewCartId);
				
				Thread.sleep(2000);
				
				clickAndWait(CheckoutTestData.viewCart);					
			}
			else			
			{
				clickAndWait(CheckoutTestData.viewCart);					
			}
				
			while(isDisplayed(ShoppingCartTestData.rmvItemFromCart))
			{					
				clickAndWait(ShoppingCartTestData.rmvItemFromCart);							
			
				if(isAlertPresent())
				{
					System.out.println("Aleart present");
					acceptAlert();				
							
				}	

					
			}
				
			
			common.clearCookies();
		}
	}
	
	
	
	public void clearMyCartWithoutCookies() throws Exception
	{ 
		if(totalItemInHeaderCart() > 0)
		{
			if(!isDisplayed(CheckoutTestData.viewCart))
			{
				click(CheckoutTestData.viewCartId);
				
				Thread.sleep(2000);
				
				clickAndWait(CheckoutTestData.viewCart);					
			}
			else			
			{
				clickAndWait(CheckoutTestData.viewCart);					
			}
				
			while(isDisplayed(ShoppingCartTestData.rmvItemFromCart))
			{					
				clickAndWait(ShoppingCartTestData.rmvItemFromCart);							
			
				if(isAlertPresent())
				{
					System.out.println("Aleart present");
					acceptAlert();				
							
				}	

					
			}
				
			
		}
	}
	
	
	
	
	@SuppressWarnings("static-access")
	public void navigateToProductDetailsPage() throws Exception
	{
		int noOfColoums = getElementSize(checkoutTestData.noOfProductsPerColoum.replace("{id}", "[1]"));		
		
		i=1;
		
		if(noOfColoums == 0)
		{
			writeTestResults("Verify products available for "+readXL.brandName,"One or more than one products should be available for "+readXL.brandName, "Products are not available for "+readXL.brandName,"",false,true);
		}
		
		while(i <= noOfColoums)
		{
			getProductNameAndPriceInCategorypage(i);
			
			if(isElementPresent(checkoutTestData.getProductAttributes.replace("{id}", "["+i+"]")))
			{
				setProductName = productName+"_Link";
				
				if(isExecuted(getMethodIsExecuted,"Navigate To Product Details Using Xpath"))
				{
					String productUrl = "";				
					
					productUrl = findElementInXLSheet(getParameterXpath,"navigate to product using xpath value").replace("productName", productName);
				
					clickAndWait(productUrl);				
									
				}
				else if(isElementPresent(setProductName))
				{
					String beforeClickUrl ="";
					
					beforeClickUrl = getCurrentUrl();
					
					System.out.println("Product Name is - "+setProductName.replaceAll("_Link", ""));
					
					clickAndWait(setProductName);
					
					if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
					{
						openPage(getAttribute(checkoutTestData.getProductAttributes+"/a","href"));
					}		
				}
				else
				{
					writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","One or more than one products should be available for "+readXL.brandName, "Products are not available for "+readXL.brandName,"",false,true);
				}				
			}
			else
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","One or more than one products should be available for "+readXL.brandName, "Products are not available for "+readXL.brandName,"",false,true);
			}
			
			if(!(!isDisplayed(checkoutTestData.addToCartButton) && !isEnabled(checkoutTestData.addToCartButton)))
			{
				if(!getProductSpPrice.isEmpty())		
				{
					writeTestResults("Verify user can select product in category page", "User can navigate to '"+productName+"' details page", 
							"Selected product name is '"+productName+"' , price is '"+getProductSpPrice+"' and special price is '"+productPrice+"'","",true,false);
					
				}
				else
				{
					writeTestResults("Verify user can select product in category page", "User can navigate to '"+productName+"' details page", 
							"Selected product name is '"+productName+"' and price is '"+productPrice+"'","",true,false);				
				}					
				
				break;					
			}
			else
			{
				if(i != noOfColoums)
				{
					goBack();
				}				
			}			
			i++;
		}
		System.out.println("User navigate to product details page");
	}
	
	
	
	
	@SuppressWarnings("static-access")
	public void navigateToProductDetailsPageDuplicate() throws Exception
	{
		int noOfColoums = getElementSize(checkoutTestData.noOfProductsPerColoum.replace("{id}", "[1]"));		
		
		i=3;
		
		if(noOfColoums == 0)
		{
			writeTestResults("Verify products available for "+readXL.brandName,"One or more than one products should be available for "+readXL.brandName, "Products are not available for "+readXL.brandName,"",false,true);
		}
		
		while(i <= noOfColoums)
		{
			getProductNameAndPriceInCategorypage(i);
			
			if(isElementPresent(checkoutTestData.getProductAttributes.replace("{id}", "["+i+"]")))
			{
				setProductName = productName+"_Link";
				
				if(isExecuted(getMethodIsExecuted,"Navigate To Product Details Using Xpath"))
				{
					String productUrl = "";				
					
					productUrl = findElementInXLSheet(getParameterXpath,"navigate to product using xpath value").replace("productName", productName);
				
					clickAndWait(productUrl);				
									
				}
				else if(isElementPresent(setProductName))
				{
					String beforeClickUrl ="";
					
					beforeClickUrl = getCurrentUrl();
					
					System.out.println("Product Name is - "+setProductName.replaceAll("_Link", ""));
					
					clickAndWait(setProductName);
					
					if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
					{
						openPage(getAttribute(checkoutTestData.getProductAttributes+"/a","href"));
					}		
				}
				else
				{
					writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","One or more than one products should be available for "+readXL.brandName, "Products are not available for "+readXL.brandName,"",false,true);
				}				
			}
			else
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","One or more than one products should be available for "+readXL.brandName, "Products are not available for "+readXL.brandName,"",false,true);
			}
			
			if(!(!isDisplayed(checkoutTestData.addToCartButton) && !isEnabled(checkoutTestData.addToCartButton)))
			{
				if(!getProductSpPrice.isEmpty())		
				{
					writeTestResults("Verify user can select product in category page", "User can navigate to '"+productName+"' details page", 
							"Selected product name is '"+productName+"' , price is '"+getProductSpPrice+"' and special price is '"+productPrice+"'","",true,false);
					
				}
				else
				{
					writeTestResults("Verify user can select product in category page", "User can navigate to '"+productName+"' details page", 
							"Selected product name is '"+productName+"' and price is '"+productPrice+"'","",true,false);				
				}					
				
				break;					
			}
			else
			{
				if(i != noOfColoums)
				{
					goBack();
				}				
			}			
			i++;
		}
		System.out.println("User navigate to product details page");
	}
	
	
	
	@SuppressWarnings("static-access")
	public void getProductNameAndPriceInCategorypage(int i) throws Exception
	{	
		String getProductAttributes = checkoutTestData.getProductAttributes.replace("{id}", "["+i+"]");
		if(isElementPresent(getProductAttributes))
		{
			productName 	= 	getText(getProductAttributes+checkoutTestData.getProductName);			
			
			if(isElementPresent(getProductAttributes+checkoutTestData.hasOriginalPriceTagInCategoryPage))
			{
				productPrice = getText(getProductAttributes+checkoutTestData.getProductPrice+checkoutTestData.hasOriginalPriceTagInCategoryPage);
			}
			else if(isElementPresent(getProductAttributes+checkoutTestData.hasSpecialPriceTagInCategoryPage))
			{
				productPrice = getText(getProductAttributes+checkoutTestData.hasSpecialPriceTagInCategoryPage);
			}
			else
			{
				productPrice = getText(getProductAttributes+checkoutTestData.getProductPrice);
			}
			
			System.out.println("Product name in category "+productName);
			System.out.println("Product price in category "+productPrice);
		}		
	}
	
	public void compareProductDetails(String productPriceInDetailPage) throws Exception
	{
		if(findElementInXLSheet(getMethodIsExecuted,"Product Name In Product Detail Page").equalsIgnoreCase("Yes"))
		{
			productNameInProductDetail();
			productNameCompareInProductDetail();
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Product Price In Product Detail Page").equalsIgnoreCase("Yes"))
		{
			productPriceCompareInProductDetail(productPriceInDetailPage);
		}		
	}
	
	public void getProductNameAndPriceInCategorypage() throws Exception
	{
		if(isElementPresent(checkoutTestData.getProductAttributes))
		{
			productName 	= 	getText(checkoutTestData.getProductAttributes+checkoutTestData.getProductName);
						
			productName 	= 	getText(checkoutTestData.getProductAttributes+checkoutTestData.getProductName);			
			
			if(isElementPresent(checkoutTestData.getProductAttributes+checkoutTestData.getProductPrice+checkoutTestData.hasSpecialPriceTagInCategoryPage))
			{
				productPrice = getText(checkoutTestData.getProductAttributes+checkoutTestData.getProductPrice+checkoutTestData.hasSpecialPriceTagInCategoryPage);
			}
			else
			{
				productPrice = getText(checkoutTestData.getProductAttributes+checkoutTestData.getProductPrice+checkoutTestData.hasOriginalPriceTagInCategoryPage);
			}
					
		}		
	}
		
	public boolean productAvailability() throws Exception
	{	
		boolean productAvailable = true;
			
		int totalItem=0;
		@SuppressWarnings("unused")
		int totalRows;
		int rem = totalItem % 4;			
		int total = totalItem/4;
			
		if(rem>0){
			totalRows = total+1;			
		}
		else 
		{
			totalRows = total;			
		}	
			
		getProductNameAndPriceInCategorypage();
		
		if(isElementPresent(checkoutTestData.getProductAttributes))
		{
			setProductName = productName+"_Link";
			
			if(isExecuted(getMethodIsExecuted,"Navigate To Product Details Using Xpath"))
			{
				String productUrl = "";				
				
				productUrl = findElementInXLSheet(getParameterXpath,"Navigate to product using xpath value").replace("productName", productName);

				clickAndWait(productUrl);
								
			}
			else if(isElementPresent(setProductName))
			{
				String beforeClickUrl ="";
				
				beforeClickUrl = getCurrentUrl();
				
				clickAndWait(setProductName);					
				
				if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
				{
					openPage(getAttribute(checkoutTestData.getProductAttributes+"/a","href"));
				}
				
				if(!getProductSpPrice.equalsIgnoreCase(""))
				{
					productPrice = getProductSpPrice;
				}	
			}
			else
			{
				productAvailable = false;			
			}				
		}
		else
		{
			productAvailable = false;				
		}
		return productAvailable;		
	}
	
	public void selectProductSizeByClick() throws Exception
	{
		int productSizeBasket = getElementSize(checkoutTestData.productSizeClassName);
		String sizeXpath = "";
		
		if(productSizeBasket!=0)
		{
			if(productSizeBasket==1)
			{
				sizeXpath = checkoutTestData.productSizeBlock.replace("{id}", "");
							
				if(!getAttribute(sizeXpath,"id").isEmpty())
				{
					click(sizeXpath);
				}
				else
				{
					System.out.println("Product is out of stok");
				}
			}
			else
			{				
				sizeXpath = checkoutTestData.productSizeBlock.replace("{id}", "");
	
				if(!getAttribute(sizeXpath,"id").isEmpty())
				{
					click(sizeXpath);
				}
				else
				{
			
					for(int i=2;i<=productSizeBasket;i++)
					{
						sizeXpath = checkoutTestData.productSizeBlock.replace("{id}", "["+i+"]");
						System.out.println("sizeXpath"+sizeXpath);
						if(!getAttribute(sizeXpath,"id").isEmpty())
						{
							click(sizeXpath);
						}
						else
						{
							System.out.println("product is out of stock");
						}
					}						
				}			
			}
		}
		else
		{
			System.out.println("No color size busket");
		}		
	}

	@SuppressWarnings("static-access")
	public boolean isAddToCartButtonPresent() throws Exception
	{
		if(!isElementPresent(checkoutTestData.addToCartButton))
		{
			return false;
		}		
		return true;
	}
	
	@SuppressWarnings("static-access")
	public void addToCart(String testCase) throws Exception
	{		
		
		
		if((!isDisplayed(checkoutTestData.addToCartButton) && !isEnabled(checkoutTestData.addToCartButton)))
		{
			if(!checkoutTestData.outOfStockProductMessage.isEmpty())
			{
				verifyOutOfStockMessage(testCase);
			}			
		}
		//Then check this product is group product
		if(!checkoutTestData.groupProductTable.isEmpty())
		{
			System.out.println("Check product type is group");
		}
		if(!checkoutTestData.groupProductTable.isEmpty() && isElementPresent(checkoutTestData.groupProductTable))
		{
			addToCartButtonForGroupProduct();			
		}
		//then check product is config and it has two selection dropdown (color/size)
		else if(!checkoutTestData.productSizeDropdownXpath.isEmpty() && isElementPresent(checkoutTestData.productSizeDropdownXpath) 
				&& !checkoutTestData.productColorDropdownXpath.isEmpty() && isElementPresent(checkoutTestData.productColorDropdownXpath) )
		{
			selectColorAndSizeForConfigProductUsingDropDown();			
		}
		//then check product is config selecting size
		else if(!checkoutTestData.productSizeClassName.isEmpty() && isElementPresent(checkoutTestData.productSizeClassName))
		{
			addToCartButtonForConfigProduct();			
		}
		//then check product is config selecting size using dropdown
		else if(!checkoutTestData.productSizeDropdownXpath.isEmpty() && isElementPresent(checkoutTestData.productSizeDropdownXpath))
		{
			addToCartButtonForConfigProductUsingDropDown();			
		}		
		//Check procuct is gift card
		else if(!checkoutTestData.giftCardSenderNameXpath.isEmpty() && isElementPresent(checkoutTestData.giftCardSenderNameXpath))
		{
			addToCartButtonForGiftCard();		
		}
		//group product
		else if(!checkoutTestData.giftCardSenderNameXpath.isEmpty() && isElementPresent(checkoutTestData.giftCardSenderNameXpath))
		{			
			addToCartButtonForGiftCard();		
		}
		//check simple product
		else
		{	
			System.out.println("Check selected product type is simple");
			
			if(isDisplayed(checkoutTestData.SelectProductSize))
			{
				click(checkoutTestData.SelectProductSize);
			}
			
			
			addToCartButton();
		}
		
		Thread.sleep(5000);
		
		if(isDisplayed("//div[3]/aside[2]/div[2]/header/button"))
		{
			clickAndWait("//div[3]/aside[2]/div[2]/header/button");
		}
	}	
	
	public void verifyOutOfStockMessage(String testCase) throws Exception
	{
		if(isElementPresent(checkoutTestData.outOfStockProductMessage))
		{
			writeTestResults(testCase, "Product should be available", "Product is out of stock following message got it:"+getText(checkoutTestData.outOfStockProductMessage),"",false,true);
				
		}
		else
		{			
			writeTestResults(testCase, "Add to cart button should be displayed in product detail page", "Add to cart button is not displayed for first "+(i-1)+" products in selected category ("+categoryName+") page","",false,true);
		}		
	}
	
	public void selectColorAndSizeForConfigProductUsingDropDown() throws Exception
	{	
		int productColorBasket = getDropdownSize(checkoutTestData.productColorDropdownXpath,checkoutTestData.productDropdownOptionsValue);
				
		System.out.println("productColorBasket"+productColorBasket);
		
		if(productColorBasket!=1)
		{
			/**
			 *Basket has only one value 
			 */
			
			productTitle = getTitle();
			
			System.out.println("productUrl"+productTitle);
			
			if(productColorBasket==2)
			{
				selectIndex(checkoutTestData.productColorDropdownXpath,1);
				
				selectSizeForConfigProductUsingDropDown();						
				
			}
			else
			{
				int i = 1;
		
				while(i<=productColorBasket)
				{
					for(i=1;i<=productColorBasket;i++)
					{					
						selectIndex(checkoutTestData.productSizeDropdownXpath,i);
						
						click(checkoutTestData.addToCartButton);
						
						productDetailAjax();
						
						if(!isErrorElementPresent(errorMsg))
						{
							i = productColorBasket+1;
							
							break;												
						}
						
						if(i==productColorBasket)
						{
							if(isElementPresent(errorMsg))
							{
								writeTestResults("User can add the product in to the shopping cart", "User can select the item from the product", "Following error found :"+getText(errorMsg),"",false,true);
							}
							else if(isDisplayed(checkoutTestData.withoutSelectingSizeErrorXpath))
							{
								writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", "Following error found :"+getText(checkoutTestData.withoutSelectingSizeErrorXpath),"",false,true);
							}
							else
							{
								writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
							}
						}															
					}
				}
			}
			
		}
		else
		{
			writeTestResults("User can add the product in to the shopping cart", "Product has no any items", "Product has no any items","",false,true);
		}	
	}
	
	public void selectSizeForConfigProductUsingDropDown() throws Exception
	{	
		int productSizeBasket = getDropdownSize(checkoutTestData.productSizeDropdownXpath,checkoutTestData.productDropdownOptionsValue);
				
		System.out.println("productSizeBasket"+productSizeBasket);
		
		if(productSizeBasket!=1)
		{
			/**
			 *Basket has only one value 
			 */
			
			productTitle = getTitle();
			
			System.out.println("productUrl"+productTitle);
			
			if(productSizeBasket==2)
			{
				selectIndex(checkoutTestData.productSizeDropdownXpath,1);				
				
				click(checkoutTestData.addToCartButton);
				
				productDetailAjax();
				
				if(isErrorElementPresent(errorMsg))
				{
					writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Following error found :"+getText(errorMsg),"",false,true);
				}
				else if(isDisplayed(checkoutTestData.withoutSelectingSizeErrorXpath))
				{
					writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", "Following error found :"+getText(checkoutTestData.withoutSelectingSizeErrorXpath),"",false,true);
				}
				else					
				{
					writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart",
							"Product is added to shopping cart","",true,false);					
				}				
				
			}
			else
			{
				int i = 1;
		
				while(i<=productSizeBasket)
				{
					for(i=1;i<=productSizeBasket;i++)
					{					
						selectIndex(checkoutTestData.productSizeDropdownXpath,i);
						
						click(checkoutTestData.addToCartButton);
						
						productDetailAjax();
						
						if(!isErrorElementPresent(errorMsg))
						{
							i = productSizeBasket+1;
							
							break;												
						}
						
						if(i==productSizeBasket)
						{
							if(isElementPresent(errorMsg))
							{
								writeTestResults("User can add the product in to the shopping cart", "User can select the item from the product", "Following error found :"+getText(errorMsg),"",false,true);
							}
							else if(isDisplayed(checkoutTestData.withoutSelectingSizeErrorXpath))
							{
								writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", "Following error found :"+getText(checkoutTestData.withoutSelectingSizeErrorXpath),"",false,true);
							}
							else
							{
								writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
							}
						}															
					}
				}
			}
			
		}
		else
		{
			writeTestResults("User can add the product in to the shopping cart", "Product has no any items", "Product has no any items","",false,true);
		}	
	}
	
	@SuppressWarnings("static-access")
	public void addToCartButtonForConfigProductUsingDropDown() throws Exception
	{	
		int productSizeBasket = getDropdownSize(checkoutTestData.productSizeDropdownXpath,checkoutTestData.productDropdownOptionsValue);
				
		System.out.println("productSizeBasket"+productSizeBasket);
		
		if(productSizeBasket!=1)
		{
			/**
			 *Basket has only one value 
			 */
			
			productTitle = getTitle();
			
			System.out.println("productUrl"+productTitle);
			
			if(productSizeBasket==2)
			{
				selectIndex(checkoutTestData.productSizeDropdownXpath,1);
				
				click(checkoutTestData.addToCartButton);
				
				productDetailAjax();
				
				if(isErrorElementPresent(errorMsg))
				{
					writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Following error found :"+getText(errorMsg),"",false,true);
				}
				else if(isDisplayed(checkoutTestData.withoutSelectingSizeErrorXpath))
				{
					writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", "Following error found :"+getText(checkoutTestData.withoutSelectingSizeErrorXpath),"",false,true);
				}
				else					
				{
					writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart",
							"Product is added to shopping cart","",true,false);					
				}				
				
			}
			else
			{
				int i = 1;
		
				while(i<=productSizeBasket)
				{
					for(i=1;i<=productSizeBasket;i++)
					{					
						selectIndex(checkoutTestData.productSizeDropdownXpath,i);
						
						click(checkoutTestData.addToCartButton);
						
						productDetailAjax();
						
						if(!isErrorElementPresent(errorMsg))
						{
							i = productSizeBasket+1;
							
							break;												
						}
						
						if(i==productSizeBasket)
						{
							if(isElementPresent(errorMsg))
							{
								writeTestResults("User can add the product in to the shopping cart", "User can select the item from the product", "Following error found :"+getText(errorMsg),"",false,true);
							}
							else if(isDisplayed(checkoutTestData.withoutSelectingSizeErrorXpath))
							{
								writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", "Following error found :"+getText(checkoutTestData.withoutSelectingSizeErrorXpath),"",false,true);
							}
							else
							{
								writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
							}
						}															
					}
				}
			}
			
		}
		else
		{
			writeTestResults("User can add the product in to the shopping cart", "Product has no any items", "Product has no any items","",false,true);
		}	
	}
	
	@SuppressWarnings("static-access")
	public void addToCartButtonForGroupProduct() throws Exception
	{	
		int getNoOfProducts = getRowCount(checkoutTestData.groupProductTable,"tr");
		String qty = "";
		String qtyAttributeValue = "";
		
		System.out.println("getNoOfProducts"+getNoOfProducts);
		
		if(getNoOfProducts!=0)
		{			
			if(getNoOfProducts == 1)
			{
				//add qty
				
				productPrice = getText(checkoutTestData.groupProductPrice.replace("{id1}", ""));
				productName = getText(checkoutTestData.groupProductName.replace("{id1}", ""));
				
				qty = checkoutTestData.groupProductQty.replace("{id1}", "");
				if(isDisplayed(qty))
				{
					sendKeys(qty,"1");
					
					click(checkoutTestData.addToCartButton);
					
					productDetailAjax();
				}
				
				if(isErrorElementPresent(errorMsg))
				{
					writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Following error found :"+getText(errorMsg),"",false,true);
				}
				else
				{
					writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", 
							"Product is added to shopping cart","",true,false);
					
				}		
				
			}
			else
			{
				i = 1;
				while(i <= getNoOfProducts)
				{
					qty = checkoutTestData.groupProductQty.replace("{id1}", "["+i+"]");
					
					productPrice = getText(checkoutTestData.groupProductPrice.replace("{id1}", "["+i+"]"));
					productName = getText(checkoutTestData.groupProductName.replace("{id1}", "["+i+"]"));
					
					qtyAttributeValue = getAttribute(qty,"name");
					if(isDisplayed(qty))
					{
						sendKeys(qtyAttributeValue+"_Name","1");
						click(checkoutTestData.addToCartButton);
						
						productDetailAjax();
					}
					
					if(isErrorElementPresent(errorMsg))
					{
						writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Following error found :"+getText(errorMsg),"",false,true);
					}
					else
					{
						writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", 
								"Product is added to shopping cart","",true,false);
						break;
					}
					 i++;
				}		
			
			}
		}
		else
		{
			writeTestResults("User can add the product in to the shopping cart", "Product has no any items", "Product has no any items","",false,true);
		}
	
	}
		
	
	@SuppressWarnings("static-access")
	public void addToCartButtonForConfigProduct() throws Exception
	{	
		int productSizeBasket = getElementSize(checkoutTestData.productSizeClassName);
		String sizeXpath = "";
		
		System.out.println("productSizeBasket"+productSizeBasket);
		
		if(productSizeBasket!=0)
		{
			/**
			 *Basket has only one value 
			 */
			
			productTitle =getTitle();
			
			System.out.println("productUrl"+productTitle);
			
			if(productSizeBasket==1)
			{
				sizeXpath = checkoutTestData.productSizeBlock.replace("{id}", "");
				
				if(getAttribute(sizeXpath,checkoutTestData.productSizeAttributeValue).indexOf(checkoutTestData.productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
					
					click(checkoutTestData.addToCartButton);
					
					productDetailAjax();
					
					//Thread.sleep(2000);
						
					/*if(!ajaxCall(ajaxLoader))
					{
						writeTestResults("Verify product is add to the cart", "Product should be add to the cart", "Product is not add to the cart within 100sec","",false,true);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify product is add to the cart");
						Assert.assertEquals("Product should be add to the cart", "Product is not add to the cart within 100sec");
					}*/						
					if(isErrorElementPresent(errorMsg))
					{
						writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Following error found :"+getText(errorMsg),"",false,true);
					}
					else
					{
						writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", 
								"Product is added to shopping cart","",true,false);
						
					}
				}
				else
				{
					writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
				}
			}
			else
			{
				sizeXpath = checkoutTestData.productSizeBlock.replace("{id}", "");				
				
				if(getAttribute(sizeXpath,checkoutTestData.productSizeAttributeValue).indexOf(checkoutTestData.productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);				
					
					click(checkoutTestData.addToCartButton);
					
					productDetailAjax();
					
					//Thread.sleep(2000);
						
					/*if(!ajaxCall(ajaxLoader))
					{
						writeTestResults("Verify product is add to the cart", "Product should be add to the cart", "Product is not add to the cart within 100sec","",false,true);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify product is add to the cart");
						Assert.assertEquals("Product should be add to the cart", "Product is not add to the cart within 100sec");
					}*/						
					if(isErrorElementPresent(errorMsg))
					{
						selectProductFromBasket(productSizeBasket,sizeXpath);
					
					}
					else
					{
						writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", 
								"Product is added to shopping cart","",true,false);
					
					}
					
				}
				else
				{
					
					selectProductFromBasket(productSizeBasket,checkoutTestData.productSizeBlock);												
				}
			
			}
		}
		else
		{
			writeTestResults("User can add the product in to the shopping cart", "Product has no any items", "Product has no any items","",false,true);
		}
	
	}
	
	@SuppressWarnings("static-access")
	public void selectProductFromBasket(int productSizeBasket,String sizeXpath) throws Exception
	{
		int i = 2;
		
		while(i<=productSizeBasket)
		{
			for(i=2;i<=productSizeBasket;i++)
			{
				sizeXpath = checkoutTestData.productSizeBlock.replace("{id}", "["+i+"]");
				
				if(getAttribute(sizeXpath,checkoutTestData.productSizeAttributeValue).indexOf(checkoutTestData.productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
					
					click(checkoutTestData.addToCartButton);
					
					productDetailAjax();
					
					if(!isElementPresent(errorMsg))
					{
						i = productSizeBasket+1;
						
						break;												
					}						
				}
				else
				{
					writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
				}
				
				if(i==productSizeBasket)
				{
					if(isElementPresent(errorMsg))
					{
						writeTestResults("User can add the product in to the shopping cart", "User can select the item from the product", "Following error found :"+getText(errorMsg),"",false,true);
					}
					else
					{
						writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
					}	
				}
			}						
		}
	}
		
	public void addToCartButtonForGiftCard() throws Exception
	{
		@SuppressWarnings("unused")
		
		String productPriceInDetailPage ="";
		
		if(isElementPresent(checkoutTestData.giftCardSenderNameXpath))
		{
			sendKeys(checkoutTestData.giftCardSenderNameXpath,checkoutTestData.giftCardSenderName);
		}
		if(isElementPresent(checkoutTestData.giftCardSenderEmailXpath))
		{
			sendKeys(checkoutTestData.giftCardSenderEmailXpath,checkoutTestData.giftCardSenderEmail);
		}
		if(isElementPresent(checkoutTestData.giftCardRecipentNameXpath))
		{
			sendKeys(checkoutTestData.giftCardRecipentNameXpath,checkoutTestData.giftCardRecipentName);
		}
		if(isElementPresent(checkoutTestData.giftCardRecipentEmailXpath))
		{
			sendKeys(checkoutTestData.giftCardRecipentEmailXpath,checkoutTestData.giftCardRecipentEmail);
		}
		if(isElementPresent(checkoutTestData.giftCardAmountDropdownXpath))
		{
			int dropDownSize = getDropdownSize(checkoutTestData.giftCardAmountDropdownXpath,"option");
			
			if(dropDownSize > 0)
			{
				selectIndex(checkoutTestData.giftCardAmountDropdownXpath,1);
				
				productPriceInDetailPage = getSelectedValueFromDropdown(checkoutTestData.giftCardAmountDropdownXpath,"option",1);				
			}
			else
			{
				writeTestResults("Verify user can select amount in gift card", "Dorpdown should have values", "Amount dropdown doesn't have any values","",false,true);
			}
		}	
		
		if(isElementPresent(checkoutTestData.giftCardAmountTxtXpath))
		{
			sendKeys(checkoutTestData.giftCardAmountTxtXpath,"100.00");
			
			productPriceInDetailPage = "$100.00";
			
		}
		
		productTitle =getTitle();
		
		addToCartButton();
		
		giftCardValidation();
	
	}
	
	public void giftCardValidation() throws Exception
	{		
		if(!(isElementPresent(findElementInXLSheet(getParameterXpath,"My Cart Xpath")) && common.totalItemInHeaderCart() > 0))
		{
			if(!checkoutTestData.giftCardWithoutSenderName.isEmpty())
			{
				if(!getValidationText(checkoutTestData.giftCardWithoutSenderName).isEmpty())
				{
					if(getText(checkoutTestData.giftCardWithoutSenderName).equalsIgnoreCase(checkoutTestData.validationMsgGiftCardWithoutSenderName))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Sender name is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Sender name is empty and message is "+getText(checkoutTestData.giftCardWithoutSenderName),"",false,true);
					}
				}
			}
			
			if(!checkoutTestData.giftCardWithoutSenderEmail.isEmpty())
			{
				if(!getValidationText(checkoutTestData.giftCardWithoutSenderEmail).isEmpty())
				{
					if(getText(checkoutTestData.giftCardWithoutSenderEmail).equalsIgnoreCase(checkoutTestData.validationMsgGiftCardWithoutSenderEmail))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Email is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Email is empty and message is "+getText(checkoutTestData.giftCardWithoutSenderEmail),"",false,true);
					}
				}
			}
			
			if(!checkoutTestData.giftCardWithoutRecipentName.isEmpty())
			{
				if(!getValidationText(checkoutTestData.giftCardWithoutRecipentName).isEmpty())
				{
					if(getText(checkoutTestData.giftCardWithoutRecipentName).equalsIgnoreCase(checkoutTestData.validationMsgGiftCardWithoutRecipentName))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Recipent name is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Recipent name is empty and message is "+getText(checkoutTestData.giftCardWithoutRecipentName),"",false,true);
					}
				}
			}
			
			
			if(!checkoutTestData.giftCardWithoutRecipentEmail.isEmpty())
			{
				if(!getValidationText(checkoutTestData.giftCardWithoutRecipentEmail).isEmpty())
				{
					if(getText(checkoutTestData.giftCardWithoutRecipentEmail).equalsIgnoreCase(checkoutTestData.validationMsgGiftCardWithoutRecipentEmail))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Reciepent Email is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Reciepent Email is empty and message is "+getText(checkoutTestData.giftCardWithoutRecipentEmail),"",false,true);
					}
				}
			}
			
			if(!checkoutTestData.giftCardWithoutDropdownAmount.isEmpty())
			{
				if(!getValidationText(checkoutTestData.giftCardWithoutDropdownAmount).isEmpty())
				{
					if(getText(checkoutTestData.giftCardWithoutDropdownAmount).equalsIgnoreCase(checkoutTestData.validationMsgGiftCardWithoutAmount))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Amount is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Amount is empty and message is "+getText(checkoutTestData.giftCardWithoutDropdownAmount),"",false,true);
					}
				}
			}
			
			if(!checkoutTestData.giftCardWithoutTxtAmount.isEmpty())
			{
				if(!getValidationText(checkoutTestData.giftCardWithoutTxtAmount).isEmpty())
				{
					if(getText(checkoutTestData.giftCardWithoutTxtAmount).equalsIgnoreCase(checkoutTestData.validationMsgGiftCardWithoutAmount))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Amount is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Amount is empty and message is "+getText(checkoutTestData.giftCardWithoutTxtAmount),"",false,true);
					}
				}
			}
		}				
	}
	
	@SuppressWarnings("static-access")
	public boolean addToCartAjaxRequest(String loader) throws Exception
	{
		int i=0;
		boolean isAjaxLoad = true;
		
		if(isElementPresent(loader))
		{	
			 while(!getAttribute(loader,checkoutTestData.addToCartAjaxClassType).equalsIgnoreCase(checkoutTestData.addToCartAjaxClassValue))
			 {	
				 Thread.sleep(200);
			   	 i++;
			   	 
			   	 if(i==50)
			   	 {
			   		isAjaxLoad = false; 
			   		break;
			   	 }	   	 
			 }		 
		}		
		return isAjaxLoad;   
	}
	
	@SuppressWarnings("static-access")
	public void addToCartButton() throws Exception
	{		
		if(!isDisplayed(checkoutTestData.addToCartButton))
		{
			writeTestResults("User can add the product in to the shopping cart", "User can add the product in to the cart", "Add to cart button is not displayed or xpath is incorrect","",false,true);
		}
		else
		{
			productTitle =getTitle();
			
			click(checkoutTestData.addToCartButton);	
			
			productDetailAjax();				
			
			if(isErrorElementPresent(checkoutTestData.productDetailsErrorMsg))
			{
				for(int i=0;i<isErrorMsgDisplayed(checkoutTestData.productDetailsErrorMsg).size();i++)
				{
					if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
					{	
						writeTestResults("User can add the product in to the shopping cart", "User can add the product in to the cart", "Following error found :"+getText(checkoutTestData.productDetailsErrorMsg),"",false,true);
					}
				}					
			}
		}		
	}

	@SuppressWarnings("static-access")
	public void productDetailAjax() throws Exception
	{
		if(checkoutTestData.HasAddToCartAjaxRequest.equalsIgnoreCase("Yes") && !checkoutTestData.addToCartAjax.isEmpty())
		{	
			System.out.println("wait before");
			if(!getAttribute(checkoutTestData.addToCartAjax,checkoutTestData.addToCartAjaxClassType).equalsIgnoreCase(checkoutTestData.addToCartAjaxClassValue))
			{
				if(!addToCartAjaxRequest(checkoutTestData.addToCartAjax))
				{
					writeTestResults("Verify user clicks continue button with correct data in billing information page", "Shipping information screen should be displayed", "Page is not load within 100sec","",false,true);
				}
			}
		}
		else
		{
			System.out.println("wait");
			Thread.sleep(2000);
		}
	}
	
		public void viewCart() throws Exception
	{ 
		if(totalItemInHeaderCart()==0)
		{
			writeTestResults("User can add the product in to the shopping cart", "Header Cart should be updated", "Cart is not updated or add to cart button is not working","",false,true);
		}		
		else if(!isElementPresent(checkoutTestData.viewCartId))
		{
			writeTestResults("Verify the my cart is displayed in header", "My cart should be displayed in header", "My cart button is not displayed in the header","",false,true);
		}
		else
		{							
			if(!isDisplayed(checkoutTestData.viewCart))
			{
				click(checkoutTestData.viewCartId);
								
				if(!isDisplayed(checkoutTestData.viewCart))
				{
					writeTestResults("Verify the goto cart is displayed in header", "Go to cart link should be displayed in header", "Goto cart button is not displayed in the header","",false,true);
				}
				
				clickAndWait(checkoutTestData.viewCart);					
			}
			else if(isDisplayed(checkoutTestData.viewCart))
			{
				clickAndWait(checkoutTestData.viewCart);					
			}			
			else
			{
				writeTestResults("Verify the my cart is displayed in header", "My cart should be displayed in header", "My cart button is not displayed in the header","",false,true);
			}				
		}		
	}
	
	public void clickMiniCart() throws Exception
	{ 
		if(isExecuted(getMethodIsExecuted,"Mini cart has dropdown"))
		{
			if(isDisplayed(checkoutTestData.viewCartId))
			{
				click(checkoutTestData.viewCartId);							
			}
		}		
	}
	
	public void titleInshoppingCartPage(String writeFileName) throws Exception
	{		
		if(!getTitle().equalsIgnoreCase(checkoutTestData.shoppingCartTitle))
		{			
			writeTestResults("Verify the shopping cart page title", "Expected shopping cart title is "+checkoutTestData.shoppingCartTitle, 
					"Actual shopping cart title is "+getTitle(),"",false,false);			
		}
		else
		{
			writeTestResults("Verify the shopping cart page title", "Expected shopping cart title is "+checkoutTestData.shoppingCartTitle, 
					"Actual shopping cart title is "+getTitle(),"",true,false);
		}		
	}
	
	public void verifyGrandTotalAfterRedeemForFixed(double grandTotalBeforeEnterCoupon) throws Exception 
	{
		if(isElementPresent(ShoppingCartTestData.grandTotalXpathAfterDiscount))
		{
			String grandTotalStringValue = getText(ShoppingCartTestData.grandTotalXpathAfterDiscount);
			
			double grandTotal  = discountTypeIsFixed(grandTotalBeforeEnterCoupon);
			
			System.out.println("grandTotal"+grandTotal);
			
			int retval = Double.compare(grandTotal, common.getValueWitoutCurrency(grandTotalStringValue));	
					 
			if(retval == 0)
			{
				 writeTestResults("Verify total value updates after redeem the coupon when coupon type is 'fixed amount discount'", 
						 grandTotal+" amount should be displayed.",grandTotal+" amount is displayed.","",true,false);				
			}
			else
			{
				 writeTestResults("Verify total value updates after redeem the coupon when coupon type is 'fixed amount discount'", grandTotal+" " +
				 		"amount should be displayed.",getText(ShoppingCartTestData.grandTotalXpathAfterDiscount)+" amount is displayed.","",false,true);
			}
			
		}
		else
		{
			 writeTestResults("Verify total value updates after redeem the coupon when coupon type is 'fixed amount discount'", 
					 "Total amount xpath should be displayed.","Total amount xpath is not displayed.","",false,true);
		}		
	}
	
	private double discountTypeIsFixed(double grandTotalBeforeEnterCoupon) throws Exception 
	{	
		DecimalFormat convertToDecimal = new DecimalFormat("###.##");
		
		return (Double.valueOf(convertToDecimal.format(grandTotalBeforeEnterCoupon - Double.parseDouble(findElementInXLSheet(getParameterXpath,"coupon code amount")))));		
	}
	
	public String productPriceInshoppingCartPage() throws Exception
	{		
		if(isElementPresent(checkoutTestData.productPriceInShoppingCartForGuest))
		{
			if(getTagListSize(checkoutTestData.productPriceInShoppingCartForGuest,checkoutTestData.tagNameForSpecialPrice)>1)
			{
				productPriceInShoppingCartPage 	= 	getText(checkoutTestData.productPriceInShoppingCartForGuest+"/p[2]");
			}
			else
			{
				productPriceInShoppingCartPage = getText(checkoutTestData.productPriceInShoppingCartForGuest);
			}	
		}
		
		return productPriceInShoppingCartPage;
		
	}
	
	public String productSubTotalInshoppingCartPage() throws Exception
	{		
		if(isElementPresent(checkoutTestData.productSubTotalInShoppingCart))
		{
			productSubTotalInShoppingCartPage = getText(checkoutTestData.productSubTotalInShoppingCart);	
		}
		
		return productSubTotalInShoppingCartPage;
		
	}
	
	public String productUnitPriceInshoppingCartPage() throws Exception
	{		
		if(isElementPresent(checkoutTestData.productPriceInShoppingCartForGuest))
		{
			if(getTagListSize(checkoutTestData.productPriceInShoppingCartForGuest,checkoutTestData.tagNameForSpecialPrice)>1)
			{
				productPriceInShoppingCartPage 	= 	getText(checkoutTestData.productPriceInShoppingCartForGuest+"/p[2]");
			}
			else
			{
				productPriceInShoppingCartPage = getText(checkoutTestData.productPriceInShoppingCartForGuest);
			}
		}
		return productPriceInShoppingCartPage;
	}
	
	public void productPriceInshoppingCartPageForGuest(String productPrice,String  writeFileName) throws Exception
	{		
		if(isElementPresent(checkoutTestData.productPriceInShoppingCartForGuest))
		{
			
			productPriceInShoppingCartPage = getText(checkoutTestData.productPriceInShoppingCartForGuest);
			
			
			System.out.println("productPriceInShoppingCartPage"+checkoutTestData.productPriceInShoppingCartForGuest);
			if(getValueWitoutCurrency(productPriceInShoppingCartPage) != getValueWitoutCurrency(productPrice))
			{
				//chk discount price displayed in grand total 
				if(getValueWitoutCurrency(getText(ShoppingCartTestData.grandTotalXpathBeforeDiscount)) != getValueWitoutCurrency(productPrice))
				{
						writeTestResults("Verify the product price in the shopping cart page ", "Expected product price is "+productPrice + " in the shopping cart page",
								"Actual product price is "+getText(ShoppingCartTestData.grandTotalXpathBeforeDiscount)+" in the shopping cart page","426faae6",false,false);
					
				}
				else
				{
					writeTestResults("Verify the product price in the shopping cart page ", "Expected product price is "+productPrice + " in the shopping cart page",
							"Actual product price is "+getText(ShoppingCartTestData.grandTotalXpathBeforeDiscount)+" in the shopping cart page","426faae6",true,false);
				}
				
				
			}
			else
			{
				writeTestResults("Verify the product price in the shopping cart page ", "Expected product price is "+productPrice + " in the shopping cart page", 
						"Actual product price is "+productPriceInShoppingCartPage+" in the shopping cart page","426faae6",true,false);				
			}			
			
		}
		else
		{
			writeTestResults("Verify the product price in the shopping cart page ", "Expected product price is "+productPrice + " in the shopping cart page", 
					"Product price xpath not displayed or xpath "+checkoutTestData.productPriceInShoppingCartForGuest+" is incorrect","426faae6",false,false);				
		}	
		productGrandTotalInshoppingCartPage();
	}
	
	public void productPriceInshoppingCartPageForMember(String productPrice,String  writeFileName) throws Exception
	{
		if(isElementPresent(checkoutTestData.productPriceInShoppingCartForMember))
		{
			if(getTagListSize(checkoutTestData.productPriceInShoppingCartForMember,checkoutTestData.tagNameForSpecialPrice)>1)
			{
				productPriceInShoppingCartPage 	= 	getText(checkoutTestData.productPriceInShoppingCartForMember+"/p[2]");
			}
			else
			{
				productPriceInShoppingCartPage = getText(checkoutTestData.productPriceInShoppingCartForMember);
			}
			if(getValueWitoutCurrency(productPriceInShoppingCartPage) != getValueWitoutCurrency(productPrice))
			{
				writeTestResults("Verify the product price in the shopping cart page ", "Expected product price is "+productPrice + " in the shopping cart page", 
						"Actual product price is "+productPriceInShoppingCartPage+" in the shopping cart page","426faae6",false,false);
			}
			else
			{
				writeTestResults("Verify the product price in the shopping cart page ", "Expected product price is "+productPrice + " in the shopping cart page", 
						"Actual product price is "+productPriceInShoppingCartPage+" in the shopping cart page","426faae6",true,false);
			}
			
			productGrandTotalInshoppingCartPage();
		}		
	}
	
	public double productGrandTotalInshoppingCartPage() throws Exception
	{		
		if(isElementPresent(ShoppingCartTestData.grandTotalXpathBeforeDiscount))
		{
			grandTotalInShoppingcart = getValueWitoutCurrency(getText(ShoppingCartTestData.grandTotalXpathBeforeDiscount));		
		}
		else if(isElementPresent(ShoppingCartTestData.grandTotalXpathAfterDiscount))
		{
			grandTotalInShoppingcart = getValueWitoutCurrency(getText(ShoppingCartTestData.grandTotalXpathAfterDiscount));	
		}
		
		System.out.println("Grand total in shoppingcart"+grandTotalInShoppingcart);
		
		return grandTotalInShoppingcart;
	}
	
	public void productNameInshoppingCartPage(String productName ,String writeFileName) throws Exception
	{
		System.out.println("Product name in shopping cart page "+productName);
		if(isElementPresent(checkoutTestData.productNameInShoppingCart))
		{
			productNameInShoppingCartPage = getText(checkoutTestData.productNameInShoppingCart);
			
			if(!productNameInShoppingCartPage.isEmpty())
			{
				if(!productNameInShoppingCartPage.equalsIgnoreCase(productName))
				{
					writeTestResults("Verify the product name in the shopping cart page", "Expected product name is "+productName+ " in the shopping cart page", 
							"Actual product name is "+productNameInShoppingCartPage+ " in the shopping cart page","426faae6",false,false);
				}
				else
				{
					writeTestResults("Verify the product name in the shopping cart page", "Expected product name is "+productName+ " in the shopping cart page",
							"Actual product name is '"+productNameInShoppingCartPage+ "' in the shopping cart page","426faae6",true,false);
					
				}				
		
			}
			else
			{
				writeTestResults("Verify the product name in the shopping cart page", "Expected product name is "+productName+ " in the shopping cart page", 
						"Xpath is incorrect","",false,false);				
			}			
		}
	}

	public String productPriceInProductDetail() throws Exception
	{
		if(isElementPresent(checkoutTestData.productPriceXpathInProductDetailPage))
		{
			if(isElementPresent(checkoutTestData.productPriceXpathInProductDetailPage+checkoutTestData.hasSpecialPriceTagInDetailsPage))
			{
				productPriceInDetailPage = getText(checkoutTestData.productPriceXpathInProductDetailPage+checkoutTestData.hasSpecialPriceTagInDetailsPage);				
			}
			else
			{				
				productPriceInDetailPage = getText(checkoutTestData.productPriceXpathInProductDetailPage+checkoutTestData.hasPriceTagInDetailsPage);						
			}	
		}
		else
		{
			if(productPriceInDetailPage.isEmpty())
			{
				productPriceInDetailPage = "";	
			}					
		}	
		return productPriceInDetailPage;	
		
	}
	
	public void compareProductDetails_old(String productPriceInDetailPage) throws Exception
	{
		if(findElementInXLSheet(getMethodIsExecuted,"Product Name In Product Detail Page").equalsIgnoreCase("Yes"))
		{
			productNameInProductDetail();
			productNameCompareInProductDetail();
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Product Price In Product Detail Page").equalsIgnoreCase("Yes"))
		{
			productPriceCompareInProductDetail(productPriceInDetailPage);
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Product QTY In Product Detail Page").equalsIgnoreCase("Yes"))
		{
			//ckProcess.productQTYInProductDetail();
			//ckProcess.productQTYCompareInProductDetail();
		}
	}
	
	public void productNameInProductDetail() throws Exception
	{
		if(isElementPresent(checkoutTestData.productNameXpathInProductDetailPage))
		{
			productNameInDetailPage = getText(checkoutTestData.productNameXpathInProductDetailPage);
		}
		else
		{
			productNameInDetailPage = "";
		}			
	}
	
	public void productNameCompareInProductDetail() throws Exception
	{		
		if(!productNameInDetailPage.equalsIgnoreCase(productName)){
			
			writeTestResults("Verify the name of the selected product is displaying in product detail page", "Expected product Name is "+productName +" " +
					"in product detail page", "Actual product name is "+productNameInDetailPage+" in product detail page","",false,false);			
		}
		else
		{
			writeTestResults("Verify the name of the selected product is displaying in product detail page", "Expected product Name is "+productName+" " +
					"in product detail page" , "Actual product name is "+productNameInDetailPage+" in product detail page","",true,false);			
		}

	}
	
	public void productPriceCompareInProductDetail(String productPriceInDetailPage) throws Exception
	{
		if(getValueWitoutCurrency(productPriceInDetailPage) == getValueWitoutCurrency(productPrice))
		{	
			writeTestResults("Verify the price of the selected product is displaying in product detail page", "Expected product price is "+productPrice +" " +
					"in product detail page", "Actual product price is "+productPriceInDetailPage+" in product detail page","",true,false);			
		}
		else
		{
			writeTestResults("Verify the price of the selected product is displaying in product detail page", "Expected product price is "+productPrice +"" +
					" in product detail page", "Actual product price is "+productPriceInDetailPage+" in product detail page","",false,false);
		}
	}	
	
	public double getValueWitoutCurrency(String priceStringValue) throws Exception 
	{		
		String convertToString = "";
		double priceConvertToDouble = 0.0;
		
		if(priceStringValue.indexOf(",") >= 0)
		{
			priceStringValue = priceStringValue.replaceAll(",", "");
			
		}
		
		if(priceStringValue.indexOf("AUD") >= 0)
		{
			convertToString = priceStringValue.substring(priceStringValue.indexOf("AUD")+3);
			
		}
		else if(priceStringValue.indexOf("AU$") >= 0)
		{
			convertToString = priceStringValue.substring(priceStringValue.indexOf("AU$")+3);
			
		}
		else if(priceStringValue.indexOf("Starting at:") >= 0)
		{
			convertToString = priceStringValue.substring(priceStringValue.indexOf("Starting at:")+12);
		}
		else
		{
			convertToString = priceStringValue.substring(priceStringValue.indexOf("$")+1);
		}		
		
		if(convertToString.indexOf("$") >= 0)
		{
			convertToString = priceStringValue.substring(priceStringValue.indexOf("$")+1);
		}
		if(!convertToString.isEmpty())
		{
			priceConvertToDouble = Double.parseDouble(convertToString);
		}
		
		return priceConvertToDouble;
		
	}
	
	public void updateDatabase(String email)throws Exception
	{		
		 db.getConnection();
		 Random rand = new Random(); 
		 rand.nextInt(40); 	
		
	    String sql = "UPDATE customer_entity SET email = ? WHERE email =?";
		try{
			 java.sql.PreparedStatement st = db.getConnection().prepareStatement(sql);
			 st.setString(1, "test"+rand.nextInt(5000)+"@test.com");
			 st.setString(2, email);
			 st.executeUpdate();			 
			 st.close(); 
		}
		catch (SQLException e) 
		{
			writeTestResults("SQL ERROR", "SQL ERROR", "Error Message is "+ e.getMessage(),"",false,true);
			
			e.printStackTrace();
		  }	
	}
		
	public void getQueryResults(String pageName)throws Exception 
	{		
		String queryString ="";
		String maxQuery = "";
		String remainingString = "";
		String maxQueryTime = "";
		String totalMageTime = "";
		
		/*pageNameOrUrl.clear();
		QueryTime.clear();
		maxQueryResults.clear();
		totalQurey.clear();
		mageTime.clear();*/
		
		//totalTimecell2.add(getText("//*[@id='profiler_section']/table/tbody/tr["+i+"]/td[2]_Xpath"));
		
		if(isElementPresent("//*[@id='profiler_section']/table/tbody/tr[2]/td[2]_Xpath"))
		{
			//mageTime.add(getText("//*[@id='profiler_section']/table/tbody/tr[2]/td[2]_Xpath"));
			
			totalMageTime = getText("//*[@id='profiler_section']/table/tbody/tr[2]/td[2]_Xpath");
		}
		else
		{
			//mageTime.add("Profiler not enable in magento source");
			
			totalMageTime = "Profiler not enable in magento source";
		}
		
		if(isElementPresent("//*[@id='profiler_section']/pre[2]_Xpath"))
		{
			queryString = getText("//*[@id='profiler_section']/pre[2]_Xpath");
		  
			maxQuery = queryString.substring(queryString.indexOf("Longest query:"));
			remainingString = queryString.substring(0, queryString.indexOf("Longest query: "));			
			maxQueryTime = queryString.substring(queryString.indexOf("Longest query length: ")+22, queryString.indexOf("Longest query: "));	
		
			System.out.println("maxQuery"+maxQuery);
			System.out.println("remaining"+remainingString);
		
			/*maxQueryResults.add(maxQuery);
			totalQurey.add(remainingString);
			QueryTime.add(maxQueryTime);*/
		}
		else
		{
			/*maxQueryResults.add("Sql results not displayed");
			totalQurey.add("Sql results not displayed");*/
			
			maxQuery = "Sql results not displayed";
			remainingString = "Sql results not displayed";
			
		}
		
			
		//pageNameOrUrl.add(pageName);
		
		//wResult.writeTestResult1(pageNameOrUrl,QueryTime,mageTime ,totalQurey,maxQueryResults, "Magento Profiler Results");
		
		wResult.writeTestResultForProfiler(pageName,maxQueryTime,totalMageTime ,remainingString,maxQuery, "Magento Profiler Results");
			
		if(pageName.length() > 40)
		{
			pageName = pageName.substring(0, 40);
		}
		
		captureScreenShot(wResult.readTime+"-MP-"+pageName);				
		
	}
	
	public void magentoProfilerResults(String pageName)throws Exception 
	{		
		if(isExecuted(isScenarioExecuted,"Page Performance Using Magento Profiler"))
		{
			getQueryResults(pageName);				
		}
	}
	
	private void navigateToAdmin()throws Exception 
	{	
		String adminUrlWithAuthantication = "";
		
		if(!appUserName.isEmpty() && !appPassword.isEmpty())
		{			
			if(!appUserName.isEmpty() && !appPassword.isEmpty())
			{
				if(adminUrl.indexOf("https://") > 0 )
				{
					adminUrlWithAuthantication = adminUrl.replaceAll("https://","https://"+appUserName+":"+appPassword+"@");
				}
				else
				{
					adminUrlWithAuthantication = adminUrl.replaceAll("http://","http://"+appUserName+":"+appPassword+"@");
				}
			}
			else
			{
				adminUrlWithAuthantication = adminUrl;
			}
		}
		else
		{
			adminUrlWithAuthantication = adminUrl;
		}
		
		openPage(adminUrlWithAuthantication);
		
		System.out.println("Navigate to admin");
		
		if(!adminUserName.isEmpty() && !adminPw.isEmpty())
		{
			sendKeys("//*[@id='username']",adminUserName);
			
			sendKeys("//*[@id='login']",adminPw);
			
			Thread.sleep(5000);
			
			clickAndWait("//*[@id='login-form']/fieldset/div[3]/div[1]/button");
			
			Thread.sleep(5000);                              
			
			driver.navigate().refresh();
			
			Thread.sleep(5000);
		
			    
			 
		
			if(!isElementPresent(customerLink))
			{
				writeTestResults("User can login to admin account and change exist email address", "User can login to admin account", "User can't login or customer link is incorrect","",false,false);
			}
		}		
		
	}
	
	public void updateUserAccountInAdmin(String userName)throws Exception 
	{	
		navigateToAdmin();
		
		updateUserAccount(userName);		
	}
	
	public void updateNewsletterUserAccountInAdmin(String userName)throws Exception 
	{	
		navigateToAdmin();
		
		updateNewsletterUserAccount(userName);		
	}
	
	private void updateNewsletterUserAccount(String userName)throws Exception 
	{	
		//int i = 0;		
		
		mouseMove(navNewsLetterLink);
		
		Thread.sleep(2000);
		
		if(isDisplayed(newsLetterLink))
		{
			clickAndWait(newsLetterLink);	
		}
		else
		{
			writeTestResults("User can login to admin account and change exist email address", "customer button should be displayed", 
					"Search button or xpath is incorrect","",false,false);
		}		
		
		String searchButton = findElementInXLSheet(getParameterXpath,"admin search button");
		
		if(isDisplayed(searchButton))
		{
			sendKeys("subscriberGrid_filter_email_Id",userName);
						
			clickAndWait(searchButton);		
			
			adminAjax();
			
			if(!getPageSource("No records found."))
			{
				unsubscribeAccount(userName);
			}
		}
		else
		{
			writeTestResults("User can login to admin account and change exist email address", "Search button should be displayed", 
					"Search button or xpath is incorrect","",false,false);
		}			
	}
	
	private void updateUserAccount(String userName)throws Exception 
	{	
		Thread.sleep(5000);
		
		click(navCustomerLink);
		
		Thread.sleep(5000);
		
		if(isDisplayed(customerLink))
		{
			Thread.sleep(5000);
			clickAndWait(customerLink);	
			
		
			driver.navigate().refresh();
			 
			Thread.sleep(5000);
			
			clickAndWait("//*[@id='container']/div/div[2]/div[1]/div[3]/div/button");
			
			
			
			
			String searchButton = "//*[@id='container']/div/div[2]/div[1]/div[5]/div/div/button[2]";
				
			
			
			do {
		        Thread.sleep(2000);
				click("//button[@class='action-remove']");
		        
		      }while( isDisplayed("//button[@class='action-remove']/span") );
			
			
			if(isDisplayed(searchButton))
			{
				
				userName = userName.substring(0, userName.indexOf("@"));
				
				
				//sendKeys("//div[@class='admin__form-field-control']//*[@name='email']",userName);
				
				sendKeys("//input[@id='data-grid-search']", userName);
				clickAndWait("//button[@class='action-submit']");
				//clickAndWait(searchButton);	
				
				
				Thread.sleep(3000);
				adminAjax();
	
				
				if(!getPageSource("We couldn't find any records."))
				{
					
					deleteAccount();
					//editAccount(userName);
				}
			}
			else
			{
				writeTestResults("User can login to admin account and change exist email address", "Search button should be displayed", 
						"Search button or xpath is incorrect","",false,false);				
			}			
		}
		clearCookies();
		
		openPage(siteUrl);
	}
	
	private void adminAjax()throws Exception 
	{
		int i = 0;
		if(isDisplayed("loading-mask_Id"))
		{
			 while(isDisplayed("loading-mask_Id"))
			 {
				 Thread.sleep(300);
			   	 //System.out.println("i"+i);
			   	 i++; 
			   	 if(i==100)
			   	 {
			   		break;
			   	 }
			 }
		}
		else
		{
			Thread.sleep(5000);
		}
	}
	
	private void editAccount(String userName)throws Exception 
	{	
		Random randomGenerator = new Random();
		 
		int randomInt = randomGenerator.nextInt(1000000);
		
		if(isDisplayed("Edit_Link")) // edit link of the customer list
		{
			click("Edit_Link");
			
			if(!isDisplayed("#tab_customer>span_Css")) // Account information tab 
			{
				writeTestResults("User can login to admin account and change exist email address", "Edit email txt should be displayed", 
						"Edit email txt not displayed or xpath is incorrect","",false,false);				
			}
			else
			{
				clickAndWait("#tab_customer>span_Css");	// Account information tab 
				
				if(!isDisplayed("//div[@class='admin__control-addon']//input[@type='email']")) // email text field in Account information tab
				{
					writeTestResults("User can login to admin account and change exist email address", "Edit email txt should be displayed",
							"Edit email txt not displayed or xpath is incorrect","",false,false);
				}
				else
				{
					
					
					sendKeys("//div[@class='admin__control-addon']//input[@type='email']","test"+randomInt+"test@netstarter.com"); // replace the email with random email address
					
					//JavascriptExecutor js = ((JavascriptExecutor)driver);
			        //   js.executeScript("document.getElementByXpath(\"//div[@class='admin__control-addon']//input[@type='email']\").setAttribute('inputName', 'test@test.com')");
	
					
					
					Thread.sleep(300);
					adminAjax();
				
					
					if(!isDisplayed("//*[@id='save")) // Save customer 
					{
						writeTestResults("User can login to admin account and change exist email address", "Save button should be displayed in edit infor page", 
								"Save button or xpath is incorrect","",false,false);
					}
					else
					{
						
						clickAndWait("//*[@id='save']");
						
						adminAjax();
					}
				}			
			}			
		}
		else
		{
			writeTestResults("User can login to admin account and change exist email address", "Edit button should be displayed", 
					"Edit button or xpath is incorrect","",false,false);
		}		
		clearCookies();
		
	}
	
	private void deleteAccount()throws Exception 
	{
		Thread.sleep(5000);
		if(!isDisplayed("//td[@class='data-grid-checkbox-cell']//input[@type='checkbox']")) 	
		{
		
			System.out.println("check box not displaying the recode");
			writeTestResults("Verify Admin User can delete a User Account from Mgento admin", "Admin User should be delete a User Account from Mgento admin", 
					"Admin User can not delete a User Account from Mgento admin","",false,true);
		
		}
		
		else 
		{
			
					
			click("//td[@class='data-grid-checkbox-cell']//input[@type='checkbox']");
			System.out.println("check the recode");
			
		}
		
		Thread.sleep(5000);
		
		if(!isDisplayed("//*[@id='container']/div/div[2]/div[2]/div[1]/div/button")) 	
			{
			System.out.println("Dropdoun not displaying to select delete option");
			writeTestResults("Verify Admin User can delete a User Account from Mgento admin", "Admin User should be delete a User Account from Mgento admin", 
					"Admin User can not delete a User Account from Mgento admin","",false,true);
		
			}
		
		else 
		{			
			click("//*[@id='container']/div/div[2]/div[2]/div[1]/div/button");
			click("//*[@id='container']/div/div[2]/div[2]/div[1]/div/div/ul/li[1]/span");
			System.out.println("Deleting the checked the recode.......");
		}
	
		Thread.sleep(5000);
		if(!isDisplayed("//*[@id='html-body']/div[4]/aside[2]/div[2]/footer/button[2]")) 	
			{
			writeTestResults("Verify Admin User can delete a User Account from Mgento admin", "Admin User should be delete a User Account from Mgento admin", 
					"Admin User can not delete a User Account from Mgento admin","",false,true);
			
			}

		else {
			Thread.sleep(5000);
			click("//*[@id='html-body']/div[4]/aside[2]/div[2]/footer/button[2]");
		
		writeTestResults("Verify Admin User can delete a User Account from Mgento admin", "Admin User should be delete a User Account from Mgento admin", 
		"Admin User can delete a User Account from Mgento admin","",true,false);
			
			}
	
		clearCookies();
	}	
	

	private void unsubscribeAccount(String userName)throws Exception 
	{	
		if(isDisplayed("subscriber_Name"))
		{
			click("subscriber_Name");
			
			if(!isDisplayed("subscriberGrid_massaction-select_Id"))
			{
				writeTestResults("User can login to admin account and change exist email address", "Edit email txt should be displayed",
						"Edit email txt not displayed or xpath is incorrect","",false,false);
			}
			else
			{
				selectText("subscriberGrid_massaction-select_Id","Unsubscribe");
			
			}
						
			if(!isDisplayed("//*[@id='subscriberGrid_massaction-form']/fieldset/span[4]/button_Xpath"))
			{
				writeTestResults("User can login to admin account and change exist email address", "Save button should be displayed in edit infor page", 
						"Save button or xpath is incorrect","",false,false);
			}
			else
			{
				clickAndWait("//*[@id='subscriberGrid_massaction-form']/fieldset/span[4]/button_Xpath");
			}
			
		}
		else
		{
			writeTestResults("User can login to admin account and change exist email address", "Edit button should be displayed", 
					"Edit button or xpath is incorrect","",false,false);
		}		
		clearCookies();		
	}
	
	public String totalTime(Date startTime,Date endTime) throws Exception
	{		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
		SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss.SSS");
    	 	
    	SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SSS");
    	
    	java.util.Date date1 = format.parse(sdf.format(startTime));
    	java.util.Date date2 = format.parse(sdf1.format(endTime));
    	
    	long diff = date2.getTime() - date1.getTime();
    	
	    long diffMiliSeconds = diff % 1000; 
	    long diffSeconds = diff / 1000 % 60;  
	    long diffMinutes = diff / (60 * 1000) % 60;       
	    long diffHours = diff / (60 * 60 * 1000);  
	    
	  //  DecimalFormat df = new DecimalFormat("#,###,##0.000");
	       
	    String totalTime =diffSeconds+"."+diffMiliSeconds;
	    
	    if(diffHours!=0)
	    {
	    	totalTime =  diffHours+" hr " +diffMinutes+" min "+ diffSeconds+ " sec";
	    }
	    else
	    {
	    	totalTime =  diffMinutes+" min "+ diffSeconds+ " sec";
	    }   
	    return totalTime;		
	}
	
	public void navigatToCategoryPage() throws Exception
	{
		clearMyCart();
				
		clickLogo();
			
		if(!common.selectBrand())
		{
			 writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", "User can select the product", "Selected brand is not dispalyed or page is not loaded properly" ,"",false,true);
		}
				
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("No"))
		{
			common.selectProductFromLeftCategory();			
		}	
				
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			if(!common.productGrid())
			{
				 writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","Grid link should be present","Link is not displayed or xpath ("+checkoutTestData.productGrid+") is incorrect","",false,true);
			}
		}
		
		System.out.println("User navigate to the category page");
	}
	
	public void navigatToCategoryPageInShoppingCart() throws Exception
	{
		clearMyCart();
				
		clickLogo();
			
		if(!common.selectBrand())
		{
			 writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", "User can select the product", "Selected brand is not dispalyed or page is not loaded properly" ,"",false,true);
		}
				
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
		{
			common.selectProductFromLeftCategory();			
		}	
				
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			if(!common.productGrid())
			{
				 writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","Grid link should be present","Link is not displayed or xpath ("+checkoutTestData.productGrid+") is incorrect","",false,true);
			}
		}
		
		System.out.println("User navigate to the category page");
	}
	
	
	public boolean urlStatus(String siteUrl)throws Exception
	{
		boolean hasSuccessResponse = false;
		
		login = appUserName + ":" + appPassword;		
		
		if(isExecuted(getParameterXpath,"enabled proxy settings"))
		{
			System.setProperty("http.proxyHost", "192.168.10.4");
			System.setProperty("http.proxyPort", "3128");
		}		
		
		if(!appUserName.isEmpty() || !appPassword.isEmpty())
		{
			base64login = new String(Base64.encodeBase64(login.getBytes()));
			
			try
			{
				getResponseStatus = Jsoup.connect(siteUrl).header("Authorization", "Basic " + base64login).timeout(1800*1000).ignoreHttpErrors(true).userAgent("Mozilla").followRedirects(true).execute().statusCode();
				hasSuccessResponse = true;
			}
			catch(Exception e)
			{
				responseError = e.getMessage();
				System.out.println(responseError);
				hasSuccessResponse = false;
			}				
		}
		else
		{
			try
			{
				getResponseStatus = Jsoup.connect(siteUrl).maxBodySize(100000000).timeout(1800*1000).ignoreHttpErrors(true).userAgent("Mozilla").followRedirects(true).execute().statusCode();
				hasSuccessResponse = true;
			}
			catch(Exception e)
			{
				responseError = e.getMessage();
				System.out.println(responseError);
				hasSuccessResponse = false;
			}
		}			
		return hasSuccessResponse;		
	}
	
}

