package com.net.test.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;

public class WriteResultsToUJ 
{
	public static ReadXl readXL = new ReadXl();
	public HashMap<String, String> getParameterXpath = new HashMap<String, String>();

	public static String filePath ="";
	public HSSFWorkbook workBook = null;
	
	public void mainParameters()throws Exception 
	{	
		getParameterXpath=readXL.getParametersXpath();	
		filePath = ReadXl.uJFolder+ findElementInXLSheet(getParameterXpath,"user journey document name");
	}
	
	public static String findElementInXLSheet(HashMap<String, String> mapName,String parameterName)throws Exception
	{
		String parameterValue = "";
		if(mapName.containsKey(parameterName))
		{
			parameterValue =  mapName.get(parameterName).trim(); 
			
		}
		
		return parameterValue ;
	
	}
	public void writeTestResultToUJ(String gUID,boolean status) throws Exception 
    {
		mainParameters();
   	
		int rowNum = getRowNumber(gUID);
		
    	int coloumNum =  Integer.parseInt(findElementInXLSheet(getParameterXpath,"user journey document updated column"));
    	if(rowNum != 0)
    	{
    		updateUJ(rowNum,coloumNum,status);
    	}
    	else
    	{
    		System.out.println(gUID +" is not displayed in the document");
    	}   	        
               
    }
    
    public void updateUJ(int rowNum,int coloumNum,boolean status) throws Exception
    {
    	FileInputStream fsIP= new FileInputStream(new File(filePath)); //Read the spreadsheet that needs to be updated
        
    	workBook = new HSSFWorkbook(fsIP); //Access the workbook
         
        HSSFSheet worksheet = workBook.getSheetAt(0); //Access the worksheet, so that we can update / modify it.
         
        Cell cell = null; // declare a Cell object
       
        cell = worksheet.getRow(rowNum).getCell(coloumNum);   // Access the second cell in second row to update the value
       
       	if(status)
        {        	     	
           	HSSFCellStyle style = workBook.createCellStyle();
           	HSSFFont font = workBook.createFont(); 
           	style.setBorderTop((short) 1);
            style.setBorderBottom((short) 1); 
            style.setBorderLeft((short) 1);
            style.setBorderRight((short) 1);
            cell.setCellStyle(style);
            font.setColor((short) 1);
            style.setFillForegroundColor(HSSFColor.GREEN.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cell.setCellValue("Pass");    
        }
        else
        {       	 	 	
        	HSSFCellStyle style = workBook.createCellStyle();
          	HSSFFont font = workBook.createFont(); 
          	style.setBorderTop((short) 1);
            style.setBorderBottom((short) 1); 
            style.setBorderLeft((short) 1);
            style.setBorderRight((short) 1);
            cell.setCellStyle(style);
            font.setColor((short) 1);
            style.setFillForegroundColor(HSSFColor.RED.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            cell.setCellValue("Fail");      
          	
         }
              
         // Get current cell value value and overwrite the value
         
        fsIP.close(); //Close the InputStream
        
        FileOutputStream output_file =new FileOutputStream(new File(filePath));  //Open FileOutputStream to write updates
         
        workBook.write(output_file); //write changes
         
        output_file.close();  //close the stream 
            
    }
    
    public int getRowNumber(String gUID)throws Exception 
    {   		
		int rowNumber = 0;
		
		FileInputStream fi=new FileInputStream(filePath);    			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet(0);    		
		    		
		System.out.println("UJ"+gUID);
		
		if(s.findCell(gUID)!=null)
		{
			rowNumber =s.findCell(gUID).getRow();
		 	   		 	
		 	System.out.println("rowNumber"+rowNumber);	 			 	
		} 		
		return rowNumber;
	}   
    
    public HSSFFont setPassFont() throws Exception
    {	
		 HSSFFont passfont = workBook.getFontAt((short) 1);
		 passfont.setBoldweight(passfont.BOLDWEIGHT_BOLD);
		 passfont.setFontName("Calibri");
		 passfont.setColor((short) 17);
		 passfont.setFontHeightInPoints((short)11);
		 return passfont; 
	}
	
	public HSSFFont setFailFont() throws Exception
	{	
		 HSSFFont failfont = workBook.getFontAt((short)2);
		 failfont.setBoldweight(failfont.BOLDWEIGHT_BOLD);
		 failfont.setFontName("Calibri");
		 failfont.setColor((short) 10);
		 failfont.setFontHeightInPoints((short)11);
		 return failfont; 
	}	
}
