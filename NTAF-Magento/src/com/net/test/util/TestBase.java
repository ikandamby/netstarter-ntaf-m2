package com.net.test.util;

/**
 * @author Netstarter
 * @version 1.0
 * This class is the main base class
 * All selenium reuse methods are define in this class
 */

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;

import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.FileSystemOptions;
import org.apache.commons.vfs.Selectors;
import org.apache.commons.vfs.VFS;
import org.apache.commons.vfs.impl.DefaultFileSystemConfigBuilder;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class TestBase {
	
	public static WebDriver driver;
	public static String readTime = "";
	/**hash map for executed class and methods **/
	public static HashMap<String, String> isclassExecuted= new HashMap<String, String>();
	public static HashMap<String, String> isScenarioExecuted = new HashMap<String, String>();
	public static String getSiteXLName;
	
	/**variables for property file **/
	public static HashMap<String, String> getParameterXpath = new HashMap<String, String>();
	public static HashMap<String, String> getMethodIsExecuted = new HashMap<String, String>();
	public static HashMap<String, String> getMessages = new HashMap<String, String>();
	
	/**variables for data file **/
	public static HashMap<String, String> getParameters = new HashMap<String, String>();
	public static HashMap<String, String> getAccountDetails = new HashMap<String, String>();
	public static HashMap<String, String> getSocialMediaDetails = new HashMap<String, String>();	
	public static HashMap<String, String> getUserDetails = new HashMap<String, String>();
	public static HashMap<String, String> getCheckoutDetails = new HashMap<String, String>();
		
	/**variables for write results files **/
	public static String writeFileName ;
	public static WriteResults wResult = new WriteResults();
	public static WriteResultsToUJ wResultToUj = new WriteResultsToUJ();
	
	/**variables and object for get data files files **/
	public static ReadXl readXL = new ReadXl();
	public String readFile;	
	
	/**variables for save screenshots**/
	private static BufferedImage bufferedImage;
	public static File scrFile;	
	public static String failScreenshotsFolder="";
	
	/**Gmail reading variables**/
	public static String bodyText = "";
	public static String subject;
	public static String suitType = ""; 
	
	public ArrayList<String> totalErrorMgs = new ArrayList<String>();	
	public String alertText = "";	
	
	public static java.util.Date startTime;
	public static long WAIT = 500;
	
	public static String fileReadTime = "";
	
	public static TestCommonMethods common = new TestCommonMethods();	

	public WebDriver getFireFoxDriver() throws Exception
	{	
		try
		{
			
		
			System.out.println(common.siteUrl +" is loading........");
			
			// selenium 3.0 
			/*	String workingDir = System.getProperty("user.dir")+"\\geckodriver.exe";
				System.setProperty("webdriver.gecko.driver",workingDir);
				System.out.print(workingDir);
				DesiredCapabilities capabilities = DesiredCapabilities.firefox();
				capabilities.setCapability("marionette", true);
				driver = new FirefoxDriver(capabilities);*/
			
				

				
				
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.cache.disk.enable", true);
		    driver = new FirefoxDriver(profile);
			
		
			//ProfilesIni allProfiles = new ProfilesIni();
			//FirefoxProfile profile = allProfiles.getProfile("default");
			//driver = new FirefoxDriver(profile);			
			
			
			
			driver.manage().timeouts().pageLoadTimeout(300000000, TimeUnit.SECONDS);			
			driver.manage().window().maximize();
		}
		catch(Exception e)
		{
			reRunFile("Following respose found "+e.getMessage());			
			writeTestResults("Verify Home page","Home page should be displayed","Following respose found "+e.getMessage(),"",false,true);
		}		
	    return driver; 		
	}
	
	public WebDriver getChromeDriver() throws Exception
	{
		try
		{
			System.setProperty("webdriver.chrome.driver", ReadXl.chromeDriver);			
			driver = new ChromeDriver();			
			driver.manage().window().maximize();
		}
		catch(Exception e)
		{
			reRunFile("Following respose found "+e.getMessage());			
			writeTestResults("Verify Home page","Home page should be displayed","Following respose found "+e.getMessage(),"",false,true);
		}				  
	    return driver;    		
	}
	
	public WebDriver getSeleniumDriver() throws Exception
	{	
		try
		{
			driver = new FirefoxDriver();			
		    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);		    	    
		    driver.manage().window().maximize();
		}
		catch(Exception e)
		{
			reRunFile("Following respose found "+e.getMessage());			
			writeTestResults("Verify Home page","Home page should be displayed","Following respose found "+e.getMessage(),"",false,true);
		}		
	    return driver; 	
	}
	
	public void mainData() throws Exception
	{
		fileReadTime = wResult.readTime;
		failScreenshotsFolder = ReadXl.screenShot;
		
	}
	
	
	
	public void executedBrowser() throws Exception
	{	
		
		
		if(findElementInXLSheet(getParameterXpath,"browser").equalsIgnoreCase("Firefox"))
		{
			driver = getFireFoxDriver(); //get selenium instance	
		}
		else if(findElementInXLSheet(getParameterXpath,"browser").equalsIgnoreCase("Chrome"))
		{
			driver = getChromeDriver(); //get selenium instance	
		}
		else
		{
			driver = getSeleniumDriver();
		}
		
		Calendar cal = Calendar.getInstance(); 	
		
		startTime = cal.getTime();
		
		common.navigateToHomePage();	
	}
	
	public void clearCookies() throws Exception
	{
		driver.manage().deleteAllCookies();	
		
		common.navigateToHomePage();
	}
	
	
	public static void getTestSuiteType(String Test_Suite_Type)throws Exception 
	{
		suitType = Test_Suite_Type;		
		readXL.getMainLocations(suitType);
		failScreenshotsFolder = ReadXl.screenShot;
	}	
	
	public boolean isElementPresent(String locator) throws Exception
	{
		boolean found = false;
		int tries = 0;
		
		if(!locator.isEmpty())
		{
			while (!found && tries < 2 )
			{
				tries += 1;				
			    try 
			    {
			    	driver.findElement(getLocator(locator));			    	
			       	found = true; // FOUND IT			       	
			    	break;
			    
			    }
			    catch (Exception e )
			    {
			    	Thread.sleep(WAIT/2);			    	 
			        found = false;
			        
			        errorLogs("Locator "+locator+" is not found");
			     }					    			   
			 } 
			
			if(!found)
			{
				System.out.println("Locator "+locator+" is not found - in "+ ((WAIT/1000)*3)/2 + " sec");
			}
		}			
		return found;
	}
	
	public boolean isErrorElementPresent(String locator) throws Exception
	{
		boolean found = false;
		int tries = 0;
		
		while (!found && tries < 2 )
		{
			tries += 1;			
		    try 
		    {
		    	driver.findElement(getLocator(locator));		    	
		    	found = true; // FOUND IT		    	
		    	break;
		    
		    }
		    catch (Exception e )
		    {
		    	Thread.sleep(WAIT/2);		    	
		        found = false;
		        errorLogs("Locator "+locator+" Element is not present");
		    }		    
		 } 		
		return found;
	}
	
	public boolean isDisplayed(String locator) throws Exception
	{		
		boolean found = false;		
		int tries = 0;
	
		while (!found && tries < 4 )
		{
			tries += 1;			
			try
			{	
				if(driver.findElement(getLocator(locator)).isDisplayed())
				{
					found = true; // FOUND IT					
			    	break;	    	
				}
				else
				{
					Thread.sleep(WAIT/5);
					
				}
			}
			catch (Exception e )
		    {
				found = false;			
		    }			
		 } 
		
		if(!found)
		{
			 errorLogs("Locator "+locator+" Element is not displyed");
		}
		 return found;		
	}
	
	public boolean isSelected(String locator) throws Exception
	{		
		boolean found = false;
		int tries = 0;
		
		while (!found && tries < 2 )
		{
			tries += 1;			
			try
			{
				if(driver.findElement(getLocator(locator)).isSelected())
				{
					found = true; // FOUND IT			    	
			    	break;
				}	
				else
				{
					Thread.sleep(WAIT/2);
				}
					
			}
			catch (Exception e )
		    {
		        found = false;			        
		    }
		 } 
		 return found;		
	}	
	
	public boolean isErrorDisplayed(String locator) throws Exception
	{		
		boolean found = false;
		int tries = 0;
		
		while (!found && tries < 2 )
		{
			tries += 1;			
			try
			{
				if(driver.findElement(getLocator(locator)).isDisplayed())
				{
					found = true; // FOUND IT			    	
			    	break;
				}
				else
				{
					Thread.sleep(WAIT/2);
				}
			}
			catch (Exception e )
		    {
		        found = false;			        
		    }			
		 } 
		 return found;		
	}	
	
	public boolean isEnabled(String locator) throws Exception
	{		
		boolean found = false;
		int tries = 0;
		
		while (!found && tries < 2 )
		{
			tries += 1;			
		    try 
		    {	    	
		    	if(driver.findElement(getLocator(locator)).isEnabled())
		    	{
		    		found = true; // FOUND IT			    	
			    	break;
		    	}
		    	else
				{
					Thread.sleep(WAIT/2);
				}   			    
		    }
		    catch (Exception e )
		    {
		        found = false;
		        errorLogs("Locator "+locator+" Element is not present");
		    } 		   
		 } 		
		 return found;		
	}
	
	public String getCSSValue(String locator,String cssvalue) throws Exception
	{		
		String color = "";
		int tries = 0;
		
		while (color=="" && tries < 2 )
		{
			tries += 1;			
		    try 
		    {	 
		    	if(isElementPresent(locator))
		    	{
		    		color = driver.findElement(getLocator(locator)).getCssValue(cssvalue);			    	
			    	break;
		    	}
		    	else
				{
					Thread.sleep(WAIT);
				}   		    
		    }
		    catch (Exception e )
		    {
		    	 errorLogs("Locator "+locator+" Element is not present");
		    }  
		 } 
		 return color;		
	}
	
	public ArrayList<String> isErrorMsgDisplayed(String locator) throws Exception
	{
		if(isElementPresent(locator))
		{
			List<WebElement> totalErrors = driver.findElements(getLocator(locator));
			 
			 for(int i=0;i<totalErrors.size();i++)
			 {
				 totalErrorMgs.add(totalErrors.get(i).getText());
			 }			 
		}		 
		return totalErrorMgs;			
	}
	
	public int getRowCount(String locator,String tagName) throws Exception
	{
		int noOfRows = 0;
		
		try
		{
			noOfRows = driver.findElement(getLocator(locator)).findElements(By.tagName(tagName)).size();	
		}
		catch(Exception e)
		{
			errorLogs("Exception"+e.getMessage());
		}
		return noOfRows; 		
	}
	
	public int getRowCountClassName(String locator,String tagName) throws Exception
	{
		int noOfRows = 0;
		
		try
		{
			noOfRows = driver.findElement(getLocator(locator)).findElements(By.className(tagName)).size();	
		}
		catch(Exception e)
		{
			errorLogs("Exception"+e.getMessage());
		}
		return noOfRows; 		
	}
	public void pageRefersh() throws Exception
	{
		driver.navigate().refresh();		
		
		if(isExecuted(getMethodIsExecuted,"has home page popup"))
		{
			if(isElementPresent(findElementInXLSheet(getParameterXpath,"home signup popup id")))
			{
				click(findElementInXLSheet(getParameterXpath,"home signup popup id"));
			}
		}				
	}
	
	public void waitForPageLoad() throws Exception
	{
		try
		{
			driver.manage().timeouts().pageLoadTimeout(100,TimeUnit.SECONDS);			
		}
		catch(Exception e)
		{
			pageRefersh();				
		}	
	}
	
	public boolean isAlertPresent1() throws Exception 
	{ 
	    try 
	    { 
	    	driver.switchTo().alert(); 	           
	        return true; 
	    }  
	    catch (NoAlertPresentException e) 
	    { 	
	    	errorLogs("Exception"+e.getMessage());
	        return false; 
	    }  
	}
	
	public boolean isAlertPresent() throws IOException 
	{
		try 
	    {
	      driver.switchTo().alert();	      
	      return true;
	    } 
	    catch (NoAlertPresentException e) 
	    {
	      errorLogs("Exception"+e.getMessage());	      
	      return false;
	    }	
	}
	 
	public String getAlert() throws Exception 
	{ 
		boolean acceptNextAlert = true;	
		
	    try 
	    {  
	    	Alert alert = driver.switchTo().alert(); 	
	    	alertText = driver.switchTo().alert().getText(); 
	    	
	        //alertText = driver.switchTo().alert().getText(); 
	      
	        if (acceptNextAlert) {
		        alert.accept();
		      } else 
		      {
		        alert.dismiss();
		      }	       
	    }  
	    catch (Exception e) 
	    { 
	    	errorLogs("Exception"+e.getMessage());	        
	    } 
	    
	    return alertText; 
	}
	
	@SuppressWarnings("unused")
	private String closeAlertAndGetItsText() 
	{
		boolean acceptNextAlert = true;			
	    try 
	    {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) 
	      {
	        alert.accept();
	      } else 
	      {
	        alert.dismiss();
	      }
	      return alertText;
	    } 
	    finally 
	    {
	      acceptNextAlert = true;
	    }
	  }
	
	public void acceptAlert() throws Exception 
	{ 
	    try 
	    { 
	    	driver.switchTo().alert().accept();       
	    }  
	    catch (Exception e) 
	    {  
	    	errorLogs("Exception"+e.getMessage());
	    }  
	}
	
	public void switchToFrame(String frameId) throws Exception 
	{ 
	    try 
	    { 
	    	driver.switchTo().frame(driver.findElement(getLocator(frameId)));	     	      
	    }  
	    catch (Exception e) 
	    { 
	    	errorLogs("Exception"+e.getMessage());
	    }  
	}
	
	public void backToDefaultPage() throws Exception 
	{ 
	    try 
	    { 
	    	driver.switchTo().defaultContent();	     	      
	    }  
	    catch (Exception e) 
	    { 
	    	errorLogs("Exception"+e.getMessage());
	    }  
	} 
	
	public void sendKeys(String locator,String value) throws Exception
	{
		try
	    {	
			driver.findElement(getLocator(locator)).clear();
			driver.findElement(getLocator(locator)).click();		
			driver.findElement(getLocator(locator)).sendKeys(value);
			
			
		
	    }
		
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }				
	}


	public void pressEnter(String locator) throws Exception
	{
		try
	    {	
			driver.findElement(getLocator(locator)).sendKeys(Keys.ENTER);
					
		
	    }
		
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }				
	}
	
	
	
	
	public boolean click(String locator) throws Exception
	{
		boolean clickLink = true;
		try
	    {
			driver.findElement(getLocator(locator)).click();			
			Thread.sleep(WAIT/5);					
		}
	    catch (Exception e )
	    {
	    	clickLink =false;
	    }			
		return clickLink;				
	}
	
	
	public boolean Doubleclick(String locator) throws Exception
	{
		boolean clickLink = true;
		try
	    {
			Actions action = new Actions(driver);
			action.moveToElement(driver.findElement(getLocator(locator))).doubleClick().build().perform();
					
			Thread.sleep(WAIT/5);					
		}
	    catch (Exception e )
	    {
	    	clickLink =false;
	    }			
		return clickLink;				
	}
	
	
	
	
	
	
	
	public int getCount(String path) throws Exception
	{
		int count=0;
		
		
		try
	    {
			List<WebElement> optionCount=driver.findElements(By.xpath(path));
			count = optionCount.size();		
			Thread.sleep(WAIT/5);					
		}
	    catch (Exception e )
	    {
	    	System.out.println(e);
	    }			
		return count;				
	}
	
	
	
	public boolean clickAndWait(String locator) throws Exception
	{
		boolean clickLink = true;
		try
	    {
			if(isElementPresent(locator))
			{
				driver.findElement(getLocator(locator)).click();				
				waitForPageLoad();				
			}			
		}
	    catch (Exception e )
	    {
	    	clickLink =false;
	    	errorLogs("Exception"+e.getMessage());
	    }		
		return clickLink;			
	}
	
	public void selectText(String locator,String selectText) throws Exception
	{
		try
	    {		
			new Select(driver.findElement(getLocator(locator))).selectByVisibleText(selectText);
		}
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }				
	}
	
	public void selectIndex(String locator,int index) throws Exception
	{
		try
	    {			
			new Select(driver.findElement(getLocator(locator))).selectByIndex(index);
		}
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }				
	}
	
	public int selectedValueInDropdown(String locator) throws Exception
	{
		int currentValue = 0;
		try
	    {
			if(isElementPresent(locator))
			{
				currentValue = Integer.parseInt(new Select(driver.findElement(getLocator(locator))).getFirstSelectedOption().getText());
			}	
			else
			{
				currentValue = 0;
			}
	    }
	    catch (Exception e )
	    {
	    	currentValue = 0;
	    	errorLogs("Exception"+e.getMessage());
	    }		
		return currentValue;				
	}
	
	public String getText(String locator) throws Exception
	{
		String textValue ="";
		try
	    {
			textValue= driver.findElement(getLocator(locator)).getText().trim();			
	    }
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }
		return textValue;				
	}
	
	public String getAttribute(String locator,String attributeType) throws Exception
	{
		String attributeValue ="";			
		try
	    {
			if(isElementPresent(locator))
			{
				attributeValue= driver.findElement(getLocator(locator)).getAttribute(attributeType).trim();
			}
			else
			{
				attributeValue = "";
			}
	    }
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }		
		return attributeValue;				
	}
	
	public String getValidationText(String locator) throws Exception
	{
		String textValue ="";
		try
	    {
			if(isErrorElementPresent(locator))
			{
				textValue= driver.findElement(getLocator(locator)).getText().trim();
			}
			else
			{
				textValue = "";
			}
	    }
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }
		return textValue;				
	}
	
	public String getValidationAttribute(String locator,String attributeType) throws Exception
	{
		String attributeValue ="";		
		try
	    {
			if(isErrorElementPresent(locator))
			{
				attributeValue= driver.findElement(getLocator(locator)).getAttribute(attributeType).trim();
			}
			else
			{
				attributeValue = "";
			}
	    }
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }
		return attributeValue;				
	}
	
	public String getTitle() throws Exception
	{
		String title ="";
		try
	    {
			title = driver.getTitle().trim();			
	    }
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }
		return title;
				
	}
	
	public String getCurrentUrl() throws Exception
	{
		String currentUrl ="";
		try
	    {
			currentUrl = driver.getCurrentUrl();			
	    }
	    catch (Exception e )
	    {
	    	errorLogs("Exception"+e.getMessage());
	    }
		return currentUrl;				
	}
	
	public void goBack() throws Exception
	{
		driver.navigate().back();		
		waitForPageLoad();				
	}
	
	public void openPage(String url) throws Exception
	{
		try
		{
			driver.get(url);
			
			/*if(!isDisplayed("ag_confirm_Id"))
			{
				Thread.sleep(WAIT*10);
			}
			
			if(isDisplayed("ag_confirm_Id"))
			{
				Thread.sleep(WAIT*2);
				
				selectText("ag_select_day_Id","6");
				selectText("ag_select_month_Id","April");
				selectText("ag_select_year_Id","1944");
				click("ag_confirm_Id");
			}*/	
			
		}
		catch (Exception e )
		{
			skipMethods("Home Page","Home page should diplayed","Home page is not diplayed within 60sec");
			throw new AssertionError("Home page is not displayed.");			
		}			
	}
	
	public boolean getPageSource(String searchText) throws Exception
	{	
		boolean pageSourceValue = true;		
		try
		{		
			
		//	if(driver.getPageSource().indexOf(searchText) >= 0)
				
				if(driver.getPageSource().contains("We couldn't find any records."))
			{
				pageSourceValue = true;
			}
			else
			{
				pageSourceValue = false;
			}			
		}
		catch (Exception e )
	    {
			pageSourceValue = false;
			errorLogs("Exception"+e.getMessage());
	    }		
		return pageSourceValue;				
	}
			
	public String getPageContent() throws Exception
	{		
		String pageSourceValue = "";		
		try
		{
			if(driver.getPageSource()!=null)
			{
				pageSourceValue = driver.getPageSource().trim();
			}			
		}
		catch (Exception e )
	    {
			pageSourceValue = "No Page source";
			errorLogs("No Page source");
	    }	
		
		return pageSourceValue;				
	}
	
	public int getTagListSize(String locator,String tag) throws Exception
	{		
		int size = 0;
		
		try
		{
			size = driver.findElement(getLocator(locator)).findElements(getLocator(tag)).size();			
		}
		catch (Exception e )
	    {
			errorLogs("Exception"+e.getMessage());
	    }		
		return size;				
	}
	
	public void getTagList(String locator,String tag) throws Exception
	{
		try
		{
			driver.findElement(getLocator(locator)).findElements(getLocator(tag));		
		}
		catch (Exception e )
	    {
			errorLogs("Exception"+e.getMessage());
	    }		
	}
	
	public int getElementSize(String element) throws Exception
	{	
		int size = 0;		
		try
		{
			size = driver.findElements(getLocator(element)).size();					
		}
		catch (Exception e )
	    {
			errorLogs("Exception"+e.getMessage());
	    }		
		return size;				
	}
	
	public int getDropdownSize(String element,String tagName) throws Exception
	{		
		int size = 0;		
		try
		{
			size = driver.findElement(getLocator(element)).findElements(By.tagName(tagName)).size();
		}
		catch (Exception e )
	    {
			errorLogs("Exception"+e.getMessage());
	    }		
		return size;				
	}
	
	public String getSelectedValueFromDropdown(String element,String tagName,int optionValue) throws Exception
	{
		String dropdownValue = "";		
		try
		{
			dropdownValue = driver.findElement(getLocator(element)).findElements(By.tagName(tagName)).get(optionValue).getText();
		}
		catch (Exception e )
	    {
			errorLogs("Exception"+e.getMessage());
	    }
		return dropdownValue;				
	}	

	@SuppressWarnings("unused")
	public static void mailRead(String subjectName,String custemerName,String email,String pw) throws InterruptedException
	{	
		boolean isMailFound = false;
		Properties props = System.getProperties();	
		
 		final String username = email;
	    final String password = "19881106";
	    String host = "smtp.gmail.com";
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.starttls.enable", "true");
	    props.put("mail.smtp.host", host);
	    props.put("mail.smtp.port", "587");
	    // Get the Session object. 				
 		try {
 			 Session session = Session.getInstance(props,
 				      new javax.mail.Authenticator() {
 				         protected PasswordAuthentication getPasswordAuthentication() {
 				            return new PasswordAuthentication(username, password);
 				         }
 				      });
 				Store store = session.getStore("imaps");
	 			store.connect("imap.gmail.com", email, "19881106");
	 	 
	 			Folder inbox = store.getFolder("Inbox");
	 			inbox.open(Folder.READ_WRITE);
	 			FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
	 			Message messages[] = inbox.search(ft);
	 			System.out.println("total "+messages.length);
	 			if(messages.length == 0)
	 			{
	 				try 
	 				{
						mailBoxAgainRead(subjectName,custemerName,email,pw);
					} catch (InterruptedException e) {
							e.printStackTrace();
					}
	 			}
	 			else
	 			{
	 	 			for (Message message : messages) 
	 	 			{
	 	 				System.out.println("message"+message.getSize());
	 	 				System.out.println("messages"+messages.length);
	 	 				subject = message.getSubject();
	 	 					String content = message.getContentType();
	 	 				if (message.getContent() instanceof Multipart) {
	 	 				    Multipart part = (Multipart)message.getContent();
	 	 				    BodyPart bodyPart = part.getBodyPart(0);
	 	 	 				part.getContentType();
	 	 	 				part.getCount();
	 	 	 				((MimeMultipart) part).getPreamble();
	 	 				}
	 	 				try 
	 	 				{
	 	 					if(subject.indexOf(subjectName)>-1)
	 	 					{
	 	 						System.out.println("has subject");
	 	 						printParts(message,custemerName); 
	 	 						isMailFound = true;
	 							break;
	 	 	 				}
	 	 					else
	 	 					{						
	 	 						isMailFound = false;
	 	 					}
	 	 	 			} catch (Exception e) {
	 		 					e.printStackTrace();
	 	 				}
	 	 	 
	 	 	 			Flags flags = message.getFlags();
	 	 	 			message.setFlag(Flags.Flag.SEEN, true);
	 	 	 			}
	 	 				if(!isMailFound)
	 	 				{
	 	 					mailBoxAgainRead(subjectName,custemerName,email,pw);
	 	 				}
	 				}
 	 			} catch (NoSuchProviderException e) {
 	 				e.printStackTrace();
 	 				mailBoxAgainRead(subjectName,custemerName,email,pw);
 	 				//System.exit(1);
 	 			} catch (MessagingException e) {
 	 				e.printStackTrace();
 	 				
 	 				mailBoxAgainRead(subjectName,custemerName,email,pw);
 	 				//System.exit(2);
 	 			} catch (IOException e) {
 	 			
 	 				e.printStackTrace();
 	 				mailBoxAgainRead(subjectName,custemerName,email,pw);
 	 			} catch (InterruptedException e) {
					e.printStackTrace();
				}
 	}
	
	public static void mailBoxAgainRead(String subjectName,String custemerName,String email,String pw) throws InterruptedException
	{	
		Properties props = System.getProperties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "993");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "993");
 		props.setProperty("mail.store.protocol", "imaps");
 		
 		try {
 			Thread.sleep(10000);
 			
 			Session session = Session.getInstance(props, null);
 			session.setDebug(true);
 			Store store = session.getStore("imaps");
 			store.connect("imap.gmail.com", email, "2wsx@#1qaz");
 			Folder inbox = store.getFolder("Inbox");
 			inbox.open(Folder.READ_WRITE);
 			FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
 			Message messages[] = inbox.search(ft);
 			for (Message message : messages) {
 				subject = message.getSubject();
 					//String content = message.getContentType();
 				if (message.getContent() instanceof Multipart) {
 				    Multipart part = (Multipart)message.getContent();
 				    //BodyPart bodyPart = part.getBodyPart(0);
 	 				part.getContentType();
 	 				part.getCount();
 	 				((MimeMultipart) part).getPreamble();
 				}
 				try {
 					if(subject.indexOf(subjectName)>-1)
 					{
 						System.out.println("has subject");
 						printParts(message,custemerName); 						
 					}
 				} catch (Exception e) {
	 					e.printStackTrace();
 				}
 	 
 	 			//Flags flags = message.getFlags();
 	 			message.setFlag(Flags.Flag.SEEN, true);
 	 			}
 			
 	 			} catch (NoSuchProviderException e) {
 	 				e.printStackTrace();
 	 				//System.exit(1);
 	 			} catch (MessagingException e) {
 	 				e.printStackTrace();
 	 				//System.exit(2);
 	 			} catch (IOException e) {
 	 			
 	 				e.printStackTrace();
 	 			}
 	 }
	
	public static void printParts(Part p,String custemerName) throws Exception 
	{
			Object obj = p.getContent();
			if (obj instanceof String) {
				if(((String) obj).toLowerCase().indexOf(custemerName.toLowerCase())>-1)
				{										
					bodyText = (String)obj;
				}
				else
				{
					bodyText = "";
				}
			} else if (obj instanceof Multipart) {
				Multipart mp = (Multipart) obj;
				int count = mp.getCount();
					for (int i = 0; i < count; i++) {
						printParts(mp.getBodyPart(i),custemerName);
					}	
			}else if (obj instanceof InputStream) {
				@SuppressWarnings("unused")
				InputStream is = (InputStream) obj;
				@SuppressWarnings("unused")
				int c;
			}
	}
	
	public void resultSheetName(String resultSheet)throws Exception 
	{
		writeFileName = resultSheet;
	}
	
	/*public static void reRunFile(String message) throws IOException 
	{ 
		File log = new File(ReadXl.reRunLogFile+"error.txt");	
		
		if(log.exists() == false)
		{
			log.createNewFile();
			
		}
		PrintWriter out = new PrintWriter(new FileWriter(log), true);   
		out.println(message);
	    out.close();
	    	    
	}*/
	
	public static void reRunFile(String message) throws IOException 
	{ 

    	File logFolder = new File(ReadXl.reRunLogFile);
    	
    	if(logFolder.exists()==true)
    	{
    		PrintWriter out = new PrintWriter(new FileWriter(ReadXl.reRunLogFile+"error.txt"), true);     
    	    out.println(message);
    	    out.close();
    	}
    	
			    
	}
	
	public static void errorLogs(String message) throws IOException 
	{ 		
        try
        {
        	File log = new File(ReadXl.logFolder+"/Error log-"+wResult.readTime+".txt");
        	
        	if(log.exists()==false)
        	{
                log.createNewFile();
        	}
        	PrintWriter out = new PrintWriter(new FileWriter(log, true));
        	out.append(message + "\r\n");
        	out.close();
        	System.out.println(message);
        }
        catch(IOException e)
        {
           // System.out.println("COULD NOT LOG!!"+e.getMessage());
        }
	}

	public void skipMethods(String className,String methodName,String Status)throws Exception 
	{
		if(Status.equalsIgnoreCase(""))
		{
			Status = "This test no need to execute";
		}
		wResult.writeSkipTestResult(className , methodName ,Status, "Skip Results");
	}
	
	public By getLocator(String locatorValue) throws Exception
	{
		By findLocator = null;
		
		if(locatorValue.indexOf("_Id")>-1)
		{
			findLocator = By.id(locatorValue.replaceAll("_Id", "").trim());			
		}
		else if(locatorValue.indexOf("_Name")>-1 || locatorValue.indexOf("_name")>-1)
		{
			findLocator = By.name(locatorValue.replaceAll("_Name", "").replaceAll("_name", "").trim());
		}
		else if(locatorValue.indexOf("_ClassName")>-1 ||locatorValue.indexOf("_className")>-1)
		{
			findLocator = By.className(locatorValue.replaceAll("_ClassName", "").replaceAll("_className", "").trim());
		}
		else if(locatorValue.indexOf("_Link")>-1)
		{
			findLocator = By.linkText(locatorValue.replaceAll("_Link", "").trim());
		}
		else if(locatorValue.indexOf("_Css")>-1 || locatorValue.indexOf("_css")>-1)
		{
			findLocator = By.cssSelector(locatorValue.replaceAll("_Css", "").replaceAll("_css", "").trim());
		}
		else if(locatorValue.indexOf("_TagName")>-1 || locatorValue.indexOf("_tagName")>-1)
		{
			findLocator = By.tagName(locatorValue.replaceAll("_TagName", "").replaceAll("_tagName", "").trim());
		}
		else if(locatorValue.indexOf("_Xpath")>-1 || locatorValue.indexOf("_")>-1)
		{
			findLocator = By.xpath(locatorValue.replaceAll("_Xpath", "").replaceAll("_xpath", "").trim());
		}
		else
		{
			findLocator = By.xpath(locatorValue.trim());
		}				
		return findLocator;				
	}
	
	public void captureScreenShot(String screenShotName) throws Exception
	{
		//BufferedImage bufferedImage;
		
		System.out.println("xxx"+failScreenshotsFolder+screenShotName);
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);		
		
		org.apache.commons.io.FileUtils.copyFile(scrFile, new File(failScreenshotsFolder+screenShotName+".png"));			
	}
	
	public void captureScreenShotForCompareImages(String screenShotName) throws Exception
	{	
		 if(findElementInXLSheet(getParameterXpath,"browser").equalsIgnoreCase("Chrome"))
		{

			 Thread.sleep(WAIT); 	
		}
		 else
		 {
			 Thread.sleep(WAIT/4); 
		 }
		
		
		
		 scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);		
		
		 org.apache.commons.io.FileUtils.copyFile(scrFile, new File(screenShotName+".png"));		
		
		 bufferedImage = ImageIO.read(new File(screenShotName+".png"));
		 
		  // create a blank, RGB, same width and height, and a white background
		  BufferedImage newBufferedImage = new BufferedImage(bufferedImage.getWidth(),
				bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
		  newBufferedImage.createGraphics().drawImage(bufferedImage, 0, 0, Color.WHITE, null);
	 
		  // write to jpeg file
		  ImageIO.write(newBufferedImage, "jpg", new File(screenShotName+".jpg"));
	 
		
		  System.out.println("Done");
		
	}
	
	public static String findElementInXLSheet(HashMap<String, String> mapName,String parameterName)throws Exception
	{
		String parameterValue = "";
		
		if(mapName.containsKey(parameterName.trim()))
		{
			parameterValue =  mapName.get(parameterName).trim(); 
		}		
		return parameterValue ;
	}
	
	public static boolean isExecuted(HashMap<String, String> type,String method)throws Exception
	{
		boolean isExecuted = false;		
		try
		{
			if(findElementInXLSheet(type,method).equalsIgnoreCase("Yes"))
			{
				isExecuted = true;
			}
			else
			{
				isExecuted = false;
			}			
		}
		catch(Exception e)
		{
			isExecuted = false;
			
			errorLogs("Following error is found in "+method+" - "+e.getMessage());			
		}		
		return isExecuted ;
	}
	
	public void mouseMove(String mouseMoveElement)throws Exception
	{
		if(findElementInXLSheet(getMethodIsExecuted,"Mouse Move Action").equalsIgnoreCase("Yes"))
		{
			if(isElementPresent(mouseMoveElement))
			{
				Actions actions = new Actions(driver);
				WebElement menuHoverLink = driver.findElement(getLocator(mouseMoveElement));
				actions.moveToElement(menuHoverLink).build().perform();
				Thread.sleep(1000);
			}		
		}
	}
	
	public static void statusChangeAllmails()
	{
 		Properties props = System.getProperties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
 		props.setProperty("mail.store.protocol", "imaps");
 		
 		try {
 			Session session = Session.getInstance(props, null);
 			session.setDebug(true);
 			Store store = session.getStore("imaps");
 			store.connect("imap.gmail.com", "kalnsh494@gmail.com", "19881106ga$$");
 			Folder inbox = store.getFolder("Inbox");
 			inbox.open(Folder.READ_WRITE);
 			FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
 			Message messages[] = inbox.search(ft);
 			for (Message message : messages) {
 				
 				//Flags flags = message.getFlags();
 	 			message.setFlag(Flags.Flag.SEEN, true);
 	 			}
 			
 	 			} catch (NoSuchProviderException e) {
 	 				e.printStackTrace();
 	 				System.exit(1);
 	 			} catch (MessagingException e) {
 	 				e.printStackTrace();
 	 				System.exit(2);
 	 			}
 	 }
	
	public void writeTestResults(String testCase,String expectedResults,String actualResults,String gUID,boolean status,boolean isAssert) throws Exception
	{
		if(!testCase.isEmpty())
		{
			writeTestResultsToExcel(testCase,expectedResults,actualResults,status);
		}
		if(!gUID.isEmpty())
		{
			writeTestResultsToUJ(gUID,status);
		}		
		if(!status)
		{
			String logMsg = "";
			String failTc = "";
			String failTc1 = "";
			logMsg =	"************************************\r\n"+
					 	" "+writeFileName+" - "+testCase+
						"\r\n Expected Results:"+
						expectedResults+
						"\r\n Actual Results:"+
						actualResults+
						"\r\n************************************";
			
			failTc = writeFileName+"-"+testCase;
			System.out.println(wResult.readTime);
			System.out.println(writeFileName);
			System.out.println(testCase);
			
			if(failTc.length() > 100)
			{
				//failTc = wResult.readTime+"-"+writeFileName+"-"+testCase;
            	
				failTc1 = failTc.substring(0, 100).replaceAll("([^a-zA-Z]|\\s)+","_");
				
			}
			else
			{
				failTc1 = failTc.replaceAll("([^a-zA-Z]|\\s)+","_");
			}
			
			System.out.println("failTc"+ wResult.readTime+"-"+failTc1);
			
			captureScreenShot( wResult.readTime+"-"+failTc1);
			
			failTc = "";	
			errorLogs(logMsg);
		}
		if(isAssert)
		{			
			Assert.assertEquals(actualResults,expectedResults);
		}
	}
	
	public static void writeTestResultsToExcel(String testCase,String expectedResults,String actualResults,boolean status) throws Exception
	{
		if(writeFileName.equalsIgnoreCase("Verify UI"))
		{
			if(testCase.indexOf("UI") >= 0)
			{
				wResult.writeTestResult(testCase,expectedResults,actualResults,status,"UI Results");	
			}
			else
			{
				//wResult.writeTestResult(testCase,expectedResults,actualResults,status,writeFileName);
			}
		}
		else
		{
			wResult.writeTestResult(testCase,expectedResults,actualResults,status,writeFileName);	
		}
		
		if(status)
		{
			System.out.println(actualResults);
		}
	}
	
	public static void writeTestResultsToUJ(String gUID,boolean status) throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"has update UJ document"))
		{
			wResultToUj.writeTestResultToUJ(gUID,status);
		}
	}
	
	public void getUJDocument() throws Exception
	{
		String uJDocumentName = findElementInXLSheet(getParameterXpath,"user journey document name");
		String remoteFilePath= findElementInXLSheet(getParameterXpath,"get user journey document")+uJDocumentName;
		File f=new File(ReadXl.uJFolder+uJDocumentName); 
		  
		if(f.exists())
	    {
	        f.delete();
	    }
	    f.createNewFile(); 
	    
	    FileObject destn=VFS.getManager().resolveFile(f.getAbsolutePath());
	    FileSystemOptions opts=new FileSystemOptions();
	    DefaultFileSystemConfigBuilder.getInstance();
	    FileObject fo=VFS.getManager().resolveFile(remoteFilePath,opts);
	    
	    System.out.println("File is exists "+fo.exists());
	   
	    if(fo.exists())
	    {
	    	destn.copyFrom(fo,Selectors.SELECT_SELF);
		    destn.close();
	    }
	    else
	    {
	    	errorLogs("UJ document is not found");
	    }
	    
	    //removeStatus(ReadXl.uJFolder+uJDocumentName);
	}
	
	public void removeStatus(String fileName) throws Exception
	{			    
	    FileInputStream fsIP= new FileInputStream(new File(fileName)); //Read the spreadsheet that needs to be updated
        
	    HSSFWorkbook workBook = new HSSFWorkbook(fsIP); //Access the workbook
         
        HSSFSheet worksheet = workBook.getSheetAt(0); //Access the worksheet, so that we can update / modify it.
         
        Cell cell = null; // declare a Cell object
        
        for(int i=0;i < worksheet.getLastRowNum(); i++)
        {
        	if(worksheet.getRow(i).getCell(0) != null)
        	{
        		cell = worksheet.getRow(i).getCell(Integer.parseInt(findElementInXLSheet(getParameterXpath,"user journey document updated column")));
           	        	
           	 	cell.setCellValue(""); 
           	 
           	 	fsIP.close(); //Close the InputStream
                
                FileOutputStream output_file =new FileOutputStream(new File(fileName));  //Open FileOutputStream to write updates
                 
                workBook.write(output_file); //write changes
                 
                output_file.close();  //close the stream              
                
        	}        	 
        }	    
	}
	
	
}
