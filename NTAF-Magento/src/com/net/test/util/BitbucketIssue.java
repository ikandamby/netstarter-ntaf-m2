package com.net.test.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class BitbucketIssue 
{
	 public String notMergeCommit = "";
	 public String lastMergeTime ="";
	 public String converTime = "";
	 public String date = "";
	 public String lastMergeUser = "";
	 
public  void getBitBucketData() throws Exception {
    String username = "gharshana";
    String password = "19881106";
    String url = "https://bitbucket.org/api/2.0/repositories/netstarter";

    HttpClient client = new DefaultHttpClient();

    JSONParser parser = new JSONParser();

    Object obj = parser.parse(processRequest(url, username, password, client));

    JSONObject jsonObject = (JSONObject) obj;
   
    long commitCount = 0;

        String repoUrl = "https://bitbucket.org/api/1.0/repositories/netstarter/bbq-galore/changesets/?limit=50";
        JSONObject commitobj = (JSONObject) parser.parse(processRequest(repoUrl, username, password, client));

        commitCount = (Long) ((JSONObject) commitobj).get("count");
        
       // String branch = "";
       
     //  branch = (String)((JSONObject) commitobj).get(1);
        		
       // System.out.println("commitCount"+commitobj);
        
        JSONArray lang= (JSONArray) commitobj.get("changesets");
        
          
        Iterator i = lang.iterator();
        
        	 
        // take each value from the json array separately
        int getLastMergeTime = 0;
        int setLastMergeTime = 0;
        
      //  String notMergeCommit = "";    
       
        
        String message = "";
        String parents = "";
        String timestamp = "";
        String author = "";
        
       
        String branch = "";
        
        
         while (i.hasNext()) 
         {
        	 JSONObject innerObj = (JSONObject) i.next();
       	    // System.out.println(getLastMergeTime+" - Message "+innerObj.get("message")+" parents "+ innerObj.get("parents") +" Time " + innerObj.get("timestamp")+" Branch " + innerObj.get("branch"));
       	     message = innerObj.get("message").toString();
       	     parents = innerObj.get("parents").toString();
       	     timestamp = innerObj.get("timestamp").toString();
       	     author = innerObj.get("raw_author").toString();
       	     if(innerObj.get("branch") == null)
       	     {
       	    	 branch = "";
        	 }
       	     else
       	     {
       	    	 branch = innerObj.get("branch").toString();
       	     }          
       	     
       	     if(branch.indexOf("develop") < 0 && parents.indexOf(",") >=0 && message.indexOf("Merge branch 'develop' into release-Staging") >=0)
    	     { 
    	    	 lastMergeTime = timestamp.substring(11);
    	    	 date = timestamp.substring(0,10);
    	    	 lastMergeUser = author;
    	    	 notMergeCommit ="";
    	    	 
    	     }
    	     else
    	     {
    	    	 if(message.indexOf("into develop") <0 && !(author.indexOf("Gayan Harshana")>=0 || author.indexOf("unknown")>=0 ) && parents.indexOf(",") < 0 )
 	 	    	{
 	 	    		if(branch.indexOf("release-Staging") < 0 || message.indexOf("Merge remote-tracking branch") < 0)
 	 	    		{
 	 	    			notMergeCommit = notMergeCommit+"<br>"+("Author: "+author+" | Msg: "+message);
 	 	    			
 	 	    		}
 	 	    	}		
    	    }
       	  getLastMergeTime++;          	     
       	     
         }
         
         if(!lastMergeTime.equalsIgnoreCase(""))
         {
        	 getMergeTime(lastMergeTime);
         }
         
        
   

}


	public void getMergeTime(String lastMergeTime) throws ParseException 
	{ 
	
	    String time1 = lastMergeTime;
	    String time2 = "4:30:00";

	    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	    timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

	    Date date1 = timeFormat.parse(time1);
	    Date date2 = timeFormat.parse(time2);

	    long sum = date1.getTime() + date2.getTime();

	    converTime = date+" "+timeFormat.format(new Date(sum));
	    System.out.println("Last merge time is "+converTime);
 
 
 
	}
private static String getBasicAuthenticationEncoding(String username, String password) {

    String userPassword = username + ":" + password;
    return new String(Base64.encodeBase64(userPassword.getBytes()));
}

public static String processRequest(String url, String username, String password, HttpClient client) throws ClientProtocolException, IOException{
    HttpGet request = new HttpGet(url);

    request.addHeader("Authorization", "Basic " + getBasicAuthenticationEncoding(username, password));

    HttpResponse response = client.execute(request);

    System.out.println("\nSending 'GET' request to URL : " + url);
    System.out.println("Response Code : " + 
            response.getStatusLine().getStatusCode());

    BufferedReader rd = new BufferedReader(
            new InputStreamReader(response.getEntity().getContent()));

    StringBuffer result = new StringBuffer();
    String line = "";
    while ((line = rd.readLine()) != null) {
        result.append(line);
    }

    return result.toString();
}

}

