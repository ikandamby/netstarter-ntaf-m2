package com.net.test.util;

import com.nexmo.messaging.sdk.NexmoSmsClient;
import com.nexmo.messaging.sdk.SmsSubmissionResult;
import com.nexmo.messaging.sdk.messages.TextMessage;

/**
 * SendTextMessage.java<br><br>
 *
 *
 * Created on 5 January 2011, 17:34
 *
 * @author  Gayan Harshana
 * @version 1.0
 */
public class SendTextMessage extends TestBase{

   /* public static final String API_KEY = "fbcf215f";
    public static final String API_SECRET = "aee674da";*/
	public static final String API_KEY = "34b278c5";
    public static final String API_SECRET = "61773827";
	
	/*public static final String API_KEY = "f4205a6d";
    public static final String API_SECRET = "2a4ae816";
*/
    public static final String SMS_FROM = "Netstarter";
    public static String SMS_TO = "";
    public static String SMS_List = "";
    public static final String SMS_TEXT = "";

    public void sendSMS(String smsText)throws Exception 
    {
    	SMS_List = findElementInXLSheet(getParameters,"SMS Numbers List");
    	
    	if(SMS_List.indexOf(";") < 0 )
    	{
    		SMS_TO =SMS_List;  
    		
    		submitMessage(smsText,SMS_TO);
    		
    	}
    	else
    	{
    		 for (String SMS_TO: SMS_List.split(";", 0))
    		 {
    			 submitMessage(smsText,SMS_TO);   	         
    	        
    		 }         
    	         
    	}

    }
    

    public void submitMessage(String smsText,String SMS_TO)throws Exception
    {
    	 NexmoSmsClient client = null;
	        try {
	            client = new NexmoSmsClient(API_KEY, API_SECRET);
	        } catch (Exception e) {
	            System.err.println("Failed to instanciate a Nexmo Client");
	            e.printStackTrace();
	            throw new RuntimeException("Failed to instanciate a Nexmo Client");
	        }

	        // Create a Text SMS Message request object ...

	        TextMessage message = new TextMessage(SMS_FROM, SMS_TO, smsText);

	        // Use the Nexmo client to submit the Text Message ...

	        SmsSubmissionResult[] results = null;
	        try {
	            results = client.submitMessage(message);
	        } catch (Exception e) {
	            System.err.println("Failed to communicate with the Nexmo Client");
	            e.printStackTrace();
	            throw new RuntimeException("Failed to communicate with the Nexmo Client");
	        }

	        // Evaluate the results of the submission attempt ...
	        System.out.println("... Message submitted in [ " + results.length + " ] parts");
	        for (int i=0;i<results.length;i++) {
	            System.out.println("--------- part [ " + (i + 1) + " ] ------------");
	            System.out.println("Status [ " + results[i].getStatus() + " ] ...");
	            if (results[i].getStatus() == SmsSubmissionResult.STATUS_OK)
	                System.out.println("SUCCESS");
	            else if (results[i].getTemporaryError())
	                System.out.println("TEMPORARY FAILURE - PLEASE RETRY");
	            else
	                System.out.println("SUBMISSION FAILED!");
	            System.out.println("Message-Id [ " + results[i].getMessageId() + " ] ...");
	            System.out.println("Error-Text [ " + results[i].getErrorText() + " ] ...");

	            if (results[i].getMessagePrice() != null)
	                System.out.println("Message-Price [ " + results[i].getMessagePrice() + " ] ...");
	            if (results[i].getRemainingBalance() != null)
	                System.out.println("Remaining-Balance [ " + results[i].getRemainingBalance() + " ] ...");
	        }
    }
}
