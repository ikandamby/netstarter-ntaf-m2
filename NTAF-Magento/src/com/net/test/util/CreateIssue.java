package com.net.test.util;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

@SuppressWarnings("deprecation")
public class CreateIssue {

public static void CreateIssueJira(String summary,String description) throws ClientProtocolException,
   IOException {
  @SuppressWarnings("unused")
String host = "https://objectone.atlassian.net/";
  // int port = 8080;
  String userName = "gayan";
  String password = "123456";
  String ResponseData;
  DefaultHttpClient httpClient = new DefaultHttpClient();
  String jsonObj = 
		  "{"
		    + "\"fields\":" +
		    " {"
		    	+ "\"project\":"
		    	+ "{"
		    	+ "\"key\": \"AUTO\""
		    	+ "},"
		    
		    	+ "\"summary\": "+"\""+summary+"\","
		    
		    	+ "\"description\": "+"\""+description+"\","
		    
			    /*+ "\"assignee\":"
			    	+ "{"
			    	+ "\"name\": \"Gayan Harshana\""
			    	+ "},"
			    
				 + "\"reporter\":"
				 	+ "{"
				 	+ "\"name\": \"Gayan Harshana\""
				 	+ "},"
			      */  
			    + "\"issuetype\":" 
			    	+ "{" 
			    	+ "\"name\": \"Bug\"" 
			    	+ "}"
			    	
		     + "}"
		   + "}";
  
  


  HttpHost targetHost = new HttpHost("objectone.atlassian.net",
    -1, "https");

  httpClient.getCredentialsProvider().setCredentials(
    new AuthScope(targetHost.getHostName(), targetHost.getPort(),
      targetHost.getSchemeName()),
    new UsernamePasswordCredentials(userName, password));

  HttpPost httpPost = new HttpPost(
    "https://objectone.atlassian.net/rest/api/2/issue/");
  StringEntity entity = new StringEntity(jsonObj);
  entity.setContentType("application/json");
  httpPost.setEntity(entity);

  // Create AuthCache instance
  AuthCache authCache = new BasicAuthCache();
  // Generate BASIC scheme object and add it to the local auth cache
  BasicScheme basicAuth = new BasicScheme();

  authCache.put(targetHost, basicAuth);

  // Add AuthCache to the execution context
  BasicHttpContext localcontext = new BasicHttpContext();
  localcontext.setAttribute(ClientContext.AUTH_CACHE, authCache);

  try {

   HttpResponse httpResponse = httpClient.execute(httpPost,
     localcontext);
   HttpEntity entitydata = httpResponse.getEntity();
   ResponseData = new String(EntityUtils.toByteArray(entitydata));
   System.out.println(ResponseData);
  } catch (ClientProtocolException e) {
   e.printStackTrace();
  } catch (IOException e) {
   e.printStackTrace();
  }
  httpClient.getConnectionManager().shutdown();
 }
}
