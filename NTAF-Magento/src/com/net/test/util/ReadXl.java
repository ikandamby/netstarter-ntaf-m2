
package com.net.test.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


import jxl.Sheet;
import jxl.Workbook;

public class ReadXl {

	public static HSSFRow currentRow=null;
	public static long cellNumericValue = 0;

	public int totalRows =0;

	public String rParaDescription ="";
	public String rParaName ="";
	
	public String readFileName = "";	
	
	public ArrayList<String> links = new ArrayList<String>();
	public ArrayList<String> linkTitle = new ArrayList<String>();
	public ArrayList<Integer> totalSize = new ArrayList<Integer>();	
	public ArrayList<String> linksXpathName = new ArrayList<String>();
	public ArrayList<Integer> totalXpath = new ArrayList<Integer>();
	
	public int totalLinkSize =0;
	public int totalNoOfXpath =0;
	
	public String readFile="" ;
	public String brandName = "";
	public String brandXpath = "";
	public String productDescription = "";
	public String productValue = "";
	
	public String userfield = "";
	public String userFieldValue = "";
	
	public String description = "";
	public String paraXpath = "";	
	public String message = "";
		
	public String className = "";
	public String isExecute = "";
	public String scenario = "";
	public String methodName = "";

	
	public int i =0;
	public int j =0;	
	public int x =0;
	public int y =0;
	public int brandIdRow=0;
	public int brandIdColumn=0;
	
	public static String dataFolder = "";
	public static String propertiesFolder = "";
	public static String screenShot = "";
	public static String executedFileName = "";
	public static String resultFolder = "";
	public static String htmlResultFolder = "";
	public static String imageFolder = "";
	public static String reRunLogFile = "";
	public static String logFolder = "";
	public static String cmOrginalIm = "";
	public static String cmFailedIm = "";
	public static String cmVerifiedIm = "";
	public static String uJFolder = "";
	public static String chromeDriver = "";
	
	

	public int totalNoOfColoums =0;
	
	public ArrayList<String> nameOfLink = new ArrayList<String>();
	public ArrayList<String> titleOfLink = new ArrayList<String>();
	public ArrayList<Integer> totalNoOFLinksPerColoum = new ArrayList<Integer>();

	public int totalNoOfLinks =0;

	
	public void getMainLocations(String Test_Suite_Type) throws Exception
	{
		String currentLocation = System.getProperty("user.dir");
		if(currentLocation.indexOf("Functest\\NTAF-Magento") >= 0)
		{
			
			if(Test_Suite_Type.equalsIgnoreCase("Full"))
			{
				executedFileName = currentLocation+"/Test_Data/Executed Classes and Scenarios In Full Test.xls";					
			}
			else if(Test_Suite_Type.equalsIgnoreCase("Mini"))
			{
				executedFileName = currentLocation+"/Test_Data/Executed Classes and Scenarios In Mini Test.xls";				
			}
			else
			{
				executedFileName = currentLocation+"/Test_Data/Executed Classes and Scenarios In UI Test.xls";	
			}
			
			dataFolder = currentLocation+"/Test_Data/";
			propertiesFolder = currentLocation+"/Properties/";
			screenShot = currentLocation+"/Screenshots/";
			resultFolder = currentLocation+"/Test_Results/";
			htmlResultFolder = currentLocation+"/Test_Html_Results/";
			imageFolder = currentLocation+"/images/";
			reRunLogFile = currentLocation+"/target/rerun/";
			uJFolder = currentLocation+"/Test_UJ_Results/";
			logFolder = currentLocation+"/log/";
			chromeDriver = currentLocation+"/lib/chromedriver.exe";
			
			cmOrginalIm = currentLocation+"/Compare_ScreenShots/Orginal_Images/";
			cmFailedIm = currentLocation+"/Compare_ScreenShots/Fail_Images/";
			cmVerifiedIm = currentLocation+"/Compare_ScreenShots/Verify_Images/";
			
		}
		else
		{
			if(Test_Suite_Type.equalsIgnoreCase("Full"))
			{
				executedFileName = currentLocation+"/NTAF-Magento/Test_Data/Executed Classes and Scenarios In Full Test.xls";					
			}
			else if(Test_Suite_Type.equalsIgnoreCase("Mini"))
			{
				executedFileName = currentLocation+"/NTAF-Magento/Test_Data/Executed Classes and Scenarios In Mini Test.xls";				
			}
			else
			{
				executedFileName = currentLocation+"/NTAF-Magento/Test_Data/Executed Classes and Scenarios In UI Test.xls";	
			}
			
			dataFolder = currentLocation+"/NTAF-Magento/Test_Data/";
			propertiesFolder = currentLocation+"/NTAF-Magento/Properties/";
			screenShot = currentLocation+"/NTAF-Magento/Screenshots/";
			resultFolder = currentLocation+"/NTAF-Magento/Test_Results/";
			htmlResultFolder = currentLocation+"/NTAF-Magento/Test_Html_Results/";
			imageFolder = currentLocation+"/NTAF-Magento/images/";
			reRunLogFile = currentLocation+"/NTAF-Magento/target/rerun/";
			uJFolder = currentLocation+"/NTAF-Magento/Test_UJ_Results/";
			logFolder = currentLocation+"/NTAF-Magento/log/";
			chromeDriver = currentLocation+"/NTAF-Magento/lib/chromedriver.exe";
			cmOrginalIm = currentLocation+"/NTAF-Magento/Compare_ScreenShots/Orginal_Images/";
			cmFailedIm = currentLocation+"/NTAF-Magento/Compare_ScreenShots/Fail_Images/";
			cmVerifiedIm = currentLocation+"/NTAF-Magento/Compare_ScreenShots/Verify_Images/";
		}
	
	}
	public String getReadFileName() throws Exception
	{	
		readFile=getReadFile();
		return readFile;	
	}
	
	public HashMap<String, String> getMainParameters()throws Exception {
		
		//getMainLocations();
		//System.out.println("Read File "+co.getReadFile());
		//System.out.println("Read Filexxxx " +getReadFileName());
		HashMap<String, String> getParameters = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet(0);
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			rParaDescription = s.getCell(0, i).getContents();
	 			rParaName = s.getCell(1, i).getContents();
	 			getParameters.put(rParaDescription, rParaName);
	 			
	 			y++;
	 			//System.out.println("aaaass"+rParaDescription);
	 		}
	 	}
	 
	 	return getParameters;
		
	}
	
	public HashMap<String, String> getMainChkOutDetails()throws Exception {
		

		HashMap<String, String> getProdouctDetails = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("Checkout Process");
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			productDescription = s.getCell(0, i).getContents();
	 			productValue = s.getCell(1, i).getContents();
	 			getProdouctDetails.put(productDescription, productValue);
	 			
	 			y++;
	 			
	 		}
	 	}
	 
	 	return getProdouctDetails;
		
	}
	
	
	
	public HashMap<String, String> getParametersXpath()throws Exception {
		
		HashMap<String, String> getParametersXpath = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(propertiesFolder+getReadFileName().replaceAll("Data", "Properties").replaceAll("Data", "Properties"));
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("Parameters");
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			description = s.getCell(1, i).getContents();
	 			paraXpath = s.getCell(2, i).getContents();
	 			getParametersXpath.put(description, paraXpath);
	 			
	 			y++;
	 			
	 		}
	 	}
	 
	 	return getParametersXpath;
		
	}
	
	public HashMap<String, String> getLocator()throws Exception {
		
		HashMap<String, String> getLocator = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(propertiesFolder+getReadFileName().replaceAll("Data", "Properties").replaceAll("Data", "Properties"));
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("Parameters");
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			description = s.getCell(2, i).getContents();
	 			paraXpath = s.getCell(3, i).getContents();
	 			getLocator.put(description, paraXpath);
	 			
	 			y++;
	 			
	 		}
	 	}
	 
	 	return getLocator;
		
	}
	
	
	
	public HashMap<String, String> getMessages()throws Exception {
		
		
		HashMap<String, String> getMessages = new HashMap<String, String>();
		y=0;
		getMessages.clear();
		
		FileInputStream fi=new FileInputStream(propertiesFolder+getReadFileName().replaceAll("Data", "Properties").replaceAll("Data", "Properties"));
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("Messages");
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			description = s.getCell(1, i).getContents();
	 			message = s.getCell(2, i).getContents();
	 			getMessages.put(description, message);
	 			
	 			y++;
	 			
	 		}
	 	}
	 
	 	return getMessages;
		
	}
	
	public HashMap<String, String> getExecutedMethods()throws Exception {
	
		HashMap<String, String> getExecutedMethods = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(propertiesFolder+getReadFileName().replaceAll("Data", "Properties").replaceAll("Data", "Properties"));
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("Executed Methods");
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows)
	 		{
	 			
	 			methodName = s.getCell(1, i).getContents();
	 			isExecute = s.getCell(2, i).getContents();
	 			
	 			getExecutedMethods.put(methodName, isExecute);
	 			
	 			y++;
	 			
	 		}
	 	}
	 
	 	return getExecutedMethods;
		
	}
	
	public void getLinksXpath(String readSheet)throws Exception {
		
		String readSheetName = readSheet;
		//int totalRows =0;
		int totalColumns=0;
		int j=0;
		//int i=0;
		linksXpathName.clear();
		totalXpath.clear();

		//System.out.println("sheetName R "+readSheetName);
		try {
			
			FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
				
			Workbook w=Workbook.getWorkbook(fi);
			Sheet s=w.getSheet(readSheetName);
			
			totalRows = s.getRows()-1;
			totalColumns = s.getColumns();
			
			
				for(j=0;j<totalColumns;j=j+2){
					
					if(s.getCell(j, 0).getContents()!=""){ 	
			 			
			 			linksXpathName.add(s.getCell(j, 0).getContents());
			 			 			
			 		}

					
					totalXpath.add(linksXpathName.size());
			
		}	
			totalNoOfXpath = totalXpath.size();
	
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	public void getLinksFromXLSheet(String readSheet)throws Exception{
		
		links.clear();
		linkTitle.clear();
		totalSize.clear();
		String readSheetName = readSheet;
		int totalRows =0;
		int totalColumns=0;
		int j=0;
		int i=0;


		//System.out.println("sheetName R "+readSheetName);
		try {
			
			FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
				
			Workbook w=Workbook.getWorkbook(fi);
			Sheet s=w.getSheet(readSheetName);
			
			totalRows = s.getRows()-1;
			totalColumns = s.getColumns();
			
				for(j=0;j<totalColumns;j=j+2){
					for(i=0;i<=totalRows;i++){
								
				 		if(s.getCell(j, i).getContents()!=""){ 	
				 			
				 			links.add(s.getCell(j, i).getContents());
				 			 			
				 		}
				 		if(s.getCell(j+1, i).getContents()!=""){ 	
				 			
				 			linkTitle.add(s.getCell(j+1, i).getContents());
					 			 			
				 		}
				 		y++;
				 		
				 	}
					
					totalSize.add(links.size());
			
		}	
			totalLinkSize = totalSize.size();
	
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}	
	
	public String getBrandXpath()throws Exception {
		
		y=0;
		getParametersXpath();	
		FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("Main Navigation");
		
		brandName = getMainChkOutDetails().get("Product Brand Type");	
		
		//System.out.println("brandName"+brandName);
		
		if(s.findCell(brandName)!=null)
		{
			brandIdRow =s.findCell(brandName).getRow();
		 	brandIdColumn = s.findCell(brandName).getColumn();
		 	
		 	//System.out.println("brandIdRow"+brandIdRow);
		 	//System.out.println("brandIdColumn"+brandIdColumn);
		 	
		 	brandXpath = getParametersXpath().get(s.getCell(brandIdColumn, 0).getContents()).replace("{id}", "["+(brandIdRow)+"]");
		 	
		}
		
		return brandXpath;
		
		
	}
	
	public String getMainBrandXpath(String mainBrandName)throws Exception {
	
		y=0;
		getParametersXpath();	
		FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("Main Navigation");
		
		brandName = mainBrandName;	
		
		//System.out.println("brandName"+brandName);
	
		if(s.findCell(brandName)!=null)
		{
			brandIdRow =s.findCell(brandName).getRow();
		 	brandIdColumn = s.findCell(brandName).getColumn();
		 	brandIdRow=brandIdRow+1;
		 	
		 //	System.out.println("brandIdRow"+brandIdRow);
		 //	System.out.println("brandIdColumn"+brandIdColumn);
		 	brandXpath = getParametersXpath().get(s.getCell(brandIdColumn, 0).getContents()).replace("{id}", "["+(brandIdRow)+"]");
		 	
		}
		//System.out.println("brand xpath"+brandXpath);
		
		return brandXpath;
		
		
	}
	
	
	public HashMap<String, String> getMainUserDetails()throws Exception 
	{
		
		HashMap<String, String> getUserDetails = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("User Registration");
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			userfield = s.getCell(0, i).getContents();
	 			userFieldValue = s.getCell(1, i).getContents();
	 			getUserDetails.put(userfield, userFieldValue);
	 			
	 			y++;
	 			//System.out.println("xxxxsssqq"+productValue);
	 		}
	 	}
	 
	 	return getUserDetails;
		
	}
	public HashMap<String, String> getMyAccountDetails()throws Exception {
		
		HashMap<String, String> getAccountDetails = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("My Account");
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			userfield = s.getCell(0, i).getContents();
	 			userFieldValue = s.getCell(1, i).getContents();
	 			getAccountDetails.put(userfield, userFieldValue);
	 			
	 			y++;
	 			//System.out.println("xxxxsssqq"+productValue);
	 		}
	 	}
	 
	 	return getAccountDetails;
		
	}
	
	public HashMap<String, String> getSocialMediaDetails()throws Exception 
	{
		
		HashMap<String, String> getSocialMediaDetails = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("Social Media Links");
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			userfield = s.getCell(0, i).getContents();
	 			userFieldValue = s.getCell(1, i).getContents();
	 			getSocialMediaDetails.put(userfield, userFieldValue);
	 			
	 			y++;
	 			//System.out.println("xxxxsssqq"+productValue);
	 		}
	 	}
	 
	 	return getSocialMediaDetails;
		
	}
	public HashMap<String, String> getTAFRegressionDetails()throws Exception {
		
		//System.out.println("Read File "+co.getReadFile());
		//System.out.println("Read Filexxxx " +getReadFileName());
		HashMap<String, String> getRegressionDetails = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet("Regression");
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			userfield = s.getCell(0, i).getContents();
	 			userFieldValue = s.getCell(1, i).getContents();
	 			getRegressionDetails.put(userfield, userFieldValue);
	 			
	 			y++;
	 			//System.out.println("xxxxsssqq"+productValue);
	 		}
	 	}
	 
	 	return getRegressionDetails;
		
	}
	public HashMap<String, String> isClassExecuted()throws Exception {
		
		HashMap<String, String> classExecutionList = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(executedFileName);
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet(0);
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			className = s.getCell(0, i).getContents();
	 			isExecute = s.getCell(2, i).getContents();
	 			classExecutionList.put(className, isExecute);
	 			
	 			y++;
	 			//System.out.println(" className "+className+" isExecute "+isExecute);
	 		}
	 	}
	 
	 	return classExecutionList;
		
	}
	
	public HashMap<String, String> isScenarioExecuted()throws Exception {
		
		HashMap<String, String> methodExecutionList = new HashMap<String, String>();
		y=0;
		
		FileInputStream fi=new FileInputStream(executedFileName);
			
		Workbook w=Workbook.getWorkbook(fi);
		Sheet s=w.getSheet(0);
		totalRows = s.getRows()-1;	
	 	
		for(i=1;i<=totalRows;i++){
	 		if(y<totalRows){
	 			scenario = s.getCell(1, i).getContents();
	 			isExecute = s.getCell(2, i).getContents();
	 			methodExecutionList.put(scenario, isExecute);
	 			
	 			y++;
	 			//System.out.println("Scenario "+scenario+" isExecute "+isExecute);
	 		}
	 	}
	 
	 	return methodExecutionList;
		
	}
	
	public String getReadFile()throws Exception {
		int i =0;
					
		FileInputStream fileInputStream = new FileInputStream(executedFileName);
	
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet worksheet = workbook.getSheetAt(1);
		currentRow = worksheet.getRow(i+1);
		
		   if(currentRow.getCell(1).getCellType() == 0){
		    	cellNumericValue = (long)currentRow.getCell(1).getNumericCellValue();
		    	readFileName = String.valueOf(cellNumericValue);
		    }
		   else if(currentRow.getCell(1).getCellType() == 1){
			   readFileName =currentRow.getCell(1).getStringCellValue();
		   }		  
		return readFileName;
	}
	
	public void getLinksFromDataFile(String readSheet)throws Exception
	{
		
		nameOfLink.clear();
		titleOfLink.clear();
		totalNoOFLinksPerColoum.clear();
		String readSheetName = readSheet;
		int totalRows =0;
		int totalColumns=0;
		int j=0;
		int i=0;

		try {
			
			FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
				
			Workbook w=Workbook.getWorkbook(fi);
			Sheet s=w.getSheet(readSheetName);
			
			totalRows = s.getRows()-1;
			totalColumns = s.getColumns();
			
			for(j=0;j<totalColumns;j=j+2)
			{
				for(i=0;i<=totalRows;i++)
				{
					if(s.getCell(j, i).getContents()!="")
					{ 	
				 		nameOfLink.add(s.getCell(j, i).getContents());
				 			 			
				 	}
				 	if(s.getCell(j+1, i).getContents()!="")
				 	{ 	
				 		titleOfLink.add(s.getCell(j+1, i).getContents());
					 			 			
				 	}
				 	y++;
				 		
				 }
					
				totalNoOFLinksPerColoum.add(nameOfLink.size());
			
			}	
			totalNoOfColoums = totalNoOFLinksPerColoum.size();
	
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void getMyAccountLinksFromDataFile(String readSheet)throws Exception
	{
		
		nameOfLink.clear();
		titleOfLink.clear();
		totalNoOFLinksPerColoum.clear();
		String readSheetName = readSheet;
		int totalRows =0;
		
		int j=0;
		int i=0;

		try {
			
			FileInputStream fi=new FileInputStream(dataFolder+getReadFileName());
				
			Workbook w=Workbook.getWorkbook(fi);
			Sheet s=w.getSheet(readSheetName);
			
			totalRows = s.getRows()-1;

			for(i=0;i<=totalRows;i++)
			{
				if(s.getCell(2, i).getContents()!="")
				{ 	
			 		nameOfLink.add(s.getCell(j, i).getContents());
			 			 			
			 	}
			 	if(s.getCell(3, i).getContents()!="")
			 	{ 	
			 		titleOfLink.add(s.getCell(j+1, i).getContents());
					 			 			
			 	}
			 	y++;
				 		
			 }
					
			totalNoOFLinksPerColoum.add(nameOfLink.size());
			

	
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	
}