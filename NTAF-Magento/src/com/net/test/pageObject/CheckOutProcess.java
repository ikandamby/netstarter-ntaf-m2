package com.net.test.pageObject;

import java.util.Calendar;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import com.net.test.data.CheckoutTestData;
import com.net.test.data.HomeTestData;
import com.net.test.data.MyAccountTestData;
import com.net.test.data.UserCreationTestData;
import com.net.test.util.ReadXl;
import com.net.test.util.TestCommonMethods;
import com.net.test.util.WriteResults;
 

public class CheckOutProcess extends CheckoutTestData
{	
	WriteResults wResult =  new WriteResults();
	ReadXl readParametrs  = new ReadXl();
	MyAccountTestData myAccountTestData = new MyAccountTestData();
	//TestCommonMethods common = new TestCommonMethods();
	//SendTextMessage sendSMS = new SendTextMessage();
	//CreateIssue createIssue = new CreateIssue();
	HomeTestData homeTestData = new HomeTestData();
	ShoppingCartPage shoppingCart = new ShoppingCartPage();
	UserCreation user = new UserCreation();
	UserCreationTestData userCreationTestData = new UserCreationTestData();
	
	public String productNameInDetailPage = "";
	public String productPriceInDetailPage = "";
	
	public int i = 0;
	public int j = 0;	
	
	public boolean isVirtualProduct = false;
	public boolean isBillingInfoValidation = false;
	public boolean isDeliveryInfoValidation = false;
	public boolean isShippingInfoValidation = false;
	private boolean isLoggedUser = false;
	public String orderStatus = "";
	public String orderNumber = "";
	public double totalPriceBeforeShipping = 0.0;
	private boolean hasShippingMethod = false;
	public String shippingCharges = "";
	public double totalPriceAftereShipping = 0.0;
	public String mailReaderUserName = "";
	private boolean shippingToDifferentAddress = false;
	
	
	public void resultSheetName(String resultSheet)throws Exception 
	{
		writeFileName=resultSheet;			
		common.resultSheetName(resultSheet);
		shoppingCart.resultSheetName(resultSheet);		
	}

	public void loginToMyAccount()throws Exception 
	{
		if(common.navigateToLoginPage())
		{
			common.login();			
			
			String pwValidation = common.passwordMatch();
			
			if(pwValidation =="")
			{
				writeTestResults("Verify My Account page", "User can navigate to my account", "User can navigate to my account","",true,false);
			}
			else
			{
				writeTestResults("Verify My Account page", "User can navigate to my account", "User can not navigate to my account"+pwValidation,"",false,false);
			}			
			isLoggedUser = true;			
		}
		else
		{
			writeTestResults("Verify Login page", "Login page should be loaded", "Login link is not present","",false,true);			
		}
	}	
	
	public void loginToMyAccountForNewlyCreatedUser()throws Exception 
	{
		if(common.navigateToLoginPage())
		{
			common.loginForOthreUser(billingJoinUserEmail, billingCustPw);
			
			String pwValidation = common.passwordMatch();
			if(pwValidation =="")
			{
				writeTestResults("Verify newly created user can again login to my account", "User can navigate to my account", "User can navigate to my account","",true,false);
			}
			else
			{
				writeTestResults("Verify newly created user can again login to my account", "User can navigate to my account", "User can not navigate to my account"+pwValidation,"",false,false);
			}			
			isLoggedUser = true;
		}
		else
		{
			writeTestResults("Verify Login page", "Login page should be loaded", "Login link is not present","",false,true);
		}
	}	
	
	public void logout() throws Exception 
	{
		if(!myAccountTestData.mouseMoveToMyAccount.isEmpty())
		{
			mouseMove(myAccountTestData.mouseMoveToMyAccount);			
		}		
		clearCookies();		
	}
	
	public void removeUserFromAdimin() throws Exception
	{
		common.updateUserAccountInAdmin(billingJoinUserEmail);
		
		openPage(common.siteUrl);
	}
	
	public void removeNewsLetterUserFromAdimin() throws Exception
	{
		common.updateUserAccountInAdmin(billingJoinUserEmail);
		
		openPage(common.siteUrl);
	}
	
	
	public void addProductIntoCart() throws Exception
	{ 
		common.clickLogo();
		
		if(!common.selectBrand())
		{
			writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", "User can select the product", 
					"Selected brand ("+TestCommonMethods.categoryName+") is not dispalyed or page is not loaded properly","",false,true);
		}
		else
		{
			writeTestResults("Verify user can select category in main navigation", "User can navigate to '"+TestCommonMethods.categoryName+"' page", "Selected category is '"+TestCommonMethods.categoryName+"'","",true,false);			
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
		{
			common.selectProductFromLeftCategory();			
		}	
		
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			if(!common.productGrid())
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","Grid link should be present",
						"Link is not displayed or xpath ("+productGrid+") is incorrect","",false,true);
			}
		}
		
		common.navigateToProductDetailsPage();	
		
		compareProductDetails(productPriceInProductDetail());		
		
		common.addToCart("Verify the cart drop down shows number of items in the shopping cart when user added one product");
				
		if(isElementPresent(myCartXpath))
		{					
			if(common.totalItemInHeaderCart() < 0)
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", "'1 Item' should be displayed in the 'mini cart'", 
						"'"+common.totalItemInHeaderCart()+"' Items' is displayed in the 'mini cart'","",false,true);
			}
			
			common.viewCart();
		}
		else
		{
			writeTestResults("Verify the 'Cart Item' field", "'Cart Item' field should be displayed", "'Cart Item' is not displayed","",false,true);
		}
	}	
	
	
	public void shippingToDifferentAddress() throws Exception
	{ 	
		shippingToDifferentAddress = true;
	}		
	
	public void selectProductFromLeftCategory() throws Exception
	{
		if(isElementPresent(leftCategory))
		{
			click(leftCategory);
		}
		else
		{
			writeTestResults("Verify left categories displayed in category page", "Left Categories should be displayed", "Left category section is not displayed or xpath is incorrect","",false,true);
		}
	}	
	
	public boolean productGrid() throws Exception
	{
		boolean foundProductGrid = true;
		if(isElementPresent(productGrid))
		{
			click(productGrid);
		}
		else
		{
			foundProductGrid =false;
		}
		return foundProductGrid;		
	}	
	
	public void productAvailability() throws Exception
	{
		if(common.productAvailability())
		{
			if(!TestCommonMethods.getProductSpPrice.isEmpty())
			{
				writeTestResults("Verify user can select product in category page", "User can navigate to '"+common.productName+"' details page", "Selected product name is '"+common.productName+"' , " +
						"price is '"+TestCommonMethods.getProductSpPrice+"' and special price is '"+common.productPrice+"'","",true,false);
			}
			else
			{
				writeTestResults("Verify user can select product in category page", "User can navigate to '"+common.productName+"' details page",
						"Selected product name is '"+common.productName+"' and price is '"+common.productPrice+"'","",true,false);
			}						
		}
	}	
	
	public void productNameInProductDetail() throws Exception
	{
		if(!groupProductTable.isEmpty() && isElementPresent(groupProductTable))
		{
			productNameInDetailPage = groupProductName;
		}
		else
		{
			if(isElementPresent(productNameXpathInProductDetailPage))
			{
				productNameInDetailPage = getText(productNameXpathInProductDetailPage);
			}
			else
			{
				productNameInDetailPage = "";
			}
		}					
	}
	
	public String productPriceInProductDetail() throws Exception
	{		
		if(isElementPresent(productPriceXpathInProductDetailPage))
		{			
			if(isElementPresent(productPriceXpathInProductDetailPage+hasPriceTagInDetailsPage))
			{
				productPriceInDetailPage = getText(productPriceXpathInProductDetailPage+hasPriceTagInDetailsPage);
			}
			else
			{
				productPriceInDetailPage = getText(productPriceXpathInProductDetailPage+hasSpecialPriceTagInDetailsPage);
			}			
		}
		else
		{
			if(productPriceInDetailPage.isEmpty())
			{
				productPriceInDetailPage = "";	
			}			
					
		}
		
		System.out.println("productPriceInDetailPage"+productPriceInDetailPage);
		return productPriceInDetailPage;		
	}	
	
	public void productNameCompareInProductDetail() throws Exception
	{
		if(!productNameInDetailPage.equalsIgnoreCase(TestCommonMethods.productName))
		{
			writeTestResults("Verify the name of the selected product is displaying in product detail page", "Expected product Name is "+TestCommonMethods.productName +" in product detail page", 
					"Actual product name is "+productNameInDetailPage+" in product detail page","",false,false);
		}
		else
		{
			writeTestResults("Verify the name of the selected product is displaying in product detail page", "Expected product Name is "+TestCommonMethods.productName+" in product detail page" ,
					"Actual product name is "+productNameInDetailPage+" in product detail page","",true,false);	
		}

	}
	public void productPriceCompareInProductDetail(String productPriceInDetailPage) throws Exception
	{
		System.out.println("sss"+common.getValueWitoutCurrency(productPriceInDetailPage));
		System.out.println("xx"+common.getValueWitoutCurrency(TestCommonMethods.productPrice));
		if(common.getValueWitoutCurrency(productPriceInDetailPage) != common.getValueWitoutCurrency(TestCommonMethods.productPrice))
		{
			writeTestResults("Verify the price of the selected product is displaying in product detail page", "Expected product price is "+common.productPrice +" in product detail page", 
					"Actual product price is "+productPriceInDetailPage+" in product detail page","",false,false);
		}
		else
		{
			writeTestResults("Verify the price of the selected product is displaying in product detail page", "Expected product price is "+common.productPrice +" in product detail page", 
					"Actual product price is "+productPriceInDetailPage+" in product detail page","",true,false);
		}
	}
	
//	public void productQTYCompareInProductDetail() throws Exception
//	{
//		if(isElementPresent(productQTYXpath))
//		{		
//			sendKeys(productQTYXpath, setQtyInDetailPage);	
//		}
//	}
//	
//	public void productQTYCompareInProductDetail() throws Exception
//	{
//		if(isElementPresent(productQTYXpath))
//		{		
//			sendKeys(productQTYXpath, setQtyInDetailPage);	
//		}
//	}

	public void addToCartButton() throws Exception
	{	
		writeTestResults("Verify the add to cart button is displayed in product detail page", "Add to cart button should be displayed in product detail page", 
				"Add to cart button is displayed in product detail page","",true,false);
		
		common.addToCartButton();
	}

	public void compareProductDetails(String productPriceInDetailPage) throws Exception
	{
		if(findElementInXLSheet(getMethodIsExecuted,"Product Name In Product Detail Page").equalsIgnoreCase("Yes"))
		{	
			productNameInProductDetail();	
			productNameCompareInProductDetail();
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Product Price In Product Detail Page").equalsIgnoreCase("Yes"))
		{
			if(!isElementPresent(groupProductTable))
			{
				productPriceCompareInProductDetail(productPriceInDetailPage);
			}			
		}	
		
	}
	
	public void addToCart() throws Exception
	{	
		if(!isDisplayed(addToCartButton))
		{
			if(!outOfStockProductMessage.isEmpty())
			{
				common.verifyOutOfStockMessage("Verify product availablity in product detail page");
			}			
		}	
		else
		{
			if(isExecuted(getMethodIsExecuted,"Product QTY In Product Detail Page"))
			{
				selectQTYInProdcutDetails();	
			}			
		}
		//then check product is config
		if(!productSizeClassName.isEmpty() && isElementPresent(productSizeClassName))
		{
			addToCartButtonForConfigProduct();		
		}			
		//Check procuct is gift card
		else if(!giftCardSenderNameXpath.isEmpty() && isElementPresent(giftCardSenderNameXpath))
		{
			addToCartButtonForGiftCard();
			
		}
		//check simple product
		else
		{			
			compareProductDetails(productPriceInProductDetail());
			
			addToCartButton();
		}
	}	
	
	public void selectQTYInProdcutDetails() throws Exception
	{		
		if(isDisplayed(simpleProductQtyTextBox))
		{
			String defaultValue = getAttribute(simpleProductQtyTextBox,getDefaultQtyAttribute);
			
			sendKeys(simpleProductQtyTextBox,setQtyInDetailPage);
				
			writeTestResults("Verify the user can enter the quantity of the product manually", "User can enter qty in the product details page and default " +
					"qty is "+defaultValue, "User can enter qty in the product details page and user entered qty is "+getAttribute(simpleProductQtyTextBox,getDefaultQtyAttribute),"",true,false);
		}		
	}
	
	public void addToCartButtonForGiftCard() throws Exception
	{		
		if(isElementPresent(giftCardSenderNameXpath))
		{
			sendKeys(giftCardSenderNameXpath,giftCardSenderName);
		}
		if(isElementPresent(giftCardSenderEmailXpath))
		{
			sendKeys(giftCardSenderEmailXpath,giftCardSenderEmail);
		}
		if(isElementPresent(giftCardRecipentNameXpath))
		{
			sendKeys(giftCardRecipentNameXpath,giftCardRecipentName);
		}
		if(isElementPresent(giftCardRecipentEmailXpath))
		{
			sendKeys(giftCardRecipentEmailXpath,giftCardRecipentEmail);
			
			isVirtualProduct = true;
		}
		if(isElementPresent(giftCardAmountDropdownXpath))
		{
			int dropDownSize = getDropdownSize(giftCardAmountDropdownXpath,"option");
			
			if(dropDownSize > 0)
			{
				selectIndex(giftCardAmountDropdownXpath,1);
				
				productPriceInDetailPage = getSelectedValueFromDropdown(giftCardAmountDropdownXpath,"option",1);				
			}
			else
			{
				writeTestResults("Verify user can select amount in gift card", "Dorpdown should have values", "Amount dropdown doesn't have any values","",false,true);
			}
		}		
		
		if(isElementPresent(giftCardAmountTxtXpath))
		{
			sendKeys(giftCardAmountTxtXpath,"100.00");
			
			productPriceInDetailPage = "$100.00";			
		}
		
		if(!isElementPresent(giftCardAmountTxtXpath) && !isElementPresent(giftCardAmountDropdownXpath))
		{ 		 
			productPriceInDetailPage = productPriceInProductDetail();			
		}		
		
		compareProductDetails(productPriceInDetailPage);
			
		addToCartButton();
		
		giftCardValidation();
	
	}
	
	public void giftCardValidation() throws Exception
	{
		if(!(isElementPresent(myCartXpath) && common.totalItemInHeaderCart() > 0))
		{
			if(!giftCardWithoutSenderName.isEmpty())
			{
				if(!getValidationText(giftCardWithoutSenderName).isEmpty())
				{
					if(getText(giftCardWithoutSenderName).equalsIgnoreCase(validationMsgGiftCardWithoutSenderName))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Sender name is empty","",false,true);	
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Sender name is empty and message is "+getText(giftCardWithoutSenderName),"",false,true);	
					}
				}
			}
			
			if(!giftCardWithoutSenderEmail.isEmpty())
			{
				if(!getValidationText(giftCardWithoutSenderEmail).isEmpty())
				{
					if(getText(giftCardWithoutSenderEmail).equalsIgnoreCase(validationMsgGiftCardWithoutSenderEmail))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Email is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Email is empty and message is "+getText(giftCardWithoutSenderEmail),"",false,true);
					}
				}
			}
			
			if(!giftCardWithoutRecipentName.isEmpty())
			{
				if(!getValidationText(giftCardWithoutRecipentName).isEmpty())
				{
					if(getText(giftCardWithoutRecipentName).equalsIgnoreCase(validationMsgGiftCardWithoutRecipentName))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Recipent name is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Recipent name is empty and message is "+getText(giftCardWithoutRecipentName),"",false,true);
					}
				}
			}
			
			
			if(!giftCardWithoutRecipentEmail.isEmpty())
			{
				if(!getValidationText(giftCardWithoutRecipentEmail).isEmpty())
				{
					if(getText(giftCardWithoutRecipentEmail).equalsIgnoreCase(validationMsgGiftCardWithoutRecipentEmail))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Reciepent Email is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Reciepent Email is empty and message is "+getText(giftCardWithoutRecipentEmail),"",false,true);
					}
				}
			}
			
			if(!giftCardWithoutDropdownAmount.isEmpty())
			{
				if(!getValidationText(giftCardWithoutDropdownAmount).isEmpty())
				{
					if(getText(giftCardWithoutDropdownAmount).equalsIgnoreCase(validationMsgGiftCardWithoutAmount))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Amount is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Amount is empty and message is "+getText(giftCardWithoutDropdownAmount),"",false,true);
					}
				}
			}
			
			if(!giftCardWithoutTxtAmount.isEmpty())
			{
				if(!getValidationText(giftCardWithoutTxtAmount).isEmpty())
				{
					if(getText(giftCardWithoutTxtAmount).equalsIgnoreCase(validationMsgGiftCardWithoutAmount))
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Amount is empty","",false,true);
					}
					else
					{
						writeTestResults("Verify user can added gift card", "Gift card should be added to the cart", "Amount is empty and message is "+getText(giftCardWithoutTxtAmount),"",false,true);
					}
				}
			}
		}	
	}	
	
	public void selectProductSizeByClick() throws Exception
	{		
		int productSizeBasket = getElementSize(productSizeClassName);
		String sizeXpath = "";
		
		if(productSizeBasket!=0)
		{
			if(productSizeBasket==1)
			{
				sizeXpath = productSizeBlock.replace("{id}", "");
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
				}
				else
				{
					writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
				}
			}
			else
			{
				sizeXpath = productSizeBlock.replace("{id}", "");
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
				}
				else
				{
					for(int i=2;i<=productSizeBasket;i++)
					{
						sizeXpath = productSizeBlock.replace("{id}", "["+i+"]");
						
						if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
						{
							click(sizeXpath);
						}
						else
						{
							writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
						}
					}						
				}			
			}
		}			
	}

	public void addToCartButtonForConfigProduct() throws Exception
	{	
		compareProductDetails(productPriceInProductDetail());
		
		writeTestResults("Verify the add to cart button is displayed in product detail page", "Add to cart button should be displayed in product detail page", 
				"Add to cart button is displayed in product detail page","",true,false);
		
		int productSizeBasket = getElementSize(productSizeClassName);
		String sizeXpath = "";
		
		System.out.println("productSizeBasket"+productSizeBasket);
		
		if(productSizeBasket!=0)
		{
			/**
			 *Basket has only one value 
			 */
			if(productSizeBasket==1)
			{
				sizeXpath = productSizeBlock.replace("{id}", "");
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
					
					clickAndWait(addToCartButton);
					
					if(isErrorElementPresent(productDetailsErrorMsg))
					{
						writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Following error found :"+getText(productDetailsErrorMsg),"",false,true);
					}
					else
					{
						writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", "Product is added to shopping cart","",true,false);	
					}
				}
				else
				{
					writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
				}
			}
			else
			{
				sizeXpath = productSizeBlock.replace("{id}", "");							
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);				
					
					click(addToCartButton);
					
					if(isErrorElementPresent(productDetailsErrorMsg))
					{
						selectProductFromBasket(productSizeBasket,sizeXpath);					
					}
					else
					{
						writeTestResults("User can add the product in to the shopping cart", "Product should added to shopping cart", "Product is added to shopping cart","",true,false);
					}					
				}
				else
				{					
					selectProductFromBasket(productSizeBasket,productSizeBlock);												
				}			
			}
		}	
	}	
	
	public void selectProductFromBasket(int productSizeBasket,String sizeXpath) throws Exception
	{
		i = 2;
		
		while(i<=productSizeBasket)
		{
			for(i=2;i<=productSizeBasket;i++)
			{
				sizeXpath = productSizeBlock.replace("{id}", "["+i+"]");
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
					
					click(addToCartButton);
					
					if(!isElementPresent(productDetailsErrorMsg))
					{
						i = productSizeBasket+1;
						
						break;												
					}			
					
					
				}
				else
				{
					writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
				}
				
				if(i==productSizeBasket)
				{
					writeTestResults("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock","",false,true);
				}
			}						
		}
	}	
	
	public void viewCart() throws Exception
	{ 
		if(common.totalItemInHeaderCart()==0)
		{
			writeTestResults("User can add the product in to the shopping cart", "Header Cart should be updated", "Cart is not updated","",false,true);
		}		
		else if(!isElementPresent(viewCartId))
		{
			writeTestResults("Verify the my cart is displayed in header", "My cart should be displayed in header", "My cart button is not displayed in the header","",false,true);
		}
		else
		{
			writeTestResults("Verify the my cart is displayed in header", "My cart should be displayed in header", "My cart button is displayed in the header","",true,false);
			
			if(!isDisplayed(viewCart))
			{
				click(viewCartId);
				
				Thread.sleep(2000);
				
				clickAndWait(viewCart);					
			}
			else if(isDisplayed(viewCart))
			{
				clickAndWait(viewCart);					
			}			
			else
			{
				writeTestResults("Verify the my cart is displayed in header", "My cart should be displayed in header", "My cart button is not displayed in the header","",false,true);
			}			
		}	
	}
	
	public void titleInshoppingCartPage() throws Exception
	{	
		common.titleInshoppingCartPage(writeFileName);
	}
		
	public void productPriceInshoppingCartPageForGuest() throws Exception
	{
		common.productPriceInshoppingCartPageForGuest(common.productPrice,writeFileName);	
	}
	
	public void productPriceInshoppingCartPageForMember() throws Exception
	{
		common.productPriceInshoppingCartPageForGuest(common.productPrice,writeFileName);		
	}
	
	public void productNameInshoppingCartPage() throws Exception
	{
		common.productNameInshoppingCartPage(common.productName,writeFileName);
				
	}
	
	public void redeemCouponInShoppingcart() throws Exception
	{
		if(isElementPresent(txtCouponCodeXpath) && isElementPresent(btnCouponCode))
		{
			common.productGrandTotalInshoppingCartPage();
			
			sendKeys(txtCouponCodeXpath,txtCouponCodeFixed);
			
			click(btnCouponCode);			
			
			if(isDisplayed(msgSuccessxpath))
			{
				writeTestResults("Validate coupon code with valid value when coupon type is 'fixed amount discount'", getText(msgSuccessxpath)+" message should be displayed.",
						getText(msgSuccessxpath)+" message is displayed.","",true,false);
				
				
				shoppingCart.verifyGrandTotalAfterRedeemForFixed(TestCommonMethods.grandTotalInShoppingcart);
			}
			else
			{
				if(isDisplayed(msgErrorxpath))
				{
					writeTestResults("Validate coupon code with valid value when coupon type is 'fixed amount discount'", "Coupon updated message should be displayed.",
							"Following message is displayed. "+getText(msgErrorxpath),"",false,true);
				}
				else
				{
					writeTestResults("Validate coupon code with valid value when coupon type is 'fixed amount discount'", "Coupon updated  message should be displayed.",
							"Validation xpath is not displayed or incorrect xpath","",false,true);
				}				
			}
			
			common.productGrandTotalInshoppingCartPage();
			
		}
		else
		{
			writeTestResults("Validate coupon code with valid value when coupon type is 'fixed amount discount'", "Coupon updated  message should be displayed.",
					"coupon code txt box or button is not displayed or incorrect xpath","",false,true);
		}				
	}	

	
	public void proceedToCheckoutButton() throws Exception
	{
		
	
		if(!isElementPresent(proceedToCheckoutButtonInShoppingCart))
		{
			writeTestResults("Verify the Proceed to checkout button is displayed in shopping cart page", "Proceed to checkout button should be displayed in shopping cart page", 
					"Proceed to checkout button is not in the correct place or button is not displayed in the shopping cart page","",false,true);
		}
		else
		{
			writeTestResults("Verify the Proceed to checkout button is displayed in shopping cart page", "Proceed to checkout button should be displayed in shopping cart page", 
					"Proceed to checkout button is displayed in shopping cart page","",true,false);
			
			clickAndWait(proceedToCheckoutButtonInShoppingCart);
		}
	}
	
	public void memberCheckoutLoginWithEmail() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"Order review details in checkout page"))
		{
			verifyProductPriceInOrderReview();
			
			verifyProductGrandTotalPriceBeforeShipping();
			
			verifyProductNameInOrderReview();
		}
			
		if(isDisplayed("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a"))
		{
			click("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a");
		}
		
		verifyCheckoutPageForMember();
			
		validateLoginWithoutEmail();
		
		validateLoginWithoutPw();
			
		validateLoginInvalidEmail();
			
		validateLoginInvalidPW();
			
		validateLoginValidDetails();
		
	}	
	
	public void verifyCheckoutPageForMember() throws Exception
	{
		
		if(isExecuted(getMethodIsExecuted,"Checkout As Member - separate buttons for login user name and pw"))
		{
			if(isElementPresent(txtCheckoutMemberEmail) && isElementPresent(btnCheckoutMemberSubmitButton))
			{
				writeTestResults("Verify Compulsory Display Fields in select checkout method page", "Email,submit buttons should displayed for login section", 
						"Email,submit buttons is displayed for login section","",true,false);
			}
			else
			{
				writeTestResults("Verify Compulsory Display Fields in select checkout method page", "Email, submit buttons should displayed for login section", 
						"Email or submit buttons is not displayed for login section","",false,true);
			}						
		}
		else
		{
			if(isElementPresent(txtCheckoutMemberEmail))
			{
				writeTestResults("Verify Compulsory Display Fields in select checkout method page", "Email,Password, submit buttons should displayed for login section",
						"Email,Password, submit buttons is displayed for login section","",true,false);
			}
			else
			{
				writeTestResults("Verify Compulsory Display Fields in select checkout method page", "Email,Password, submit buttons should displayed for login section", 
						"Email or Password or submit buttons is not displayed for login section","",false,true);				
			}	
		}
	}
	
	public void validateLoginWithoutEmail() throws Exception
	{
		if(isDisplayed("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a"))
		{
			click("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a");
		}
		
		sendKeys(txtCheckoutMemberEmail,"");
		
		Thread.sleep(1000);
		
		click(btnCheckoutMemberSubmitButton);	
		
		Thread.sleep(1000);
		
		if(isDisplayed(checkoutMemberEmptyEmailXpath))
		{
			if(getText(checkoutMemberEmptyEmailXpath).equalsIgnoreCase(msgCheckoutMemberEmptyEmail))
			{
				writeTestResults("Verify Returning Customer Login with empty email", msgCheckoutMemberEmptyEmail+" Error message should be displayed", 
						msgCheckoutMemberEmptyEmail+" Error message is displayed","4270c12e",true,false);
			}
			else
			{
				writeTestResults("Verify Returning Customer Login with empty email", msgCheckoutMemberEmptyEmail+" Error message should be displayed", 
						getText(checkoutMemberEmptyEmailXpath)+" Error message is displayed","4270c12e",true,false);
			}			
		}
		else
		{
			writeTestResults("Verify Returning Customer Login with empty email", msgCheckoutMemberEmptyEmail+" Error message should be displayed", 
					checkoutMemberEmptyEmailXpath +" xpath is not displayed","4270c12e",false,true);
			
			goBack();
		}	
			
		pageRefersh();

	}
	
	public void validateLoginWithoutPw() throws Exception
	{
		if(isDisplayed("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a"))
		{
			click("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a");
		}
		
		if(isExecuted(getMethodIsExecuted,"Checkout As Member - separate buttons for login user name and pw"))
		{
			sendKeys(txtCheckoutMemberEmail,myAccountTestData.loginEmailAddress);
			
			Thread.sleep(1000);
			
			click(btnCheckoutMemberSubmitButton);
			
			Thread.sleep(1000);
			
			sendKeys(txtCheckoutMemberPassword,"");
			
			Thread.sleep(1000);
			
			click(btnCheckoutMemberSeparateSubmitButtonForPw);
			
		}		
		else
		{
			sendKeys(txtCheckoutMemberEmail,myAccountTestData.loginEmailAddress);
			
			sendKeys(txtCheckoutMemberPassword,"");
			
			click(btnCheckoutMemberSubmitButton);
		}
				
		if(isDisplayed(checkoutMemberEmptyPasswordXpath))
		{
			if(getText(checkoutMemberEmptyPasswordXpath).equalsIgnoreCase(msgCheckoutMemberEmptyPassword))
			{
				writeTestResults("Verify Returning Customer Login with empty password", msgCheckoutMemberEmptyPassword+" Error message should be displayed", 
						msgCheckoutMemberEmptyEmail+" Error message is displayed","",true,false);			
			}
			else
			{
				writeTestResults("Verify Returning Customer Login with empty password", msgCheckoutMemberEmptyPassword+" Error message should be displayed", 
						getText(checkoutMemberEmptyPasswordXpath)+" Error message is displayed","",true,false);
			}			
		}
		else
		{
			writeTestResults("Verify Returning Customer Login with empty password", msgCheckoutMemberEmptyPassword+" Error message should be displayed", 
					checkoutMemberEmptyPasswordXpath +" xpath is not displayed","",false,false);
			
			goBack();
		}
		
		pageRefersh();

	}
	
	public void validateLoginInvalidEmail() throws Exception
	{
		if(isDisplayed("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a"))
		{
			click("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a");
		}
		
		if(isExecuted(getMethodIsExecuted,"Checkout As Member - separate buttons for login user name and pw"))
		{
			sendKeys(txtCheckoutMemberEmail,"InvalidEmail@test.com");
			
			Thread.sleep(1000);
			
			click(btnCheckoutMemberSubmitButton);
			
			Thread.sleep(1000);
			
			if(isDisplayed(btnBillingInfoContinue))
			{
				writeTestResults("Verify Returning Customer Login with invalid email", "Billing infor page should be displayed", "Billing infor page is displayed","4270c3fe",true,false);
			}
			else
			{
				writeTestResults("Verify Returning Customer Login with invalid email", "Billing infor page should be displayed", "Billing infor page is not displayed","4270c3fe",false,false);
			}				
		}
		else
		{
			sendKeys(txtCheckoutMemberEmail,"InvalidEmail@test.com");
			
			sendKeys(txtCheckoutMemberPassword,myAccountTestData.loginPassword);
			
			clickAndWait(btnCheckoutMemberSubmitButton);
			
			if(isDisplayed(checkoutMemberInvalidPasswordXpath))
			{
				if(getText(checkoutMemberInvalidPasswordXpath).equalsIgnoreCase(msgCheckoutMemberInvalidPassword))
				{
					writeTestResults("Verify Returning Customer Login with invalid email", msgCheckoutMemberInvalidPassword+" Error message should be displayed",
							msgCheckoutMemberInvalidPassword+" Error message is displayed","4270c3fe",true,false);
				}
				else
				{
					writeTestResults("Verify Returning Customer Login with invalid email", msgCheckoutMemberInvalidPassword+" Error message should be displayed",
							getText(checkoutMemberInvalidPasswordXpath)+" Error message is displayed","4270c3fe",true,false);
				}			
			}
			else
			{
				writeTestResults("Verify Returning Customer Login with invalid email", msgCheckoutMemberInvalidPassword+" Error message should be displayed", 
						checkoutMemberInvalidPasswordXpath +" xpath is not displayed","4270c3fe",false,false);
				
				goBack();
			}				
		}		
		
		pageRefersh();
	}
	
	public void validateLoginValidDetails() throws Exception
	{
		if(isDisplayed("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a"))
		{
			click("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a");
		}
		
		if(isExecuted(getMethodIsExecuted,"Checkout As Member - separate buttons for login user name and pw"))
		{
			sendKeys(txtCheckoutMemberEmail,myAccountTestData.loginEmailAddress);
			
			Thread.sleep(1000);
			
			click(btnCheckoutMemberSubmitButton);
			
			Thread.sleep(1000);
			
			sendKeys(txtCheckoutMemberPassword,myAccountTestData.loginPassword);
			
			Thread.sleep(1000);
			
			clickAndWait(btnCheckoutMemberSeparateSubmitButtonForPw);
			
			Thread.sleep(1000);
			
		}
		else
		{
			sendKeys(txtCheckoutMemberEmail,myAccountTestData.loginEmailAddress);
			
			sendKeys(txtCheckoutMemberPassword,myAccountTestData.loginPassword);
			
			click(btnCheckoutMemberSubmitButton);		
			
		}
		
		if(isDisplayed(checkoutMemberInvalidPasswordXpath))
		{
			if(isExecuted(getMethodIsExecuted,"Checkout As Member - separate buttons for login user name and pw"))
			{
				sendKeys(txtCheckoutMemberPassword,myAccountTestData.loginPassword);
				
				Thread.sleep(1000);
				
				clickAndWait(btnCheckoutMemberSeparateSubmitButtonForPw);
				
				Thread.sleep(1000);
				
			}
			else
			{				
				sendKeys(txtCheckoutMemberEmail,myAccountTestData.loginEmailAddress);
				
				sendKeys(txtCheckoutMemberPassword,myAccountEditPw);
				
				clickAndWait(btnCheckoutMemberSubmitButton);
			}
			
		}
		mailReaderUserName = myAccountTestData.loginEmailAddress;
		
		System.out.println("mailReaderUserName"+mailReaderUserName);
		if(isDisplayed(checkoutMemberInvalidPasswordXpath))
		{
			writeTestResults("Verify Returning Customer Login with valid details",  "Billing information should be displayed",
					"Following error is found:"+getText(checkoutMemberInvalidPasswordXpath),"4270bcd8",false,true);		
		}
		if(!isDisplayed(btnBillingInfoContinue))
		{
			Thread.sleep(3000);
		}
		
		if(isDisplayed(btnBillingInfoContinue))
		{
			writeTestResults("Verify Returning Customer Login with valid details",  "Billing information should be displayed", "Billing information is displayed","4270bcd8",true,false);
		}
		else
		{
			writeTestResults("Verify Returning Customer Login with valid details",  "Billing information should be displayed", "Billing information is not displayed.Following page is displayed " +
					""+getTitle(),"4270bcd8",false,true);			
		}		
	}
	
	public void validateLoginInvalidPW() throws Exception
	{		
		if(isDisplayed("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a"))
		{
			click("//*[contains(concat(' ', normalize-space(@class), ' '), 'tabs')]/li[2]/a");
		}
		if(isExecuted(getMethodIsExecuted,"Checkout As Member - separate buttons for login user name and pw"))
		{
			sendKeys(txtCheckoutMemberEmail,myAccountTestData.loginEmailAddress);
			
			Thread.sleep(1000);
			
			click(btnCheckoutMemberSubmitButton);
			
			Thread.sleep(1000);
			
			sendKeys(txtCheckoutMemberPassword,"XXXXXX");
			
			click(btnCheckoutMemberSeparateSubmitButtonForPw);
			
			Thread.sleep(1000);
			
		}	
		else
		{
			sendKeys(txtCheckoutMemberEmail,myAccountTestData.loginEmailAddress);
			
			sendKeys(txtCheckoutMemberPassword,"XXXXXX");
			
			click(btnCheckoutMemberSubmitButton);
		}
				
		if(isDisplayed(checkoutMemberInvalidPasswordXpath))
		{
			if(getText(checkoutMemberInvalidPasswordXpath).equalsIgnoreCase(msgCheckoutMemberInvalidPassword))
			{
				writeTestResults("Verify Returning Customer Login with invalid password", msgCheckoutMemberInvalidPassword+" Error message should be displayed",
						msgCheckoutMemberInvalidPassword+" Error message is displayed","",true,false);
			}
			else
			{
				writeTestResults("Verify Returning Customer Login with invalid password", msgCheckoutMemberInvalidPassword+" Error message should be displayed", 
						getText(checkoutMemberInvalidPasswordXpath)+" Error message is displayed","",true,false);
			}
		}
		else
		{
			writeTestResults("Verify Returning Customer Login with invalid password", msgCheckoutMemberInvalidPassword+" Error message should be displayed", 
					checkoutMemberInvalidPasswordXpath +" xpath is not displayed","",false,false);
			goBack();
		}	
		
		pageRefersh();
	}
	
	public void memberCheckoutLoginWithEmailAndPw() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"Order review details in checkout page"))
		{
			verifyProductPriceInOrderReview();
			
			verifyProductGrandTotalPriceBeforeShipping();
			
			verifyProductNameInOrderReview();
		}
		
		validateLoginWithoutEmail();
		
		validateLoginWithoutPw();
			
		validateLoginInvalidEmail();
			
		validateLoginInvalidPW();
			
		validateLoginValidDetails();
		
	}	
	
	
	public void CheckoutAsGuest_withEmail() throws Exception
	{
		String emailAddress = "";
		String Password = "";
		common.magentoProfilerResults(writeFileName+"Start checkout page");
		
		if(isExecuted(getMethodIsExecuted,"Select Checkout Type"))
		{
			if(isElementPresent(chkCheckoutAsGuestUser))
			{
				writeTestResults("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed", 
						"Checkout 'Guest' continue button is displayed","4270cbc4",true,false);	
				
				click(chkCheckoutAsGuestUser);
				
				Thread.sleep(5000);
			}
			else
			{
				writeTestResults("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed", 
						"Checkout 'Guest' continue button is not displayed","4270cbc4",false,true);				
			}
		}
		
		if(isExecuted(getMethodIsExecuted,"Order review details in checkout page"))
		{
			verifyProductPriceInOrderReview();
			
			verifyProductGrandTotalPriceBeforeShipping();
			
			verifyProductNameInOrderReview();
		}
		
		if(!txtBillingLoginEmail.isEmpty())
		{
			if(!isDisplayed(txtBillingLoginEmail))
			{
				Thread.sleep(2000);								
			}
			
			if(isDisplayed(txtBillingLoginEmail))
			{
				if(writeFileName.indexOf("Join Member During Purchase") >= 0)
				{
					emailAddress = billingJoinUserEmail;
					
				}
				else
				{
					emailAddress = billingGusetEmail;	
					
				}
				
				sendKeys(txtBillingLoginEmail,emailAddress);
				click(btnBillingContinue);
				Thread.sleep(5000);
				mailReaderUserName = emailAddress;
				
				
				writeTestResults("Verify the 'Email Address' text field displayed for guest user", "'Email Address' text field should be displayed for guest user", 
						"'Email Address' text field is displayed","4270cea8",true,false);
			}
			else
			{
				writeTestResults("Verify the 'Email Address' text field displayed for guest user", "'Email Address' text field should be displayed for guest user", 
						"'Email Address' text field is not displayed or xpath is incorrect","4270cea8",false,true);				
			}
				
		}	
		
		
		
		
		
		
		
		
	}	
	
	public void fillGuestUserDetails() throws Exception
	{
	/*	pageRefersh();
		
		if(isExecuted(getMethodIsExecuted,"After refersh navigate to  checkout method section"))
		{
			if(isExecuted(getMethodIsExecuted,"Checkout As Guest With Email"))
			{
				if(isExecuted(getMethodIsExecuted,"Select Checkout Type"))
				{
					if(isElementPresent(chkCheckoutAsGuestUser))
					{
						click(chkCheckoutAsGuestUser);
						Thread.sleep(1000);
					}
					else
					{
						writeTestResults("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed",
								"Checkout 'Guest' continue button is not displayed","",false,true);						
					}
				}		
				
				if(!txtBillingLoginEmail.isEmpty())
				{
					if(!isElementPresent(txtBillingLoginEmail))
					{
						Thread.sleep(2000);
					}
					
					if(isElementPresent(txtBillingLoginEmail))
					{
						sendKeys(txtBillingLoginEmail,billingLoginEmail);				
						
					}
					else
					{
						writeTestResults("Verify the 'Email Address' text field displayed for guest user", "'Email Address' text field should be displayed for guest user", 
								"'Email Address' text field is not displayed or xpath is incorrect","",false,true);						
					}
					if(isElementPresent(btnCheckoutAsGuestUser))
					{
						click(btnCheckoutAsGuestUser);
						
						if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxUserType.isEmpty())
						{
							if(!getAttribute(chkoutAjaxUserType,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
							{
								if(checkoutAjaxRequest(chkoutAjaxUserType))
								{
									writeTestResults("Verify billing information page", "Billing information screen should be displayed", "Billing infomation page is not load within 100sec","",false,true);									
								}
							}
						}
						else
						{
							Thread.sleep(2000);
						}										
					}
					else
					{
						writeTestResults("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed", 
								"Checkout 'Guest' continue button is not displayed","",false,true);
					}						
				}	
			}
			else if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
			{
				CheckoutAsGuest_withoutEmail();	
			}			
		}*/
	}
	
	public void fillJoingtUserDetails() throws Exception
	{
		pageRefersh();
		
		if(isExecuted(getMethodIsExecuted,"After refersh navigate to  checkout method section"))
		{
			if(isExecuted(getMethodIsExecuted,"Checkout As Guest With Email"))
			{
				if(isExecuted(getMethodIsExecuted,"Select Checkout Type"))
				{
					if(isElementPresent(chkCheckoutAsGuestUser))
					{
						click(chkCheckoutAsGuestUser);
						Thread.sleep(1000);
					}
					else
					{
						writeTestResults("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed", 
								"Checkout 'Guest' continue button is not displayed","",false,true);
					}
				}		
				
				if(!txtBillingLoginEmail.isEmpty())
				{
					if(!isElementPresent(txtBillingLoginEmail))
					{
						Thread.sleep(2000);
					}
					
					if(isElementPresent(txtBillingLoginEmail))
					{
						sendKeys(txtBillingLoginEmail,billingJoinUserEmail);
						
						mailReaderUserName = billingJoinUserEmail;
					}
					else
					{
						writeTestResults("Verify the 'Email Address' text field displayed for guest user", "'Email Address' text field should be displayed for guest user", 
								"'Email Address' text field is not displayed or xpath is incorrect","",false,true);
					}
					if(isElementPresent(btnCheckoutAsGuestUser))
					{
						click(btnCheckoutAsGuestUser);
						
						if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxUserType.isEmpty())
						{
							if(!getAttribute(chkoutAjaxUserType,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
							{
								if(checkoutAjaxRequest(chkoutAjaxUserType))
								{
									writeTestResults("Verify billing information page", "Billing information screen should be displayed", "Billing infomation page is not load within 100sec","",false,true);
								}
							}
						}
						else
						{
							Thread.sleep(2000);
						}
										
					}
					else
					{
						writeTestResults("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed", 
								"Checkout 'Guest' continue button is not displayed","",false,true);
					}								
				}	
			}
			else if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
			{
				CheckoutAsJoin_withoutEmail();	
			}						
		}
	}
	
	public void CheckoutAsGuest_withoutEmail() throws Exception
	{		
	/*	if(isDisplayed("login:guest_Id"))
		{
			click("login:guest_Id");
					
			if(isDisplayed(btnCheckoutAsGuestUser))
			{
				writeTestResults("Verify user clicks continue button ", "Billing information screen should be displayed", "Continue button is displayed in 'checkout method' screen","4270cbc4",true,false);
				
				clickAndWait(btnCheckoutAsGuestUser);				
			}
			else 
			{
				writeTestResults("Verify user clicks continue button ", "Billing information screen should be displayed", "Continue button is not displayed in 'checkout method' screen","4270cbc4",false,true);
			}			
		}
		else
		{
			writeTestResults("Verify user can select checkout method", "user can select checkout method as 'guest'", "Guest method is not displayed ","4270cbc4",false,true);			
		}*/		
	}
	
	public void CheckoutAsJoin_withoutEmail() throws Exception
	{		
		if(isDisplayed("login:register_Id"))
		{
			click("login:register_Id");
							   					
			if(isDisplayed(btnCheckoutAsGuestUser))
			{
				clickAndWait(btnCheckoutAsGuestUser);				
			}
			else 
			{
				writeTestResults("Verify user clicks continue button ", "Billing information screen should be displayed", "Continue button is not displayed in 'checkout method' screen ","",false,true);
			}			
		}
		else
		{
			writeTestResults("Verify user can select checkout method", "user can select checkout method as 'guest'", "Guest method is not displayed ","",false,true);
		}	
	}
	
	public void selectNewAddress() throws Exception
	{
		
		
		if(!selectNewBillingAddressDropDown.isEmpty())
		{
			if(isElementPresent(selectNewBillingAddressDropDown))
			{
				//click(selectNewBillingAddressDropDown);				
				
				//click(newBillingAddressdropdownlabel+"/li["+getTagListSize(newBillingAddressdropdownlabel,"li")+"]");
				
								
				selectText(selectNewBillingAddressDropDown, newBillingAddressdropdownlabel);						
			}			
		}
		
	}	
	
	public void selectNewAddressDelivery() throws Exception
	{
		
		
		if(!selectNewShippingAddressDropDown.isEmpty())
		{
			if(isElementPresent(selectNewShippingAddressDropDown))
			{
				//click(selectNewBillingAddressDropDown);				
				
				//click(newBillingAddressdropdownlabel+"/li["+getTagListSize(newBillingAddressdropdownlabel,"li")+"]");
				
								
				selectText(selectNewShippingAddressDropDown, newShippingAddressdropdownlabel);						
			}			
		}
		
	}	
	
	
	public void memberNewBillingAddressValidation() throws Exception
	{	
		if(!selectNewBillingAddressDropDown.isEmpty())
		{
			if(isElementPresent(selectNewBillingAddressDropDown))
			{
				//click(selectNewBillingAddressDropDown);
				//click(newBillingAddressdropdownlabel+"/li["+getTagListSize(newBillingAddressdropdownlabel,"li")+"]");
				
				selectText(selectNewBillingAddressDropDown, newBillingAddressdropdownlabel);
						
				writeTestResults("Verify member can select 'New Address' in billing information page", "User can select 'New Address' in billing information page", 
						"User can select 'New Address' in billing information page","",true,false);
				
				validateBillingInforPage();
			}
			else
			{
				validateBillingInforPage();
			}
		}
		else
		{
			validateBillingInforPage();	
		}
	}
	
	public void billingInfoNewAddress() throws Exception
	{
		billingInfoPage();
		/*if(!selectNewBillingAddressDropDown.isEmpty())
		{
			if(isElementPresent(selectNewBillingAddressDropDown))
			{
				click(selectNewBillingAddressDropDown);
				
				click(newBillingAddressdropdownlabel);
				
				//selectText(selectNewBillingAddressDropDown, newBillingAddressdropdownlabel);
						
				writeTestResults("Verify member can select 'New Address' in billing information page", "User can select 'New Address' in billing information page", 
						"User can select 'New Address' in billing information page","",true,false);
				
				Thread.sleep(WAIT*2);
				
				billingInfoPage();
			}
			else
			{
				billingInfoPage();
			}
			
		}
		else
		{
			billingInfoPage();
		}*/
	}
	
	
	public void newBillingAddressFieldsValues() throws Exception
	{
		if(!txtBillingFirstName.isEmpty())
		{
			if(isElementPresent(txtBillingFirstName))
			{
				if(getAttribute((txtBillingFirstName),"value").isEmpty())
				{
					writeTestResults("Verify the 'First Name' text field displayed in billing information page", "First Name text field should be displayed in billing information page", 
							"First Name text field is displayed in billing information page","",true,false);
					
					sendKeys((txtBillingFirstName),billingFirstName);					
				}
				else
				{
					writeTestResults("Verify the 'First Name' text field displayed in billing information page", "Empty 'First Name' text field should be displayed in billing information page", 
							"Following text ("+getAttribute((txtBillingFirstName),"value")+ " ) field is displayed in billing information page","",false,false);
					
					sendKeys((txtBillingFirstName),billingFirstName);
				}
			}
			else
			{
				writeTestResults("Verify the 'First Name' text field displayed in billing information page", "First Name text field should be displayed in billing information page", 
						"First Name text field is not displayed in billing information page","",false,false);
			}
		}
		if(!txtBillingLastName.isEmpty())
		{
			if(isElementPresent(txtBillingLastName))
			{
				if(getAttribute(txtBillingLastName,"value").isEmpty())
				{
					writeTestResults("Verify the 'Last Name' text field displayed in billing information page", "Empty Last Name text field should be displayed in billing information page", 
							"Last Name text field is displayed in billing information page","",true,false);
					
					sendKeys(txtBillingLastName,billingLastName);
					
				}
				else
				{
					writeTestResults("Verify the 'Last Name' text field displayed in billing information page", "Empty 'Last Name' text field should be displayed in billing information page", 
							"Following text ("+getAttribute((txtBillingLastName),"value")+ " ) field is displayed in billing information page","",false,false);
				}
						
			}
			else
			{
				writeTestResults("Verify the 'Last Name' text field displayed in billing information page", "Last Name text field should be displayed in billing information page", 
						"Last Name text field is not displayed in billing information page","",false,false);							
			}
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Checkout As Guest Without Email").equalsIgnoreCase("Yes"))
		{			
			if(!isLoggedUser)
			{
				if(!txtBillingEmail.isEmpty())
				{
					if(isElementPresent(txtBillingEmail))
					{
						if(getAttribute(txtBillingEmail,"value").isEmpty())
						{
							writeTestResults("Verify the 'Email Address' text field displayed in billing information page", "Empty 'Email Address' text field should be displayed in billing information page", 
									"'Email Address' text field is displayed","",true,false);
							
							sendKeys(txtBillingEmail,billingLoginEmail);
						}
						else
						{
							writeTestResults("Verify the 'Email Address' text field displayed in billing information page", "Empty 'Email Address' text field should be displayed in billing information page", 
									"Following text ("+getAttribute((txtBillingEmail),"value")+ ") is displayed in billing infor page","",false,false);
							
							sendKeys(txtBillingEmail,billingLoginEmail);
						}									
					}
					else
					{
						writeTestResults("Verify the 'Email Address' text field displayed in billing information page", "'Email Address' text field should be displayed in billing information page", 
								"'Email Address' text field is not displayed or xpath is incorrect","",false,true);						
					}
				}
			}
		}
		
		if(!txtBillingTelephone.isEmpty())
		{
			if(isElementPresent(txtBillingTelephone))
			{
				if(getAttribute(txtBillingTelephone,"value").isEmpty())
				{
					writeTestResults("Verify the 'Contact Number' text field displayed in billing information page", "Empty Contact Number text field should be displayed in billing information page", 
							"Contact Number field is displayed in billing information page","",true,false);
					
					sendKeys(txtBillingTelephone,billingTelephone);
				}
				else
				{
					writeTestResults("Verify the 'Contact Number' text field displayed in billing information page", "Empty Contact Number text field should be displayed in billing information page", 
							"Following text ("+getAttribute((txtBillingTelephone),"value")+ ") is displayed in billing infor page","",false,false);
					
					sendKeys(txtBillingTelephone,billingTelephone);
				}				
			}
			else
			{		
				writeTestResults("Verify the 'Contact Number' text field displayed in billing information page", "Contact Number text field should be displayed in billing information page", 
						"Contact Number text field is not displayed in billing information page","",false,false);
			}
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Find Billing Address").equalsIgnoreCase("Yes"))
		{
			click(linkaddressHint);
		}
		
		if(!txtBillingstreet1.isEmpty())
		{
			if(isElementPresent(txtBillingstreet1))
			{
				if(getAttribute(txtBillingstreet1,"value").isEmpty())
				{
					writeTestResults("Verify the 'Street' text field displayed in billing information page", "Empty Street text field should be displayed in billing information page",
							"Street field is displayed in billing information page","",true,false);
									
					sendKeys(txtBillingstreet1,billingstreet1);
				}
				else
				{
					writeTestResults("Verify the 'Street' text field displayed in billing information page", "Empty Street text field should be displayed in billing information page", 
							"Following text ("+getAttribute((txtBillingstreet1),"value")+ ") is displayed in billing infor page","",false,false);
				
					sendKeys(txtBillingstreet1,billingstreet1);
				}
				
			}
			else
			{	
				writeTestResults("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in billing information page", 
						"Street text field is not displayed in billing information page","",false,false);
				
			}
		}
		if(!txtBillingCity.isEmpty())
		{
			if(isElementPresent(txtBillingCity))
			{
				if(getAttribute(txtBillingCity,"value").isEmpty())
				{
					writeTestResults("Verify the 'City' text field displayed in billing information page", "Empty City text field should be displayed in billing information page", 
							"City field is displayed in billing information page","",true,false);
					
					sendKeys(txtBillingCity,billingCity);
				}
				else
				{
					writeTestResults("Verify the 'City' text field displayed in billing information page", "Empty City text field should be displayed in billing information page", 
							"Following text ("+getAttribute((txtBillingCity),"value")+ ") is displayed in billing infor page","",false,false);
			
					sendKeys(txtBillingCity,billingCity);
				}				
			}
			else
			{
				writeTestResults("Verify the 'City' text field displayed in billing information page", "City text field should be displayed in billing information page", 
						"City text field is not displayed in billing information page","",false,false);				
			}
		}		
				
		if(!txtBillingPostcode.isEmpty())
		{
			if(isElementPresent(txtBillingPostcode))
			{
				if(getAttribute(txtBillingPostcode,"value").isEmpty())
				{
					writeTestResults("Verify the 'Post Code' text field displayed in billing information page", "Empty Post Code text field should be displayed in billing information page", 
							"Post Code field is displayed in billing information page","",true,false);
					
					sendKeys(txtBillingPostcode,billingPostcode);
				}
				else
				{
					writeTestResults("Verify the 'Post Code' text field displayed in billing information page", "Empty Post Code text field should be displayed in billing information page", 
							"Following text ("+getAttribute((txtBillingPostcode),"value")+ ") is displayed in billing infor page","",false,false);
					
					sendKeys(txtBillingPostcode,billingPostcode);
				}				
			}
			else
			{
				writeTestResults("Verify the 'Post Code' text field displayed in billing information page", "Post Code text field should be displayed in billing information page",
						"Post Code text field is not displayed in billing information page","",false,false);
			}
		}	
		if(!selectBillingCountry_Id.isEmpty())
		{
			if(isElementPresent(selectBillingCountry_Id))
			{
				writeTestResults("Verify the 'Country' dropdown field displayed in billing information page", "Country dropdown field should be displayed in billing information page", 
						"Country field is displayed in billing information page","",true,false);
				
				selectText(selectBillingCountry_Id,billingCountry_Id);
			}
			else
			{	
				writeTestResults("Verify the 'Country' dropdown field displayed in billing information page", "Country dropdown field should be displayed in billing information page",
						"Country dropdown field is not displayed in billing information page","",false,false);
				
				selectText(selectBillingCountry_Id,billingCountry_Id);
			}
		}
		if(!selectBillingRegion_Id.isEmpty())
		{
			if(isElementPresent(selectBillingRegion_Id))
			{
				if(getAttribute(selectBillingRegion_Id,"value").isEmpty())
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Empty Region dropdown field should be displayed in billing information page",
							"Region field is displayed in billing information page","",true,false);
				
					selectText(selectBillingRegion_Id,billingRegion_Id);
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Empty Region dropdown field should be displayed in billing information page", 
							"Following text ("+getAttribute((selectBillingRegion_Id),"value")+ ") is displayed in billing infor page","",false,false);
				
					selectText(selectBillingRegion_Id,billingRegion_Id);
				}							
			}
			else
			{
				writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
						"Region dropdown field is not displayed in billing information page","",false,false);				
			}
		}
		
		if(!selectBillingRegion_Id_DropDown.isEmpty())
		{
			if(isElementPresent(selectBillingRegion_Id_DropDown))
			{
				if(getAttribute(selectBillingRegion_Id_DropDown,"value").equalsIgnoreCase("Please select region, state or province"))
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Empty Region dropdown field should be displayed in billing information page", 
							"Region field is displayed in billing information page","",true,false);
					
					click(selectBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
									
					click(billingRegion_Id+"_Link");
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Empty Region dropdown field should be displayed in billing information page", 
							"Following text ("+getAttribute((selectBillingRegion_Id_DropDown),"value")+ ") is displayed in billing infor page","",false,false);
									
					click(selectBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
									
					click(billingRegion_Id+"_Link");
				}							
			}
			else
			{	
				writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
						"Region dropdown field is not displayed in billing information page","",false,false);			
			}
		}
	}
	
	public void validateBillingInforPage() throws Exception
	{
		if(!isDisplayed(btnBillingInfoContinue))
		{	
			if(isDisplayed(txtCheckoutMemberPassword))
			{
				writeTestResults("Verify billing information page", "User can enter enter data in billing information page", "User already registerd.User email is "+mailReaderUserName,"",false,true);				
			}
			else
			{
				writeTestResults("Verify billing information page", "User can enter enter data in billing information page", "Billing information page is not load correctly","",false,true);				
			}					
		}
		
		
		
		else
		{
			common.magentoProfilerResults(writeFileName+"Billing infor page");
			
			isBillingInfoValidation = true;
			
			if(!txtBillingFirstName.isEmpty())
			{
				if(isElementPresent(txtBillingFirstName))
				{
					sendKeys((txtBillingFirstName),"");
				}
			}
			
			if(!txtBillingLastName.isEmpty())
			{
				if(isElementPresent(txtBillingLastName))
				{
					sendKeys(txtBillingLastName,"");
				}
			}
			
			if(!txtBillingTelephone.isEmpty())
			{
				if(isElementPresent(txtBillingTelephone))
				{
					sendKeys(txtBillingTelephone,"");								
				}
			}
			
			if(findElementInXLSheet(getMethodIsExecuted,"Find Billing Address").equalsIgnoreCase("Yes"))
			{
				click(linkaddressHint);
			}
			
			if(!txtBillingPostcode.isEmpty())
			{
				if(isElementPresent(txtBillingPostcode))
				{
					sendKeys(txtBillingPostcode,"");					
				}
			}
			
			if(!txtBillingstreet1.isEmpty())
			{
				if(isElementPresent(txtBillingstreet1))
				{
					sendKeys(txtBillingstreet1,"");										
				}
			}
			if(!txtBillingCity.isEmpty())
			{
				if(isElementPresent(txtBillingCity))
				{
					sendKeys(txtBillingCity,"");							
				}
			}						
				
			if(!selectBillingCountry_Id.isEmpty())
			{
				if(isElementPresent(selectBillingCountry_Id))
				{
					selectText(selectBillingCountry_Id,"");			
				}
			}
						
			if(!selectBillingRegion_Id.isEmpty())
			{
				if(isElementPresent(selectBillingRegion_Id))
				{
					selectText(selectBillingRegion_Id,"");		
				}
			}
			
			if(!selectBillingRegion_Id_DropDown.isEmpty())
			{
				if(isElementPresent(selectBillingRegion_Id_DropDown))
				{
					click(selectBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					
					click(billingRegion_Id+"_Link");							
					
				}
				else if(isElementPresent(selectMemberBillingRegion_Id_DropDown))
				{
					click(selectMemberBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);

					click(billingRegion_Id+"_Link");							
				
				}
			}
			
			if(!txtBillingRegion.isEmpty())
			{
				if(isElementPresent(txtBillingRegion))
				{
					sendKeys(txtBillingRegion,"");							
				}
			}
		}
	}
	
	/*--------------------------Delivery Information----------------------------*/
	public void validateDeliveryInforPage() throws Exception
	{
		
			common.magentoProfilerResults(writeFileName+"Delivery infor page");

			selectNewAddressDelivery();
			isDeliveryInfoValidation = true;
			
			if(!txtDeliveryFirstName.isEmpty())
			{
				if(isElementPresent(txtDeliveryFirstName))
				{
					sendKeys((txtDeliveryFirstName),"");
				}
			}
			
			if(!txtDeliveryLastName.isEmpty())
			{
				if(isElementPresent(txtDeliveryLastName))
				{
					sendKeys(txtDeliveryLastName,"");
				}
			}
			
			if(!txtDeliveryTelephone.isEmpty())
			{
				if(isElementPresent(txtDeliveryTelephone))
				{
					sendKeys(txtDeliveryTelephone,"");								
				}
			}
			
			
			
			if(!txtDeliverySuburb.isEmpty())
			{
				if(isElementPresent(txtDeliverySuburb))
				{
					sendKeys(txtDeliverySuburb,"");					
				}
			}
			
			if(!txtDeliverystreet1.isEmpty())
			{
				if(isElementPresent(txtDeliverystreet1))
				{
					sendKeys(txtDeliverystreet1,"");										
				}
			}
								
				
			
			
	}
	/*---------------------------Delivery Information--------------------*/
	

	/*--------------------------Delivery Information----------------------------*/
	public void DeliveryInforPage() throws Exception
	{
		
		if(!isDisplayed(btnBillingInfoContinue))
		{		
			writeTestResults("Verify billing information page", "User can enter enter data in billing information page", "Billing information page is not load correctly","",false,true);					
		}
		else
		{
			if(isElementPresent(chkSameShipAddress) && !chkSameShipAddress.isEmpty())
			{
				click(chkSameShipAddress);				
			}
			
			if(!txtBillingFirstName.isEmpty())
			{
				if(isDisplayed(txtBillingFirstName))
				{
					sendKeys(txtBillingFirstName,billingFirstName);			
					
					writeTestResults("Verify the 'First Name' text field displayed in billing information page", "First Name text field should be displayed in billing information page",
							"First Name text field is displayed in billing information page","",true,false);
					
				}
				else
				{
					writeTestResults("Verify the 'First Name' text field displayed in billing information page", "First Name text field should be displayed in billing information page",
							"First Name text field is not displayed in billing information page","",false,false);					
				}
			}
			
			
			
			if(!txtBillingLastName.isEmpty())
			{
				if(isDisplayed(txtBillingLastName))
				{					
					sendKeys(txtBillingLastName,billingLastName);
					
					writeTestResults("Verify the 'Last Name' text field displayed in billing information page", "Last Name text field should be displayed in billing information page", 
							"Last Name text field is displayed in billing information page","",true,false);
				}
				else
				{
					writeTestResults("Verify the 'Last Name' text field displayed in billing information page", "Last Name text field should be displayed in billing information page", 
							"Last Name text field is not displayed in billing information page","",false,false);								
				}
			}
			
			
			if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
			{
				if(!isLoggedUser)
				{
					if(!txtBillingEmail.isEmpty())
					{
						if(isDisplayed(txtBillingEmail))
						{
							sendKeys(txtBillingEmail,billingJoinUserEmail);
							
							writeTestResults("Verify the 'Email Address' text field displayed in billing information page", 
									"'Email Address' text field should be displayed in billing information page", "'Email Address' text field is displayed","",true,false);
						}
						else
						{
							writeTestResults("Verify the 'Email Address' text field displayed in billing information page",
									"'Email Address' text field should be displayed in billing information page", "'Email Address' text field is not displayed or xpath is incorrect","",false,true);
						}
					}	
					
					mailReaderUserName = billingLoginEmail;				
				}			
			}
			
			if(!txtBillingTelephone.isEmpty())
			{
				if(isDisplayed(txtBillingTelephone))
				{
					sendKeys(txtBillingTelephone,billingTelephone);
					
					writeTestResults("Verify the 'Contact Number' text field displayed in billing information page", "Contact Number text field should be displayed in billing information page",
							"Contact Number field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Contact Number' text field displayed in billing information page", "Contact Number text field should be displayed in billing information page",
							"Contact Number text field is not displayed in billing information page","",false,false);
				}
			}

			if(!txtBillingstreet1.isEmpty())
			{
				if(isDisplayed(txtBillingstreet1))
				{
					sendKeys(txtBillingstreet1,billingstreet1);
					
					writeTestResults("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in billing information page", 
							"Street field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in billing information page", 
							"Street text field is not displayed in billing information page","",false,false);					
				}
			}
			
			
			
			
			if(findElementInXLSheet(getMethodIsExecuted,"Find Billing Address").equalsIgnoreCase("Yes"))
			{
				click(linkaddressHint);
			}
			
			if(!txtBillingPostcode.isEmpty())
			{
				if(isElementPresent(txtBillingPostcode))
				{		
					
					sendKeys(txtBillingPostcode,billingPostcode);
					Thread.sleep(3000);
					
					click(txtBillingPostcode);
					 Actions action = new Actions(driver);
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      
				      Thread.sleep(3000);
				      action.sendKeys(Keys.ENTER).build().perform();
				      
				      
				      sendKeys(txtBillingPostcode,billingPostcode);
						Thread.sleep(3000);
						
						click(txtBillingPostcode);
						
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      
					      Thread.sleep(3000);
					      action.sendKeys(Keys.ENTER).build().perform();
					
					
					if(isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In Checkout"))
					{
						
						if(!isDisplayed(txtPostalCode))
						{
							click(txtPostalCode);
						}
						
						else
						{
							
						}
						if(!isElementPresent(selectBillingPostCodeDropDown))
						{
							i =0;
							while(!isElementPresent(selectBillingPostCodeDropDown))
							{
								if(isElementPresent(selectBillingPostCodeDropDown))
								{
									break;
								}
								else
								{
									Thread.sleep(1000);
									i++;
								}
								
								if(i == 5)
								{
									break;
								}
							}
						}
						
						click(selectBillingPostCodeDropDown);												
					}				
					writeTestResults("Verify the 'Post Code' text field displayed in billing information page", "Post Code text field should be displayed in billing information page",
							"Post Code field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Post Code' text field displayed in billing information page", "Post Code text field should be displayed in billing information page", 
							"Post Code text field is not displayed in billing information page","",false,false);
				}
			}
			
			
			
			
			
			
			
			if(!txtBillingCity.isEmpty())
			{
				if(isDisplayed(txtBillingCity))
				{
//					if(!isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In Checkout"))
//					{
						sendKeys(txtBillingCity,billingCity);
//					}	
//					else
//					{
//						click(txtBillingCity);
//					}
					writeTestResults("Verify the 'City' text field displayed in billing information page", "City text field should be displayed in billing information page",
							"City field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'City' text field displayed in billing information page", "City text field should be displayed in billing information page", 
							"City text field is not displayed in billing information page","",false,false);					
				}
			}	
			
			
			
			
			if(!txtPassword.isEmpty() && !txtConfirmPassword.isEmpty())
			{
				if(isDisplayed(txtPassword)&& isDisplayed(txtConfirmPassword))
				{
					sendKeys(txtPassword,billingPassword);
					sendKeys(txtConfirmPassword,billingConfirmPassword);
					
				}	
					writeTestResults("Verify the 'Password and confirm password' text field displayed in billing information page", "Password and confirm password text field should be displayed in billing information page",
							"Password and confirm password field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Password and confirm password' text field displayed in billing information page", "Password and confirm password text field should be displayed in billing information page", 
							"Password and confirm password text field is not displayed in billing information page","",false,false);					
				}
									

	
			
			if(!selectBillingCountry_Id.isEmpty())
			{
				if(isDisplayed(selectBillingCountry_Id))
				{
					selectText(selectBillingCountry_Id,billingCountry_Id);
					
					writeTestResults("Verify the 'Country' dropdown field displayed in billing information page", "Country dropdown field should be displayed in billing information page", 
							"Country field is displayed in billing information page","",true,false);
				}
				else
				{	
					writeTestResults("Verify the 'Country' dropdown field displayed in billing information page", "Country dropdown field should be displayed in billing information page", 
							"Country dropdown field is not displayed in billing information page","",false,false);					
				}
			}			
			
			
			if(!selectBillingRegion_Id.isEmpty())
			{
				if(isDisplayed(selectBillingRegion_Id))
				{
					selectText(selectBillingRegion_Id,billingRegion_Id);	
					
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
							"Region field is displayed in billing information page","",true,false);
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
							"Region dropdown field is not displayed in billing information page","",false,false);					
				}
			}
			
			if(!selectBillingRegion_Id_DropDown.isEmpty())
			{
				if(isDisplayed(selectBillingRegion_Id_DropDown))
				{
					System.out.println("selectBillingRegion_Id_DropDown"+selectBillingRegion_Id_DropDown);
					
					click(selectBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					
					click(billingRegion_Id+"_Link");
					
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page",
							"Region field is displayed in billing information page","",true,false);
				}
				else if(isDisplayed(selectMemberBillingRegion_Id_DropDown))
				{
					click(selectMemberBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					
					click(billingRegion_Id+"_Link");
					
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
							"Region field is displayed in billing information page","",true,false);
				
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page",
							"Region dropdown field is not displayed in billing information page","",false,false);					
				}
			}
			
			if(!txtBillingRegion.isEmpty())
			{
				if(isDisplayed(txtBillingRegion))
				{
					sendKeys(txtBillingRegion,billingRegion_Id);
					
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
							"Region field is displayed in billing information page","",true,false);	
					
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page",
							"Region dropdown field is not displayed in billing information page","",false,false);					
				}
			}
			if(isExecuted(getMethodIsExecuted,"Billing has birthday") && writeFileName.indexOf("Registered-") < 0)
			{
				if(writeFileName.indexOf("Reg-")< 0)
				{
					selectDOB();
				}
				
			}
			
		}
			
			
	}
	/*---------------------------Delivery Information--------------------*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public void validatePasswordFields() throws Exception
	{
		validatePwFields();
		
		validatePwFieldsWithOutValue();
		
		validatePwFieldsWithLessPw();
		
		validatePwFieldsMismatchValueWithoutFormData();
		
		//validatePwFieldsMismatchValueWithFormData();
		
	}
	
	public void fillPwFields() throws Exception
	{
		if(!txtBillingCustPw.isEmpty())
		{
			if(isElementPresent(txtBillingCustPw))
			{
				sendKeys((txtBillingCustPw),billingCustPw);
			}
		}
		
		if(!txtBillingCustConfirmPw.isEmpty())
		{
			if(isElementPresent(txtBillingCustConfirmPw))
			{
				sendKeys(txtBillingCustConfirmPw,billingCustPw);
			}
		}					
	}
	
	private void validatePwFieldsMismatchValueWithoutFormData() throws Exception
	{
		if(!txtBillingCustPw.isEmpty())
		{
			if(isElementPresent(txtBillingCustPw))
			{
				sendKeys((txtBillingCustPw),"123456");
			}
		}
		
		if(!txtBillingCustConfirmPw.isEmpty())
		{
			if(isElementPresent(txtBillingCustConfirmPw))
			{
				sendKeys(txtBillingCustConfirmPw,"654321");
			}
		}
		
		if(isDisplayed(btnBillingInfoContinue))
		{
			click(btnBillingInfoContinue);
		}
		
		if(isDisplayed(msgBillingMisMatchPw))
		{
			writeTestResults("Validate new user password and confirm password when enter mismatch passwords and without form data", "Error message should be displayed", 
					"Following messages is displayed "+getText(msgBillingMisMatchPw),"",true,false);
		}
		else
		{
			if(isAlertPresent())
			{
				writeTestResults("Validate new user password and confirm password when enter mismatch passwords and with form data", "Error message should be displayed", 
						"Following messages is displayed "+getAlert(),"",true,false);				
			}
			else
			{
				writeTestResults("Validate new user password and confirm password when enter mismatch passwords", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","",false,false);	
			}
					
		}		
	}
	
	private void validatePwFieldsMismatchValueWithFormData() throws Exception
	{
		fillBillingInfo();
		
		String alert = "";	
		
		if(!txtBillingCustPw.isEmpty())
		{
			if(isElementPresent(txtBillingCustPw))
			{
				sendKeys((txtBillingCustPw),"123456");
			}
		}
		
		if(!txtBillingCustConfirmPw.isEmpty())
		{
			if(isElementPresent(txtBillingCustConfirmPw))
			{
				sendKeys(txtBillingCustConfirmPw,"654321");
			}
		}
		
		if(isDisplayed(btnBillingInfoContinue))
		{
			click(btnBillingInfoContinue);
		}
		
		if(isDisplayed(msgBillingMisMatchPw))
		{
			writeTestResults("Validate new user password and confirm password when enter mismatch passwords", "Error message should be displayed", 
					"Following messages is displayed "+getText(msgBillingMisMatchPw),"",true,false);
		}
		else
		{	
			click(btnBillingInfoContinue);
			
			if(isAlertPresent())
			{	
				alert = getAlert();
				
				writeTestResults("Validate new user password and confirm password when enter mismatch passwords", "Error message should be displayed", 
						"Following messages is displayed "+alert,"",true,false);				
			}
			else
			{
				writeTestResults("Validate new user password and confirm password when enter mismatch passwords", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","",false,true);	
			}
					
		}		
	}	
	
	private void validatePwFieldsWithLessPw() throws Exception
	{
		if(!txtBillingCustPw.isEmpty())
		{
			if(isElementPresent(txtBillingCustPw))
			{
				sendKeys((txtBillingCustPw),"123");
			}
		}			
		
		if(isDisplayed(btnBillingInfoContinue))
		{
			click(btnBillingInfoContinue);
		}
		
		if(isDisplayed(msgBillingLessPw))
		{
			writeTestResults("Validate new user less password", "Error message should be displayed", "Following messages is displayed "+getText(msgBillingLessPw),"",true,false);
		}
		else
		{
			writeTestResults("Validate new user less password", "Error message should be displayed", "Error message is not displayed or xpath is incorrect","",false,true);
		}		
	}
	
	private void validatePwFieldsWithOutValue() throws Exception
	{
		if(!txtBillingCustPw.isEmpty())
		{
			if(isElementPresent(txtBillingCustPw))
			{
				sendKeys((txtBillingCustPw),"");
			}
		}
		
		if(!txtBillingCustConfirmPw.isEmpty())
		{
			if(isElementPresent(txtBillingCustConfirmPw))
			{
				sendKeys(txtBillingCustConfirmPw,"");
			}
		}
		
		if(isDisplayed(btnBillingInfoContinue))
		{
			click(btnBillingInfoContinue);
		}
		
		if(isDisplayed(msgBillingWithoutPw) && isDisplayed(msgBillingWithoutConfirmPw))
		{
			writeTestResults("Validate new user password and confirm password fields without values", "Error message should be displayed",
					"Following messages are displayed "+getText(msgBillingWithoutPw)+ " and "+getText(msgBillingWithoutPw),"",true,false);
		}
		else
		{
			writeTestResults("Validate new user password and confirm password fields without values", "Error message should be displayed", "Error message is not displayed or xpath is incorrect","",false,true);			
		}		
	}
	
	private void validatePwFields() throws Exception
	{
		if(isDisplayed(txtBillingCustPw) && isDisplayed(txtBillingCustConfirmPw))
		{
			writeTestResults("Verify new user password and confirm password fields in billing info", "Password and confirm password fields should be displayed", 
					"Password and confirm password fields is displayed","",true,false);
		}
		else
		{
			writeTestResults("Verify new user password and confirm password fields in billing info", "Password and confirm password fields should be displayed", 
					"Password or confirm password fields is not displayed","",false,true);					
		}			
	}
	
	public void fillBillingInfo() throws Exception
	{
		if(!isDisplayed(btnBillingInfoContinue))
		{		
			writeTestResults("Verify billing information page", "User can enter enter data in billing information page", "Billing information page is not load correctly","",false,true);					
		}
		else
		{			
			if(!txtBillingFirstName.isEmpty())
			{
				if(isDisplayed(txtBillingFirstName))
				{
					sendKeys((txtBillingFirstName),billingFirstName);
				}
			}
			if(!txtBillingLastName.isEmpty())
			{
				if(isDisplayed(txtBillingLastName))
				{
					sendKeys(txtBillingLastName,billingLastName);
				}
			}
			if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
			{
				if(!isLoggedUser)
				{
					if(!txtBillingEmail.isEmpty())
					{
						if(isDisplayed(txtBillingEmail))
						{
							sendKeys(txtBillingEmail,billingJoinUserEmail);
						}
					}	
					
					mailReaderUserName = billingLoginEmail;				
				}			
			}
			
			if(!txtBillingTelephone.isEmpty())
			{
				if(isDisplayed(txtBillingTelephone))
				{
					sendKeys(txtBillingTelephone,billingTelephone);
				}
			}
			
			if(findElementInXLSheet(getMethodIsExecuted,"Find Billing Address").equalsIgnoreCase("Yes"))
			{
				click(linkaddressHint);
			}
			
			if(!txtBillingPostcode.isEmpty())
			{
				if(isElementPresent(txtBillingPostcode))
				{						
					sendKeys(txtBillingPostcode,billingPostcode);
					
					if(isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In Checkout"))
					{
						if(isElementPresent("billing:postcodesuburbremove_Id"))
						{
							click("billing:postcodesuburbremove_Id");
						}
						if(!isElementPresent(selectBillingPostCodeDropDown))
						{
							i =0;
							while(!isElementPresent(selectBillingPostCodeDropDown))
							{
								if(isElementPresent(selectBillingPostCodeDropDown))
								{
									break;
								}
								else
								{
									Thread.sleep(1000);
									i++;
								}
								
								if(i == 5)
								{
									break;
								}
							}
						}
						
						click(selectBillingPostCodeDropDown);												
					}				
				}
			}
			
			if(!txtBillingstreet1.isEmpty())
			{
				if(isDisplayed(txtBillingstreet1))
				{
					sendKeys(txtBillingstreet1,billingstreet1);
				}
			}
			if(!txtBillingCity.isEmpty())
			{
				if(isDisplayed(txtBillingCity))
				{
					if(!isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In Checkout"))
					{
						sendKeys(txtBillingCity,billingCity);
					}	
					else
					{
						click(txtBillingCity);
					}
				}	
			}					
				
			if(!selectBillingCountry_Id.isEmpty())
			{
				if(isDisplayed(selectBillingCountry_Id))
				{
					selectText(selectBillingCountry_Id,billingCountry_Id);
				}
			}			
			
			if(!selectBillingRegion_Id.isEmpty())
			{
				if(isDisplayed(selectBillingRegion_Id))
				{
					selectText(selectBillingRegion_Id,billingRegion_Id);	
				}
			}
			
			if(!selectBillingRegion_Id_DropDown.isEmpty())
			{
				if(isDisplayed(selectBillingRegion_Id_DropDown))
				{
					System.out.println("selectBillingRegion_Id_DropDown"+selectBillingRegion_Id_DropDown);
					
					click(selectBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					
					click(billingRegion_Id+"_Link");
					
				}
				else if(isDisplayed(selectMemberBillingRegion_Id_DropDown))
				{
					click(selectMemberBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					
					click(billingRegion_Id+"_Link");
					
				}
			}
			
			if(!txtBillingRegion.isEmpty())
			{
				if(isDisplayed(txtBillingRegion))
				{
					sendKeys(txtBillingRegion,billingRegion_Id);
				}
			}
			if(isExecuted(getMethodIsExecuted,"Billing has birthday"))
			{
				selectDOB();
			}
			
		}
	}	
	
	public void billingInfoPage() throws Exception
	{
		if(!isDisplayed(btnBillingInfoContinue))
		{		
			writeTestResults("Verify billing information page", "User can enter enter data in billing information page", "Billing information page is not load correctly","",false,true);					
		}
		else
		{
			if(isElementPresent(chkSameShipAddress) && !chkSameShipAddress.isEmpty())
			{
				click(chkSameShipAddress);				
			}
			
			if(!txtBillingFirstName.isEmpty())
			{
				if(isDisplayed(txtBillingFirstName))
				{
					sendKeys(txtBillingFirstName,billingFirstName);			
					
					writeTestResults("Verify the 'First Name' text field displayed in billing information page", "First Name text field should be displayed in billing information page",
							"First Name text field is displayed in billing information page","",true,false);
					
				}
				else
				{
					writeTestResults("Verify the 'First Name' text field displayed in billing information page", "First Name text field should be displayed in billing information page",
							"First Name text field is not displayed in billing information page","",false,false);					
				}
			}
			
			
			
			if(!txtBillingLastName.isEmpty())
			{
				if(isDisplayed(txtBillingLastName))
				{					
					sendKeys(txtBillingLastName,billingLastName);
					
					writeTestResults("Verify the 'Last Name' text field displayed in billing information page", "Last Name text field should be displayed in billing information page", 
							"Last Name text field is displayed in billing information page","",true,false);
				}
				else
				{
					writeTestResults("Verify the 'Last Name' text field displayed in billing information page", "Last Name text field should be displayed in billing information page", 
							"Last Name text field is not displayed in billing information page","",false,false);								
				}
			}
			
			
			if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
			{
				if(!isLoggedUser)
				{
					if(!txtBillingEmail.isEmpty())
					{
						if(isDisplayed(txtBillingEmail))
						{
							sendKeys(txtBillingEmail,billingJoinUserEmail);
							
							writeTestResults("Verify the 'Email Address' text field displayed in billing information page", 
									"'Email Address' text field should be displayed in billing information page", "'Email Address' text field is displayed","",true,false);
						}
						else
						{
							writeTestResults("Verify the 'Email Address' text field displayed in billing information page",
									"'Email Address' text field should be displayed in billing information page", "'Email Address' text field is not displayed or xpath is incorrect","",false,true);
						}
					}	
					
					mailReaderUserName = billingLoginEmail;				
				}			
			}
			
			if(!txtBillingTelephone.isEmpty())
			{
				if(isDisplayed(txtBillingTelephone))
				{
					sendKeys(txtBillingTelephone,billingTelephone);
					
					writeTestResults("Verify the 'Contact Number' text field displayed in billing information page", "Contact Number text field should be displayed in billing information page",
							"Contact Number field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Contact Number' text field displayed in billing information page", "Contact Number text field should be displayed in billing information page",
							"Contact Number text field is not displayed in billing information page","",false,false);
				}
			}

			if(!txtBillingstreet1.isEmpty())
			{
				if(isDisplayed(txtBillingstreet1))
				{
					sendKeys(txtBillingstreet1,billingstreet1);
					
					writeTestResults("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in billing information page", 
							"Street field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in billing information page", 
							"Street text field is not displayed in billing information page","",false,false);					
				}
			}
			
			
			
			
			if(findElementInXLSheet(getMethodIsExecuted,"Find Billing Address").equalsIgnoreCase("Yes"))
			{
				click(linkaddressHint);
			}
			
			if(!txtBillingPostcode.isEmpty())
			{
				if(isElementPresent(txtBillingPostcode))
				{		
					
					sendKeys(txtBillingPostcode,billingPostcode);
					Thread.sleep(3000);
					
					click(txtBillingPostcode);
					 Actions action = new Actions(driver);
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      
				      Thread.sleep(3000);
				      action.sendKeys(Keys.ENTER).build().perform();
				      sendKeys(txtBillingPostcode,billingPostcode);
						Thread.sleep(3000);
						
						click(txtBillingPostcode);
						
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      action.sendKeys(Keys.ARROW_DOWN).build().perform();
					      
					      Thread.sleep(3000);
					      action.sendKeys(Keys.ENTER).build().perform();
					
					
					if(isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In Checkout"))
					{
						
						if(!isDisplayed(txtPostalCode))
						{
							click(txtPostalCode);
						}
						
						else
						{
							
						}
						if(!isElementPresent(selectBillingPostCodeDropDown))
						{
							i =0;
							while(!isElementPresent(selectBillingPostCodeDropDown))
							{
								if(isElementPresent(selectBillingPostCodeDropDown))
								{
									break;
								}
								else
								{
									Thread.sleep(1000);
									i++;
								}
								
								if(i == 5)
								{
									break;
								}
							}
						}
						
						click(selectBillingPostCodeDropDown);												
					}				
					writeTestResults("Verify the 'Post Code' text field displayed in billing information page", "Post Code text field should be displayed in billing information page",
							"Post Code field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Post Code' text field displayed in billing information page", "Post Code text field should be displayed in billing information page", 
							"Post Code text field is not displayed in billing information page","",false,false);
				}
			}
			
			
			
			
			
			
			
			if(!txtBillingCity.isEmpty())
			{
				if(isDisplayed(txtBillingCity))
				{
//					if(!isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In Checkout"))
//					{
						sendKeys(txtBillingCity,billingCity);
//					}	
//					else
//					{
//						click(txtBillingCity);
//					}
					writeTestResults("Verify the 'City' text field displayed in billing information page", "City text field should be displayed in billing information page",
							"City field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'City' text field displayed in billing information page", "City text field should be displayed in billing information page", 
							"City text field is not displayed in billing information page","",false,false);					
				}
			}	
			
			
			
			
			if(!txtPassword.isEmpty() && !txtConfirmPassword.isEmpty())
			{
				if(isDisplayed(txtPassword)&& isDisplayed(txtConfirmPassword))
				{
					sendKeys(txtPassword,billingPassword);
					sendKeys(txtConfirmPassword,billingConfirmPassword);
					
				}	
					writeTestResults("Verify the 'Password and confirm password' text field displayed in billing information page", "Password and confirm password text field should be displayed in billing information page",
							"Password and confirm password field is displayed in billing information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Password and confirm password' text field displayed in billing information page", "Password and confirm password text field should be displayed in billing information page", 
							"Password and confirm password text field is not displayed in billing information page","",false,false);					
				}
									

	
			
			if(!selectBillingCountry_Id.isEmpty())
			{
				if(isDisplayed(selectBillingCountry_Id))
				{
					selectText(selectBillingCountry_Id,billingCountry_Id);
					
					writeTestResults("Verify the 'Country' dropdown field displayed in billing information page", "Country dropdown field should be displayed in billing information page", 
							"Country field is displayed in billing information page","",true,false);
				}
				else
				{	
					writeTestResults("Verify the 'Country' dropdown field displayed in billing information page", "Country dropdown field should be displayed in billing information page", 
							"Country dropdown field is not displayed in billing information page","",false,false);					
				}
			}			
			
			
			if(!selectBillingRegion_Id.isEmpty())
			{
				if(isDisplayed(selectBillingRegion_Id))
				{
					selectText(selectBillingRegion_Id,billingRegion_Id);	
					
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
							"Region field is displayed in billing information page","",true,false);
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
							"Region dropdown field is not displayed in billing information page","",false,false);					
				}
			}
			
			if(!selectBillingRegion_Id_DropDown.isEmpty())
			{
				if(isDisplayed(selectBillingRegion_Id_DropDown))
				{
					System.out.println("selectBillingRegion_Id_DropDown"+selectBillingRegion_Id_DropDown);
					
					click(selectBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					
					click(billingRegion_Id+"_Link");
					
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page",
							"Region field is displayed in billing information page","",true,false);
				}
				else if(isDisplayed(selectMemberBillingRegion_Id_DropDown))
				{
					click(selectMemberBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					
					click(billingRegion_Id+"_Link");
					
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
							"Region field is displayed in billing information page","",true,false);
				
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page",
							"Region dropdown field is not displayed in billing information page","",false,false);					
				}
			}
			
			if(!txtBillingRegion.isEmpty())
			{
				if(isDisplayed(txtBillingRegion))
				{
					sendKeys(txtBillingRegion,billingRegion_Id);
					
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", 
							"Region field is displayed in billing information page","",true,false);	
					
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page",
							"Region dropdown field is not displayed in billing information page","",false,false);					
				}
			}
			if(isExecuted(getMethodIsExecuted,"Billing has birthday") && writeFileName.indexOf("Registered-") < 0)
			{
				if(writeFileName.indexOf("Reg-")< 0)
				{
					selectDOB();
				}
				
			}
			
		}
	}	
	
	public void selectDOB() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"Birthday has dropdown"))
		{
			if(isElementPresent(billingBirthdayDay))
			{
				selectText(billingBirthdayDay,user.userCreationBirthdayDay);
			}
			else
			{			
				 writeTestResults("Verify the 'day' dropdown displayed in billing info page", "Day dropdown sould be displayed in billing info page", "Day dropdown is not displayed in billing info page","",false,false);
			}
			if(isElementPresent(billingBirthdayMonth))
			{
				selectText(billingBirthdayMonth,user.userCreationBirthdayMonth);
			}
			else
			{			
				 writeTestResults("Verify the 'Month' dropdown displayed in billing info page", "Month dropdown sould be displayed in billing info page", "Month dropdown is not displayed in billing info page","",false,false);
			}
			if(isElementPresent(billingBirthdayYear))
			{
				selectText(billingBirthdayYear,user.userCreationBirthdayYear);
			}
			else
			{			
				 writeTestResults("Verify the 'Year' dropdown displayed in billing info page", "Year dropdown sould be displayed in billing info page", "Year dropdown is not displayed in billing info page","",false,false);
			}
			
		}
	}
	
	/*
	 * if(isExecuted(getMethodIsExecuted,"Birthday has dropdown"))
		{
			if(isElementPresent(userCreationDobDayDropdown))
			{
				selectText(userCreationDobDayDropdown,userCreationBirthdayDay);
			}
			else
			{			
				 writeTestResults("Verify the 'day' dropdown displayed in billing info page", "Day dropdown sould be displayed in billing info page", "Day dropdown is not displayed in billing info page","",false,false);
			}
			if(isElementPresent(userCreationDobMonthDropdown))
			{
				selectText(userCreationDobMonthDropdown,userCreationBirthdayMonth);
			}
			else
			{			
				 writeTestResults("Verify the 'Month' dropdown displayed in billing info page", "Month dropdown sould be displayed in billing info page", "Month dropdown is not displayed in billing info page","",false,false);
			}
			if(isElementPresent(userCreationDobYearDropdown))
			{
				selectText(userCreationDobYearDropdown,userCreationBirthdayYear);
			}
			else
			{			
				 writeTestResults("Verify the 'Year' dropdown displayed in billing info page", "Year dropdown sould be displayed in billing info page", "Year dropdown is not displayed in billing info page","",false,false);
			}
			
		}
	 */
	public void createAccountLater() throws Exception
	{
		if(!billingCreateAccount.isEmpty())
		{
			if(isDisplayed(billingCreateAccount))
			{
				click(billingCreateAccount);	
				
				writeTestResults("Verify the 'Create an account and save your address for next time'  checkbox displayed in billing information page", 
						"Create an account and save your address for next time field should be displayed in billing information page", 
						"Create an account and save your address for next time  field is displayed in billing information page","",true,false);	
			}
			else
			{
				writeTestResults("Verify the 'Create an account and save your address for next time'  checkbox displayed in billing information page", 
						"Create an account and save your address for next time field should be displayed in billing information page", 
						"Create an account and save your address for next time  field is not displayed in billing information page","",false,true);
			}
		}
	}
	
	public void billingInfoConButton() throws Exception
	{
		String alert = "";	
		
		if(isDisplayed(btnBillingInfoContinue))
		{			
			click(btnBillingInfoContinue);
			
			if(isAlertPresent())
			{
				alert =getAlert();
				
				acceptAlert();
				
				writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
						"Following error found. "+alert,"",false,true);
			}
			
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxBilling.isEmpty())
			{
				if(!getAttribute(chkoutAjaxBilling,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxBilling))
					{
						writeTestResults("Verify user clicks continue button with correct data in billing information page", "Shipping information screen should be displayed", 
								"Page is not load within 100sec","",false,true);					
					}
				}
			}
			else
			{
				Thread.sleep(2000);
			}
			
			if(isBillingInfoValidation)
			{
				billingInforEmptyValidation();
				
				isBillingInfoValidation = false;
			}
			else
			{
				billingInforSubmitValidation();
			}			
		}
		else
		{
			writeTestResults("Verify user clicks continue button with correct data in billing information page", "Shipping information screen should be displayed", 
					"Shipping information screen is not displayed","4270d966",false,true);			
		}		
	}
	
	
	public void DeliveryInfoConButton() throws Exception
	{
		String alert = "";	
		
		if(isDisplayed(btnShippingMethodContinue))
		{		
			
			click(btnShippingMethodContinue);
			
			if(isAlertPresent())
			{
				alert =getAlert();
				
				acceptAlert();
				
				writeTestResults("Verify user clicks submit button with correct data in Delivery information page", "Delivery options page should displayed", 
						"Following error found. "+alert,"",false,true);
			}
			
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxBilling.isEmpty())
			{
				if(!getAttribute(chkoutAjaxBilling,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxBilling))
					{
						writeTestResults("Verify user clicks continue button with correct data in delivery information page", "Delivery options screen should be displayed", 
								"Page is not load within 100sec","",false,true);					
					}
				}
			}
			else
			{
				Thread.sleep(2000);
			}
			
			if(isDeliveryInfoValidation)
			{
				DeliveryInforEmptyValidation();
				
				isDeliveryInfoValidation = false;
			}
			else
			{
				billingInforSubmitValidation();
			}			
		}
		
		else if(isDisplayed(btnDeliveryMethodContinue))
		{

			click(btnDeliveryMethodContinue);
			
			if(isAlertPresent())
			{
				alert =getAlert();
				
				acceptAlert();
				
				writeTestResults("Verify user clicks submit button with correct data in Delivery information page", "Delivery options page should displayed", 
						"Following error found. "+alert,"",false,true);
			}
			
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxBilling.isEmpty())
			{
				if(!getAttribute(chkoutAjaxBilling,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxBilling))
					{
						writeTestResults("Verify user clicks continue button with correct data in delivery information page", "Delivery options screen should be displayed", 
								"Page is not load within 100sec","",false,true);					
					}
				}
			}
			else
			{
				Thread.sleep(2000);
			}
			
			if(isDeliveryInfoValidation)
			{
				DeliveryInforEmptyValidation();
				
				isDeliveryInfoValidation = false;
			}
			else
			{
				billingInforSubmitValidation();
			}
			
		}
		
		else
		{
			writeTestResults("Verify user clicks continue button with correct data in Delivery information page", "Delivery options screen should be displayed", 
					"Delivery options screen is not displayed","4270d966",false,true);			
		}		
	}
	
	
	public void chkSameAddress() throws Exception
	{
		if(!chkSameShipAddress.isEmpty())
		{	
			if(!isSelected(chkSameShipAddress))
			{

					writeTestResults("Verify user can select same billing address in delivery information page", "User can select same billing address option", 
							"User can select same shipping address option","4270d966",true,false);
					
					click(chkSameShipAddress);

				
			}				
		}	
	}
	
	
	
	
	public void chkDifferentAddress() throws Exception
	{		
		if(!isVirtualProduct)
		{	
			if(isSelected(chkSameShipAddress))
			{

					writeTestResults("Verify user can select different shipping address in billing information page", "User can select different shipping address option", 
							"User can select different shipping address option","4270d966",true,false);
					
					click(chkSameShipAddress);

				
			}				
		}		
	}
	
	public void memberNewShippingAddressValidation() throws Exception
	{		
		if(!selectNewShippingAddressDropDown.isEmpty())
		{
			if(isDisplayed(selectNewShippingAddressDropDown))
			{	
				//click(selectNewShippingAddressDropDown);
				
				//click(newShippingAddressdropdownlabel);
				
				selectText(selectNewShippingAddressDropDown, newShippingAddressdropdownlabel);
				
				writeTestResults("Verify member can select 'New Address' in shipping information page", "User can select 'New Address' in shipping information page", 
						"User can select 'New Address' in shipping information page","",true,false);	
				
				validateShippingInfoPage();
			}
			else
			{
				validateShippingInfoPage();
			}
		}
		else
		{
			validateShippingInfoPage();	
		}
	}	
	
	public void memberNewShippingAddress() throws Exception
	{		
		if(!selectNewShippingAddressDropDown.isEmpty())
		{
			if(isElementPresent(selectNewShippingAddressDropDown))
			{
				//click(selectNewShippingAddressDropDown);
				
				//click(newShippingAddressdropdownlabel);
				
				selectText(selectNewShippingAddressDropDown, newShippingAddressdropdownlabel);
										
				writeTestResults("Verify member can select 'New Address' in shipping information page", "User can select 'New Address' in shipping information page", 
						"User can select 'New Address' in shipping information page","",true,false);				
				
				shippingInfoPage();
			}
			else
			{
				shippingInfoPage();
			}
		}
		else
		{
			shippingInfoPage();	
		}
	}
	
	public void shippingInfoPage() throws Exception
	{
		if(!isVirtualProduct)
		{
			if(!txtShippingFirstName.isEmpty())
			{
				if(isDisplayed(txtShippingFirstName))
				{
					sendKeys((txtShippingFirstName),shippingFirstName);
					writeTestResults("Verify the 'First Name' text field displayed in shipping information page", "First Name text field should be displayed in shipping information page", 
							"First Name text field is displayed in shipping information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'First Name' text field displayed in shipping information page", "First Name text field should be displayed in shipping information page", 
							"First Name text field is not displayed in shipping information page","",false,false);					
				}
			}
			if(!txtShippingLastName.isEmpty())
			{
				if(isDisplayed(txtShippingLastName))
				{
					sendKeys(txtShippingLastName,shippingLastName);
					
					writeTestResults("Verify the 'Last Name' text field displayed in shipping information page", "Last Name text field should be displayed in shipping information page", 
							"Last Name text field is displayed in shipping information page","",true,false);						
				}
				else
				{
					writeTestResults("Verify the 'Last Name' text field displayed in shipping information page", "Last Name text field should be displayed in shipping information page", 
							"Last Name text field is not displayed in shipping information page","",false,false);
				}
			}
					
			if(!txtShippingTelephone.isEmpty())
			{
				if(isDisplayed(txtShippingTelephone))
				{
					sendKeys(txtShippingTelephone,shippingTelephone);
					
					writeTestResults("Verify the 'Contact Number' text field displayed in shipping information page", "Contact Number text field should be displayed in shipping information page", 
							"Contact Number field is displayed in shipping information page","",true,false);	
				}
				else
				{
					writeTestResults("Verify the 'Contact Number' text field displayed in shipping information page", "Contact Number text field should be displayed in shipping information page", 
							"Contact Number text field is not displayed in shipping information page","",false,false);
				}
			}
			
			if(findElementInXLSheet(getMethodIsExecuted,"Find Shipping Address").equalsIgnoreCase("Yes"))
			{
				click(linkaddressHint);
			}
			
			if(!txtShippingPostcode.isEmpty())
			{
				if(isDisplayed(txtShippingPostcode))
				{

					click(txtShippingPostcode);
				
				sendKeys(txtShippingPostcode,shippingPostcode);
				

			      Thread.sleep(8000);
					Actions action = new Actions(driver);
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      action.sendKeys(Keys.ARROW_DOWN).build().perform();
				      
				      Thread.sleep(3000);
				      action.sendKeys(Keys.ENTER).build().perform();
				      
				      click(txtShippingPostcode);
						
						sendKeys(txtShippingPostcode,shippingPostcode);
						

					      Thread.sleep(8000);
							
						      action.sendKeys(Keys.ARROW_DOWN).build().perform();
						      action.sendKeys(Keys.ARROW_DOWN).build().perform();
						      action.sendKeys(Keys.ARROW_DOWN).build().perform();
						      action.sendKeys(Keys.ARROW_DOWN).build().perform();
						      action.sendKeys(Keys.ARROW_DOWN).build().perform();
						      
						      Thread.sleep(3000);
						      action.sendKeys(Keys.ENTER).build().perform();
					
					
					if(isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In Checkout"))
					{
						if(!isElementPresent(selectShippingPostCodeDropDown))
						{
							i =0;
							while(!isElementPresent(selectShippingPostCodeDropDown))
							{
								if(isElementPresent(selectShippingPostCodeDropDown))
								{
									break;
								}
								else
								{
									Thread.sleep(1000);
									i++;
								}
								
								if(i ==5)
								{
									break;
								}
							}
						}
						
						click(selectShippingPostCodeDropDown);
					}	
					writeTestResults("Verify the 'Post Code' text field displayed in shipping information page", "Post Code text field should be displayed in shipping information page", 
							"Post Code field is displayed in shipping information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Post Code' text field displayed in shipping information page", "Post Code text field should be displayed in shipping information page", 
							"Post Code text field is not displayed in shipping information page","",false,false);
				}
			}
			
			if(!txtShippingstreet1.isEmpty())
			{
				if(isDisplayed(txtShippingstreet1))
				{
					sendKeys(txtShippingstreet1,shippingstreet1);
					
					writeTestResults("Verify the 'Street' text field displayed in shipping information page", "Street text field should be displayed in shipping information page", 
							"Street field is displayed in shipping information page","",true,false);	
				}
				else
				{
					writeTestResults("Verify the 'Street' text field displayed in shipping information page", "Street text field should be displayed in shipping information page", 
							"Street text field is not displayed in shipping information page","",false,false);
				}
			}
			if(!txtShippingCity.isEmpty())
			{
				if(isDisplayed(txtShippingCity))
				{
					if(!isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In Checkout"))
					{
						sendKeys(txtShippingCity,shippingCity);
					}
					
					writeTestResults("Verify the 'City' text field displayed in shipping information page", "City text field should be displayed in shipping information page",
							"City field is displayed in shipping information page","",true,false);	
					
				}
				else
				{
					writeTestResults("Verify the 'City' text field displayed in shipping information page", "City text field should be displayed in shipping information page",
									"City text field is not displayed in shipping information page","",false,false);					
				}
			}				
				
			if(!selectShippingCountry_Id.isEmpty())
			{
				if(isDisplayed(selectShippingCountry_Id))
				{
					selectText(selectShippingCountry_Id,shippingCountry_Id);
					writeTestResults("Verify the 'Country' dropdown field displayed in shipping information page", "Country dropdown field should be displayed in shipping information page", 
							"Country field is displayed in shipping information page","",true,false);
				}
				else
				{	
					writeTestResults("Verify the 'Country' dropdown field displayed in shipping information page", "Country dropdown field should be displayed in shipping information page", 
							"Country dropdown field is not displayed in shipping information page","",false,false);					
				}
			}
			if(!selectShippingRegion_Id.isEmpty())
			{
				if(isDisplayed(selectShippingRegion_Id))
				{
					selectText(selectShippingRegion_Id,shippingRegion_Id);
					
					writeTestResults("Verify the 'Region' dropdown field displayed in shipping information page", "Region dropdown field should be displayed in shipping information page", 
							"Region field is displayed in shipping information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in shipping information page", "Region dropdown field should be displayed in shipping information page", 
							"Region dropdown field is not displayed in shipping information page","",false,false);					
				}
			}
			
			if(!selectShippingRegion_Id_DropDown.isEmpty())
			{
				if(isDisplayed(selectShippingRegion_Id_DropDown))
				{
					click(selectShippingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					
					click(shippingRegion_Id+"_Link");
					
					writeTestResults("Verify the 'Region' dropdown field displayed in shipping information page", "Region dropdown field should be displayed in shipping information page", 
							"Region field is displayed in shipping information page","",true,false);						
				}
				else if(isDisplayed(selectMemberShippingRegion_Id_DropDown))
				{
					click(selectMemberShippingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					
					click(shippingRegion_Id+"_Link");
					
					writeTestResults("Verify the 'Region' dropdown field displayed in shipping information page", "Region dropdown field should be displayed in shipping information page", 
							"Region field is displayed in shipping information page","",true,false);	
				
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in shipping information page", "Region dropdown field should be displayed in shipping information page",
							"Region dropdown field is not displayed in shipping information page","",true,false);					
				}
			}
			
			if(!txtShippingRegion.isEmpty())
			{
				if(isDisplayed(txtShippingRegion))
				{
					sendKeys(txtShippingRegion,shippingRegion_Id);
					writeTestResults("Verify the 'Region' dropdown field displayed in shipping information page", "Region dropdown field should be displayed in shipping information page", 
							"Region field is displayed in shipping information page","",true,false);					
				}
				else
				{
					writeTestResults("Verify the 'Region' dropdown field displayed in shipping information page", "Region dropdown field should be displayed in shipping information page",
							"Region dropdown field is not displayed in shipping information page","",false,false);
				}
			}			
		}
		else
		{
			writeTestResults("Verify the text fields displayed in shipping information page", "This product is 'Virtual' product so no shipping infor page for this product ", 
					"This product is 'Virtual' product so no shipping infor page for this product","",true,false);
		}
	}	
	
	public void validateShippingInfoPage() throws Exception
	{
		if(!isVirtualProduct)
		{
			common.magentoProfilerResults(writeFileName+"Shipping infor page");
			
			isShippingInfoValidation =true;
			
			if(!txtShippingFirstName.isEmpty())
			{
				if(isDisplayed(txtShippingFirstName))
				{
					sendKeys((txtShippingFirstName),"");									
				}
			}
			if(!txtShippingLastName.isEmpty())
			{
				if(isDisplayed(txtShippingLastName))
				{
					sendKeys(txtShippingLastName,"");									
				}
			}
					
			if(!txtShippingTelephone.isEmpty())
			{
				if(isDisplayed(txtShippingTelephone))
				{
					sendKeys(txtShippingTelephone,"");
				}
			}
			
			if(findElementInXLSheet(getMethodIsExecuted,"Find Shipping Address").equalsIgnoreCase("Yes"))
			{
				click(linkaddressHint);
			}
			
			if(!txtShippingPostcode.isEmpty())
			{
				if(isDisplayed(txtShippingPostcode))
				{					
					sendKeys(txtShippingPostcode,"");										
				}
			}
			
			if(!txtShippingstreet1.isEmpty())
			{
				if(isDisplayed(txtShippingstreet1))
				{
					sendKeys(txtShippingstreet1,"");				
				}
			}
			if(!txtShippingCity.isEmpty())
			{
				if(isDisplayed(txtShippingCity))
				{
					sendKeys(txtShippingCity,"");							
				}
			}		
					
				
			if(!selectShippingCountry_Id.isEmpty())
			{
				if(isDisplayed(selectShippingCountry_Id))
				{
					selectText(selectShippingCountry_Id,"");			
				}
			}
			if(!selectShippingRegion_Id.isEmpty())
			{
				if(isDisplayed(selectShippingRegion_Id))
				{
					selectText(selectShippingRegion_Id,"");		
				}
			}
			
			if(!txtShippingRegion.isEmpty())
			{
				if(isDisplayed(txtShippingRegion))
				{
					sendKeys(txtShippingRegion,"");
				}
			}			
		}
	}	
	
	public void shippingInfoConButton() throws Exception
	{
		if(!isVirtualProduct)
		{
			if(isElementPresent(btnShippingInfoContinue))
			{	
				writeTestResults("Verify the continue button is displayed in shipping information page", "Continue button should be displayed in billing information page", 
						"Continue button is displayed in shipping information page","",true,false);
				
				click(btnShippingInfoContinue);	
				
				verifyErrorMsgIShippingInfor();	
				
				if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxShipping.isEmpty())
				{
					if(!getAttribute(chkoutAjaxShipping,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
					{
						if(!checkoutAjaxRequest(chkoutAjaxShipping))
						{
							writeTestResults("Verify user clicks continue button with correct data in shipping information page", "Payment information screen should be displayed", 
									"Page is not load within 100sec","",false,true);	
						}
					}
				}
				else
				{
					Thread.sleep(5000);
				}				
				if(isShippingInfoValidation)
				{
					verifyErrorMsgIShippingInfor();	
					
					shippingInforEmptyValidation();
					
					isShippingInfoValidation = false;
				}
				else
				{
					submitShippingValidation();
					
					verifyErrorMsgIShippingInfor();	
				}
				
			}
			else
			{
				writeTestResults("Verify user clicks continue button with correct data in billing information page", "Shipping information screen should be displayed", 
						"Shipping information screen is not displayed","",false,true);				
			}
		}		
	}	
	
	public void shippingMethodValidation() throws Exception
	{
		if(!isVirtualProduct)
		{
			common.magentoProfilerResults(writeFileName+"Select shipping method");
			
			if(!isElementPresent(btnShippingMethodContinue))
			{
				Thread.sleep(2000);
			}	
			if(isElementPresent(btnShippingMethodContinue))
			{
				writeTestResults("","","","4270e668",true,false);
			}	
			
			int totalShippingMethods = getTagListSize(getTotalShippingMethodsXpath,getShippingMethodTag);									   
			
			writeTestResults("Verify available shipping methods displayed in shipping method section", 
					"Shipping method types should be displayed",totalShippingMethods +" shipping method(s) are displayed","4270e942",true,false);
			
			if(totalShippingMethods >= 1)
			{
				i = 1;
				
				//checked radio button is already selected

				if(i > totalShippingMethods)
				{
					if(isElementPresent(btnShippingMethodContinue))
					{							
						click(btnShippingMethodContinue);			
						
						if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxShippingMethod.isEmpty())
						{
							if(!getAttribute(chkoutAjaxShippingMethod,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
							{
								if(!checkoutAjaxRequest(chkoutAjaxShippingMethod))
								{
									writeTestResults("Verify user clicks continue button with correct data in billing information page", "Payment information screen should be displayed",
											"Page is not load within 100sec","",false,true);								
								}
							}
						}
						else
						{
							Thread.sleep(5000);
						}
						
						vlaidationMsgInShippingMethods();		
					}
				}
			}				
		}		
	}	
	
	public void vlaidationMsgInShippingMethods() throws Exception
	{
		String alert = "";
		
		if(isAlertPresent())
		{
			alert =getAlert();
			
			writeTestResults("Verify user can't navigate to payment section  without selecting shipping method", "Error message should be displayed", 
					"Following error found :"+alert,"",true,false);
		}	
	}	
	
	public void selectShippingMethod() throws Exception
	{
		String getShippingValueAttri = "";
		String getShippingMethodAttri = "";
		if(!isVirtualProduct)
		{
			int totalShippingMethods = getTagListSize(getTotalShippingMethodsXpath,getShippingMethodTag);
			
			if(totalShippingMethods == 0)
			{
				writeTestResults("Verify the shipping methods displayed in shipping method page", "Shipping methods should be displayed in shipping method page", 
						"Shipping methods not displayed in shipping method page","4270e942",false,true);
			}
			
			System.out.println("Total Shipping Methods"+totalShippingMethods);
			if(totalShippingMethods>1)
			{					
				i = 1;
				
				getShippingValueAttri = getShiipingValue.replace("{id}", "["+i+"]");
				getShippingMethodAttri = selectShippingMethod.replace("{id}", "["+i+"]").replaceAll("input", "label");
				
				if(getText(getShippingValueAttri).equalsIgnoreCase(""))
				{
					i = 2;						
					getShippingValueAttri = getShiipingValue.replace("{id}", "["+i+"]");
					getShippingMethodAttri = selectShippingMethod.replace("{id}", "["+i+"]").replaceAll("input", "label");
					
					
					if(!getText(getShippingValueAttri).equalsIgnoreCase(""))
					{
						shippingCharges = getText(getShippingValueAttri);
						if(isDisplayed(getShippingMethodAttri))
						{
							shippingCharges = getText(getShippingValueAttri);
							click(getShippingMethodAttri);
						}
						else
						{
							writeTestResults("Verify user shipping method check box", "Shipping method checkbox should displayed","Shipping method chcekbox is not displayed","",false,true);
						}
					}
				}
				else
				{
					if(isDisplayed(getShippingMethodAttri))
					{
						shippingCharges = getText(getShippingValueAttri);
						click(getShippingMethodAttri);
					}
					else
					{
						writeTestResults("Verify user shipping method check box", "Shipping method checkbox should displayed","Shipping method chcekbox is not displayed","",false,true);
					}
					
				}
				
				hasShippingMethod = true;				
			}
			if(totalShippingMethods == 1)
			{	
				getShippingValueAttri = getShiipingValue.replace("{id}", "[1]");
				
				shippingCharges = getText(getShippingValueAttri);
				
				System.out.println("getShippingValueAttri"+getShippingValueAttri);
				System.out.println("shippingCharges"+shippingCharges);
				
				hasShippingMethod = true;	
			}
			
		}
		else
		{
			writeTestResults("Verify the shipping methods displayed in shipping method page", "This product is 'Virtual' product so no shipping infor page for this product ", 
					"This product is 'Virtual' product so no shipping infor page for this product","4270e942",true,false);
		}
		
		totalPriceBeforeShipping = common.grandTotalInShoppingcart;
		
		System.out.println("totalPriceBeforeShipping"+totalPriceBeforeShipping);
		
		if(!isExecuted(getMethodIsExecuted,"Order review details in checkout page"))
		{
			//totalPriceBeforeShipping = common.getValueWitoutCurrency(common.grandTotalXpathAfterDiscount);
			totalPriceBeforeShipping = common.grandTotalInShoppingcart;
			
		}
		
		System.out.println("Total Price Before Shipping"+totalPriceBeforeShipping);
	}
	
	public void shippingMethodConButton() throws Exception
	{	
		if(!isVirtualProduct)
		{
			if(isElementPresent(btnDeliveryMethodContinue))
			{	
				writeTestResults("Verify the continue button is displayed in shipping method page", "Continue button should be displayed in shipping method page", 
						"Continue button is displayed in shipping method page","",true,false);
				
				click(btnDeliveryMethodContinue);	
				
				if(isAlertPresent())
				{
					String alert =getAlert();
					
					acceptAlert();
					
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"Following error found. "+alert,"",false,true);
				}
				
				if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxShippingMethod.isEmpty())
				{
					if(!getAttribute(chkoutAjaxShippingMethod,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
					{
						if(!checkoutAjaxRequest(chkoutAjaxShippingMethod))
						{
							writeTestResults("Verify user clicks continue button with correct data in billing information page", "Payment information screen should be displayed", 
									"Page is not load within 100sec","4270ebe0",false,true);							
						}
					}
				}
				else
				{
					Thread.sleep(5000);
				}
				
				verifyErrorMsgIShippingContinue();
								
			}
			else
			{
				writeTestResults("Verify the continue button is displayed in shipping method page", "Continue button should be displayed in shipping method page", 
						"Continue button is not displayed in shipping method page","4270ebe0",false,true);			
			}
		}	
	}
	
	public void verifyErrorMsgIShippingContinue() throws Exception
	{
		String alert = "";
				
		if(!isDisplayed(btnContinuePaymentScreen))
		{
			if(isAlertPresent())
			{
				alert =getAlert();
				writeTestResults("Verify 'Payment' Page", "'Payment' page should be displayed", "Following error found :"+alert,"4270ebe0",false,true);
			}
			else if(isDisplayed(errorMsg))
			{
				for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
				{
					if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
					{	
						writeTestResults("Verify 'Payment' Page", "'Payment' page should be displayed", "Following error found :"+getText(errorMsg),"4270ebe0",false,true);
					}
				}
			}			
		}	
		
		//if(isExecuted(getMethodIsExecuted,"Select Shipping Method") && !hasShippingMethod)
		if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
		{
			totalPriceAftereShipping = totalPriceBeforeShipping + common.getValueWitoutCurrency(shippingCharges);			
		}
		else
		{
			totalPriceAftereShipping = totalPriceBeforeShipping;
		}
		
		System.out.println("totalPriceAftereShipping"+totalPriceAftereShipping);
		
		System.out.println("total"+totalPriceBeforeShipping + common.getValueWitoutCurrency(shippingCharges));
		
		if(hasShippingMethod)
		{
			double totalValue;
			if(isExecuted(getMethodIsExecuted,"Order review details in checkout page"))
			{
				totalValue = common.getValueWitoutCurrency(getText(productTotalPriceAfterShippingInOrderReview));
			}
			else
			{
				totalValue = common.getValueWitoutCurrency(getText(productTotalPriceXpathInOrderReview));
			}
			
			
			totalPriceAftereShipping = totalPriceBeforeShipping + common.getValueWitoutCurrency(shippingCharges);
			
			System.out.println("totalPriceAftereShipping"+totalPriceAftereShipping);	
			if(isExecuted(getMethodIsExecuted,"Order review details in checkout page"))
			{
				if(common.getValueWitoutCurrency(getText(productTotalPriceAfterShippingInOrderReview)) != totalPriceAftereShipping)
				{
					writeTestResults("Verify the total price of the selected product is displaying in order review after adding shiiping charges", 
							"Expected total product price is "+totalPriceAftereShipping +" in order review", 
							"Actual product total price is "+common.getValueWitoutCurrency(getText(productTotalPriceAfterShippingInOrderReview))+" in order review","",false,false);					
				}
				else
				{
					writeTestResults("Verify the total price of the selected product is displaying in order review after adding shiiping charges", 
							"Expected total product price is "+totalPriceAftereShipping +" in order review", 
							"Actual product total price is "+common.getValueWitoutCurrency(getText(productTotalPriceAfterShippingInOrderReview))+" in order review","",true,false);
				}
			}
			
		}			
	}
	
	
	public void paymentMethodsTypes() throws Exception
	{
		if(isDisplayed(chkPaymentIsCC) && isDisplayed(chkPaymentIsPaypal))
		{
			writeTestResults("Verify user can view all the payment options", "All payment types should be in payment information page", 
					"All payment types are in payment information page","4270f1a8",true,false);
		}
			
	}
	
	public void paymentMethodIsCC() throws Exception
	{
		
		
		if(!isDisplayed(chkPaymentIsCC))
		{
			Thread.sleep(2000);
		}
		if(isElementPresent(chkPaymentIsCC))
		{
			common.magentoProfilerResults(writeFileName+"Payment method page");
			
			click(chkPaymentIsCC);
			
			
			
			
			
			
			writeTestResults("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", 
					"User can select payment type is credit card in payment information page","4270fc84",true,false);			
		}
		else
		{
			writeTestResults("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", 
					"Credit card option is not displayed or credit card xpath is incorrect in payment information page","4270fc84",false,true);
		}		
	}
	
	public void paymentMethodIsPaypal() throws Exception
	{
		if(isDisplayed(chkPaymentIsPaypal))
		{
			common.magentoProfilerResults(writeFileName+"Payment method page");
			
			click(chkPaymentIsPaypal);
			
			writeTestResults("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", 
					"User can select payment type is paypal in payment information page","",true,false);			
		}
		else
		{
			writeTestResults("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", 
					"Paypal option is not displayed or paypal xpath is incorrect in payment information page","",false,true);	
		}		
	}
	
	public void creditCardValidation() throws Exception
	{	
		creditCardEmptyValidation();
	}
	
	public void creditCardEmptyValidation() throws Exception
	{	
		if(findElementInXLSheet(getMethodIsExecuted,"Select DataCash Iframe").equalsIgnoreCase("Yes"))
		{	
			Thread.sleep(10000);
			switchToFrame("datacash_hcc_iframe_block");
			Thread.sleep(1000);
		}
		else if(!isElementPresent(btnContinuePaymentScreen))
		{
			Thread.sleep(10000);
		}
		
		if(!txtCCName.isEmpty())
		{
			if(isElementPresent(txtCCName))
			{
				sendKeys(txtCCName,cCName);
			}				
		}
		if(!txtCCNumber.isEmpty())
		{
			if(isElementPresent(txtCCNumber))
			{
				sendKeys(txtCCNumber,cCNumber);											
			}			
		}
		if(!selectCCType.isEmpty())
		{
			if(isElementPresent(selectCCType))
			{
				selectText(selectCCType,cCType);								
			}
		}
		
		if(!txtCCCvs.isEmpty())
		{
			if(isElementPresent(txtCCCvs))
			{
				sendKeys(txtCCCvs,cCCvs);							
			}			
		}
		if(!selectCCMonth.isEmpty())
		{
			if(isElementPresent(selectCCMonth))
			{
				selectText(selectCCMonth,cCMonth);										
			}			
		}
		
		if(!selectCCYear.isEmpty())
		{
			if(isElementPresent(selectCCYear))
			{
				selectText(selectCCYear,cCYear);				
			}			
		}
		
		if(isElementPresent(btnContinuePaymentScreen))
		{			
			click(btnContinuePaymentScreen);
		}		
	}
	
	public void fillCreditCardDetails() throws Exception
	{	
		if(isExecuted(getMethodIsExecuted,"Select DataCash Iframe"))
		{	
			waitForPageLoad();
								
			switchToFrame("datacash_hcc_iframe_Id");
			Thread.sleep(1000);
		}
		
		if(!txtCCName.isEmpty())
		{
			if(isElementPresent(txtCCName))
			{
				sendKeys(txtCCName,cCName);
				
				writeTestResults("Verify the 'Credit Card Name' text field displayed in payment information page", "'Credit card Name' text field should be displayed in payment information page",
						"'Credit Card Name' field is displayed in payment information page","",true,false);					
			}
			else
			{
				writeTestResults("Verify the 'Credit Card Name' text field displayed in payment information page", "'Credit card Name' text field should be displayed in payment information page", 
						"'Credit Card Name' field is not displayed in payment information page","",false,false);									
			}			
		}
		if(!txtCCNumber.isEmpty())
		{
			if(isElementPresent(txtCCNumber))
			{
				sendKeys(txtCCNumber,cCNumber);
				
				writeTestResults("Verify the 'Credit Card Number' text field displayed in payment information page", "'Credit Card Number' text field should be displayed in payment information page",
						"'Credit Card Number' field is displayed in payment information page","",true,false);					
			}
			else
			{
				writeTestResults("Verify the 'Credit Card Number' text field displayed in payment information page", "'Credit Card Number' text field should be displayed in payment information page", 
						"'Credit Card Number' field is not displayed in payment information page","",false,false);
			}			
		}
		if(!selectCCType.isEmpty())
		{
			if(isElementPresent(selectCCType))
			{
				selectText(selectCCType,cCType);
				
				writeTestResults("Verify the 'Credit Card Type' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", 
						"'Credit Card Type' field is displayed in payment information page","",true,false);	
			}
			else
			{
				writeTestResults("Verify the 'Credit Card Type' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", 
						"'Credit Card Type' field is not displayed in payment information page","",false,false);								
			}		
		}
		
		if(!txtCCCvs.isEmpty())
		{
			if(isElementPresent(txtCCCvs))
			{
				sendKeys(txtCCCvs,cCCvs);
				
				writeTestResults("Verify the 'Credit Card CVC' text field displayed in payment information page", "'Credit Card CVC' text field should be displayed in payment information page",
						"'Credit Card CVC' field is displayed in payment information page","",true,false);					
			}
			else
			{
				writeTestResults("Verify the 'Credit Card CVC' text field displayed in payment information page", "'Credit Card CVC' text field should be displayed in payment information page", 
						"'Credit Card CVC' field is not displayed in payment information page","",false,false);
			}			
		}
		if(!selectCCMonth.isEmpty())
		{
			if(isElementPresent(selectCCMonth))
			{
				selectText(selectCCMonth,cCMonth);
				
				writeTestResults("Verify the 'Credit Card Expire Month' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", 
						"'Credit Card Expire Month' field is displayed in payment information page","",true,false);	
			}
			else
			{
				writeTestResults("Verify the 'Credit Card Expire Month' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", 
						"'Credit Card Expire Month' field is not displayed in payment information page","",false,false);						
			}			
		}
		
		if(!selectCCYear.isEmpty())
		{
			if(isElementPresent(selectCCYear))
			{
				click(selectCCYear);
				selectText(selectCCYear,cCYear);
				
				writeTestResults("Verify the 'Credit Card Expire Year' field displayed in payment information page", "'Credit Card Expire Year' field should be displayed in payment information page", 
						"'Credit Card Expire Year' field is displayed in payment information page","",true,false);					
			}
			else
			{
				writeTestResults("Verify the 'Credit Card Expire Year' field displayed in payment information page", "'Credit Card Expire Year' field should be displayed in payment information page", 
						"'Credit Card Expire Year' field is not displayed in payment information page","",false,false);									
			}			
		}
		
		if(!selectCCTypeDropDown.isEmpty())
		{
			if(isElementPresent(selectCCTypeDropDown))
			{
				click(selectCCTypeDropDown);
				
				Thread.sleep(500);
				
				click(selectCCTypeDropDownValue.replaceAll("id1", cCType));
				
				writeTestResults("Verify the 'Credit Card Type' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", 
						"'Credit Card Type' field is displayed in payment information page","",true,false);					
			}
			else
			{
				writeTestResults("Verify the 'Credit Card Type' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", 
						"'Credit Card Type' field is not displayed in payment information page","",false,false);									
			}
		}
		if(!selectCCMonthDropDown.isEmpty())
		{
			if(isElementPresent(selectCCMonthDropDown))
			{				
				click(selectCCMonthDropDown);
				
				Thread.sleep(500);
				
				click(selectCCMonthDropDownValue.replaceAll("id1", cCMonth));
				
				writeTestResults("Verify the 'Credit Card Expire Month' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", 
						"'Credit Card Expire Month' field is displayed in payment information page","",true,false);				
			}
			else
			{
				writeTestResults("Verify the 'Credit Card Expire Month' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", 
						"'Credit Card Expire Month' field is not displayed in payment information page","",false,false);
			}
		}
		if(!selectCCYearDropDown.isEmpty())
		{
			if(isElementPresent(selectCCYearDropDown))
			{
				click(selectCCYearDropDown);
				
				Thread.sleep(500);
				
				click(selectCCYearDropDownValue.replaceAll("id1", cCYear));
				
				writeTestResults("Verify the 'Credit Card Expire Year' field displayed in payment information page", "'Credit Card Expire Year' field should be displayed in payment information page", 
						"'Credit Card Expire Year' field is displayed in payment information page","",true,false);				
			}
			else
			{
				writeTestResults("Verify the 'Credit Card Expire Year' field displayed in payment information page", "'Credit Card Expire Year' field should be displayed in payment information page",
						"'Credit Card Expire Year' field is not displayed in payment information page","",false,false);									
			}
		}		
	}
	
	public void selectPaymentTeamsAndCondition() throws Exception
	{
		if(isDisplayed(chkTeamsAndCondtion))
		{
			click(chkTeamsAndCondtion);
			
			writeTestResults("Verify the 'Teams and condition' field displayed in payment information page", "'Teams and condition' should be displayed in payment information page", 
					"'Teams and condition' is displayed and user can tick the 'Teams and condition' in payment information page","",true,false);		}
		else
		{
			writeTestResults("Verify the 'Teams and condition' field displayed in payment information page", "'Teams and condition' should be displayed in payment information page", 
					"'Teams and condition' is not displayed or xpath is incorrect in payment information page","",false,true);
		}
	}
	
	public void credit_card_payment_submitButton() throws Exception
	{		

		if(isElementPresent(btnContinuePaymentScreen))
		{			
			click(btnContinuePaymentScreen);
			
			writeTestResults("Verify the 'Continue button' in the payment screen", "'Continue button' should be displayed in payment information page", 
					"'Continue button' is displayed in payment information page","",true,false);
				
			verifyErrorMsgInOrderConfirmPage();
			
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxPayment.isEmpty())
			{
				if(!getAttribute(chkoutAjaxPayment,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxPayment))
					{
						writeTestResults("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", 
								"'Continue button' is not displayed or xpath is incorrect in payment information page","",false,true);
					}
				}
			}			
		}
		else
		{
			writeTestResults("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", 
					"'Continue button' is not displayed or xpath is incorrect in payment information page","",false,true);									
		}
	}	
	
	public void payment_submitButton() throws Exception
	{
		if(isElementPresent(btnContinuePaymentScreen))
		{
			click(btnContinuePaymentScreen);
			
			writeTestResults("Verify the 'Continue button' in the payment screen", "'Continue button' should be displayed in payment information page", 
					"'Continue button' is displayed in payment information page","",true,false);
			
			verifyErrorMsgInOrderConfirmPage();
			
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxPayment.isEmpty())
			{
				if(!getAttribute(chkoutAjaxPayment,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxPayment))
					{
						writeTestResults("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", 
								"'Continue button' is not displayed or xpath is incorrect in payment information page","",false,true);					
					}
				}
			}
			else
			{
				Thread.sleep(5000);
			}
			
			verifyErrorMsgInOrderConfirmPage();					
		}
		else
		{
			writeTestResults("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", 
					"'Continue button' is not displayed or xpath is incorrect in payment information page","",false,true);									
		}
	}
	
	public void verifyPaypalAccount() throws Exception
	{
		if(!isDisplayed(btnPaypalSubmit))
		{
			i =0;
			while(!isDisplayed(btnPaypalSubmit))
			{
				if(isDisplayed(btnPaypalSubmit))
				{
					break;
				}
				else
				{
					Thread.sleep(1000);
					i++;
				}
				
				if(i ==5)
				{
					break;
				}
			}			
		}
		else
		{
			System.out.println("Paypal login is displayed");
		}			
		
		if(isDisplayed("loadLogin_Id"))
		{
			click("loadLogin_Id");
		}
		
		/*if((isElementPresent(txtPaypalEmail) && isElementPresent(txtPaypalPassword) && isElementPresent(btnPaypalSubmit)))
		{
			writeTestResults("Verify paypal page", "Paypal detail page should displayed", "Paypal detail page is displayed","",true,false);
		}*/
			
		
		switchToFrame("//iframe[@name='injectedUl']");
		
		if((isDisplayed(".//*[@id='email']") && isElementPresent(txtPaypalPassword) && isElementPresent(btnPaypalSubmit)))
		{
			writeTestResults("Verify paypal page", "Paypal detail page should displayed", "Paypal detail page is displayed","",true,false);
			
			sendKeys(txtPaypalEmail,paypalEmail);
			
			sendKeys(txtPaypalPassword,paypalPassword);
			
			clickAndWait(btnPaypalSubmit);	
			
			paypalValidation();	
			backToDefaultPage();
			
			if(!isDisplayed(btnPaypalContinue))
			{
				i =0;
				while(!isDisplayed(btnPaypalContinue))
				{
					if(isDisplayed(btnPaypalContinue))
					{
						break;
					}
					else
					{
						Thread.sleep(1000);
						i++;
					}
					
					if(i ==5)
					{
						break;
					}
				}				
			}
			else				
			{
				System.out.println("User logged to the system");
			}
			
			
			
			
			
			if(isElementPresent(btnPaypalContinue))
			{
				clickAndWait(btnPaypalContinue);
				
				writeTestResults("Verify paypal login", "User can navigate to paypal review page", "User can navigate to paypal review page","",true,false);	
			}
			else
			{
				writeTestResults("Verify paypal page", "Paypal detail page should displayed", "Paypal detail page is not displayed within timeout period.page url is "+getCurrentUrl(),"",false,true);				
			}
			
			if(isDisplayed("//*[@id='doneInfo']/ul/li/a_Xpath"))
			{
				clickAndWait("//*[@id='doneInfo']/ul/li/a_Xpath");						
			}			
		}
		else
		{
			writeTestResults("Verify paypal login page", "Paypal login page should be displayed", "Paypal login page is not displayed or paypal login fields are not displayed.page url is " +
					""+getCurrentUrl(),"",false,true);			
		}
	}
	
	public void paypalValidation() throws Exception
	{
		if(isDisplayed("//p[@class='group error']_Xpath"))
		{
			writeTestResults("Verify paypal login details", "User can loing to the paypal account", "Following error is displayed "+getText("//p[@class='group error']_Xpath"),"",false,true);
		}		
	}
	
	public void verifyProductPriceInOrderReview() throws Exception
	{	
		System.out.println("Total Price in Product review"+getText(productTotalPriceBeforeShippingInOrderReview));
		
		if(common.getValueWitoutCurrency(getText(productPriceXpathInOrderReview)) != common.getValueWitoutCurrency(common.productPrice))
		{
			writeTestResults("Verify the price of the selected product is displaying in order review", "Expected product price is "+common.productPrice +" in order review", 
					"Actual product price is "+getText(productPriceXpathInOrderReview)+" in order review","42711f02",false,false);
				
		}
		else
		{
			writeTestResults("Verify the price of the selected product is displaying in order review", "Expected product price is "+common.productPrice+" in order review" , 
					"Actual product price is "+getText(productPriceXpathInOrderReview)+" and total price is "+getText(productTotalPriceBeforeShippingInOrderReview)+" in order review","42711f02",true,false);
		}
		System.out.println("Total Price in Product review"+getText(productTotalPriceXpathInOrderReview));
	}
	
	public void verifyProductGrandTotalPriceBeforeShipping() throws Exception
	{	
		totalPriceBeforeShipping = common.getValueWitoutCurrency(getText(productTotalPriceBeforeShippingInOrderReview));
		
		if(totalPriceBeforeShipping != TestCommonMethods.grandTotalInShoppingcart)
		{	
			writeTestResults("Verify the total price of the selected product is displaying in order review", "Expected total product price is "+TestCommonMethods.grandTotalInShoppingcart +" in order review",
					"Actual product total price is "+getText(productTotalPriceBeforeShippingInOrderReview)+" in order review","",false,false);		
		}
		else
		{
			writeTestResults("Verify the total price of the selected product is displaying in order review", "Expected product price is "+TestCommonMethods.grandTotalInShoppingcart+" in order review" , 
					"Actual product total price is "+getText(productTotalPriceBeforeShippingInOrderReview)+" in order review","",true,false);
		}
	}
	
	public void verifyProductNameInOrderReview() throws Exception
	{	
		if(getText(productNameXpathOrderReview).toLowerCase().indexOf(common.productName.toLowerCase()) >=0)
		{
			writeTestResults("Verify the name of the selected product is displaying in order review", "Expected product Name is "+common.productName+" in order review" , 
					"Actual product name is "+getText(productNameXpathOrderReview)+" in order review","42711f02",true,false);
		}
		else
		{
			writeTestResults("Verify the name of the selected product is displaying in order review", "Expected product Name is "+common.productName +" in order review", 
					"Actual product name is "+getText(productNameXpathOrderReview)+" in order review","42711f02",false,false);
			
		}
	}
	
	public void verifyProductPriceInPaypalReview() throws Exception
	{		
		if(!getText(productPriceXpathInPaypalReviewPage).equalsIgnoreCase(common.productPrice))
		{
			writeTestResults("Verify the price of the selected product is displaying in paypal review page", "Expected product price is "+common.productPrice +" in paypal review page", 
					"Actual product price is "+getText(productPriceXpathInPaypalReviewPage)+" in paypal review page","",false,false);		
		}
		else
		{
			writeTestResults("Verify the price of the selected product is displaying in paypal review page", "Expected product price is "+common.productPrice+" in paypal review page" , 
					"Actual product price is "+getText(productPriceXpathInPaypalReviewPage)+" in paypal review page","",true,false);		
		}
	}
	
	public void verifyProductNameInPaypalReview() throws Exception
	{		
		if(!getText(productNameXpathPaypalReviewPage).equalsIgnoreCase(common.productName))
		{
			writeTestResults("Verify the name of the selected product is displaying in paypal review page", "Expected product Name is "+common.productName +" in paypal review page", 
					"Actual product name is "+getText(productNameXpathPaypalReviewPage)+" in paypal review page","",false,false);
		}
		else
		{
			writeTestResults("Verify the name of the selected product is displaying in paypal review page", "Expected product Name is "+common.productName+" in paypal review page" , 
					"Actual product name is "+getText(productNameXpathPaypalReviewPage)+" in paypal review page","",true,false);					
		}
	}
	
	public void continuePaypalReview() throws Exception
	{
		if(!isElementPresent(btnsubmitPaypalReviewPage))
		{
			writeTestResults("Verify 'Complete Order' button is displaying in paypal review page", "'Complete Order' button should displayed in paypal review page", 
					"'Complete Order' button is not displayed in paypal review page or button locator is incorrect","",false,true);
		}
		else
		{			
			clickAndWait(btnsubmitPaypalReviewPage);
			
			writeTestResults("Verify 'Complete Order' button is displaying in paypal review page", "'Complete Order' button should displayed in paypal review page", 
					"'Complete Order' button is displayed in paypal review page","",true,false);
		}
	}
	
	public void billingAddressInReviewPage() throws Exception
	{
		if(!isElementPresent(billingAddressInReviewPage))
		{
			writeTestResults("Verify the billing address is displaying in order review page", "Billing address should displayed in order review page", 
					"Billing address is not displayed  or billing address locator is incorrect in order review page","",false,false);
		}
		else
		{
			getBillingAddressInReviewPage = getText(billingAddressInReviewPage);			
		}				
	}
		
	
	public void shippingAddressInReviewPage() throws Exception
	{
		getShippingAddressInReviewPage = getText(shippingAddressInReviewPage);		
	}
	
	
	public void continueOrderReview() throws Exception
	{
		if(!isElementPresent(orderReviewContinueButton))
		{
			writeTestResults("Verify 'Complete Order' button is displaying in order review page", "'Complete Order' button should displayed in order review page", 
					"'Complete Order' button is not displayed in order review page or button locator is incorrect","",false,true);			
		}
		else
		{
			click(orderReviewContinueButton);				
		}
		
		if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxorderReview.isEmpty())
		{
			if(!getAttribute(chkoutAjaxorderReview,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
			{
				if(!checkoutAjaxRequest(chkoutAjaxorderReview))
				{
					writeTestResults("Verify the 'Continue button' field displayed in order review information page", "'Continue button' should be displayed in payment information page", 
							"'Continue button' is not displayed or xpath is incorrect in order review information page","",false,true);				
				}
			}
		}
		else
		{
			Thread.sleep(5000);
		}
	}
	
	public void verrifyAleartOrErrorMsgInOrderConfirmPage() throws Exception
	{
		String alert = "";		
		
		if(isAlertPresent())
		{			
			alert =getAlert();
			
			writeTestResults("Verify 'Order confirmation' Page", "'Order Confirmation' page should be displayed", "Following error found :"+alert,"",false,true);
		}		
		if(isErrorDisplayed(errorMsg))
		{
			for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
			{
				if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
				{	
					writeTestResults("Verify 'Order confirmation' Page", "'Order Confirmation' page should be displayed", "Following error found :"+getText(errorMsg),"",false,true);
				}
			}
		}		
	}

	public void verifyErrorMsgInOrderConfirmPage() throws Exception
	{
		String alert = "";		
		
		if(isAlertPresent())
		{			
			alert =getAlert();
			
			writeTestResults("Verify 'Order confirmation' Page", "'Order Confirmation' page should be displayed", "Following error found :"+alert,"",false,true);
		}
		
		if(isErrorDisplayed(errorMsg))
		{
			for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
			{
				if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
				{	
					writeTestResults("Verify 'Order confirmation' Page", "'Order Confirmation' page should be displayed", "Following error found :"+getText(errorMsg),"",false,true);
				}
			}
		}	
	}

	public void creditCardError() throws Exception
	{			
		 if(!(isDisplayed(orderReviewContinueButton) || getPageSource(orderIdMsgInConfirmationPage)))
		 {
			 if(isDisplayed(ccInvalidCardNumberXpath))
			 {
				writeTestResults("User can navigate to review or confirmation page", "'Order Confirmation or Review' page should be displayed", 
						"Following error found :"+getText(ccInvalidCardNumberXpath),"",false,true);
			}
			else if(isDisplayed(ccInvalidCvcXpath))
			{
				writeTestResults("User can navigate to review or confirmation page", "'Order Confirmation or Review' page should be displayed", "Following error found :"+getText(ccInvalidCvcXpath),"",false,true);
			}	
			else if(isDisplayed(ccEmptyNameXpath))
			{
				writeTestResults("User can navigate to review or confirmation page", "'Order Confirmation or Review' page should be displayed", 
						"Following error found :"+getText(ccEmptyNameXpath),"",false,true);
			}
			else if(isDisplayed(ccEmptyCardTypeXpath))
			{
				writeTestResults("User can navigate to review or confirmation page", "'Order Confirmation or Review' page should be displayed", 
						"Following error found :"+getText(ccEmptyCardTypeXpath),"",false,true);
			}
			else if(isDisplayed(ccEmptyCardNumberXpath))
			{
				writeTestResults("User can navigate to review or confirmation page", "'Order Confirmation or Review' page should be displayed", 
						"Following error found :"+getText(ccEmptyCardNumberXpath),"",false,true);
			}
			else if(isDisplayed(ccEmptyExpMonthXpath))
			{
				writeTestResults("User can navigate to review or confirmation page", "'Order Confirmation or Review' page should be displayed", 
						"Following error found :"+getText(ccEmptyExpMonthXpath),"",false,true);
			}
			else if(isDisplayed(ccEmptyCvcXpath))
			{
				writeTestResults("User can navigate to review or confirmation page", "'Order Confirmation or Review' page should be displayed", "Following error found :"+getText(ccEmptyCvcXpath),"",false,true);
			}
			else if(isDisplayed(ccEmptyExpYearXpath))
			{
				writeTestResults("User can navigate to review or confirmation page", "'Order Confirmation or Review' page should be displayed", "Following error found :"+getText(ccEmptyExpYearXpath),"",false,true);
			}
		 }				
	}	
	
	public void verifyErrorMsgIBillingInfor() throws Exception
	{
		String alert = "";
	
		if(isAlertPresent())
		{
			alert =getAlert();
		
			acceptAlert();
			
			writeTestResults("Verify 'Shipping infor' Page", "'Shipping infor' page should be displayed", "Following error found :"+alert,"",false,true);
		}
		
		/*if(isDisplayed(errorMsg))
		{
			for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
			{
				if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
				{	
					writeTestResults("Verify 'Shipping infor' Page", "'Shipping infor' page should be displayed", "Following error found :"+getText(errorMsg),"",false,true);
				}
			}
		}*/			
	}
	
	public void verifyErrorMsgIShippingInfor() throws Exception
	{
		String alert = "";
		
		if(isAlertPresent())
		{
			alert =getAlert();
			
			writeTestResults("Verify 'Shipping infor' Page", "'Shipping infor' page should be displayed", "Following error found :"+alert,"",false,true);
		}	
		
		/*if(isDisplayed(errorMsg))
		{
			for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
			{
				if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
				{	
					writeTestResults("Verify 'Shipping infor' Page", "'Shipping infor' page should be displayed", "Following error found :"+getText(errorMsg),"",false,true);
				}
			}
		}*/			
	}
	
	public String creditcardError() throws Exception
	{
		String alert = "";
		
		if(isAlertPresent())
		{
			alert =getAlert();
			
			acceptAlert();		
		}
		return alert;					
	}
	
	public void paymentMethodIsMoneyOrder() throws Exception
	{
		if(isDisplayed(chkPaymentIsMoneyOrder))
		{
			click(chkPaymentIsMoneyOrder);
			
			common.magentoProfilerResults(writeFileName+"Payment method page");
			
			writeTestResults("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", 
					"User can selct payment type is money order in payment information page","",true,false);
		}
		else
		{
			if(!isDisplayed(btnContinuePaymentScreen))
			{
				writeTestResults("Verify user can navigate to payment information page", "Payment informantion should be displayed", "Payment infomation page is not displayed","",false,true);			
			}
			else
			{
				writeTestResults("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", 
						"Money order option is not displayed or money order xpath is incorrect in payment information page","",false,true);
			}			
		}
	}
	
	public void moneyOrderSubmitButton() throws Exception
	{
		System.out.println("moneyOrderContinueButton"+btnContinuePaymentScreen);
		
		if(!isDisplayed(btnContinuePaymentScreen))
		{
			writeTestResults("Verify 'Money Order' button is displaying in Payment information", "'Money Order' button should displayed in payment information page", 
					"'Money Order' button is not displayed payment information page or button locator is incorrect","",false,true);		
		}
		else
		{
			writeTestResults("Verify 'Money Order' button is displaying in Payment information", "'Money Order' button should displayed in payment information page", 
					"'Money Order' button is displayed payment information page","",true,false);
						
			clickAndWait(btnContinuePaymentScreen);				
						
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes") && !chkoutAjaxPayment.isEmpty())
			{
				if(!getAttribute(chkoutAjaxPayment,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxPayment))
					{
						writeTestResults("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page",
								"'Continue button' is not displayed or xpath is incorrect in payment information page","",false,true);
					}
				}
			}
			else
			{
				Thread.sleep(5000);
			}				
		}
	}
	
	public void orderIdInConfirmationPage() throws Exception
	{
		if(!getPageSource(orderIdMsgInConfirmationPage))
		{
			if(getText("/html/body_xpath").indexOf("Fatal error")>-1)
			{
				writeTestResults("Verify Thank You message in confirmation page", "Confirmation page should displayed", 
						"Following error is displayed. "+getText("/html/body_xpath"),"42713014",false,true);				
			}
			if(getPageSource("An error occurred in the process of payment"))
			{
				writeTestResults("Verify Thank You message in confirmation page", "Confirmation page should displayed", 
						"Following error is displayed. 'An error occurred in the process of payment'","42713014",false,true);				
			}
			
			if(!getPageSource(orderIdMsgInConfirmationPage))
			{
				for(int i=0;i<10;i++)
				{
					Thread.sleep(200);				
				}				
			}
			if(!getPageSource(orderIdMsgInConfirmationPage))
			{
				writeTestResults("Verify Thank You message in confirmation page", orderIdMsgInConfirmationPage+"' message should displayed in confirmation page",
						orderIdMsgInConfirmationPage+" message is not displayed in confirmation page","42713014",false,true);				
			}
		}			
		else
		{
			common.magentoProfilerResults(writeFileName+"Order confirmation page");
			
			if(!(isElementPresent(orderIdXpath)||isElementPresent(loggedUserOrderIdXpath)))
			{
				writeTestResults("Verify Thank You message in confirmation page", "Confirmation page should displayed", "Thank you message is not displayed or order id locator is incorrect","42713014",false,true);			
			}			
			else
			{
				
				String orderText = "";		 
				orderText = getText(orderIdXpath).trim();
				
				System.out.println("orderText"+orderText);
				orderNumber = orderText.replaceAll(orderIdMsgInConfirmationPage, "").trim();
				
				
				
				System.out.println("orderNumber"+orderNumber);			
								
				if(orderNumber.indexOf(".")>-1)
				{
					orderNumber = orderNumber.replace(".", "");
				}
				
				writeTestResults("Verify Thank You message in confirmation page", "'Thank you for your purchase' message should displayed in confirmation page", 
						"'Thank you for your purchase' message is displayed in confirmation page and Your Order Id is "+orderNumber,"42713014",true,false);
			}			
		}		
	}
	
	public void continueShopping() throws Exception
	{
		if(isElementPresent(continueShoppingButton))
		{
			writeTestResults("Verify the Continue Shopping button is displayed in the confirmation page", "Continue Shopping button should be displayed in the confirmation page", 
					"Continue Shopping button is displayed in the confirmation page","42713abe",true,false);
			
			clickAndWait(continueShoppingButton);
			
			if(common.siteUrl.equalsIgnoreCase(getCurrentUrl()))
			{
				writeTestResults("Verify confirmation page is redirect to the home page when the user clicks 'Continue Shopping Button'", "Home page should be displayed", 
						"Home page is displayed","42713abe",true,false);
			}
			else
			{
				writeTestResults("Verify confirmation page is redirect to the home page when the user clicks 'Continue Shopping Button'", "Expected home page title is "+homeTestData.siteTitle, 
						"Actual page title is "+getTitle(),"42713abe",false,true);				
			}
		}
		else
		{
			writeTestResults("Verify the Continue Shopping button is displayed in the confirmation page", "Continue Shopping button should be displayed in the confirmation page", 
					"Continue Shopping button is not displayed in the confirmation page","42713abe",false,false);
		}
	}
	
	public void ConfirmationMaiReader() throws Exception 
	{				
		if(orderNumber.isEmpty()||orderNumber.equalsIgnoreCase(""))
		{
			writeTestResults("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation Id is NULL","",false,false);
		}
		else
		{			
			String customerName = billingFirstName+" "+billingLastName;
			
			Thread.sleep(3000);
			
			mailRead(orderNumber,customerName,mailReaderUserName,"amith344632");
			
			String textMail =bodyText;
			
			if(textMail.toLowerCase().indexOf(orderNumber.toLowerCase())>-1 || textMail.toLowerCase().indexOf(customerName.toLowerCase())>-1)
			{
				writeTestResults("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", 
						"Confirmation mail is Received","42712d3a",true,false);
			}
			
			else
			{
				writeTestResults("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", 
						"Confirmation mail is not Received yet","42712d3a",false,false);					
			}
		}		
	}
	
	public void memberConfirmationMaiReader() throws Exception {
		
		System.out.println("orderNumber"+orderNumber);
		
		if(orderNumber.isEmpty()||orderNumber.equalsIgnoreCase(""))
		{
			writeTestResults("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation Id is NULL","",false,false);
		}
		else
		{
			String customerName = billingFirstName+" "+billingLastName;
			
			Thread.sleep(3000);
			
			mailRead(orderNumber,customerName,myAccountTestData.loginEmailAddress,"19881106");
			String textMail =bodyText;
						
			if(textMail.toLowerCase().indexOf(orderNumber.toLowerCase())>-1 || textMail.toLowerCase().indexOf(customerName.toLowerCase())>-1)
			{
				writeTestResults("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation mail is Received","",true,false);
			}			
			else
			{
				writeTestResults("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation mail is not Received yet","",false,false);
			}
		}	
	}
	
	public void verifyUserRegisterConfirmationMail() throws Exception
	{		
		String customerName = billingFirstName+" "+billingLastName;
			
		Thread.sleep(2000);
			
		mailRead(userCreationTestData.userCreationMailSubject,customerName,billingJoinUserEmail,"19881106");
			
		String textMail = bodyText;
			
		if(textMail.indexOf(userCreationTestData.userCreationMailSubject)>-1)
		{
			writeTestResults("Verify  the Email of the User registration", "Registration mail should Received to gmail account", "Registration mail is Received","",true,false);
		}			
		else
		{
			writeTestResults("Verify  the Email of the User registration", "Registration mail should Received to gmail account", "Registration mail is not Received or subject is incorrect","",false,false);
		}	
	}		
	
	
	
	public void billingInforEmptyValidation() throws Exception
	{
		isBillingInfoValidation = false; 
		
		
		
		
		if(!validationBillingFirstName.isEmpty())
		{	
			if(isDisplayed(validationBillingFirstName))
			{
				if(getText(validationBillingFirstName).equalsIgnoreCase(validationMsgBillingFirstName))
				{
					writeTestResults("Verify user clicks submit button without enter first name in billing information page", validationMsgBillingFirstName+" message should be displayed",
							validationMsgBillingFirstName+" message is displayed","4270d484",true,false);							
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter first name in billing information page", validationMsgBillingFirstName+" message should be displayed",
							getText(validationBillingFirstName)+" message is displayed" ,"4270d484",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter first name in billing information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270d484",false,true);			
			}
		}
		if(!validationBillingLastName.isEmpty())
		{
			if(isDisplayed(validationBillingLastName))
			{
				if(getText(validationBillingLastName).equalsIgnoreCase(validationMsgBillingLastName))
				{
					writeTestResults("Verify user clicks submit button without enter last name in billing information page", validationMsgBillingLastName+" message should be displayed",
							validationMsgBillingLastName+" message is displayed","4270d484",true,false);							
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter last name in billing information page", validationMsgBillingLastName+" message should be displayed",
							getText(validationBillingLastName)+" message is displayed","4270d484",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter last name in billing information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270d484",false,true);			
			}		
		}
		if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
		{
			if(!isLoggedUser)
			{
				if(isDisplayed(validationBillingEmail))
				{
					if(getText(validationBillingEmail).equalsIgnoreCase(validationMsgBillingEmail))
					{
						writeTestResults("Verify user clicks submit button without enter email in billing information page", validationMsgBillingEmail+" message should be displayed",
								validationMsgBillingEmail+" message is displayed","4270d484",true,false);
					}
					else
					{
						writeTestResults("Verify user clicks submit button without enter email in billing information page", validationMsgBillingEmail+" message should be displayed",
								getText(validationBillingEmail)+" message is displayed","4270d484",true,false);
					}
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter email in billing information page", "Error message should be displayed", 
							"Error message is not displayed or xpath is incorrect","4270d484",false,true);				
				}		
			}				
		}
		if(!validationBillingTelephone.isEmpty())
		{
			if(isDisplayed(validationBillingTelephone))
			{
				if(getText(validationBillingTelephone).equalsIgnoreCase(validationMsgBillingTelephone))
				{
					writeTestResults("Verify user clicks submit button without enter telephone in billing information page", validationMsgBillingTelephone+" message should be displayed",
							validationMsgBillingTelephone+" message is displayed","4270d484",true,false);							
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter telephone in billing information page", validationMsgBillingTelephone+" message should be displayed",
							getText(validationBillingTelephone)+" message is displayed","4270d484",true,false);								
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter telephone in billing information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270d484",false,true);
			}						
		}
		if(!validationBillingPostcode.isEmpty())
		{
			if(isDisplayed(validationBillingPostcode))
			{
				if(getText(validationBillingPostcode).equalsIgnoreCase(validationMsgBillingPostcode))
				{
					writeTestResults("Verify user clicks submit button without enter postcode in billing information page", validationMsgBillingPostcode+" message should be displayed",
							validationMsgBillingPostcode+" message is displayed","4270d484",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter postcode in billing information page", validationMsgBillingPostcode+" message should be displayed",
							getText(validationBillingPostcode)+" message is displayed","4270d484",true,false);								
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter postcode in billing information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270d484",false,true);
			}					
		}
		if(!validationBillingCountry_Id.isEmpty())
		{
			if(isDisplayed(validationBillingCountry_Id))
			{
				if(getText(validationBillingCountry_Id).equalsIgnoreCase(validationMsgBillingCountry_Id))
				{
					writeTestResults("Verify user clicks submit button without enter country in billing information page", validationMsgBillingCountry_Id+" message should be displayed",
							validationMsgBillingCountry_Id+" message is displayed","4270d484",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter country in billing information page", validationMsgBillingCountry_Id+" message should be displayed",
							getText(validationBillingCountry_Id)+" message is displayed","4270d484",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter country in billing information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270d484",false,true);
			}					
		}			
	}
	
	//--------------------------------------------------------------------------------------------------------------------
	public void DeliveryInforEmptyValidation() throws Exception
	{
		
		
		isDeliveryInfoValidation = false; 
		
		if(!validationDeliveryFirstName.isEmpty())
		{	
			if(isDisplayed(validationDeliveryFirstName))
			{
				if(getText(validationDeliveryFirstName).equalsIgnoreCase(validationMsgDeliveryFirstName))
				{
					writeTestResults("Verify user clicks submit button without enter first name in delivery information page", validationMsgDeliveryFirstName+" message should be displayed",
							validationMsgDeliveryFirstName+" message is displayed","4270d484",true,false);							
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter first name in delivery information page", validationMsgDeliveryFirstName+" message should be displayed",
							getText(validationDeliveryFirstName)+" message is displayed" ,"4270d484",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter first name in delivery information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270d484",false,true);			
			}
		}
		if(!validationDeliveryLastName.isEmpty())
		{
			if(isDisplayed(validationDeliveryLastName))
			{
				if(getText(validationDeliveryLastName).equalsIgnoreCase(validationMsgDeliveryLastName))
				{
					writeTestResults("Verify user clicks submit button without enter last name in delivery information page", validationMsgDeliveryLastName+" message should be displayed",
							validationMsgDeliveryLastName+" message is displayed","4270d484",true,false);							
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter last name in delivery information page", validationMsgDeliveryLastName+" message should be displayed",
							getText(validationDeliveryLastName)+" message is displayed","4270d484",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter last name in delivery information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270d484",false,true);			
			}		
		}
		
		if(!validationDeliveryTelephone.isEmpty())
		{
			if(isDisplayed(validationDeliveryTelephone))
			{
				if(getText(validationDeliveryTelephone).equalsIgnoreCase(validationMsgDeliveryTelephone))
				{
					writeTestResults("Verify user clicks submit button without enter telephone in billing information page", validationMsgDeliveryTelephone+" message should be displayed",
							validationMsgDeliveryTelephone+" message is displayed","4270d484",true,false);							
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter telephone in billing information page", validationMsgBillingTelephone+" message should be displayed",
							getText(validationDeliveryTelephone)+" message is displayed","4270d484",true,false);								
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter telephone in delivery information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270d484",false,true);
			}						
		}
//		if(!validationDeliveryCity.isEmpty())
//		{
//			if(isDisplayed(validationDeliveryCity))
//			{
//				if(getText(validationDeliveryCity).equalsIgnoreCase(validationMsgDeliveryCity))
//				{
//					writeTestResults("Verify user clicks submit button without enter suburb in delivery information page", validationMsgDeliveryCity+" message should be displayed",
//							validationMsgDeliveryCity+" message is displayed","4270d484",true,false);
//				}
//				else
//				{
//					writeTestResults("Verify user clicks submit button without enter suburb in delivery information page", validationMsgDeliveryCity+" message should be displayed",
//							getText(validationDeliveryCity)+" message is displayed","4270d484",true,false);								
//				}
//			}
//			else
//			{
//				writeTestResults("Verify user clicks submit button without enter suburb in delivery information page", "Error message should be displayed", 
//						"Error message is not displayed or xpath is incorrect","4270d484",false,true);
//			}					
//		}
		if(!validationDeliveryAddress1.isEmpty())
		{
			if(isDisplayed(validationDeliveryAddress1))
			{
				if(getText(validationDeliveryAddress1).equalsIgnoreCase(validationMsgDeliveryAddress1))
				{
					writeTestResults("Verify user clicks submit button without enter country in billing information page", validationMsgDeliveryAddress1+" message should be displayed",
							validationMsgDeliveryAddress1+" message is displayed","4270d484",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter country in billing information page", validationMsgDeliveryAddress1+" message should be displayed",
							getText(validationDeliveryAddress1)+" message is displayed","4270d484",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter country in billing information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270d484",false,true);
			}					
		}			
	}
	
	
	
	public void billingInforSubmitValidation() throws Exception
	{
		if(!shippingToDifferentAddress)
		{				
			if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
			{
				if(!isDisplayed(btnShippingMethodContinue))
				{
					validationMsgInBillingInfor();						
				}				
			}
			else
			{
				if(!isDisplayed(btnContinuePaymentScreen))
				{
					validationMsgInBillingInfor();				
				}
			}			
		}
		else
		{
			if(!isDisplayed(btnShippingInfoContinue))
			{
				validationMsgInBillingInfor();
			}
		}					
	}
	
	private void validationMsgInBillingInfor() throws Exception
	{
		if(!getValidationText(validationBillingFirstName).isEmpty())
		{                     
			if(isDisplayed(validationBillingFirstName))
			{
				if(getText(validationBillingFirstName).equalsIgnoreCase(validationMsgBillingFirstName))
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"First name text box value is empty in billing information page.","",false,true);					
				}
				else
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"First name text box value is empty and message is "+getText(validationBillingFirstName),"",false,true);
				}
			}
		}
		if(!validationBillingLastName.isEmpty())
		{
			if(isDisplayed(validationBillingLastName))
			{
				if(getText(validationBillingLastName).equalsIgnoreCase(validationMsgBillingLastName))
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"Last name text box value is empty in billing information page.","",false,true);					
				}
				else
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"Last name text box value is empty and message is "+getText(validationBillingLastName),"",false,true);
				}
			}
		}
		if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
		{
			if(!validationBillingEmail.isEmpty())
			{
				if(isDisplayed(validationBillingLastName))
				{
					if(getText(validationBillingLastName).equalsIgnoreCase(validationMsgBillingEmail))
					{
						writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
								"Email text box value is empty in billing information page.","",false,true);						
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
								"Email text box value is empty and message is "+getText(validationBillingLastName),"",false,true);
					}
				}
			}
		}
		if(!validationBillingTelephone.isEmpty())
		{
			if(isDisplayed(validationBillingTelephone))
			{
				if(getText(validationBillingTelephone).equalsIgnoreCase(validationMsgBillingTelephone))
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"Email text box value is empty in billing information page.","",false,true);
				}
				else
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"First name text box value is empty and message is "+getText(validationBillingTelephone),"",false,true);
				}
			}
		}
		if(!validationBillingPostcode.isEmpty())
		{
			if(isDisplayed(validationBillingPostcode))
			{
				if(getText(validationBillingPostcode).equalsIgnoreCase(validationMsgBillingPostcode))
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"Postcode text box value is empty in billing information page.","",false,true);
				}
				else
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"Postcode text box value is empty and message is "+getText(validationBillingPostcode),"",false,true);
				}
			}
			
		}
		if(!validationBillingCountry_Id.isEmpty())
		{
			if(isDisplayed(validationBillingCountry_Id))
			{
				if(getText(validationBillingCountry_Id).equalsIgnoreCase(validationMsgBillingCountry_Id))
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"Country value is empty in billing information page.","",false,true);
				}
				else
				{
					writeTestResults("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", 
							"Country value is empty and message is "+getText(validationMsgBillingCountry_Id),"",false,true);
				}
			}
		}		
		
		if(!msgBillingWithoutPw.isEmpty())
		{                     
			if(isDisplayed(msgBillingWithoutPw))
			{
				writeTestResults("Verify user clicks submit button with correct data in billing information page", "Shipping infor page should displayed", 
						"Password text box value is empty and following message is displayed in billing information page."+getText(msgBillingWithoutPw),"",false,true);
			}
		}
		
		if(!msgBillingWithoutConfirmPw.isEmpty())
		{                     
			if(isDisplayed(msgBillingWithoutConfirmPw))
			{
				writeTestResults("Verify user clicks submit button with correct data in billing information page", "Shipping infor page should displayed", 
						"Confirm password text box value is empty and following message is displayed in billing information page."+getText(msgBillingWithoutConfirmPw),"",false,true);
			}
		}
	}
	
	public void submitShippingValidation() throws Exception
	{
		if(!(isDisplayed(btnShippingMethodContinue) || isDisplayed(btnContinuePaymentScreen)))
		{
			if(isDisplayed(validationShippingFirstName))
			{
				if(getText(validationShippingFirstName).equalsIgnoreCase(validationMsgShippingFirstName))
				{
					writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
							"First name text box value is empty in shipping information page.","",false,true);					
				}
				else
				{
					writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
							"First name text box value is empty and message is "+getText(validationShippingFirstName),"",false,true);
				}
			}
			if(!validationShippingLastName.isEmpty())
			{
				if(isDisplayed(validationShippingLastName))
				{
					if(getText(validationShippingLastName).equalsIgnoreCase(validationMsgShippingLastName))
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
								"Last name text box value is empty in shipping information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
								"Last name text box value is empty and message is "+getText(validationShippingLastName),"",false,true);
					}
				}
			}
			
			if(!validationShippingTelephone.isEmpty())
			{
				if(isDisplayed(validationShippingTelephone))
				{
					if(getText(validationShippingTelephone).equalsIgnoreCase(validationMsgShippingTelephone))
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
								"Telephone text box value is empty or telephone not valid in shipping information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
								"Telephone text box value is empty or telephone not valid and message is "+getText(validationShippingTelephone),"",false,true);
					}
				}
			}
			if(!validationShippingPostcode.isEmpty())
			{
				if(isDisplayed(validationShippingPostcode))
				{
					if(getText(validationShippingPostcode).equalsIgnoreCase(validationMsgShippingPostcode))
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
								"Post code text box value is empty in shipping information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
								"Post code text box value is empty and message is "+getText(validationShippingPostcode),"",false,true);
					}
				}
				
			}
			if(!validationShippingCountry_Id.isEmpty())
			{
				if(isDisplayed(validationShippingCountry_Id))
				{
					if(getText(validationShippingCountry_Id).equalsIgnoreCase(validationMsgShippingCountry_Id))
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
								"Country value is empty in shipping information page.","",false,true);						
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
								"Country value is empty and message is "+getText(validationMsgShippingCountry_Id),"",false,true);
					}
				}
			}
			
			if(!validationShippingRegion.isEmpty())
			{
				if(isDisplayed(validationShippingRegion))
				{
					if(getText(validationShippingRegion).equalsIgnoreCase(validationMsgShippingRegion))
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", 
								"State value is empty in shipping information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed",
								"State value is empty and message is "+getText(validationShippingCountry_Id),"",false,true);
					}
				}					
			}
		}
	}
	
	public void shippingInforEmptyValidation() throws Exception
	{
		isShippingInfoValidation = false;
		
		if(isDisplayed(validationShippingFirstName))
		{
			if(getText(validationShippingFirstName).equalsIgnoreCase(validationMsgShippingFirstName))
			{
				writeTestResults("Verify user clicks submit button without enter first name in shipping information page", validationMsgShippingFirstName+" message should be displayed",
						validationMsgShippingFirstName+" message is displayed","4270deca",true,false);
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter first name in shipping information page", validationMsgShippingFirstName+" message should be displayed",
						getText(validationShippingFirstName)+" message is displayed","4270deca",true,false);
			}
		}
		else
		{
			writeTestResults("Verify user clicks submit button without enter first name in shipping information page", "Error message should be displayed", 
					"Error message is not displayed or xpath is incorrect","4270deca",false,true);
		}
		
		if(!validationShippingLastName.isEmpty())
		{
			if(isDisplayed(validationShippingLastName))
			{
				if(getText(validationShippingLastName).equalsIgnoreCase(validationMsgShippingLastName))
				{
					writeTestResults("Verify user clicks submit button without enter last name in shipping information page", validationMsgShippingLastName+" message should be displayed",
							validationMsgShippingLastName+" message is displayed","4270deca",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter last name in shipping information page", validationMsgShippingLastName+" message should be displayed",
							getText(validationShippingLastName)+" message is displayed","4270deca",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter last name in shipping information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270deca",false,true);
			}
		}
		
		if(!validationShippingTelephone.isEmpty())
		{
			if(isDisplayed(validationShippingTelephone))
			{
				if(getText(validationShippingTelephone).equalsIgnoreCase(validationMsgShippingTelephone))
				{
					writeTestResults("Verify user clicks submit button without enter phone number in shipping information page", validationMsgShippingTelephone+" message should be displayed",
							validationMsgShippingTelephone+" message is displayed","4270deca",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter phone number in shipping information page", validationMsgShippingTelephone+" message should be displayed",
							getText(validationShippingTelephone)+" message is displayed","4270deca",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter phone number in shipping information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270deca",false,true);
			}
		}
		if(!validationShippingPostcode.isEmpty())
		{
			if(isDisplayed(validationShippingPostcode))
			{
				if(getText(validationShippingPostcode).equalsIgnoreCase(validationMsgShippingPostcode))
				{
					writeTestResults("Verify user clicks submit button without enter postcode in shipping information page", validationMsgShippingPostcode+" message should be displayed",
							validationMsgShippingPostcode+" message is displayed","4270deca",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter postcode in shipping information page", validationMsgShippingPostcode+" message should be displayed",
							getText(validationShippingPostcode)+" message is displayed","4270deca",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter postcode in shipping information page", "Error message should be displayed", "" +
						"Error message is not displayed or xpath is incorrect","4270deca",false,true);
			}			
		}
		if(!validationShippingCountry_Id.isEmpty())
		{
			if(isDisplayed(validationShippingCountry_Id))
			{
				if(getText(validationShippingCountry_Id).equalsIgnoreCase(validationMsgShippingCountry_Id))
				{
					writeTestResults("Verify user clicks submit button without enter country in shipping information page", validationMsgShippingCountry_Id+" message should be displayed",
							validationMsgShippingCountry_Id+" message is displayed","4270deca",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button without enter country in shipping information page", validationMsgShippingCountry_Id+" message should be displayed",
							getText(validationShippingCountry_Id)+" message is displayed","4270deca",true,false);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button without enter country in shipping information page", "Error message should be displayed", 
						"Error message is not displayed or xpath is incorrect","4270deca",false,true);
			}
		}		
	}	
	
	public void verifyOrderDisplayedInMyAccount() throws Exception
	{
		common.navigateToHomePage();		
		
		common.accountLinksInUnderMyAcc();
		
		if(isElementPresent(myAccountLink))
		{
			clickAndWait(myAccountLink);
		}
		else 
		{
			openPage(common.siteUrl+myAccountUrl);
		}	
		
		if(getPageSource("Dashboard"))
		{
			writeTestResults("Verify my account page","User can navigate to my account page", "User Can navigate to my account page","",true,false);
			
			
		}
		else
		{
			writeTestResults("Verify Order displayed in my account section", "Order "+orderNumber +" should be displayed in my account", "My Account page is not displayed","",false,true);
		}		
	}
	
	public boolean ajaxCall(String loader) throws Exception
	{
		int i=0;
		boolean isAjaxLoad = true;
		
		if(isElementPresent(chkoutAjaxLoader))
		{	
			while(isDisplayed(chkoutAjaxLoader))
			 {			   	 
			   	 Thread.sleep(100);
			   	 System.out.println("i"+i);
			   	 i++;
			   	 
			   	 if(i==100)
			   	 {
			   		isAjaxLoad = false; 
			   		break;
			   	 }		   	 
			 }		 
		}
		return isAjaxLoad;   
	}
	
	public boolean checkoutAjaxRequest(String loader) throws Exception
	{
		int i=0;
		boolean isAjaxLoad = true;
		
		if(isElementPresent(loader))
		{	
			 while(!getAttribute(loader,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
			 {	
				 Thread.sleep(100);
			   	 
			   	 i++;
			   	 
			   	 if(!isDisplayed(loader))
			   	 {
			   		 break;
			   	 }
			   	 
			   	 if(i==200)
			   	 {
			   		isAjaxLoad = false; 
			   		break;
			   	 }			   	 
			 }			 
		}		
		return isAjaxLoad;	   
	}
	
	public void verifyOrderInMyOrderSection() throws Exception
	{
		// get order id place in the table
		int oderPlace = navigateToOrderPage();
		
		System.out.println("oderPlace"+oderPlace);
		
		orderStatus(oderPlace);
		
		clickAndWait(orderTable + "/tr["+oderPlace+"]/td[6]/a");		
		
		verifyOrderDetails();
		
		reorder();
		
	}
	
	private void reorder() throws Exception
	{
		String orderHeader = "";
		boolean isPrintBtnDisplayed = false;
		boolean isReOrderBtnDisplayed = false;
		if(isElementPresent(lbOrderViewHeader))
		{
			orderHeader = getText(lbOrderViewHeader);
			
			System.out.println("orderHeader"+orderHeader);
			
			if(isDisplayed(btPrinteOrder))
			{
				isPrintBtnDisplayed = true;
			}
			
			if(isDisplayed(btnReOrder))
			{
				isReOrderBtnDisplayed = true;
			}
			if(orderHeader.indexOf("Payment Review") >= 0)
			{
				if(isPrintBtnDisplayed && !isReOrderBtnDisplayed)
				{
					writeTestResults("Verify the system displays the order Action - Reorder", "Only print option should be displayed.Because order type is Payment Review", 
							"Only print button is displayed.","427147f2",true,false);
				}
				else
				{
					writeTestResults("Verify the system displays the order Action - Reorder","Only print option should be displayed.Because order type is Payment Review", 
							"Both buttons are displayed","427147f2",false,true);
				}
			}
			
			if(orderHeader.indexOf("Payment Review") < 0)
			{
				if(isPrintBtnDisplayed && isReOrderBtnDisplayed)
				{
					writeTestResults("Verify the system displays the order Action - Reorder", "Print/Reorder button should be displayed.", "Print/Reorder button is displayed.","427147f2",true,false);
					
					navigateShoppingcartFromReorder();					
				}
				else
				{
					writeTestResults("Verify the system displays the order Action - Reorder", "Print/Reorder button should be displayed.", "only one button is displayed","427147f2",true,false);
				}
			}			
		}
		else
		{
			writeTestResults("Verify the system displays the order Action - Reorder", "Print/Reorder button should be displayed.", "Reorder button is not displayed or xpath is incorrect","427147f2",false,true);
		}					
	}
	
	private void navigateShoppingcartFromReorder() throws Exception
	{
		if(isElementPresent(btnReOrder))
		{
			clickAndWait(btnReOrder);
			
			if(isElementPresent(proceedToCheckoutButtonInShoppingCart))
			{
				writeTestResults("Verify the system displays the order Action - Reorder", "System should allow user to reorder the same order " +
						"and navigete to shopping cart page", "Shopping cart page is displayed","42714b4e",true,false);
			}
			else
			{
				writeTestResults("Verify the system displays the order Action - Reorder", "System should allow user to reorder the same order " +
						"and navigete to shopping cart page", "Following page is displayed "+getTitle(),"42714b4e",false,true);				
			}
		}
		else
		{
			writeTestResults("Verify the system displays the order Action - Reorder", "System should allow user to reorder the same order " +
					"and navigete to shopping cart page", "Reorder button is not displayed or xpath is incorrect","42714b4e",false,true);
		}		
	}
	
	private void orderStatus(int orderplace) throws Exception
	{
		orderStatus = getText(orderTable + "/tr["+orderplace+"]/td[5]");
		
		if(!orderStatus.isEmpty())
		{
			writeTestResults("Verify the system displays the order status / History ", "Order status should has status in my order section", "Order status is "+orderStatus,
					"4271490a",true,false);
		}
		else
		{
			writeTestResults("Verify the system displays the order status / History ", "Order status should has status in my order section", "Order status is empty",
					"4271490a",false,false);
		}				
	}
	
	private void verifyOrderDetails() throws Exception
	{		
		String productName = getText(orderName);
		String productPrice = getText(orderPrice);
		double productTotalPrice = common.getValueWitoutCurrency(getText(orderTotalPrice));
		
		if((productName.toLowerCase().indexOf(common.productName.toLowerCase()) >= 0) && (productPrice.indexOf(common.productPrice) >= 0)
				 && (productTotalPrice == totalPriceAftereShipping))
		{
			writeTestResults("Verify order details in order view page", "Relavent data should be displayed in order view page", "Relavent data is displayed in order view page",
					"42714a2c",true,false);
		}
		else
		{
			if(productName.toLowerCase().indexOf(common.productName.toLowerCase()) < 0)
			{
				writeTestResults("Verify product name in my account section", common.productName+" should be displayed in my account", productName+" is displayed",
						"",false,false);
			}
			else
			{
				writeTestResults("Verify product name in my account section", common.productName+" should be displayed in my account", productName+" is displayed",
						"",true,false);
			}
			if(productPrice.indexOf(common.productPrice) < 0)
			{
				writeTestResults("Verify product price in my account section", common.productPrice+" should be displayed in my account", productPrice+" is displayed",
						"",false,false);			
			}
			else
			{
				writeTestResults("Verify product price in my account section", common.productPrice+" should be displayed in my account", productPrice+" is displayed",
						"",true,false);
			}
			
			if(productTotalPrice != totalPriceAftereShipping)
			{
				writeTestResults("Verify product total price in my account section", totalPriceAftereShipping+" should be displayed in my account", productTotalPrice+" is displayed",
						"",false,false);
			}
			else
				
			{
				writeTestResults("Verify product total price in my account section", totalPriceAftereShipping+" should be displayed in my account", productTotalPrice+" is displayed",
						"",true,false);
			}
		}				
	}
	
	private int navigateToOrderPage() throws Exception
	{				
		String getOrderNoFromTable = "";
		int itemFound = 1;
		
		if(isElementPresent(myOrderLink))
		{
			clickAndWait(myOrderLink);
			
			writeTestResults("Verify user can navigate to order review page", "My order page should be displayed in my account", " My order page is displayed",
					"427146d0",true,false);
		}
		else
		{
			writeTestResults("Verify user can navigate to order review page", "My order page should be displayed in my account", " My order page is not displayed",
					"427146d0",false,true);
		}
		
		int rowCount = getRowCount(orderTable,"tr");
		
		while(itemFound < rowCount)
		{
			if(itemFound == 1)
			{
				getOrderNoFromTable = getText(orderTable+"/tr/td");
				
				if(orderNumber.equalsIgnoreCase(getOrderNoFromTable))
				{
					break;
				}				
			}
			
			if(itemFound < (rowCount-1) && itemFound != 1 )
			{
				getOrderNoFromTable = getText(orderTable+"/tr["+itemFound+"]/td");
				
				if(orderNumber.equalsIgnoreCase(getOrderNoFromTable))
				{
					break;
				}
			}
			
			if(itemFound == rowCount)
			{
				writeTestResults("Verify Order displayed in my order section", "Order "+orderNumber +" should be displayed in my account", "order is not displayed in my order section","",false,true);
			}
			
			itemFound++;			
		}
		return itemFound;	
	}	
		
	public void totalTime(java.util.Date startTime) throws Exception
	{		
		java.util.Date endTime;		
		Calendar cal = Calendar.getInstance(); 		
		endTime = cal.getTime();
		String totalTime = common.totalTime(startTime, endTime);
		writeTestResults("Total time to execute "+writeFileName, "Total time =",totalTime,"",true,false);
	}		
}

					
	
	