package com.net.test.pageObject;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.net.test.data.MyAccountTestData;
import com.net.test.data.UserCreationTestData;
import com.net.test.util.DBConnectionUtill;
import com.net.test.util.TestCommonMethods;

public class UserCreation extends UserCreationTestData
{
	DBConnectionUtill db = new DBConnectionUtill();
	TestCommonMethods common = new TestCommonMethods();	
	MyAccount account = new MyAccount();	
	MyAccountTestData myAccountTestData = new MyAccountTestData();
		
	public void resultSheetName(String resultSheet)throws Exception
	{
		writeFileName=resultSheet;		
	}
	
	public void loadHomePage()throws Exception
	{
		common.navigateToHomePage();
	}
	
	public void navigateToUserRegisterPage()throws Exception
	{			
		String beforeClickUrl = null;
		
		//common.accountLinksInUnderMyAcc();
		
		if(isExecuted(getMethodIsExecuted,"Login and create account in same page"))
	
		{
			if(!common.navigateToLoginPage())
			{
				 writeTestResults("Verify login page","User can navigate to login page", "User Can't navigate to login page.login link is not displayed or link path ("+loginLink+") is incorrect","426f3926",false,false);
			}			
		}
		
		if(!isElementPresent(registerLink))				
		{
			 writeTestResults("Verify the User Register link", "Register link should be displayed in the home page" , "Register link is not displayed in home page or link xpath is incorrect with property file","426f3926",false,true);
		}
		else
		{
			beforeClickUrl = getCurrentUrl();
			
			Thread.sleep(2000);
			
			clickAndWait(registerLink);
			
			writeTestResults("Verify login page","User can navigate to login page", "User Can navigate to login page","426f3926",true,false);
			 
			if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
			{
				openPage(common.siteUrl+registerURL);
			}	
			
		}
		if(getText("/html/body_xpath").indexOf("Fatal error")>-1)
		{
			writeTestResults("Verify Register page","Register page should be displayed", 
					"Following error is displayed. "+getText("/html/body_xpath"),"42713014",false,true);				
		}
	}	

	
	public void navigateToRegisterPage()throws Exception
	{	
		String link = registerLink;
		
		if(!(common.siteUrl+registerURL).equalsIgnoreCase(getCurrentUrl()))
		{		
			clickAndWait(link);				
		}		
	}
	
	public void userRegisterPageResults()throws Exception
	{	
		
		Thread.sleep(5000);
		
		if(!getTitle().equalsIgnoreCase(registerPageTitle))
		{
			 writeTestResults("Verify register page","User can navigate to register page", "User Can navigate to register page","426f3926",true,false);
			 
			 writeTestResults("Verify the User Register page", "Expected User Register page title is "+registerPageTitle , "Page is not redirect to register page title is "+getTitle(),
					 "426f1f68",false,false);			
		}
		 
		else
		{ 
			 writeTestResults("Verify the User Register page title", "Expected User Register page title is "+registerPageTitle , "Actual User Register page title is "+getTitle(),
					 "426f1f68",true,false);	
		}			

	}
	
	public void createNewUser() throws Exception
	{
 		common.updateUserAccountInAdmin(userCreationEmailAddress);
		
		if(!isDisplayed(txtFirstName))
		{
			navigateToUserRegisterPage();
		}		
		if(!txtFirstName.isEmpty())
		{
			if(isElementPresent(txtFirstName))
			{
				sendKeys(txtFirstName,userCreationFirstName);
				
				 writeTestResults("Verify the 'First Name' text field displayed in user registration page", "First Name text field should be displayed in user " +
				 		"registration page", "First Name text field is displayed in user registration page","",true,false);
			}
			else
			{
				 writeTestResults("Verify the 'First Name' text field displayed in user registration page", "First Name text field should be displayed in user " +
				 		"registration page", "First Name text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtLastName.isEmpty())
		{
			if(isElementPresent(txtLastName))
			{
				sendKeys(txtLastName,userCreationLastName);
				
				 writeTestResults("Verify the 'Last Name' text field displayed in user registration page", "Last Name text field should be " +
				 		"displayed in user registration page", "Last Name text field is displayed in user registration page","",true,false);
			}	
			else
			{			
				 writeTestResults("Verify the 'Last Name' text field displayed in user registration page", "Last Name text field should be displayed" +
				 		" in user registration page", "Last Name text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtEmail1.isEmpty())
		{
			if(isElementPresent(txtEmail1))
			{
				sendKeys(txtEmail1,userCreationEmailAddress);
				
				 writeTestResults("Verify the 'Email Address' text field displayed in user registration page", "Email Address text field should be displayed in " +
				 		"user registration page", "Email Address text field is displayed in user registration page","",true,false);
			}
			else
			{			
				 writeTestResults("Verify the 'Email Address' text field displayed in user registration page", "Email Address text field should be displayed in user" +
				 		" registration page", "Email Address text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtEmail2.isEmpty())
		{
			if(isElementPresent(txtEmail2))
			{
				sendKeys(txtEmail2,userCreationConfirmEmailAddress);
				
				 writeTestResults("Verify the 'Confirm Email Address' text field displayed in user registration page", "Confirm Email Address text field " +
				 		"should be displayed in user registration page", "Confirm Email Address text field is displayed in user registration page","",true,false);
			}
			else
			{			
				 writeTestResults("Verify the 'Confirm Email Address' text field displayed in user registration page", "Confirm Email Address text field " +
				 		"should be displayed in user registration page", "Confirm Email Address text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtPassword.isEmpty())
		{
			if(isElementPresent(txtPassword))
			{
				sendKeys(txtPassword,userCreationPassword);
				
				 writeTestResults("Verify the 'password' text field displayed in user registration page", "password text field should be displayed in user " +
				 		"registration page", "password field is displayed in user registration page","",true,false);
			}
			else
			{			
				 writeTestResults("Verify the 'password' text field displayed in user registration page", "password text field should be displayed in user " +
				 		"registration page", "password text field is not displayed in user registration page","",false,false);
			}
		}
			
		if(!txtConfirmPassword.isEmpty())
		{
			if(isElementPresent(txtConfirmPassword))
			{
				sendKeys(txtConfirmPassword,userCreationConfirmPassword);
				
				  writeTestResults("Verify the 'confirm Password' text field displayed in user registration page", "confirm Password text field " +
				  		"should be displayed in user registration page", "confirm Password field is displayed in user registration page","",true,false);
			}
			else
			{			
				 writeTestResults("Verify the 'confirm Password' text field displayed in user registration page", "confirm Password text field " +
				 		"should be displayed in user registration page", "confirm Password text field is not displayed in user registration page","",false,false);
			}
		}	
		
		if(isExecuted(getMethodIsExecuted,"Gender has dropdown"))
		{
			if(isElementPresent(userCreationGenderDropdown))
			{
				selectText(userCreationGenderDropdown,userCreationGender);
			}
			else
			{			
				 writeTestResults("Verify the 'Gender' dropdown displayed in user registration page", "Gender dropdown sould be displayed in user registration page", "Gender dropdown is not displayed in user registration page","",false,false);
			}
			
		}
		else
		{
			if(!userCreationGender.isEmpty())
			{
				if(userCreationGender.equalsIgnoreCase("Male"))
				{
					click(isMale);
				}
				else
				{
					click(isFemale);
				}
			}
		}
		if(isExecuted(getMethodIsExecuted,"Birthday has dropdown"))
		{
			if(isElementPresent(userCreationDobDayDropdown))
			{
				
				selectText(userCreationDobDayDropdown,userCreationBirthdayDay); 
			}
			else
			{			
				 writeTestResults("Verify the 'day' dropdown displayed in user registration page", "Day dropdown sould be displayed in user registration page", "Day dropdown is not displayed in user registration page","",false,false);
			}
			if(isElementPresent(userCreationDobMonthDropdown))
			{
				
				selectText(userCreationDobMonthDropdown,userCreationBirthdayMonth);
			}
			else
			{			
				 writeTestResults("Verify the 'Month' dropdown displayed in user registration page", "Month dropdown sould be displayed in user registration page", "Month dropdown is not displayed in user registration page","",false,false);
			}
			if(isElementPresent(userCreationDobYearDropdown))
			{
				
				selectText(userCreationDobYearDropdown,userCreationBirthdayYear);
			}
			else
			{			
				 writeTestResults("Verify the 'Year' dropdown displayed in user registration page", "Year dropdown sould be displayed in user registration page", "Year dropdown is not displayed in user registration page","",false,false);
			}
			
		}
		
		
		
		else
		{
			if(!txtBirthdayDay.isEmpty())
			{
				if(isElementPresent(txtBirthdayDay))
				{
					sendKeys(txtBirthdayDay,userCreationBirthdayDay);
					
					  writeTestResults("Verify the 'Birthday day' text field displayed in user registration page", "Birthday day text field should be displayed in " +
					  		"user registration page", "Birthday day field is displayed in user registration page","",true,false);
				}
				else
				{
				
					 writeTestResults("Verify the 'Birthday day' text field displayed in user registration page", "Birthday day text field should be displayed in " +
					 		"user registration page", "Birthday day text field is not displayed in user registration page","",false,false);
				}
			}
			if(!txtBirthdayMonth.isEmpty())
			{
				if(isElementPresent(txtBirthdayMonth))
				{
					sendKeys(txtBirthdayMonth,userCreationBirthdayMonth);
					
					  writeTestResults("Verify the 'Birthday month' text field displayed in user registration page", "Birthday month text field should be displayed in " +
					  		"user registration page", "Birthday month field is displayed in user registration page","",true,false);
				}
				else
				{
				
					 writeTestResults("Verify the 'Birthday month' text field displayed in user registration page", "Birthday month text field should be " +
					 		"displayed in user registration page", "Birthday month text field is not displayed in user registration page","",false,false);
				}
			}
			if(!txtBirthdayYear.isEmpty())
			{
				if(isElementPresent(txtBirthdayYear))
				{
					sendKeys(txtBirthdayYear,userCreationBirthdayYear);
					
					  writeTestResults("Verify the 'Birthday year' text field displayed in user registration page", "Birthday year text field should be displayed " +
					  		"in user registration page", "Birthday year field is displayed in user registration page","",true,false);
				}
				else
				{
				
					 writeTestResults("Verify the 'Birthday year' text field displayed in user registration page", "Birthday year text field should be displayed " +
					 		"in user registration page", "Birthday year text field is not displayed in user registration page","",false,false);
				}
			}
		}
		
		
		//edit-------------------------------------------------------------------------------------
		
		if(!txtUserCreationPostcode.isEmpty())
		{
			if(isDisplayed(txtUserCreationPostcode))
			{	
				
				sendKeys(txtUserCreationPostcode,userCreationPostcode);
				
				
				
				if(isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In User Creation"))
				{
					Thread.sleep(2000);
					
					if(!isElementPresent(selectPostCode_DropDown))
					{
						Thread.sleep(2000);
					}
					
					click(selectPostCode_DropDown);
					Thread.sleep(2000);
						
					if(!isElementPresent(txtstor))
						
							{
							Thread.sleep(2000);
							
							
							}
						       
								click(txtstor);
						
				}	
				
				
				
				
				 writeTestResults("Verify the 'Post Code' text field displayed in user registration page", "Post Code text field should be displayed in " +
				 		"billing information page", "Post Code field is displayed in user registration page","",true,false);				
			}
			else
			{
				
				 writeTestResults("Verify the 'Post Code' text field displayed in user registration page", "Post Code text field should be displayed in " +
				 		"billing information page", "Post Code text field is not displayed in user registration page","",false,false);
			}
		}
		
		
	
		
		if(!txtTelephone.isEmpty())
		{
			if(isDisplayed(txtTelephone))
			{
				sendKeys(txtTelephone,telephone);
							
				 writeTestResults("Verify the 'Contact Number' text field displayed in user registration page", "Contact Number text field should be displayed in" +
				 		" billing information page", "Contact Number field is displayed in user registration page","",true,false);
				
			}
			else
			{
				
				 writeTestResults("Verify the 'Contact Number' text field displayed in user registration page", "Contact Number text field should be displayed in " +
				 		"billing information page", "Contact Number text field is not displayed in user registration page","",false,false);
				
			}
		}
		
		if(!txtStreet.isEmpty())
		{
			if(isDisplayed(txtStreet))
			{
				sendKeys(txtStreet,street);
									
				 writeTestResults("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in " +
				 		"billing information page", "Street field is displayed in billing information page","",true,false);			
			}
			else
			{
				
				 writeTestResults("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in " +
				 		"billing information page", "Street text field is not displayed in billing information page","",false,false);				
			}
		}
		
		if(!isAgree.isEmpty())
		{
			if(isElementPresent(isAgree))
			{
				click(isAgree);
				
				 writeTestResults("Verify the 'Terms & Conditions' checkbox displayed in user registration page", "Terms & Conditions' checkbox should be " +
				 		"displayed in user registration page", "Terms & Conditions' checkbox is displayed in user registration page","",true,false);
			}
			else
			{
				 writeTestResults("Verify the 'Terms & Conditions' checkbox displayed in user registration page", "Terms & Conditions' checkbox should be " +
				 		"displayed in user registration page", "Terms & Conditions' checkbox is not displayed in user registration page","",false,false);
			}
		}
				
		if(!btnUserCreation.isEmpty())
		{
			if(isElementPresent(btnUserCreation))
			{				
				clickAndWait(btnUserCreation);
					
				common.accountLinksInUnderMyAcc();
				
				if(isElementPresent(linkLogout))
				{			
					 writeTestResults("Verify My Account page", "It's should be redirected to the 'My Account' page", "It's redirected to 'My Account","",true,false);
				}
				else
				{
					submitValidation();	
				}
	
			}
			else
			{
				 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", 
						 "Submit button is not displayed","",false,false);				
			}
		}		
	}

	//private void sendKeys(String txtUserCreationPostcode,String userCreationPostcode, String txtstor) {
		// TODO Auto-generated method stub
		
	//}

	public void accountDashboard() throws Exception
	{		
		//String userName = account.getUserName();
			
		
		
		String welComeMsgActual = driver.findElement(By.xpath(myAccWlcomeMsg)).getText(); 
		
		String welComeMsgExpected = "Thank you for registering with Dilmah Online Store.";
		
		if(welComeMsgActual.equals(welComeMsgExpected ))
		{
			writeTestResults("Verify the welcome message is displayed correctly", "Expected well Come massage "+welComeMsgExpected, welComeMsgActual+" is displayed in 'My Account","42724d82",true,false);
		}
		else
		{
			writeTestResults("Verify the welcome message is displayed correctly", "Expected well Come massage "+welComeMsgExpected, welComeMsgActual+" is displayed in 'My Account","42724d82",false,true);
		}
		
		if(isElementPresent(linkLogout) || getTitle().equalsIgnoreCase(MyAccountTestData.titleMyAccount))
		{
			 writeTestResults("Verify the My Account section", "My Account page should be displayed", "My Account page is displayed",
					 "426f54ba",true,false);
			// writeTestResults("Verify 'successful message'", "Successful message should be displayed", "Successful message is displayed",
					// "426f4bbe",true,false);
		}
		else
		{
			 writeTestResults("Verify the My Account content", "My Account page should be displayed", "My Account page is not displayed or logout link("+linkLogout+") " +
			 		"is not displayed","426f4bbe",false,true);
		}		
	}
	
	public void displayLogOutLink() throws Exception
	{		
		String logout = linkLogout;

		if(!isElementPresent(logout))
		{
			common.accountLinksInUnderMyAcc();
		}
		
		
		if(isElementPresent(logout))
		{				
			 writeTestResults("Verify the log out link displayed in My Account", "Logout link should be displayed in the My Account", "Logout link is displayed in the" +
			 		"My Account","426f5154",true,false);
			
			clickAndWait(logout);
		}
		else
		{			
			pageRefersh();
			
			 writeTestResults("Verify the log out link displayed in My Account", "Logout link should be displayed in the My Account", "Logout link is not displayed in" +
			 		" the My Account","426f5154",false,true);					
		}	
		clearCookies();
		
		common.navigateToHomePage();
	}	
	
	public void userLogOut()throws Exception
	{	
		myAccountMouseMove();
		
		common.accountLinksInUnderMyAcc();
		
		if(!isElementPresent(loginLink))
		{
			clickAndWait(linkLogout);	
			
			common.navigateToHomePage();
		}
		
		pageRefersh();
		
		clearCookies();		
	}
	
	public void updateDatabase()throws Exception
	{		
		 db.getConnection();
		 Random rand = new Random(); 
		 rand.nextInt(40); 	
		
	    String sql = "UPDATE customer_entity SET email = ? WHERE email =?";
		try{
			 java.sql.PreparedStatement st = db.getConnection().prepareStatement(sql);
			 st.setString(1, "test"+rand.nextInt(5000)+"@test.com");
			 st.setString(2, userCreationEmailAddress);
			 st.executeUpdate();			 
			 st.close(); 
		}
		catch (SQLException e) 
		{
			 writeTestResults("SQL ERROR", "SQL ERROR", "Error Message is "+ e.getMessage(),"",false,true);
			
			e.printStackTrace();
		  }		
	}
	
	public void loadMainPage()throws Exception 
	{
		openPage(common.siteUrl);
	}
	
	public void goBackToHomePage()throws Exception 
	{
		String link = registerLink;
		
		String beforeClickUrl ="";
		
		if(common.secureUrl.indexOf("@")>-1){
			common.secureUrl = common.siteUrl.substring(common.siteUrl.indexOf("@")+1);
			
			if(common.secureUrl.indexOf("https://")>-1)
			{
				common.secureUrl = "https://".concat(common.secureUrl);
			}
			else
			{
				common.secureUrl = "http://".concat(common.secureUrl);
			}
		}
		
		if(!(common.secureUrl+findElementInXLSheet(getParameterXpath,"Regiter URL")).equalsIgnoreCase(getCurrentUrl()))
		{			
			openPage(common.siteUrl);
			
			if(!common.navigateToLoginPage())
			{
				 writeTestResults("Verify login page","User can navigate to login page", "User Can't navigate to login page.login link is not displayed " +
				 		"or link path ("+loginLink+") is incorrect","",false,true);
			}		
			if(!isElementPresent(link))				
			{
				 writeTestResults("Verify the User Register link", "Register link should be displayed in the home page" , "Register link is not displayed " +
				 		"in home page or link xpath is incorrect with property file","",false,true);
			}
			else
			{
				beforeClickUrl = getCurrentUrl();
				
				clickAndWait(link);
						
				if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
				{
					openPage(common.siteUrl+registerURL);
				}					
			}				
		}				
	}
		
	public void createAlreadyExistsUser()throws Exception 
	{				
		fillTheForm();		
		
		if(!txtEmail1.isEmpty())
		{
			sendKeys(txtEmail1,userCreationAlreadyExixtsUser);
		}
		if(!txtEmail2.isEmpty())
		{
			sendKeys(txtEmail2,userCreationAlreadyExixtsUser);
		}
				
		clickAndWait(btnUserCreation);	
				
		if(isElementPresent(registerAlreadyRegistedUser))
		{			
			if(getText(registerAlreadyRegistedUser).equalsIgnoreCase(registerMsgAlreadyRegistedUser))
			{
				 writeTestResults("Verify the user creation functionality with already exists user", "Expected message is '"+registerMsgAlreadyRegistedUser, 
						 "Actual message is '"+getText(registerAlreadyRegistedUser),
						 "426f3f3e",true,false);				
			}
			else 
			{
				 writeTestResults("Verify the user creation functionality with already exists user", "Expected message is "+registerMsgAlreadyRegistedUser, 
						 "Actual message is "+getText(registerAlreadyRegistedUser),
						 "426f3f3e",false,true);
			}
		}		
		else
		{
			common.accountLinksInUnderMyAcc();
		}
		
		if(isElementPresent(linkLogout))
		{
			userLogOut();			
			
		}
		
		common.navigateToHomePage();
				
	}
	
	public void fillTheForm() throws Exception
	{	
		loadRegisterPage();
		
		pageRefersh();		

		if(!txtFirstName.isEmpty())
		{
			if(isElementPresent(txtFirstName))
			{
				sendKeys(txtFirstName,userCreationFirstName);				
			}
			else
			{
				 writeTestResults("Verify the 'First Name' text field displayed in user registration page", "First Name text field should be displayed " +
				 		"in user registration page", "First Name text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtLastName.isEmpty())
		{
			if(isElementPresent(txtLastName))
			{
				sendKeys(txtLastName,userCreationLastName);				
			}	
			else
			{			
				 writeTestResults("Verify the 'Last Name' text field displayed in user registration page", "Last Name text field should be displayed " +
				 		"in user registration page", "Last Name text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtEmail1.isEmpty())
		{
			if(isElementPresent(txtEmail1))
			{
				sendKeys(txtEmail1,userCreationEmailAddress);				
			}
			else
			{			
				 writeTestResults("Verify the 'Email Address' text field displayed in user registration page", "Email Address text field should be" +
				 		" displayed in user registration page", "Email Address text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtEmail2.isEmpty())
		{
			if(isElementPresent(txtEmail2))
			{
				sendKeys(txtEmail2,userCreationConfirmEmailAddress);				
			}
			else
			{			
				 writeTestResults("Verify the 'Confirm Email Address' text field displayed in user registration page", "Confirm Email Address text field should be " +
				 		"displayed in user registration page", "Confirm Email Address text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtPassword.isEmpty())
		{
			if(isElementPresent(txtPassword))
			{
				sendKeys(txtPassword,userCreationPassword);				
			}
			else
			{			
				 writeTestResults("Verify the 'password' text field displayed in user registration page", "password text field should be displayed in user " +
				 		"registration page", "password text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtConfirmPassword.isEmpty())
		{
			if(isElementPresent(txtConfirmPassword))
			{
				sendKeys(txtConfirmPassword,userCreationConfirmPassword);				
			}
			else
			{			
				 writeTestResults("Verify the 'confirm Password' text field displayed in user registration page", "confirm Password text field should be " +
				 		"displayed in user registration page", "confirm Password text field is not displayed in user registration page","",false,false);
			}			
		}
		
		if(!isAgree.isEmpty())
		{
			if(isElementPresent(isAgree))
			{
				click(isAgree);
				
			}
			else
			{
				 writeTestResults("Verify the 'Terms & Conditions' checkbox displayed in user registration page", "Terms & Conditions' checkbox should be displayed " +
				 		"in user registration page", "Terms & Conditions' checkbox is not displayed in user registration page","",false,false);
			}
		}
		
		if(isExecuted(getMethodIsExecuted,"Gender has dropdown"))
		{
			if(isElementPresent(userCreationGenderDropdown))
			{
				selectText(userCreationGenderDropdown,userCreationGender);
			}
			else
			{			
				 writeTestResults("Verify the 'Gender' dropdown displayed in user registration page", "Gender dropdown sould be displayed in user registration page", "Gender dropdown is not displayed in user registration page","",false,false);
			}
			
		}
		else
		{
			if(!userCreationGender.isEmpty())
			{
				if(userCreationGender.equalsIgnoreCase("Male"))
				{
					click(isMale);
				}
				else
				{
					click(isFemale);
				}
			}
		}
		
		if(isExecuted(getMethodIsExecuted,"Birthday has dropdown"))
		{
			if(isElementPresent(userCreationDobDayDropdown))
			{
				selectText(userCreationDobDayDropdown,userCreationBirthdayDay);
			}
			else
			{			
				 writeTestResults("Verify the 'day' dropdown displayed in user registration page", "Day dropdown sould be displayed in user registration page", "Day dropdown is not displayed in user registration page","",false,false);
			}
			if(isElementPresent(userCreationDobMonthDropdown))
			{
				selectText(userCreationDobMonthDropdown,userCreationBirthdayMonth);
			}
			else
			{			
				 writeTestResults("Verify the 'Month' dropdown displayed in user registration page", "Month dropdown sould be displayed in user registration page", "Month dropdown is not displayed in user registration page","",false,false);
			}
			if(isElementPresent(userCreationDobYearDropdown))
			{
				selectText(userCreationDobYearDropdown,userCreationBirthdayYear);
			}
			else
			{			
				 writeTestResults("Verify the 'Year' dropdown displayed in user registration page", "Year dropdown sould be displayed in user registration page", "Year dropdown is not displayed in user registration page","",false,false);
			}
			
		}
		else
		{
			if(!txtBirthdayDay.isEmpty())
			{
				if(isElementPresent(txtBirthdayDay))
				{
					sendKeys(txtBirthdayDay,userCreationBirthdayDay);
					
					  writeTestResults("Verify the 'Birthday day' text field displayed in user registration page", "Birthday day text field should be displayed in " +
					  		"user registration page", "Birthday day field is displayed in user registration page","",true,false);
				}
				else
				{
				
					 writeTestResults("Verify the 'Birthday day' text field displayed in user registration page", "Birthday day text field should be displayed in " +
					 		"user registration page", "Birthday day text field is not displayed in user registration page","",false,false);
				}
			}
			if(!txtBirthdayMonth.isEmpty())
			{
				if(isElementPresent(txtBirthdayMonth))
				{
					sendKeys(txtBirthdayMonth,userCreationBirthdayMonth);
					
					  writeTestResults("Verify the 'Birthday month' text field displayed in user registration page", "Birthday month text field should be displayed in " +
					  		"user registration page", "Birthday month field is displayed in user registration page","",true,false);
				}
				else
				{
				
					 writeTestResults("Verify the 'Birthday month' text field displayed in user registration page", "Birthday month text field should be " +
					 		"displayed in user registration page", "Birthday month text field is not displayed in user registration page","",false,false);
				}
			}
			if(!txtBirthdayYear.isEmpty())
			{
				if(isElementPresent(txtBirthdayYear))
				{
					sendKeys(txtBirthdayYear,userCreationBirthdayYear);
					
					  writeTestResults("Verify the 'Birthday year' text field displayed in user registration page", "Birthday year text field should be displayed " +
					  		"in user registration page", "Birthday year field is displayed in user registration page","",true,false);
				}
				else
				{
				
					 writeTestResults("Verify the 'Birthday year' text field displayed in user registration page", "Birthday year text field should be displayed " +
					 		"in user registration page", "Birthday year text field is not displayed in user registration page","",false,false);
				}
			}
		}
		
		// edit --------------------------------------------------------------
		if(!txtUserCreationPostcode.isEmpty())
		{
			if(isDisplayed(txtUserCreationPostcode))
			{	
				
				sendKeys(txtUserCreationPostcode,userCreationPostcode);
				
				
				
				if(isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In User Creation"))
				{
					Thread.sleep(2000);
					
					if(!isElementPresent(selectPostCode_DropDown))
					{
						Thread.sleep(2000);
					}
					
					click(selectPostCode_DropDown);
					Thread.sleep(2000);
						
						if(!isElementPresent(txtstor))
						
							{
							Thread.sleep(2000);
							
							}
						;
						click(txtstor);
						
				}
	}	
			else
			{				
				 writeTestResults("Verify the 'Post Code' text field displayed in user registration page", "Post Code text field should be displayed " +
				 		"in billing information page", "Post Code text field is not displayed in user registration page","",false,false);
			}
		}
		
		if(!txtTelephone.isEmpty())
		{
			if(isDisplayed(txtTelephone))
			{
				sendKeys(txtTelephone,telephone);
			}
			else
			{				
				 writeTestResults("Verify the 'Contact Number' text field displayed in user registration page", "Contact Number text" +
				 		" field should be displayed in billing information page", "Contact Number text field is not displayed in user registration page","",false,false);
				
			}
		}
		
		if(!txtStreet.isEmpty())
		{
			if(isDisplayed(txtStreet))
			{
				sendKeys(txtStreet,street);
			}
			else
			{				
				 writeTestResults("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in billing " +
				 		"information page", "Street text field is not displayed in billing information page","",false,false);
			}
		
		}
		
		Thread.sleep(WAIT);
	}
	
	public void clearForm() throws Exception
	{		
		loadRegisterPage();				
	}
	
	public void createUserWithoutTitle()throws Exception
	{
		updateDatabase();
		loadRegisterPage();
		fillTheForm();	
		clickSubmitbutton();
		accountDashboard();
		userLogOut();		
	}
	
	public void loadRegisterPage()throws Exception
	{		
		if(!isDisplayed(txtFirstName))
		{
			navigateToUserRegisterPage();
		}			
	}

	public void submitValidation() throws Exception
	{
		if(!registerAlreadyRegistedUser.isEmpty())
		{
			if(isDisplayed(registerAlreadyRegistedUser))
			{
				if(getText(registerAlreadyRegistedUser).equalsIgnoreCase(registerMsgAlreadyRegistedUser))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Following message got it '"+getText(registerAlreadyRegistedUser)+"'","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Actual message is "+getText(registerAlreadyRegistedUser),"",false,true);
				}			
			}				
		}
		
		if(!registerWithoutFirstName.isEmpty())
		{
			if(isDisplayed(registerWithoutFirstName))
			{
				if(getText(registerWithoutFirstName).equalsIgnoreCase(registerMsgWithoutFirstName))
				{					
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "First name text box value is empty in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "First name text box value is empty and message is "+getText(registerWithoutFirstName),"",false,true);
				}
			}
		}
		if(!registerWithoutLastName.isEmpty())
		{
			if(isDisplayed(registerWithoutLastName))
			{
				if(getText(registerWithoutLastName).equalsIgnoreCase(registerMsgWithoutLastName))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Last name text box value is empty in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Last name text box value is empty and message is "+getText(registerWithoutLastName),"",false,true);
				}
			}
		}
		if(!registerWithoutEmail.isEmpty())
		{
			if(isDisplayed(registerWithoutEmail))
			{
				if(getText(registerWithoutEmail).equalsIgnoreCase(registerMsgWithoutEmail))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Email text box value is empty in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "First name text box value is empty and message is "+getText(registerWithoutEmail),"",false,true);
				}
			}
		}
		if(!registerWithoutConfirmEmail.isEmpty())
		{
			if(isDisplayed(registerWithoutConfirmEmail))
			{
				if(getText(registerWithoutConfirmEmail).equalsIgnoreCase(registerMsgWithoutConfirmEmail))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Confirm email text box value is empty in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Confirm email text box value is empty and message is "+getText(registerWithoutConfirmEmail),"",false,true);
				}
			}
			
		}
		if(!registerWithoutPassword.isEmpty())
		{
			if(isDisplayed(registerWithoutPassword))
			{
				if(getText(registerWithoutPassword).equalsIgnoreCase(registerMsgWithoutPassword))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Password text box value is empty in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Password text box value is empty and message is "+getText(registerWithoutPassword),"",false,true);
				}
			}
		}
		
		if(!registerWithoutPostcode.isEmpty())
		{
			if(isDisplayed(registerWithoutPostcode))
			{
				if(getText(registerWithoutPostcode).equalsIgnoreCase(registerMsgWithoutPostcode))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Post code text box value is empty in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Password text box value is empty and message is "+getText(registerWithoutPostcode),"",false,true);
				}
			}
		}
		
		
		if(!registerLessConfirmPassword.isEmpty())
		{
			if(isDisplayed(registerLessConfirmPassword))
			{
				if(getText(registerLessConfirmPassword).equalsIgnoreCase(registerMsgWithoutConfirmPassword))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Confirm password text box value is empty in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Confirm password text box value is empty and message is "+getText(registerLessConfirmPassword),"",false,true);
				}
			}
		}
		if(!registerLessPassword.isEmpty())
		{
			if(isDisplayed(registerLessPassword))
			{
				if(getText(registerLessPassword).equalsIgnoreCase(registerMsgLessPassword))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Password length is less 6 in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Password length is less 6 and message is "+getText(registerLessPassword),"",false,true);
				}
			}
		}
		if(!registerLessConfirmPassword.isEmpty())
		{
			if(isDisplayed(registerLessConfirmPassword))
			{
				if(getText(registerLessConfirmPassword).equalsIgnoreCase(registerMsgLessConfirmPassword))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Confirm password length less 6 in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Confirm password length less 6 and message is "+getText(registerLessConfirmPassword),"",false,true);
				}
			}
		}
		if(!registerMissMatchPassword.isEmpty())
		{
			if(isDisplayed(registerMissMatchPassword))
			{
				if(getText(registerMissMatchPassword).equalsIgnoreCase(registerMsgMissMatchPassword))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Password mismatch in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Password mismatch and message is "+getText(registerMissMatchPassword),"",false,true);
				}
			}
		}
		if(!registerMissmatchEmail.isEmpty())
		{
			if(isDisplayed(registerMissmatchEmail))
			{
				if(getText(registerMissmatchEmail).equalsIgnoreCase(registerMsgMissmatchEmail))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Email mismatch in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Email mismatch and message is "+getText(registerMissmatchEmail),"",false,true);
				}
			}
		}
		if(!registerInvalidEmail.isEmpty())
		{
			if(isDisplayed(registerInvalidEmail))
			{
				if(getText(registerInvalidEmail).equalsIgnoreCase(registerMsgInvalidEmail))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Invalid email in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Invalid email and message is "+getText(registerInvalidEmail),"",false,true);
				}
			}
		}
		if(!registerMissmatchConfirmEmail.isEmpty())
		{
			if(isDisplayed(registerMissmatchConfirmEmail))
			{
				if(getText(registerMissmatchConfirmEmail).equalsIgnoreCase(registerMsgInvalidConfirmEmail))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Invalid confirm email in user registration page.","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Invalid confirm email and message is "+getText(registerMissmatchConfirmEmail),"",false,true);
				}
			}
		}
		
		
		if(!registerInvalidBirthday.isEmpty())
		{
			if(isDisplayed(registerInvalidBirthday))
			{
				if(getText(registerInvalidBirthday).equalsIgnoreCase(registerMsgInvalidBirthday))
				{					
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Following message got it '"+getText(registerInvalidBirthday)+"'","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Actual message is "+getText(registerInvalidBirthday),"",false,true);
				}
			}						
		}	
		
		/*if(!registeWitoutPostcode.isEmpty())
		{
			if(isDisplayed(registeWitoutPostcode))
			{
				if(getText(registeWitoutPostcode).equalsIgnoreCase(registeMsgWitoutPostcode))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Following message got it '"+getText(registeWitoutPostcode)+"'","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Actual message is "+getText(registeWitoutPostcode),"",false,true);
				}
			}
		}*/
		if(!registerWithoutTelephone.isEmpty())
		{
			if(isDisplayed(registerWithoutTelephone))
			{
				if(getText(registerWithoutTelephone).equalsIgnoreCase(registerMsgWithoutTelephone))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Following message got it '"+getText(registerWithoutTelephone)+"'","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Actual message is "+getText(registerWithoutTelephone),"",false,true);
				}
			}
			
		}
		
		if(!registerInvalidTelephone.isEmpty())
		{
			if(isDisplayed(registerInvalidTelephone))
			{
				if(getText(registerInvalidTelephone).equalsIgnoreCase(registerMsgInvalidTelephone))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Following message got it '"+getText(registerInvalidTelephone)+"'","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Actual message is "+getText(registerInvalidTelephone),"",false,true);
				}
			}			
		}
		
		if(!registeWitoutStreet.isEmpty())
		{
			if(isDisplayed(registeWitoutStreet))
			{
				if(getText(registeWitoutStreet).equalsIgnoreCase(registeMsgWitoutStreet))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Following message got it '"+getText(registeWitoutStreet)+"'","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", 
							 "Actual message is "+getText(registeWitoutStreet),"",false,true);
				}
			}			
		}
		
		if(!registeWitoutStreet.isEmpty())
		{
			if(isDisplayed(registeWitoutStreet))
			{
				if(getText(registeWitoutStreet).equalsIgnoreCase(registeMsgWitoutStreet))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Following message got it '"+getText(registeWitoutStreet)+"'","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Actual message is "+getText(registeWitoutStreet),"",false,true);
				}
			}			
		}
			
		if(!registeWitoutTerms.isEmpty())
		{
			if(isDisplayed(registeWitoutTerms))
			{
				if(getText(registeWitoutTerms).equalsIgnoreCase(registeMsgWitoutTerms))
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Following message got it '"+getText(registeWitoutTerms)+"'","",false,true);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Actual message is "+getText(registeWitoutTerms),"",false,true);
				}
			}
		}
		
		if(getText("/html/body/pre").indexOf("error")>-1)
		{
			writeTestResults("Verify user clicks submit button with correct data in user registration page", "My Account page should be displayed", "Following message got it '"+getText("/html/body/pre")+"'","",false,true);

		}
	}
		
	public void clickSubmitbutton() throws Exception
	{		
		if(isElementPresent(btnUserCreation))
		{
			click(btnUserCreation);
			
			submitValidation();		
		}	
	}
	
	public void fieldValidationWithoutData()throws Exception 
	{		
		fillTheForm();
					
		sendKeys(txtFirstName,"");
		sendKeys(txtLastName,"");
		sendKeys(txtEmail1,"");
		sendKeys(txtConfirmPassword,"");
		sendKeys(txtPassword,"");
			
		click(btnUserCreation);
		
		
		
		if(isDisplayed(registerWithoutFirstName) && isDisplayed(registerWithoutLastName) && isDisplayed(registerWithoutEmail)
				&& isDisplayed(registerWithoutConfirmPassword) && isDisplayed(registerWithoutPassword))
		{				
			 writeTestResults("Verify user clicks submit button with without filling data in user registration page", "Error message should be displayed", 
					 "Error message is displayed in user registration page.",
					 "426f3c78",true,false);
			 
		}
					
	}
	
	public void fieldValidationWithoutFirstname()throws Exception 
	{		
		if(!txtFirstName.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtFirstName,"");
			
			click(btnUserCreation);
			
			if(isDisplayed(registerWithoutFirstName))
			{				
				if(getText(registerWithoutFirstName).equalsIgnoreCase(registerMsgWithoutFirstName))
				{
					 writeTestResults("Verify user clicks submit button with without 'First Name' in user registration page", registerMsgWithoutFirstName+" " +
					 		"message should be displayed", registerMsgWithoutFirstName+" message is displayed in user registration page.","",true,false);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with without 'First Name' in user registration page", registerMsgWithoutFirstName+"" +
					 		" message should be displayed", registerMsgWithoutFirstName+" message is not displayed in user registration page.following message got it. "+getText(registerWithoutFirstName),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify user clicks submit button with without 'First Name' in user registration page", registerMsgWithoutFirstName+" " +
				 		"message should be displayed", registerMsgWithoutFirstName+" message is not displayed in user registration page.following xpath is not present '"+registerWithoutFirstName,"",false,true);
			}
		}		
	}
	
	public void fieldValidationWithoutlastname()throws Exception 
	{		
		if(!txtLastName.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtLastName,"");
			
			click(btnUserCreation);
						
			if(isDisplayed(registerWithoutLastName))
			{
				if(getText(registerWithoutLastName).equalsIgnoreCase(registerMsgWithoutLastName))
				{
					 writeTestResults("Verify user clicks submit button with without 'Last Name' in user registration page", registerMsgWithoutLastName+" message should be displayed", registerMsgWithoutLastName+" message is displayed in user registration page.","",true,false);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with without 'Last Name' in user registration page", registerMsgWithoutLastName+" message should be displayed", registerMsgWithoutLastName+"  message is not displayed in user registration page.following message got it. "+getText(registerWithoutLastName),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify user clicks submit button with without 'Last Name' in user registration page", registerMsgWithoutLastName+" message should be displayed", registerMsgWithoutLastName+" message is not displayed in user registration page.following xpath is not present '"+registerWithoutLastName,"",false,true);
			}	
		}	
	}
	
	public void fieldValidationWithoutEmail_address()throws Exception 
	{		
		if(!txtEmail1.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtEmail1,"");
			
			click(btnUserCreation);	
			
			
			
			if(isDisplayed(registerWithoutEmail))
			{				
				if(getText(registerWithoutEmail).equalsIgnoreCase(registerMsgWithoutEmail))
				{
					 writeTestResults("Verify user clicks submit button with without 'email address' in user registration page", registerMsgWithoutEmail+" message " +
					 		"should be displayed", registerMsgWithoutEmail+" message is displayed in user registration page.","",true,false);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with without 'email address' in user registration page", registerMsgWithoutEmail+" message " +
					 		"should be displayed", registerMsgWithoutEmail+" message is not displayed in user registration page." +
					 				"following message got it. "+getText(registerWithoutEmail),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify user clicks submit button with without 'email address' in user registration page", registerMsgWithoutEmail+" message should be displayed", registerMsgWithoutEmail+" message is not displayed in user registration page.following xpath is not present '"+registerWithoutEmail,"",false,true);
			}	
		}		
	}
	
	public void fieldValidationWithoutConfirmEmailAddress()throws Exception 
	{		
		if(!txtEmail2.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtEmail2,"");
			
			click(btnUserCreation);
			
			
			if(isDisplayed(registerWithoutConfirmEmail))
			{
				if(getText(registerWithoutConfirmEmail).equalsIgnoreCase(registerMsgWithoutConfirmEmail))
				{
					 writeTestResults("Verify user clicks submit button with without 'Confirm email address' in user registration page", 
							 registerMsgWithoutConfirmEmail+" message should be displayed", registerMsgWithoutConfirmEmail+" message is " +
							 		"displayed in user registration page.","",true,false);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with without 'Confirm email address' in user registration page",
							 registerMsgWithoutConfirmEmail+" message should be displayed", registerMsgWithoutConfirmEmail+" message is not " +
							 		"displayed in user registration page.following message got it. "+getText(registerWithoutConfirmEmail),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify user clicks submit button with without 'Confirm email address' in user registration page", 
						 registerMsgWithoutConfirmEmail+" message should be displayed", registerMsgWithoutConfirmEmail+" message is not " +
						 		"displayed in user registration page.following xpath is not present '"+registerWithoutConfirmEmail,"",false,true);
			}	
		}		
	}
	
	public void fieldValidationWithoutConfirmationPassword()throws Exception 
	{		
		if(!txtConfirmPassword.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtConfirmPassword,"");
						
			click(btnUserCreation);
			
			
			
			if(isDisplayed(registerWithoutConfirmPassword))
			{
				if(getText(registerWithoutConfirmPassword).equalsIgnoreCase(registerMsgWithoutConfirmPassword))
				{
					 writeTestResults("Verify user clicks submit button with without 'Confirm password' in user registration page", registerMsgWithoutConfirmPassword+" " +
					 		"message should be displayed", registerMsgWithoutConfirmPassword+" message is displayed in user registration page.","",true,false);
					
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with without 'Confirm password' in user registration page", registerMsgWithoutConfirmPassword+"" +
					 		" message should be displayed", registerMsgWithoutConfirmPassword+" message is not displayed in user registration page." +
					 				"following message got it. "+getText(registerWithoutConfirmPassword),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify user clicks submit button with without 'Confirm password' in user registration page", registerMsgWithoutConfirmPassword+"" +
				 		" message should be displayed", registerMsgWithoutConfirmPassword+" message is not " +
				 				"displayed in user registration page.following xpath is not present '"+registerWithoutConfirmPassword,"",false,true);
			}	
		}				
	}
	
	public void fieldValidationWithoutPassword()throws Exception 
	{		
		if(!txtPassword.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtPassword,"");
			
			click(btnUserCreation);
			
			
			
			if(isDisplayed(registerWithoutPassword))
			{
				if(getText(registerWithoutPassword).equalsIgnoreCase(registerMsgWithoutPassword))
				{
					 writeTestResults("Verify user clicks submit button with without 'Password' in user registration page", registerMsgWithoutPassword+" " +
					 		"message should be displayed", registerMsgWithoutPassword+" message is displayed in user registration page.","",true,false);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with without 'Password' in user registration page", registerMsgWithoutPassword+" " +
					 		"message should be displayed", registerMsgWithoutPassword+" message is not displayed in user registration page." +
					 				"following message got it. "+getText(registerWithoutPassword),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify user clicks submit button with without 'Password' in user registration page", registerMsgWithoutPassword+"" +
				 		" message should be displayed", registerMsgWithoutPassword+" message is not displayed in user registration page.following xpath is " +
				 				"not present '"+registerWithoutPassword,"",false,true);
				}	
		}		
	}
	
	public void passwordValidation1()throws Exception 
	{		
		if(!txtPassword.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtPassword,"12");	
			
			
			click(btnUserCreation);			
			
				
			if(isDisplayed(registerLessPassword))
			{
				if(getText(registerLessPassword).equalsIgnoreCase(registerMsgLessPassword))
				{
					 writeTestResults("Verify password validation when user type less than 6 characters", registerMsgLessPassword+" message should be displayed",
							 registerMsgLessPassword+" message is displayed in user registration page.",
							 "426f4592",true,false);
				}
				else
				{
					 writeTestResults("Verify password validation when user type less than 6 characters", registerMsgLessPassword+" message should be displayed",
							 registerMsgLessPassword+" message is not displayed in user registration page.following message got it. "+
					 getText(registerLessPassword),"426f4592",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify password validation when user type less than 6 characters", registerMsgLessPassword+" message should be displayed", 
						 registerMsgLessPassword+" message is not displayed in user registration page.following xpath is not present '"+registerLessPassword,
						 "426f4592",false,true);
			}				
		}	
	}
	
	public void passwordValidation2()throws Exception 
	{		
		if(!txtConfirmPassword.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtConfirmPassword,"654321");
			
			
			click(btnUserCreation);
			
			
			if(isDisplayed(registerMissMatchPassword))
			{
				if(getText(registerMissMatchPassword).equalsIgnoreCase(registerMsgMissMatchPassword))
				{
					 writeTestResults("Verify password validation when password and confirm password is mismatch", registerMsgMissMatchPassword+" " +
					 		"message should be displayed", registerMsgMissMatchPassword+" message is displayed in user registration page.",
					 		"426f484e",true,false);					 
				}
				else
				{
					
					 writeTestResults("Verify password validation when password and confirm password is mismatch", registerMsgMissMatchPassword+"" +
					 		" message should be displayed", registerMsgMissMatchPassword+" message is not displayed in user registration page." +
					 				"following message got it. "+getText(registerMissMatchPassword),"426f484e",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify password validation when password and confirm password is mismatch", registerMsgMissMatchPassword+" " +
				 		"message should be displayed", registerMsgMissMatchPassword+" message is not displayed in user registration page." +
				 				"following xpath is not present '"+registerMissMatchPassword,
				 				"426f484e",false,true);
			}		
		}
	}
	
	public void emailValidation1()throws Exception 
	{		
		if(!txtEmail1.isEmpty())
		{
			fillTheForm();			
			
			sendKeys(txtEmail1,"test.com");			
			
			click(btnUserCreation);			
			
			Thread.sleep(WAIT);
			
			if(isDisplayed(registerInvalidEmail))
			{				
				if(getText(registerInvalidEmail).equalsIgnoreCase(registerMsgInvalidEmail))
				{
					 writeTestResults("Verify email validation when email address hasn't '@' sign('test.yahoo')", registerMsgInvalidEmail+" " +
					 		"message should be displayed", registerMsgInvalidEmail+" message is displayed in user registration page.",
					 		"426f425e",true,false);
				}
				else
				{
					 writeTestResults("Verify email validation when email address hasn't '@' sign('test.yahoo')", registerMsgInvalidEmail+" message should be displayed", 
							 registerMsgInvalidEmail+" message is not displayed in user registration page.following message got it. "+getText(registerInvalidEmail),
							 "426f425e",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify email validation when email address hasn't '@' sign('test.yahoo')", registerMsgInvalidEmail+" message should be displayed", 
						 registerMsgInvalidEmail+" message is not displayed in user registration page.following xpath is not present '"+registerInvalidEmail,
						 "426f425e",false,true);
			}	
		}		
	}
	
	public void emailValidation2()throws Exception 
	{		
		if(!txtEmail1.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtEmail1,"test@yahoo");
						
			click(btnUserCreation);			
			
			if(isDisplayed(registerInvalidEmail))
			{
				if(getText(registerInvalidEmail).equalsIgnoreCase(registerMsgInvalidEmail))
				{
					 writeTestResults("Verify email validation when email address hasn't '.' sign('test@yahoo')", registerMsgInvalidEmail+" message should be displayed", registerMsgInvalidEmail+" message is displayed in user registration page.",
							 "426f425e",true,false);
				}
				else
				{
					 writeTestResults("Verify email validation when email address hasn't '.' sign('test@yahoo')", registerMsgInvalidEmail+" message should be displayed", registerMsgInvalidEmail+" message is not displayed in user registration page.following message got it. "+getText(registerInvalidEmail),
							 "426f425e",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify email validation when email address hasn't '.' sign('test@yahoo')", registerMsgInvalidEmail+" message should be displayed", registerMsgInvalidEmail+" message is not displayed in user registration page.following xpath is not present '"+registerInvalidEmail,
						 "426f425e",false,true);
			}	
		}	
	}
	
	//edit------------------------------------------------------------------------------------------------------------------
	
	public void postcodeValidation()throws Exception 
	{		
		if(!txtUserCreationPostcode.isEmpty())
		{
			fillTheForm();
			
			
			driver.findElement(By.xpath(".//*[@id='your-current-store-info']/div/div[2]/button")).click();
			//sendKeys(txtUserCreationPostcode,"200");
			
			click(btnUserCreation);
			
			
			
			if(isDisplayed(registeWitoutPostcode))
			{
				if(getText(registeWitoutPostcode).equalsIgnoreCase(registerMsgWithoutPostcode))
				{
					 writeTestResults("Verify user clicks submit button without 'postcode' in user registration page",registerMsgWithoutPostcode+" " +
					 		"message should be displayed", registerMsgWithoutPostcode+" message is displayed in user registration page.","",true,false);
				}
				else
				{
					 writeTestResults("Verify user clicks submit button with without 'Postcode' in user registration page", registerMsgWithoutPostcode+" " +
					 		"message should be displayed", registerMsgWithoutPostcode+" message is not displayed in user registration page." +
					 				"following message got it. "+getText(registeWitoutPostcode),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify user clicks submit button without 'Postcode' in user registration page", registerMsgWithoutPostcode+"" +
				 		" message should be displayed", registerMsgWithoutPostcode+" message is not displayed in user registration page.following xpath is " +
				 				"not present '"+registeWitoutPostcode,"",false,true);
				}	
		}		
	}
	
	public void confirmEmailValidation1()throws Exception 
	{	
		if(!txtEmail2.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtEmail2,"test.com");
			
			click(btnUserCreation);
			
			
			
			if(isDisplayed(registerMissmatchConfirmEmail))
			{
				if(getText(registerMissmatchConfirmEmail).equalsIgnoreCase(registerMsgInvalidConfirmEmail))
				{
					 writeTestResults("Verify email validation when confirm email address hasn't '@' sign('test.com')", registerMsgInvalidConfirmEmail+" message should be displayed", registerMsgInvalidConfirmEmail+" message is displayed in user registration page.","",true,false);
				}
				else
				{
					 writeTestResults("Verify email validation when confirm email address hasn't '@' sign('test.com')", registerMsgInvalidConfirmEmail+" message should be displayed", registerMsgInvalidConfirmEmail+" message is not displayed in user registration page.following message got it. "+getText(registerMissmatchConfirmEmail),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify email validation when confirm email address hasn't '@' sign('test.com')", registerMsgInvalidConfirmEmail+" message should be displayed", registerMsgInvalidConfirmEmail+" message is not displayed in user registration page.following xpath is not present '"+registerMissmatchConfirmEmail,"",false,true);
			}	
		}		
	}
	
	public void confirmEmailValidation2()throws Exception 
	{		
		if(!txtEmail2.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtEmail2,"test@yahoo");
			
			click(btnUserCreation);
			
			
			
			if(isDisplayed(registerMissmatchConfirmEmail))
			{
				if(getText(registerMissmatchConfirmEmail).equalsIgnoreCase(registerMsgInvalidConfirmEmail))
				{
					 writeTestResults("Verify email validation when confirm email address hasn't '@' sign('test.yahoo')", registerMsgInvalidConfirmEmail+" message should be displayed", registerMsgInvalidConfirmEmail+" message is displayed in user registration page.","",true,false);
					
				}
				else
				{
					 writeTestResults("Verify email validation when confirm email address hasn't '@' sign('test.yahoo')", registerMsgInvalidConfirmEmail+" message should be displayed", registerMsgInvalidConfirmEmail+" message is not displayed in user registration page.following message got it. "+getText(registerMissmatchConfirmEmail),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify email validation when confirm email address hasn't '@' sign('test.yahoo')", registerMsgInvalidConfirmEmail+" message should be displayed", registerMsgInvalidConfirmEmail+" message is not displayed in user registration page..following xpath is not present '"+registerMissmatchConfirmEmail,"",false,true);
			}	
		}	
	}
	
	public void compareEmailAndconfirmEmail()throws Exception 
	{		
		if(!txtEmail1.isEmpty() && !txtEmail2.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtEmail1,"test@yahoo.com");
			
			sendKeys(txtEmail2,"test@gmail.com");
				
			click(btnUserCreation);
			
			
			if(isDisplayed(registerMissmatchConfirmEmail))
			{
				if(getText(registerMissmatchConfirmEmail).equalsIgnoreCase(registerMsgInvalidConfirmEmail))
				{
					 writeTestResults("Verify email validation when email and confirm email is mismatch", "'The email addresses you entered do not match..' message should be displayed", "'The email addresses you entered do not match..' message is displayed in user registration page.","",true,false);
				}
				else
				{
					 writeTestResults("Verify email validation when email and confirm email is mismatch", "'The email addresses you entered do not match..' message should be displayed", "'The email addresses you entered do not match..' message is not displayed in user registration page.following message got it. "+getText(registerMissmatchConfirmEmail),"",false,true);
				}
			}
			else
			{
				 writeTestResults("Verify email validation when email and confirm email is mismatch", "'The email addresses you entered do not match..' message should be displayed", "'The email addresses you entered do not match..' message is not displayed in user registration page.following xpath is not present '"+registerMissmatchConfirmEmail,"",false,true);
			}	
		}			
	}
	
	public void validationMsgInBirthdayDay()throws Exception 
	{		
		if(!txtBirthdayDay.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtBirthdayDay,"");
			
			click(btnUserCreation);
					
			if(!registerInvalidBirthday.isEmpty())
			{
				if(isDisplayed(registerInvalidBirthday))
				{
					if(getText(registerInvalidBirthday).equalsIgnoreCase(registerMsgInvalidBirthday))
					{
						 writeTestResults("Verify birthday validation when user not enterd birth day", "Expected message is "+registerMsgInvalidBirthday, "Acual message is '"+getText(registerInvalidBirthday)+"'","",true,false);
						
					}
					else
					{
						 writeTestResults("Verify birthday validation when user not enterd birth day", "Expected message is "+registerMsgInvalidBirthday, "Acual message is '"+getText(registerInvalidBirthday)+"'","",false,true);
					}
				}	
				else
				{
					 writeTestResults("Verify birthday validation when user not enterd birth day", "Expected message is "+registerMsgInvalidBirthday, registerMsgInvalidBirthday+ " message is not displayed or invalid xpath ("+registerInvalidBirthday+")","",false,true);
				}
			}			
		}
	}	
	
	public void validationMsgInBirthdayMonth()throws Exception 
	{		
		if(!txtBirthdayMonth.isEmpty())
		{
			fillTheForm();
			
			sendKeys(txtBirthdayMonth,"");
			
			click(btnUserCreation);
					
			if(!registerInvalidBirthday.isEmpty())
			{
				if(isDisplayed(registerInvalidBirthday))
				{
					if(getText(registerInvalidBirthday).equalsIgnoreCase(registerMsgInvalidBirthday))
					{
						 writeTestResults("Verify birthday validation when user not enterd birth month", "Expected message is "+registerMsgInvalidBirthday, "Acual message is '"+getText(registerInvalidBirthday)+"'","",true,false);
						
					}
					else
					{
						 writeTestResults("Verify birthday validation when user not enterd birth month", "Expected message is "+registerMsgInvalidBirthday, "Acual message is '"+getText(registerInvalidBirthday)+"'","",false,true);
					}
				}
				else
				{
					 writeTestResults("Verify birthday validation when user not enterd birth month", "Expected message is "+registerMsgInvalidBirthday, registerMsgInvalidBirthday+ " message is not displayed or invalid xpath ("+registerInvalidBirthday+")","",false,true);
				}
			}			
		}				
	}
	
	public void validationMsgInBirthdayYear()throws Exception 
	{		
		if(!txtBirthdayYear.isEmpty())
		{
			fillTheForm();			

			sendKeys(txtBirthdayYear,"");
				
			click(btnUserCreation);
			
			if(!registerInvalidBirthday.isEmpty())
			{
				if(isDisplayed(registerInvalidBirthday))
				{
					if(getText(registerInvalidBirthday).equalsIgnoreCase(registerMsgInvalidBirthday))
					{
						 writeTestResults("Verify birthday validation when user not enterd birth year", "Expected message is "+registerMsgInvalidBirthday, "Acual message is '"+getText(registerInvalidBirthday)+"'","",true,false);
							
					}
					else
					{
						 writeTestResults("Verify birthday validation when user not enterd birth year", "Expected message is "+registerMsgInvalidBirthday, "Acual message is '"+getText(registerInvalidBirthday)+"'","",false,true);
					}
				}
				else
				{
					 writeTestResults("Verify birthday validation when user not enterd birth year", "Expected message is "+registerMsgInvalidBirthday, registerMsgInvalidBirthday+ " message is not displayed or invalid xpath ("+registerInvalidBirthday+")","",false,true);
				}
			}			
		}		
	}

	public void termsConditionsLink()throws Exception 
	{
		//navigateToRegisterPage();
		
		if(!linkTermsAndConditions.isEmpty())
		{
			if(isElementPresent(linkTermsAndConditions))
			{
				 writeTestResults("Verify Terms and Conditions link present in the user registering page", "'Terms and Conditions' link should be displayed", "'Terms and Conditions' link is displayed in user registration page.","",true,false);
				
				click(linkTermsAndConditions);	
				
				if(getTitle().equalsIgnoreCase(titleTermsAndConditions))
				{
					 writeTestResults("Verify Terms and Conditions link in the user registering page", "'Terms and Conditions' link should be redirct to 'Terms and Conditions' page", "'Terms and Conditions' link is redirect to 'Terms and Conditions' page","",true,false);
				
				}
				else
				{
					 writeTestResults("Verify Terms and Conditions link in the user registering page", "'Terms and Conditions' link should be redirct to 'Terms and Conditions' page", "Following page got it '"+getTitle()+"'","",false,true);
				}				
			}
		}		
	}
	
	public void privacyPolicyLink()throws Exception 
	{
		//navigateToRegisterPage();
		
		if(!linkPrivacyPolicy.isEmpty())
		{
			if(isElementPresent(linkPrivacyPolicy))
			{
				 writeTestResults("Verify Privacy Policy link present in the user registering page", "'Privacy Policy' link should be displayed", "'Privacy Policy' link is displayed in user registration page.","",true,false);
				
				click(linkPrivacyPolicy);					
				
				if(getTitle().equalsIgnoreCase(titlePrivacyPolicy))
				{
					 writeTestResults("Verify Privacy Policy link in the user registering page", "'Privacy Policy' link should be redirct to 'Privacy Policy' page", "'Privacy Policy' link is redirect to 'Privacy Policy' page","",true,false);
				}
				else
				{
					 writeTestResults("Verify Privacy Policy link in the user registering page", "'Privacy Policy' link should be redirct to 'Privacy Policy' page", "Following page got it '"+getTitle()+"'","",false,true);
				}				
			}
		}		
	}
		
	public void ConfirmationMaiReader() throws Exception
	{		
		String customerName = userCreationFirstName+" "+userCreationLastName;
		
		Thread.sleep(20000);
			
		mailRead(userCreationMailSubject,customerName,userCreationEmailAddress,"2wsx@#1qaz");
			
		String textMail =bodyText;
		
		if(!textMail.isEmpty())
		{
			 writeTestResults("Verify  the Email of the User registration", "Registration mail should receive to gmail account", "Registration mail is Received",
					 "426f4e84",true,false);
		}
		else
		{
			 writeTestResults("Verify  the Email of the User registration", "Registration mail should receive to gmail account", "Registration mail is not Received or subject is incorrect",
					 "426f4e84",false,true);
		}	
	}
	
	private void myAccountMouseMove()  throws Exception
	{	
		if(isExecuted(getMethodIsExecuted,"Mouse Hover In My Account"))
		{
			String mouseMoveToMyAccount = findElementInXLSheet(getParameterXpath,"mouse move to my account");
			
			if(isElementPresent(mouseMoveToMyAccount))
			{
				mouseMove(mouseMoveToMyAccount);
			}			
		}
	}
		
	public void totalTime(java.util.Date startTime) throws Exception
	{		
		java.util.Date endTime;		
		Calendar cal = Calendar.getInstance(); 		
		endTime = cal.getTime();
		String totalTime = common.totalTime(startTime, endTime);		   
	    writeTestResults("Total time to execute "+writeFileName, "Total time =",totalTime,"",true,false);		
	}

}