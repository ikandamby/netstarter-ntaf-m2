package com.net.test.pageObject;

//import java.util.List;

//import org.apache.bcel.generic.Select;
import java.util.Calendar;

import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.testng.Assert;
//import com.net.test.data.CheckoutTestData;
import com.net.test.data.SeoGoogleTrackingTestData;
import com.net.test.util.CreateIssue;
import com.net.test.util.ReadXl;
import com.net.test.util.SendTextMessage;
import com.net.test.util.TestCommonMethods;
import com.net.test.util.WriteResults;
 

public class SeoGoogleTracking extends SeoGoogleTrackingTestData

{	
	WriteResults wResult =  new WriteResults();
	ReadXl readParametrs  = new ReadXl();
	TestCommonMethods common = new TestCommonMethods();
	SendTextMessage sendSMS = new SendTextMessage();
	CreateIssue createIssue = new CreateIssue();
	
	public WebDriver driver =null;		
	
	public String orderId;
	public String conOrderId; 
	
	public String productNameInDetailPage;	
	public String productPriceInDetailPage;
	public String productPriceOld;
	public String price[];
	public String productPriceInDetailPageOld;
	public String productQTYInDetailPage;	
	public String productPriceInShoppingCartPage;
	public String productNameInShoppingCartPage;
	public String productQTYInShoppingCartPage;
	public String productPriceInOrderReview;
	public String productNameInOrderReview;
	public String productQTYInOrderReview;
	public int totalItem =0;

	public String pageError;
	public int i = 0;
	public int j = 0;	
	
	public void skipMethods(String methodName)throws Exception {
		
		wResult.writeSkipTestResult("Checkout Process ",methodName ,"", "Skip Results");
	}
	

	public void resultSheetName(String resultSheet)throws Exception 
	{
		writeFileName=resultSheet;		
	}

	public void loginToMyAccount()throws Exception 
	{
		if(common.navigateToLoginPage())
		{
			String validation = common.login();
			if(validation == "")
			{
				String pwValidation = common.passwordMatch();
				if(pwValidation =="")
				{
					wResult.writeTestResult("Verify My Account page", "User can navigate to my account", "User can navigate to my account", true, writeFileName);
						
				}
				else
				{
					wResult.writeTestResult("Verify My Account page", "User can navigate to my account", "User can not navigate to my account"+pwValidation, false, writeFileName);
					
				}
			}
			else
			{
				wResult.writeTestResult("Verify Login page", "Login page should be loaded", "Following field not present "+validation, false, writeFileName);
				Assert.assertEquals("Login page should be loaded", "Following field not present "+validation);
			
			}
		}
		else
		{
			wResult.writeTestResult("Verify Login page", "Login page should be loaded", "Login link is not present", false, writeFileName);
			Assert.assertEquals("Login page should be loaded", "Login link is not present");
		
		}
		
		common.goBackToHomePage();
		
		
	}
	
	
	public void homePageTrackingCode() throws Exception
	{
		System.out.println("checkoutAsGuestSelectUserTypeFunctionValue"+findElementInXLSheet(getParameterXpath,"home test123").trim());
		
		//System.out.println("xxx"+getPageSource(findElementInXLSheet(getParameterXpath,"home test123").trim()));
		if(!TagManagerContentTag.isEmpty())
		{
			System.out.println("xxx"+getPageSource(TagManagerContentTag));	
		}
		
		if(isElementPresent(homeProductXpath))
		{
			String productOnclickValue = getAttribute(homeProductXpath,"onclick");
			
			
			System.out.println("productOnclickValue"+productOnclickValue);
			
			if(productOnclickValue.indexOf(homeProductGACode) >= 0 )
			{
				String gaCode =	productOnclickValue.substring(productOnclickValue.indexOf("dataLayer.push"), productOnclickValue.indexOf("})")+2);
				
				String productName = "";
				
				if(getAttribute(homeProductNameXpath,"title") != null)
				{
					productName = getAttribute(homeProductNameXpath,"title");
				}
				else
				{
					productName = getText(homeProductNameXpath);
				}
					
				wResult.writeTestResult("Verify Product GA code in home page ", "GA Code : "+homeProductGACode+"\nProduct Name is : "+productName, "GA Code is - "+gaCode, true, writeFileName);
				
				
			}
			else
			{
				wResult.writeTestResult("Verify Product GA code in home page ", homeProductGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue, false, writeFileName);
				Assert.assertEquals(homeProductGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Home product attribute");
					
			}
			
		}
		else
		{
			wResult.writeTestResult("Verify Product in home page ", "Product should be displayed", "Product attribute is not displayed or incorrect xpath. "+homeProductXpath, false, writeFileName);
			Assert.assertEquals("Product should be displayed", "Product attribute is not displayed or incorrect xpath. "+homeProductXpath);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Home product attribute");
			
		}
		
	}
	
	
	public void homePageNewsLetterSubscribe() throws Exception
	{
		
		if(!TagManagerContentTag.isEmpty())
		{
			System.out.println("xxx"+getPageSource(TagManagerContentTag));	
		}
		
		if(isElementPresent(homeProductXpath))
		{
			String productOnclickValue = getAttribute(homeProductXpath,"onclick");
			
			
			System.out.println("productOnclickValue"+productOnclickValue);
			
			if(productOnclickValue.indexOf(homeProductGACode) >= 0 )
			{
				String gaCode =	productOnclickValue.substring(productOnclickValue.indexOf("dataLayer.push"), productOnclickValue.indexOf("})")+2);
				
				String productName = "";
				
				if(getAttribute(homeProductNameXpath,"title") != null)
				{
					productName = getAttribute(homeProductNameXpath,"title");
				}
				else
				{
					productName = getText(homeProductNameXpath);
				}
					
				wResult.writeTestResult("Verify Product GA code in home page ", "GA Code : "+homeProductGACode+"\nProduct Name is : "+productName, "GA Code is - "+gaCode, true, writeFileName);
				
				
			}
			else
			{
				wResult.writeTestResult("Verify Product GA code in home page ", homeProductGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue, false, writeFileName);
				Assert.assertEquals(homeProductGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Home product attribute");
					
			}
			
		}
		else
		{
			wResult.writeTestResult("Verify Product in home page ", "Product should be displayed", "Product attribute is not displayed or incorrect xpath. "+homeProductXpath, false, writeFileName);
			Assert.assertEquals("Product should be displayed", "Product attribute is not displayed or incorrect xpath. "+homeProductXpath);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Home product attribute");
			
		}
		
	}
	
	public boolean selectBrand() throws Exception
	{
		
		String categoryXpath = readXL.getBrandXpath();
		String beforeClickUrl ="";
		
		boolean foundCategory =true;
			
		if(categoryXpath.isEmpty()||categoryXpath.equalsIgnoreCase(null))
		{
			foundCategory =false;
		}
		else if(isElementPresent(categoryXpath))
		{
			beforeClickUrl = getCurrentUrl();	
			
			if(click(categoryXpath))
			{											
				Thread.sleep(100);
				
				waitForPageLoad();
				
				if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
				{
					openPage(getAttribute(categoryXpath,"href"));
				}			
				foundCategory =true;
				
			}
			else
			{
				foundCategory =false;
				wResult.writeTestResult("Verify user can select category in main navigation", "User can navigate to '"+readXL.brandName+"' page", "Select category '"+readXL.brandName+"' is not displayed in main navigation or xpath is incorrect", false, writeFileName);
				Assert.assertEquals("User can navigate to '"+readXL.brandName+"' page", "Select category '"+readXL.brandName+"' is not displayed in main navigation or xpath is incorrect");
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-User can select category in main navigation");
				
			}
			
		
		}
		else
		{
			foundCategory =false;
		}
		return foundCategory;
				
	}
	
	public void selectProductFromLeftCategory() throws Exception
	{
		if(isElementPresent(leftCategory))
		{
			click(leftCategory);
		}
		else
		{
			wResult.writeTestResult("Verify left categories displayed in category page", "Left Categories should be displayed", "Left category section is not displayed or xpath is incorrect", false, writeFileName);
			Assert.assertEquals("Left Categories should be displayed", "Left category section is not displayed or xpath is incorrect");
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Left Category");
			
			
		}
	}
	
	
	public boolean productGrid() throws Exception
	{
		boolean foundProductGrid = true;
		if(isElementPresent(productGrid))
		{
			click(productGrid);
		}
		else
		{
			foundProductGrid =false;
		}
		return foundProductGrid;
		
		
	}
	
	
	public boolean productAvailability() throws Exception
	{
			boolean productAvailable = true;

			
			int totalItem=0;
			int totalRows;
			int rem = totalItem % 4;			
			int total = totalItem/4;
			
			if(rem>0){
				totalRows = total+1;
				System.out.println("rem"+totalRows);
			}
			else 
			{
				totalRows = total;
				System.out.println("totalRows"+totalRows);
			}			
			
			
			System.out.println("getProductAttributes"+getProductAttributes);
			System.out.println("isElementPresent"+isElementPresent(getProductAttributes));
			
			if(isElementPresent(getProductAttributes))
			{
				productItem		=	getText(getProductAttributes);				
							
				productName 	= 	getText(getProductAttributes+getProductName);
					
				
				if(getTagListSize(getProductAttributes+getProductPrice,"p_TagName") > 1)
				{
					productPrice 	= 	getText(getProductAttributes+getProductPrice+"/p[2]");
				}
				else if(getTagListSize(getProductAttributes+getProductPrice,"p_TagName") == 1)
				{
					productPrice 	= 	getText(getProductAttributes+getProductPrice+"/p");
				}
				else
				{
					productPrice 	= 	getText(getProductAttributes+getProductPrice);
				}
				
				if(getTagListSize(getProductAttributes+getProductPrice,"p_TagName") == 1)
				
				
				if(isElementPresent(categoryProductAddToCartXpath))
				{
					String productOnclickValue = getAttribute(categoryProductAddToCartXpath,"onclick");
										
					System.out.println("productOnclickValue"+productOnclickValue);
					
					if(productOnclickValue.indexOf(CategoryProductGACode) >= 0 )
					{
						String gaCode =	productOnclickValue.substring(productOnclickValue.indexOf("dataLayer.push"), productOnclickValue.indexOf("})")+2);
												
						wResult.writeTestResult("Verify Product GA code in category page ", "GA Code : "+CategoryProductGACode+"\nProduct Name is : "+productName, "GA Code is - "+gaCode, true, writeFileName);
						
						
					}
					else
					{
						wResult.writeTestResult("Verify Product GA code in Category page ", CategoryProductGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue, false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Category product attribute");
							
					}
					
				}
				else
				{
					wResult.writeTestResult("Verify Product in category page ", "Product should be displayed", "Product attribute is not displayed or incorrect xpath. "+categoryProductAddToCartXpath, false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Category product attribute");
					
				}
								
					
				setProductName = productName+"_Link";
				
				System.out.println("productPriceCategoryPage"+productPrice);
				System.out.println("productNameCategoryPage"+productName);
				
				if(isElementPresent(setProductName))
				{
					String beforeClickUrl ="";
					
					beforeClickUrl = getCurrentUrl();
					
					click(setProductName);	
					
					waitForPageLoad();
					
					if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
					{
						openPage(getAttribute(getProductAttributes+"/a","href"));
					}				
						
					
				}
				else
				{
					productAvailable = false;
					wResult.writeTestResult("Verify user can select product in category page", "User can navigate to '"+productName+"' details page", "user can't navigate to product details page", false, writeFileName);
					Assert.assertEquals( "User can navigate to '"+productName+"' details page", "user can't navigate to product details page");
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-User can navigate to '"+productName+"' details page");
					
				}
				
			}
			else
			{
				productAvailable = false;
				wResult.writeTestResult("Verify user can select product in category page", "products should be displayed in category page", "Products are not displayed or product attribute xpath is incorrect", false, writeFileName);
				Assert.assertEquals("User can navigate to '"+productName+"' details page", "Products are not displayed or product attribute xpath is incorrect");
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user can select product in category page");
				
					
			}
			return productAvailable;
		
	}
	
	
	public void productNameInProductDetail() throws Exception
	{
		if(isElementPresent(productNameXpathInProductDetailPage))
		{
			productNameInDetailPage = getText(productNameXpathInProductDetailPage);
		}
		else
		{
			productNameInDetailPage = "";
		}
		
		
	}
	
	public void productPriceInProductDetail() throws Exception
	{
		System.out.println("productPriceXpath"+productPriceXpathInProductDetailPage);
		System.out.println("xxx"+isElementPresent(productPriceXpathInProductDetailPage));
		
		if(isElementPresent(productPriceXpathInProductDetailPage))
		{
			System.out.println("yyy"+getTagListSize(productPriceXpathInProductDetailPage,"p_TagName"));
			
			if(getTagListSize(productPriceXpathInProductDetailPage,"p_TagName")>1)
			{
				productPriceInDetailPage 	= 	getText(productPriceXpathInProductDetailPage+"/p[2]");
			}
			else
			{
				productPriceInDetailPage = getText(productPriceXpathInProductDetailPage);	
			}	
			
			
		}
		else
		{
			productPriceInDetailPage = "";			
		}
		
		System.out.println("productPriceInDetailPage "+productPriceInDetailPage);
		
	}
	
	/*public void productQTYInProductDetail() throws Exception
	{
		//productQTYInDetailPage =findElementInXLSheet(getCheckoutDetails,("QTY");
		
		//productQTYXpath  = findElementInXLSheet(getParameterXpath,"QTY");
	
	
	}*/	

	public void productNameCompareInProductDetail() throws Exception
	{
		if(!productNameInDetailPage.equalsIgnoreCase(productName)){
			
			wResult.writeTestResult("Verify the name of the selected product is displaying in product detail page", "Expected product Name is "+productName +" in product detail page", "Actual product name is "+productNameInDetailPage+" in product detail page", false, writeFileName);
			
		}
		else
		{
			wResult.writeTestResult("Verify the name of the selected product is displaying in product detail page", "Expected product Name is "+productName+" in product detail page" , "Actual product name is "+productNameInDetailPage+" in product detail page", true, writeFileName);
			
			
		}

	}
	public void productPriceCompareInProductDetail() throws Exception
	{
		if(!productPriceInDetailPage.trim().equalsIgnoreCase(productPrice.trim())){
			
			wResult.writeTestResult("Verify the price of the selected product is displaying in product detail page", "Expected product price is "+productPrice +" in product detail page", "Actual product price is "+productPriceInDetailPage+" in product detail page", false, writeFileName);
			
		}
		else
		{
			wResult.writeTestResult("Verify the price of the selected product is displaying in product detail page", "Expected product price is "+productPrice +" in product detail page", "Actual product price is "+productPriceInDetailPage+" in product detail page", true, writeFileName);
			
			
		}

	}
	public void productQTYCompareInProductDetail() throws Exception
	{
		if(isElementPresent(productQTYXpath)){
			
			sendKeys(productQTYXpath, productQTYInDetailPage);	
		}

	}
	
	public void verifyAddToCartButton() throws Exception
	{
		if(isElementPresent(outOfStockProductMessage))
		{
			wResult.writeTestResult("Verify product availablity in product detail page", "Product should be available", "Product is out of stock following message got it:"+getText(outOfStockProductMessage), false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-"+writeFileName+"- Verify product availablity in product detail page");
			Assert.assertEquals("Product should be available", "Product is out of stock following message got it:"+getText(outOfStockProductMessage));
		
		}
		else
		{
			wResult.writeTestResult("Verify the add to cart button is displayed in product detail page", "Add to cart button should be displayed in product detail page", "Add to cart button is not displayed in product detail page", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-"+writeFileName+"- Verify the add to cart button is displayed in product detail page");
			Assert.assertEquals("Add to cart button should be displayed in product detail page", "Add to cart button is not displayed in product details page ");
			
		}
		
	}
	 
	public void addToCartButton() throws Exception
	{		
			
		click(addToCartButton);
			
		Thread.sleep(2000);
			
		/*if(!ajaxCall(ajaxLoader))
		{
			wResult.writeTestResult("Verify product is add to the cart", "Product should be add to the cart", "Product is not add to the cart within 100sec", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify product is add to the cart");
			Assert.assertEquals("Product should be add to the cart", "Product is not add to the cart within 100sec");
		}*/
		if(isErrorElementPresent(errorMsg))
		{
			for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
			{
				System.out.println("XXXX"+totalErrorMgs.get(i));
				if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
				{	
					wResult.writeTestResult("User can add the product in to the shopping cart", "User can add the product in to the cart", "Following error found :"+getText(errorMsg), false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-"+writeFileName+"-Add to cart button");
					Assert.assertEquals("User can add the product in to the cart", "Following error found :"+getText(errorMsg));
				}
			}
				
		}
	}

	
	public void wishList() throws Exception
	{	
		if(findElementInXLSheet(getMethodIsExecuted,"Wishlist In Product Detail Page").equalsIgnoreCase("Yes"))
		{	
			
			if(!isElementPresent(wishListLink))
			{
				wResult.writeTestResult("Verify 'add to wishlist' link in 'Product detial' page","Wishlist link should be displayed", "Wishlist link is not present in product detail page", false, writeFileName);
				Assert.assertEquals("Wishlist link should be displayed", "Wishlist link is not present in product detail page");

			}
			else
			{
				String productOnclickValue = getAttribute(wishListLink,"onclick");
			
				System.out.println("productOnclickValue"+productOnclickValue);
				
				if(productOnclickValue.indexOf(productDetailWishlistGACode) >= 0 )
				{
					String gaCode =	productOnclickValue.substring(productOnclickValue.indexOf("dataLayer.push"), productOnclickValue.indexOf("})")+2);
											
					wResult.writeTestResult("Verify product wishlist GA code in details page", "GA Code : "+productDetailWishlistGACode+"\nProduct Name is : "+productName, "GA Code is - "+gaCode, true, writeFileName);
								
				}
				
				else
				{
					wResult.writeTestResult("Verify product wishlist GA code in details page", productDetailWishlistGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue, false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Product details add to cart attribute");
						
				}
			}
			
			
		}
			
	}
	
	public void emailToFriends() throws Exception
	{
		if(!isElementPresent(ProductDetailsEmailToFriendsXpath))
		{
			wResult.writeTestResult("Verify 'email to friends' link in 'Product detial' page","email to friends link should be displayed", "email to friends link is not present in product detail page", false, writeFileName);
			Assert.assertEquals("email to friends link should be displayed", "email to friends link is not present in product detail page");

		}
		else
		{
			String productOnclickValue = getAttribute(ProductDetailsEmailToFriendsXpath,"onclick");
		
			System.out.println("productOnclickValue"+productOnclickValue.indexOf(productDetailEmailToFriendsGACode));
			
			if(productOnclickValue.indexOf(productDetailEmailToFriendsGACode) >= 0 )
			{
				String gaCode =	productOnclickValue.substring(productOnclickValue.indexOf("dataLayer.push"), productOnclickValue.indexOf("})")+2);
										
				wResult.writeTestResult("Verify product email to friends GA code in details page", "GA Code : "+productDetailEmailToFriendsGACode+"\nProduct Name is : "+productName, "GA Code is - "+gaCode, true, writeFileName);
				
			}
			
			else
			{
				wResult.writeTestResult("Verify product email to friends GA code in details page", productDetailEmailToFriendsGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue, false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Product details email to friends attribute");
					
			}
		}
	}
	
	public void headerCheckout() throws Exception
	{
		if(!isElementPresent(ProductDetailsHeaderCheckoutXpath))
		{
			wResult.writeTestResult("Verify 'Header Checkout' link in 'Product detial' page","Header Checkout link should be displayed", "Header Checkout link is not present in product detail page", false, writeFileName);
			Assert.assertEquals("Header Checkout link should be displayed", "Header Checkout link is not present in product detail page");

		}
		else
		{
			String productOnclickValue = getAttribute(ProductDetailsHeaderCheckoutXpath,"onclick");
		
			System.out.println("productOnclickValue"+productOnclickValue.indexOf(productDetailHeaderCheckoutGACode));
			
			if(productOnclickValue.indexOf(productDetailHeaderCheckoutGACode) >= 0 )
			{
				String gaCode =	productOnclickValue.substring(productOnclickValue.indexOf("dataLayer.push"), productOnclickValue.indexOf("})")+2);
										
				wResult.writeTestResult("Verify header checkout GA code in details page", "GA Code : "+productDetailHeaderCheckoutGACode+"\nProduct Name is : "+productName, "GA Code is - "+gaCode, true, writeFileName);
				
			}
			
			else
			{
				wResult.writeTestResult("Verify header checkout GA code in details page", productDetailHeaderCheckoutGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue, false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Product details header checkout attribute");
					
			}
		}
	}
	
	public void addToCart() throws Exception
	{	
		//openPage("http://ryb.sara.netstarter.com.au/little-fawn-long-sleeve-bodysuit-musk");
		if(!isElementPresent(addToCartButton))
		{
			verifyAddToCartButton();
		}
		else
		{
			String productOnclickValue = getAttribute(ProductDetailsAddToCartXpath,"onclick");
		
			System.out.println("productOnclickValue"+productOnclickValue);
			
			if(productOnclickValue.indexOf(productDetailAddToCartGACode) >= 0 )
			{
				String gaCode =	productOnclickValue.substring(productOnclickValue.indexOf("dataLayer.push"), productOnclickValue.indexOf("})")+2);
										
				wResult.writeTestResult("Verify product add to cart GA code in details page", "GA Code : "+productDetailAddToCartGACode+"\nProduct Name is : "+productName, "GA Code is - "+gaCode, true, writeFileName);
				
				
			}
			
			else
			{
				wResult.writeTestResult("Verify product add to cart GA code in details page", productDetailAddToCartGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue, false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Product details add to cart attribute");
					
			}
		}
			
		if(isElementPresent(productSizeClassName))
		{
			addToCartButton1();
		}
		else
		{
			addToCartButton();
		}
	
		
	}
	
	
	/*public void selectProductSizeByClick() throws Exception
	{
		//setLinkXpath = findElementInXLSheet(getParameterXpath,getLinkXpath).trim().replace("{id}", "");
		//getAttribute		
		
		int productSizeBasket = getElementSize(productSizeClassName);
		String sizeXpath = "";
		
		if(productSizeBasket!=0)
		{
			if(productSizeBasket==1)
			{
				System.out.println("yyyy");
				
				sizeXpath = productSizeBlock.replace("{id}", "");
				
				System.out.println("ccc"+sizeXpath);
				
				if(!getAttribute(sizeXpath,"id").isEmpty())
				{
					click(sizeXpath);
				}
				else
				{
					System.out.println("Product is out of stok");
				}
			}
			else
			{
				System.out.println("xxxx"+productSizeBlock);
				sizeXpath = productSizeBlock.replace("{id}", "");
				System.out.println("dddd"+sizeXpath);
				
				System.out.println();
				if(!getAttribute(sizeXpath,"id").isEmpty())
				{
					click(sizeXpath);
				}
				else
				{
					System.out.println("vvfff");
					
					for(int i=2;i<=productSizeBasket;i++)
					{
						sizeXpath = productSizeBlock.replace("{id}", "["+i+"]");
						System.out.println("dddd"+sizeXpath);
						if(!getAttribute(sizeXpath,"id").isEmpty())
						{
							click(sizeXpath);
						}
						else
						{
							System.out.println("product is out of stock");
						}
					}
						
						
				}
			
			}
		}
		else
		{
			System.out.println("No color size busket");
		}
		
	
	}	
	*/
	public void selectProductSizeByClick() throws Exception
	{
		//setLinkXpath = findElementInXLSheet(getParameterXpath,getLinkXpath).trim().replace("{id}", "");
		//getAttribute		
		
		int productSizeBasket = getElementSize(productSizeClassName);
		String sizeXpath = "";
		
		if(productSizeBasket!=0)
		{
			if(productSizeBasket==1)
			{
				sizeXpath = productSizeBlock.replace("{id}", "");
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
				}
				else
				{
					wResult.writeTestResult("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock", false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"- Product is out of stock");
					Assert.assertEquals("Product is out of stock", "Product is out of stock");

				}
			}
			else
			{
				sizeXpath = productSizeBlock.replace("{id}", "");
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
				}
				else
				{
					for(int i=2;i<=productSizeBasket;i++)
					{
						sizeXpath = productSizeBlock.replace("{id}", "["+i+"]");
						
						if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
						{
							click(sizeXpath);
						}
						else
						{
							wResult.writeTestResult("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock", false, writeFileName);
							captureScreenShot(wResult.readTime+"-"+writeFileName+"- Product is out of stock");
							Assert.assertEquals("Product is out of stock", "Product is out of stock");

						}
					}
						
						
				}
			
			}
		}
			
	}

	public void addToCartButton1() throws Exception
	{		
		
		int productSizeBasket = getElementSize(productSizeClassName);
		String sizeXpath = "";
		
		System.out.println("productSizeBasket"+productSizeBasket);
		
		if(productSizeBasket!=0)
		{
			/**
			 *Basket has only one value 
			 */
			if(productSizeBasket==1)
			{
				sizeXpath = productSizeBlock.replace("{id}", "");
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
					
					click(addToCartButton);
					
					Thread.sleep(5000);
						
					/*if(!ajaxCall(ajaxLoader))
					{
						wResult.writeTestResult("Verify product is add to the cart", "Product should be add to the cart", "Product is not add to the cart within 100sec", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify product is add to the cart");
						Assert.assertEquals("Product should be add to the cart", "Product is not add to the cart within 100sec");
					}*/						
					if(isErrorElementPresent(errorMsg))
					{
						wResult.writeTestResult("User can add the product in to the shopping cart", "Product is out of stock", "Following error found :"+getText(errorMsg), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Add to cart button");
						Assert.assertEquals("Product is out of stock", "Following error found :"+getText(errorMsg));
					}
					else
					{
						wResult.writeTestResult("User can add the product in to the shopping cart", "Product should added to shopping cart", "Product is added to shopping cart", true, writeFileName);
						
					}
				}
				else
				{
					wResult.writeTestResult("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock", false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"- Product is out of stock");
					Assert.assertEquals("Product is out of stock", "Product is out of stock");
					}
			}
			else
			{
				sizeXpath = productSizeBlock.replace("{id}", "");
							
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					System.out.println("yyy"+getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue));
					
					click(sizeXpath);
					
					click(addToCartButton);
					
					Thread.sleep(5000);
						
					/*if(!ajaxCall(ajaxLoader))
					{
						wResult.writeTestResult("Verify product is add to the cart", "Product should be add to the cart", "Product is not add to the cart within 100sec", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify product is add to the cart");
						Assert.assertEquals("Product should be add to the cart", "Product is not add to the cart within 100sec");
					}*/						
					if(isErrorElementPresent(errorMsg))
					{
						selectProductFromBasket(productSizeBasket,sizeXpath);
					
					}
					else
					{
						wResult.writeTestResult("User can add the product in to the shopping cart", "Product should added to shopping cart", "Product is added to shopping cart", true, writeFileName);
					
					}
					
				}
				else
				{
					
					selectProductFromBasket(productSizeBasket,productSizeBlock);
												
				}
			
			}
		}
	
	}	
	
	public void selectProductFromBasket(int productSizeBasket,String sizeXpath) throws Exception
	{
		i = 2;
		j = 0;
		
		while(i<=productSizeBasket)
		{
			for(i=2;i<=productSizeBasket;i++)
			{
				sizeXpath = productSizeBlock.replace("{id}", "["+i+"]");
				
				if(getAttribute(sizeXpath,productSizeAttributeValue).indexOf(productSizeclassOutofStockValue) < 0)
				{
					click(sizeXpath);
					
					click(addToCartButton);
					
					Thread.sleep(5000);						
						
					//System.out.println("xxx"+isElementPresent(errorMsg));
					
					if(!isElementPresent(errorMsg))
					{
						i = productSizeBasket+1;
						
						break;												
					}			
					
					
				}
				else
				{
					wResult.writeTestResult("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock", false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"- Product is out of stock");
					Assert.assertEquals("Product is out of stock", "Product is out of stock");

				}
				
				if(i==productSizeBasket)
				{
					wResult.writeTestResult("User can add the product in to the shopping cart", "Product is out of stock", "Product is out of stock", false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"- Product is out of stock");
					Assert.assertEquals("Product is out of stock", "Product is out of stock");

				}
			}
						
		}
	}
	
	
	public void selectProductSizeByDropdown() throws Exception
	{
	
		int productSizeBasket = driver.findElement(By.id("attribute173")).findElements(By.tagName("option")).size();
		
		//int productSizeBasket =0;
	
		if(productSizeBasket>1)
		{
			//new Select(driver.findElement(By.id("attribute173"))).selectByIndex(2)
			
			selectIndex("attribute173_Id",1);
		}
		else
		{
			System.out.println("No color size busket");
		}
		
	
	}	
	
	
	public void addToCartButton2() throws Exception
	{		
		
		int productSizeBasket = getDropdownSize("attribute173_Id","option");
		
		//int productSizeBasket = getElementSize(productSizeClassName);
		String sizeXpath = "";
		System.out.println("productSizeBasket"+productSizeBasket);
		if(productSizeBasket>1)
		{
			//new Select(driver.findElement(By.id("attribute173"))).selectByIndex(2)
		
			selectIndex("attribute173_Id",1);
		
			click(addToCartButton);
		
			Thread.sleep(2000);
			
			/*if(!ajaxCall(ajaxLoader))
			{
				wResult.writeTestResult("Verify product is add to the cart", "Product should be add to the cart", "Product is not add to the cart within 100sec", false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify product is add to the cart");
				Assert.assertEquals("Product should be add to the cart", "Product is not add to the cart within 100sec");
			}*/
			
			while(isErrorElementPresent(errorMsg) && (j<productSizeBasket))
			{					
				for(j=2;j<=productSizeBasket;j++)
				{
					sizeXpath = productSizeBlock.replace("{id}", "["+j+"]");
					System.out.println("dddd"+sizeXpath);
					if(!getAttribute(sizeXpath,"id").isEmpty())
					{
						click(sizeXpath);
					
						click(addToCartButton);
						
						Thread.sleep(2000);
						
						/*if(!ajaxCall(ajaxLoader))
						{
							wResult.writeTestResult("Verify product is add to the cart", "Product should be add to the cart", "Product is not add to the cart within 100sec", false, writeFileName);
							captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify product is add to the cart");
							
						}*/
								
						if(!isElementPresent(errorMsg))
						{
							break;												
						}								
								
					}
					else
					{
						System.out.println("product is out of stock");
					}
				}
			}	
		}

	}
	
	public void viewCart() throws Exception
	{ 
		if(common.totalItemInHeaderCart()==0)
		{
			wResult.writeTestResult("User can add the product in to the shopping cart", "Header Cart should be updated", "Cart is not updated", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"- Cart is not updated");
			Assert.assertEquals("Header Cart should be updated", "Cart is not updated");

		}		
		else if(!isElementPresent(viewCartId))
		{
			wResult.writeTestResult("Verify the my cart is displayed in header", "My cart should be displayed in header", "My cart button is not displayed in the header", false, writeFileName);
			Assert.assertEquals("my cart should be displayed in header", "my cart button is not displayed in the header");
		
		}
		else
		{
			Thread.sleep(2000);
			
			System.out.println("Cart displayed"+isDisplayed(viewCart));
						
			if(!isDisplayed(viewCart))
			{
				click(viewCartId);
				Thread.sleep(2000);
				
				if(!isDisplayed(viewCart))
				{
					wResult.writeTestResult("Verify the my cart is displayed in header", "My cart should be displayed in header", "My cart button is not displayed in the header", false, writeFileName);
					Assert.assertEquals("my cart should be displayed in header", "my cart button is not displayed in the header");
				
				}
				else
				{
					click(viewCart);	
					Thread.sleep(1000);
					
					waitForPageLoad();
				}
				
			}
			else if(isDisplayed(viewCart))
			{
				click(viewCart);	
				Thread.sleep(2000);
				
				waitForPageLoad();
			}
			/*if(isDisplayed(viewCart))
			{
				click(viewCart);	
				Thread.sleep(5000);
			}*/
			
			
			
			else
			{
				wResult.writeTestResult("Verify the my cart is displayed in header", "My cart should be displayed in header", "My cart button is not displayed in the header", false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify the my cart is displayed in header");	
				Assert.assertEquals("my cart should be displayed in header", "my cart button is not displayed in the header");
					
			}
			
			
			
			
		}
		
	}
	
	
	public void titleInshoppingCartPage() throws Exception
	{
		
		if(!getTitle().equalsIgnoreCase(shoppingCartTitle))
		{			
			wResult.writeTestResult("Verify the shopping cart page title", "Expected shopping cart title is "+shoppingCartTitle, "Actual shopping cart title is "+getTitle(), false, writeFileName);
			
		}
		else
		{
			wResult.writeTestResult("Verify the shopping cart page title", "Expected shopping cart title is "+shoppingCartTitle, "Actual shopping cart title is "+getTitle(), true, writeFileName);
		}
		

		
	}
		
	public void productPriceInshoppingCartPage() throws Exception
	{
		if(isElementPresent(productPriceInShoppingCart))
		{
			if(getTagListSize(productPriceInShoppingCart,"p_TagName")>1)
			{
				productPriceInShoppingCartPage 	= 	getText(productPriceInShoppingCart+"/p[2]");
			}
			else
			{
				productPriceInShoppingCartPage = getText(productPriceInShoppingCart);
			}
			System.out.println("productPriceInShoppingCartPage"+productPriceInShoppingCartPage);
			
			
			if(!(productPriceInShoppingCartPage).equalsIgnoreCase(productPrice))
			{
				wResult.writeTestResult("Verify the product price in the shopping cart page ", "Expected product price is "+productPrice + " in the shopping cart page", "Actual product price is "+productPriceInShoppingCartPage+" in the shopping cart page", false, writeFileName);
			
			}
			else
			{
				wResult.writeTestResult("Verify the product price in the shopping cart page ", "Expected product price is "+productPrice + " in the shopping cart page", "Actual product price is "+productPriceInShoppingCartPage+" in the shopping cart page", true, writeFileName);
				
			}
		}
		
	}
	
	public void productNameInshoppingCartPage() throws Exception
	{	
		
		if(isElementPresent(productNameInShoppingCart))
		{
			productNameInShoppingCartPage =getText(productNameInShoppingCart);
			
			if(!productNameInShoppingCartPage.isEmpty())
			{
				if(!productNameInShoppingCartPage.equalsIgnoreCase(productName))
				{
					wResult.writeTestResult("Verify the product name in the shopping cart page", "Expected product name is "+productName+ " in the shopping cart page", "Actual product name is "+productNameInShoppingCartPage+ " in the shopping cart page", false, writeFileName);
					
				}
				else
				{
					wResult.writeTestResult("Verify the product name in the shopping cart page", "Expected product name is "+productName+ " in the shopping cart page", "Actual product name is '"+productNameInShoppingCartPage+ "' in the shopping cart page", true, writeFileName);
					
				}				
		
			}
			else
			{
				wResult.writeTestResult("Verify the product name in the shopping cart page", "Expected product name is "+productName+ " in the shopping cart page", "Xpath is incorrect", true, writeFileName);
				
			}
			
		}

	}
	
	public void productQTYInshoppingCartPage() throws Exception
	{		
		if(isElementPresent(productQTYInShoppingCart))
		{
			productQTYInShoppingCartPage = getText(productQTYInShoppingCart);
			
			if(!productQTYInShoppingCartPage.equalsIgnoreCase(productQTYInDetailPage))
			{
				wResult.writeTestResult("Verify the product QTY in the shopping cart page ", "Expected product QTY is "+productQTYInDetailPage + " in the shopping cart page", "Actual product QTY is "+productQTYInShoppingCartPage+" in the shopping cart page", false, writeFileName);
				
			}
			else
			{
				wResult.writeTestResult("Verify the product QTY in the shopping cart page ", "Expected product QTY is "+productQTYInDetailPage + " in the shopping cart page", "Actual product QTY is "+productQTYInShoppingCartPage+" in the shopping cart page", true, writeFileName);
				
			}

		}
	
	}
	
	
	public void paypalInShoppingCart() throws Exception
	{
		if(!isElementPresent(PaypalCheckoutButtonInShoppingCart))
		{
			wResult.writeTestResult("Verify the paypal button is displayed in shopping cart page", "Paypal button should be displayed in shopping cart page", "Paypal button is not in the correct place or button is not displayed in the shopping cart page", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify the Paypal button is displayed in shopping cart page");		
			Assert.assertEquals("Paypal button should be displayed in shopping cart page", "Paypal button is not in the correct place or button is not displayed in the shopping cart page");
			
		}
		else
		{
			String productOnclickValue = getAttribute(PaypalCheckoutButtonInShoppingCart,"onclick");
			
			System.out.println("productOnclickValue"+productOnclickValue.indexOf(shoppingPaypalCheckoutGACode));
				
			if(productOnclickValue.indexOf(shoppingPaypalCheckoutGACode) >= 0 )
			{
				String gaCode =	productOnclickValue.substring(productOnclickValue.indexOf("dataLayer.push"), productOnclickValue.indexOf("})")+2);
											
				wResult.writeTestResult("Verify paypal button GA code in shopping cart page", "GA Code : "+shoppingPaypalCheckoutGACode+"\nProduct Name is : "+productName, "GA Code is - "+gaCode, true, writeFileName);
					
			}
				
			else
			{
				wResult.writeTestResult("Verify paypal button GA code in shopping cart page", shoppingPaypalCheckoutGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue, false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paypal button attribute");
						
			}
						
		}

	}
	
	
	public void proceedToCheckoutButton() throws Exception
	{
		if(!isElementPresent(proceedToCheckoutButtonInShoppingCart))
		{
			wResult.writeTestResult("Verify the Proceed to checkout button is displayed in shopping cart page", "Proceed to checkout button should be displayed in shopping cart page", "Proceed to checkout button is not in the correct place or button is not displayed in the shopping cart page", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify the Proceed to checkout button is displayed in shopping cart page");		
			Assert.assertEquals("Proceed to checkout button should be displayed in shopping cart page", "Proceed to checkout button is not in the correct place or button is not displayed in the shopping cart page");
			
		}
		else
		{
			String productOnclickValue = getAttribute(proceedToCheckoutButtonInShoppingCart,"onclick");
			
			System.out.println("productOnclickValue"+productOnclickValue.indexOf(shoppingProceedToCheckoutGACode));
				
			if(productOnclickValue.indexOf(shoppingProceedToCheckoutGACode) >= 0 )
			{
				String gaCode =	productOnclickValue.substring(productOnclickValue.indexOf("dataLayer.push"), productOnclickValue.indexOf("})")+2);
											
				wResult.writeTestResult("Verify proceed to checkout button GA code in shopping cart page", "GA Code : "+shoppingProceedToCheckoutGACode+"\nProduct Name is : "+productName, "GA Code is - "+gaCode, true, writeFileName);
					
			}
				
			else
			{
				wResult.writeTestResult("Verify proceed to checkout button GA code in shopping cart page", shoppingProceedToCheckoutGACode+" ,should be displayed", "Following is displayed. "+productOnclickValue, false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-proceed to checkout button attribute");
						
			}
						
			click(proceedToCheckoutButtonInShoppingCart);
			
			waitForPageLoad();					
			
			
			/*if(isErrorElementPresent(errorMsg) && isErrorMsgDisplayed(errorMsg).size()>1)
			{			
				System.out.println("Error"+isErrorMsgDisplayed(errorMsg).size());
				
				for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
				{
					System.out.println("i"+i);
					System.out.println("XXXX"+totalErrorMgs.get(i));
					if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
					{
						wResult.writeTestResult("Verify Checkout page is displayed ", "Checkout page should displayed", "Following error found :"+totalErrorMgs.get(i), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Proceed to checkout button");
						Assert.assertEquals("Checkout page should displayed", "Following error found :"+totalErrorMgs.get(i));
					}
				}
				
			}*/
			
			/*if(!ajaxCall(ajaxLoader))
			{
				wResult.writeTestResult("Verify Checkout page is displayed ", "Checkout page should displayed", "Page is not load within 100sec", false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Proceed to checkout button");
				Assert.assertEquals("Checkout page should displayed", "Page is not load within 100sec");
			}*/
			
		}

	}
	
	
	public void CheckoutAsGuest_withEmail() throws Exception
	{
		//billing_show-hide
		if(isElementPresent(chkCheckoutAsGuestUser))
		{
			click(chkCheckoutAsGuestUser);
			Thread.sleep(1000);
		}
		else
		{
			wResult.writeTestResult("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed", "Checkout 'Guest' continue button is not displayed", false, writeFileName);
			Assert.assertEquals("Billing information screen should be displayed", "Checkout 'Guest' continue button is not displayed");
			
		}
		
		if(!txtBillingLoginEmail.trim().isEmpty())
		{
			if(isElementPresent(txtBillingLoginEmail))
			{
				sendKeys(txtBillingLoginEmail,billingLoginEmail);
				
			}
			else
			{
				wResult.writeTestResult("Verify the 'Email Address' text field displayed for guest user", "'Email Address' text field should be displayed for guest user", "'Email Address' text field is not displayed or xpath is incorrect", false, writeFileName);
				Assert.assertEquals("'Email Address' text field should be displayed for guest user", "'Email Address' text field is not displayed or xpath is incorrect");
				
			}
			
			
			System.out.println("yyy"+getPageSource("function gaEmail()"));
			/**SEO GA CODE **/
			if(isElementPresent(btnCheckoutAsGuestUser))
			{
				
				if(!checkoutAsGuestUserAndHasFunctionInSelectUserType.isEmpty())
				{
					String getFunctionValue = getAttribute(btnCheckoutAsGuestUser,"onclick");
					
					System.out.println("getFunctionValue"+getFunctionValue);
					System.out.println("checkoutAsGuestUserAndHasFunctionInSelectUserType"+checkoutAsGuestUserAndHasFunctionInSelectUserType);
					
					
					
					if(getFunctionValue.equalsIgnoreCase(checkoutAsGuestUserAndHasFunctionInSelectUserType))
					{
						if(getPageSource(checkoutAsGuestSelectUserTypeFunctionValue.trim()))
						{
							System.out.println(getPageContent().indexOf(checkoutAsGuestSelectUserTypeGACode));
							if(getPageContent().indexOf(checkoutAsGuestSelectUserTypeGACode) >= 0)
							{
								wResult.writeTestResult("Verify guest continue button GA code in checkout page", "GA Code : "+checkoutAsGuestSelectUserTypeGACode, "GA Code is - "+checkoutAsGuestSelectUserTypeGACode, true, writeFileName);
											
							}
						}
						else
						{
							wResult.writeTestResult(getPageContent(), checkoutAsGuestSelectUserTypeFunctionValue+" ,function should be displayed", checkoutAsGuestSelectUserTypeFunctionValue+" function not displayed.", false, writeFileName);
								
						}
					}
					else
					{
						wResult.writeTestResult("Verify guest continue button GA code in checkout page", checkoutAsGuestUserAndHasFunctionInSelectUserType+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
						
					}				
					
				}
				
							
				/*if(!checkoutAsMemberUserAndHasFunctionInSelectUserType.isEmpty())
				{
					String getFunctionValue = getAttribute(btnCheckoutAsGuestUser,"onclick");
					
					System.out.println("getFunctionValue"+getFunctionValue);
					System.out.println("checkoutAsGuestUserAndHasFunctionInSelectUserType"+checkoutAsGuestUserAndHasFunctionInSelectUserType);
					if(getFunctionValue.equalsIgnoreCase(checkoutAsGuestUserAndHasFunctionInSelectUserType))
					{
						if(getPageSource(checkoutAsGuestSelectUserTypeFunctionValue))
						{
							System.out.println(getPageContent().indexOf(checkoutAsGuestSelectUserTypeGACode));
							if(getPageContent().indexOf(checkoutAsGuestSelectUserTypeGACode) >= 0)
							{
								wResult.writeTestResult("Verify guest continue button GA code in checkout page", "GA Code : "+checkoutAsGuestSelectUserTypeGACode, "GA Code is - "+checkoutAsGuestSelectUserTypeGACode, true, writeFileName);
											
							}
						}
						else
						{
							wResult.writeTestResult("Verify guest continue button GA code in checkout page", checkoutAsGuestSelectUserTypeFunctionValue+" ,function should be displayed", checkoutAsGuestSelectUserTypeFunctionValue+" function not displayed.", false, writeFileName);
								
						}
					}
					else
					{
						wResult.writeTestResult("Verify guest continue button GA code in checkout page", checkoutAsGuestUserAndHasFunctionInSelectUserType+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
						
					}
					
					
				}
				
				if(!checkoutAsGuestUserAndHasFunctionInSelectUserType.isEmpty())
				{
					String getFunctionValue = getAttribute(btnCheckoutAsGuestUser,"onclick");
					
					System.out.println("getFunctionValue"+getFunctionValue);
					System.out.println("checkoutAsGuestUserAndHasFunctionInSelectUserType"+checkoutAsGuestUserAndHasFunctionInSelectUserType);
					if(getFunctionValue.equalsIgnoreCase(checkoutAsGuestUserAndHasFunctionInSelectUserType))
					{
						if(getPageSource(checkoutAsGuestSelectUserTypeFunctionValue))
						{
							System.out.println(getPageContent().indexOf(checkoutAsGuestSelectUserTypeGACode));
							if(getPageContent().indexOf(checkoutAsGuestSelectUserTypeGACode) >= 0)
							{
								wResult.writeTestResult("Verify guest continue button GA code in checkout page", "GA Code : "+checkoutAsGuestSelectUserTypeGACode, "GA Code is - "+checkoutAsGuestSelectUserTypeGACode, true, writeFileName);
											
							}
						}
						else
						{
							wResult.writeTestResult("Verify guest continue button GA code in checkout page", checkoutAsGuestSelectUserTypeFunctionValue+" ,function should be displayed", checkoutAsGuestSelectUserTypeFunctionValue+" function not displayed.", false, writeFileName);
								
						}
					}
					else
					{
						wResult.writeTestResult("Verify guest continue button GA code in checkout page", checkoutAsGuestUserAndHasFunctionInSelectUserType+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
						
					}
					
					
				}
				*/
				
				click(btnCheckoutAsGuestUser);
				
				if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes"))
				{
					if(!getAttribute(chkoutAjaxUserType,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
					{
						if(checkoutAjaxRequest(chkoutAjaxUserType))
						{
							wResult.writeTestResult("Verify billing information page", "Billing information screen should be displayed", "Billing infomation page is not load within 100sec", false, writeFileName);
							Assert.assertEquals("Billing information screen should be displayed", "Billing infomation page is not load within 100sec");
							captureScreenShot(wResult.readTime+"-"+writeFileName+"-Billing info page");				

						}
						
						
					}
				}
				else
				{
					Thread.sleep(2000);
				}
				
				
				/*if(isDisplayed(chkoutAjaxUserType))
				{
					if(!ajaxCall(chkoutAjaxUserType))
					{
						wResult.writeTestResult("Verify billing information page", "Billing information screen should be displayed", "Billing infomation page is not load within 100sec", false, writeFileName);
						Assert.assertEquals("Billing information screen should be displayed", "Billing infomation page is not load within 100sec");
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Billing info page");				
					}
				}				
				else
				{
					Thread.sleep(2000);
				}*/
				
			}
			else
			{
				wResult.writeTestResult("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed", "Checkout 'Guest' continue button is not displayed", false, writeFileName);
				Assert.assertEquals("Billing information screen should be displayed", "Checkout 'Guest' continue button is not displayed");
				
			}
			/*if(validationBillingGuestEmailRequired.equalsIgnoreCase(validationMsgBillingGuestEmailRequired))
			{
				wResult.writeTestResult("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed", "Email address is required in checkout page.", false, writeFileName);
				Assert.assertEquals("Billing information screen should be displayed", "Email address is required in checkout page.");
				
			}
			else if(validationBillingGuestEmailInvalid.equalsIgnoreCase(validationMsgBillingGuestEmailInvalid))
			{
				wResult.writeTestResult("Verify user clicks continue button with correct email address in checkout page", "Billing information screen should be displayed", "Email address is invalid in checkout page.", false, writeFileName);
				Assert.assertEquals("Billing information screen should be displayed", "Email address is invalid in checkout page.");
				
			}
			*/
			
		}	
		else
		{
			wResult.writeTestResult("Verify the 'Email Address' text field displayed for guest user", "'Email Address' text field should be displayed for guest user", "'Email Address' text field xpath is not in the property file.", false, writeFileName);
			Assert.assertEquals("'Email Address' text field should be displayed for guest user", "'Email Address' text field xpath is not in the property file.");
			
		}
		
		//Billing infor seo
		
		if(!checkoutSameAddressHasFunction.isEmpty())
		{
			String getFunctionValue = getAttribute(btnBillingInfoContinue,"onclick");
			
			System.out.println("getFunctionValue"+getFunctionValue);
			
			if(getFunctionValue.equalsIgnoreCase(checkoutSameAddressHasFunction))
			{
				if(getPageSource(checkoutSameAddressFunctionValue))
				{
					System.out.println(getPageContent().indexOf(checkoutSameAddressGACode));
					
					if(getPageContent().indexOf(checkoutSameAddressGACode) >= 0)
					{
						wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are same", "GA Code : "+checkoutSameAddressGACode, "GA Code is - "+checkoutSameAddressGACode, true, writeFileName);
									
					}
				}
				else
				{
					wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are same", checkoutSameAddressFunctionValue+" ,function should be displayed", checkoutSameAddressFunctionValue+" function not displayed.", false, writeFileName);
						
				}
			}
			else
			{
				wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are same", checkoutSameAddressHasFunction+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
				
			}				
			
		}
		
		if(!checkoutDifferentAddressHasFunction.isEmpty())
		{
			String getFunctionValue = getAttribute(btnShippingInfoContinue,"onclick");
			
			System.out.println("getFunctionValue"+getFunctionValue);
			
			if(getFunctionValue.equalsIgnoreCase(checkoutDifferentAddressHasFunction))
			{
				if(getPageSource(checkoutDifferentAddressFunctionValue))
				{
					System.out.println(getPageContent().indexOf(checkoutDifferentAddressGACode));
					
					if(getPageContent().indexOf(checkoutDifferentAddressGACode) >= 0)
					{
						wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are different", "GA Code : "+checkoutDifferentAddressGACode, "GA Code is - "+checkoutDifferentAddressGACode, true, writeFileName);
									
					}
				}
				else
				{
					wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are different", checkoutDifferentAddressFunctionValue+" ,function should be displayed", checkoutDifferentAddressFunctionValue+" function not displayed.", false, writeFileName);
						
				}
			}
			else
			{
				wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are different", checkoutDifferentAddressHasFunction+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
				
			}				
			
		}
		
		//pickup store
		
		if(!checkoutPickUpInStoreHasFunction.isEmpty())
		{
			String getFunctionValue = getAttribute("shipping_store_continue_button_Id","onclick");
			
			System.out.println("getFunctionValue"+getFunctionValue);
			
			if(getFunctionValue.equalsIgnoreCase(checkoutPickUpInStoreHasFunction))
			{
				if(getPageSource(checkoutPickUpInStoreFunctionValue))
				{
					System.out.println(getPageContent().indexOf(checkoutPickUpInStoreGACode));
					
					if(getPageContent().indexOf(checkoutPickUpInStoreGACode) >= 0)
					{
						wResult.writeTestResult("Verify GA code in billing infro continue button when user select pick up store", "GA Code : "+checkoutPickUpInStoreGACode, "GA Code is - "+checkoutPickUpInStoreGACode, true, writeFileName);
									
					}
				}
				else
				{
					wResult.writeTestResult("Verify GA code in billing infro continue button when user select pick up store", checkoutPickUpInStoreFunctionValue+" ,function should be displayed", checkoutPickUpInStoreFunctionValue+" function not displayed.", false, writeFileName);
						
				}
			}
			else
			{
				wResult.writeTestResult("Verify GA code in billing infro continue button when user select pick up store", checkoutPickUpInStoreHasFunction+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
				
			}				
			
		}
		

	}
	
	public void CheckoutAsGuest_withoutEmail() throws Exception
	{
		if(isElementPresent("login:guest_Id"))
		{
			click("login:guest_Id");
			System.out.println("xxxxx");
			
			
			
			if(isElementPresent(btnCheckoutAsGuestUser))
			{
				click(btnCheckoutAsGuestUser);
				Thread.sleep(2000);
			}
			else 
			{
				wResult.writeTestResult("Verify user clicks continue button ", "Billing information screen should be displayed", "Continue button is not displayed in 'checkout method' screen ", false, writeFileName);
				Assert.assertEquals("Billing information screen should be displayed", "Continue button is not displayed in 'checkout method' screen ");
				
			}
			
		}
		else
		{
			wResult.writeTestResult("Verify user can select checkout method", "user can select checkout method as 'guest'", "Guest method is not displayed ", false, writeFileName);
			Assert.assertEquals("user can select checkout method as 'guest'", "Guest method is not displayed ");
			
		}
		
		
	}
	
	public void billingInfoNewAddress() throws Exception
	{
		if(!selectNewAddress_DropDown.trim().isEmpty())
		{
			if(isElementPresent(selectNewAddress_DropDown))
			{
				System.out.println("selectNewAddress_DropDown"+selectNewAddress_DropDown);
				click(selectNewAddress_DropDown);
				
				Thread.sleep(1000);
				
				click("New Address_Link");
						
				wResult.writeTestResult("Verify member can select 'New Address' in billing information page", "User can select 'New Address' in billing information page", "User can select 'New Address' in billing information page", true, writeFileName);
				
				//newBillingAddressFieldsValues();
				billingInfoPage();
			}
			else
			{
				billingInfoPage();
			}
			
		}
		else
		{
			billingInfoPage();
		}
	}
	
	
	public void newBillingAddressFieldsValues() throws Exception
	{
		/*if(selenium.isElementPresent("id=billing-address-select")){
			selenium.select("id=billing-address-select", "label=New Address");
			Thread.sleep(TIME/2);
		}*/
	
		if(!txtBillingFirstName.trim().isEmpty())
		{
			if(isElementPresent(txtBillingFirstName))
			{
				if(getAttribute((txtBillingFirstName),"value").isEmpty())
				{
					wResult.writeTestResult("Verify the 'First Name' text field displayed in billing information page", "First Name text field should be displayed in billing information page", "First Name text field is displayed in billing information page", true, writeFileName);
					
					sendKeys((txtBillingFirstName),billingFirstName);					
				}
				else
				{
					wResult.writeTestResult("Verify the 'First Name' text field displayed in billing information page", "Empty 'First Name' text field should be displayed in billing information page", "Following text ("+getAttribute((txtBillingFirstName),"value")+ " ) field is displayed in billing information page", false, writeFileName);		
					
					sendKeys((txtBillingFirstName),billingFirstName);
				}
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'First Name' text field displayed in billing information page", "First Name text field should be displayed in billing information page", "First Name text field is not displayed in billing information page", false, writeFileName);
				
			}
		}
		if(!txtBillingLastName.trim().isEmpty())
		{
			if(isElementPresent(txtBillingLastName))
			{
				if(getAttribute(txtBillingLastName,"value").isEmpty())
				{
					wResult.writeTestResult("Verify the 'Last Name' text field displayed in billing information page", "Empty Last Name text field should be displayed in billing information page", "Last Name text field is displayed in billing information page", true, writeFileName);
					
					sendKeys(txtBillingLastName,billingLastName);
					
				}
				else
				{
					wResult.writeTestResult("Verify the 'Last Name' text field displayed in billing information page", "Empty 'Last Name' text field should be displayed in billing information page", "Following text ("+getAttribute((txtBillingLastName),"value")+ " ) field is displayed in billing information page", false, writeFileName);
				}
						
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Last Name' text field displayed in billing information page", "Last Name text field should be displayed in billing information page", "Last Name text field is not displayed in billing information page", false, writeFileName);
							
			}
		}
		if(findElementInXLSheet(getMethodIsExecuted,"Checkout As Guest Without Email").equalsIgnoreCase("Yes") && !isElementPresent(linkLogOut))
		{
			if(!txtBillingEmail.trim().isEmpty())
			{
				if(isElementPresent(txtBillingEmail))
				{
					if(getAttribute(txtBillingEmail,"value").isEmpty())
					{
						wResult.writeTestResult("Verify the 'Email Address' text field displayed in billing information page", "Empty 'Email Address' text field should be displayed in billing information page", "'Email Address' text field is displayed", true, writeFileName);
						
						sendKeys(txtBillingEmail,billingLoginEmail);
					}
					else
					{
						wResult.writeTestResult("Verify the 'Email Address' text field displayed in billing information page", "Empty 'Email Address' text field should be displayed in billing information page", "Following text ("+getAttribute((txtBillingEmail),"value")+ ") is displayed in billing infor page", false, writeFileName);
						
						sendKeys(txtBillingEmail,billingLoginEmail);
					}
								
				}
				else
				{
					wResult.writeTestResult("Verify the 'Email Address' text field displayed in billing information page", "'Email Address' text field should be displayed in billing information page", "'Email Address' text field is not displayed or xpath is incorrect", false, writeFileName);
					Assert.assertEquals("'Email Address' text field should be displayed in billing information page", "'Email Address' text field is not displayed or xpath is incorrect");
					
				}
			}
		}
		
		if(!txtBillingTelephone.trim().isEmpty())
		{
			if(isElementPresent(txtBillingTelephone))
			{
				if(getAttribute(txtBillingTelephone,"value").isEmpty())
				{
					wResult.writeTestResult("Verify the 'Contact Number' text field displayed in billing information page", "Empty Contact Number text field should be displayed in billing information page", "Contact Number field is displayed in billing information page", true, writeFileName);
					
					sendKeys(txtBillingTelephone,billingTelephone);
				}
				else
				{
					wResult.writeTestResult("Verify the 'Contact Number' text field displayed in billing information page", "Empty Contact Number text field should be displayed in billing information page", "Following text ("+getAttribute((txtBillingTelephone),"value")+ ") is displayed in billing infor page", false, writeFileName);
					
					sendKeys(txtBillingTelephone,billingTelephone);
				}
				
			}
			else
			{				
				wResult.writeTestResult("Verify the 'Contact Number' text field displayed in billing information page", "Contact Number text field should be displayed in billing information page", "Contact Number text field is not displayed in billing information page", false, writeFileName);
			}
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Find Billing Address").equalsIgnoreCase("Yes"))
		{
			click(linkaddressHint);
		}
		
		if(!txtBillingstreet1.trim().isEmpty())
		{
			if(isElementPresent(txtBillingstreet1))
			{
				if(getAttribute(txtBillingstreet1,"value").isEmpty())
				{
					wResult.writeTestResult("Verify the 'Street' text field displayed in billing information page", "Empty Street text field should be displayed in billing information page", "Street field is displayed in billing information page", true, writeFileName);
				
					sendKeys(txtBillingstreet1,billingstreet1);
				}
				else
				{
					wResult.writeTestResult("Verify the 'Street' text field displayed in billing information page", "Empty Street text field should be displayed in billing information page", "Following text ("+getAttribute((txtBillingstreet1),"value")+ ") is displayed in billing infor page", false, writeFileName);
				
					sendKeys(txtBillingstreet1,billingstreet1);
				}
				
			}
			else
			{				
				wResult.writeTestResult("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in billing information page", "Street text field is not displayed in billing information page", false, writeFileName);
				
			}
		}
		if(!txtBillingCity.trim().isEmpty())
		{
			if(isElementPresent(txtBillingCity))
			{
				if(getAttribute(txtBillingCity,"value").isEmpty())
				{
					wResult.writeTestResult("Verify the 'City' text field displayed in billing information page", "Empty City text field should be displayed in billing information page", "City field is displayed in billing information page", true, writeFileName);
					
					sendKeys(txtBillingCity,billingCity);
				}
				else
				{
					wResult.writeTestResult("Verify the 'City' text field displayed in billing information page", "Empty City text field should be displayed in billing information page", "Following text ("+getAttribute((txtBillingCity),"value")+ ") is displayed in billing infor page", false, writeFileName);
			
					sendKeys(txtBillingCity,billingCity);
				}
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'City' text field displayed in billing information page", "City text field should be displayed in billing information page", "City text field is not displayed in billing information page", false, writeFileName);
				
			}
		}
		
				
		if(!txtBillingPostcode.trim().isEmpty())
		{
			if(isElementPresent(txtBillingPostcode))
			{
				if(getAttribute(txtBillingPostcode,"value").isEmpty())
				{
					wResult.writeTestResult("Verify the 'Post Code' text field displayed in billing information page", "Empty Post Code text field should be displayed in billing information page", "Post Code field is displayed in billing information page", true, writeFileName);
					
					sendKeys(txtBillingPostcode,billingPostcode);
				}
				else
				{
					wResult.writeTestResult("Verify the 'Post Code' text field displayed in billing information page", "Empty Post Code text field should be displayed in billing information page", "Following text ("+getAttribute((txtBillingPostcode),"value")+ ") is displayed in billing infor page", false, writeFileName);
					
					sendKeys(txtBillingPostcode,billingPostcode);
				}
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Post Code' text field displayed in billing information page", "Post Code text field should be displayed in billing information page", "Post Code text field is not displayed in billing information page", false, writeFileName);
			}
		}	
		if(!selectBillingCountry_Id.trim().isEmpty())
		{
			if(isElementPresent(selectBillingCountry_Id))
			{
				wResult.writeTestResult("Verify the 'Country' dropdown field displayed in billing information page", "Country dropdown field should be displayed in billing information page", "Country field is displayed in billing information page", true, writeFileName);
				
				selectText(selectBillingCountry_Id,billingCountry_Id);
			}
			else
			{				
				wResult.writeTestResult("Verify the 'Country' dropdown field displayed in billing information page", "Country dropdown field should be displayed in billing information page", "Country dropdown field is not displayed in billing information page", false, writeFileName);
				
				selectText(selectBillingCountry_Id,billingCountry_Id);
			}
		}
		if(!selectBillingRegion_Id.trim().isEmpty())
		{
			if(isElementPresent(selectBillingRegion_Id))
			{
				if(getAttribute(selectBillingRegion_Id,"value").isEmpty())
				{
					wResult.writeTestResult("Verify the 'Region' dropdown field displayed in billing information page", "Empty Region dropdown field should be displayed in billing information page", "Region field is displayed in billing information page", true, writeFileName);
				
					selectText(selectBillingRegion_Id,billingRegion_Id);
				}
				else
				{
					wResult.writeTestResult("Verify the 'Region' dropdown field displayed in billing information page", "Empty Region dropdown field should be displayed in billing information page", "Following text ("+getAttribute((selectBillingRegion_Id),"value")+ ") is displayed in billing infor page", false, writeFileName);
				
					selectText(selectBillingRegion_Id,billingRegion_Id);
				}
							
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", "Region dropdown field is not displayed in billing information page", false, writeFileName);
				
			}
		}
		
		if(!selectBillingRegion_Id_DropDown.trim().isEmpty())
		{
			if(isElementPresent(selectBillingRegion_Id_DropDown))
			{
				System.out.println("xxxx"+getAttribute(selectBillingRegion_Id_DropDown,"value"));
				if(getAttribute(selectBillingRegion_Id_DropDown,"value").equalsIgnoreCase("Please select region, state or province"))
				{
					wResult.writeTestResult("Verify the 'Region' dropdown field displayed in billing information page", "Empty Region dropdown field should be displayed in billing information page", "Region field is displayed in billing information page", true, writeFileName);
					
					click(selectBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
									
					click(billingRegion_Id+"_Link");
				}
				else
				{
					wResult.writeTestResult("Verify the 'Region' dropdown field displayed in billing information page", "Empty Region dropdown field should be displayed in billing information page", "Following text ("+getAttribute((selectBillingRegion_Id_DropDown),"value")+ ") is displayed in billing infor page", false, writeFileName);
									
					click(selectBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
									
					click(billingRegion_Id+"_Link");
				}
							
			}
			else
			{				
				wResult.writeTestResult("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", "Region dropdown field is not displayed in billing information page", false, writeFileName);
				
			}
		}
		

	}	
		
	public void billingInfoPage() throws Exception
	{
		/*if(selenium.isElementPresent("id=billing-address-select")){
			selenium.selectText("id=billing-address-select", "label=New Address");
			Thread.sleep(TIME/2);
		}*/
		
			
		
		if(!isEnabled(btnBillingInfoContinue))
		{		
				wResult.writeTestResult("Verify billing information page", "User can enter enter data in billing ionformation page", "Billing information page is not load correctly", false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify billing information page");
				Assert.assertEquals("User can enter enter data in billing ionformation page", "Billing information page is not load correctly");
				
			
		}
		else
		{
			if(!txtBillingFirstName.trim().isEmpty())
			{
				if(isElementPresent(txtBillingFirstName))
				{
					sendKeys((txtBillingFirstName),billingFirstName);
					
				}
				else
				{
					
					wResult.writeTestResult("Verify the 'First Name' text field displayed in billing information page", "First Name text field should be displayed in billing information page", "First Name text field is not displayed in billing information page", false, writeFileName);
					
				}
			}
			if(!txtBillingLastName.trim().isEmpty())
			{
				if(isElementPresent(txtBillingLastName))
				{
					sendKeys(txtBillingLastName,billingLastName);
					
				}
				else
				{
					
					wResult.writeTestResult("Verify the 'Last Name' text field displayed in billing information page", "Last Name text field should be displayed in billing information page", "Last Name text field is not displayed in billing information page", false, writeFileName);
								
				}
			}
			if(findElementInXLSheet(getMethodIsExecuted,"Checkout As Guest Without Email").equalsIgnoreCase("Yes") && !isElementPresent(linkLogOut))
			{
				if(!txtBillingEmail.trim().isEmpty())
				{
					if(isElementPresent(txtBillingEmail))
					{
						sendKeys(txtBillingEmail,billingLoginEmail);
					}
					else
					{
						wResult.writeTestResult("Verify the 'Email Address' text field displayed in billing information page", "'Email Address' text field should be displayed in billing information page", "'Email Address' text field is not displayed or xpath is incorrect", false, writeFileName);
						Assert.assertEquals("'Email Address' text field should be displayed in billing information page", "'Email Address' text field is not displayed or xpath is incorrect");
						
					}
				}
			}
			
			if(!txtBillingTelephone.trim().isEmpty())
			{
				if(isElementPresent(txtBillingTelephone))
				{
					sendKeys(txtBillingTelephone,billingTelephone);
								
					
				}
				else
				{
					
					wResult.writeTestResult("Verify the 'Contact Number' text field displayed in billing information page", "Contact Number text field should be displayed in billing information page", "Contact Number text field is not displayed in billing information page", false, writeFileName);
					
				}
			}
			
			if(findElementInXLSheet(getMethodIsExecuted,"Find Billing Address").equalsIgnoreCase("Yes"))
			{
				click(linkaddressHint);
			}
			
			if(!txtBillingPostcode.trim().isEmpty())
			{
				if(isElementPresent(txtBillingPostcode))
				{
					sendKeys(txtBillingPostcode,"Invalid");
					
					Thread.sleep(2000);					
					
					if(isElementPresent("//*[@id='billing_postcode_clear']_Xpath"))
					{
						click("//*[@id='billing_postcode_clear']_Xpath");
					}
					
					sendKeys(txtBillingPostcode,billingPostcode);
					
					Thread.sleep(3000);
					
						
				}
				else
				{
					
					wResult.writeTestResult("Verify the 'Post Code' text field displayed in billing information page", "Post Code text field should be displayed in billing information page", "Post Code text field is not displayed in billing information page", false, writeFileName);
				}
			}
			
			if(!txtBillingstreet1.trim().isEmpty())
			{
				if(isElementPresent(txtBillingstreet1))
				{
					sendKeys(txtBillingstreet1,billingstreet1);
										
					
				}
				else
				{
					
					wResult.writeTestResult("Verify the 'Street' text field displayed in billing information page", "Street text field should be displayed in billing information page", "Street text field is not displayed in billing information page", false, writeFileName);
					
				}
			}
			if(!txtBillingCity.trim().isEmpty())
			{
				if(isElementPresent(txtBillingCity))
				{
					sendKeys(txtBillingCity,billingCity);
					
				}
				else
				{
					
					wResult.writeTestResult("Verify the 'City' text field displayed in billing information page", "City text field should be displayed in billing information page", "City text field is not displayed in billing information page", false, writeFileName);
					
				}
			}		
					
				
			if(!selectBillingCountry_Id.trim().isEmpty())
			{
				if(isElementPresent(selectBillingCountry_Id))
				{
					selectText(selectBillingCountry_Id,billingCountry_Id);
				}
				else
				{				
					wResult.writeTestResult("Verify the 'Country' dropdown field displayed in billing information page", "Country dropdown field should be displayed in billing information page", "Country dropdown field is not displayed in billing information page", false, writeFileName);
					
				}
			}
			if(!selectBillingRegion_Id.trim().isEmpty())
			{
				if(isElementPresent(selectBillingRegion_Id))
				{
					selectText(selectBillingRegion_Id,billingRegion_Id);		
							
					
				}
				else
				{
					
					wResult.writeTestResult("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", "Region dropdown field is not displayed in billing information page", false, writeFileName);
					
				}
			}
			
			if(!selectBillingRegion_Id_DropDown.trim().isEmpty())
			{
				if(isElementPresent(selectBillingRegion_Id_DropDown))
				{
					
					click(selectBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					System.out.println("billingRegion_Id"+billingRegion_Id);
					click(billingRegion_Id+"_Link");
							
					
				}
				else if(isElementPresent(selectMemberBillingRegion_Id_DropDown))
				{
					System.out.println("selectMemberBillingRegion_Id_DropDown"+selectMemberBillingRegion_Id_DropDown);
					click(selectMemberBillingRegion_Id_DropDown);
					
					Thread.sleep(1000);
					System.out.println("billingRegion_Id"+billingRegion_Id);
					click(billingRegion_Id+"_Link");
							
				
				}
				else
				{
					
					wResult.writeTestResult("Verify the 'Region' dropdown field displayed in billing information page", "Region dropdown field should be displayed in billing information page", "Region dropdown field is not displayed in billing information page", false, writeFileName);
					
				}
			}			
		}

		
		

	}	
	
	public void createAccountLater() throws Exception
	{
		if(!billingCreateAccount.trim().isEmpty())
		{
			if(isElementPresent(billingCreateAccount))
			{
				click(billingCreateAccount);		
						
				wResult.writeTestResult("Verify the 'Create an account and save your address for next time'  checkbox displayed in billing information page", 
						"Create an account and save your address for next time field should be displayed in billing information page", 
						"Create an account and save your address for next time  field is displayed in billing information page", true, writeFileName);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Create an account and save your address for next time'  checkbox displayed in billing information page", 
						"Create an account and save your address for next time field should be displayed in billing information page", 
						"Create an account and save your address for next time  field is not displayed in billing information page", true, writeFileName);
				
				
			}
		}
	}
	
	
	public void billingInfoConButton() throws Exception
	{
		
		if(isElementPresent(btnBillingInfoContinue))
		{	
			//SEO
			
			if(!checkoutSameAddressHasFunction.isEmpty())
			{
				String getFunctionValue = getAttribute(btnBillingInfoContinue,"onclick");
				
				System.out.println("getFunctionValue"+getFunctionValue);
				
				if(getFunctionValue.equalsIgnoreCase(checkoutSameAddressHasFunction))
				{
					if(getPageSource(checkoutSameAddressFunctionValue))
					{
						System.out.println(getPageContent().indexOf(checkoutSameAddressGACode));
						
						if(getPageContent().indexOf(checkoutSameAddressGACode) >= 0)
						{
							wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are same", "GA Code : "+checkoutSameAddressGACode, "GA Code is - "+checkoutSameAddressGACode, true, writeFileName);
										
						}
					}
					else
					{
						wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are same", checkoutSameAddressFunctionValue+" ,function should be displayed", checkoutSameAddressFunctionValue+" function not displayed.", false, writeFileName);
							
					}
				}
				else
				{
					wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are same", checkoutSameAddressHasFunction+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
					
				}				
				
			}
			

			click(btnBillingInfoContinue);	
			
			//please-wait-wrapper
				
			/*if(isDisplayed(chkoutAjaxBilling))
			{
				if(!ajaxCall(chkoutAjaxBilling))
				{
					wResult.writeTestResult("Verify user clicks continue button with correct data in billing information page", "Shipping information screen should be displayed", "Page is not load within 100sec", false, writeFileName);
					Assert.assertEquals("Shipping information screen should be displayed", "Shipping information screen is not displayed");
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Billing info continue button");				
				}
			}
			else
			{
				Thread.sleep(5000);
			}*/
			
			
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes"))
			{
				System.out.println("chkoutAjaxBilling"+getAttribute(chkoutAjaxBilling,chkoutAjaxClassType));
				
				if(!getAttribute(chkoutAjaxBilling,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxBilling))
					{
						wResult.writeTestResult("Verify user clicks continue button with correct data in billing information page", "Shipping information screen should be displayed", "Page is not load within 100sec", false, writeFileName);
						Assert.assertEquals("Shipping information screen should be displayed", "Shipping information screen is not displayed");
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Billing info continue button");				
					
					}
				}
			}
			else
			{
				Thread.sleep(5000);
			}
			
			
			submitValidation();
			
			/*else
			{
				verifyErrorMsgIBillingInfor();	
				
				submitValidation();
			}*/

		   
		}
		else
		{
			wResult.writeTestResult("Verify user clicks continue button with correct data in billing information page", "Shipping information screen should be displayed", "Shipping information screen is not displayed", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Billing info continue button");
			Assert.assertEquals("Shipping information screen should be displayed", "Shipping information screen is not displayed");
			
		}
	}
	
	public void chkDifferentAddress() throws Exception
	{
		if(isElementPresent(chkDifferentShipAddress))
		{
			if(!checkoutSameAddressHasFunction.isEmpty())
			{
				String getFunctionValue = getAttribute(btnBillingInfoContinue,"onclick");
				
				System.out.println("getFunctionValue"+getFunctionValue);
				
				if(getFunctionValue.equalsIgnoreCase(checkoutSameAddressHasFunction))
				{
					if(getPageSource(checkoutSameAddressFunctionValue))
					{
						System.out.println(getPageContent().indexOf(checkoutSameAddressGACode));
						
						if(getPageContent().indexOf(checkoutSameAddressGACode) >= 0)
						{
							wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are same", "GA Code : "+checkoutSameAddressGACode, "GA Code is - "+checkoutSameAddressGACode, true, writeFileName);
										
						}
					}
					else
					{
						wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are same", checkoutSameAddressFunctionValue+" ,function should be displayed", checkoutSameAddressFunctionValue+" function not displayed.", false, writeFileName);
							
					}
				}
				else
				{
					wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are same", checkoutSameAddressHasFunction+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
					
				}				
				
			}
			

			click(chkDifferentShipAddress);

		}
		else
		{
			wResult.writeTestResult("Verify user can select different shipping address in billing information page", "Shipping information screen should be displayed", "Shipping information screen is not displayed", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-select different ship address");
			Assert.assertEquals("Shipping information screen should be displayed", "Shipping information screen is not displayed");
			
		}
	}
	
	
	public void shippingInfoPage() throws Exception
	{
		
		
		if(!txtShippingFirstName.trim().isEmpty())
		{
			if(isElementPresent(txtShippingFirstName))
			{
				sendKeys((txtShippingFirstName),shippingFirstName);
								
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'First Name' text field displayed in shipping information page", "First Name text field should be displayed in shipping information page", "First Name text field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		if(!txtShippingLastName.trim().isEmpty())
		{
			if(isElementPresent(txtShippingLastName))
			{
				sendKeys(txtShippingLastName,shippingLastName);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Last Name' text field displayed in shipping information page", "Last Name text field should be displayed in shipping information page", "Last Name text field is not displayed in shipping information page", false, writeFileName);
							
			}
		}
				
		if(!txtShippingTelephone.trim().isEmpty())
		{
			if(isElementPresent(txtShippingTelephone))
			{
				sendKeys(txtShippingTelephone,shippingTelephone);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Contact Number' text field displayed in shipping information page", "Contact Number text field should be displayed in shipping information page", "Contact Number text field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Find Shipping Address").equalsIgnoreCase("Yes"))
		{
			click(linkaddressHint);
		}
		
		if(!txtShippingPostcode.trim().isEmpty())
		{
			if(isElementPresent(txtShippingPostcode))
			{
				sendKeys(txtShippingPostcode,"Invalid");
				
				Thread.sleep(2000);					
				
				if(isElementPresent("//*[@id='shipping_postcode_clear']_Xpath"))
				{
					click("//*[@id='shipping_postcode_clear']_Xpath");
				}
				
				sendKeys(txtShippingPostcode,shippingPostcode);
				
				Thread.sleep(3000);
				
						
				
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Post Code' text field displayed in shipping information page", "Post Code text field should be displayed in shipping information page", "Post Code text field is not displayed in shipping information page", false, writeFileName);
			}
		}
		
		if(!txtShippingstreet1.trim().isEmpty())
		{
			if(isElementPresent(txtShippingstreet1))
			{
				sendKeys(txtShippingstreet1,shippingstreet1);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Street' text field displayed in shipping information page", "Street text field should be displayed in shipping information page", "Street text field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		if(!txtShippingCity.trim().isEmpty())
		{
			if(isElementPresent(txtShippingCity))
			{
				sendKeys(txtShippingCity,shippingCity);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'City' text field displayed in shipping information page", "City text field should be displayed in shipping information page", "City text field is not displayed in shipping information page", false, writeFileName);
				
			}
		}		
				
			
		if(!selectShippingCountry_Id.trim().isEmpty())
		{
			if(isElementPresent(selectShippingCountry_Id))
			{
				selectText(selectShippingCountry_Id,shippingCountry_Id);
			}
			else
			{				
				wResult.writeTestResult("Verify the 'Country' dropdown field displayed in shipping information page", "Country dropdown field should be displayed in shipping information page", "Country dropdown field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		if(!selectShippingRegion_Id.trim().isEmpty())
		{
			if(isElementPresent(selectShippingRegion_Id))
			{
				selectText(selectShippingRegion_Id,shippingRegion_Id);		
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Region' dropdown field displayed in shipping information page", "Region dropdown field should be displayed in shipping information page", "Region dropdown field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		
		if(!selectShippingRegion_Id_DropDown.trim().isEmpty())
		{
			if(isElementPresent(selectShippingRegion_Id_DropDown))
			{
				click(selectShippingRegion_Id_DropDown);
				
				Thread.sleep(1000);
				System.out.println("shippingRegion_Id"+shippingRegion_Id);
				click(shippingRegion_Id+"_Link");
						
			}
			else if(isElementPresent(selectMemberShippingRegion_Id_DropDown))
			{
				System.out.println("selectMemberShippingRegion_Id_DropDown"+selectMemberShippingRegion_Id_DropDown);
				click(selectMemberShippingRegion_Id_DropDown);
				
				Thread.sleep(1000);
				System.out.println("shippingRegion_Id"+shippingRegion_Id);
				click(shippingRegion_Id+"_Link");
			
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Region' dropdown field displayed in shipping information page", "Region dropdown field should be displayed in shipping information page", "Region dropdown field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		
		

	}	
	
	public void shippingInfoConButton() throws Exception
	{
		
		if(isElementPresent(btnShippingInfoContinue))
		{	
			
			if(!checkoutDifferentAddressHasFunction.isEmpty())
			{
				String getFunctionValue = getAttribute(btnShippingInfoContinue,"onclick");
				
				System.out.println("getFunctionValue"+getFunctionValue);
				
				if(getFunctionValue.equalsIgnoreCase(checkoutDifferentAddressHasFunction))
				{
					if(getPageSource(checkoutDifferentAddressFunctionValue))
					{
						System.out.println(getPageContent().indexOf(checkoutDifferentAddressGACode));
						
						if(getPageContent().indexOf(checkoutDifferentAddressGACode) >= 0)
						{
							wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are different", "GA Code : "+checkoutDifferentAddressGACode, "GA Code is - "+checkoutDifferentAddressGACode, true, writeFileName);
										
						}
					}
					else
					{
						wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are different", checkoutDifferentAddressFunctionValue+" ,function should be displayed", checkoutDifferentAddressFunctionValue+" function not displayed.", false, writeFileName);
							
					}
				}
				else
				{
					wResult.writeTestResult("Verify GA code in billing infro continue button when user select both address are different", checkoutDifferentAddressHasFunction+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
					
				}				
				
			}
			
			//pickup store
			
			if(!checkoutPickUpInStoreHasFunction.isEmpty())
			{
				String getFunctionValue = getAttribute(btnShippingInfoContinue,"onclick");
				
				System.out.println("getFunctionValue"+getFunctionValue);
				
				if(getFunctionValue.equalsIgnoreCase(checkoutPickUpInStoreHasFunction))
				{
					if(getPageSource(checkoutPickUpInStoreFunctionValue))
					{
						System.out.println(getPageContent().indexOf(checkoutPickUpInStoreGACode));
						
						if(getPageContent().indexOf(checkoutPickUpInStoreGACode) >= 0)
						{
							wResult.writeTestResult("Verify GA code in billing infro continue button when user select pick up store", "GA Code : "+checkoutPickUpInStoreGACode, "GA Code is - "+checkoutPickUpInStoreGACode, true, writeFileName);
										
						}
					}
					else
					{
						wResult.writeTestResult("Verify GA code in billing infro continue button when user select pick up store", checkoutPickUpInStoreFunctionValue+" ,function should be displayed", checkoutPickUpInStoreFunctionValue+" function not displayed.", false, writeFileName);
							
					}
				}
				else
				{
					wResult.writeTestResult("Verify GA code in billing infro continue button when user select pick up store", checkoutPickUpInStoreHasFunction+" ,function should be displayed", "Following function is displayed. "+getFunctionValue, false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-guest continue button attribute");
					
				}				
				
			}
			
			click(btnShippingInfoContinue);	
		
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes"))
			{
				if(!getAttribute(chkoutAjaxShipping,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxShipping))
					{
						wResult.writeTestResult("Verify user clicks continue button with correct data in shipping information page", "Payment information screen should be displayed", "Page is not load within 100sec", false, writeFileName);
						Assert.assertEquals("Payment information screen should be displayed", "Payment information screen is not displayed");
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Shipping info continue button");				
	
					}
				}
			}
			else
			{
				Thread.sleep(5000);
			}

			verifyErrorMsgIShippingInfor();	
			
			submitShiipingValidation();
		}
		else
		{
			wResult.writeTestResult("Verify user clicks continue button with correct data in billing information page", "Shipping information screen should be displayed", "Shipping information screen is not displayed", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Billing info continue button");
			Assert.assertEquals("Shipping information screen should be displayed", "Shipping information screen is not displayed");
			
		}
	}
	
	
	
	
	public void selectShippingMethod() throws Exception
	{
		int totalShippingMethods = getTagListSize(getTotalShippingMethods,getShippingMethodTag);
		
		if(totalShippingMethods>0)
		{
			wResult.writeTestResult("Verify the shipping methods displayed in shipping method page", "Shipping methods should be displayed in shipping method page", totalShippingMethods+" Shipping method(s) displayed in shipping method page", true, writeFileName);
			click(selectShippingMethod);	
		}
		else
		{
			wResult.writeTestResult("Verify the shipping methods displayed in shipping method page", "Shipping methods should be displayed in shipping method page", "Shipping methods not displayed in shipping method page", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify the shipping methods displayed in shipping method pag");			
			Assert.assertEquals("Shipping methods should be displayed in shipping method page", "Shipping methods not displayed in shipping method page");
			
		}
	}
	
	public void shippingMethodConButton() throws Exception
	{				
		if(isElementPresent(btnShippingMethodContinue))
		{			
			wResult.writeTestResult("Verify the continue button is displayed in shipping method page", "Continue button should be displayed in shipping method page", "Continue button is displayed in shipping method page", true, writeFileName);
			
			click(btnShippingMethodContinue);			
			
			/*if(isDisplayed(chkoutAjaxGiftCard))
			{
				//chkoutAjaxShipping
				System.out.println("1"+isDisplayed(chkoutAjaxBilling));
				System.out.println("2"+isDisplayed(chkoutAjaxUserType));
				System.out.println("3"+isDisplayed(chkoutAjaxShipping));
				System.out.println("4"+isDisplayed(chkoutAjaxGiftCard));
				System.out.println("5"+isDisplayed(chkoutAjaxPayment));
				
				if(!ajaxCall(chkoutAjaxGiftCard))
				{
					wResult.writeTestResult("Verify user clicks continue button with correct data in billing information page", "Payment information screen should be displayed", "Page is not load within 100sec", false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Shopping info continue button");
					Assert.assertEquals("Payment information screen should be displayed", "Payment information screen is not displayed");
									
				}	
			}			
			else
			{
				Thread.sleep(5000);
			}*/
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes"))
			{
				if(!getAttribute(chkoutAjaxGiftCard,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxGiftCard))
					{
						wResult.writeTestResult("Verify user clicks continue button with correct data in billing information page", "Payment information screen should be displayed", "Page is not load within 100sec", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Shopping info continue button");
						Assert.assertEquals("Payment information screen should be displayed", "Payment information screen is not displayed");
						
					}
				}
			}
			else
			{
				Thread.sleep(5000);
			}
							
		}
		else
		{
			wResult.writeTestResult("Verify the continue button is displayed in shipping method page", "Continue button should be displayed in shipping method page", "Continue button is not displayed in shipping method page", false, writeFileName);
			Assert.assertEquals("Continue button should be displayed in shipping method page", "Continue button is not displayed in shipping method page");
			
		}
	}
	
	
	/*public void shippingInfoPage() throws Exception
	{
	
		if(!txtShoppingFirstName.trim().isEmpty())
		{
			if(isElementPresent(txtShoppingFirstName))
			{
				sendKeys((txtShoppingFirstName),shoppingFirstName);
								
				wResult.writeTestResult("Verify the 'First Name' text field displayed in shopping information page", "First Name text field should be displayed in shopping information page", "First Name text field is displayed in shopping information page", true, writeFileName);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'First Name' text field displayed in shopping information page", "First Name text field should be displayed in shopping information page", "First Name text field is not displayed in shopping information page", false, writeFileName);
				
			}
		}
		if(!txtShoppingLastName.trim().isEmpty())
		{
			if(isElementPresent(txtShoppingLastName))
			{
				sendKeys(txtShoppingLastName,shoppingLastName);
								
				wResult.writeTestResult("Verify the 'Last Name' text field displayed in shopping information page", "Last Name text field should be displayed in shopping information page", "Last Name text field is displayed in shopping information page", true, writeFileName);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Last Name' text field displayed in shopping information page", "Last Name text field should be displayed in shopping information page", "Last Name text field is not displayed in shopping information page", false, writeFileName);
							
			}
		}
		if(!txtShoppingTelephone.trim().isEmpty())
		{
			if(isElementPresent(txtShoppingTelephone))
			{
				sendKeys(txtShoppingTelephone,shoppingTelephone);
							
				wResult.writeTestResult("Verify the 'Contact Number' text field displayed in shopping information page", "Contact Number text field should be displayed in shopping information page", "Contact Number field is displayed in shopping information page", true, writeFileName);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Contact Number' text field displayed in shopping information page", "Contact Number text field should be displayed in shopping information page", "Contact Number text field is not displayed in shopping information page", false, writeFileName);
				
			}
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Find shopping Address").equalsIgnoreCase("Yes"))
		{
			click(linkaddressHint);
		}
		
		if(!txtShoppingstreet1.trim().isEmpty())
		{
			if(isElementPresent(txtShoppingstreet1))
			{
				sendKeys(txtShoppingstreet1,shoppingstreet1);
									
				wResult.writeTestResult("Verify the 'Street' text field displayed in shopping information page", "Street text field should be displayed in shopping information page", "Street field is displayed in shopping information page", true, writeFileName);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Street' text field displayed in shopping information page", "Street text field should be displayed in shopping information page", "Street text field is not displayed in shopping information page", false, writeFileName);
				
			}
		}
		if(!txtShoppingCity.trim().isEmpty())
		{
			if(isElementPresent(txtShoppingCity))
			{
				sendKeys(txtShoppingCity,shoppingCity);
						
				wResult.writeTestResult("Verify the 'City' text field displayed in shopping information page", "City text field should be displayed in shopping information page", "City field is displayed in shopping information page", true, writeFileName);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'City' text field displayed in shopping information page", "City text field should be displayed in shopping information page", "City text field is not displayed in shopping information page", false, writeFileName);
				
			}
		}
		if(!selectshoppingRegion_Id.trim().isEmpty())
		{
			if(isElementPresent(selectshoppingRegion_Id))
			{
				selectText(selectshoppingRegion_Id,shoppingRegion_Id);
						
				wResult.writeTestResult("Verify the 'Region' dropdown field displayed in shopping information page", "Region dropdown field should be displayed in shopping information page", "Region field is displayed in shopping information page", true, writeFileName);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Region' dropdown field displayed in shopping information page", "Region dropdown field should be displayed in shopping information page", "Region dropdown field is not displayed in shopping information page", false, writeFileName);
				
			}
		}
				
		if(!txtShoppingPostcode.trim().isEmpty())
		{
			if(isElementPresent(txtShoppingPostcode))
			{
				sendKeys(txtShoppingPostcode,shoppingPostcode);
			
				wResult.writeTestResult("Verify the 'Post Code' text field displayed in shopping information page", "Post Code text field should be displayed in shopping information page", "Post Code field is displayed in shopping information page", true, writeFileName);
				
			}
			else
			{
				
				wResult.writeTestResult("Verify the 'Post Code' text field displayed in shopping information page", "Post Code text field should be displayed in shopping information page", "Post Code text field is not displayed in shopping information page", false, writeFileName);
			}
		}	
		if(!selectCountry_Id.trim().isEmpty())
		{
			if(isElementPresent(selectCountry_Id))
			{
				selectText(selectCountry_Id,shoppingCountry_Id);
		
				wResult.writeTestResult("Verify the 'Country' dropdown field displayed in shopping information page", "Country dropdown field should be displayed in shopping information page", "Country field is displayed in shopping information page", true, writeFileName);
			}
			else
			{				
				wResult.writeTestResult("Verify the 'Country' dropdown field displayed in shopping information page", "Country dropdown field should be displayed in shopping information page", "Country dropdown field is not displayed in shopping information page", false, writeFileName);
				
			}
		}	

	}
	
	*/	
	public void paymentMethodIsCC() throws Exception
	{
		if(isElementPresent(chkPaymentIsCC))
		{
			click(chkPaymentIsCC);
			wResult.writeTestResult("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", "User can select payment type is credit card in payment information page", true, writeFileName);	
			
		}
		else
		{
			wResult.writeTestResult("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", "Credit card option is not displayed or credit card xpath is incorrect in payment information page", false, writeFileName);	
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Select payment type");
			Assert.assertEquals("User can select the payment type in payment information page", "Credit card option is not displayed or credit card xpath is incorrect in payment information page");
		
		}
		
	}
	
	public void paymentMethodIsPaypal() throws Exception
	{
		if(isElementPresent(chkPaymentIsPaypal))
		{
			click(chkPaymentIsPaypal);
			
			wResult.writeTestResult("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", "User can select payment type is paypal in payment information page", true, writeFileName);	
			
		}
		else
		{
			wResult.writeTestResult("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", "Paypal option is not displayed or paypal xpath is incorrect in payment information page", false, writeFileName);	
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Select payment type");
			Assert.assertEquals("User can select the payment type in payment information page", "Paypal option is not displayed or paypal xpath is incorrect in payment information page");
		
		}
		
	}
	
	public void fillCreditCardDetails() throws Exception
	{	
		if(findElementInXLSheet(getMethodIsExecuted,"Select DataCash Iframe").equalsIgnoreCase("Yes"))
		{	
			Thread.sleep(10000);
			switchToFrame("datacash_hcc_iframe_block");
			Thread.sleep(1000);
		}
		else if(!isElementPresent(btnContinuePaymentScreen))
		{
			Thread.sleep(10000);
		}
		if(!txtCCName.trim().isEmpty())
		{
			if(isElementPresent(txtCCName))
			{
				sendKeys(txtCCName,cCName);
					
				wResult.writeTestResult("Verify the 'Credit Card Name' text field displayed in payment information page", "'Credit card Name' text field should be displayed in payment information page", "'Credit Card Name' field is displayed in payment information page", true, writeFileName);
					
			}
			else
			{
				wResult.writeTestResult("Verify the 'Credit Card Name' text field displayed in payment information page", "'Credit card Name' text field should be displayed in payment information page", "'Credit Card Name' field is not displayed in payment information page", false, writeFileName);	
									
			}
			
		}
		if(!txtCCNumber.trim().isEmpty())
		{
			if(isElementPresent(txtCCNumber))
			{
				sendKeys(txtCCNumber,cCNumber);
					
				wResult.writeTestResult("Verify the 'Credit Card Number' text field displayed in payment information page", "'Credit Card Number' text field should be displayed in payment information page", "'Credit Card Number' field is displayed in payment information page", true, writeFileName);
					
			}
			else
			{
				wResult.writeTestResult("Verify the 'Credit Card Number' text field displayed in payment information page", "'Credit Card Number' text field should be displayed in payment information page", "'Credit Card Number' field is not displayed in payment information page", false, writeFileName);	
									
			}
			
		}
		if(!selectCCType.trim().isEmpty())
		{
			if(isElementPresent(selectCCType))
			{
				selectText(selectCCType,cCType);
					
				wResult.writeTestResult("Verify the 'Credit Card Type' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", "'Credit Card Type' field is displayed in payment information page", true, writeFileName);
					
			}
			else
			{
				wResult.writeTestResult("Verify the 'Credit Card Type' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", "'Credit Card Type' field is not displayed in payment information page", false, writeFileName);	
									
			}
			
		}
		
		if(!txtCCCvs.trim().isEmpty())
		{
			if(isElementPresent(txtCCCvs))
			{
				sendKeys(txtCCCvs,cCCvs);
					
				wResult.writeTestResult("Verify the 'Credit Card CVC' text field displayed in payment information page", "'Credit Card CVC' text field should be displayed in payment information page", "'Credit Card CVC' field is displayed in payment information page", true, writeFileName);
					
			}
			else
			{
				wResult.writeTestResult("Verify the 'Credit Card CVC' text field displayed in payment information page", "'Credit Card CVC' text field should be displayed in payment information page", "'Credit Card CVC' field is not displayed in payment information page", false, writeFileName);	
									
			}
			
		}
		if(!selectCCMonth.trim().isEmpty())
		{
			if(isElementPresent(selectCCMonth))
			{
				selectText(selectCCMonth,cCMonth);
					
				wResult.writeTestResult("Verify the 'Credit Card Expire Month' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", "'Credit Card Expire Month' field is displayed in payment information page", true, writeFileName);
					
			}
			else
			{
				wResult.writeTestResult("Verify the 'Credit Card Expire Month' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", "'Credit Card Expire Month' field is not displayed in payment information page", false, writeFileName);	
									
			}
			
		}
		
		if(!selectCCYear.trim().isEmpty())
		{
			if(isElementPresent(selectCCYear))
			{
				selectText(selectCCYear,cCYear);
					
				wResult.writeTestResult("Verify the 'Credit Card Expire Year' field displayed in payment information page", "'Credit Card Expire Year' field should be displayed in payment information page", "'Credit Card Expire Year' field is displayed in payment information page", true, writeFileName);
					
			}
			else
			{
				wResult.writeTestResult("Verify the 'Credit Card Expire Year' field displayed in payment information page", "'Credit Card Expire Year' field should be displayed in payment information page", "'Credit Card Expire Year' field is not displayed in payment information page", false, writeFileName);	
									
			}
			
		}
		
		if(!selectCCTypeDropDown.trim().isEmpty())
		{
			if(isElementPresent(selectCCTypeDropDown))
			{
				System.out.println("selectCCType"+selectCCTypeDropDown);
				click(selectCCTypeDropDown);
				
				Thread.sleep(1000);
				
				System.out.println("selectCCType"+selectCCType);
				
				click(cCType+"_Link");
						
				wResult.writeTestResult("Verify the 'Credit Card Type' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", "'Credit Card Type' field is displayed in payment information page", true, writeFileName);
				
			}
			else
			{
				wResult.writeTestResult("Verify the 'Credit Card Type' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", "'Credit Card Type' field is not displayed in payment information page", false, writeFileName);	
									
			}
		}
		if(!selectCCMonthDropDown.trim().isEmpty())
		{
			if(isElementPresent(selectCCMonthDropDown))
			{
				System.out.println("selectCCMonth"+selectCCMonthDropDown);
				click(selectCCMonthDropDown);
				
				Thread.sleep(1000);
				System.out.println("selectCCMonth"+selectCCMonth);
				click(cCMonth+"_Link");
						
				wResult.writeTestResult("Verify the 'Credit Card Expire Month' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", "'Credit Card Expire Month' field is displayed in payment information page", true, writeFileName);
				
			}
			else
			{
				wResult.writeTestResult("Verify the 'Credit Card Expire Month' field displayed in payment information page", "'Credit Card Expire Month' field should be displayed in payment information page", "'Credit Card Expire Month' field is not displayed in payment information page", false, writeFileName);	
									
			}
		}
		if(!selectCCYearDropDown.trim().isEmpty())
		{
			if(isElementPresent(selectCCYearDropDown))
			{
				System.out.println("selectCCYear"+selectCCYearDropDown);
				click(selectCCYearDropDown);
				
				Thread.sleep(1000);
				System.out.println("selectCCYear"+selectCCYear);
				click(cCYear+"_Link");
						
				wResult.writeTestResult("Verify the 'Credit Card Expire Year' field displayed in payment information page", "'Credit Card Expire Year' field should be displayed in payment information page", "'Credit Card Expire Year' field is displayed in payment information page", true, writeFileName);
				
			}
			else
			{
				wResult.writeTestResult("Verify the 'Credit Card Expire Year' field displayed in payment information page", "'Credit Card Expire Year' field should be displayed in payment information page", "'Credit Card Expire Year' field is not displayed in payment information page", false, writeFileName);	
									
			}
		}		
	}
	
	public void selectPaymentTeamsAndCondition() throws Exception
	{
		if(isElementPresent(chkTeamsAndCondtion))
		{
			click(chkTeamsAndCondtion);
			
			wResult.writeTestResult("Verify the 'Teams and condition' field displayed in payment information page", "'Teams and condition' should be displayed in payment information page", "'Teams and condition' is displayed and user can tick the 'Teams and condition' in payment information page", true, writeFileName);	
			
		}
		else
		{
			wResult.writeTestResult("Verify the 'Teams and condition' field displayed in payment information page", "'Teams and condition' should be displayed in payment information page", "'Teams and condition' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
			Assert.assertEquals("'Teams and condition' should be displayed in payment information page", "'Teams and condition' is not displayed or xpath is incorrect in payment information page");
		
		}
	}
	
	public void credit_card_payment_submitButton() throws Exception
	{
		System.out.println("1"+isDisplayed(chkoutAjaxBilling));
		System.out.println("2"+isDisplayed(chkoutAjaxUserType));
		System.out.println("3"+isDisplayed(chkoutAjaxShipping));
		System.out.println("4"+isDisplayed(chkoutAjaxGiftCard));
		System.out.println("5"+isDisplayed(chkoutAjaxPayment));
		
		/*if(isDisplayed(chkoutAjaxGiftCard))
		{
			if(!ajaxCall(chkoutAjaxGiftCard))
			{
				wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
				Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
							
			}
		}*/
		
		if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes"))
		{
			if(!getAttribute(chkoutAjaxGiftCard,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
			{
				if(!checkoutAjaxRequest(chkoutAjaxGiftCard))
				{
					wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
					Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
				
				}
			}
		}
		else
		{
			Thread.sleep(2000);
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Select DataCash Iframe").equalsIgnoreCase("Yes"))
		{
			if(isElementPresent("continue_Id"))
			{
				click("continue_Id");

				wResult.writeTestResult("Verify the 'Continue button' in the payment screen", "'Continue button' should be displayed in payment information page", "'Continue button' is displayed in payment information page", true, writeFileName);

				if(isErrorDisplayed("//*[@class='error']_Xpath"))
				{
					wResult.writeTestResult("Verify the 'Credit card validation'in payment information page", "'Order review page' should be displayed", "Following error(s) found '"+getText("//*[@class='error']_Xpath"), false, writeFileName);	
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
					Assert.assertEquals("'Order review page' should be displayed", "Following error(s) found '"+getText("//*[@class='error']_Xpath"));
			
				}


				if(findElementInXLSheet(getMethodIsExecuted,"Select DataCash Iframe").equalsIgnoreCase("Yes"))
				{
					backToDefaultPage();
				}
				
				
				/*if(isDisplayed(ajaxLoader))
				{
					Thread.sleep(5000);
					
					verifyErrorMsgInOrderConfirmPage();
					
					if(!ajaxCall(ajaxLoader))
					{
						wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
						Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
									
					}
				}
				
				else
				{
					Thread.sleep(5000);
				}
				*/
				
				if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes"))
				{
					if(!getAttribute(chkoutAjaxPayment,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
					{
						if(!checkoutAjaxRequest(chkoutAjaxPayment))
						{
							wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
							captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
							Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
						
						}
					}
				}
				else
				{
					Thread.sleep(5000);
				}
				
				verifyErrorMsgInOrderConfirmPage();
				

			}
			
			else
			{
				wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
				Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
										
			}
		}
		else if(isElementPresent(btnContinuePaymentScreen))
		{
			Thread.sleep(2000);
			
			click(btnContinuePaymentScreen);
			
			wResult.writeTestResult("Verify the 'Continue button' in the payment screen", "'Continue button' should be displayed in payment information page", "'Continue button' is displayed in payment information page", true, writeFileName);
			
			/*if(isDisplayed(chkoutAjaxPayment))
			{
				if(!ajaxCall(chkoutAjaxPayment))
				{
					wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
					Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
								
				}
			}
			
			else
			{
				
				Thread.sleep(5000);
				
				
			}*/
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes"))
			{
				if(!getAttribute(chkoutAjaxPayment,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxPayment))
					{
						wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
						Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");

					}
				}
			}
			else
			{
				Thread.sleep(5000);
			}
		}
		else
		{
			wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
			Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
									
		}
	}
	
	
	public void payment_submitButton() throws Exception
	{
		if(isElementPresent(btnContinuePaymentScreen))
		{
			click(btnContinuePaymentScreen);
			
			wResult.writeTestResult("Verify the 'Continue button' in the payment screen", "'Continue button' should be displayed in payment information page", "'Continue button' is displayed in payment information page", true, writeFileName);
			
			/*if(isDisplayed(chkoutAjaxPayment))
			{
				Thread.sleep(5000);
				
				verifyErrorMsgInOrderConfirmPage();				
				
				if(!ajaxCall(chkoutAjaxPayment))
				{
					wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
					Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
								
				}
			}
			
			else
			{
				Thread.sleep(5000);
			}*/
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes"))
			{
				if(!getAttribute(chkoutAjaxPayment,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxPayment))
					{
						wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
						Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
					
					}
				}
			}
			else
			{
				Thread.sleep(5000);
			}
			
			verifyErrorMsgInOrderConfirmPage();		
			
			
			
		}
		else
		{
			wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
			Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
									
		}
	}
	
	public void verifyPaypalAccount() throws Exception
	{
		if(!(isElementPresent(txtPaypalEmail) && isElementPresent(txtPaypalPassword) && isElementPresent(btnPaypalSubmit)))
		{
			Thread.sleep(5000);
		}
		
		if(isElementPresent("loadLogin_Id"))
		{
			click("loadLogin_Id");
		}
				
		if((isElementPresent(txtPaypalEmail) && isElementPresent(txtPaypalPassword) && isElementPresent(btnPaypalSubmit)))
		{
			wResult.writeTestResult("Verify paypal page", "Paypal detail page should displayed", "Paypal detail page is displayed", true, writeFileName);	
						
			sendKeys(txtPaypalEmail,paypalEmail);
			
			sendKeys(txtPaypalPassword,paypalPassword);
			
			click(btnPaypalSubmit);	
			
			paypalValidation();	
			
			if(!isElementPresent(btnPaypalContinue))
			{
				Thread.sleep(5000);
			}
			
			if(isElementPresent(btnPaypalContinue))
			{
				click(btnPaypalContinue);
					
				wResult.writeTestResult("Verify paypal login", "User can navigate to paypal review page", "User can navigate to paypal review page", true, writeFileName);	
				
			}
			else
			{
				wResult.writeTestResult("Verify paypal page", "Paypal detail page should displayed", "Paypal detail page is not displayed within timeout period.page url is "+getCurrentUrl(), false, writeFileName);	
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paypal continue page");
				Assert.assertEquals("Paypal detail page should displayed", "Paypal detail page is not displayed within timeout period.page url is "+getCurrentUrl());
				
			}
			
			if(isElementPresent("//*[@id='doneInfo']/ul/li/a_Xpath"))
			{
				click("//*[@id='doneInfo']/ul/li/a_Xpath");
						
			}
			
			
		}
		else
		{
			wResult.writeTestResult("Verify paypal login page", "Paypal login page should be displayed", "Paypal login page is not displayed or paypal login fields are not displayed.page url is "+getCurrentUrl(), false, writeFileName);	
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paypal login page");
			Assert.assertEquals("Paypal login page should be displayed","Paypal login page is not displayed or paypal login fields are not displayed.page url is "+getCurrentUrl());
			
		}
	}
	
	public void paypalValidation() throws Exception
	{
		if(isElementPresent("//p[@class='group error']_Xpath"))
		{
			wResult.writeTestResult("Verify paypal login details", "User can loing to the paypal account", "Following error is displayed "+getText("//p[@class='group error']_Xpath"), false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paypal login");
			Assert.assertEquals("User can loing to the paypal account", "Following error is displayed "+getText("//p[@class='group error']_Xpath"));
		}		
	}
	
	public void verifyProductPriceInOrderReview() throws Exception
	{		
		if(!getText(productPriceXpathInOrderReviewPage).equalsIgnoreCase(productPrice)){
			
			wResult.writeTestResult("Verify the price of the selected product is displaying in order review page", "Expected product price is "+productPrice +" in order review page", "Actual product price is "+getText(productPriceXpathInOrderReviewPage)+" in order review page", false, writeFileName);
		
		}
		else
		{
			wResult.writeTestResult("Verify the price of the selected product is displaying in order review page", "Expected product price is "+productPrice+" in order review page" , "Actual product price is "+getText(productPriceXpathInOrderReviewPage)+" in order review page", true, writeFileName);
		
		}

	}
	
	public void verifyProductNameInOrderReview() throws Exception
	{		
		if(!getText(productNameXpathOrderReviewPage).equalsIgnoreCase(productName)){
			
			wResult.writeTestResult("Verify the name of the selected product is displaying in order review page", "Expected product Name is "+productName +" in order review page", "Actual product name is "+getText(productNameXpathOrderReviewPage)+" in order review page", false, writeFileName);
			
		}
		else
		{
			wResult.writeTestResult("Verify the name of the selected product is displaying in order review page", "Expected product Name is "+productName+" in order review page" , "Actual product name is "+getText(productNameXpathOrderReviewPage)+" in order review page", true, writeFileName);
					
		}

	}
	
	public void verifyProductPriceInPaypalReview() throws Exception
	{		
		if(!getText(productPriceXpathInPaypalReviewPage).equalsIgnoreCase(productPrice)){
			
			wResult.writeTestResult("Verify the price of the selected product is displaying in paypal review page", "Expected product price is "+productPrice +" in paypal review page", "Actual product price is "+getText(productPriceXpathInPaypalReviewPage)+" in paypal review page", false, writeFileName);
		
		}
		else
		{
			wResult.writeTestResult("Verify the price of the selected product is displaying in paypal review page", "Expected product price is "+productPrice+" in paypal review page" , "Actual product price is "+getText(productPriceXpathInPaypalReviewPage)+" in paypal review page", true, writeFileName);
		
		}

	}
	
	public void verifyProductNameInPaypalReview() throws Exception
	{		
		if(!getText(productNameXpathPaypalReviewPage).equalsIgnoreCase(productName)){
			
			wResult.writeTestResult("Verify the name of the selected product is displaying in paypal review page", "Expected product Name is "+productName +" in paypal review page", "Actual product name is "+getText(productNameXpathPaypalReviewPage)+" in paypal review page", false, writeFileName);
			
		}
		else
		{
			wResult.writeTestResult("Verify the name of the selected product is displaying in paypal review page", "Expected product Name is "+productName+" in paypal review page" , "Actual product name is "+getText(productNameXpathPaypalReviewPage)+" in paypal review page", true, writeFileName);
					
		}

	}
	
	public void continuePaypalReview() throws Exception
	{
		if(!isElementPresent(btnsubmitPaypalReviewPage))
		{
			wResult.writeTestResult("Verify 'Complete Order' button is displaying in paypal review page", "'Complete Order' button should displayed in paypal review page", "'Complete Order' button is not displayed in paypal review page or button locator is incorrect", false, writeFileName);
			Assert.assertEquals("'Complete Order' button should displayed in paypal review page", "'Complete Order' button is not displayed in paypal review page or button locator is incorrect");
			
		}
		else
		{
			click(btnsubmitPaypalReviewPage);
			wResult.writeTestResult("Verify 'Complete Order' button is displaying in paypal review page", "'Complete Order' button should displayed in paypal review page", "'Complete Order' button is displayed in paypal review page", true, writeFileName);
			
			Thread.sleep(5000);
		}		

	}
	
	public void billingAddressInReviewPage() throws Exception
	{
		if(!isElementPresent(billingAddressInReviewPage))
		{
			wResult.writeTestResult("Verify the billing address is displaying in order review page", "Billing address should displayed in order review page", "Billing address is not displayed  or billing address locator is incorrect in order review page", false, writeFileName);
			
		}
		else
		{
			getBillingAddressInReviewPage = getText(billingAddressInReviewPage);
			
			if(getBillingAddressInReviewPage.indexOf(billingFirstName)>-1)
			{
				
			}
			
			
		}
		
		
		System.out.println("getBillingAddressInReviewPage"+getBillingAddressInReviewPage);
	}
	
	public void shippingAddressInReviewPage() throws Exception
	{
		getShippingAddressInReviewPage = getText(shippingAddressInReviewPage);
		System.out.println("getShippingAddressInReviewPage"+getShippingAddressInReviewPage);
	}
	//billingAddressInReviewPage
	
	public void continueOrderReview() throws Exception
	{
		if(!isElementPresent(orderReviewContinueButton))
		{
			wResult.writeTestResult("Verify 'Complete Order' button is displaying in order review page", "'Complete Order' button should displayed in order review page", "'Complete Order' button is not displayed in order review page or button locator is incorrect", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify 'Complete Order' button is displaying in order review page");
			Assert.assertEquals("'Complete Order' button should displayed in order review page", "'Complete Order' button is not displayed in order review page or button locator is incorrect");
			
		}
		else
		{
			click(orderReviewContinueButton);
			
			waitForPageLoad();
		}		

	}
	
	public void verifyErrorMsgInOrderConfirmPage() throws Exception
	{
		String alert = "";
		System.out.println("aleart presetnt CONFIRMATION"+isAlertPresent());
		
		if(isAlertPresent())
		{
			
			alert =getAlert();
			//System.out.println("aleart presetnt3"+isAlertPresent());
			System.out.println("Payment is fail."+alert );
			//sendSMS.sendSMS("Payment is fail."+alert );
			//CreateIssue.CreateIssueJira("Verify 'Order confirmation' Page","Following error found :"+alert);
			wResult.writeTestResult("Verify 'Order confirmation' Page", "'Order Confirmation' page should be displayed", "Following error found :"+alert, false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Payment fail");
			
			//acceptAlert();
			
			Assert.assertEquals("'Order Confirmation' page should be displayed", "Following error found :"+alert);

		}

		
		if(isErrorDisplayed(errorMsg))
		{
			for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
			{
				System.out.println("XXXX"+totalErrorMgs.get(i));
				if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
				{	
					wResult.writeTestResult("Verify 'Order confirmation' Page", "'Order Confirmation' page should be displayed", "Following error found :"+getText(errorMsg), false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Payment fail");
					//sendSMS.sendSMS("Payment is fail."+getText(errorMsg) );
					
					Assert.assertEquals("'Order Confirmation' page should be displayed", "Following error found :"+getText(errorMsg));
				}
			}
		}

			
	}
	
	/*public void creditCardError() throws Exception
	{
				
		if(isErrorDisplayed(errorMsg))
		{
			for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
			{
				System.out.println("XXXX"+totalErrorMgs.get(i));
				if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
				{	
					wResult.writeTestResult("Verify 'Order confirmation' Page", "'Order Confirmation' page should be displayed", "Following error found :"+getText(errorMsg), false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Payment fail");
					Assert.assertEquals("'Order Confirmation' page should be displayed", "Following error found :"+getText(errorMsg));
				}
			}
		}

			
	}
	*/
	
	
	public void verifyErrorMsgIBillingInfor() throws Exception
	{
		String alert = "";
		System.out.println("aleart billing infor"+isAlertPresent());
		
		if(isAlertPresent())
		{
			alert =getAlert();
			System.out.println("aleart billing infor"+isAlertPresent());
			wResult.writeTestResult("Verify 'Shipping infor' Page", "'Shipping infor' page should be displayed", "Following error found :"+alert, false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Billing info fail");
			acceptAlert();
			Assert.assertEquals("'Shipping infor' page should be displayed", "Following error found :"+alert);

		}

		
		if(isDisplayed(errorMsg))
		{
			for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
			{
				System.out.println("XXXX"+totalErrorMgs.get(i));
				if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
				{	
					wResult.writeTestResult("Verify 'Shipping infor' Page", "'Shipping infor' page should be displayed", "Following error found :"+getText(errorMsg), false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Billing info fail");
					Assert.assertEquals("'Shipping infor' page should be displayed", "Following error found :"+getText(errorMsg));
				}
			}
		}
		
		

			
	}
	
	public void verifyErrorMsgIShippingInfor() throws Exception
	{
		String alert = "";
		//System.out.println("aleart presetnt2"+isAlertPresent());
		
		System.out.println("aleart presetnt2"+isAlertPresent());
		
		if(isAlertPresent())
		{
			alert =getAlert();
			System.out.println("aleart presetnt3"+isAlertPresent());
			wResult.writeTestResult("Verify 'Order confirmation' Page", "'Order Confirmation' page should be displayed", "Following error found :"+alert, false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Payment fail");
			//acceptAlert();
			Assert.assertEquals("'Order Confirmation' page should be displayed", "Following error found :"+alert);

		}
		
		/*if(getAlert()!="")
		{
			alert =getAlert();
			System.out.println("aleart presetnt3"+isAlertPresent());
			wResult.writeTestResult("Verify 'Shipping infor' Page", "'Shipping infor' page should be displayed", "Following error found :"+alert, false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Shipping info fail");
			acceptAlert();
			Assert.assertEquals("'Shipping infor' page should be displayed", "Following error found :"+alert);

		}*/

		
		if(isDisplayed(errorMsg))
		{
			for(int i=0;i<isErrorMsgDisplayed(errorMsg).size();i++)
			{
				System.out.println("XXXX"+totalErrorMgs.get(i));
				if(!totalErrorMgs.get(i).equalsIgnoreCase(""))
				{	
					wResult.writeTestResult("Verify 'Shipping infor' Page", "'Shipping infor' page should be displayed", "Following error found :"+getText(errorMsg), false, writeFileName);
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Shipping info fail");
					Assert.assertEquals("'Shipping infor' page should be displayed", "Following error found :"+getText(errorMsg));
				}
			}
		}

			
	}
	
	public String creditcardError() throws Exception
	{
		String alert = "";
		System.out.println("aleart presetnt2"+isAlertPresent());
		
		if(isAlertPresent())
		{
			alert =getAlert();
			System.out.println("aleart presetnt3"+isAlertPresent());
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Credit Card Payment Error");
			acceptAlert();
			
			
		}

		return alert;
					
	}
	
	public void paymentMethodIsMoneyOrder() throws Exception
	{
		if(isElementPresent(chkPaymentIsMoneyOrder))
		{
			click(chkPaymentIsMoneyOrder);
			wResult.writeTestResult("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", "User can selct payment type is money order in payment information page", true, writeFileName);	
			
		}
		else
		{
			wResult.writeTestResult("Verify user can select payment type in payment information page", "User can select the payment type in payment information page", "Money order option is not displayed or money order xpath is incorrect in payment information page", false, writeFileName);	
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Select payment type");
			Assert.assertEquals("User can select the payment type in payment information page", "Money order option is not displayed or money order xpath is incorrect in payment information page");
		
		}
	}
	
	public void moneyOrderSubmitButton() throws Exception
	{
		System.out.println("moneyOrderContinueButton"+moneyOrderContinueButton);
		
		if(!isElementPresent(moneyOrderContinueButton))
		{
			wResult.writeTestResult("Verify 'Money Order' button is displaying in Payment information", "'Money Order' button should displayed in payment information page", "'Money Order' button is not displayed payment information page or button locator is incorrect", false, writeFileName);
			Assert.assertEquals("'Money Order' button should displayed in payment information page", "'Money Order' button is not displayed payment information page or button locator is incorrect");
		
		}
		else
		{
			wResult.writeTestResult("Verify 'Money Order' button is displaying in Payment information", "'Money Order' button should displayed in payment information page", "'Money Order' button is displayed payment information page", true, writeFileName);
						
			click(moneyOrderContinueButton);	
			
			/*if(isDisplayed(chkoutAjaxPayment))
			{
				if(!ajaxCall(chkoutAjaxPayment))
				{
					wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
					captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
					Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
								
				}
			}
			
			else
			{
				
				Thread.sleep(5000);
				
				
			}*/
			
			if(HasCheckoutAjaxRequest.equalsIgnoreCase("Yes"))
			{
				if(!getAttribute(chkoutAjaxPayment,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
				{
					if(!checkoutAjaxRequest(chkoutAjaxPayment))
					{
						wResult.writeTestResult("Verify the 'Continue button' field displayed in payment information page", "'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page", false, writeFileName);	
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Paymet info continue button");	
						Assert.assertEquals("'Continue button' should be displayed in payment information page", "'Continue button' is not displayed or xpath is incorrect in payment information page");
					
					}
				}
			}
			else
			{
				Thread.sleep(5000);
			}
			
			waitForPageLoad();
			
			
		}
	}
	
	public void orderIdInConfirmationPage() throws Exception
	{
			
		if(getText("/html/body_xpath").indexOf("Fatal error")>-1)
		{
			wResult.writeTestResult("Verify Thank You message in confirmation page", "Confirmation page should displayed", "Following error is displayed. "+getText("/html/body_xpath"), false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify Thank You message in confirmation page");
			Assert.assertEquals("Confirmation page should displayed", "Following error is displayed. "+getText("/html/body_xpath"));
			
		}
		/*if(getPageSource("An error occurred in the process of payment"))
		{
			wResult.writeTestResult("Verify Thank You message in confirmation page", "Confirmation page should displayed", "Following error is displayed. 'An error occurred in the process of payment'", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify Thank You message in confirmation page");
			Assert.assertEquals("Confirmation page should displayed", "Following error is displayed. 'An error occurred in the process of payment'");
			
		}*/
		
		if(!getPageSource(orderIdMsgInConfirmationPage))
		{
			for(int i=0;i<20;i++)
			{
				Thread.sleep(1000);	
				System.out.println("I"+i);
			}
						
			
		}
		System.out.println("orderIdMsgInConfirmationPage"+orderIdMsgInConfirmationPage);
		
		if(!getPageSource(orderIdMsgInConfirmationPage))
		{
			wResult.writeTestResult("Verify Thank You message in confirmation page", orderIdMsgInConfirmationPage+"' message should displayed in confirmation page", orderIdMsgInConfirmationPage+" message is not displayed in confirmation page", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify Thank You message in confirmation page");
			Assert.assertEquals(orderIdMsgInConfirmationPage+"' message should displayed in confirmation page", orderIdMsgInConfirmationPage+" message is not displayed in confirmation page");
			
		}
		else
		{
			if(!(isElementPresent(orderIdXpath)||isElementPresent(loggedUserOrderIdXpath)))
			{
				wResult.writeTestResult("Verify Thank You message in confirmation page", "Confirmation page should displayed", "Thank you message is not displayed or order id locator is incorrect", false, writeFileName);
				captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify Thank You message in confirmation page");
				Assert.assertEquals("Confirmation page should displayed", "Thank you message is not displayed or order id locator is incorrect");
			
			}
			
			else
			{
				if(isElementPresent(orderIdXpath))
				{
					orderNumber = getText(orderIdXpath).replaceAll(orderIdMsgInConfirmationPage, "");
				}
				else
				{
					orderNumber = getText(loggedUserOrderIdXpath).replaceAll(orderIdMsgInConfirmationPage, "");
				}
				//orderNumber = getText(orderIdXpath).replaceAll(orderIdMsgInConfirmationPage, "");
				System.out.println("orderNumber"+orderNumber);
				
								
				if(orderNumber.indexOf(".")>-1)
				{
					orderNumber = orderNumber.replace(".", "");
				}
				
				wResult.writeTestResult("Verify Thank You message in confirmation page", "'Thank you for your purchase' message should displayed in confirmation page", "'Thank you for your purchase' message is displayed in confirmation page and Your Order Id is "+orderNumber, true, writeFileName);
				//sendSMS.sendSMS("Successfully created order. Your order number is "+orderNumber);
			}
			
		}		
		
	}
	
	
	public void continueShopping() throws Exception
	{
		if(isElementPresent(continueShoppingButton))
		{
			wResult.writeTestResult("Verify the Continue Shopping button is displayed in the confirmation page", "Continue Shopping button should be displayed in the confirmation page", "Continue Shopping button is displayed in the confirmation page", true, writeFileName);
						
			click(continueShoppingButton);
			
			waitForPageLoad();

			if(common.siteUrl.trim().equalsIgnoreCase(getCurrentUrl().trim()))
			{
					wResult.writeTestResult("Verify confirmation page is redirect to the home page when the user clicks 'Continue Shopping Button'", "Home page should be displayed", "Home page is displayed", true, writeFileName);
			
			}
			else
			{
				wResult.writeTestResult("Verify confirmation page is redirect to the home page when the user clicks 'Continue Shopping Button'", "Expected home page title is "+siteTitle, "Actual page title is "+getTitle(), false, writeFileName);
				Assert.assertEquals("Expected home page title is "+siteTitle, "Actual load page title is "+getTitle());
				
			}
		}
		else
		{
			wResult.writeTestResult("Verify the Continue Shopping button is displayed in the confirmation page", "Continue Shopping button should be displayed in the confirmation page", "Continue Shopping button is not displayed in the confirmation page", false, writeFileName);
		
		}
		

	}
	
	public void ConfirmationMaiReader() throws Exception {
		
		orderNumber = orderNumber.trim();
		
		System.out.println("orderNumber"+orderNumber);
		
		if(orderNumber.isEmpty()||orderNumber.equalsIgnoreCase("")){
			wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation Id is NULL", false, writeFileName);
		}
		else
		{			
			String customerName = billingFirstName+" "+billingLastName;
			
			System.out.println("customerName"+customerName);
			
			
			mailRead(orderNumber,customerName,billingLoginEmail,"19881106");
			
			String textMail =bodyText;
			System.out.println("textMail  "+textMail);
			
			System.out.println("mail res"+textMail.indexOf(customerName));
			System.out.println("mail res11"+textMail.indexOf(orderNumber));
			
			if(textMail.indexOf(orderNumber)>-1 || textMail.indexOf(customerName)>-1){
				wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation mail is Received", true, writeFileName);
				
			}
			
			else{
				wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation mail is not Received yet", false, writeFileName);
				Assert.assertEquals("Confirmation mail should Received", "Confirmation mail is not Received");
			}
		}
		
		
	}
	
	public void memberConfirmationMaiReader() throws Exception {
		
		System.out.println("orderNumber"+orderNumber);
		
		if(orderNumber.isEmpty()||orderNumber.equalsIgnoreCase("")){
			wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation Id is NULL", false, writeFileName);
		}
		else
		{
			String customerName = billingFirstName+" "+billingLastName;
			System.out.println("customerName"+customerName);
			
			mailRead(orderNumber,customerName,loginEmailAddress,"19881106");
			String textMail =bodyText;
			
			System.out.println("textMail  "+textMail);
			
			System.out.println("mail res112"+textMail.indexOf(customerName));
			System.out.println("mail res113"+textMail.indexOf(orderNumber));
			
			//wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation mail is Received", true, writeFileName);
			
			if(textMail.indexOf(orderNumber)>-1 || textMail.indexOf(customerName)>-1)
			{
				wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation mail is Received", true, writeFileName);
				
			}
			
			else
			{
				wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation mail is not Received yet", false, writeFileName);
				Assert.assertEquals("Confirmation mail should Received", "Confirmation mail is not Received");
			}
		}
		
		
	}
	
	
/*	public void billingAleart() throws Exception{
		
		
		//selenium.setSpeed("5000");
		//System.out.println("ssss"+selenium.isAlertPresent());
		//System.out.println("dddd"+selenium.getAlert());
		if(selenium.isAlertPresent()){
			String aleartMessage = selenium.getAlert();
			
			if(aleartMessage.indexOf("There is already a customer registered using this email address")>-1)
			{
				
				//System.out.println("ssweew"+aleartMessage.indexOf("There is already a customer registered using this email address"));
				
				wResult.writeTestResult("Verify email address in billing information page", "User can navigate to next screen", "Aleart message is '"+aleartMessage+"'.", false, writeFileName);
				Assert.assertEquals("User can navigate to next screen", "Aleart message is '"+aleartMessage+"'.");
				selenium.captureScreenShot(wResult.readTime+"-"+writeFileName+"-Screenshots/Billing information page_Email validation.png");
				
			}
		}
		
	}
	
	public void selectShippingInfoPage() throws Exception{
		if(selenium.isElementPresent("id=billing:use_for_shipping_no")){
			
			selenium.click("id=billing:use_for_shipping_no");
			wResult.writeTestResult("Verify user can select 'Ship to a different address' check box in billing information page", "'Ship to a different address' check bos is present in billing information page", "'Ship to a different address' check bos is present in billing information page", true, writeFileName);
			
		}
		else
		{
			wResult.writeTestResult("Verify user can select 'Ship to a different address' check box in billing information page", "'Ship to a different address' check bos is present in billing information page", "'Ship to a different address' check bos is not present in billing information page", false, writeFileName);
			Assert.assertEquals("'Ship to a different address' check bos is present in billing information page", "'Ship to a different address' check bos is not present in billing information page");
			selenium.captureScreenShot(wResult.readTime+"-"+writeFileName+"-Screenshots/Ship to a different address check box.png");
		
			
		}
		
	}	
	
	
	public void shippingInfoPage() throws Exception{
		
		
				
		if(selenium.isElementPresent("id=shipping:firstname")){
			if(selenium.isEditable("id=shipping:firstname")){
				
				selenium.type("id=shipping:firstname", findElementInXLSheet(getCheckoutDetails,("Shipping First Name"));
				wResult.writeTestResult("Verify the 'First Name' text field displayed in shipping information page", "First Name text field should be displayed in shipping information page", "First Name text field is displayed in shipping information page", true, writeFileName);
				
				
			}else{
				
				wResult.writeTestResult("Verify the 'First Name' text field displayed in shipping information page", "First Name text field should be displayed in shipping information page", "First Name text field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		
		if(selenium.isElementPresent("id=shipping:lastname")){
			if(selenium.isEditable("id=shipping:lastname")){
				
				
				selenium.type("id=shipping:lastname", findElementInXLSheet(getCheckoutDetails,("Shipping Last Name"));
				wResult.writeTestResult("Verify the 'Last Name' text field displayed in shipping information page", "Last Name text field should be displayed in shipping information page", "Last Name text field is displayed in shipping information page", true, writeFileName);
				
				
			}else{
				
				wResult.writeTestResult("Verify the 'Last Name' text field displayed in shipping information page", "Last Name text field should be displayed in shipping information page", "Last Name text field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		
		if(selenium.isElementPresent("id=shipping:street1")){
			if(selenium.isEditable("id=shipping:street1")){
				
				selenium.type("id=shipping:street1", findElementInXLSheet(getCheckoutDetails,("Shipping Address"));
				wResult.writeTestResult("Verify the 'Address' text field displayed in shipping information page", "Address text field should be displayed in shipping information page", "Address text field is displayed in shipping information page", true, writeFileName);
				
				
			}else{
				
				wResult.writeTestResult("Verify the 'Address' text field displayed in shipping information page", "Address text field should be displayed in shipping information page", "Address text field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		
		
		if(selenium.isElementPresent("id=shipping:telephone")){
			if(selenium.isEditable("id=shipping:telephone")){
				
				selenium.type("id=shipping:telephone", findElementInXLSheet(getCheckoutDetails,("Shipping Contact Number"));
				wResult.writeTestResult("Verify the 'Contact Number' text field displayed in shipping information page", "Contact Number text field should be displayed in shipping information page", "Contact Number field is displayed in shipping information page", true, writeFileName);
				
				
			}else{
				
				wResult.writeTestResult("Verify the 'Contact Number' text field displayed in shipping information page", "Contact Number text field should be displayed in shipping information page", "Contact Number text field is not displayed in shipping information page", false, writeFileName);
				
			}
		}
		
		if(selenium.isElementPresent("id=shipping:postcodesuburb")){
			
			selenium.type("id=shipping:postcodesuburb","");
			Thread.sleep(TIME/2);		
			selenium.focus("id=shipping:postcodesuburb");
			selenium.type("id=shipping:postcodesuburb", findElementInXLSheet(getCheckoutDetails,("Shipping Postal Code"));
			selenium.click("id=shipping:postcodesuburb");
			selenium.typeKeys("id=shipping:postcodesuburb", "\b");
		
			Thread.sleep(TIME/2);
			
			
			//selenium.click("//div[@onclick='Autocomplete.instances[0].selectText(0);']");
		
		}

		
	}

	
		
	public void shippingMethodPage() throws Exception{
		
		
		
		String shippingMethodConButton = findElementInXLSheet(getParameterXpath,"Shipping method continue button");
		String selectShippingMethod = findElementInXLSheet(getParameterXpath,"Shipping method");
		

		
		if(!selenium.isTextPresent(findElementInXLSheet(getParameterXpath,"Shipping method screen"))){
			
			Thread.sleep(TTIME);
		}
		//System.out.println("selectShippingMethod"+ selenium.isElementPresent(selectShippingMethod));		
		if(selenium.isElementPresent(selectShippingMethod)){
			//System.out.println("selectShippingMethod"+ selectShippingMethod);
			selenium.click(selectShippingMethod);
			//System.out.println("selectShippingMethod"+selectShippingMethod);
			Thread.sleep(TIME/2);
			
		}
		if(selenium.isElementPresent(shippingMethodConButton)){
				
				wResult.writeTestResult("Verify the continue button is displayed in shipping method page", "Continue button should be displayed in shipping information page", "Continue button is displayed in shipping method page", true, writeFileName);
				
				selenium.click(shippingMethodConButton);
				Thread.sleep(TIME);
		}else
		{
				wResult.writeTestResult("Verify the continue button is displayed in shipping method page", "Continue button should be displayed in shipping method page", "Continue button is not displayed in shipping method page", false, writeFileName);
				Assert.assertEquals("Continue button should be displayed in shipping method page", "Continue button is not displayed in shipping method page");
				selenium.captureScreenShot(wResult.readTime+"-"+writeFileName+"-Screenshots/Shipping Method_Continue Button.png");
				
		}
		
	}

	public void confirmationPage() throws Exception{
		String successContant ="";
				
		
		
		//System.out.println("Aleart "+selenium.isAlertPresent());
		/*if(selenium.isAlertPresent())
		{
			pageError = selenium.getAlert();
			wResult.writeTestResult("Verify confirmation page", "Confirmation page should be displayed","Following error is found '"+pageError+"'", false, writeFileName);
			selenium.captureScreenShot(wResult.readTime+"-"+writeFileName+"-Screenshots/ERROR.png");
		//	sms.sendSMS("Checkout process is fail,following error is found- "+pageError);
			Assert.assertEquals("Paypal screen should be displayed","Following error is found '"+pageError+"'");
		
		}else if(selenium.isElementPresent("xpath=//*[@class='error-msg']"))
		{
			pageError = selenium.getText("xpath=//*[@class='error-msg']");
			wResult.writeTestResult("Verify confirmation page", "Confirmation page should be displayed","Following error is found '"+pageError+"'", false, writeFileName);
			selenium.captureScreenShot(wResult.readTime+"-"+writeFileName+"-Screenshots/ERROR.png");
		//	sms.sendSMS("Checkout process is fail,following error is found- "+pageError);
			Assert.assertEquals("Paypal screen should be displayed","Following error is found '"+pageError+"'");
		
		}*/
		
	/*	if(driver.getPageSource().contains("Your order number is"))
		{
			
			successContant = driver.findElement(By.xpath(findElementInXLSheet(getParameterXpath,"Thank you message"))).getText().trim();
			System.out.println("successContant " +successContant);
			orderId = successContant.substring(successContant.indexOf("Your order number is")+20,successContant.indexOf("Your order information")).trim();
			
			System.out.println("orderId " +orderId);
			
			wResult.writeTestResult("Verify Thank You message displayed in confirmation page", "'Thank you for your purchase' message should displayed in confirmation page", "'Thank you for your purchase' message is displayed in confirmation page and Your Order Id is "+orderId, true, writeFileName);
			
		}
		else if(driver.getPageSource().contains("Payment profile"))
		{
			
			successContant = driver.findElement(By.xpath(findElementInXLSheet(getParameterXpath,"Thank you message"))).getText().trim();
			
			orderId = successContant;
			
			wResult.writeTestResult("Verify Thank You message in confirmation page", "'Thank you for your purchase' message should displayed in confirmation page", "'Thank you for your purchase' message is displayed in confirmation page and Your Order Id is "+orderId, true, writeFileName);
	
		}
		else
		{	
			
			wResult.writeTestResult("Verify Thank You message displayed in confirmation page", "'Thank you for your purchase' message should displayed in confirmation page", "Confirmation page is not loaded. Following page is found "+ getTitle() , false, writeFileName);
			Assert.assertEquals("'Thank you for your purchase' message should displayed in confirmation page", "confirmation page is not loaded.Following page we got it"+ getTitle());	
		}
		
		
	}

	public void continueShopping() throws Exception
	{
	
		
		String continueShoppingButton = findElementInXLSheet(getParameterXpath,"Continue Shopping button");
	

		if(isElementPresent(continueShoppingButton))
		{
			wResult.writeTestResult("Verify the Continue Shopping button is displayed in the confirmation page", "Continue Shopping button should be displayed in the confirmation page", "Continue Shopping button is displayed in the confirmation page", true, writeFileName);
						
			click(continueShoppingButton);
			
			if(url.indexOf("@")>-1){
				url = url.substring(url.indexOf("@")+1).trim();
				url = "http://".concat(url);
			}
		
			if(!url.equalsIgnoreCase(driver.getCurrentUrl()))
			{
					wResult.writeTestResult("Verify confirmation page is redirect to the home page when the user clicks 'Continue Shopping Button'", "Home page should be displayed", "Home page is displayed", true, writeFileName);
			
			}
			else
			{
				wResult.writeTestResult("Verify confirmation page is redirect to the home page when the user clicks 'Continue Shopping Button'", "Expected home page title is "+getParameters.get("Site Title"), "Actual page title is "+getTitle(), false, writeFileName);
				Assert.assertEquals("Expected home page title is "+getParameters.get("Site Title"), "Actual load page title is "+getTitle());
				
			}
		}
		else
		{
			wResult.writeTestResult("Verify the Continue Shopping button is displayed in the confirmation page", "Continue Shopping button should be displayed in the confirmation page", "Continue Shopping button is not displayed in the confirmation page", false, writeFileName);
		
		}
		

	}
	
	public void ConfirmationMaiReader() throws Exception {
		
		if(orderId.isEmpty()||orderId.equalsIgnoreCase("")){
			wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation Id is NULL", false, writeFileName);
		}
		else
		{
			String customerName = findElementInXLSheet(getCheckoutDetails,("First Name")+" "+findElementInXLSheet(getCheckoutDetails,("Last Name");
			common.mailRead(orderId,customerName,findElementInXLSheet(getCheckoutDetails,("Email Address"),findElementInXLSheet(getCheckoutDetails,("Gmail Password"));
			String textMail =bodyText1;
			//System.out.println("textMail  "+textMail);
			
			if(textMail.indexOf(orderId)>-1){
				wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation mail is Received", true, writeFileName);
				
			}
			
			else{
				wResult.writeTestResult("Verify confirmation mail Received to gmail Account", "Confirmation mail should Received", "Confirmation mail is not Received yet", false, writeFileName);
				Assert.assertEquals("Confirmation mail should Received", "Confirmation mail is not Received");
			}
		}
		
		
	}
	
	
	
	*/
	
	public void submitValidation() throws Exception
	{
		//getAttribute(validationBillingFirstName,"value") != null &&
		//&& getAttribute(validationBillingFirstName,"value")!=""
			if(!getValidationText(validationBillingFirstName).isEmpty())
			{
				if(!getAttribute(validationBillingFirstName,"value").isEmpty())
				{
					if(getText(validationBillingFirstName).equalsIgnoreCase(validationMsgBillingFirstName))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "First name text box value is empty in billing information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "First name text box value is empty in billing information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "First name text box value is empty and message is "+getText(validationBillingFirstName), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "First name text box value is empty and message is "+getText(validationBillingFirstName));
						
					}
				}
			}
			if(!validationBillingLastName.trim().isEmpty())
			{
				if(!getValidationText(validationBillingLastName).isEmpty())
				{
					if(getText(validationBillingLastName).equalsIgnoreCase(validationMsgBillingLastName))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "Last name text box value is empty in billing information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "Last name text box value is empty in billing information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "Last name text box value is empty and message is "+getText(validationBillingLastName), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "Last name text box value is empty and message is "+getText(validationBillingLastName));
						
					}
				}
			}
			if(findElementInXLSheet(getMethodIsExecuted,"Checkout As Guest Without Email").equalsIgnoreCase("Yes"))
			{
				if(!validationBillingEmail.trim().isEmpty())
				{
					if(!getValidationText(validationBillingLastName).isEmpty())
					{
						if(getText(validationBillingLastName).equalsIgnoreCase(validationMsgBillingEmail))
						{
							wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "Email text box value is empty in billing information page.", false, writeFileName);
							captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
							Assert.assertEquals("shipping infor page should displayed", "Last name text box value is empty in billing information page.");
							
						}
						else
						{
							wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "Email text box value is empty and message is "+getText(validationBillingLastName), false, writeFileName);
							captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
							Assert.assertEquals("shipping infor page should displayed", "Last name text box value is empty and message is "+getText(validationBillingLastName));
							
						}
					}
				}
			}
			if(!validationBillingTelephone.trim().isEmpty())
			{
				if(!getValidationText(validationBillingTelephone).isEmpty())
				{
					if(getText(validationBillingTelephone).equalsIgnoreCase(validationMsgBillingTelephone))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "Email text box value is empty in billing information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "email text box value is empty in billing information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "First name text box value is empty and message is "+getText(validationBillingTelephone), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "Expected message is "+validationMsgBillingTelephone, "First name text box value is empty and message is "+getText(validationBillingTelephone));
						
					}
				}
			}
			if(!validationBillingPostcode.trim().isEmpty())
			{
				if(!getValidationText(validationBillingPostcode).isEmpty())
				{
					if(getText(validationBillingPostcode).equalsIgnoreCase(validationMsgBillingPostcode))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "Confirm email text box value is empty in billing information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "Confirm email text box value is empty in billing information page.");
					
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "Confirm email text box value is empty and message is "+getText(validationBillingPostcode), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "Confirm email text box value is empty and message is "+getText(validationBillingPostcode));
					
					}
				}
				
			}
			if(!validationBillingCountry_Id.trim().isEmpty())
			{
				if(!getValidationText(validationBillingCountry_Id).isEmpty())
				{
					if(getText(validationBillingCountry_Id).equalsIgnoreCase(validationMsgBillingCountry_Id))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "Country value is empty in billing information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "Country value is empty in billing information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "Country value is empty and message is "+getText(validationMsgBillingCountry_Id), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "Country value is empty and message is "+getText(validationMsgBillingCountry_Id));
						
					}
				}
			}
			
			if(!validationBillingRegion.trim().isEmpty())
			{
				if(!getValidationText(validationBillingRegion).isEmpty())
				{
					if(getText(validationBillingRegion).equalsIgnoreCase(validationMsgBillingRegion))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "State value is empty in billing information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "State value is empty in billing information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "State value is empty and message is "+getText(validationBillingCountry_Id), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "State value is empty and message is "+getText(validationBillingCountry_Id));
						
					}
				}
				
				/*System.out.println("getattribute"+getAttribute(validationBillingRegion,"value"));
				if(!getAttribute(validationBillingRegion,"value").equalsIgnoreCase(""))
				{
					if(getText(validationBillingRegion).equalsIgnoreCase(validationMsgBillingRegion))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "State value is empty in billing information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "State value is empty in billing information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in billing information page", "shipping infor page should displayed", "State value is empty and message is "+getText(validationBillingCountry_Id), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in billing information page");
						Assert.assertEquals("shipping infor page should displayed", "State value is empty and message is "+getText(validationBillingCountry_Id));
						
					}
				}*/
			}

	}
	
	public void submitShiipingValidation() throws Exception
	{
		//getAttribute(validationShippingFirstName,"value") != null &&
		//&& getAttribute(validationShippingFirstName,"value")!=""
			if(!getValidationText(validationShippingFirstName).isEmpty())
			{
				if(!getValidationAttribute(validationShippingFirstName,"value").isEmpty())
				{
					if(getText(validationShippingFirstName).equalsIgnoreCase(validationMsgShippingFirstName))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "First name text box value is empty in shipping information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "First name text box value is empty in shipping information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "First name text box value is empty and message is "+getText(validationShippingFirstName), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "First name text box value is empty and message is "+getText(validationShippingFirstName));
						
					}
				}
			}
			if(!validationShippingLastName.trim().isEmpty())
			{
				if(!getValidationText(validationShippingLastName).isEmpty())
				{
					if(getText(validationShippingLastName).equalsIgnoreCase(validationMsgShippingLastName))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "Last name text box value is empty in shipping information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "Last name text box value is empty in shipping information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "Last name text box value is empty and message is "+getText(validationShippingLastName), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "Last name text box value is empty and message is "+getText(validationShippingLastName));
						
					}
				}
			}
			
			if(!validationShippingTelephone.trim().isEmpty())
			{
				if(!getValidationText(validationShippingTelephone).isEmpty())
				{
					if(getText(validationShippingTelephone).equalsIgnoreCase(validationMsgShippingTelephone))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "Telephone text box value is empty or telephone not valid in shipping information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "Telephone text box value is empty or telephone not valid in shipping information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "Telephone text box value is empty or telephone not valid and message is "+getText(validationShippingTelephone), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "Expected message is "+validationMsgShippingTelephone, "Telephone text box value is empty and or telephone not valid message is "+getText(validationShippingTelephone));
						
					}
				}
			}
			if(!validationShippingPostcode.trim().isEmpty())
			{
				if(!getValidationText(validationShippingPostcode).isEmpty())
				{
					if(getText(validationShippingPostcode).equalsIgnoreCase(validationMsgShippingPostcode))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "Post code text box value is empty in shipping information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "Post code text box value is empty in shipping information page.");
					
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "Post code text box value is empty and message is "+getText(validationShippingPostcode), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "Post code text box value is empty and message is "+getText(validationShippingPostcode));
					
					}
				}
				
			}
			if(!validationShippingCountry_Id.trim().isEmpty())
			{
				if(!getValidationText(validationShippingCountry_Id).isEmpty())
				{
					if(getText(validationShippingCountry_Id).equalsIgnoreCase(validationMsgShippingCountry_Id))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "Country value is empty in shipping information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "Country value is empty in shipping information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "Country value is empty and message is "+getText(validationMsgShippingCountry_Id), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "Country value is empty and message is "+getText(validationMsgShippingCountry_Id));
						
					}
				}
			}
			
			if(!validationShippingRegion.trim().isEmpty())
			{
				if(!getValidationText(validationShippingRegion).isEmpty())
				{
					if(getText(validationShippingRegion).equalsIgnoreCase(validationMsgShippingRegion))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "State value is empty in shipping information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "State value is empty in shipping information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "State value is empty and message is "+getText(validationShippingCountry_Id), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "State value is empty and message is "+getText(validationShippingCountry_Id));
						
					}
				}
				
				/*System.out.println("getattribute"+getAttribute(validationShippingRegion,"value"));
				if(!getAttribute(validationShippingRegion,"value").equalsIgnoreCase(""))
				{
					if(getText(validationShippingRegion).equalsIgnoreCase(validationMsgShippingRegion))
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "State value is empty in shipping information page.", false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "State value is empty in shipping information page.");
						
					}
					else
					{
						wResult.writeTestResult("Verify user clicks submit button with correct data in shipping information page", "shipping infor page should displayed", "State value is empty and message is "+getText(validationShippingCountry_Id), false, writeFileName);
						captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify user clicks submit button with correct data in shipping information page");
						Assert.assertEquals("shipping infor page should displayed", "State value is empty and message is "+getText(validationShippingCountry_Id));
						
					}
				}*/
			}

	}
	
	
	public void verifyOrderDisplayedInMyAccount() throws Exception
	{
		common.navigateToHomePage();		

		if(!common.navigateToLoginPage())
		{
			wResult.writeTestResult("Verify login page","User can navigate to login page", "User Can't navigate to login page.login link is not displayed or link path ("+loginLink+") is incorrect", false, writeFileName);
			Assert.assertEquals("User can navigate to login page", "User Can't navigate to login page.login link is not displayed or link path ("+loginLink+") is incorrect");
	
		}		
		else
		{
			wResult.writeTestResult("Verify login page","User can navigate to login page", "User Can navigate to login page", true, writeFileName);
			
			
		}
		if(getPageSource(orderNumber))
		{
			wResult.writeTestResult("Verify Order displayed in my account section", "Order "+orderNumber +" should be displayed in my account", "Order "+orderNumber +" is displayed in my account", true, writeFileName);
			
		}
		else
		{
			wResult.writeTestResult("Verify Order displayed in my account section", "Order "+orderNumber +" should be displayed in my account", "Order "+orderNumber +" is not displayed in my account", false, writeFileName);
			captureScreenShot(wResult.readTime+"-"+writeFileName+"-Verify Order displayed in my account section");
			Assert.assertEquals("Order "+orderNumber +" should be displayed in my account", "Order "+orderNumber +" is not displayed in my account");
		
		}
		
		
	}
	public boolean ajaxCall(String loader) throws Exception
	{
		int i=0;
		boolean isAjaxLoad = true;
		
		if(isElementPresent(chkoutAjaxLoader))
		{	
			System.out.println("aaa"+isDisplayed(chkoutAjaxLoader));
			
			while(isDisplayed(chkoutAjaxLoader))
			 {			   	 
			   	 Thread.sleep(1000);
			   	 System.out.println("i"+i);
			   	 i++;
			   	 
			   	 if(i==100)
			   	 {
			   		isAjaxLoad = false; 
			   		break;
			   	 }
			   	 
			 }
			 
		}
		return isAjaxLoad;
		
	   
	}
	
	public boolean checkoutAjaxRequest(String loader) throws Exception
	{
		int i=0;
		boolean isAjaxLoad = true;
		
		if(isElementPresent(loader))
		{	
			System.out.println("cccc "+getAttribute(loader,chkoutAjaxClassType));
			 while(!getAttribute(loader,chkoutAjaxClassType).equalsIgnoreCase(chkoutAjaxClassValue))
			 {	
				 System.out.println("loader "+getAttribute(loader,chkoutAjaxClassType));
				 System.out.println("chkoutAjaxClassValue"+chkoutAjaxClassValue);
			   	 Thread.sleep(1000);
			   	 System.out.println("i"+i);
			   	 i++;
			   	 
			   	 if(i==100)
			   	 {
			   		isAjaxLoad = false; 
			   		break;
			   	 }
			   	 
			 }
			 
		}		
		return isAjaxLoad;
		
	   
	}
	
		
	
	
	public void totalTime(java.util.Date startTime) throws Exception
	{		
		java.util.Date endTime;		
		Calendar cal = Calendar.getInstance(); 		
		endTime = cal.getTime();
		String totalTime = common.totalTime(startTime, endTime);		   
	    wResult.writeTestResult("Total time to execute "+writeFileName, "Total time =",totalTime, true, writeFileName);
		
	}
		
}