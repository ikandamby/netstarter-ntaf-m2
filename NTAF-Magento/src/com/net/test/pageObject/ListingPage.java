package com.net.test.pageObject;

import java.util.Calendar;

import com.net.test.data.CheckoutTestData;
import com.net.test.data.ListingTestData;
import com.net.test.util.CreateIssue;
import com.net.test.util.ReadXl;
import com.net.test.util.SendTextMessage;
import com.net.test.util.TestCommonMethods;
import com.net.test.util.WriteResults; 

public class ListingPage extends ListingTestData
{	
	WriteResults wResult =  new WriteResults();
	ReadXl readParametrs  = new ReadXl();
	TestCommonMethods common = new TestCommonMethods();
	CheckOutProcess ckProcess = new CheckOutProcess(); 
	CheckoutTestData checkoutTestData = new CheckoutTestData();
	SendTextMessage sendSMS = new SendTextMessage();
	CreateIssue createIssue = new CreateIssue();
	HomePage home = new HomePage();

	protected static int noOfProductsPerPage = 0;
	protected static int totalItemCount = 0;
	protected static int paginationListSize = 0;
	protected static int noOfProducts = 0;
	protected static int noOfColoums = 0;
	
	protected static String getProductName = "";
	protected static String getProductPrice = "";
	protected static String getCompareProductName = "";
	protected static String temp ="";
	protected static boolean isSorted = true;
	protected static boolean tafFirstCateProductsRow = false;	

	public void skipMethods(String methodName)throws Exception 
	{		
		wResult.writeSkipTestResult("Shopping Cart ",methodName ,"", "Skip Results");
	}
	
	public void resultSheetName(String resultSheet)throws Exception 
	{
		writeFileName=resultSheet;		
	}
	public void getDataFromDataFile() throws Exception
	{ 
		getLocation();
	}
	
	public void navigateToCategoryPage() throws Exception
	{ 		
		if(!isDisplayed(categoryViewXpath))
		{
			common.clickLogo();
			
			if(!common.selectBrand())
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", "User can select the product", "Selected brand is not dipalyed or page is not loaded properly" ,"",false,true);
			}
			else
			{
				writeTestResultsToUJ("426f8e4e",true);
				writeTestResultsToUJ("426f8b88",true);			
			}
						
			if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
			{
				common.selectProductFromLeftCategory();				
			}	
			
			if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
			{
				if(!common.productGrid())
				{
					 writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","Grid link should be present","Link is not displayed or xpath ("+checkoutTestData.productGrid+") is incorrect","",false,true);
				}
			}
		}		
	}
	
	public void verifyNoOfItemsPerPage() throws Exception
	{ 
		if(!dropItemShowXpath.isEmpty())
		{
			if(isElementPresent(dropItemShowXpath))
			{
				noOfProductsPerPage = selectedValueInDropdown(dropItemShowXpath);
				
				if(noOfProductsPerPage > 0)
				{
					 writeTestResults("Verify no of items per page","Display no of items per page","Default no of items per page is "+
							 		selectedValueInDropdown(dropItemShowXpath),"426f97d6",true,false);	
				}
				else
				{
					 writeTestResults("Verify no of items per page","Display no of items per page","Default item count per page is 0","426f97d6",false,true);
				}
						
			}
			else
			{
				 writeTestResults("Verify display the no items per page","Display no of items per page","Dropdown is not displayed or incorrect xpath "
						 			+dropItemShowXpath,"426f97d6",false,true);
			}
		}				
	}
	
	public void getTotalProductCount() throws Exception
	{ 
		String getItemAmounttext ="";
		
		if(!lbTotalItems.isEmpty() && isElementPresent(lbTotalItems))
		{
			getItemAmounttext = getText(lbTotalItems);
			
			if(getItemAmounttext.indexOf("Item(s)") >= 0)
			{
				totalItemCount = Integer.parseInt(getItemAmounttext.substring(getItemAmounttext.indexOf(""),getItemAmounttext.indexOf("Item(s)")).trim());
			}
			else if(getItemAmounttext.indexOf("of") >= 0)
			{
				totalItemCount = Integer.parseInt(getItemAmounttext.substring(getItemAmounttext.indexOf("of")+2).trim());
			}		
			
			if(totalItemCount > 0 )
			{
				 writeTestResults("Verify no of total products","Display total products count","Total item count is "+totalItemCount,"",true,false);
			}
			else
			{
				 writeTestResults("Verify no of total products","Display no of items per page","Total item count xpath not present or incorrect xpath "+lbTotalItems,"",false,true);
			}
		}
		
			
	}
	
	public void getTotalNoOfPages() throws Exception
	{
		if(isElementPresent(getNoofPagesXpath) && isElementPresent(getPagesCountTagName))
		{
			paginationListSize = getTagListSize(getNoofPagesXpath,getPagesCountTagName);
			
			System.out.println("paginationListSize"+paginationListSize);
			if(paginationListSize > 1)
			{
				 writeTestResults("Verify display all no of pages ","Display no of pages",(paginationListSize-1)+" pages are visible","",true,false);	
			}
			else
			{
				 writeTestResults("Verify display all no of pages ","Display no of pages","Total no of pages count is 0","",true,false);
			}			
		}	
	}
	
	public void navigateToNextPreviousPages() throws Exception
	{
		int i=1;
		String getCurrentUrl = "";
	
		if(paginationListSize > 1)
		{
			while(i < (paginationListSize-1))
			{
				getCurrentUrl = getCurrentUrl();
				if(isDisplayed(nextButton))
				{
					click(nextButton);
					
					if(!ajaxCall(ajaxLoader))
					{
						 writeTestResults("Sort results should be displayed", "Sort results should be displayed", 
								 "Sort results is not displayed within 100sec","426f9b0a",false,true);
					}
					
					if(getCurrentUrl.equalsIgnoreCase(getCurrentUrl()))
					{
						 writeTestResults("Verify 'Next' button is working","User can click next button till last page","Next button is not working","426f9dee",false,true);
					}
				}
				else
				{
					 writeTestResults("Verify 'Next' button is working","User can click next button till last page","Next button is not displayed in "+i+" page"
							 ,"426f9dee",false,true);
				}				
				
				i++;
			 }
			 writeTestResults("Verify 'Next' button is working","User can click next button till last page","User can click next button till last page","426f9dee",true,false);
		
			while(1 < i)
			{
				getCurrentUrl = getCurrentUrl();
				
				if(isDisplayed(previuosButton))
				{
					click(previuosButton);
					
					if(!ajaxCall(ajaxLoader))
					{
						 writeTestResults("Sort results should be displayed", "Sort results should be displayed", 
								 "Sort results is not displayed within 100sec","426f9b0a",false,true);
					}
					
					if(getCurrentUrl.equalsIgnoreCase(getCurrentUrl()))
					{
						 writeTestResults("Verify 'Previous' button is working","User can click previous button till first page","Previous button is not working","426f9dee",false,true);
					}
				}
				else
				{
					 writeTestResults("Verify 'Previous' button is working","User can click previous button till first page","Previous button is not displayed in "+i+" page","426f9dee",false,true);
				}
				
				
				i--;
			}
			 writeTestResults("Verify 'Previous' button is working","User can click previous button till first page","User can click previous button till first page","426f9dee",true,false);
		}					
	}	
	
	public void sortByNameAsc(String pageType) throws Exception
	{		
		if(pageType.equalsIgnoreCase("product listing"))
		{
			navigateToCategoryPage();	
		}
		else if(pageType.equalsIgnoreCase("product search results"))
		{
			if(getTitle().indexOf("Search results for:") < 0)
			{
				productSearch();					
			}			
			if(!isElementPresent(categoryViewXpath))
			{
				 writeTestResults("Verify user can sort the products by 'name' Asc in "+pageType+" page" , "Products should be displayed",
						 "Products are not displayed","",false,true);
			}
		}
		
		if(!isDisplayed(dropdownSort))
		{
			 writeTestResults("Verify 'sort dropdown'","Sort drop down should be displayed","Sort dropdown is not dispalyed or xpath is incorrect","426f9b0a",false,true);
		}
		else
		{
			if(isExecuted(getMethodIsExecuted,"Sort option has click event"))
			{
			   click(dropdownSort);
			   
			   click(sortByNameAsc);	
			   
			   if(!ajaxCall(ajaxLoader))
				{
					 writeTestResults("Sort results should be displayed", "Sort results should be displayed", 
							 "Sort results is not displayed within 100sec","426f9b0a",false,true);
				}
			    
			}
			else
			{
				selectText(dropdownSort,sortByNameAsc);	
				
				 if(!ajaxCall(ajaxLoader))
				 {
					 writeTestResults("Sort results should be displayed", "Sort results should be displayed", "Sort results is not displayed within 100sec","426f9b0a",false,true);
				 }
			}				
			
			int i =1;				
			
			
			if(isDisplayed(nextButton))
			{
				while(isDisplayed(nextButton))
				{					
					verifySortByName("asc",pageType);
					
					if(!isDisplayed(nextButton))
					{
						break;
					}
					else
					{
						click(nextButton);
						
						if(!ajaxCall(ajaxLoader))
						{
							 writeTestResults("Sort results should be displayed", "Sort results should be displayed", 
									 "Sort results is not displayed within 100sec","426f9b0a",false,true);
						}
					}									
					
					if(isSorted)
					{
						 writeTestResults("Verify user can sort the products by 'name' Asc in "+pageType+" page",
								 "Display the sorted the products",i +" page display the sorted the products by name asc","426f9b0a",true,false);	
					}
					else
					{
						 writeTestResults("Verify user can sort the products by 'name' Asc in "+pageType+" page",
								 "Display the sorted the products",i +" page is not sorted by name asc","426f9b0a",false,false);
					}						
					i++;			
				}	
			}
			else
			{
				verifySortByName("asc",pageType);
				
				if(isSorted)
				{
					 writeTestResults("Verify user can sort the products by 'name' Asc in "+pageType+" page","Display the sorted the products",i +" page display the sorted the products by name asc","426f9b0a",true,false);	
				}
				else
				{
					 writeTestResults("Verify user can sort the products by 'name' Asc in "+pageType+" page","Display the sorted the products",i +" page is not sorted by name asc","426f9b0a",false,false);
				}	
			}			
		}
		
	}
	
	public void sortByNameDesc(String pageType) throws Exception
	{
		if(pageType.equalsIgnoreCase("product listing"))
		{
			navigateToCategoryPage();	
		}
		else if(pageType.equalsIgnoreCase("product search results"))
		{
			if(getTitle().indexOf("Search results for:") < 0)
			{
				productSearch();			
			}			
			if(!isElementPresent(categoryViewXpath))
			{
				 writeTestResults("Verify user can sort the products by 'name' desc in "+pageType+" page" , "Products should be displayed","Products are not displayed","",false,true);
			}
		}
		
		if(!isDisplayed(dropdownSort))
		{
			 writeTestResults("Verify 'sort dropdown'","Sort drop down should be displayed","Sort dropdown is not dispalyed or xpath is incorrect","",false,true);
		}
		else
		{
			selectText(dropdownSort,sortByNameAsc);		
			
			if(isDisplayed(sortByNameDesc))
			{
				clickAndWait(sortByNameDesc);
			}
			
			 if(!ajaxCall(ajaxLoader))
			 {
				writeTestResults("Sort results should be displayed", "Sort results should be displayed", "Sort results is not displayed within 100sec","",false,true);
			 }
			
			isSorted = true;
			int i =1;		
			
			if(isDisplayed(nextButton))
			{
				while(isDisplayed(nextButton))
				{				
					verifySortByName("desc",pageType);
				
					if(!isDisplayed(nextButton))
					{
						break;
					}
					else
					{
						click(nextButton);
						
						if(!ajaxCall(ajaxLoader))
						{
							 writeTestResults("Sort results should be displayed", "Sort results should be displayed", "Sort results is not displayed within 100sec","",false,true);
						}
					}	
				
				if(isSorted)
				{
					 writeTestResults("Verify user can sort the products by 'name' desc in "+pageType+" page","Display the sorted the products",i +" page display the sorted the products by name desc","",true,false);	
				}
				else
				{
					 writeTestResults("Verify user can sort the products by 'name' desc in "+pageType+" page","Display the sorted the products",i +" page is not sorted by name desc","",false,false);
				}			
				
				i++;			
				}
				
			}
			else
			{
				verifySortByName("desc",pageType);
				
				if(isSorted)
				{
					 writeTestResults("Verify user can sort the products by 'name' desc in "+pageType+" page","Display the sorted the products",i +" page display the sorted the products by name desc","426f9b0a",true,false);	
				}
				else
				{
					 writeTestResults("Verify user can sort the products by 'name' desc in "+pageType+" page","Display the sorted the products",i +" page is not sorted by name asc","426f9b0a",false,false);
				}					
			}				
		}		
	}
	
	private void verifySortByName(String sortType,String pageType) throws Exception
	{
		temp = "";
		getProductName = "";
		
		isSorted = true;
		
		noOfProducts = getElementSize(categoryViewXpath);
		System.out.println("noOfProducts"+noOfProducts);
		
		for(int i=1; i <= noOfProducts; i++)
		{
			noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "["+i+"]"))).size();
			
			if(readParametrs.readFileName.indexOf("The Athletes Foot") >= 0 && i==1)
			{
				noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "["+i+"]"))).size() - 1;
			}
			else
			{
				noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "["+i+"]"))).size();
			}
				
			System.out.println("noOfColoums"+noOfColoums);
			
			for(int j = 1; j <= noOfColoums; j++)
			{
				if(temp.isEmpty())
				{
					temp = getText(getSortedProductName.replace("{id}", "["+i+"]").replace("{id1}", "["+j+"]"));
					
				}
				else
				{
					getProductName = getText(getSortedProductName.replace("{id}", "["+i+"]").replace("{id1}", "["+j+"]"));
					
				}
				
				if(!getProductName.isEmpty())
				{
					if(sortType.equalsIgnoreCase("asc"))
					{
						if(getProductName.compareToIgnoreCase(temp) < 0)
						{
							isSorted = false;
							
							 writeTestResults("Verify user can sort the products by 'name' "+sortType+" in "+pageType+" page","Display the sorted the products","Follwing products not sorted - '"+getProductName +"' and "+temp,"",false,false);							
						}
						
						temp = getProductName;
					}
					else if(sortType.equalsIgnoreCase("desc"))
					{
						if(temp.compareToIgnoreCase(getProductName) < 0)
						{
							isSorted = false;
							
							 writeTestResults("Verify user can sort the products by 'name' "+sortType+" in "+pageType+" page","Display the sorted the products","Follwing products not sorted - '"+getProductName +"' and "+temp,"",false,false);
						}
						
						temp = getProductName;
					}					
				}			
			}
		}	
	}
	
	public void sortByPriceAsc(String pageType) throws Exception
	{
		if(pageType.equalsIgnoreCase("product listing"))
		{
			navigateToCategoryPage();	
		}
		else if(pageType.equalsIgnoreCase("product search results"))
		{
			if(getTitle().indexOf("Search results for:") < 0)
			{
				productSearch();				
			}
			
			if(!isElementPresent(categoryViewXpath))
			{
				 writeTestResults("Verify user can sort the products by 'name' Asc in "+pageType+" page" , "Products should be displayed","Products are not displayed","",false,true);
			}
		}
		
		if(!isDisplayed(dropdownSort))
		{
			 writeTestResults("Verify 'sort dropdown'","Sort drop down should be displayed","Sort dropdown is not dispalyed or xpath is incorrect","",false,true);
		}
		else
		{
			if(isExecuted(getMethodIsExecuted,"Sort option has click event"))
			{
			   click(dropdownSort);
			   
			   click(sortByPriceAsc);
			  		    
			}
			else
			{
				selectText(dropdownSort,sortByPriceAsc);	
								
				 if(!ajaxCall(ajaxLoader))
				 {
					 writeTestResults("Sort results should be displayed", "Sort results should be displayed", "Sort results is not displayed within 100sec","",false,false);
				 }
			}			
			
			int i =1;				
			isSorted = true;
			while(isDisplayed(nextButton))
			{				
				verifySortByPrice("asc",pageType);
				
				if(!nextButton.isEmpty())
				{
					click(nextButton);
					
					if(!ajaxCall(ajaxLoader))
					{
						writeTestResults("Sort results should be displayed", "Sort results should be displayed", "Sort results is not displayed within 100sec","",false,false);
					}
				}
				
				if(isSorted)
				{
					 writeTestResults("Verify user can sort the products by 'Price' Asc in "+pageType+" page","Display the sorted the products",i +" page display the sorted the products by price asc","",true,false);	
				}
				else
				{
					 writeTestResults("Verify user can sort the products by 'Price' Asc in "+pageType+" page","Display the sorted the products",i +" page is not sorted by price asc","",false,false);
				}			
				
				i++;			
			}		
			
			if(!isDisplayed(nextButton))
			{
				verifySortByPrice("asc",pageType);
				
				if(isSorted)
				{
					 writeTestResults("Verify user can sort the products by 'Price' Asc in "+pageType+" page","Display the sorted the products",i +" page display the sorted the products by price asc","",true,false);	
				}
				else
				{
					 writeTestResults("Verify user can sort the products by 'Price' Asc in "+pageType+" page","Display the sorted the products",i +" page is not sorted by price asc","",false,false);
				}					
			}		
		}	
	}
	
	public void sortByPriceDesc(String pageType) throws Exception
	{
		if(pageType.equalsIgnoreCase("product listing"))
		{
			navigateToCategoryPage();	
		}
		else if(pageType.equalsIgnoreCase("product search results"))
		{
			if(getTitle().indexOf("Search results for:") < 0)
			{
				productSearch();				
			}
			
			if(!isElementPresent(categoryViewXpath))
			{
				 writeTestResults("Verify user can sort the products by 'name' desc in "+pageType+" page" , "Products should be displayed","Products are not displayed","",false,true);
			}
		}
		
		if(!isDisplayed(dropdownSort))
		{
			 writeTestResults("Verify 'sort dropdown'","Sort drop down should be displayed","Sort dropdown is not dispalyed or xpath is incorrect","",false,true);
		}
		else
		{			
			if(isExecuted(getMethodIsExecuted,"Sort option has click event"))
			{
			   click(dropdownSort);
			   
			   click(sortByPriceDesc);
			   
			   
			    
			}
			else
			{
				selectText(dropdownSort,sortByPriceDesc);
				
				if(!ajaxCall(ajaxLoader))
				{
					 writeTestResults("Sort results should be displayed", "Sort results should be displayed", "Sort results is not displayed within 100sec","",false,true);
				}

			}
				
			
			int i =1;		
			
			while(isDisplayed(nextButton))
			{
				verifySortByPrice("desc",pageType);
				
				if(!nextButton.isEmpty())
				{
					click(nextButton);
					
					if(!ajaxCall(ajaxLoader))
					{
						 writeTestResults("Sort results should be displayed", "Sort results should be displayed", "Sort results is not displayed within 100sec","",false,true);
					}
				}
				
				if(isSorted)
				{
					 writeTestResults("Verify user can sort the products by 'Price' desc in "+pageType+" page","Display the sorted the products",i +" page display the sorted the products by price desc","",true,false);	
				}
				else
				{
					 writeTestResults("Verify user can sort the products by 'Price' desc in "+pageType+" page","Display the sorted the products",i +" page is not sorted by price desc","",false,false);
				}			
				
				i++;			
			}
			
			if(!isDisplayed(nextButton))
			{
				verifySortByPrice("desc",pageType);
				
				if(isSorted)
				{
					 writeTestResults("Verify user can sort the products by 'Price' desc in "+pageType+" page","Display the sorted the products",i +" page display the sorted the products by price desc","",true,false);	
				}
				else
				{
					 writeTestResults("Verify user can sort the products by 'Price' desc in "+pageType+" page","Display the sorted the products",i +" page is not sorted by price desc","",false,false);
				}				
			}		
		}		
	}
	
	
	private void verifySortByPrice(String sortType,String pageType) throws Exception
	{		
		noOfProducts = driver.findElements(getLocator(categoryViewXpath)).size();
		System.out.println("noOfRows"+noOfProducts);
		String productPriceXpath = "";
		String getProductPriceValue = "";
		String tempPrductName = "";
		getProductName = ""; 
		getProductPrice = "";
		temp = "";
		isSorted = true;
		
		for(int i=1; i <= noOfProducts; i++)
		{
			noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "["+i+"]"))).size();
			
			if(readParametrs.readFileName.indexOf("The Athletes Foot") >= 0 && i==1)
			{
				noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "["+i+"]"))).size() - 1;
			}
			else
			{
				noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "["+i+"]"))).size();
			}
			for(int j = 1; j <= noOfColoums; j++)
			{
				if(temp.isEmpty())
				{
					productPriceXpath = getSortedProductPrice.replace("{id}", "["+i+"]").replace("{id1}", "["+j+"]");
					tempPrductName = getText(getSortedProductName.replace("{id}", "["+i+"]").replace("{id1}", "["+j+"]"));
					
					/*if(getTagListSize(productPriceXpath,"p_TagName")>0)
					{
						if(getTagListSize(productPriceXpath,"p_TagName") == 1)
						{
							getProductPrice 	= 	getText(productPriceXpath+"/p[1]//*[@class='price']");	
						}
						else if(getTagListSize(productPriceXpath,"p_TagName") == 3)
						{
							getProductPrice 	= 	getText(productPriceXpath+"/p[3]//span[@class='price']");	
						}
						else if(getTagListSize(productPriceXpath,"p_TagName") == 2)
						{
							getProductPrice 	= 	getText((productPriceXpath+"/p[2]//span[@class='price']"));
						}																	
					}
					else
					{					
						getProductPrice 	= 	getText(productPriceXpath+"//span[@class='price']");					
					}*/
					
					if(isElementPresent(productPriceXpath+"//*[@class='regular-price']"))
					{
						getProductPrice 	= 	getText(productPriceXpath+"//*[@class='regular-price']");
					}
					else if(isElementPresent(productPriceXpath+"//*[@class='special-price']"))
					{
						getProductPrice 	= 	getText(productPriceXpath+"//*[@class='special-price']");
					}
					else
					{
						getProductPrice 	= 	getText(productPriceXpath+"//*[@class='price']");	
					}
						
																		
					temp = Double.toString(common.getValueWitoutCurrency(getProductPrice));					
				}
				else
				{
					if(!(noOfProducts == 1 && j==6))
					{
						productPriceXpath = getSortedProductPrice.replace("{id}", "["+i+"]").replace("{id1}", "["+j+"]");
						getProductName = getText(getSortedProductName.replace("{id}", "["+i+"]").replace("{id1}", "["+j+"]"));	
						
						/*if(getTagListSize(productPriceXpath,"p_TagName")>0)
						{
							if(getTagListSize(productPriceXpath,"p_TagName") == 1)
							{
								getProductPrice 	= 	getText(productPriceXpath+"/p[1]//*[@class='price']");	
							}
							else if(getTagListSize(productPriceXpath,"p_TagName") == 3)
							{
								getProductPrice 	= 	getText(productPriceXpath+"/p[3]//span[@class='price']");	
							}
							else if(getTagListSize(productPriceXpath,"p_TagName") == 2)
							{
								getProductPrice 	= 	getText((productPriceXpath+"/p[2]//span[@class='price']"));
							}																	
						}
						else
						{					
							getProductPrice 	= 	getText(productPriceXpath+"//span[@class='price']");					
						}
						*/
						if(isElementPresent(productPriceXpath+"//*[@class='regular-price']"))
						{
							getProductPrice 	= 	getText(productPriceXpath+"//*[@class='regular-price']");
						}
						else if(isElementPresent(productPriceXpath+"//*[@class='special-price']"))
						{
							getProductPrice 	= 	getText(productPriceXpath+"//*[@class='special-price']");
						}
						else
						{
							getProductPrice 	= 	getText(productPriceXpath+"//*[@class='price']");	
						}
						
						
						getProductPriceValue = Double.toString(common.getValueWitoutCurrency(getProductPrice));
					}
					
					
				}
				
				System.out.println("getProductPrice"+getProductPrice);
				
				if(!(noOfProducts == 1 && j==6))
				{
					if(!getProductPriceValue.isEmpty())
					{
						if(sortType.equalsIgnoreCase("asc"))
						{
							int retval = Double.compare(Double.parseDouble(getProductPriceValue), Double.parseDouble(temp));
							
							if(retval < 0)
							{
								isSorted = false;
								
								 writeTestResults("Verify user can sort the products by 'price' "+sortType+" in "+pageType+" page","Display the sorted the products","Follwing products not sorted - '"+tempPrductName +"' and "+getProductName,"",false,false);
							}
							
							temp = getProductPriceValue;
							tempPrductName = getProductName;
							
						}
						else if(sortType.equalsIgnoreCase("desc"))
						{
							int retval = Double.compare(Double.parseDouble(getProductPriceValue), Double.parseDouble(temp));
							if(retval > 0)
							{
								isSorted = false;
								
								 writeTestResults("Verify user can sort the products by 'price' "+sortType+" in "+pageType+" page","Display the sorted the products","Follwing products not sorted - '"+getProductName +"' and "+ tempPrductName,"",false,false);
							}
							
							temp = getProductPriceValue;
							tempPrductName = getProductName;
						}					
						
					}		
				}
				
							
			}
		}	
	}
	
	public void verifyProductDisplayTypeIsGrid() throws Exception
	{ 
		if(isElementPresent(categoryViewXpath))
		{
			noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "[1]"))).size();
			noOfProducts = driver.findElements(getLocator(categoryViewXpath)).size();
			
			if(noOfColoums >1)
			{
				 writeTestResults("Verify product listing can be displayed as grid or list view","Display products in a grid view",
						 "Display products in a grid view" ,"426f91be",true,false);
			}			
			else if(noOfProducts == 1 && noOfColoums >1)
			{
				 writeTestResults("Verify product listing can be displayed as grid or list view","Display products in a grid view",
						 "Display products in a grid view" ,"426f91be",true,false);
			}			
			else if(noOfProducts == 1 && noOfColoums == 1)
			{
				 writeTestResults("Verify product listing can be displayed as grid or list view","Display products in a grid view",
						 "Display products in a grid view" ,"426f91be",true,false);
			}
			else
			{
				 writeTestResults("Verify product listing can be displayed as grid or list view","Display products in a grid view",
						 "Only one product is displayed in one row","426f91be",false,true);
			}
		}			
	}	
	
	public void leftNavigationList() throws Exception
	{
		navigateToCategoryPage();			
	}
	
	public void addToCartValidation() throws Exception
	{
		addToCartWithoutSelectingSize();
	}
	
	/*private void addtoCartWithoutQTY() throws Exception
	{
		if(!CheckoutTestData.productQTYXpath.isEmpty() && isDisplayed(CheckoutTestData.productQTYXpath))
		{
			sendKeys(CheckoutTestData.productQTYXpath,"0");
			
			if(isDisplayed(TestCommonMethods.addToCartButton))
			{
				click(TestCommonMethods.addToCartButton);
			}
			
			if(isDisplayed(withoutSelectingSizeErrorXpath))
			{
				 writeTestResults("without selecting", "'Error message should be displayed'", "'Error message is displayed. "+getText(withoutSelectingSizeErrorXpath) ,"",true,false);
					
			}
			else
			{
				 writeTestResults("without selecting", "'Error message should be displayed'", "'Error message is not displayed.or xpath is incorrect" ,"",false,true);
			}
			
		}
	}*/
	
	@SuppressWarnings("static-access")
	public void addToCartWithoutSelectingSize() throws Exception
	{ 
		
		common.navigatToCategoryPage();				
		
		common.navigateToProductDetailsPageDuplicate();	
		
		
		if(!productSizeClassName.isEmpty() && isElementPresent(productSizeClassName))
		{
			if(isDisplayed(checkoutTestData.addToCartButton))
			{
				click(checkoutTestData.addToCartButton);
				
				if(isDisplayed(withoutSelectingSizeErrorXpath))
				{
					 writeTestResults("without selecting", "'Error message should be displayed'", "'Error message is displayed. "+getText(withoutSelectingSizeErrorXpath) ,"",true,false);
						
				}
				else
				{
					 writeTestResults("without selecting", "'Error message should be displayed'", "'Error message is not displayed.or xpath is incorrect" ,"",false,true);
				}
			}	
		}
		else if(!productSizeDropdownXpath.isEmpty() && isElementPresent(productSizeDropdownXpath))
		{
			if(isDisplayed(checkoutTestData.addToCartButton))
			{
				click(checkoutTestData.addToCartButton);
				
				if(isDisplayed(withoutSelectingSizeErrorXpath))
				{
					 writeTestResults("without selecting", "'Error message should be displayed'", "'Error message is displayed. "+getText(withoutSelectingSizeErrorXpath) ,"",true,false);
						
				}
				else
				{
					 writeTestResults("without selecting", "'Error message should be displayed'", "'Error message is not displayed.or xpath is incorrect" ,"",false,true);
				}
			}	
		}		
	}	
	
	public void NavigateToProductDetails() throws Exception
	{ 		
		getLocation();
		
		getDataFromXLSheet();
		
		common.clearMyCart();
		
		common.clickLogo();
		
		readXL.getBrandXpath();
		
		common.clickLogo();
		
		if(!common.selectBrand())
		{
			writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", "User can select the product", 
					"Selected brand ("+TestCommonMethods.categoryName+") is not dispalyed or page is not loaded properly","",false,true);
		}
		else
		{
			writeTestResults("Verify user can select category in main navigation", "User can navigate to '"+TestCommonMethods.categoryName+"' page", "Selected category is '"+TestCommonMethods.categoryName+"'","",true,false);			
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
		{
			common.selectProductFromLeftCategory();			
		}	
		
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			if(!common.productGrid())
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","Grid link should be present",
						"Link is not displayed or xpath ("+checkoutTestData.productGrid+") is incorrect","",false,true);
			}
		}	
		String productNameCategory = TestCommonMethods.productName;
		
		common.navigateToProductDetailsPage();			
		
		ckProcess.productNameInProductDetail();
		
			
		String productNameDetails = ckProcess.productNameInDetailPage;
		
		System.out.println("productNameCategory"+productNameCategory);
		System.out.println("productNameDetails"+productNameDetails);
		
		if(productNameCategory.equalsIgnoreCase(productNameDetails))
		{
			writeTestResultsToUJ("426fa122",true);
		}
		else
		{
			writeTestResultsToUJ("426fa122",false);
		}
		
		ckProcess.compareProductDetails(ckProcess.productPriceInProductDetail());
	}
		
	public void verifyProductDisplayTypeIsGridInProductSearch() throws Exception
	{ 
		//readParametrs.getReadFile();
		
		if(isElementPresent(categoryViewXpath))
		{
			noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "[1]"))).size();
			noOfProducts = driver.findElements(getLocator(categoryViewXpath)).size();
			
			if(noOfColoums >1)
			{
				 writeTestResults("Verify product search results can be displayed as grid or list view","Display products in a grid view","Display products in a grid view" ,"",true,false);
			}			
			else if(noOfProducts == 1 && noOfColoums >1)
			{
				 writeTestResults("Verify product search results can be displayed as grid or list view","Display products in a grid view","Display products in a grid view" ,"",true,false);
			}			
			else if(noOfProducts == 1 && noOfColoums == 1)
			{
				 writeTestResults("Verify product search results can be displayed as grid or list view","Display products in a grid view","Display products in a grid view" ,"",true,false);
			}
			else
			{
				writeTestResults("Verify product search results can be displayed as grid or list view","Display products in a grid view","Only one product is displayed in one row","",false,true);
			}
		}		
	}
	
	public void searchTerm()throws Exception 
	{	
		
		
		home.searchTerm();
	}
	
	public void productSearch()throws Exception 
	{	
		if(!isElementPresent(home.searchTextBox))
		{
			common.clickLogo();
			
			if(!isElementPresent(home.searchTextBox))
			{
				 writeTestResults("Verify the user can type text in basic search", "user can type text in basic search" , "Search text box xpath not present","",false,true);
			}			
		}
		else
		{
			sendKeys(home.searchTextBox,home.searchTerm);
			
			if(!isElementPresent(home.searchButton))
			{
				 writeTestResults("Verify search button", "Search button should be displayed in home page" , "Search button xpath("+home.searchButton+") not present","",false,true);
			}
			else
			{
				clickAndWait(home.searchButton);
			}			
		}				
	}
	
	public void searchResults()throws Exception 
	{	
		home.searchResults();		
	}

	public void verifyContent() throws Exception
	{
		temp = "";
		noOfProducts = driver.findElements(getLocator(categoryViewXpath)).size();
		
		for(int i=1; i <= noOfProducts; i++)
		{		
			if(readParametrs.readFileName.indexOf("The Athletes Foot") >= 0 && i==1)
			{
				noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "["+i+"]"))).size() - 1;
				
				System.out.println("noOfColoumszz"+noOfColoums);
				
			}
			else
			{
				noOfColoums = driver.findElements(getLocator(noOfProductsPerColoum.replace("{id}", "["+i+"]"))).size();
			}
			System.out.println("noOfColoums"+noOfColoums);
			
			
			for(int j = 1; j <= noOfColoums; j++)
			{				
				getProductName = getText(getSortedProductName.replace("{id}", "["+i+"]").replace("{id1}", "["+j+"]"));
				
				if(getProductName.toLowerCase().indexOf(home.searchTerm.toLowerCase()) < 0)
				{
					isSorted = false;
				//	 writeTestResults("Verify when the user enter key word then the search result will include all the products which include that key word in the product name",
				//			 "Display all the products containing that key word",
				//			 "Following product name hasn't that keyword ("+searchTerm+") and product name is "+getProductName,"",false,false);
					
				}
				else
				{
					isSorted = true;
				}
			}
		}
	}
	
	
	public void verifySearchResultsContent() throws Exception
	{
		isSorted = true;
		
		while(isDisplayed(nextButton))
		{			
			verifyContent();
			
			if(!nextButton.isEmpty())
			{
				click(nextButton);
				
				if(!ajaxCall(ajaxLoader))
				{
					 writeTestResults("Sort results should be displayed", "Sort results should be displayed", "Sort results is not displayed within 100sec","",false,true);
				}
			}
			
			if(isSorted)
			{
				 writeTestResults("Verify when the user enter key word then the search result will include all the products which include that key word in the product name",
						 "Display all the products containing that key word","Display all the products containing that key word and keyword is "+home.searchTerm,"",false,false);
					
			}
			else
			{
				 writeTestResults("Verify when the user enter key word then the search result will include all the products which include that key word in the product name","" +
				 		"Display all the products containing that key word","Some of product(s) not containing that key word and keyword is "+home.searchTerm,"",false,false);
			}			
			
		}
		
		if(!isDisplayed(nextButton))
		{
			verifyContent();
			
		
		}		
		common.clickLogo();		
	}
	
	public void productDetailsProfilerResults(String pageName)throws Exception 
	{
		String productPageNameAndUrl = pageName +"- Product name is "+ TestCommonMethods.productNameInDetailPage +" and product url is "+getCurrentUrl();
		
		common.getQueryResults(productPageNameAndUrl);			
	}
	
	public void productSearchResultsProfilerResults(String pageName)throws Exception 
	{
		String productPageNameAndUrl = pageName +" and url is "+getCurrentUrl();
		
		common.getQueryResults(productPageNameAndUrl);	
		
	}
	
	public boolean ajaxCall(String loader) throws Exception
	{
		int i=0;
		boolean isAjaxLoad = true;
		
		if(isElementPresent(ajaxLoader))
		{	
			while(isDisplayed(ajaxLoader))
			 {			   	 
			   	 Thread.sleep(1000);
			   	 System.out.println("i"+i);
			   	 i++;
			   	 
			   	 if(i==100)
			   	 {
			   		isAjaxLoad = false; 
			   		break;
			   	 }			   	 
			 }			 
		}
		else
		{
			Thread.sleep(2000);
		}
		
		return isAjaxLoad;
	}
	public void totalTime(java.util.Date startTime) throws Exception
	{		
		java.util.Date endTime;		
		Calendar cal = Calendar.getInstance(); 		
		endTime = cal.getTime();
		String totalTime = common.totalTime(startTime, endTime);		   
	    writeTestResults("Total time to execute "+writeFileName, "Total time =",totalTime,"",true,false);		
	}		
}