package com.net.test.pageObject;

import java.util.Calendar;

import org.openqa.selenium.By;

import com.net.test.util.ReadXl;
import com.net.test.util.TestCommonMethods;
import com.net.test.data.CheckoutTestData;
import com.net.test.data.MyAccountTestData;
import com.net.test.data.UserCreationTestData;

public class MyAccount extends MyAccountTestData
{
	TestCommonMethods common = new TestCommonMethods();
	ReadXl readParametrs  = new ReadXl();
	UserCreationTestData userCreationTestData = new UserCreationTestData();
	CheckoutTestData checkoutTestData = new CheckoutTestData();
	
	public String textMail;
	public String readDataFileName;
	public String setLinkXpath;
	public String getLinkXpath;		
	public String getTextValue;
	public String linkPosition;
	public static String beforeClickUrl;

	public void resultSheetName(String resultSheet)throws Exception
	{		
		writeFileName=resultSheet;
	}
	
	public void navigateToLoginPage()throws Exception 
	{
		if(!common.navigateToHomePage())
		{
			writeTestResults("Verify the home page", "Home page should be displayed" , "Home page is not loaded please chk",
					"426f134c",false,true);
		}
		if(!common.navigateToLoginPage())
		{
			writeTestResults("Verify login page","User can navigate to login page", "User Can't navigate to login page.login link is not displayed or link path ("+loginLink+") is incorrect",
					"426f134c",false,true);	
			writeTestResultsToUJ("42725a34",false);
		}
		else
		{
			writeTestResults("Verify login page","User can navigate to login page", "User Can navigate to login page.",
					"426f134c",true,false);
			
			writeTestResultsToUJ("42725a34",true);

			System.out.println("Navigate to login page");
		}	
	}
	
	public void login()throws Exception
	{	
		String loginFieldsValidation = "";
		
		if(!isDisplayed(txtLoginEmailAddress))
		{
			common.navigateToLoginPage();
		}
		
		loginFieldsValidation =	common.login();
		
		if(!loginFieldsValidation.equalsIgnoreCase("Element present"))
		{
			writeTestResults("Verify fields in login page","All the field shoud be displayed", "Following field "+loginFieldsValidation+" is not displayed or location is incorrect","",false,true);				
		}
		
		loginFieldsValidation = common.passwordMatch();
		
		if(!loginFieldsValidation.equalsIgnoreCase(""))
		{
			writeTestResults("Verify user can login to my account","My Account should be displayed", "Following error "+loginFieldsValidation+" is displayed","",false,true);				
		}
		
		if(isExecuted(getMethodIsExecuted,"Mouse Hover In My Account"))
		{
			if(!mouseMoveToMyAccount.isEmpty() && !isElementPresent(myAccountLink))
			{
				mouseMove(mouseMoveToMyAccount);
			}			
			
			if(isElementPresent(myAccountLink))
			{
				clickAndWait(myAccountLink);
				
				writeTestResultsToUJ("42725c8c",true);
				
				writeTestResults("Verify user can login to my account","My Account should be displayed", "My Account page is displayed","426f169e",true,false);
			}
			else
			{
				writeTestResultsToUJ("42725c8c",false);

			}
		}		
	}	
	
	public void verifyMyAccountLinkFormOtherPage() throws Exception
	{
		common.navigateToHomePage();
		
		common.accountLinksInUnderMyAcc();		
				
		if(isElementPresent("//*[contains(concat(' ', normalize-space(@class), ' '), 'welcome-msg')]/a"))
		{
			clickAndWait("//*[contains(concat(' ', normalize-space(@class), ' '), 'welcome-msg')]/a");
					
			writeTestResults("Verify already logged user can navigate to my account through home page","My Account should be displayed", "My Account is displayed","",true,false);
		}
		else
		{
			writeTestResults("Verify already logged user can navigate to my account through home page","My Account should be displayed", "Can't click My Account","",false,false);

		}
	}
	
	public void logOutLink() throws Exception
	{
		if(!isDisplayed(linkLogOut))
		{			
			if(isElementPresent(myAccountLink))
			{
				click(myAccountLink);
			}
			
			
		}
				
				
		if(isDisplayed(linkLogOut))
		{
			clickAndWait(linkLogOut);	
			
			if(common.siteUrl.equalsIgnoreCase(getCurrentUrl()))
			{
				writeTestResults("Verify logout functionality when user clicks logout button","User should navigate to home page", "Home page is displayed","42725b6a",true,false);
			}
			else
			{
				Thread.sleep(5000);
				System.out.println(getCurrentUrl());
				
				if(common.siteUrl.equalsIgnoreCase(getCurrentUrl()))
					
					
				{
					writeTestResults("Verify logout functionality when user clicks logout button","User should navigate to home page", "Home page is displayed","42725b6a",true,false);
				}
				else
				{
					
					writeTestResults("Verify logout functionality when user clicks logout button","User should navigate to home page", "Following page is found "+ getTitle(),"42725b6a",false,true);
				}				
			}		
		}
		else
		{
			writeTestResults("Verify logout functionality when user clicks logout button","User should navigate to home page", "Logout link is not displayed","42725b6a",false,true);;
		}
	}
	
	private void myAccountMouseMove()  throws Exception
	{	
		if(isExecuted(getMethodIsExecuted,"Mouse Hover In My Account"))
		{
			String mouseMoveToMyAccount = findElementInXLSheet(getParameterXpath,"mouse move to my account");
			
			if(isElementPresent(mouseMoveToMyAccount))
			{
				mouseMove(mouseMoveToMyAccount);
			}			
		}	
	}
	
	public void login1(String loginEmailAddress,String myAccountEditPw)throws Exception
	{	
		click(myAccountLinksLogOut);
		String loginFieldsValidation = "";
		if(!isElementPresent(txtLoginEmailAddress))
		{
			common.navigateToLoginPage();
		}
		
		loginFieldsValidation =	common.loginForOthreUser(loginEmailAddress,myAccountEditPw);
		
		if(!loginFieldsValidation.equalsIgnoreCase(""))
		{
			writeTestResults("Verify fields in login page","All the field shoud be displayed", "Following field "+loginFieldsValidation+" is not displayed or location is incorrect","",false,true);		
				
		}
		loginFieldsValidation = common.passwordMatch();
		
		if(!loginFieldsValidation.equalsIgnoreCase(""))
		{
			writeTestResults("Verify fields in login page","My Account should be displayed", "Following error "+loginFieldsValidation+" is displayed","",false,true);		
			
		}
		
		if(isExecuted(getMethodIsExecuted,"Mouse Hover In My Account"))
		{
			if(!mouseMoveToMyAccount.isEmpty() && !isElementPresent(myAccountLink))
			{
				mouseMove(mouseMoveToMyAccount);
			}			
			if(isElementPresent(myAccountLink))
			{
				clickAndWait(myAccountLink);
			}
		}		
	}
	
	public void fillLoginDetails(String email,String pw)throws Exception
	{	
		pageRefersh();
		
		if(!isDisplayed(txtLoginEmailAddress))
		{
			common.navigateToLoginPage();
		}		
		if(!isDisplayed(txtLoginEmailAddress))
		{
			writeTestResults("Verify fields in login page","Email Address should be displayed", "Email Address text box is not displayed or  path ("+txtLoginEmailAddress+") is incorrect","",false,true);			
		}
		else
		{
			sendKeys(txtLoginEmailAddress,email);
		}		
		if(!isElementPresent(txtLoginPassword))
		{
			writeTestResults("Verify fields in login page","Password should be displayed", "Password text box is not displayed or  path ("+txtLoginPassword+") is incorrect","",false,true);	
		}
		else
		{
			sendKeys(txtLoginPassword,pw);
		}		
		if(!isElementPresent(btnLoginSubmitButton))
		{
			writeTestResults("Verify fields in login page","Submit button should be displayed", "Submit button is not displayed or  path ("+btnLoginSubmitButton+") is incorrect","",false,true);	
		}
		else
		{
			clickAndWait(btnLoginSubmitButton);	
		}		
		if(hasMyAccountAjaxRequest.equalsIgnoreCase("Yes"))
		{
			if(!getAttribute(myAccountAjax,myAccountAjaxClassType).equalsIgnoreCase(myAccountAjaxClassValue))
			{
				if(!myAccountAjaxRequest(myAccountAjax))
				{
					writeTestResults("Verify login validation", "Page should be load without errors", "Page is not load properly","",false,true);	
				
				}
			}
		}		
			
	}
	
	public void fillForgotPwDetails(String email)throws Exception
	{
		pageRefersh();
		
		navigateToForgotPasswordPage();
		
		sendKeys(txtForgotPwEmail,email);
		
		click(btnForgotPwSubmit);
		
		Thread.sleep(2000);			
	}
		
	public void navigateToForgotPasswordPage()throws Exception
	{		
		if(!isDisplayed(linkForgotPw))
		{
			common.navigateToLoginPage();
			
			if(isDisplayed(linkForgotPw))
			{
				clickAndWait(linkForgotPw);
			}
			else
			{
				common.navigateToLoginPage();
				
				clickAndWait(linkForgotPw);
			}			
		}	
		else
		{
			clickAndWait(linkForgotPw);
		}						
	}
	
	public void loginWithoutUserNameAndpw()throws Exception
	{
		fillLoginDetails("","");		
		
		if(isDisplayed(msgXpathEmailAddress) && isDisplayed(msgXpathPassword))
		{
			writeTestResults("Verify empty field validation when the user click login button without 'Email Address' and pw","Expected message is '"+msgEmailAddress+"',"+msgPasswordRequired,
					"Actual message is '"+getText(msgXpathEmailAddress)+"', and "+getText(msgXpathPassword),
					"426f195a",true,false);
		}
		else if(!isDisplayed(msgXpathEmailAddress))
		{
			writeTestResults("Verify empty field validation when the user click login button without 'Email Address'", 
					"Expected message is '"+msgEmailAddress+"'", "xpath not a present or xpath("+msgXpathEmailAddress+") is incorrect",
					"426f195a",false,true);
		}
		else if(!isDisplayed(msgXpathPassword))
		{
			writeTestResults("Verify empty field validation when the user click login button without 'Password'", 
					"Expected message is '"+msgPasswordRequired+"'", "xpath not a present or xpath("+msgXpathPassword+") is incorrect'",
					"426f195a",false,true);
		}		
	}
	
	public void loginWithoutUserName()throws Exception
	{	
		fillLoginDetails("",loginPassword);
		
		if(isDisplayed(msgXpathEmailAddress))
		{
			if(getText(msgXpathEmailAddress).equalsIgnoreCase(msgEmailAddress))
			{				
				writeTestResults("Verify empty field validation when the user click login button without 'Email Address'", "Expected message is '"+msgEmailAddress+"'", "Actual message is '"+getText(msgXpathEmailAddress)+"'",
						"",true,false);
			}
			else
			{
				writeTestResults("Verify empty field validation when the user click login button without 'Email Address'", "Expected message is '"+msgEmailAddress+"'", "Actual message is '"+getText(msgXpathEmailAddress)+"'",
						"",false,true);
			}
		}
		else
		{
			writeTestResults("Verify empty field validation when the user click login button without 'Email Address'", "Expected message is '"+msgEmailAddress+"'", "xpath not a present or xpath("+msgXpathEmailAddress+") is incorrect",
					"",false,true);
		}	
	}
	
	public void loginInvalidEmail1()throws Exception
	{	
		fillLoginDetails("Test.com",loginPassword);
		
		if(isDisplayed(msgXpathInvalidAccountEmailAddress))
		{
			if(getText(msgXpathInvalidAccountEmailAddress).equalsIgnoreCase(msgInvalidAccountEmailAddress))
			{
				writeTestResults("Verify 'login' field validation when user enter email without '@'(Test.com) sign", "Expected message is '"+msgInvalidAccountEmailAddress+"'", "Actual message is '"+getText(msgXpathInvalidAccountEmailAddress)+"'",
						"",true,false);
				}
			else
			{
				writeTestResults("Verify 'login' field validation when user enter email without '@'(Test.com) sign", "Expected message is '"+msgInvalidAccountEmailAddress+"'", "Actual message is '"+getText(msgXpathInvalidAccountEmailAddress)+"'",
						"",false,true);			
			}
		}
		else
		{
			writeTestResults("Verify 'login' field validation when user enter email without '@'(Test.com) sign", "Expected message is '"+msgInvalidAccountEmailAddress+"'", "xpath not a present or xpath("+msgXpathInvalidAccountEmailAddress+") is incorrect'",
					"",false,true);			
		}		
	}
	
	public void loginInvalidEmail2()throws Exception
	{	
		fillLoginDetails("Test@com",loginPassword);
		
		if(isDisplayed(msgXpathInvalidAccountEmailAddress))
		{
			if(getText(msgXpathInvalidAccountEmailAddress).equalsIgnoreCase(msgInvalidAccountEmailAddress))
			{
					writeTestResults("Verify 'login' field validation when user enter email without '.'(Test@com) sign", "Expected message is '"+msgInvalidAccountEmailAddress+"'", "Actual message is '"+getText(msgXpathInvalidAccountEmailAddress)+"'",
							"",true,false);
			}
			else
			{
				writeTestResults("Verify 'login' field validation when user enter email without '.'(Test@com) sign", "Expected message is '"+msgInvalidAccountEmailAddress+"'", "Actual message is '"+getText(msgXpathInvalidAccountEmailAddress)+"'" ,
						"",false,true);
			}
		}
		else
		{
			writeTestResults("Verify 'login' field validation when user enter email without '.'(Test@com) sign", "Expected message is '"+msgInvalidAccountEmailAddress+"'", "xpath not a present or xpath("+msgXpathInvalidAccountEmailAddress+") is incorrect'",
					"",false,true);
		}	
	}
	
	public void loginInvalidUserNameAndPw()throws Exception
	{		
		fillLoginDetails("Invalid@email.com","12345678");
		
		if(isDisplayed(msgXpathInvalidLogin))
		{
			if(getText(msgXpathInvalidLogin).equalsIgnoreCase(msgInvalidUserName))
			{
				writeTestResults("Verify field validation when user type invalid email address and password", "Expected message is '"+msgInvalidUserName+"'", "Actual message is '"+getText(msgXpathInvalidLogin)+"'",
						"426f1c16",true,false);	
									
			}
			else
			{
				writeTestResults("Verify field validation when user type invalid email address and password", "Expected message is '"+msgInvalidUserName+"'", "Actual message is '"+getText(msgXpathInvalidLogin)+"'",
						"426f1c16",false,true);
			}
		}
		else
		{
			writeTestResults("Verify field validation when user type invalid email address and password", "Expected message is '"+msgInvalidUserName+"'", "xpath not a present or xpath("+msgXpathInvalidLogin+") is incorrect'",
					"426f1c16",false,true);
		}		
	}
	
	public void loginPasswordless()throws Exception
	{	
		fillLoginDetails(loginEmailAddress,"12345");
		
		if(isDisplayed(msgXpathAccountPwLess))
		{
			if(getText(msgXpathAccountPwLess).equalsIgnoreCase(msgAccountPwLess))
			{
				writeTestResults("Verify field validation when user enter password less then 6 characters", "Expected message is '"+msgAccountPwLess+"'", "Actual message is '"+getText(msgXpathAccountPwLess)+"'",
						"",true,false);
			}
			else
			{
				writeTestResults("Verify field validation when user enter password less then 6 characters", "Expected message is '"+msgAccountPwLess+"'", "Actual message is '"+getText(msgXpathAccountPwLess)+"'",
						"",false,true);
			}
		}
		else
		{
			writeTestResults("Verify field validation when user enter password less then 6 characters", "Expected message is '"+msgAccountPwLess+"'", "xpath not a present or xpath("+msgXpathAccountPwLess+") is incorrect'",
					"",false,true);
		}	
	}
	
	public void loginWithoutPW()throws Exception
	{	
		fillLoginDetails(loginEmailAddress,"");
		
		if(isDisplayed(msgXpathPassword))
		{
			if(getText(msgXpathPassword).equalsIgnoreCase(msgPasswordRequired))
			{
				writeTestResults("Verify empty field validation when the user click login button without 'Password'", "Expected message is '"+msgPasswordRequired+"'", "Actual message is '"+getText(msgXpathPassword)+"'",
						"",true,false);
			}
			else
			{
				writeTestResults("Verify empty field validation when the user click login button without 'Password'", "Expected message is '"+msgPasswordRequired+"'", "Actual message is '"+getText(msgXpathPassword)+"'",
						"",false,true);
			}
		}
		else
		{
			writeTestResults("Verify empty field validation when the user click login button without 'Password'", "Expected message is '"+msgPasswordRequired+"'", "xpath not a present or xpath("+msgXpathPassword+") is incorrect'",
					"",false,true);
		}			
	}
	
	public void loginInvalidUserName()throws Exception
	{		
		fillLoginDetails("Invalid@email.com",loginPassword);
		
		if(isDisplayed(msgXpathInvalidLogin))
		{
			if(getText(msgXpathInvalidLogin).equalsIgnoreCase(msgInvalidUserName))
			{
				writeTestResults("Verify field validation when user type invalid email address", "Expected message is '"+msgInvalidUserName+"'", "Actual message is '"+getText(msgXpathInvalidLogin)+"'",
						"",true,false);	
									
			}
			else
			{
				writeTestResults("Verify field validation when user type invalid email address", "Expected message is '"+msgInvalidUserName+"'", "Actual message is '"+getText(msgXpathInvalidLogin)+"'",
						"",false,true);
			}
		}
		else
		{
			writeTestResults("Verify field validation when user type invalid email address", "Expected message is '"+msgInvalidUserName+"'", "xpath not a present or xpath("+msgXpathInvalidLogin+") is incorrect'",
					"",false,true);
		}		
	}
	
	public void loginInvalidPW()throws Exception
	{			
		fillLoginDetails(loginEmailAddress,"invalidPW");
		
		if(isDisplayed(msgXpathInvalidLogin))
		{
			if(getText(msgXpathInvalidLogin).equalsIgnoreCase(msgInvalidPw))
			{
				writeTestResults("Verify field validation when user type invalid password", "Expected message is '"+msgInvalidPw+"'", "Actual message is '"+getText(msgXpathInvalidLogin)+"'" ,
						"",true,false);
			}
			else
			{
				writeTestResults("Verify field validation when user type invalid password", "Expected message is '"+msgInvalidPw+"'", "Actual message is '"+getText(msgXpathInvalidLogin)+"'",
						"",false,true);
			}
		}
		else
		{
			writeTestResults("Verify field validation when user type invalid password", "Expected message is '"+msgInvalidPw+"'", "xpath not a present or xpath("+msgXpathInvalidLogin+") is incorrect'",
					"",false,true);
		}		
	}
	
	public void navigateToForgetPasswordPage()throws Exception
	{	
		if(!isDisplayed(linkForgotPw))
		{
			common.navigateToLoginPage();
		}	
		
		if(isElementPresent(linkForgotPw))
		{		
			click(linkForgotPw);
			
			if(isElementPresent(txtForgotPwEmail))
			{	
				writeTestResults("Verify the user can access the Forgot password page ", "Should be redirect to the Forgot password page", "It's redirect to the Forgot password page","",true,false);
				
				System.out.println("Navigate to forget password page");
			}
			else
			{
				writeTestResults("Verify the user can access the Forgot password page ", "Should be redirect to the Forgot password page", "It's not redirect to the Forgot password page."+getTitle(),"",false,true);
			}			
		}
		else
		{
			navigateToForgotPasswordPage();
			
			if(!isElementPresent(linkForgotPw))
			{
				writeTestResults("Verify the user can access the Forgot password page ", "Should be redirect to the Forgot password page", "Forgot password link is not present or incorrct link in property file("+linkForgotPw+")","",false,true);
			}
		}		
	}
	
	public void forgotPWInvalidEmail1()throws Exception
	{			
		fillForgotPwDetails("Test.com");
				
		if(isDisplayed(msgXpathForgotPwEmailValidation))
		{
			if(getText(msgXpathForgotPwEmailValidation).equalsIgnoreCase(msgForgotPwEmailValidation))
			{
				writeTestResults("Verify 'Forgot Password' field validation when user enter email without '@'(Test.com) sign", "Expected message is '"+msgForgotPwEmailValidation+"'", "Actual message is '"+getText(msgXpathForgotPwEmailValidation)+"'" ,"",true,false);
			}
			else
			{
				writeTestResults("Verify 'Forgot Password' field validation when user enter email without '@'(Test.com) sign", "Expected message is '"+msgForgotPwEmailValidation+"'", "Actual message is '"+getText(msgXpathForgotPwEmailValidation)+"'","",false,true);
			}
		}
		else
		{
			writeTestResults("Verify 'Forgot Password' field validation when user enter email without '@'(Test.com) sign", "Expected message is '"+msgForgotPwEmailValidation+"'", "xpath not a present or xpath("+msgXpathForgotPwEmailValidation+") is incorrect'","",false,true);
		}		
	}
	
	public void forgotPWInvalidEmail2()throws Exception
	{		
		fillForgotPwDetails("Test@com");
		
		if(isDisplayed(msgXpathForgotPwEmailValidation))
		{
			if(getText(msgXpathForgotPwEmailValidation).equalsIgnoreCase(msgForgotPwEmailValidation))
			{
				writeTestResults("Verify 'Forgot Password' field validation when user enter email without '.'(Test@com) sign", "Expected message is '"+msgForgotPwEmailValidation+"'", "Actual message is '"+getText(msgXpathForgotPwEmailValidation)+"'","",true,false);
			}
			else
			{
				writeTestResults("Verify 'Forgot Password' field validation when user enter email without '.'(Test@com) sign", "Expected message is '"+msgForgotPwEmailValidation+"'", "Actual message is '"+getText(msgXpathForgotPwEmailValidation)+"'","",false,true);
			}
		}
		else
		{
			writeTestResults("Verify 'Forgot Password' field validation when user enter email without '.'(Test@com) sign", "Expected message is '"+msgForgotPwEmailValidation+"'", "xpath not a present or xpath("+msgXpathForgotPwEmailValidation+") is incorrect'","",false,true);
		}		
	}
	
	public void forgotPWWithoutEmail()throws Exception
	{	
		fillForgotPwDetails("");
		
		if(isDisplayed(msgXpathForgotPwEmailRequired))
		{
			if(getText(msgXpathForgotPwEmailRequired).equalsIgnoreCase(msgForgotPwEmailRequired))
			{
				writeTestResults("Verify 'Forgot Password' field validation when user enter submit button without email address", "Expected message is '"+msgForgotPwEmailRequired+"'", "Actual message is '"+getText(msgXpathForgotPwEmailRequired)+"'" ,"",true,false);
			}
			else
			{
				writeTestResults("Verify 'Forgot Password' field validation when user enter submit button without email address", "Expected message is '"+msgForgotPwEmailRequired+"'", "Actual message is '"+getText(msgXpathForgotPwEmailRequired)+"'","",false,true);
			}
		}
		else
		{
			writeTestResults("Verify 'Forgot Password' field validation when user enter submit button without email address", "Expected message is '"+msgForgotPwEmailRequired+"'", "xpath not a present or xpath("+msgXpathForgotPwEmailRequired+") is incorrect'","",false,true);
		}
	}
	
	public void forgotPWWithInvalidEmail()throws Exception
	{		
		fillForgotPwDetails("invalid@test.com");
				
		if(isDisplayed(msgXpathForgotPwFail))
		{
			writeTestResults("Verify the forgotten password section email field text box validation for unrecognized email address","User can enter email address","User can enter email address",
					"426f2e68",true,false);
						
			if(getText(msgXpathForgotPwFail).equalsIgnoreCase(msgForgotPwFail))
			{
				writeTestResults("Verify the forgotten password section email field for unrecognized email address", "Expected message is '"+msgForgotPwFail+"'", "Actual message is '"+getText(msgXpathForgotPwFail)+"'",
						"426f32d2",true,false);
			}
			else
			{
				writeTestResults("Verify the forgotten password section email field for unrecognized email address", "Expected message is '"+msgForgotPwFail+"'", "Actual message is '"+getText(msgXpathForgotPwFail)+"'",
						"426f32d2",false,true);
			}
		}
		else
		{
			writeTestResults("Verify the forgotten password section email field for unrecognized email address", "Expected message is '"+msgForgotPwFail+"'", "xpath not a present or xpath("+msgXpathForgotPwFail+") is incorrect'" ,
					"426f32d2",false,true);
		}		
	}
	
	public void forgotPWWithValidEmail()throws Exception
	{	
		fillForgotPwDetails(forgotPwEmailAddress);
		
		if(isDisplayed(msgXpathForgotPwSuccess))
		{
			writeTestResults("Verify the forgotten password section email field text box validation for valid email address","User can enter email address","User can enter email address",
					"426f2238",true,false);			
			
			if(getText(msgXpathForgotPwSuccess).equalsIgnoreCase(msgForgotPwSuccess))
			{
				writeTestResults("Verify the forgotten password with correct email", "Expected message is '"+msgForgotPwSuccess+"'", "Actual message is '"+getText(msgXpathForgotPwSuccess)+"'",
						"426f2576",true,false);
			}
			else
			{
				writeTestResults("Verify the forgotten password with correct email", "Expected message is '"+msgForgotPwSuccess+"'", "Actual message is '"+getText(msgXpathForgotPwSuccess)+"'",
						"426f2576",false,true);
			}
		}
		else			
		{
			writeTestResults("Verify the forgotten password with correct email", "Expected message is '"+msgForgotPwSuccess+"'", "xpath not a present or xpath("+msgXpathForgotPwSuccess+") is incorrect'",
					"426f2576",false,true);
		}		
	}
	
	public void ConfirmationMaiReader() throws Exception
	{		
		String customerName = forgotPwFirstName;
		Thread.sleep(10000);
		mailRead(forgotPwMailSubject,customerName,forgotPwEmailAddress,"19881106");
		textMail =bodyText;
		System.out.println("textMail  "+textMail);
		System.out.println("customerName"+customerName);			
		
		if(textMail.toLowerCase().indexOf(customerName.toLowerCase())>-1)
		{
			writeTestResults("Verify  the Email of the forgotten password", "Reset password mail should receive to gmail account", "Reset password mail is Received",
					"426f283c",true,false);
		}
		else
		{
			writeTestResults("Verify  the Email of the forgotten password", "Reset password mail should receive to gmail account", "Reset password mail is not Received or subject name is incorrect",
					"426f283c",false,true);
		}		
	}	
	
	public void loginUsingForgotPW()throws Exception
	{	
		//\/customer\/account\/resetpassword\/\?id=.+"\s
		    String getForgotPWLink = common.siteUrl+textMail.substring(textMail.indexOf("customer/account/resetpassword/")+0,textMail.indexOf("\" style=3D\"color=:#1E7EC8;")).trim();
		
			openPage(getForgotPWLink);
				
			if(isElementPresent(txtForgotPw))
			{
				sendKeys(txtForgotPw, forgotPw);
			}
			else
			{
				writeTestResults("Verify the Forgot Password textbox", "Forgot Password text box should be displayed", "Forgot Password text box is not displayed","",false,true);
			}
			
			if(isElementPresent(txtForgotPwConfirm))
			{
				sendKeys(txtForgotPwConfirm, forgotConfirmPw);					
			}
			else
			{
				writeTestResults("Verify the Forgot Confirm Password textbox", "Forgot Confirm Password text box should be displayed", "Forgot Confirm Password text box is not displayed","",false,true);
			}
			
			if(isElementPresent(btnForgotPwSubmit))
			{
				click(btnForgotPwSubmit);
			}
			else
			{				
				writeTestResults("Verify the submit button in reset password screen", "Submit should be displayed", "Submit is not displayed","",false,true);
			}
			
			if(getPageSource("Your password has been updated"))
			{
				writeTestResults("Verify the system allows user to update password Section", "user should be able to update password", "User can update the password","",true,false);
			}
			else
			{
				writeTestResults("Verify the system allows user to update password Section", "User should be able to update password", "User can't update the password","",false,true);
			}
			
			clearCookies();
	}	
		
	public void myAccountPageTitle()throws Exception
	{	
		common.accountLinksInUnderMyAcc();
		
		if(isElementPresent(linkLogOut))
		{
			writeTestResults("Verify the system allows user to log in to the system","User should login to the system and My Account Section should be enabled for the user" , "User can login to the system.",
					"426f35fc",true,false);
		
			if(!getTitle().equalsIgnoreCase(titleMyAccount))
			{
				writeTestResults("Verify the 'My Account' page title", "Expected 'My Account' page title is "+titleMyAccount , "Actual 'My Account' page title is "+getTitle(),"",false,true);				
			}
			else
			{
				writeTestResults("Verify the 'My Account' page title", "Expected 'My Account' page title is "+titleMyAccount , "Actual 'My Account' page title is "+getTitle(),"",true,false);
			}	
		}

	}
	
	public void accountDashboard()throws Exception
	{		
		if(getPageSource("Thank you for editAccounting with"))		
		{
			writeTestResults("Verify the User Creation Porcess", "User should sueccfully Created and load my account page", "User has sueccfully Created and load my account page","",true,false);	
			
			String userName = getText(lbAccountUserName);
			
			String acUserName = userCreationTestData.userCreationFirstName+" "+userCreationTestData.userCreationLastName;
			
			if(userName.indexOf(acUserName)>-1)
			{
				writeTestResults("Verify user name in the account dashboard", "User name should be displayed in 'My Account'", "User name is displayed in 'My Account",
						"",true,false);
			}
			else
			{
				writeTestResults("Verify user name in the account dashboard", "User name should be displayed in 'My Account'", "User name is not displayed in 'My Account",
						"",false,true);
			}
		}
	}
	
	public void userName()throws Exception
	{
		
		String wellComeName = myAccountFirstName + "\n" +loginEmailAddress ;
		

		String actualUserName = driver.findElement(By.xpath(lbAccountUserName)).getText(); 
		
		
		 if(wellComeName.toLowerCase().indexOf(actualUserName.toLowerCase()) != -1)
		 {
			 writeTestResults("Verify the welcome message is displayed correctly", "Expected user name is "+ wellComeName, actualUserName+" is displayed in 'My Account","426f578a",true,false);
		
		 }
		  
		 else
			{
				writeTestResults("Verify the welcome message is displayed correctly", "Expected user name is "+ wellComeName, "Actual user name is "+actualUserName,"426f578a",false,true);
			}
		//String userName = getUserName();
		
		//if(userName.toLowerCase().trim().equalsIgnoreCase(exUserName.toLowerCase().trim()))
		 	//{
			//writeTestResults("Verify the welcome message is displayed correctly", "Expected user name is "+exUserName, userName+" is displayed in 'My Account","426f578a",true,false);
		 //	}
		//else
		//{
			//writeTestResults("Verify the welcome message is displayed correctly", "Expected user name is "+exUserName, "Actual user name is "+userName,"426f578a",false,true);
		//}
	}
	
	public String getUserName()throws Exception
	{	
		String userName ="";
		
		if(isElementPresent(lbAccountUserName))
		{
			String acUserName =getText(lbAccountUserName).toLowerCase();
			
			System.out.println("acUserName"+acUserName);
			
			if(acUserName.indexOf("hey")>-1 && acUserName.indexOf("!")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hey ")+3,acUserName.indexOf("!"));
			}
			else if(acUserName.indexOf("hey")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hey ")+3);	
			}
			else if(acUserName.indexOf("hello,")>-1 && acUserName.indexOf("!")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hello,")+6,acUserName.indexOf("!"));
			}
			else if(acUserName.indexOf("hello")>-1 && acUserName.indexOf("!")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hello")+5,acUserName.indexOf("!"));
			}
			else if(acUserName.indexOf("hey")>-1 && acUserName.indexOf(",")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hey")+3,acUserName.indexOf(","));
			}
			else if(acUserName.indexOf("hi,")>-1 && acUserName.indexOf("!")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hi,")+3,acUserName.indexOf("!"));
			}
			else if(acUserName.indexOf("hi")>-1 && acUserName.indexOf("!")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hi")+2,acUserName.indexOf("!"));
			}
			else if(acUserName.indexOf("hi,")>-1 && acUserName.indexOf(",")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hi,")+3,acUserName.indexOf(", welcome"));
			}
			else if(acUserName.indexOf("hi")>-1 && acUserName.indexOf(",")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hi")+2,acUserName.indexOf(","));
			}
			else if(acUserName.indexOf("hello,")>-1)
			{
				userName = acUserName.substring(acUserName.indexOf("hello,")+6);
			}
			
			System.out.println("userName"+userName);			
		}
		else
		{
			writeTestResults("Verify the welcome message is displayed correctly", 
					"Welcome message should be displayed in 'My Account'", 
					"user name is not displayed or user name xpath is incorrect "+lbAccountUserName,"426f578a",false,true);
		}	
		return userName;
	}
	
	public void accountInformationBreadcrumbs()throws Exception
	{		
		String breadcrumbs = "";
				
		if(isElementPresent(lbAccountBreadcrumb))		
		{
			breadcrumbs = getText(lbAccountBreadcrumb);
			
			System.out.println("breadcrumbs"+breadcrumbs);
			
			if(breadcrumbs.equalsIgnoreCase("Home My Account Account Information"))
			{
				writeTestResults("Verify the forgotten password with correct email", "Expected message is '"+msgForgotPwSuccess+"'", "Actual message is '"+getText(msgXpathForgotPwSuccess)+"'","",true,false);
			}
			else
			{
				writeTestResults("Verify the Breadcrumb is correctly shown in the Account information page", "Breadcrumb should be correct ", "Breadcrumb is wrong.Following breadcrumb is displayed. '"+breadcrumbs+"'","",false,false);
			}
		}
		else
		{
			writeTestResults("Verify the Breadcrumb is correctly shown in the Account information page", "Breadcrumb should be correct ", "Breadcrumb tag is not displayed","",false,false);
		}					
	}	
	
	public void myAccountLinks1(String sheetName) throws Exception 
	{	
		readDataFileName = sheetName;
		linkPosition = sheetName.replace("Links", "").toLowerCase();
		String winHandleBefore ="";
		String pageTitle = "";
		String pageURL = "";
		beforeClickUrl = null;
		int j=0;
		int totalLinks = 0;
		//goBackToHomePage();
		int failCount = 0;
		
		readParametrs.getLinksFromDataFile(readDataFileName);
		readParametrs.getLinksXpath(readDataFileName);
		
		for(int k =0;k<readParametrs.totalNoOfColoums;k++)
		{			
			if(k>0)
			{
				getLinkXpath = readParametrs.linksXpathName.get(k).toString().trim();
				
				totalLinks = Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k).toString().trim())-Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k-1).toString().trim());
				
				//totalLinks = totalLinks + 1;
				
				int previousSize = Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k-1).toString().trim());
				
				for(j=totalLinks;j>1;j--)
				{	
					setLinkXpath = findElementInXLSheet(getParameterXpath,"my account links xpath").trim().replace("{id}", "["+(j-1)+"]");
					
					if(!isElementPresent(setLinkXpath))
					{
						openPage(common.siteUrl);
						
						navigateToMyAccount();
					}
					if(!isElementPresent(setLinkXpath))
					{
						writeTestResults("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' xpath present in the page",setLinkXpath+" xpath should be displayed in the page" 
								,setLinkXpath+" xpath is not displayed","",false,false);
					}
					else
					{
						getTextValue = getText(setLinkXpath);

						if(getTextValue.equalsIgnoreCase(""))
						{
							getTextValue = getAttribute(setLinkXpath,"title");
						}
						if(!readParametrs.nameOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j))).toString().trim().equalsIgnoreCase(getTextValue))
						{
								writeTestResults("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j)))+"' text is present in the correct place","'"+ 
										readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text should displayed in the "+linkPosition ,"'"+
												readParametrs.nameOfLink.get((totalLinks+previousSize)-j)+" text is not displayed in correct place","",false,false);
								failCount++;	
						}
							else
							{
								beforeClickUrl = getCurrentUrl();
								
								click(setLinkXpath);
								
								Thread.sleep(100);
								if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
								{
									openPage(getAttribute(setLinkXpath,"href"));
								}
								
								if(getCurrentUrl().indexOf("/#")>-1)
								{
									writeTestResults("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is redirects to the correct content page.",
											"Expected title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is "+readParametrs.titleOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)),
											"Link is not working","",false,false);
									failCount++;
								}
								else
								{
									 pageTitle = getTitle();
									 pageURL =getCurrentUrl();
									 
									 if(driver.getWindowHandles().size()>1)
									 {
										 winHandleBefore = driver.getWindowHandle();
											
										 for(String winHandle : driver.getWindowHandles()){
										      driver.switchTo().window(winHandle);
										      pageTitle = getTitle();
											  pageURL =getCurrentUrl();
											  
										  }		
										  driver.close();
		
										  driver.switchTo().window(winHandleBefore);
 
									 }	
									  
									if(!pageTitle.equalsIgnoreCase(readParametrs.titleOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j))).toString().trim()))
									{	
										writeTestResults("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is redirects to the correct content page.",
												"Expected title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' l" +"ink is "+readParametrs.titleOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)),
												"Actual title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j))+"' link is '"+pageTitle +"' and page url is "+pageURL,"",false,false);
										failCount++;
										}
									
									else
									{
										writeTestResults("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"
												+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is "+readParametrs.titleOfLink.get((previousSize+totalLinks-1)-
														(totalLinks-j)),"Actual title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j))+"' link is "+pageTitle,"",true,false);	
									}
								}									
							}							
						}							
						
				}
			}			
		}
		if(failCount == 0)
		{
			writeTestResults("","","","42715d32",true,false);
		}
		else
		{
			writeTestResults("","","","42715d32",false,false);
		}
	}
	
	public void myAccountLinks(String sheetName) throws Exception 
	{	
		readDataFileName = sheetName;
		linkPosition = sheetName.replace("Links", "").toLowerCase();
		String winHandleBefore ="";
		String pageTitle = "";
		String pageURL = "";
		beforeClickUrl = null;
		int j=0;
		int totalLinks = 0;
		//goBackToHomePage();
		
		readParametrs.getLinksFromDataFile(readDataFileName);
		readParametrs.getLinksXpath(readDataFileName);
		System.out.println("ddd"+readParametrs.totalNoOfColoums);
		
		for(int k =0;k<readParametrs.totalNoOfColoums;k++)
		{			
			System.out.println("K  "+k);
			
			
			if(k>0)
			{
				getLinkXpath =readParametrs.linksXpathName.get(k).toString().trim();
				
				totalLinks = Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k).toString().trim())-Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k-1).toString().trim());
				
				int previousSize = Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k-1).toString().trim());
				
				System.out.println("previousSize"+previousSize);
				System.out.println("totalLinks"+totalLinks);
				
				for(j=totalLinks;j>1;j--)
				{
					
					System.out.println("totalLinks"+totalLinks);
					/*if(j==1)
					{
						setLinkXpath = findElementInXLSheet(getParameterXpath,getLinkXpath).trim().replace("{id}", "");
						
						if(!isElementPresent(setLinkXpath))
						{
							wResult.writeTestResult("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-1).toString().trim()+"' xpath present in the page",setLinkXpath+" xpath should be displayed in the page" ,setLinkXpath+" xpath is not displayed", false, writeFileName);
							
						}
						else
						{
							
							getTextValue = getText(setLinkXpath);
							System.out.println("getText"+getTextValue);
							

							if(getTextValue.equalsIgnoreCase(""))
							{
								getTextValue = getAttribute(setLinkXpath,"title");
							}
							System.out.println("getText1"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j));
							
							if(!(readParametrs.nameOfLink.get((totalLinks+previousSize)-j)).toString().trim().equalsIgnoreCase(getTextValue))
							{								
								wResult.writeTestResult("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text is present in the correct place","'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text should displayed in the "+linkPosition ,"'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+" text is not displayed in correct place", false, writeFileName);
						
							}
							else
							{
								//wResult.writeTestResult("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text is present in the correct place","'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text should displayed in the "+linkPosition ,"'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text is displayed in the "+linkPosition, true, writeFileName);
								beforeClickUrl = getCurrentUrl();
								
								click(setLinkXpath);
																
								Thread.sleep(100);
								if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
								{
									openPage(getAttribute(setLinkXpath,"href"));
								}
								
								if(getCurrentUrl().indexOf("/#")>-1){
									
									wResult.writeTestResult("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' link is "+readParametrs.titleOfLink.get((totalLinks+previousSize)-j),"Link is not working", false, writeFileName);
									captureScreenShot(wResult.readTime+"-"+writeFileName+"-"+"Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim().replaceAll("[^\\w\\s]","")+"' link");	
								}
								else
								{
									 pageTitle = getTitle();
									 pageURL =getCurrentUrl();
									 if(driver.getWindowHandles().size()>1)
									 {
										 winHandleBefore = driver.getWindowHandle();

										 for(String winHandle : driver.getWindowHandles()){
										      driver.switchTo().window(winHandle);
										      pageTitle = getTitle();
											  pageURL =getCurrentUrl();
											 
										  }	
										  driver.close();
		
										  driver.switchTo().window(winHandleBefore);
 
									 }	
									  
									if(!pageTitle.equalsIgnoreCase(readParametrs.titleOfLink.get((totalLinks+previousSize)-j).toString().trim()))
									{											
										wResult.writeTestResult("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' link is "+readParametrs.titleOfLink.get((totalLinks+previousSize)-j),"Actual title of the '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j)+"' link is '"+pageTitle +"' and page url is "+pageURL, false, writeFileName);
										captureScreenShot(wResult.readTime+"-"+writeFileName+"-"+"Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim().replaceAll("[^\\w\\s]","")+"' link");	
									}
									else
									{
											
										wResult.writeTestResult("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' link is "+readParametrs.titleOfLink.get((totalLinks+previousSize)-j),"Actual title of the '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j)+"' link is "+pageTitle, true, writeFileName);
																		
									}
								}
								//goBack();
								//goBackToHomePage();
						
							}	
							
						}

						
					}*/
					System.out.println("xxxx"+j);
					//if(j>1)
					if(j>0)
					{
						setLinkXpath = findElementInXLSheet(getParameterXpath,getLinkXpath).trim().replace("{id}", "["+(j-1)+"]");
						
						System.out.println("setLinkXpath"+setLinkXpath);
						
						if(!isElementPresent(setLinkXpath))
						{
							wResult.writeTestResult("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' xpath present in the page",setLinkXpath+" xpath should be displayed in the page" ,setLinkXpath+" xpath is not displayed", false, writeFileName);
						
						}
						else
						{
							getTextValue = getText(setLinkXpath);
							System.out.println("getText"+getTextValue);
							

							if(getTextValue.equalsIgnoreCase(""))
							{
								getTextValue = getAttribute(setLinkXpath,"title");
							}
							
							System.out.println("getText1"+readParametrs.nameOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j))));
							
						
							if(!readParametrs.nameOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j))).toString().trim().equalsIgnoreCase(getTextValue))
							{
								
								wResult.writeTestResult("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j)))+"' text is present in the correct place","'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text should displayed in the "+linkPosition ,"'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j)+" text is not displayed in correct place", false, writeFileName);
								
							}
							else
							{
								//wResult.writeTestResult("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j)+"' text is present in the correct place","'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text should displayed in the "+linkPosition ,"'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j)+"' text is displayed in the "+linkPosition, true, writeFileName);
								beforeClickUrl = getCurrentUrl();
								
								click(setLinkXpath);
								
								Thread.sleep(100);
								if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
								{
									openPage(getAttribute(setLinkXpath,"href"));
								}
								
								if(getCurrentUrl().indexOf("/#")>-1){
									wResult.writeTestResult("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is "+readParametrs.titleOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)),"Link is not working", false, writeFileName);
									captureScreenShot(wResult.readTime+"-"+writeFileName+"-"+"Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim().replaceAll("[^\\w\\s]","")+"' link");
								}
								else
								{
									 pageTitle = getTitle();
									 pageURL =getCurrentUrl();
									 
									 if(driver.getWindowHandles().size()>1)
									 {
										 winHandleBefore = driver.getWindowHandle();
											
										 for(String winHandle : driver.getWindowHandles()){
										      driver.switchTo().window(winHandle);
										      pageTitle = getTitle();
											  pageURL =getCurrentUrl();
											  
										  }		
										  driver.close();
		
										  driver.switchTo().window(winHandleBefore);
 
									 }	
									  
									if(!pageTitle.equalsIgnoreCase(readParametrs.titleOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j))).toString().trim()))
									{											
										wResult.writeTestResult("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is "+readParametrs.titleOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)),"Actual title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j))+"' link is '"+pageTitle +"' and page url is "+pageURL, false, writeFileName);
										captureScreenShot(wResult.readTime+"-"+writeFileName+"-"+"Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim().replaceAll("[^\\w\\s]","")+"' link");										
									}
									else
									{
											
										wResult.writeTestResult("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is "+readParametrs.titleOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)),"Actual title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j))+"' link is "+pageTitle, true, writeFileName);
																				
									}
								}
									
							}
							
						}
							
							
					}
		
				}
			}
			
		}
	}
	public void myAccountLinks2(String sheetName) throws Exception 
	{	
		readDataFileName = sheetName;
		linkPosition = sheetName.replace("Links", "").toLowerCase();
		String winHandleBefore ="";
		String pageTitle = "";
		String pageURL = "";
		beforeClickUrl = null;
		int j=0;
		int totalLinks = 0;
		//goBackToHomePage();
		
		readParametrs.getLinksFromDataFile(readDataFileName);
		readParametrs.getLinksXpath(readDataFileName);
		System.out.println("ddd"+readParametrs.totalNoOfColoums);
		
		for(int k =0;k<readParametrs.totalNoOfColoums;k++)
		{			
			System.out.println("K  "+k);
			
			
			if(k>0)
			{
				getLinkXpath =readParametrs.linksXpathName.get(k).toString().trim();
				
				totalLinks = Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k).toString().trim())-Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k-1).toString().trim());
				
				int previousSize = Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k-1).toString().trim());
				
				System.out.println("previousSize"+previousSize);
				System.out.println("totalLinks"+totalLinks);
				
				for(j=totalLinks;j>1;j--)
				{
					
					System.out.println("totalLinks"+totalLinks);
					
					System.out.println("xxxx"+j);
					//if(j>1)
					if(j>0)
					{
						setLinkXpath = findElementInXLSheet(getParameterXpath,getLinkXpath).trim().replace("{id}", "["+(j)+"]");
						
						System.out.println("setLinkXpath"+setLinkXpath);
						
						if(!isElementPresent(setLinkXpath))
						{
							wResult.writeTestResult("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' xpath present in the page",setLinkXpath+" xpath should be displayed in the page" ,setLinkXpath+" xpath is not displayed", false, writeFileName);
						
						}
						else
						{
							getTextValue = getText(setLinkXpath);
							System.out.println("getText"+getTextValue);
							

							if(getTextValue.equalsIgnoreCase(""))
							{
								getTextValue = getAttribute(setLinkXpath,"title");
							}
							
							System.out.println("getText1"+readParametrs.nameOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j))));
							
						
							if(!readParametrs.nameOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j))).toString().trim().equalsIgnoreCase(getTextValue))
							{
								
								wResult.writeTestResult("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j)))+"' text is present in the correct place","'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text should displayed in the "+linkPosition ,"'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j)+" text is not displayed in correct place", false, writeFileName);
								
							}
							else
							{
								//wResult.writeTestResult("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get((totalLinks+previousSize)-j)+"' text is present in the correct place","'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j).toString().trim()+"' text should displayed in the "+linkPosition ,"'"+ readParametrs.nameOfLink.get((totalLinks+previousSize)-j)+"' text is displayed in the "+linkPosition, true, writeFileName);
								beforeClickUrl = getCurrentUrl();
								
								click(setLinkXpath);
								
								Thread.sleep(100);
								if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
								{
									openPage(getAttribute(setLinkXpath,"href"));
								}
								
								if(getCurrentUrl().indexOf("/#")>-1){
									wResult.writeTestResult("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is "+readParametrs.titleOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)),"Link is not working", false, writeFileName);
									captureScreenShot(wResult.readTime+"-"+writeFileName+"-"+"Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim().replaceAll("[^\\w\\s]","")+"' link");
								}
								else
								{
									 pageTitle = getTitle();
									 pageURL =getCurrentUrl();
									 
									 if(driver.getWindowHandles().size()>1)
									 {
										 winHandleBefore = driver.getWindowHandle();
											
										 for(String winHandle : driver.getWindowHandles()){
										      driver.switchTo().window(winHandle);
										      pageTitle = getTitle();
											  pageURL =getCurrentUrl();
											  
										  }		
										  driver.close();
		
										  driver.switchTo().window(winHandleBefore);
 
									 }	
									  
									if(!pageTitle.equalsIgnoreCase(readParametrs.titleOfLink.get(((previousSize+totalLinks-1)-(totalLinks-j))).toString().trim()))
									{											
										wResult.writeTestResult("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is "+readParametrs.titleOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)),"Actual title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j))+"' link is '"+pageTitle +"' and page url is "+pageURL, false, writeFileName);
										captureScreenShot(wResult.readTime+"-"+writeFileName+"-"+"Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim().replaceAll("[^\\w\\s]","")+"' link");										
									}
									else
									{
											
										wResult.writeTestResult("Verify "+linkPosition+", '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is redirects to the correct content page.","Expected title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)).toString().trim()+"' link is "+readParametrs.titleOfLink.get((previousSize+totalLinks-1)-(totalLinks-j)),"Actual title of the '"+readParametrs.nameOfLink.get((previousSize+totalLinks-1)-(totalLinks-j))+"' link is "+pageTitle, true, writeFileName);
																				
									}
								}
									
							}
							
						}
							
							
					}
		
				}
			}
			
		}
	}

	public void verifyAccountInformations()throws Exception
	{		
		if(!myAccountInfoLink.isEmpty())		
		{
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				
				//clickAndWait(myAccountInfoLink);
			}
			
			
			
			String wellComeName = myAccountFirstName + "\n" +loginEmailAddress ;
			String actualUserName = driver.findElement(By.xpath(lbAccountUserName)).getText(); 
			
			
			 if(wellComeName.toLowerCase().indexOf(actualUserName.toLowerCase()) != -1) 
				
			{
				 writeTestResults("Verify Account Information attributes is correctly populated", "First Name and email, should be there under account information",
							"First Name and email, display under account information",
							"426f5d70",true,false);
				
			}
				else
				{
					
					 writeTestResults("Verify Account Information attributes is correctly populated", "First Name and email, should be there under account information",
								"First Name and email, not displaying under account information",
								"426f5d70",false,true);
				}
			}
			else
			{
				writeTestResults("Verify the user entered Account Information is correctly populated", "System should display all the user entered information correctly ","First/Last name or email address not displayed",
						"426f5d70",false,false);
			}
			
		}		
	
	
	public void editAccountInformations()throws Exception
	{		
		if(!myAccountInfoLink.isEmpty())	
		{	
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				clickAndWait(myAccountInfoLink);
			}
			
			common.magentoProfilerResults("Edit account information Page");	
			
			System.out.println("Edit infor");
			
			if(isElementPresent(myAccountFirstNameXpath) && isElementPresent(myAccountLastNameXpath) && isElementPresent(editInforSaveButton))
			{
				writeTestResults("Verify edit Account Information page is correctly populated", "Account information should be displayed","Account information page is displayed",
						"426f63f6",true,false);
				
				sendKeys(myAccountFirstNameXpath,myAccountEditFirstName);
				
				sendKeys(myAccountLastNameXpath,myAccountEditLastName);
							
				clickAndWait(editInforSaveButton);
				
				submitValidation();
				
				if(!editAccountSaveMsg.isEmpty())
				{
					if(getPageSource(editAccountSaveMsg))
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "Data should be saved", "Data is saved in edit account",
								"426f6fd6",true,false);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "Data should be saved", "Data is saved message is not displayed",
								"426f6fd6",false,false);
					}
				}
							
			}
			else
			{
				writeTestResults("Verify the user entered Account Information is correctly populated", "First name and Last name and submit button should be displayed",
						"First name or last name or submit button element is not displayed","426f63f6",false,true);
			}
			
			logout();
			
			login();
			
			if(!myAccountInfoLink.isEmpty())		
			{
				System.out.println("Edit infor");
				if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
				{
					if(!isElementPresent(myAccountInfoLink))
					{
						openPage(common.siteUrl);					
						navigateToMyAccount();
					}
					clickAndWait(myAccountInfoLink);
				}
				
				if(isElementPresent(myAccountFirstNameXpath) && isElementPresent(myAccountLastNameXpath) && isElementPresent(myAccountEmailXpath))
				{
					if(!getAttribute(myAccountFirstNameXpath,"value").equalsIgnoreCase(myAccountEditFirstName))
					{
						writeTestResults("If user changed information, verify the system displays new changes correctly ", 
								"Expected first name is "+myAccountEditFirstName,"Actual first name is "+getAttribute(myAccountFirstNameXpath,"value"),
								"426f66bc",false,false);
					}
					else if(!getAttribute(myAccountLastNameXpath,"value").equalsIgnoreCase(myAccountEditLastName))
					{
						writeTestResults("If user changed information, verify the system displays new changes correctly ", "Expected last name is "+myAccountEditLastName,"Actual last name is "+getAttribute(myAccountLastNameXpath,"value"),
								"426f66bc",false,false);
					}
					
					else
					{
						writeTestResults("Changed information should be correctly populated", "System should display all the user entered information correctly ","System is displayed all the user entered information correctly ",
								"426f66bc",true,false);
					}
				}				
			}
			else
			{
				writeTestResults("Verify the user entered Account Information is correctly populated", "Edited First name and Last name should be displayed","Account information link is not displayed or xpath is incorrect",
						"426f66bc",false,true);
			}			
			if(isElementPresent(myAccountFirstNameXpath) && isElementPresent(myAccountLastNameXpath) && isElementPresent(editInforSaveButton))
			{
				sendKeys(myAccountFirstNameXpath,myAccountFirstName);
				
				sendKeys(myAccountLastNameXpath,myAccountLastName);
							
				clickAndWait(editInforSaveButton);
				
				System.out.println("Edit infor");
			}			
		}		
	}
	
	public void editPassword()throws Exception
	{
		pageRefersh();
		
		if(!myAccountInfoLink.isEmpty())		
		{
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				clickAndWait(myAccountInfoLink);
			}		
			
		}
		
		if(isElementPresent(myAccountChangePwXpath))
		{
			writeTestResults("", "","","426f731e",true,false);
			
			writeTestResults("", "","","426f7666",true,false);
			
			writeTestResults("", "","","426f7922",true,false);		
			
			click(myAccountChangePwXpath);
			
			sendKeys(myAccountCurrentPasswordXpath,loginPassword);
			
			sendKeys(myAccountPasswordXpath,myAccountEditPw);
			 
			sendKeys(myAccountConfirmPasswordXpath,myAccountEditConfirmPw);			
			
			clickAndWait(editInforSaveButton);
			
			
			
			
			click(MyAccountTestData.myAccountLink);
			clickAndWait(linkLogOut);
			Thread.sleep(6000);
			click(myAccountLinksLogOut);
			common.loginAfterPassChange();
			
		}
		else
			
		{
			writeTestResults("Verify the user entered Account Information is correctly populated", "Change password check box should be displayed","Checkbox button or submit button element is not displayed",
					"",false,true);
		}
		
		logout();
		
		login1(loginEmailAddress,myAccountEditPw);			
		
		
		if(!myAccountInfoLink.isEmpty())		
		{
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				//clickAndWait(myAccountInfoLink);
			}
			
			writeTestResults("Verify the system allow user to change the password ", "System should save the entered details ","System should allow user to login to the system using the new password ",
					"426f6cfc",true,false);
		}
		
		if(isElementPresent(myAccountChangePwXpath) )
		{
			click(myAccountChangePwXpath);
			
			sendKeys(myAccountCurrentPasswordXpath,myAccountEditConfirmPw);
			
			sendKeys(myAccountPasswordXpath,loginPassword);
			
			sendKeys(myAccountConfirmPasswordXpath,loginPassword);
			
			clickAndWait(editInforSaveButton);
		}			
	}
	
	public void FieldValidationWithoutFirstname()throws Exception 
	{
		pageRefersh();
		
		if(!myAccountInfoLink.isEmpty())		
		{
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				clickAndWait(myAccountInfoLink);
			}	
		}	
		
		if(!myAccountFirstNameXpath.trim().isEmpty())
		{
			sendKeys(myAccountFirstNameXpath,"");
			
			click(editInforSaveButton);
			
			if(isDisplayed(editAccountWithoutFirstName))
			{				
				if(getText(editAccountWithoutFirstName).equalsIgnoreCase(editAccountMsgWithoutFirstName))
				{
					writeTestResults("Verify user clicks submit button with without 'First Name' in user 'My Account ' edit information page", editAccountMsgWithoutFirstName+" message should be displayed", editAccountMsgWithoutFirstName+" message is displayed in user 'My Account ' edit information page.","",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button with without 'First Name' in user 'My Account ' edit information page", editAccountMsgWithoutFirstName+" message should be displayed", editAccountMsgWithoutFirstName+" message is not displayed in user 'My Account ' edit information page.following message got it. "+getText(editAccountWithoutFirstName),"",false,true);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button with without 'First Name' in user 'My Account ' edit information page", editAccountMsgWithoutFirstName+" message should be displayed", editAccountMsgWithoutFirstName+" message is not displayed in user 'My Account ' edit information page.following xpath is not present '"+editAccountWithoutFirstName,"",false,true);
			}
		}		
	}
	
	public void FieldValidationWithoutlastname()throws Exception 
	{
		pageRefersh();
				
		if(!myAccountInfoLink.isEmpty())		
		{
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				clickAndWait(myAccountInfoLink);
			}
		}		
		if(!myAccountLastNameXpath.isEmpty())
		{
			sendKeys(myAccountLastNameXpath,"");
			
			click(editInforSaveButton);
						
			if(isDisplayed(editAccountWithoutLastName))
			{
				if(getText(editAccountWithoutLastName).equalsIgnoreCase(editAccountMsgWithoutLastName))
				{
					writeTestResults("Verify user clicks submit button with without 'Last Name' in user 'My Account ' edit information page", editAccountMsgWithoutLastName+" message should be displayed", editAccountMsgWithoutLastName+" message is displayed in user 'My Account ' edit information page.","",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button with without 'Last Name' in user 'My Account ' edit information page", editAccountMsgWithoutLastName+" message should be displayed", editAccountMsgWithoutLastName+"  message is not displayed in user 'My Account ' edit information page.following message got it. "+getText(editAccountWithoutLastName),"",false,true);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button with without 'Last Name' in user 'My Account ' edit information page", editAccountMsgWithoutLastName+" message should be displayed", editAccountMsgWithoutLastName+" message is not displayed in user 'My Account ' edit information page.following xpath is not present '"+editAccountWithoutLastName,"",false,true);
			}	
		}
	}
	
	public void FieldValidationWithoutConfirmationPassword()throws Exception 
	{
		pageRefersh();
				
		if(!myAccountInfoLink.isEmpty())		
		{			
			System.out.println("getAttribute"+getAttribute("//*[@class='block-content']/ul/li/a_Xpath","title"));
			
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				clickAndWait(myAccountInfoLink);
			}		
			
		}	
		if(isDisplayed(myAccountChangePwXpath))
		{
			click(myAccountChangePwXpath);
		}
		else
		{
			writeTestResults("Verify user clicks submit button with without 'Confirm password' in user 'My Account ' edit information page", "Change password check box should be displayed", "Changed password check box is not displayed in user 'My Account ' edit information page","",false,true);
		}
		
		
		if(!myAccountConfirmPasswordXpath.isEmpty())
		{
			sendKeys(myAccountCurrentPasswordXpath,loginPassword);
			
			sendKeys(myAccountConfirmPasswordXpath,"");		
			
			click(editInforSaveButton);
			
			if(isDisplayed(editAccountWithoutConfirmPassword))
			{
				if(getText(editAccountWithoutConfirmPassword).equalsIgnoreCase(editAccountMsgWithoutConfirmPassword))
				{
					writeTestResults("Verify user clicks submit button with without 'Confirm password' in user 'My Account ' edit information page", editAccountMsgWithoutConfirmPassword+" message should be displayed", editAccountMsgWithoutConfirmPassword+" message is displayed in user 'My Account ' edit information page.","",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button with without 'Confirm password' in user 'My Account ' edit information page", editAccountMsgWithoutConfirmPassword+" message should be displayed", editAccountMsgWithoutConfirmPassword+" message is not displayed in user 'My Account ' edit information page.following message got it. "+getText(editAccountWithoutConfirmPassword),"",false,true);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button with without 'Confirm password' in user 'My Account ' edit information page", editAccountMsgWithoutConfirmPassword+" message should be displayed", editAccountMsgWithoutConfirmPassword+" message is not displayed in user 'My Account ' edit information page.following xpath is not present '"+editAccountWithoutConfirmPassword,"",false,true);
			}	
		}				
	}
	
	public void FieldValidationWithoutPassword()throws Exception 
	{
		pageRefersh();
				
		if(!myAccountInfoLink.isEmpty())		
		{
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				clickAndWait(myAccountInfoLink);
			}	
			
		}
		
		if(isDisplayed(myAccountChangePwXpath))
		{
			click(myAccountChangePwXpath);
		}
		else
		{
			writeTestResults("Verify user clicks submit button with without 'Password' in user 'My Account ' edit information page", "Change password check box should be displayed", "Changed password check box is not displayed in user 'My Account ' edit information page","",false,true);
		}
		
		
		if(!myAccountPasswordXpath.isEmpty())
		{
			sendKeys(myAccountCurrentPasswordXpath,loginPassword);
					
			sendKeys(myAccountPasswordXpath,"");			
			
			click(editInforSaveButton);
			
			if(isDisplayed(editAccountWithoutPassword))
			{
				if(getText(editAccountWithoutPassword).equalsIgnoreCase(editAccountMsgWithoutPassword))
				{
					writeTestResults("Verify user clicks submit button with without 'Password' in user 'My Account ' edit information page", editAccountMsgWithoutPassword+" message should be displayed", editAccountMsgWithoutPassword+" message is displayed in user 'My Account ' edit information page.","",true,false);
				}
				else
				{
					writeTestResults("Verify user clicks submit button with without 'Password' in user 'My Account ' edit information page", editAccountMsgWithoutPassword+" message should be displayed", editAccountMsgWithoutPassword+" message is not displayed in user 'My Account ' edit information page.following message got it. "+getText(editAccountWithoutPassword),"",false,true);
				}
			}
			else
			{
				writeTestResults("Verify user clicks submit button with without 'Password' in user 'My Account ' edit information page", editAccountMsgWithoutPassword+" message should be displayed", editAccountMsgWithoutPassword+" message is not displayed in user 'My Account ' edit information page.following xpath is not present '"+editAccountWithoutPassword,"",false,true);
			}	
		}	
	}
	
	public void passwordValidation1()throws Exception 
	{
		pageRefersh();
						
		if(!myAccountInfoLink.isEmpty())		
		{
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				clickAndWait(myAccountInfoLink);
			}
		}
		
		if(isDisplayed(myAccountChangePwXpath))
		{
			click(myAccountChangePwXpath);
		}
		else
		{
			writeTestResults("Verify password validation when user type less than 6 characters", "Change password check box should be displayed", "Changed password check box is not displayed in user 'My Account ' edit information page","",false,true);
		}
		
		
		if(!myAccountPasswordXpath.isEmpty())
		{
			sendKeys(myAccountCurrentPasswordXpath,loginPassword);
						
			sendKeys(myAccountPasswordXpath,"12");		
			
			clickAndWait(editInforSaveButton);
				
			if(isDisplayed(editAccountLessPassword))
			{
				if(getText(editAccountLessPassword).equalsIgnoreCase(editAccountMsgLessPassword))
				{
					writeTestResults("Verify password validation when user type less than 6 characters", editAccountMsgLessPassword+" message should be displayed", editAccountMsgLessPassword+" message is displayed in user 'My Account ' edit information page.","",true,false);
				}
				else
				{
					writeTestResults("Verify password validation when user type less than 6 characters", editAccountMsgLessPassword+" message should be displayed", editAccountMsgLessPassword+" message is not displayed in user 'My Account ' edit information page.following message got it. "+getText(editAccountLessPassword),"",false,true);
					
				}
			}
			else
			{
				writeTestResults("Verify password validation when user type less than 6 characters", editAccountMsgLessPassword+" message should be displayed", editAccountMsgLessPassword+" message is not displayed in user 'My Account ' edit information page.following xpath is not present '"+editAccountLessPassword,"",false,true);
			}		
		}	
	}
	
	public void passwordValidation2()throws Exception 
	{		
		if(!myAccountInfoLink.isEmpty())		
		{		
			if(!getAttribute(myAccountInfoLink.replace("/a", ""),"class").equalsIgnoreCase("current"))
			{
				if(!isElementPresent(myAccountInfoLink))
				{
					openPage(common.siteUrl);					
					navigateToMyAccount();
				}
				clickAndWait(myAccountInfoLink);
			}	
		
			
			if(!isDisplayed(myAccountChangePwXpath))
			{
				writeTestResults("Verify password validation when password and confirm password is mismatch", "Change password check box should be displayed", "Changed password check box is not displayed in user 'My Account ' edit information page","",false,true);
			}
			
		}
				
		if(!myAccountConfirmPasswordXpath.isEmpty())
		{
			sendKeys(myAccountCurrentPasswordXpath,loginPassword);
			
			sendKeys(myAccountPasswordXpath,"333333");
			
			sendKeys(myAccountConfirmPasswordXpath,"654321");
			
			clickAndWait(editInforSaveButton);
			
			if(isDisplayed(editAccountMissMatchPassword))
			{
				if(getText(editAccountMissMatchPassword).equalsIgnoreCase(editAccountMsgMissMatchPassword))
				{
					writeTestResults("Verify password validation when password and confirm password is mismatch", editAccountMsgMissMatchPassword+" message should be displayed", editAccountMsgMissMatchPassword+" message is displayed in user 'My Account ' edit information page.","",true,false);
				}
				else
				{
					writeTestResults("Verify password validation when password and confirm password is mismatch", editAccountMsgMissMatchPassword+" message should be displayed", editAccountMsgMissMatchPassword+" message is not displayed in user 'My Account ' edit information page.following message got it. "+getText(editAccountMissMatchPassword),"",false,true);
				}
			}
			else
			{
				writeTestResults("Verify password validation when password and confirm password is mismatch", editAccountMsgMissMatchPassword+" message should be displayed", editAccountMsgMissMatchPassword+" message is not displayed in user 'My Account ' edit information page.following xpath is not present '"+editAccountMissMatchPassword,"",false,true);
			}
		}
	}
	
	public void submitValidation() throws Exception
	{
		if(!editAccountSaveMsg.isEmpty())
		{
			if(!getPageSource(editAccountSaveMsg))
			{
				writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "Data should be saved", "Data missing in edit account","",false,true);
			}
		}
		else
		{
			if(!editAccountWithoutFirstName.isEmpty())
			{
				if(isDisplayed(editAccountWithoutFirstName))
				{
					if(getText(editAccountWithoutFirstName).equalsIgnoreCase(editAccountMsgWithoutFirstName))
					{						
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "First name text box value is empty in user 'My Account ' edit information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "First name text box value is empty and message is "+getText(editAccountWithoutFirstName),"",false,true);
					}
				}
			}
			if(!editAccountWithoutLastName.isEmpty())
			{
				if(isDisplayed(editAccountWithoutLastName))
				{
					if(getText(editAccountWithoutLastName).equalsIgnoreCase(editAccountMsgWithoutLastName))
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Last name text box value is empty in user 'My Account ' edit information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Last name text box value is empty and message is "+getText(editAccountWithoutLastName),"",false,true);
					}
				}
			}
		}				
	}
	
	public void submitValidationPassword() throws Exception
	{		
			if(!editAccountWithoutPassword.isEmpty())
			{
				if(isDisplayed(editAccountWithoutPassword))
				{
					if(getText(editAccountWithoutPassword).equalsIgnoreCase(editAccountMsgWithoutPassword))
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Password text box value is empty in user 'My Account ' edit information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Password text box value is empty and message is "+getText(editAccountWithoutPassword),"",false,true);
					}
				}
			}
			if(!editAccountLessConfirmPassword.isEmpty())
			{
				if(isDisplayed(editAccountLessConfirmPassword))
				{
					if(getText(editAccountLessConfirmPassword).equalsIgnoreCase(editAccountMsgWithoutConfirmPassword))
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Confirm password text box value is empty in user 'My Account ' edit information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Confirm password text box value is empty and message is "+getText(editAccountLessConfirmPassword),"",false,true);
					}
				}
			}
			if(!editAccountLessPassword.isEmpty())
			{
				if(isDisplayed(editAccountLessPassword))
				{
					if(getText(editAccountLessPassword).equalsIgnoreCase(editAccountMsgLessPassword))
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Password length is less 6 in user 'My Account ' edit information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Password length is less 6 and message is "+getText(editAccountLessPassword),"",false,true);
					}
				}
			}
			if(!editAccountLessConfirmPassword.isEmpty())
			{
				if(isDisplayed(editAccountLessConfirmPassword))
				{
					if(getText(editAccountLessConfirmPassword).equalsIgnoreCase(editAccountMsgLessConfirmPassword))
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Confirm password length less 6 in user 'My Account ' edit information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Confirm password length less 6 and message is "+getText(editAccountLessConfirmPassword),"",false,true);
					}
				}
			}
			if(!editAccountMissMatchPassword.isEmpty())
			{
				if(isDisplayed(editAccountMissMatchPassword))
				{
					if(getText(editAccountMissMatchPassword).equalsIgnoreCase(editAccountMsgMissMatchPassword))
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Password mismatch in user 'My Account ' edit information page.","",false,true);
					}
					else
					{
						writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' edit information page", "My Account page should be displayed", "Password mismatch and message is "+getText(editAccountMissMatchPassword),"",false,true);
					}
				}
			}
		}
	
	public void editExistAddress()throws Exception 
	{
		editAddress();	
	}
	
	public void addNewAddress()throws Exception 
	{	
		//navigate to address book
		if(!getAttribute(linkAddressBook.replace("/a", ""),"class").equalsIgnoreCase("current"))
		{
			if(!isElementPresent(linkAddressBook))
			{
				openPage(common.siteUrl);					
				navigateToMyAccount();
			}
		}
		
		if(isElementPresent(linkAddressBook))
		{
			writeTestResults("Verify Address book link", "Address book should be there", "Address book is displayed in my account",
					"426f60cc",true,false);
			
			clickAndWait(linkAddressBook);
			
			writeTestResults("", "", "","426f82b4",true,false);
			
			writeTestResults("", "", "","426f884a",true,false);
			
			deleteExistAddress();
			
			if(isDisplayed(btnNewaddress))
			{
				click(btnNewaddress);
				
				createNewAddress();		
				
				verifyAddress();							
			}
			else
			{
				//createNewAddress();		
				
				//verifyAddress();	
				writeTestResults("User can navigate to address book section", "Address information page should be displayed", "New address button is not displayed",
						"426f884a",false,true);
			}
		}
		else
		{
			writeTestResults("User can navigate to address book section", "Address information page should be displayed", "Address book link is not present or xpath is incorrect"+linkAddressBook,
					"426f884a",false,true);
		}
	}	
	
	/*private void verifyEditAddress()throws Exception 
	{
		if(isElementPresent(btnEditAddress))
		{
			click(btnEditAddress);			
			
			if(!txtNewAddressFirstName.isEmpty() && !newAddressFirstName.isEmpty())
			{
				if(isElementPresent(txtNewAddressFirstName))
				{
					if(!getAttribute(txtNewAddressFirstName,"value").equalsIgnoreCase("Edit"+newAddressFirstName))
					{
						writeTestResults("Verify the user saved address are correctly displayed after edit the address", "Expected value is "+"Edit"+newAddressFirstName, "Actual value is "+getAttribute(txtNewAddressFirstName,"value"),"",false,true);	
					}
				}
			}
			
			if(!txtNewAddressLastName.isEmpty() && !newAddressLastName.isEmpty())
			{
				if(isElementPresent(txtNewAddressLastName))
				{
					if(!getAttribute(txtNewAddressLastName,"value").equalsIgnoreCase("Edit"+newAddressLastName))
					{
						writeTestResults("Verify the user saved address are correctly displayed after edit the address", "Expected value is "+"Edit"+newAddressLastName, "Actual value is "+getAttribute(txtNewAddressLastName,"value"),"",false,true);	
					}
				}				
			}
			if(!txtNewAddressTelephone.isEmpty() && !newAddressTelephone.isEmpty())
			{
				if(isElementPresent(txtNewAddressTelephone))
				{
					if(!getAttribute(txtNewAddressTelephone,"value").equalsIgnoreCase("094"+newAddressTelephone))
					{
						writeTestResults("Verify the user saved address are correctly displayed after edit the address", "Expected value is "+"094"+newAddressTelephone, "Actual value is "+getAttribute(txtNewAddressTelephone,"value"),"",false,true);	
					}
				}	
				
			}			
			if(!txtNewAddressState.isEmpty() && !newAddressState.isEmpty())
			{
				if(isElementPresent(txtNewAddressState))
				{
					if(!getAttribute(txtNewAddressState,"value").equalsIgnoreCase(newAddressState))
					{
						writeTestResults("Verify the user saved address are correctly displayed after edit the address", "Expected value is "+"Edit"+newAddressState, "Actual value is "+getAttribute(txtNewAddressState,"value"),"",false,true);	
					}
				}				
			}
			if(!txtNewAddressStreet.isEmpty() && !newAddressStreet.isEmpty())
			{
				if(isElementPresent(txtNewAddressStreet))
				{
					if(!getAttribute(txtNewAddressStreet,"value").equalsIgnoreCase("Edit"+newAddressStreet))
					{
						writeTestResults("Verify the user saved address are correctly displayed after edit the address", "Expected value is "+"Edit"+newAddressStreet, "Actual value is "+getAttribute(txtNewAddressStreet,"value"),"",false,true);	
					}
				}			
			}
			if(!txtNewAddressCity.isEmpty() && !newAddressCity.isEmpty())
			{
				if(isElementPresent(txtNewAddressCity))
				{
					if(!getAttribute(txtNewAddressCity,"value").equalsIgnoreCase(newAddressCity))
					{
						writeTestResults("Verify the user saved address are correctly displayed after edit the address", "Expected value is "+"Edit"+newAddressCity, "Actual value is "+getAttribute(txtNewAddressCity,"value"),"",false,true);	
					}
				}
				
			}
			
			if(!txtNewAddressPostCode.isEmpty() && !newAddressPostCode.isEmpty())
			{
				if(isElementPresent(txtNewAddressPostCode))
				{
					if(!getAttribute(txtNewAddressPostCode,"value").equalsIgnoreCase(newAddressPostCode) && (getAttribute(txtNewAddressPostCode,"value").indexOf(newAddressPostCode) < 0))
					{
						writeTestResults("Verify the user saved address are correctly displayed after edit the address", "Expected value is "+newAddressPostCode, "Actual value is "+getAttribute(txtNewAddressPostCode,"value"),"",false,true);
					}
				}				
			}			
		}
		else
		{
			writeTestResults("Verify edit button in address book page", "Edit button should be displayed", "Edit button is not present or xpath is incorrect"+btnEditAddress,"",false,true);
		}
	}*/
	
	private void verifyAddressInfo()throws Exception 
	{
		if(!txtNewAddressFirstName.isEmpty() && !newAddressFirstName.isEmpty())
		{
			if(isElementPresent(txtNewAddressFirstName))
			{
				if(!getAttribute(txtNewAddressFirstName,"value").equalsIgnoreCase(newAddressFirstName))
				{
					writeTestResults("Verify the user saved address are correctly displayed after add new address", "Expected first name is "+newAddressFirstName, "Actual value is "+getAttribute(txtNewAddressFirstName,"value"),"",false,false);
				}
			}
		}		
		if(!txtNewAddressLastName.isEmpty() && !newAddressLastName.isEmpty())
		{
			if(isElementPresent(txtNewAddressLastName))
			{
				if(!getAttribute(txtNewAddressLastName,"value").equalsIgnoreCase(newAddressLastName))
				{
					writeTestResults("Verify the user saved address are correctly displayed after add new address", "Expected last name is "+newAddressLastName, "Actual value is "+getAttribute(txtNewAddressLastName,"value"),"",false,false);
				}
			}				
		}
		if(!txtNewAddressTelephone.isEmpty() && !newAddressTelephone.isEmpty())
		{
			if(isElementPresent(txtNewAddressTelephone))
			{
				if(!getAttribute(txtNewAddressTelephone,"value").equalsIgnoreCase(newAddressTelephone))
				{
					writeTestResults("Verify the user saved address are correctly displayed after add new address", "Expected telephone is "+newAddressTelephone, "Actual value is "+getAttribute(txtNewAddressTelephone,"value"),"",false,false);
				}
			}				
		}		
		/*if(!txtNewAddressState.isEmpty() && !newAddressState.isEmpty())
		{
			if(isElementPresent(txtNewAddressState))
			{
				if(!getAttribute(txtNewAddressState,"value").equalsIgnoreCase(newAddressState))
				{
					writeTestResults("Verify the user saved address are correctly displayed after add new address", "Expected state is "+newAddressState, "Actual value is "+getAttribute(txtNewAddressState,"value"),"",false,false);
				}
			}			
		}
		if(!txtNewAddressStreet.isEmpty() && !newAddressState.isEmpty())
		{
			if(isElementPresent(txtNewAddressStreet))
			{
				if(!getAttribute(txtNewAddressStreet,"value").equalsIgnoreCase(newAddressStreet))
				{
					writeTestResults("Verify the user saved address are correctly displayed after add new address", "Expected state is "+newAddressStreet, "Actual value is "+getAttribute(txtNewAddressStreet,"value"),"",false,false);
				}
			}			
		}
		if(!txtNewAddressCity.isEmpty() && !newAddressCity.isEmpty())
		{
			if(isElementPresent(txtNewAddressCity))
			{
				if(!getAttribute(txtNewAddressCity,"value").equalsIgnoreCase(newAddressCity))
				{
					writeTestResults("Verify the user saved address are correctly displayed after add new address", "Expected city is "+newAddressCity, "Actual value is "+getAttribute(txtNewAddressCity,"value"),"",false,false);
				}
			}			
		}
		
		if(!txtNewAddressPostCode.isEmpty() && !newAddressPostCode.isEmpty())
		{
			if(isElementPresent(txtNewAddressPostCode))
			{
				if(!getAttribute(txtNewAddressPostCode,"value").equalsIgnoreCase(newAddressPostCode) && (getAttribute(txtNewAddressPostCode,"value").indexOf(newAddressPostCode) < 0))
				{
					writeTestResults("Verify the user saved address are correctly displayed after add new address", "Expected postcode is "+newAddressPostCode, "Actual value is "+getAttribute(txtNewAddressPostCode,"value"),"",false,false);					
				}
			}				
		}
		*/
		if(!btnGoBack.isEmpty())
		{
			if(isElementPresent(btnGoBack))
			{
				click(btnGoBack);
			}
			else
			{
				goBack();
			}
		}
	}
	
	private void verifyAddress()throws Exception 
	{
		if(isElementPresent(btnEditAddress))
		{
			click(btnEditAddress);	
			
			verifyAddressInfo();			
			
		}
		else
		{
			if(isElementPresent(linkAddressBook))
			{
				clickAndWait(linkAddressBook);
				
				click(btnEditAddress);	
				
				verifyAddressInfo();
			}			
		}	
	}
	
	private void createNewAddress()throws Exception 
	{		
		if(!txtNewAddressFirstName.isEmpty() && !newAddressFirstName.isEmpty())
		{
			if(isDisplayed(txtNewAddressFirstName))
			{
				sendKeys(txtNewAddressFirstName,newAddressFirstName);
			}
		}		
		if(!txtNewAddressLastName.isEmpty() && !newAddressLastName.isEmpty())
		{
			if(isDisplayed(txtNewAddressLastName))
			{
				sendKeys(txtNewAddressLastName,newAddressLastName);
			}
		}
		if(!txtNewAddressTelephone.isEmpty() && !newAddressTelephone.isEmpty())
		{
			if(isDisplayed(txtNewAddressTelephone))
			{
				sendKeys(txtNewAddressTelephone,newAddressTelephone);
			}
		}
		if(!selectNewAddressCountry.isEmpty() && !newAddressCountry.isEmpty())
		{
			if(isDisplayed(selectNewAddressCountry))
			{
				selectText(selectNewAddressCountry,newAddressCountry);
			}
		}
		if(!txtNewAddressState.isEmpty() && !newAddressState.isEmpty())
		{
			if(isDisplayed(txtNewAddressState))
			{
				sendKeys(txtNewAddressState,newAddressState);
			}
		}
		if(!selectNewAddressState.isEmpty() && !newAddressState.isEmpty())
		{
			if(isDisplayed(selectNewAddressState))
			{
				selectText(selectNewAddressState,newAddressState);
			}
		}
		if(!txtNewAddressStreet.isEmpty() && !newAddressStreet.isEmpty())
		{
			if(isDisplayed(txtNewAddressStreet))
			{
				sendKeys(txtNewAddressStreet,newAddressStreet);
			}
		}
		
		if(!txtNewAddressZip.isEmpty() && !newAddressZip.isEmpty())
		{
			if(isDisplayed(txtNewAddressZip))
			{
				sendKeys(txtNewAddressZip,newAddressZip);
			}
		}
		if(!txtNewAddressCity.isEmpty() && !newAddressCity.isEmpty())
		{
			if(isDisplayed(txtNewAddressCity))
			{			
				if(!isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In My Account"))
				{
					sendKeys(txtNewAddressCity,newAddressCity);
				}
				else
				{
					sendKeys(txtNewAddressCity,newAddressCity);
				}
				
			}
		}
		if(!txtNewAddressPostCode.isEmpty() && !newAddressPostCode.isEmpty())
		{
			if(isDisplayed(txtNewAddressPostCode))
			{
				sendKeys(txtNewAddressPostCode,newAddressPostCode);
				
				if(isExecuted(getMethodIsExecuted,"Select Postcode Using Auto Suggestion In My Account"))
				{
					Thread.sleep(2000);
					
					click(selectNewAddressPostCode);					
				}				
			}
		}		
		if(isElementPresent(btnNewAddresssave))
		{
			clickAndWait(btnNewAddresssave);
			
			if(!isDisplayed(newAddressCreateSuccesXpath))
			{
				newAddressValidation();
			}
			else
			{
				writeTestResults("Verify the user can create new Address in the Address Book", "System should allow user to create new Addresses", "User can create new address","",true,false);
			}
		}
		else
		{
			writeTestResults("Verify the user can create new Address in the Address Book", "Form data and save button should be displayed", "Save button is not displayed","",false,true);
		}
	}
	
	public void deleteCreatedAddress()throws Exception 
	{
		
		if(isDisplayed(linkAddressBook))
				{
			clickAndWait(linkAddressBook);		
				}
				
		
		int i = 0;
		
		int rowCount = getRowCount(lbGetAllSavedAddresses,"li");

		System.out.println("rowCount"+rowCount);
		while(i < rowCount)
		{
			if(isElementPresent(btnDeleteAddress))
			{
				click(btnDeleteAddress);
				
				if(isAlertPresent())
				{
					acceptAlert();
				}	
				
			}
			else
			{
				break;
			}
			i++;		
		}	
		
		if(isDisplayed(btnDeleteAddress))
		{
			writeTestResults("Verify the system allows user to Delete existing Addresses", "user can delete existing address ", rowCount+" no of addresses are still exist",
					"426f858e",false,true);			
		}	
		else
		{
			writeTestResults("Verify the system allows user to Delete existing Addresses", "user can delete existing address ", rowCount+" address deleted",
					"426f858e",true,false);
		}
	}
		
	private void deleteExistAddress()throws Exception 
	{
		int i = 0;
		
		if(!lbGetAllSavedAddresses.isEmpty())
		{
			if(isElementPresent(lbGetAllSavedAddresses))
			{
				int rowCount = getRowCount(lbGetAllSavedAddresses,"li");
				
				while(i < rowCount)
				{
					if(isDisplayed(btnDeleteAddress))
					{
						click(btnDeleteAddress);
						
						if(isAlertPresent())
						{
							System.out.println("Aleart present");
							acceptAlert();
						}	
						
					}
					else
					{
						break;
					}
					i++;					
				}	
			}			
		}			
	}
		
	private void editAddress()throws Exception 
	{		
		if(!isDisplayed("//*[@id='my-account-sub-menu']/li[3]"))
		{
			clickAndWait(linkAddressBook);
		}
		
		
		
		if(isElementPresent(btnEditAddress))
		{
			clickAndWait(btnEditAddress);	
			
			editAddressInfo();			
		}
		else
		{
			if(isElementPresent(linkAddressBook))
			{
				clickAndWait(linkAddressBook);
				
				click(btnEditAddress);	
				
				editAddressInfo();
			}				
		}			
	}
	
	private void editAddressInfo()throws Exception 
	{
		writeTestResults("Verify the user can navigate to edit Address", "User should redirect to edit address page", "User is redirect to edit address page",
				"426f7c42",true,false);
		
		if(!txtNewAddressFirstName.isEmpty() && !newAddressFirstName.isEmpty())
		{
			if(isElementPresent(txtNewAddressFirstName))
			{
				sendKeys(txtNewAddressFirstName,"Edit"+newAddressFirstName);
			}
		}		
		if(!txtNewAddressLastName.isEmpty() && !newAddressLastName.isEmpty())
		{
			if(isElementPresent(txtNewAddressLastName))
			{
				sendKeys(txtNewAddressLastName,"Edit"+newAddressLastName);
			}
		}
		if(!txtNewAddressTelephone.isEmpty() && !newAddressTelephone.isEmpty())
		{
			if(isElementPresent(txtNewAddressTelephone))
			{
				sendKeys(txtNewAddressTelephone,"094"+newAddressTelephone);
			}
		}
		if(!txtNewAddressState.isEmpty() && !newAddressState.isEmpty())
		{
			if(isElementPresent(txtNewAddressState))
			{
				sendKeys(txtNewAddressState,newAddressState);
			}
		}
		if(!txtNewAddressStreet.isEmpty() && !newAddressFirstName.isEmpty())
		{
			if(isElementPresent(txtNewAddressStreet))
			{
				sendKeys(txtNewAddressStreet,"Edit"+newAddressStreet);
			}
		}
		if(!txtNewAddressCity.isEmpty() && !newAddressCity.isEmpty())
		{
			if(isElementPresent(txtNewAddressCity))
			{
				sendKeys(txtNewAddressCity,newAddressCity);
			}
		}		
		if(isElementPresent(btnNewAddresssave))
		{
			clickAndWait(btnNewAddresssave);
			
			if(!isDisplayed(newAddressCreateSuccesXpath))
			{
				newAddressValidation();
			}
			else
			{
				writeTestResults("Verify the user can edit Address in the Address Book", "System should allow user to edit Addresses", "User can edit address",
						"426f7f58",true,false);				
			}			
		}		
	}
		
	private void newAddressValidation() throws Exception 
	{		
		if(isErrorElementPresent(myAccountErrorMsg))
		{
			writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' create new address page", "User can create new address", "Folllowing error is found."+getText(myAccountErrorMsg),"",false,true);
		}
		
		if(!isDisplayed(newAddressCreateSuccesXpath))
		{
			if(!newAddressWithoutFirstName.isEmpty())
			{
				if(isDisplayed(newAddressWithoutFirstName))
				{
					writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' create new address page", "User can create new address", "Folllowing error is found."+getText(newAddressWithoutFirstName),"",false,true);
				}
			}
			if(!newAddressWithoutLastName.isEmpty())
			{
				if(isDisplayed(newAddressWithoutLastName))
				{
					writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' create new address page", "User can create new address", "Folllowing error is found."+getText(newAddressWithoutLastName),"",false,true);
				}
			}
			if(!newAddressWithoutTelephone.isEmpty())
			{
				if(isDisplayed(newAddressWithoutTelephone))
				{
					writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' create new address page", "User can create new address", "Folllowing error is found."+getText(newAddressWithoutTelephone),"",false,true);
				}
			}
			if(!newAddressWithoutStreet.isEmpty())
			{
				if(isDisplayed(newAddressWithoutStreet))
				{
					writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' create new address page", "User can create new address", "Folllowing error is found."+getText(newAddressWithoutStreet),"",false,true);					
				}
			}
			if(!newAddressWithoutCity.isEmpty())
			{
				if(isDisplayed(newAddressWithoutCity))
				{
					writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' create new address page", "User can create new address", "Folllowing error is found."+getText(newAddressWithoutCity),"",false,true);
				}
			}
			if(!newAddressWithoutPostCode.isEmpty())
			{
				if(isDisplayed(newAddressWithoutPostCode))
				{
					writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' create new address page", "User can create new address", "Folllowing error is found."+getText(newAddressWithoutPostCode),"",false,true);					
				}
			}
			if(!newAddressWithoutState.isEmpty())
			{
				System.out.println(isDisplayed(newAddressWithoutState));
				if(isDisplayed(newAddressWithoutState))
				{
					writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' create new address page", "User can create new address", "Folllowing error is found."+getText(newAddressWithoutState),"",false,true);					
				}
			}
			if(!newAddressWithoutCountry.isEmpty())
			{
				System.out.println(isDisplayed(newAddressWithoutCountry));
				if(isDisplayed(newAddressWithoutCountry))
				{
					writeTestResults("Verify user clicks submit button with correct data in user 'My Account ' create new address page", "User can create new address", "Folllowing error is found."+getText(newAddressWithoutCountry),"",false,true);
				}
			}					
		}	
	}
	
	private void getProdctsInWishList() throws Exception
	{
		int i = 0;
		
		if(!getPageSource("You have no items in your wishlist."))
		{
			if(isElementPresent(wishListTable))
			{
				int rowCount =getCount("//div[@class='tbody']/div[@class='tr']");
				
				while(i < rowCount)
				{				
					if(isElementPresent(rmvItemFromWishList))
					{
						click(rmvItemFromWishList);
						
						if(isAlertPresent())
						{
							acceptAlert();
						}	
						
					}
					else
					{
						break;
					}
					i++;		
					
				}	
			}			
		}		
	}
		
	private void navigateToMyAccount() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"Mouse Hover In My Account"))
		{
			if(!mouseMoveToMyAccount.isEmpty() && !isElementPresent(myAccountLink))
			{
				mouseMove(mouseMoveToMyAccount);
			}			
			else 
			{
				
				common.accountLinksInUnderMyAcc();
			}			
			
		}
		else
		{
			if(isElementPresent(myAccountLink))
			{
				click(myAccountLink);
			}
			else 
			{
				openPage(common.siteUrl+myAccountUrl);
			}
		}				
	}
	
	private void removeProdctsInWishList() throws Exception
	{
		navigateToMyAccount();
		
		if(!isDisplayed(txtWishListCommentBoxXpath))
		{
			if(isElementPresent(myWishlistLink))
			{
				clickAndWait(myWishlistLink);
					
					getProdctsInWishList();
								
			}
			else
			{
				writeTestResults("Verify my wishlist link in my account", "My Wishlist link should be displayed in left panel", "My wishlist link not present or " +"xpath is incorrect."+myWishlistLink,"",false,true);
			}					
		}
		else
		{
			
			getProdctsInWishList();				
		}	
	}
	
	private void verifyProductDetilsInWishlist() throws Exception
	{
		if(getText(wishListProdcutName).isEmpty())
		{
			Thread.sleep(2000);
		}		
		if(getText(wishListProdcutName).indexOf(TestCommonMethods.productName) >= 0 )
		{
			writeTestResults("Verify product name in wishlist page","Selected product name should be "+TestCommonMethods.productName, "Selected product name is "+TestCommonMethods.productName,"",true,false);
		}
		else
		{
			writeTestResults("Verify product name in wishlist page","Selected product name should be "+TestCommonMethods.productName, "Selected product name is "+getText(wishListProdcutName),"",false,false);
		}
		
		if(getText(wishListProdcutPrice).indexOf(TestCommonMethods.productPrice) >= 0 )
		{
			
			writeTestResults("Verify product price in wishlist page","Selected product price should be "+TestCommonMethods.productPrice, "Selected product price is "+TestCommonMethods.productPrice,"",true,false);
		}
		else
		{
			writeTestResults("Verify product price in wishlist page","Selected product price should be "+TestCommonMethods.productPrice, "Selected product price is "+getText(wishListProdcutPrice),"",true,false);
		}
	}
	
	public void addToWishlistLoggedUserPrDetails() throws Exception
	{
		removeProdctsInWishList();
		
		common.clickLogo();
		
		if(!common.selectBrand())
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page","User can select the product", "Selected brand is not dipalyed or page is not loaded properly","",false,true);	
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
		{
			common.selectProductFromLeftCategory();			
		}			
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			common.productGrid();
		}		
		
		
		common.navigateToProductDetailsPage();
		
		if(isElementPresent(wishListLink) || isElementPresent("//ul[@class='add-to-links']/li/a_Xpath"))
		{
			pageRefersh();
			
			beforeClickUrl = getCurrentUrl();
			
			clickAndWait(wishListLink);	
			
			if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
			{
				clickAndWait("//ul[@class='add-to-links']/li/a_Xpath");
			}
			
			
			if(getPageSource("has been added to your wishlist"))
			{
				pageRefersh();
		
				verifyProductDetilsInWishlist();
				
				verifyWishlistComments();
				
				verifyShareWishList();
			}
			else
			{
				writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page","Should navigate to 'My Wishlist' page in My Account Should display the product selected", 
						"Selected product is not displayed in 'My Wishlist' page","",false,false);
			}					
		}
		else
		{
			if(getPageSource("An error occurred while adding item to wishlist:") && getPageSource("already in wishlist"))
			{
				writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page","Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is already added to 'Wishlist' and product is displayed in 'My Wishlist'","",true,false);	
									
			}
			else
			{
				writeTestResults("Verify 'add to wishlist' link in 'Product detail' page","Wishlist link should be displayed", "Wishlist link is not present in product listing page or xpath("+wishListLink+") is incorrect","",false,true);
			}
		}	
	}
	
	public void addAllProductsInWishlistListingPage() throws Exception
	{
		removeProdctsInWishList();
		
		common.clickLogo();
		
		if(!common.selectBrand())
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product Listing page","User can select the product", "Selected brand is not dipalyed or page is not loaded properly","",false,true);
		}	
		
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			common.productGrid();
		}			
		if(isElementPresent(wishListLink))
		{
			common.getProductNameAndPriceInCategorypage(1);
			
			beforeClickUrl = getCurrentUrl();
			
			clickAndWait(wishListLink);			
			
			if(getPageSource("has been added to your wishlist"))
			{
				pageRefersh();
				
				clickAndWait(myWishlistLink);
				
				writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product Listing page","Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is displayed in 'My Wishlist' page","",true,false);	
				
				addAllToCart();		
			}
			else
			{
				writeTestResults("Verify the user can edit Address in the Address Book", "System should allow user to edit Addresses", "User can edit address","",true,false);				
			}
		}		
	}
	
	private void addAllToCart() throws Exception 
	{
		if(!isDisplayed(btnAddToCart))
		{
			writeTestResults("Verify that the user can add products into cart ","Add to cart button should be displayed", "Add to cart button is not displayed or xpath is incorrect","",false,true);	
		}
		else
		{
			clickAndWait(btnAddToCart);
			
			if(isDisplayed(common.errorMsg))
			{
				writeTestResults("Verify that the system allows user to add wish list products to shopping cart"," System should allow user to add all the products to Shopping Cart which were in the wish list", "Following error found "+getText(common.errorMsg),"",true,true);
			
			}
			else if(isDisplayed(checkoutTestData.proceedToCheckoutButtonInShoppingCart))
			{
				writeTestResults("Verify that the system allows user to add wish list products to shopping cart"," System should allow user to add all the products to Shopping Cart which were in the wish list", 
						"User can add all products to shopping bag","",true,false);	
			}
			else
			{
				writeTestResults("Verify that the system allows user to add wish list products to shopping cart"," System should allow user to add all the products to Shopping Cart which were in the wish list", "Following page found "+getTitle(),"",false,true);
			
			}		
		}				
	}
	
	public void addAllProductsInWishlistDetailsPage() throws Exception
	{
		removeProdctsInWishList();
		
		common.clickLogo();
		
		if(!common.selectBrand())
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page","User can select the product", "Selected brand is not dipalyed or page is not loaded properly","",false,true);	
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
		{
			common.selectProductFromLeftCategory();			
		}		
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			common.productGrid();
		}		
		/*if(findElementInXLSheet(getMethodIsExecuted,"Product Availability").equalsIgnoreCase("Yes"))
		{
			common.productAvailability();
		}		
		if(findElementInXLSheet(getMethodIsExecuted,"Select Product Size By Click").equalsIgnoreCase("Yes"))
		{
			common.selectProductSizeByClick();
		}*/
		
		common.navigateToProductDetailsPage();
		
		if(isElementPresent(wishListLink) || isElementPresent("//ul[@class='add-to-links']/li/a_Xpath"))
		{
			beforeClickUrl = getCurrentUrl();
			
			clickAndWait(wishListLink);	
			
			if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
			{
				clickAndWait("//ul[@class='add-to-links']/li/a_Xpath");
			}	
			if(getPageSource("has been added to your wishlist"))
			{
				pageRefersh();
				
				
				
				writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
						"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is displayed in 'My Wishlist' page","",true,false);	
				
				addAllToCart();
			}
			else
			{
				writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
						"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is not displayed in 'My Wishlist' page","",true,false);	
			}				
		}
		else
		{
			writeTestResults("Verify 'add to wishlist' link in 'Product detail' page","Wishlist link should be displayed", "Wishlist link is not present in product listing page or xpath("+wishListLink+") is incorrect","",false,true);
		}
		if(getPageSource("An error occurred while adding item to wishlist:") && getPageSource("already in wishlist"))
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
					"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is already added to 'Wishlist' and product is displayed in 'My Wishlist'","",true,false);
		}			
	}
	
	public void addToWishlistLoggedUserPrListing() throws Exception
	{	
		removeProdctsInWishList();
		
		common.clickLogo();
		
		if(!common.selectBrand())
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product Listing page","User can select the product", "Selected brand is not dipalyed or page is not loaded properly","",false,true);
		}			
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			common.productGrid();
		}	
		
		if(isElementPresent(wishListLink))
		{
			common.getProductNameAndPriceInCategorypage(1);
			
			beforeClickUrl = getCurrentUrl();
			
			clickAndWait(wishListLink);
			
			if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
			{
				clickAndWait("//ul[@class='add-to-links']/li/a_Xpath");
			}
			
			if(getPageSource("has been added to your wishlist"))
			{
				pageRefersh();
				
				clickAndWait(myWishlistLink);
				
				writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product Listing page",
						"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is displayed in 'My Wishlist' page","",true,false);	
				
				verifyProductDetilsInWishlist();
				
				verifyWishlistComments();
				
				verifyShareWishList();
			}
			else
			{
				if(getPageSource("An error occurred while adding item to wishlist:") && getPageSource("already in wishlist"))
				{
					writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
							"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is already added to 'Wishlist' and product is displayed in 'My Wishlist'","",true,false);
				}
				else
				{
					writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product listing page",
							"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is not displayed in 'My Wishlist' page","",false,true);
				}			
			}				
		}
		else
		{
			writeTestResults("Verify 'add to wishlist' link in 'Product Listing' page","Wishlist link should be displayed", "Wishlist link is not present in product listing page or xpath("+wishListLink+") is incorrect","",false,true);
		}		
	}
	
	private void verifyWishlistComments() throws Exception 
	{
		if(!isDisplayed(txtWishListCommentBoxXpath) || !isDisplayed(btnWishListUpdate))
		{
			writeTestResults("Verify that the user can add some comments to Wish list ","Comment box and update button should be displayed", "Comment box or update button is not displayed or xpath incorrect","",false,true);
		}
		else
		{
			sendKeys(txtWishListCommentBoxXpath,wishListCommentBox);
			
			clickAndWait(btnWishListUpdate);
			
			if(getAttribute(txtWishListCommentBoxXpath,"value").equalsIgnoreCase(wishListCommentBox))
			{
				writeTestResults("Verify that the user can add some comments to Wish list ","System should allow user to add comments & Save ","Following comment is displayed in the comment box.'"+getAttribute(txtWishListCommentBoxXpath,"value")+"'","",true,false);	
			}
			else
			{
				writeTestResults("Verify that the user can add some comments to Wish list ","System should allow user to add comments & Save ", 
						"Following comment is displayed in the comment box. '"+getAttribute(txtWishListCommentBoxXpath,"value")+"'","",false,false);
			}
		}		
	}
	
	private void navigateToShareWishList() throws Exception 
	{
		
		
		if(isDisplayed(btnShareWishlist))
		{
			clickAndWait(btnShareWishlist);
			
			validateShareWishlist();
			
			verifyShareWishlist();
			
		}
		else
		{
			writeTestResults("Verify share wishlist button in wishlist page", "Share button should be displayed", "Share button is not present or xpath is incorrect."+btnShareWishlist,"",false,true);	
		}
	}	
	
	private void verifyShareWishlist() throws Exception 
	{
		if(!isDisplayed(txtShareWishlistEmailXpath) || !isDisplayed(txtShareWishlistMessageXpath) || !isDisplayed(btnshareWishSave))
		{
			navigateToWishlistPage();
			
			if(isDisplayed(btnShareWishlist))
			{
				clickAndWait(btnShareWishlist);
			}
			
		}		
			
		if(!isDisplayed(txtShareWishlistEmailXpath) || !isDisplayed(txtShareWishlistMessageXpath) || !isDisplayed(btnshareWishSave))
		{
			writeTestResults("Verify share wishlist in wishlist page", "Email addresses, message and save button should be displayed", "Email addresses or message or save button is not displayed or xpath is incorrect.","",false,true);	
		}
		else
		{
			sendKeys(txtShareWishlistEmailXpath,shareWishlistEmail);
			
			sendKeys(txtShareWishlistMessageXpath,shareWishlistMessage);
			
			Thread.sleep(2000);
			clickAndWait(btnshareWishSave);

			if(getText(shareWishlistSuccessXpath).equalsIgnoreCase(msgShareWishlistSuccess))
			{
				writeTestResults("Verify user can share product in wishlist", msgShareWishlistSuccess+" message should be displayed", msgShareWishlistSuccess+" message is displayed","",true,false);
			}
			else
			{
				writeTestResults("Verify user can share product in wishlist", msgShareWishlistSuccess+" message should be displayed", getText(shareWishlistSuccessXpath)+" message is displayed","",false,false);
			}
		}
	}
	
	
	private void validateShareWishlist() throws Exception 
	{
		if(!isDisplayed(txtShareWishlistEmailXpath) || !isDisplayed(txtShareWishlistMessageXpath) || !isDisplayed(btnshareWishSave))
		{
			writeTestResults("Verify share wishlist in wishlist page", "Email addresses, message and save button should be displayed", "Email addresses or message or save button is not displayed or xpath is incorrect.","",false,true);
		}
		else
		{
			sendKeys(txtShareWishlistEmailXpath,"Test@test.com;test");
			
			clickAndWait(btnshareWishSave);
			
			if(getText(shareWishlistValidationXpath).equals(msgShareWishlistValidation))
			{
				writeTestResults("Validate share product email addresses field", msgShareWishlistValidation+" message should be displayed", msgShareWishlistValidation+" message is displayed","",true,false);
			}
			else
			{
				writeTestResults("Validate share product email addresses field", msgShareWishlistValidation+" message should be displayed", getText(shareWishlistValidationXpath)+" message is displayed","",false,false);
			}
		}
	}
	
	private void navigateToWishlistPage() throws Exception 
	{	
		clickAndWait(myWishlistLink);		
	}
	
	private void verifyShareWishList() throws Exception 
	{
		if(!isDisplayed(btnShareWishlist))
		{
			clickAndWait("//*[@class='block-content']/ul/li/a_Xpath");
				
			if(!isElementPresent(myWishlistLink))
			{
				writeTestResults("Verify my wishlist link in my account", "My Wishlist link should be displayed in left panel", "My wishlist link not present or xpath is incorrect."+myWishlistLink,"",false,true);
			}
			else
			{
				navigateToShareWishList();
			}					
		}
		else
		{
			navigateToShareWishList();
		}		
	}
	
	public void logout() throws Exception 
	{
		clearCookies();
		
		//pageRefersh();
		
		common.navigateToHomePage();
	}
	
	public boolean myAccountAjaxRequest(String loader) throws Exception
	{
		int i=0;
		boolean isAjaxLoad = true;
		
		if(isElementPresent(loader))
		{	
			while(!getAttribute(loader,myAccountAjaxClassType).equalsIgnoreCase(myAccountAjaxClassValue))
			{	
			   	 Thread.sleep(200);
			   	 System.out.println("i"+i);
			   	 i++;
			   	 
			   	 if(!isDisplayed(loader))
			   	 {
			   		 break;
			   	 }
			   	 
			   	 if(i==100)
			   	 {
			   		isAjaxLoad = false; 
			   		break;
			   	 }			   	 
			 }			 
		}	
		return isAjaxLoad;  
	}

	public void totalTime(java.util.Date startTime) throws Exception
	{		
		java.util.Date endTime;		
		Calendar cal = Calendar.getInstance(); 		
		endTime = cal.getTime();
		String totalTime = common.totalTime(startTime, endTime);	
		writeTestResults("Total time to execute "+writeFileName, "Total time =",totalTime,"",true,false);
	}	
}