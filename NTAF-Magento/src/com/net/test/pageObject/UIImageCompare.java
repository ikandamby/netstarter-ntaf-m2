/*
 * Home page functinality
 * 
 * NTAF 1.0
 *
 * 2013-06-14
 * 
 * Powered by netstarters
 */

package com.net.test.pageObject;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Random;

import javax.swing.GrayFilter;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


import com.net.test.data.CheckoutTestData;
import com.net.test.data.HomeTestData;
import com.net.test.data.UserCreationTestData;
import com.net.test.util.DBConnectionUtill;
import com.net.test.util.ReadXl;
import com.net.test.util.TestCommonMethods;
import com.net.test.util.WriteResults;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageDecoder;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;


public class UIImageCompare extends HomeTestData
{
	
	WriteResults wResult =  new WriteResults();
	ReadXl readXL  = new ReadXl();
	TestCommonMethods common = new TestCommonMethods();
	CheckOutProcess ckProcess = new CheckOutProcess();
	CheckoutTestData checkoutTestData = new CheckoutTestData();
	//CreateIssue createIssue = new CreateIssue();
	DBConnectionUtill db = new DBConnectionUtill();
	UserCreation user = new UserCreation();
	UserCreationTestData userData = new UserCreationTestData();
	MyAccount account = new MyAccount();
	
	String orginalIm ="";
	String verifiedIm ="";
	String failedIm ="";
	String verifyImageSaveTime ="";
	//SendTextMessage sendSMS = new SendTextMessage();
	
	protected static BufferedImage img1 = null;
	protected static BufferedImage img2 = null;
	protected static BufferedImage imgc = null;
	protected static int comparex = 0;
	protected static int comparey = 0;
	protected static int factorA = 0;
	protected static int factorD = 10;
	protected static boolean match = false;
	protected static int debugMode = 0; // 1: textual indication of change, 2: difference of factors

	
	public void resultSheetName(String resultSheet)throws Exception 
	{
		writeFileName=resultSheet;		
	}
	
	public void skipMethods(String methodName)throws Exception 
	{		
		wResult.writeSkipTestResult("Home Page ",methodName ,"", "Skip Results");
	}	
	public void imagesSavedFolder()throws Exception 
	{
		orginalIm = ReadXl.cmOrginalIm+findElementInXLSheet(getParameterXpath,"browser")+"/";
		verifiedIm = ReadXl.cmVerifiedIm+findElementInXLSheet(getParameterXpath,"browser")+"/";
		failedIm = ReadXl.cmFailedIm+findElementInXLSheet(getParameterXpath,"browser")+"/";
		verifyImageSaveTime = wResult.readTime;
	}
	public void goBackToHomePage()throws Exception 
	{
		if(!common.siteUrl.equalsIgnoreCase(getCurrentUrl()))
		{
			//logFile("open home page");
			openPage(common.siteUrl);				
		}
	}
	
	public void compareHomePageScreenShot(String screenName)throws Exception 
	{
		wResult.getWriteFileName();	
		
		imagesSavedFolder();
		
		common.clickLogo();
		
		mouseMoveToLeft("Get home page screenshot",screenName);
		
		if(isSelectedImagePresent("//*[contains(concat(' ', normalize-space(@class), ' '), 'slick-list draggable')]//div[2]"))
		{
			captureAndCompareScreenshot("Get home page screenshot",screenName);
		}
		
	}
	
	public void navigateToCategoryPage()throws Exception 
	{
		if(!common.selectBrand())
		{
			writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", "User can select the product", "Selected brand is not dipalyed or page is not loaded properly" ,"",false,true);
		}
						
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
		{
			common.selectProductFromLeftCategory();				
		}	
		
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			if(!common.productGrid())
			{
				 writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","Grid link should be present","Link is not displayed or xpath ("+checkoutTestData.productGrid+") is incorrect","",false,true);
			}
		}		
	}	
	
	private void mouseMoveToLeft(String getScreenShot,String screenName)throws Exception 
	{
		removeExistsImage(getScreenShot,screenName);		
		Robot robot = new Robot();
		robot.mouseMove(0,0);
		
		Thread.sleep(WAIT*5);
	}	
	
	public void compareCategoryPageScreenShot(String screenName)throws Exception 
	{		
		navigateToCategoryPage();
		
		mouseMoveToLeft("Get category page screenshot",screenName);
		
		if(isSelectedImagePresent("//*[contains(concat(' ', normalize-space(@class), ' '), 'slick-list draggable')]//div[2]"))
		{
			captureAndCompareScreenshot("Get category page screenshot",screenName);	
		}
			
	}
	
	public void compareProductPageScreenShot(String screenName)throws Exception 
	{		
		common.navigateToProductDetailsPage();	
		
		captureAndCompareScreenshot("Get details page screenshot",screenName);				
	}
	
	public void compareHeaderCartScreenShot(String screenName)throws Exception 
	{		
		common.addToCart("Verify the cart drop down shows number of items in the shopping cart when user added one product");
		
		if(common.totalItemInHeaderCart()==0)
		{
			writeTestResults("User can add the product in to the shopping cart", "Header Cart should be updated", "Cart is not updated","",false,true);
		}		
		
		else
		{
			if(!isDisplayed(checkoutTestData.viewCart))
			{
				click(checkoutTestData.viewCartId);
				
				Thread.sleep(2000);
			}
		}
				
		captureAndCompareScreenshot("Get header cart screenshot",screenName);	
	}
	
	public void compareShoppingCartScreenShot(String screenName)throws Exception 
	{		
		common.viewCart();
		
		captureAndCompareScreenshot("Get shopping cart screenshot",screenName);	
	}
	
	public void compareCheckoutUserTypeScreenShot(String screenName)throws Exception 
	{		
		if(!isElementPresent(ckProcess.proceedToCheckoutButtonInShoppingCart))
		{
			writeTestResults("Verify the Proceed to checkout button is displayed in shopping cart page", "Proceed to checkout button should be displayed in shopping cart page", 
					"Proceed to checkout button is not in the correct place or button is not displayed in the shopping cart page","",false,true);
		}
		else
		{			
			clickAndWait(ckProcess.proceedToCheckoutButtonInShoppingCart);
		}
		
		captureAndCompareScreenshot("Get checkout type screenshot",screenName);	
		
		if(isExecuted(getMethodIsExecuted,"Checkout As Guest With Email"))
		{
			ckProcess.CheckoutAsGuest_withEmail();	
		}
		else if(isExecuted(getMethodIsExecuted,"Checkout As Guest Without Email"))
		{
			ckProcess.CheckoutAsGuest_withoutEmail();	
		}
	}
	
	public void compareCheckouBillingScreenShot(String screenName)throws Exception 
	{	
		captureAndCompareScreenshot("Get billing info screenshot",screenName);
		
		if(isExecuted(getMethodIsExecuted,"Billing Info Page"))
		{
			ckProcess.billingInfoPage();
		}	
		if(isExecuted(getMethodIsExecuted,"Create An Account Later"))
		{
			ckProcess.createAccountLater();
		}
		
		if(isExecuted(getMethodIsExecuted,"Select Different Address"))
		{
			ckProcess.chkDifferentAddress();				
		}
		if(isExecuted(getMethodIsExecuted,"Continue Billing Button For Different Address"))
		{
			ckProcess.billingInfoConButton();				
		}
	}
	
	public void compareCheckoutShipplingScreenShot(String screenName)throws Exception 
	{	
		captureAndCompareScreenshot("Get shipping info screenshot",screenName);
		
		if(isExecuted(getMethodIsExecuted,"Shipping Info Page"))
		{
			ckProcess.shippingInfoPage();			
		}
		if(isExecuted(getMethodIsExecuted,"Shipping Info Continue Button"))
		{
			ckProcess.shippingInfoConButton();				
			
		}
	}
	
	public void compareCheckoutShippngMethodScreenShot(String screenName)throws Exception 
	{	
		captureAndCompareScreenshot("Get shipping method screenshot",screenName);
		
		if(isExecuted(getMethodIsExecuted,"Select Shipping Method"))
		{
			ckProcess.selectShippingMethod();		
		}
		if(isExecuted(getMethodIsExecuted,"Shipping Method Page"))
		{
			ckProcess.shippingMethodConButton();
		}
	}
	
	public void compareCheckoutpaymentScreenShot(String screenName)throws Exception 
	{		
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is CC"))
		{
			ckProcess.paymentMethodIsCC();
		}
		
		captureAndCompareScreenshot("Get payment method screenshot",screenName);
		
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is CC"))
		{
			ckProcess.fillCreditCardDetails();
		}
		
		if(isExecuted(getMethodIsExecuted,"Payment - Terms Condition"))
		{
			ckProcess.selectPaymentTeamsAndCondition();
		}
		
		if(isExecuted(getMethodIsExecuted,"Select Payment Method Is CC"))
		{
			ckProcess.credit_card_payment_submitButton();		
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
			
			ckProcess.creditCardError();
		}
	}
	
	public void compareCheckoutOrderReviewScreenShot(String screenName)throws Exception 
	{
		if(isExecuted(getMethodIsExecuted,"Order Review"))
		{
			captureAndCompareScreenshot("Get order review screenshot",screenName);
			
			ckProcess.continueOrderReview();
			
			ckProcess.verifyErrorMsgInOrderConfirmPage();
		}
		
	}
	
	public void compareCheckoutConfirmScreenShot(String screenName)throws Exception 
	{		
		if(isExecuted(getMethodIsExecuted,"Confimation Page"))
		{
			ckProcess.orderIdInConfirmationPage();
			
			captureAndCompareScreenshot("Get confirmation page screenshot",screenName);
		}
	}
	
	public void compareUserCreationScreenShot(String screenName)throws Exception 
	{	
		common.navigateToHomePage();
		
		common.clearMyCart();
		
		user.navigateToUserRegisterPage();
				
		mouseMoveToLeft("Get user creation page screenshot",screenName);
		
		captureAndCompareScreenshot("Get user creation page screenshot",screenName);
		
	}
	
	public void compareLoginPageScreenShot(String screenName)throws Exception 
	{	
		account.navigateToLoginPage();
		
		mouseMoveToLeft("Get user login page screenshot",screenName);
		
		captureAndCompareScreenshot("Get user login page screenshot",screenName);
		
	}
	
	public void compareForgotPwPageScreenShot(String screenName)throws Exception 
	{	
		account.navigateToForgetPasswordPage();
		
		captureAndCompareScreenshot("Get forgot password page screenshot",screenName);
		
	}
	
	public void compareHomePageLoggedUserScreenShot(String screenName)throws Exception 
	{
		account.login();
		
		common.clearMyCart();
			
		common.navigateToHomePage();
		
		mouseMoveToLeft("Get home page screenshot for logged user",screenName);
		
		if(isSelectedImagePresent("//*[contains(concat(' ', normalize-space(@class), ' '), 'slick-list draggable')]//div[2]"))
		{
			captureAndCompareScreenshot("Get home page screenshot for logged user",screenName);	
		}
		
					
	}
	
	public void compareCategoryPageLoggedUserScreenShot(String screenName)throws Exception 
	{		
		navigateToCategoryPage();
		
		mouseMoveToLeft("Get category page screenshot for logged user",screenName);
		
		if(isSelectedImagePresent("//*[contains(concat(' ', normalize-space(@class), ' '), 'slick-list draggable')]//div[2]"))
		{
			captureAndCompareScreenshot("Get category page screenshot for logged user",screenName);		
		}
		
				
	}
	
	public void compareProductPageLoggedUserScreenShot(String screenName)throws Exception 
	{		
		common.navigateToProductDetailsPage();	
		
		captureAndCompareScreenshot("Get details page screenshot for logged user",screenName);				
	}
	
	public void compareHeaderCartLoggedUserScreenShot(String screenName)throws Exception 
	{		
		common.addToCart("Verify the cart drop down shows number of items in the shopping cart when user added one product");
		
		if(common.totalItemInHeaderCart()==0)
		{
			writeTestResults("User can add the product in to the shopping cart", "Header Cart should be updated", "Cart is not updated","",false,true);
		}		
		
		else
		{
			if(!isDisplayed(checkoutTestData.viewCart))
			{
				click(checkoutTestData.viewCartId);
				
				Thread.sleep(2000);
			}
		}
				
		captureAndCompareScreenshot("Get header cart screenshot for logged user",screenName);	
	}
	
	public void compareShoppingCartLoggedUserScreenShot(String screenName)throws Exception 
	{		
		common.viewCart();
		
		captureAndCompareScreenshot("Get shopping cart screenshot for logged user",screenName);	
	}
	
	public void removeExistsImage(String getScreenShot,String screenName)throws Exception 
	{
		
		File f1 = new File(orginalIm+screenName+".png");
		File f2 = new File(orginalIm+screenName+".jpg");		  
		
		if(isExecuted(getMethodIsExecuted,"Capture screenshots"))
		{
			if(isExecuted(getMethodIsExecuted,getScreenShot))
			{
				if(f1.exists())
			    {
				    f1.delete();
			    }
				if(f2.exists())
			    {
			        f2.delete();
			    }
				
				Thread.sleep(WAIT*2);
			}			
		}
		
	}	
	
	public void captureAndCompareScreenshot(String getScreenShot,String screenName)throws Exception 
	{		
		if(isExecuted(getMethodIsExecuted,"Capture screenshots"))
		{
			if(isExecuted(getMethodIsExecuted,getScreenShot))
			{
				captureScreenShotForCompareImages(orginalIm+screenName);	
				
				writeTestResults("Verify "+screenName + "UI","Orginal image shoulsd be saved","Image is saved","",true,false);
			
			}
		}		
		else
		{	
			if(isExecuted(getMethodIsExecuted,"compare screenshots"))
			{
				captureScreenShotForCompareImages(verifiedIm+screenName+"-"+verifyImageSaveTime);	
				
				if(chkImage(orginalIm+screenName+".jpg"))
				{
					//compareScreenShots(verifiedIm,orginalIm,screenName);	
				}
			}
			
							
		}
	}
	
	/*
	 * File f1 = new File(screenShotName+".png");
		File f2 = new File(screenShotName+".jpg");
		
		System.out.println("f1.exists()"+f1.exists());
		System.out.println("f2.exists()"+f2.exists());
		if(f1.exists()) 
		{
			f1.delete();	
		}
		if(f2.exists()) 
		{
			f2.delete();	
		}
	 */
	
	public void deleteExistImages(String screenShotName) throws Exception 
	{
		File f1 = new File(screenShotName+".png");
		File f2 = new File(screenShotName+".jpg");
		
		System.out.println("f1.exists()"+f1.exists());
		System.out.println("f2.exists()"+f2.exists());
		if(f1.exists()) 
		{
			f1.delete();	
		}
		if(f2.exists()) 
		{
			f2.delete();	
		}
		
	}
	public void moveFaildImages(String imag) throws Exception 
	{
		File source = new File(imag);
		File dest = new File(failedIm+wResult.readTime);
		
		System.out.println("source"+source);
		System.out.println("dest"+dest);
		try {
			
			if(!dest.exists())
			{
				dest.mkdir();
			}
		    FileUtils.copyFileToDirectory(source, dest);
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
	/* create a runable demo thing. */
	public void compareScreenShots(String imagFolder1,String imagFolder2,String imagName) throws Exception 
	{
		// Set the comparison parameters.
		String imag1 = "";
		String imag2 = "";
		
		
		imag1 = imagFolder1 + imagName +"-"+wResult.readTime+".jpg";
		imag2 = imagFolder2 + imagName +".jpg";
					
		System.out.println("imag1"+imag1);
		System.out.println("imag2"+imag2);
		img1 = imageToBufferedImage(loadJPG(imag1));
		img2 = imageToBufferedImage(loadJPG(imag2));
				
		//   (num vertical regions, num horizontal regions, sensitivity, stabilizer)
		setParameters(60, 40, 1, 30);
		// Display some indication of the differences in the image.
		setDebugMode(2);
		// Compare.
		compare();
		// Display if these images are considered a match according to our parameters.
		System.out.println("Match: " + match());
		// If its not a match then write a file to show changed regions.
		if (!match())
		{
			moveFaildImages(imag1);
			moveFaildImages(imag2);
			
			String failImage = failedIm +imagName+"- Failed -"+wResult.readTime + ".jpg";
			
			saveJPG(getChangeIndicator(),failImage);
			
			moveFaildImages(failImage);
			
			writeTestResults("Verify "+imagName + "UI","UI should be match","UI is not match","",false,false);
			
		}
		else
		{
			writeTestResults("Verify "+imagName + "UI","UI should be match","UI is match","",true,false);
		}
	}
	
	// like this to perhaps be upgraded to something more heuristic in the future.
	protected void autoSetParameters() {
		comparex = 10;
		comparey = 10;
		factorA = 10;
		factorD = 10;
	}
	
	// set the parameters for use during change detection.
	public static void setParameters(int x, int y, double factorA, int factorD) {
		comparex = x;
		comparey = y;
		factorA = factorA;
		factorD = factorD;
	}
	
	// want to see some stuff in the console as the comparison is happening?
	public static void setDebugMode(int m) {
		debugMode = m;
	}
	
	// compare the two images in this object.
	public static void compare() {
		// setup change display image
		imgc = imageToBufferedImage(img2);
		Graphics2D gc = imgc.createGraphics();
		Color myColour = new Color(0, 255,0,100 );
		gc.setColor(myColour);
		// convert to gray images.
		img1 = imageToBufferedImage(GrayFilter.createDisabledImage(img1));
		img2 = imageToBufferedImage(GrayFilter.createDisabledImage(img2));
		// how big are each section
		int blocksx = (int)(img1.getWidth() / comparex);
		int blocksy = (int)(img1.getHeight() / comparey);
		// set to a match by default, if a change is found then flag non-match
		match = true;
		// loop through whole image and compare individual blocks of images
		for (int y = 0; y < comparey; y++) {
			if (debugMode > 0) System.out.print("|");
			for (int x = 0; x < comparex; x++) 
			{
				double b1 = getAverageBrightness(img1.getSubimage(x*blocksx, y*blocksy, blocksx - 1, blocksy - 1));
				double b2 = getAverageBrightness(img2.getSubimage(x*blocksx, y*blocksy, blocksx - 1, blocksy - 1));
								
				double diff = Math.abs(b1 - b2);
				if (diff > factorA) { // the difference in a certain region has passed the threshold value of factorA
					// draw an indicator on the change image to show where change was detected.
					gc.fillRect(x*blocksx, y*blocksy, blocksx, blocksy);
					match = false;
				}
				if (debugMode == 1) System.out.print((diff > factorA ? "X" : " "));
				if (debugMode == 2) System.out.print(diff + (x < comparex - 1 ? "," : ""));
			}
			if (debugMode > 0) System.out.println("|");
		}
	}
	
	// return the image that indicates the regions where changes where detected.
	public static BufferedImage getChangeIndicator() {
		return imgc;
	}
	
	// returns a value specifying some kind of average brightness in the image.
	protected static double getAverageBrightness(BufferedImage img) {
		Raster r = img.getData();
		double avaBrightness;
		double total = 0;
		for (int y = 0; y < r.getHeight(); y++) 
		{
			for (int x = 0; x < r.getWidth(); x++) 
			{
				total += r.getSample(r.getMinX() + x, r.getMinY() + y, 0);			
			}
		}	
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		//avaBrightness = total / ((r.getWidth()/factorD)*(r.getHeight()/factorD));
		return Double.parseDouble(decimalFormat.format(total / ((r.getWidth()/factorD)*(r.getHeight()/factorD))));
	}
	

	// returns true if image pair is considered a match
	public static boolean match() {
		return match;
	}

	// buffered images are just better.
	protected static BufferedImage imageToBufferedImage(Image img) {
		BufferedImage bi = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = bi.createGraphics();
		g2.drawImage(img, null, null);
		return bi;
	}
	
	// write a buffered image to a jpeg file.
	protected static void saveJPG(Image img, String filename) {
		BufferedImage bi = imageToBufferedImage(img);
		FileOutputStream out = null;
		try { 
			out = new FileOutputStream(filename);
		} catch (java.io.FileNotFoundException io) { 
			System.out.println("File Not Found"); 
		}
		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
		JPEGEncodeParam param = encoder.getDefaultJPEGEncodeParam(bi);
		param.setQuality(0.8f,false);
		encoder.setJPEGEncodeParam(param);
		try { 
			encoder.encode(bi); 
			out.close(); 
		} catch (java.io.IOException io) {
			System.out.println("IOException"); 
		}
	}
	
	//check image is exsits
	protected static boolean chkImage(String filename) {
		FileInputStream in = null;
		boolean found = true;
		try { 
			in = new FileInputStream(filename);
			
		} catch (java.io.FileNotFoundException io) { 
			System.out.println("File Not Found"); 
			found = false;
		}
		return found;
	}
	// read a jpeg file into a buffered image
	protected static Image loadJPG(String filename) {
		FileInputStream in = null;
		try { 
			in = new FileInputStream(filename);
		} catch (java.io.FileNotFoundException io) { 
			System.out.println("File Not Found"); 
		}
		JPEGImageDecoder decoder = JPEGCodec.createJPEGDecoder(in);
		BufferedImage bi = null;
		try { 
			bi = decoder.decodeAsBufferedImage(); 
			in.close(); 
		} catch (java.io.IOException io) {
			System.out.println("IOException");
		}
		return bi;
	}
	
	public boolean isSelectedImagePresent(String imageXpath)throws Exception 
	{
		int i = 0;
		boolean imagePresent =true;
		
		while(!isDisplayed(imageXpath))
		{			
			i++;
			
			if(i==1000)
			{
				imagePresent = false;
				break;
			}		
		}	
		
		return imagePresent;
		
	}
	public void totalTime(java.util.Date startTime) throws Exception
	{		
		java.util.Date endTime;		
		Calendar cal = Calendar.getInstance(); 		
		endTime = cal.getTime();
		String totalTime = common.totalTime(startTime, endTime);		   
	    writeTestResults("Total time to execute "+writeFileName, "Total time =",totalTime ,"",true,false);		
	}
	
	
			
}