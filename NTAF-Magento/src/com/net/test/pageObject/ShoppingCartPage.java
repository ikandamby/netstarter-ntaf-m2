package com.net.test.pageObject;

//import java.util.List;

//import org.apache.bcel.generic.Select;
//import org.openqa.selenium.By;
import java.text.DecimalFormat;
import java.util.Calendar;

import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import com.net.test.data.CheckoutTestData;
import com.net.test.data.ShoppingCartTestData;
import com.net.test.util.CreateIssue;
import com.net.test.util.ReadXl;
import com.net.test.util.SendTextMessage;
import com.net.test.util.TestCommonMethods;
import com.net.test.util.WriteResults;
 

public class ShoppingCartPage extends ShoppingCartTestData

{	
	WriteResults wResult =  new WriteResults();
	ReadXl readParametrs  = new ReadXl();
	TestCommonMethods common = new TestCommonMethods();
	SendTextMessage sendSMS = new SendTextMessage();
	CreateIssue createIssue = new CreateIssue();
	CheckoutTestData checkoutTestData = new CheckoutTestData();
	
	public WebDriver driver =null;		
		

	public void skipMethods(String methodName)throws Exception 
	{		
		wResult.writeSkipTestResult("Shopping Cart ",methodName ,"", "Skip Results");
	}

	public void resultSheetName(String resultSheet)throws Exception 
	{
		writeFileName=resultSheet;		
	}
	public void getDataFromDataFile() throws Exception
	{ 
		getLocation();
		
		getDataFromXLSheet();		
	}
	
	public void NavigateToProductDetails() throws Exception
	{ 		
		common.navigatToCategoryPage();	
		
		if(!common.productAvailability())
		{
			 writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product","One or more than one products should be available for "+readXL.brandName, "Products are not available for "+readXL.brandName,"",false,true);
		}
		else
		{
			writeTestResults("Verify user can select product in category page", "User can navigate to '"+TestCommonMethods.productName+"' details page", "Selected product name is '"+TestCommonMethods.productName+"' and price is '"+TestCommonMethods.productPrice+"'","",true,false);
		}
		
		common.compareProductDetails(common.productPriceInProductDetail());
	}
	
	public void verifyMiniCart() throws Exception
	{
		common.navigatToCategoryPageInShoppingCart();
		
		common.navigateToProductDetailsPage();	
		
		common.addToCart("Verify the cart drop down shows number of items in the shopping cart when user added one product");
		
		if(isElementPresent(checkoutTestData.myCartXpath))
		{					
			if(common.totalItemInHeaderCart()==1)
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", 
						"'1 Item' should be displayed in the 'mini cart'", "'1 Item' is displayed in the 'mini cart'"  ,"42725da4",true,false);
				
			}
			else
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", 
						"'1 Item' should be displayed in the 'mini cart'", "'"+common.totalItemInHeaderCart()+"' Items' is displayed in the 'mini cart'" ,"42725da4",false,true);
			}
		
					
		}
		else
		{
			 writeTestResults("Verify the 'Cart Item' field", "'Cart Item' field should be displayed", "'Cart Item' is not displayed" ,"",false,true);
		}
				
		
		if(isExecuted(getMethodIsExecuted,"Mini cart has edit item link"))
		{
			common.clickMiniCart();		
			
			editMiniCart();
		}
		if(isExecuted(getMethodIsExecuted,"Mini cart has shopping cart link"))
		{
			common.clickMiniCart();
			
			NavigateToShoppingCartViaMiniCart();
		}
		if(isExecuted(getMethodIsExecuted,"Mini cart has checkout link"))
		{
			common.clickMiniCart();
			
			NavigateToCheckoutPageViaMiniCart();
		}
		if(isExecuted(getMethodIsExecuted,"Mini cart has delete item link"))
		{
			common.clickLogo();
			
			common.clickMiniCart();
			
			deleteItemInMiniCart();
		}
	}
	
	public void editMiniCart() throws Exception
	{
		if(!isDisplayed(linkEditMiniCart))
		{
			common.clickMiniCart();		
		}
		if(isDisplayed(linkEditMiniCart))
		{
			writeTestResults("Verify the cart drop down has edit item link", "Edit item link should be diplayed in mini cart", "Edit link is displayed in mini cart'" ,"",true,false);
			
			clickAndWait(linkEditMiniCart);
			
			if(isDisplayed(btnUpdateCart))
			{
				writeTestResults("Verify user can edit the product in mini cart", "Product page should be displayed", "Product page is displayed." ,"427360dc",true,false);
			}
			else
			{
				writeTestResults("Verify user can edit the product in mini cart", "Product page should be displayed", "Product page is not displayed.Following page is found "+getTitle() ,"427360dc",false,false);
			}
		}
		else
		{
			writeTestResults("Verify the cart drop down has edit item link", "Edit item link should be diplayed in mini cart", "Edit link is not displayed in mini cart'" ,"427360dc",false,false);
		}			
		
	}
	
	public void deleteItemInMiniCart() throws Exception
	{	
		if(!isDisplayed(linkRemoveItem))
		{
			common.clickMiniCart();		
		}
		if(isDisplayed(linkRemoveItem))
		{
			writeTestResults("Verify the cart drop down has 'Remove item' link", "'Remove item' link should be diplayed in mini cart", "'Remove item' is displayed in mini cart'" ,"",true,false);
			
			click(linkRemoveItem);
			
			if(isAlertPresent())
			{
				System.out.println("Aleart present");
				acceptAlert();
					
				waitForPageLoad();
			}
			
			if(common.totalItemInHeaderCart() == 0)
			{
				writeTestResults("Verify user can remove item from mini cart", "Empty cart page should be displayed", "Empty cart page is displayed." ,"42735fb0",true,false);
			}
			else
			{
				writeTestResults("Verify user can remove item from mini cart", "Empty cart page should be displayed", "Empty cart page is not displayed.Following page is found "+getTitle() ,"42735fb0",false,false);
			}
				
		}
		else
		{
			writeTestResults("Verify the cart drop down has 'Remove item' link", "'Remove item' link should be diplayed in mini cart", 
					"'Remove item' is not displayed in mini cart'" ,"42735fb0",false,false);
		}
	}
	
	public void NavigateToShoppingCartViaMiniCart() throws Exception
	{	
		if(!isDisplayed(linkGotoShoppingCart))
		{
			common.clickMiniCart();		
		}
		if(isDisplayed(linkGotoShoppingCart))
		{
			writeTestResults("Verify the cart drop down has 'Go to shopping cart' link", "'Go to shopping cart' link should be diplayed in mini cart", "'Go to shopping cart' is displayed in mini cart'" ,"",true,false);
			
			clickAndWait(linkGotoShoppingCart);
			
			if(isDisplayed(proceedToCheckoutButtonInShoppingCart))
			{
				writeTestResults("Verify user can navigate to shopping cart via mini cart", "Shopping cart page should be displayed", "Shopping cart page is displayed." ,"42735c2c",true,false);
			}
			else
			{
				writeTestResults("Verify user can navigate to shopping cart via mini cart", "Shopping cart page should be displayed", "Shopping cart page is not displayed.Following page is found "+getTitle() ,"42735c2c",false,false);
			}
		}
		else
		{
			writeTestResults("Verify the cart drop down has 'Go to shopping cart' link", "'Go to shopping cart' link should be diplayed in mini cart", 
					"'Go to shopping cart' is not displayed in mini cart'" ,"42735c2c",false,false);
		}
	}
	
	public void NavigateToCheckoutPageViaMiniCart() throws Exception
	{
		if(!isDisplayed(linkCheckout))
		{
			common.clickMiniCart();		
		}
		if(isDisplayed(linkCheckout))
		{
			writeTestResults("Verify the cart drop down has 'Checkout' link", "'Checkout' link should be diplayed in mini cart", "'Checkout' link is displayed in mini cart'" ,"",true,false);
			
			clickAndWait(linkCheckout);
			
			if(!(isElementPresent(checkoutTestData.chkCheckoutAsGuestUser) || isElementPresent(checkoutTestData.btnCheckoutAsGuestUser)))
			{
				Thread.sleep(5000);
			}
			else
			{
				System.out.println("Checkout page found");
			}
			
			if(isDisplayed(checkoutTestData.chkCheckoutAsGuestUser) || isElementPresent("login:guest_Id") || isDisplayed(checkoutTestData.btnCheckoutAsGuestUser))
			{
				writeTestResults("Verify user can navigate to checkout via mini cart", "checkout page should be displayed", 
						"checkout page is displayed." ,"42735d44",true,false);
			}
			else
			{
				writeTestResults("Verify user can navigate to checkout via mini cart", "checkout cart page should be displayed",
						"checkout cart page is not displayed.Following page is found "+getTitle() ,"42735d44",false,false);
			}
			
		}
		else
		{
			writeTestResults("Verify the cart drop down has 'Checkout' link", "'Checkout' link should be diplayed in mini cart", 
					"'Checkout' is not displayed in mini cart'" ,"42735d44",false,false);
		}
	}


	public void addProductIntoCart() throws Exception
	{ 		
		common.navigatToCategoryPageInShoppingCart();				
		
		common.navigateToProductDetailsPage();	
				
		common.addToCart("Verify the cart drop down shows number of items in the shopping cart when user added one product");
		
		common.viewCart();	
	}
	
	public void verifyTitle() throws Exception
	{	
		common.titleInshoppingCartPage(writeFileName);			
	}
	
	public void verifyProductImageLink() throws Exception
	{
		if(isElementPresent(shoppingCartImgeLink))
		{
			clickAndWait(shoppingCartImgeLink);			
			
			if(getTitle().equalsIgnoreCase(common.productTitle))
			{
				 writeTestResults("Verify On-Click Product Image link navigate to product details page ", "Page should navigated to Product Detail page", 
						 "Page is navigated to Product Detail page","426fb2b6",true,false);				
			}
			else
			{
				 writeTestResults("Verify On-Click Product Image link navigate to product details page ", "Following page should be displayed. Page url is" +
				 		" "+common.productTitle, "Following page is displayed."+getCurrentUrl()+". Page title is "+getTitle(),"426fb2b6",false,false);
			}
			
			common.viewCart();
		}		
	}
	
	public void verifyProductPriceForGuest() throws Exception
	{	
		double getUnitProductPrice;
		
		getUnitProductPrice = common.getValueWitoutCurrency(common.productUnitPriceInshoppingCartPage());
		
		if(common.getValueWitoutCurrency(common.productPrice) ==  getUnitProductPrice)
		{
			 writeTestResults("Verify product unit price", common.productPrice+" should be displayed", getUnitProductPrice+" is displayed","426fae1a",true,false);
		}
		else
		{
			 writeTestResults("Verify product unit price", common.productPrice+" should be displayed", getUnitProductPrice+" is displayed","426fae1a",false,false);
		}
		
		//common.productPriceInshoppingCartPageForGuest(TestCommonMethods.productPrice,writeFileName);			
	}
	
	public void verifyProductName() throws Exception
	{	
		common.productNameInshoppingCartPage(common.productName,writeFileName);		
	}
	
	public void viewCart() throws Exception
	{	
		common.viewCart();	
	}	
	
	public void emptyCart() throws Exception
	{	
		common.clearMyCartWithoutCookies();
		
		
		
		
		
		
		if(isElementPresent(shoppingCartEmptyXpath))
		{
			if(getText(shoppingCartEmptyXpath).equalsIgnoreCase(msgShoppingCartEmpty))
			{
				 writeTestResults("Verify when NO Items in the Shopping Cart", "Expected message is '"+msgShoppingCartEmpty + "' in the shopping cart page", 
						 "Actual message is "+msgShoppingCartEmpty+" in the shopping cart page","426fbfe0",true,false);
				
				 writeTestResultsToUJ("426fbfe0",true);
			}
			else
			{
				 writeTestResults("Verify when NO Items in the Shopping Cart", "Expected message is '"+msgShoppingCartEmpty + "' in the shopping cart page", 
						 "Actual message is "+getText(shoppingCartEmptyXpath)+" in the shopping cart page","426fbfe0",false,true);
				 
				 writeTestResultsToUJ("426fbfe0",false);
			}			
			emptyCartContinue();
		}
		else
		{
			 writeTestResults("Verify when NO Items in the Shopping Cart", "Expected message is"+msgShoppingCartEmpty + " in the shopping cart page", 
					 shoppingCartEmptyXpath+" not present in the shopping cart page","426fbfe0",false,true);
			 
			 writeTestResultsToUJ("426fbfe0",false);
		}		
	}	
	
	public void emptyCartContinue() throws Exception
	{	
		if(isElementPresent(shoppingCartEmptyContinueButton))
		{
			clickAndWait(shoppingCartEmptyContinueButton);
			
			if(getCurrentUrl().equalsIgnoreCase(common.siteUrl))
			{
				

				 writeTestResults("Verify home page displayed when user clicks continue button in empty cart page", "Home page should be displayed", 
						 "Home page is displayed","426fbcac",true,false);
			}
			else
			{
				 writeTestResults("Verify home page displayed when user clicks continue button in empty cart page", "Home page should be displayed", 
						 "Following page is displayed "+getCurrentUrl(),"426fbcac",false,true);
			}
		}
		else
		{
			 writeTestResults("Verify home page displayed when user clicks continue button in empty cart page", "Home page should be displayed", 
					 "Following attribute is not displayed "+shoppingCartEmptyContinueButton,"426fbcac",false,true);
		}
	}
		
	public void validateCart() throws Exception
	{
		String qty = "";
				
		if(isElementPresent(productQTYInShoppingCart) && isElementPresent(updateButton))
		{
			qty = getAttribute(productQTYInShoppingCart,"value");
			
			sendKeys(productQTYInShoppingCart,"TEST");
			
			click(updateButton);			
			
			if(qty.equalsIgnoreCase(getAttribute(productQTYInShoppingCart,"value")))
			{
				if(isDisplayed(msgErrorxpath))
				{
					 writeTestResults("Field validation of Items to Purchase", "Only digits should be allowed", "Only digits is allowed to update qty " +
					 		"and following message found."+getText(msgErrorxpath),"",true,false);	
				}
				else
				{
					 writeTestResults("Field validation of Items to Purchase", "Only digits should be allowed", "Only digits is allowed to update qty","",true,false);
				}
				
			}
			else
			{
				 writeTestResults("Field validation of Items to Purchase", "Only digits should be allowed", "Following value is displayed in qty text " +
				 		"box "+getAttribute(productQTYInShoppingCart,"value"),"",false,false);
			}
		}		
		else
		{
			if(!isDisplayed(productQTYInShoppingCart))
			{
				 writeTestResults("Verify QTY field when user enter invalid QTY", "QTY text box should be allowed", "QTY text box is not displayed or xpath is incorrect","",false,false);
			}
			if(!isDisplayed(updateButton))
			{
				 writeTestResults("Verify QTY update button when user enter invalid QTY", "Update button should be allowed", "Update button is not displayed or xpath is incorrect","",false,false);
			}
			
		}		
	}
	
	public void updateCart() throws Exception
	{		
	
		double getGrandTotal;
		double getSubTotal;
		
		if(isElementPresent(productQTYInShoppingCart) && isElementPresent(updateButton))
		{
			getSubTotal = common.getValueWitoutCurrency(common.productSubTotalInshoppingCartPage());
			getGrandTotal = common.productGrandTotalInshoppingCartPage();			
			
			sendKeys(productQTYInShoppingCart,"2");
			
			click(updateButton);
			if(isDisplayed(outOFStockInShoppingCart))
			{
				 writeTestResults("Functionality of Update Cart Button", "Cart should be updated", "Some of the products cannot be" +
				 		" ordered in requested quantity.message is displayed","426fc990",true,false);
			}
			else if("2".equalsIgnoreCase(getAttribute(productQTYInShoppingCart,"value")))
			{
				 writeTestResults("Functionality of Update Cart Button", "Cart should be updated", "Cart is updated","426fc990",true,false);
				 writeTestResultsToUJ("426fd304",true);
				 
				/*if(getSubTotal == common.getValueWitoutCurrency(common.productPriceInshoppingCartPage()))
				{
					 writeTestResults("Verify updated subtotal price after edit QTY", getSubTotal*2+" should be displayed", getSubTotal*2+ " is displayed","426fcc9c",true,false);
				}
				else
				{
					writeTestResults("Verify updated price subtotal after edit QTY", getSubTotal*2+" should be displayed", common.productPriceInshoppingCartPage()+ " is displayed","426fcc9c",false,false);
				}*/
				
				if(getGrandTotal*2 == common.productGrandTotalInshoppingCartPage())
				{
					 writeTestResults("Verify updated total price after edit QTY", getGrandTotal*2+" should be displayed", getGrandTotal*2+ " is displayed","426fcfc6",true,false);
				}
				else
				{
					writeTestResults("Verify updated total price after edit QTY", getGrandTotal*2+" should be displayed", common.productGrandTotalInshoppingCartPage()+ " is displayed","426fcfc6",false,false);
				}
				 
			}
			else
			{
				if(isElementPresent(msgErrorxpath))
				{
					 writeTestResults("Functionality of Update Cart Button", "Cart should be updated with 2 items",
							 "Following error found."+getText(msgErrorxpath),"426fc990",false,false);
				}
				else
				{
					 writeTestResults("Functionality of Update Cart Button", "Cart should be updated with 2 items",
							 "Following value is displayed in qty textbox "+getAttribute(productQTYInShoppingCart,"value"),"426fc990",false,true);
				}
			}
		}		
		else
		{
			if(!isDisplayed(productQTYInShoppingCart))
			{
				 writeTestResults("Verify QTY field when user enter valid QTY", "QTY text box should be allowed", "QTY text box is not displayed or xpath is incorrect","426fc990",false,false);
			}
			if(!isDisplayed(updateButton))
			{
				 writeTestResults("Verify QTY update button  when user enter valid QTY", "Update button should be allowed", "Update button is not displayed or xpath is incorrect","426fc990",false,false);
			}
		}	

	}
	
	public void continueShopping() throws Exception
	{
		if(isElementPresent(continueShoppingButton))
		{
			clickAndWait(continueShoppingButton);
			
			if(getCurrentUrl().equalsIgnoreCase(common.siteUrl))
			{
				 writeTestResults("Functionality of Continue Button", "Home page should be displayed","Home page is displayed","",true,false);
				
			}
			else
			{
				 writeTestResults("Functionality of Continue Button", "Home page should be displayed","Following page is displayed."+getTitle(),"",false,true);
			}
		}
		else
		{
			 writeTestResults("Functionality of Continue Button", "Home page should be displayed","Continue shopping button is not displayed or xpath is incorrect","",false,true);
		}
		
		common.viewCart();		
	}
	
	public void proceedToCheckoutButton() throws Exception
	{		
		addProductIntoCart();
		
		if(!isElementPresent(proceedToCheckoutButtonInShoppingCart))
		{
			 writeTestResults("Verify the Proceed to checkout button is displayed in shopping cart page", "Proceed to checkout button should be displayed in shopping cart page", "Proceed to checkout button is not in the correct place or button is not displayed in the shopping cart page","",false,true);
		}
		else
		{
			 writeTestResults("Verify the Proceed to checkout button is displayed in shopping cart page", "Proceed to checkout button should be displayed in shopping cart page", "Proceed to checkout button is displayed in shopping cart page","",true,false);
			
			clickAndWait(proceedToCheckoutButtonInShoppingCart);
			
			if(!(isElementPresent(checkoutTestData.chkCheckoutAsGuestUser) || isElementPresent(checkoutTestData.btnCheckoutAsGuestUser)))
			{
				Thread.sleep(5000);
			}
			else
			{
				System.out.println("Checkout page found");
			}
			
			if(isDisplayed(checkoutTestData.chkCheckoutAsGuestUser) || isElementPresent("login:guest_Id") || isDisplayed(checkoutTestData.btnCheckoutAsGuestUser))
			{
				 writeTestResults("Functionality of Checkout Button", "Page should navigates to Checkout Process page", "Page is navigates to Checkout Process page","",true,false);
			}
			else
			{
				 writeTestResults("Functionality of Checkout Button", "Page should navigates to Checkout Process page", "Page is not navigates to Checkout Process page.Following page is displayed "+getTitle(),"",false,true);
			}
		}						
	}	
	
	/**
	 * This method use for check how many products in the cart
	 * If cart count is 0 or > 1 clear the cart and add new product into cart
	 * @throws Exception
	 */
	public void verifyItemCount() throws Exception
	{	
		if(!(common.totalItemInHeaderCart() > 0))
		{
			addProductIntoCart();
		}
		
	}
	
	public void enablePromotionCodeTxtBox() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"has enable promotion code txt box"))
		{
			if(!isDisplayed(txtCouponCodeXpath) && isDisplayed(lbEnableCouponCode))
			{
				click(lbEnableCouponCode);
			}
		}
	}
	
	private void enableGiftCardTxtBox() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"has enable giftcard txt box"))
		{
			if(!isDisplayed(txtGiftCardXoath) && isDisplayed(lbEnableGiftCardCode))
			{
				click(lbEnableGiftCardCode);
			}
		}
	}
	
	public void validateEmptyGiftCard() throws Exception
	{	
		verifyItemCount();
		
		enableGiftCardTxtBox();
		
		if(isElementPresent(txtGiftCardXoath) && isElementPresent(btnGiftCardCode))
		{
			sendKeys(txtGiftCardXoath,"");
			
			click(btnGiftCardCode);
			
			if(isElementPresent(validationWithoutGiftCardXpath))
			{
				if(getText(validationWithoutGiftCardXpath).equalsIgnoreCase(msgValidationWithoutGiftCard))
				{
					 writeTestResults("Validate gift card with empty value", msgValidationWithoutGiftCard+" message should be displayed.",
							 msgValidationWithoutGiftCard+" message is displayed.","42707da4",true,false);
					
				}
				else
				{
					 writeTestResults("Validate gift card with empty value", msgValidationWithoutGiftCard+" message should be displayed.",
							 getText(validationWithoutGiftCardXpath)+" message is displayed.","42707da4",true,false);
				}
			}
			else
			{
				 writeTestResults("Validate gift card with empty value", msgValidationWithoutGiftCard+" message should be displayed.",
						 "Validation xpath is not displayed or incorrect xpath","42707da4",false,true);
			}
		}
		else
		{
			 writeTestResults("Validate gift card with empty value", msgValidationWithoutGiftCard+" message should be displayed.",
					 "Gift card txt box or button is not displayed or incorrect xpath","42707da4",false,true);
		}
	}
	
	public void validateEmptyCouponCode() throws Exception
	{	
		clearCookies();
		
		pageRefersh();
		
		verifyItemCount();
		
		enablePromotionCodeTxtBox();
		
		if(isElementPresent(txtCouponCodeXpath) && isElementPresent(btnCouponCode))
		{
			sendKeys(txtCouponCodeXpath,"");
			
			click(btnCouponCode);		
			
			if(isAlertPresent())
			{
				String alert = getAlert();
				 writeTestResults("Validate coupon code with empty value", alert+" message should be displayed.",alert+" message is displayed.","426fd67e",true,false);
				
			}
			
			else if(!validationWithoutCouponCodeXpath.isEmpty() && isDisplayed(validationWithoutCouponCodeXpath))
			{
				if(getText(validationWithoutCouponCodeXpath).equalsIgnoreCase(msgValidationWithoutCouponCode))
				{
					 writeTestResults("Validate coupon code with empty value", msgValidationWithoutCouponCode+" message should be displayed.",
							 msgValidationWithoutCouponCode+" message is displayed.","426fd67e",true,false);
					
				}
				else
				{
					 writeTestResults("Validate coupon code with empty value", msgValidationWithoutCouponCode+" message should be displayed.",
							 getText(validationWithoutCouponCodeXpath)+" message is displayed.","426fd67e",true,false);
				}
			}
			else
			{
				 writeTestResults("Validate coupon code with empty value", msgValidationWithoutCouponCode+" message should be displayed.",
						 "Validation xpath is not displayed or incorrect xpath","426fd67e",false,true);
			}
		}
		else
		{
			 writeTestResults("Validate coupon code with empty value", msgValidationWithoutCouponCode+" message should be displayed.",
					 "coupon code txt box or button is not displayed or incorrect xpath","426fd67e",false,true);
		}
	}
	
	public void validateInvalidGiftCard() throws Exception
	{		
		verifyItemCount();
		
		enableGiftCardTxtBox();
		
		if(isElementPresent(txtGiftCardXoath) && isElementPresent(btnGiftCardCode))
		{
			sendKeys(txtGiftCardXoath,"TEXT");
			
			clickAndWait(btnGiftCardCode);
			
			if(!isDisplayed(msgErrorxpath))
			{
				int i =0;
				while(!isDisplayed(msgErrorxpath))
				{
					if(isDisplayed(msgErrorxpath))
					{
						break;
					}
					else
					{
						Thread.sleep(1000);
						i++;
					}
					
					if(i ==5)
					{
						break;
					}
				}
			}
			if(isDisplayed(msgErrorxpath))
			{
				if(getText(msgErrorxpath).equalsIgnoreCase(msgErrorGiftCard))
				{
					 writeTestResults("Validate gift card with invalid value", msgErrorGiftCard+" message should be displayed.",msgErrorGiftCard+" " +
					 		"message is displayed.","42708966",true,false);
					
				}
				else
				{
					 writeTestResults("Validate gift card with invalid value", msgErrorGiftCard+" message should be displayed.",getText(msgErrorxpath)+" " +
					 		"message is displayed.","42708966",true,false);
				}
			}
			else
			{
				if(isDisplayed(msgSuccessxpath))
				{
					 writeTestResults("Validate gift card with invalid value", msgErrorGiftCard+" message should be displayed.",
							 "Following message is displayed. "+getText(msgSuccessxpath),"42708966",false,true);
				}
				else
				{
					 writeTestResults("Validate gift card with invalid value", msgErrorGiftCard+" message should be displayed.",
							 "Validation xpath is not displayed or incorrect xpath","42708966",false,true);
				}				
			}
		}
		else
		{
			 writeTestResults("Validate gift card with invalid value", msgErrorGiftCard+" message should be displayed.","Gift card txt box or button is not displayed or incorrect xpath","",false,true);
		}
	}
	
	public void validateInvalidCouponCode() throws Exception
	{		
		verifyItemCount();
		
		enablePromotionCodeTxtBox();
		
		if(isElementPresent(txtCouponCodeXpath) && isElementPresent(btnCouponCode))
		{
			sendKeys(txtCouponCodeXpath,"TEST INVALID");
			
			clickAndWait(btnCouponCode);
			
			if(!isDisplayed(msgErrorxpath))
			{
				int i =0;
				while(!isDisplayed(msgErrorxpath))
				{
					if(isDisplayed(msgErrorxpath))
					{
						break;
					}
					else
					{
						Thread.sleep(1000);
						i++;
					}
					
					if(i ==5)
					{
						break;
					}
				}
			}
			
			if(isDisplayed(msgErrorxpath))
			{
				if(getText(msgErrorxpath).equalsIgnoreCase(msgErrorCoupon))
				{
					 writeTestResults("Validate coupon code with invalid value", msgErrorCoupon+" message should be displayed.",msgErrorCoupon+"" +
					 		" message is displayed.","",true,false);
					
				}
				else
				{
					 writeTestResults("Validate coupon code with invalid value", msgErrorCoupon+" message should be displayed.",getText(msgErrorxpath)+" " +
					 		"message is displayed.","",true,false);
				}
			}
			else
			{
				if(isDisplayed(msgSuccessxpath))
				{
					 writeTestResults("Validate coupon code with invalid value", msgErrorCoupon+" message should be displayed.","Following message is displayed. "+getText(msgSuccessxpath),"",false,true);
				}
				else
				{
					 writeTestResults("Validate coupon code with invalid value", msgErrorCoupon+" message should be displayed.","Validation xpath is not displayed or incorrect xpath","",false,true);
				}				
			}
		}
		else
		{
			 writeTestResults("Validate coupon code with invalid value", msgErrorCoupon+" message should be displayed.","coupon code txt box or button is not displayed or incorrect xpath","",false,true);
		}
	}
		
	public void verifyValidGiftCardForFullRedeem() throws Exception
	{	
		clearCookies();
		
		pageRefersh();
		
		verifyItemCount();
		
		double grandTotalBeforeEnterCoupon = 0.0;
		
		if(isElementPresent(grandTotalXpathBeforeDiscount))
		{
			grandTotalBeforeEnterCoupon = common.getValueWitoutCurrency(getText(grandTotalXpathBeforeDiscount));			 
			
		}
		
		enableGiftCardTxtBox();
		
		if(isElementPresent(txtGiftCardXoath) && isElementPresent(btnGiftCardCode))
		{
			sendKeys(txtGiftCardXoath,txtGiftCardFullRedeem);
			
			clickAndWait(btnGiftCardCode);
			
			if(isDisplayed(msgSuccessxpath))
			{
				if(getText(msgSuccessxpath).equalsIgnoreCase(msgSuccessForGiftCardFullRedeem))
				{
					 writeTestResults("Validate gift card with valid value when gift crd type is 'full redeem", msgSuccessForGiftCardFullRedeem+" " +
					 		"message should be displayed.",msgSuccessForGiftCardFullRedeem+" message is displayed.","4270ac3e",true,false);
					
				}
				else
				{
					 writeTestResults("Validate gift card with valid value when gift crd type is 'full redeem", msgSuccessForGiftCardFullRedeem+" " +
					 		"message should be displayed.",getText(msgSuccessxpath)+" message is displayed.","4270ac3e",true,false);
				}
				
				verifyGrandTotalForGiftCardFullRedeem(grandTotalBeforeEnterCoupon);
			}
			else
			{
				if(isDisplayed(msgErrorxpath))
				{
					 writeTestResults("Validate gift card with valid value when gift crd type is 'full redeem", msgSuccessForGiftCardFullRedeem+" message should be displayed.","Following message is displayed. "+getText(msgErrorxpath),"",false,true);
				}
				else
				{
					 writeTestResults("Validate gift card with valid value when gift crd type is 'full redeem", msgSuccessForGiftCardFullRedeem+" message should be displayed.","Validation xpath is not displayed or incorrect xpath","",false,true);
				}				
			}
		}
		else
		{
			 writeTestResults("Validate gift card with valid value when gift crd type is 'full redeem", msgSuccessForGiftCardFullRedeem+" message should be displayed.","Gift card txt box or button is not displayed or incorrect xpath","",false,true);
		}
	}
	
	public void verifyValidGiftCardForPartRedeem() throws Exception
	{	
		clearCookies();
		
		pageRefersh();
		
		verifyItemCount();
		
		double grandTotalBeforeEnterCoupon = 0.0;
		
		if(isElementPresent(grandTotalXpathBeforeDiscount))
		{
			grandTotalBeforeEnterCoupon =common.getValueWitoutCurrency(getText(grandTotalXpathBeforeDiscount));			 
			
		}
		
		enableGiftCardTxtBox();
		
		if(isElementPresent(txtGiftCardXoath) && isElementPresent(btnGiftCardCode))
		{
			sendKeys(txtGiftCardXoath,txtGiftCardPartRedeem);
			
			clickAndWait(btnGiftCardCode);
			
			if(isDisplayed(msgSuccessxpath))
			{
				if(getText(msgSuccessxpath).equalsIgnoreCase(msgSuccessForGiftCardPartRedeem))
				{
					 writeTestResults("Validate gift card with valid value when gift crd type is 'partially  redeem", msgSuccessForGiftCardPartRedeem+" " +
					 		"message should be displayed.",msgSuccessForGiftCardPartRedeem+" message is displayed.","42709334",true,false);
					
				}
				else
				{
					 writeTestResults("Validate gift card with valid value when gift crd type is 'partially  redeem", msgSuccessForGiftCardPartRedeem+" " +
					 		"message should be displayed.",getText(msgSuccessxpath)+" message is displayed.","42709334",true,false);
				}
				
				verifyGrandTotalForGiftCardPartRedeem(grandTotalBeforeEnterCoupon);
			}
			else
			{
				if(isDisplayed(msgErrorxpath))
				{
					 writeTestResults("Validate gift card with valid value when gift crd type is 'partially  redeem", msgSuccessForGiftCardPartRedeem+" " +
					 		"message should be displayed.","Following message is displayed. "+getText(msgErrorxpath),"42709334",false,true);
				}
				else
				{
					 writeTestResults("Validate gift card with valid value when gift crd type is 'partially  redeem", 
							 msgSuccessForGiftCardPartRedeem+" message should be displayed.","Validation xpath is not displayed or incorrect xpath","42709334",false,true);
				}			
			}
		}
		else
		{
			 writeTestResults("Validate gift card with valid value when gift crd type is 'partially  redeem", msgSuccessForGiftCardPartRedeem+" message should be displayed.","Gift card txt box or button is not displayed or incorrect xpath","",false,true);
		}
	}
		
	public void verifyValidCouponCodeWithFixedValue() throws Exception
	{
		clearCookies();
		
		pageRefersh();	
		
		verifyItemCount();
		
		double grandTotalBeforeEnterCoupon = 0.0;
		
		if(isElementPresent(grandTotalXpathBeforeDiscount))
		{
			grandTotalBeforeEnterCoupon =common.getValueWitoutCurrency(getText(grandTotalXpathBeforeDiscount));				
		}
		
		enablePromotionCodeTxtBox();
		
		if(isElementPresent(txtCouponCodeXpath) && isElementPresent(btnCouponCode))
		{
			sendKeys(txtCouponCodeXpath,txtCouponCodeFixed);
			
			click(btnCouponCode);			
			
			if(isDisplayed(msgSuccessxpath))
			{
				if(getText(msgSuccessxpath).equalsIgnoreCase(msgSuccessForCouponFixed))
				{
					 writeTestResults("Validate coupon code with valid value when coupon type is 'fixed amount discount'", msgSuccessForCouponFixed+" " +
					 		"message should be displayed.",msgSuccessForCouponFixed+" message is displayed.","426fd9c6",true,false);
				}
				else
				{
					 writeTestResults("Validate coupon code with valid value when coupon type is 'fixed amount discount'", msgSuccessForCouponFixed+" " +
					 		"message should be displayed.",getText(msgSuccessxpath)+" message is displayed.","426fd9c6",true,false);
				}
				
				verifyGrandTotalAfterRedeemForFixed(grandTotalBeforeEnterCoupon);
			}
			else
			{
				if(isDisplayed(msgErrorxpath))
				{
					 writeTestResults("Validate coupon code with valid value when coupon type is 'fixed amount discount'",
							 msgSuccessForCouponFixed+" message should be displayed.","Following message is displayed. "+getText(msgErrorxpath),"426fd9c6",false,true);
				}
				else
				{
					 writeTestResults("Validate coupon code with valid value when coupon type is 'fixed amount discount'", 
							 msgSuccessForCouponFixed+" message should be displayed.","Validation xpath is not displayed or incorrect xpath","426fd9c6",false,true);
				}				
			}
		}
		else
		{
			 writeTestResults("Validate coupon code with valid value when coupon type is 'fixed amount discount'", msgSuccessForCouponFixed+" " +
			 		"message should be displayed.","coupon code txt box or button is not displayed or incorrect xpath","426fd9c6",false,true);
		}
	}
	
	public void verifyValidCouponCodeWithPercentOfPriceValue() throws Exception
	{
		clearCookies();
		
		pageRefersh();
		
		verifyItemCount();
		
		double grandTotalBeforeEnterCoupon = 0.0;
		
		if(isElementPresent(grandTotalXpathBeforeDiscount))
		{
			grandTotalBeforeEnterCoupon =common.getValueWitoutCurrency(getText(grandTotalXpathBeforeDiscount));				
		}
		
		enablePromotionCodeTxtBox();
		
		if(isElementPresent(txtCouponCodeXpath) && isElementPresent(btnCouponCode))
		{
			sendKeys(txtCouponCodeXpath,txtCouponCodePrecent);
			
			click(btnCouponCode);
			
			if(isDisplayed(msgSuccessxpath))
			{
				if(getText(msgSuccessxpath).equalsIgnoreCase(msgSuccessForCouponPrecent))
				{
					 writeTestResults("Validate coupon code with valid value when coupon type is 'percent of product price discount'",
							 msgSuccessForCouponPrecent+" message should be displayed.",msgSuccessForCouponPrecent+" message is displayed.","",true,false);
								
				}
				else
				{
					 writeTestResults("Validate coupon code with valid value when coupon type is 'percent of product price discount'", 
							 msgSuccessForCouponPrecent+" message should be displayed.",getText(msgSuccessxpath)+" message is displayed.","",true,false);
				}
				
				verifyGrandTotalAfterRedeemForPercent(grandTotalBeforeEnterCoupon);
			}
			else
			{
				if(isDisplayed(msgErrorxpath))
				{
					 writeTestResults("Validate coupon code with valid value when coupon type is 'percent of product price discount'", msgSuccessForCouponPrecent+" message should be displayed.","Following message is displayed. "+getText(msgErrorxpath),"",false,true);
				}
				else
				{
					 writeTestResults("Validate coupon code with valid value when coupon type is 'percent of product price discount'", msgSuccessForCouponPrecent+" message should be displayed.","Validation xpath is not displayed or incorrect xpath","",false,true);
				}				
			}
		}
		else
		{
			 writeTestResults("Validate coupon code with valid value when coupon type is 'percent of product price discount'", msgSuccessForCouponPrecent+" message should be displayed.","coupon code txt box or button is not displayed or incorrect xpath","",false,true);
		}
	}
	
	public void verifyGrandTotalAfterRedeemForFixed(double grandTotalBeforeEnterCoupon) throws Exception 
	{
		getLocation();
		
		if(isElementPresent(grandTotalXpathAfterDiscount))
		{
			String grandTotalStringValue = getText(grandTotalXpathAfterDiscount);
			
			double grandTotal  = discountTypeIsFixed(grandTotalBeforeEnterCoupon);
			
			System.out.println("grandTotal"+grandTotal);
			
			int retval = Double.compare(grandTotal, common.getValueWitoutCurrency(grandTotalStringValue));	
					 
			if(retval == 0)
			{
				 writeTestResults("Verify total value updates after redeem the coupon when coupon type is 'fixed amount discount'", 
						 grandTotal+" amount should be displayed.",grandTotal+" amount is displayed.","426fdd86",true,false);				
			}
			else
			{
				 writeTestResults("Verify total value updates after redeem the coupon when coupon type is 'fixed amount discount'", grandTotal+" " +
				 		"amount should be displayed.",getText(grandTotalXpathAfterDiscount)+" amount is displayed.","426fdd86",false,true);
			}
			
		}
		else
		{
			 writeTestResults("Verify total value updates after redeem the coupon when coupon type is 'fixed amount discount'", 
					 "Total amount xpath should be displayed.","Total amount xpath is not displayed.","426fdd86",false,true);
		}		
	}
	
	private void verifyGrandTotalForGiftCardFullRedeem(double grandTotalBeforeEnterGiftCard) throws Exception 
	{	
		if(isElementPresent(grandTotalXpathFullRedeemDiscount))
		{
			String grandTotalStringValue = getText(grandTotalXpathFullRedeemDiscount);
			
			double grandTotal  = 0.00;
			
			System.out.println("grandTotal"+grandTotal);
			
			int retval = Double.compare(grandTotal, common.getValueWitoutCurrency(grandTotalStringValue));	
					 
			if(retval == 0)
			{
				 writeTestResults("Verify total value updates after redeem the gift card when card type is 'full redeem'", grandTotal+" amount should be displayed.",
						 grandTotal+" amount is displayed.","",true,false);
				
			}
			else
			{
				 writeTestResults("Verify total value updates after redeem the gift card when card type is 'full redeem'", grandTotal+" amount should be displayed.",
						 getText(grandTotalXpathFullRedeemDiscount)+" amount is displayed.","",false,true);
			}
			
		}
		else
		{
			 writeTestResults("Verify total value updates after redeem the gift card when card type is 'full redeem'", "Total amount xpath should be displayed.","Total amount xpath is not displayed.","",false,true);
		}		
	}
	
	private void verifyGrandTotalForGiftCardPartRedeem(double grandTotalBeforeEnterGiftCard) throws Exception 
	{	
		if(isElementPresent(grandTotalXpathAfterDiscount))
		{
			String grandTotalStringValue = getText(grandTotalXpathFullRedeemDiscount);
			
			double grandTotal  = discountTypeIsPartRedeem(grandTotalBeforeEnterGiftCard);
			
			grandTotalStringValue = grandTotalStringValue.replaceAll(",","");
			
			System.out.println("grandTotal"+grandTotal);
			
			int retval = Double.compare(grandTotal, common.getValueWitoutCurrency(grandTotalStringValue));	
					 
			if(retval == 0)
			{
				 writeTestResults("Verify total value updates after redeem the gift card when card type is 'part redeem'", grandTotal+" amount should be displayed.",
						 grandTotal+" amount is displayed.","42708c68",true,false);
				
			}
			else
			{
				 writeTestResults("Verify total value updates after redeem the gift card when card type is 'part redeem'", grandTotal+" amount should be displayed.",
						 getText(grandTotalXpathAfterDiscount)+" amount is displayed.","42708c68",false,true);
			}
			
		}
		else
		{
			 writeTestResults("Verify total value updates after redeem the gift card when card type is 'part redeem'", "Total amount xpath should be displayed.","Total amount xpath is not displayed.","",false,true);
		}		
	}
	
	private double discountTypeIsFixed(double grandTotalBeforeEnterCoupon) throws Exception 
	{	
		DecimalFormat convertToDecimal = new DecimalFormat("###.##");
		
		return (Double.valueOf(convertToDecimal.format(grandTotalBeforeEnterCoupon - Double.parseDouble(couponValueFixed))));		
	}
	
	
	
	
	private double discountTypeIsPercent(double grandTotalBeforeEnterCoupon) throws Exception 
	{	
		DecimalFormat convertToDecimal = new DecimalFormat("###.##");
		
		return (Double.valueOf(convertToDecimal.format((grandTotalBeforeEnterCoupon * (100.0 - Double.parseDouble(couponValuePrecent))) / 100)));		
	}
	
	private double discountTypeIsPartRedeem(double grandTotalBeforeEnterCoupon) throws Exception 
	{	
		DecimalFormat convertToDecimal = new DecimalFormat("###.##");
		
		return (Double.valueOf(convertToDecimal.format(grandTotalBeforeEnterCoupon - Double.parseDouble(giftCardValuePart))));		
	}
	
	private void verifyGrandTotalAfterRedeemForPercent(double grandTotalBeforeEnterCoupon) throws Exception 
	{
	
		if(isElementPresent(grandTotalXpathAfterDiscount))
		{
			String grandTotalStringValue = getText(grandTotalXpathAfterDiscount);
			
			double grandTotal  = discountTypeIsPercent(grandTotalBeforeEnterCoupon);
			
			System.out.println("grandTotal"+grandTotal);
			
			int retval = Double.compare(grandTotal, common.getValueWitoutCurrency(grandTotalStringValue));	
					 
			if(retval == 0)
			{
				 writeTestResults("Verify total value updates after redeem the coupon when coupon type is 'percent of product price discount'",
						 grandTotal+" amount should be displayed.",grandTotal+" amount is displayed.","",true,false);
			}
			else
			{
				 writeTestResults("Verify total value updates after redeem the coupon when coupon type is 'percent of product price discount'", 
						 grandTotal+" amount should be displayed.",getText(grandTotalXpathAfterDiscount)+" amount is displayed.","",false,false);
			}			
		}
		else
		{
			 writeTestResults("Verify total value updates after redeem the coupon when coupon type is 'percent of product price discount'", "Total amount xpath should be displayed.","Total amount xpath is not displayed.","",false,true);
		}		
	}
	
	public void shoppingCartProfilerResults(String pageName)throws Exception 
	{
		common.getQueryResults(pageName);			
	}

	public void totalTime(java.util.Date startTime) throws Exception
	{		
		java.util.Date endTime;		
		Calendar cal = Calendar.getInstance(); 		
		endTime = cal.getTime();
		String totalTime = common.totalTime(startTime, endTime);		   
	    writeTestResults("Total time to execute "+writeFileName, "Total time =",totalTime,"",true,false);		
	}
		
}