/*
 * Home page functinality
 * 
 * NTAF 1.0
 *
 * 2013-06-14
 * 
 * Powered by netstarters
 */

package com.net.test.pageObject;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Random;

import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.server.handler.SendKeys;

import com.net.test.data.CheckoutTestData;
import com.net.test.data.HomeTestData;
import com.net.test.data.MyAccountTestData;
import com.net.test.util.DBConnectionUtill;
import com.net.test.util.ReadXl;
import com.net.test.util.TestCommonMethods;
import com.net.test.util.WriteResults;


public class HomePage extends HomeTestData
{
	
	WriteResults wResult =  new WriteResults();
	ReadXl readParametrs  = new ReadXl();
	TestCommonMethods common = new TestCommonMethods();
	CheckOutProcess ckProcess = new CheckOutProcess();
	CheckoutTestData checkoutTestData = new CheckoutTestData();
	//CreateIssue createIssue = new CreateIssue();
	DBConnectionUtill db = new DBConnectionUtill();
	MyAccount account = new MyAccount();
	MyAccountTestData myAccountTestData = new MyAccountTestData();
	//SendTextMessage sendSMS = new SendTextMessage();
	
	public String readDataFileName;
	public String setLinkXpath;
	public String getLinkXpath;		
	public String getTextValue;
	public String linkPosition;
	public static String beforeClickUrl;	
	public int i = 0;
	
	
	public void resultSheetName(String resultSheet)throws Exception 
	{
		writeFileName=resultSheet;		
	}
	
	public void skipMethods(String methodName)throws Exception 
	{		
		wResult.writeSkipTestResult("Home Page ",methodName ,"", "Skip Results");
	}	
	
	public void goBackToHomePage()throws Exception 
	{
		if(!common.siteUrl.equalsIgnoreCase(getCurrentUrl()))
		{
			//logFile("open home page");
			openPage(common.siteUrl);				
		}
	}
	
	public void appLoadResults()throws Exception 
	{
		if(!siteTitle.equalsIgnoreCase(getTitle()))
		{	
			writeTestResults("Verify the home page title", "Expected home page title is "+siteTitle , "Actual home page title is "+getTitle(),"",false,true);
		}
		else
		{
			writeTestResults("Verify the home page title", "Expected home page title is "+siteTitle , "Actual home page title is "+getTitle(),"",true,false);			
		}				
	}
	
	public void clickLogo() throws Exception
	{		
		if(!isElementPresent(mainLogo))			
		{
			openPage(common.siteUrl);				
		}
		else
		{
			click(mainLogo);			
		}	
	}	
	
	public void mainLogo()throws Exception 
	{	
		if(!isElementPresent(mainLogo))			
		{
			openPage(common.siteUrl);
			
			writeTestResults("Verify main logo present in correct place", "Main logo should be displayed in the header" , "Main logo position is incorrect or xpath("+mainLogo+") is incorrect","",false,true);			
		}
		else if(!click(mainLogo))
		{
			writeTestResults("Verify main logo can click in home page", "User can click the main logo" , "User can't click the main logo","",false,true);
		}		
	}
	
	public void mainLogoToolTip()throws Exception 
	{		
		if(!isElementPresent(mainLogo))			
		{
			writeTestResults("Verify main logo tool tip present in correct place", "Main logo tool tip should be displayed" , "Main logo tool tip position is incorrect or xpath("+mainLogoToolTip+") is incorrect","",false,true);			
		}
		else if(!getAttribute(mainLogo,mainLogoToolTip).equalsIgnoreCase(mainLogoToolTipText))
		{
			writeTestResults("Verify main logo tool tip text", "Expected text is "+ mainLogoToolTipText, "Actual text is "+getAttribute(mainLogo,mainLogoToolTip),"",false,true);
		}
		else
		{
			if(!getAttribute(mainLogo,mainLogoToolTip).isEmpty())
			{
				writeTestResults("Verify main logo tool tip text", "Expected text is "+ mainLogoToolTipText, "Actual text is "+getAttribute(mainLogo,mainLogoToolTip) ,"",true,false);
					
			}
					
		}		
	}	
	
	public void mainLogoResults()throws Exception 
	{
		if(!(common.siteUrl).equalsIgnoreCase(getCurrentUrl()))
		{
			writeTestResults("Verify the home main logo link should redirect to the sitemap Page when user in Home page", "Main logo link should redirect to the site Map" , "Main logo link is not redirect to the site Map. " +
					"Following page title got it '"+getCurrentUrl()+ "'","427255f2",false,true);
		}
		else
		{
			writeTestResults("Verify the home main logo link should redirect to the sitemap Page when user in Home page", "Main logo link should redirect to the site Map", "Main logo link is redirect to the site map and page title is " +
					""+getTitle()+ " Page" ,"427255f2",true,false);
		}		
	}	
	
	public void searchTerm()throws Exception 
	{	
		if(!isElementPresent(searchTextBox))
		{
			writeTestResults("Verify the user can type text in basic search", "user can type text in basic search" , "Search text box xpath not present","",false,true);
		}
		else
		{
			writeTestResults("Verify the user can type text in basic search", "user can type text in basic search" , "user can type text ("+searchTerm+") in basic search" ,"",true,false);
			
			click(searchTextBox);
						
			sendKeys(searchTextBox,searchTerm);
			pressEnter(searchTextBox);
			
			if(!isElementPresent(searchButton))
			{
				writeTestResults("Verify search button", "Search button should be displayed in home page" , "Search button xpath("+searchButton+") not present","",false,true);
			}
			else
			{
				clickAndWait(searchButton);
				
				
				
			}
			
			if(isDisplayed("//*[@id='cboxClose']"))
			{

				clickAndWait("//*[@id='cboxClose']");
			}
			
			
		}
	}
	
	public void searchResults()throws Exception 
	{	
		String categoryXpath = ckProcess.getProductAttributes.replace("{id}", "[1]");
		
		if(getTitle().indexOf("Search results for:")>-1)
		{
			writeTestResults("Verify the Search results", "Search results page should displayed" , "Search results page is displayed" ,"4272453a",true,false);						
		}
		else
		{
			writeTestResults("Verify the Search results", "Search results page should displayed" , "Search results page is not displayed.Actual title is "+getTitle(),"4272453a",false,true);
		}
	}
	
	public void appLink(String sheetName) throws Exception 
	{	
		goBackToHomePage();
		
		readDataFileName = sheetName;
		linkPosition = sheetName.replace("Links", "").toLowerCase();
		boolean mouseMoveAction = false;
		String mainCateName = "";
		
		//logFile(sheetName);
		String winHandleBefore ="";
		String pageTitle = "";
		String pageURL = "";
		int hasTitleTag = 1;
		String xlSheetValue ="";
		String xlSheetTitle ="";
		beforeClickUrl = null;
		int failCount = 0;
		int failCountInMainNav = 0;
		int failCountInFooter = 0;
		
		
		readParametrs.getLinksFromDataFile(readDataFileName);
		
		readParametrs.getLinksXpath(readDataFileName);
		
		
		for(int k = 0;k <= (readParametrs.totalNoOfColoums-1); k++)
		{	
			mainCateName = "";
			
		
			
			int totalLinks = 0;
			if(k==0)
			{					
				totalLinks = Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(0).toString().trim());
				
				getLinkXpath = readParametrs.linksXpathName.get(0).toString().trim();
				
				System.out.println("getLinkXpath"+getLinkXpath);
				
				if(isExecuted(getMethodIsExecuted,"Footer title is inside the class") && sheetName.equalsIgnoreCase("Footer Links"))
				{
					hasTitleTag = 2;
					totalLinks = totalLinks + 1;
				}
				else
				{
					hasTitleTag = 1;
				}				
				
				if(!sheetName.equalsIgnoreCase("Main Navigation"))
				{
					if(sheetName=="Header Links")
					
					{
						hasTitleTag=hasTitleTag+1;
					}
					
					
					for(i=hasTitleTag;i<totalLinks;i++)
					{				
						setLinkXpath = findElementInXLSheet(getParameterXpath,getLinkXpath).trim().replace("{id}", "["+i+"]");
							
						System.out.println("Link Xpath - "+setLinkXpath);
						
						
						if(!isElementPresent(setLinkXpath))
						{
							openPage(common.siteUrl);
							
							if(!isElementPresent(setLinkXpath))
							{
								writeTestResults("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get(i).toString().trim()+"' xpath present in the page",setLinkXpath+" xpath should be displayed in the page" ,setLinkXpath+" xpath is not displayed","",false,false);	
								
								if(sheetName.equalsIgnoreCase("Main Navigation"))
								{
									failCount++;
									failCountInMainNav++;
								}
								
								
								if(sheetName.equalsIgnoreCase("Footer Links"))
								{
									failCountInFooter++;
								}							
							}								
						}
						
						
						else
						{
							getTextValue = getText(setLinkXpath);
							
							if(getTextValue.equalsIgnoreCase(""))
							{
								getTextValue = getAttribute(setLinkXpath,"title");
							}
							//logFile("getText1"+getTextValue);
							
							if(isExecuted(getMethodIsExecuted,"Footer title is inside the class") && sheetName.equalsIgnoreCase("Footer Links"))
							{
								xlSheetValue = readParametrs.nameOfLink.get(i-1).toString().trim();
							}
							else
							{
								if(sheetName=="Header Links")
									
								{
									xlSheetValue = readParametrs.nameOfLink.get(i-1).toString().trim();
								}
								
								else
								{
									xlSheetValue = readParametrs.nameOfLink.get(i).toString().trim();
								}
							}
							
							if(!xlSheetValue.equalsIgnoreCase(getTextValue))
							{							
								writeTestResults("Verify the "+linkPosition+", '"+xlSheetValue+"' text is present in the correct place","'"+ xlSheetValue+"' text should displayed in the "+linkPosition ,"'"+ xlSheetValue+" text is not displayed in correct place","",false,false);
								
								if(sheetName.equalsIgnoreCase("Main Navigation"))
								{
									failCount++;
									failCountInMainNav++;
								}
								if(sheetName.equalsIgnoreCase("Footer Links"))
								{
									failCountInFooter++;
								}
							}
							else 
							{
								beforeClickUrl = getCurrentUrl();
																
								clickAndWait(setLinkXpath);
																
								Thread.sleep(100);
								
								if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
								{
									openPage(getAttribute(setLinkXpath,"href"));
								}
								
								//logFile("total windows"+driver.getWindowHandles().size());
								
								if(getCurrentUrl().indexOf("/#")>-1 && driver.getWindowHandles().size() <= 1)
								{
									writeTestResults("Verify the "+linkPosition+", '"+xlSheetValue+"' link is redirects to the correct content page.","Expected title of the '"+xlSheetValue+"' link is "+xlSheetValue,"Link is not working","",false,false);
									
									if(sheetName.equalsIgnoreCase("Main Navigation"))
									{
										failCount++;
										failCountInMainNav++;
									}
									if(sheetName.equalsIgnoreCase("Footer Links"))
									{
										failCountInFooter++;
									}
								}
								else
								{									
									 pageTitle = getTitle();
									 pageURL =getCurrentUrl();
									
									 if(driver.getWindowHandles().size()>1)
									 {
										 winHandleBefore = driver.getWindowHandle();
										 for(String winHandle : driver.getWindowHandles())
										 {
											 driver.switchTo().window(winHandle);
											 Thread.sleep(3000);
										     pageTitle = getTitle();
											 pageURL =getCurrentUrl();											 
										 }		
										  driver.close();
											 
										  driver.switchTo().window(winHandleBefore);
									 }
									 
									 if(isExecuted(getMethodIsExecuted,"Footer title is inside the class") && sheetName.equalsIgnoreCase("Footer Links"))
									 {
										 xlSheetTitle = readParametrs.titleOfLink.get(i-1).toString().trim();
									 }
									 else
									 {
										 
										 if(sheetName=="Header Links")
												
											{
											 xlSheetTitle = readParametrs.titleOfLink.get(i-1).toString().trim();
											}
										 
										 else
										 {
											 xlSheetTitle = readParametrs.titleOfLink.get(i).toString().trim();
										 }
									 }
									 
									 
									 
									 
									 if(xlSheetTitle.equals("label"))
										{
										    if(getTextValue.toLowerCase().equals(xlSheetValue.toLowerCase()))
										    	{
										    		writeTestResults("Verify the "+linkPosition+mainCateName+", '"+xlSheetValue+"' label is display in the correct place.","Expected label of the '"+xlSheetValue+"' label is "+xlSheetValue,"Actual label of the '"+xlSheetValue+"' label is "+xlSheetValue ,"",true,false);
										    	}
										    else
										    {
										    	writeTestResults("Verify the "+linkPosition+mainCateName+", '"+xlSheetValue+"' label is display in the correct place.","Expected label of the '"+xlSheetValue+"' label is "+xlSheetValue,"Actual label of the '"+xlSheetValue+"' label is "+xlSheetValue ,"",false,false);
										    	
										    }
										}
									 
									 else if(!pageTitle.equalsIgnoreCase(xlSheetTitle))
									
									{										
										writeTestResults("Verify the "+linkPosition+", '"+xlSheetValue+"' link is redirects to the correct content page.","Expected url of the '"+xlSheetValue+"' link is "+xlSheetTitle,"Actual url of the '"+xlSheetValue+"' is '"+pageTitle ,"",false,false);
										
										if(sheetName.equalsIgnoreCase("Main Navigation"))
										{
											failCount++;
											failCountInMainNav++;
										}
										if(sheetName.equalsIgnoreCase("Footer Links"))
										{
											failCountInFooter++;
										}
									}
									else
									{									
										writeTestResults("Verify the "+linkPosition+", '"+xlSheetValue+"' link is redirects to the correct content page.","Expected Page Title  of the '"+xlSheetValue+"' link is "+xlSheetTitle,"Actual Title of the '"+xlSheetValue+"' is "+pageTitle ,"",true,false);
									}
							  		
							  		goBack();
									
									goBackToHomePage();
										
								}					
						
							}	
								
						}
		
					}
				}
				
				if(sheetName.equalsIgnoreCase("Main Navigation"))
				{
					if(sheetName=="Header Links")
					
					{
						hasTitleTag=hasTitleTag+1;
					}
					
					if(sheetName=="Main Navigation")
						
					{
						hasTitleTag=hasTitleTag+1;
					}
					
					for(i=hasTitleTag;i<(totalLinks+1);i++)
					{				
						setLinkXpath = findElementInXLSheet(getParameterXpath,getLinkXpath).trim().replace("{id}", "["+i+"]");
							
						System.out.println("Link Xpath - "+setLinkXpath);
						
						
						if(!isElementPresent(setLinkXpath))
						{
							openPage(common.siteUrl);
							
							if(!isElementPresent(setLinkXpath))
							{
								writeTestResults("Verify the "+linkPosition+", '"+readParametrs.nameOfLink.get(i).toString().trim()+"' xpath present in the page",setLinkXpath+" xpath should be displayed in the page" ,setLinkXpath+" xpath is not displayed","",false,false);	
								
								if(sheetName.equalsIgnoreCase("Main Navigation"))
								{
									failCount++;
									failCountInMainNav++;
								}
								
								
								if(sheetName.equalsIgnoreCase("Footer Links"))
								{
									failCountInFooter++;
								}							
							}								
						}
						
						
						else
						{
							getTextValue = getText(setLinkXpath);
							
							if(getTextValue.equalsIgnoreCase(""))
							{
								getTextValue = getAttribute(setLinkXpath,"title");
							}
							//logFile("getText1"+getTextValue);
							
							if(isExecuted(getMethodIsExecuted,"Footer title is inside the class") && sheetName.equalsIgnoreCase("Footer Links"))
							{
								xlSheetValue = readParametrs.nameOfLink.get(i-1).toString().trim();
							}
							else
							{
								if(sheetName=="Header Links")
									
								{
									xlSheetValue = readParametrs.nameOfLink.get(i-1).toString().trim();
								}
								
								if(sheetName=="Main Navigation")
									
								{
									xlSheetValue = readParametrs.nameOfLink.get(i-1).toString().trim();
								}
								else
								{
									xlSheetValue = readParametrs.nameOfLink.get(i).toString().trim();
								}
							}
							
							if(!xlSheetValue.equalsIgnoreCase(getTextValue))
							{							
								writeTestResults("Verify the "+linkPosition+", '"+xlSheetValue+"' text is present in the correct place","'"+ xlSheetValue+"' text should displayed in the "+linkPosition ,"'"+ xlSheetValue+" text is not displayed in correct place","",false,false);
								
								if(sheetName.equalsIgnoreCase("Main Navigation"))
								{
									failCount++;
									failCountInMainNav++;
								}
								if(sheetName.equalsIgnoreCase("Footer Links"))
								{
									failCountInFooter++;
								}
							}
							else 
							{
								beforeClickUrl = getCurrentUrl();
																
								clickAndWait(setLinkXpath);
																
								Thread.sleep(100);
								
								if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
								{
									openPage(getAttribute(setLinkXpath,"href"));
								}
								
								//logFile("total windows"+driver.getWindowHandles().size());
								
								if(getCurrentUrl().indexOf("/#")>-1 && driver.getWindowHandles().size() <= 1)
								{
									writeTestResults("Verify the "+linkPosition+", '"+xlSheetValue+"' link is redirects to the correct content page.","Expected title of the '"+xlSheetValue+"' link is "+xlSheetValue,"Link is not working","",false,false);
									
									if(sheetName.equalsIgnoreCase("Main Navigation"))
									{
										failCount++;
										failCountInMainNav++;
									}
									if(sheetName.equalsIgnoreCase("Footer Links"))
									{
										failCountInFooter++;
									}
								}
								else
								{									
									 pageTitle = getTitle();
									 pageURL =getCurrentUrl();
									
									 if(driver.getWindowHandles().size()>1)
									 {
										 winHandleBefore = driver.getWindowHandle();
										 for(String winHandle : driver.getWindowHandles())
										 {
											 driver.switchTo().window(winHandle);
											 Thread.sleep(3000);
										     pageTitle = getTitle();
											 pageURL =getCurrentUrl();											 
										 }		
										  driver.close();
											 
										  driver.switchTo().window(winHandleBefore);
									 }
									 
									 if(isExecuted(getMethodIsExecuted,"Footer title is inside the class") && sheetName.equalsIgnoreCase("Footer Links"))
									 {
										 xlSheetTitle = readParametrs.titleOfLink.get(i-1).toString().trim();
									 }
									 else
									 {
										 
										 if(sheetName=="Header Links")
												
											{
											 xlSheetTitle = readParametrs.titleOfLink.get(i-1).toString().trim();
											}
										 
										 if(sheetName=="Main Navigation")
												
											{
											 xlSheetTitle = readParametrs.titleOfLink.get(i-1).toString().trim();
											}
										 else
										 {
											 xlSheetTitle = readParametrs.titleOfLink.get(i).toString().trim();
										 }
									 }
									 
									 
									 
									 
									 if(xlSheetTitle.equals("label"))
										{
										    if(getTextValue.toLowerCase().equals(xlSheetValue.toLowerCase()))
										    	{
										    		writeTestResults("Verify the "+linkPosition+mainCateName+", '"+xlSheetValue+"' label is display in the correct place.","Expected label of the '"+xlSheetValue+"' label is "+xlSheetValue,"Actual label of the '"+xlSheetValue+"' label is "+xlSheetValue ,"",true,false);
										    	}
										    else
										    {
										    	writeTestResults("Verify the "+linkPosition+mainCateName+", '"+xlSheetValue+"' label is display in the correct place.","Expected label of the '"+xlSheetValue+"' label is "+xlSheetValue,"Actual label of the '"+xlSheetValue+"' label is "+xlSheetValue ,"",false,false);
										    	
										    }
										}
									 
									 else if(!pageTitle.equalsIgnoreCase(xlSheetTitle))
									
									{										
										writeTestResults("Verify the "+linkPosition+", '"+xlSheetValue+"' link is redirects to the correct content page.","Expected url of the '"+xlSheetValue+"' link is "+xlSheetTitle,"Actual url of the '"+xlSheetValue+"' is '"+pageTitle ,"",false,false);
										
										if(sheetName.equalsIgnoreCase("Main Navigation"))
										{
											failCount++;
											failCountInMainNav++;
										}
										if(sheetName.equalsIgnoreCase("Footer Links"))
										{
											failCountInFooter++;
										}
									}
									else
									{									
										writeTestResults("Verify the "+linkPosition+", '"+xlSheetValue+"' link is redirects to the correct content page.","Expected Page Title  of the '"+xlSheetValue+"' link is "+xlSheetTitle,"Actual Title of the '"+xlSheetValue+"' is "+pageTitle ,"",true,false);
									}
							  		
							  		goBack();
									
									goBackToHomePage();
										
								}					
						
							}	
								
						}
		
					}
				}
				
				

				if(failCountInMainNav > 0 )
				{
					writeTestResultsToUJ("427362f8",false);
				}
				else
				{
					writeTestResultsToUJ("427362f8",true);
				}
			}
			if(k>0)
			{
				getLinkXpath =readParametrs.linksXpathName.get(k).toString().trim();
				
				totalLinks = Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k).toString().trim())-Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k-1).toString().trim());
				
				int previousSize = Integer.parseInt(readParametrs.totalNoOFLinksPerColoum.get(k-1).toString().trim());
				
				if(isExecuted(getMethodIsExecuted,"Footer title is inside the class") && sheetName.equalsIgnoreCase("Footer Links"))
				{
					hasTitleTag = 2;
					totalLinks = totalLinks + 1;
				}
				else
				{
					hasTitleTag = 1;
				}
				
				/**
				 * get main category name
				 */
				String mainCategoryXpath = "";
				
				if(isExecuted(getMethodIsExecuted,"Mouse Hover In Main Navigation") && sheetName.equalsIgnoreCase("Main Navigation"))
				{
					String mainCategoryName = readParametrs.nameOfLink.get(hasTitleTag-1+previousSize).toString().trim();
					
					mainCategoryName = mainCategoryName.substring(mainCategoryName.indexOf("links in")+9).trim();
					
					System.out.println("Main Category Name - "+mainCategoryName);
					
					mainCategoryXpath = readXL.getMainBrandXpath(mainCategoryName);	
					
					System.out.println("mainCategoryXpath"+mainCategoryXpath);
									
						
				}	
				
				for(int j=hasTitleTag;j<(totalLinks);j++)
				{	
					if(!mainCategoryXpath.isEmpty()||!mainCategoryXpath.equalsIgnoreCase(null))
					{
						if(isElementPresent(mainCategoryXpath))
						{
							
							//mouseMove(mainCategoryXpath);
							System.out.println("mainCategoryXpath"+mainCategoryXpath);
							mouseMove(mainCategoryXpath);
							mainCateName ="("+readXL.brandName+")";
						}
					}	
					
					//System.out.println("getLinkXpath"+getLinkXpath);
					setLinkXpath = findElementInXLSheet(getParameterXpath,getLinkXpath).trim().replace("{id}", "["+j+"]");
					
					//System.out.println("setLinkXpath"+setLinkXpath);
					
					if(!isElementPresent(setLinkXpath))
					{
						openPage(common.siteUrl);
						
						if(!isElementPresent(setLinkXpath))
						{
							writeTestResults("Verify the "+linkPosition+mainCateName+", '"+readParametrs.nameOfLink.get(j+previousSize).toString().trim()+"' xpath present in the page",setLinkXpath+" xpath should be displayed in the page" ,setLinkXpath+" xpath is not displayed","",false,false);	
						}						
					}
					else
					{
						getTextValue = getText(setLinkXpath);
							
						if(getTextValue.equalsIgnoreCase(""))
						{
							getTextValue = getAttribute(setLinkXpath,"title");
						}
						
						if(isExecuted(getMethodIsExecuted,"Footer title is inside the class") && sheetName.equalsIgnoreCase("Footer Links"))
						{
							xlSheetValue = readParametrs.nameOfLink.get(j-1+previousSize).toString().trim();
						}
						else
						{
							xlSheetValue = readParametrs.nameOfLink.get(j+previousSize).toString().trim();
						}						
						
						if(!xlSheetValue.toString().trim().equalsIgnoreCase(getTextValue))
						{						
							writeTestResults("Verify the "+linkPosition+mainCateName+", '"+xlSheetValue+"' text is present in the correct place","'"+ xlSheetValue+"' text should displayed in the "+linkPosition ,"'"+ xlSheetValue+" text is not displayed in correct place","",false,false);
							
							if(sheetName.equalsIgnoreCase("Footer Links"))
							{
								failCountInFooter++;
							}
						}
						
						
						
						
						
						
						else
						{
							beforeClickUrl = getCurrentUrl();
							
							clickAndWait(setLinkXpath);
							
							Thread.sleep(100);
							if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
							{
								openPage(getAttribute(setLinkXpath,"href"));
							}
						
							if(getCurrentUrl().indexOf("/#")>-1 && driver.getWindowHandles().size() <= 1)
							{
								writeTestResults("Verify the "+linkPosition+mainCateName+", '"+xlSheetValue+"' link is redirects to the correct content page.","Expected title of the '"+xlSheetValue+"' link is "+xlSheetValue,"Link is not working","",false,false);
								
								if(sheetName.equalsIgnoreCase("Footer Links"))
								{
									failCountInFooter++;
								}
							}
							else
							{
								 pageTitle = getTitle();
								 pageURL =getCurrentUrl();
								 String Elementlabel= getText(setLinkXpath);
								 
								 if(driver.getWindowHandles().size()>1)
								 {
									 winHandleBefore = driver.getWindowHandle();
										
									 for(String winHandle : driver.getWindowHandles())
									 {
									      driver.switchTo().window(winHandle);
									      Thread.sleep(3000);
									      
									      pageTitle = getTitle();
										  pageURL =getCurrentUrl();
										  
									  }		
									  driver.close();
	
									  driver.switchTo().window(winHandleBefore);
								 }
								 
								if(isExecuted(getMethodIsExecuted,"Footer title is inside the class") && sheetName.equalsIgnoreCase("Footer Links"))
								{
									xlSheetTitle = readParametrs.titleOfLink.get(j-1+previousSize).toString().trim();
								}
								else
								{
									xlSheetTitle = readParametrs.titleOfLink.get(j+previousSize).toString().trim();
								} 
								
								
								if(xlSheetTitle.equals("label"))
								{
									writeTestResults("Verify the "+linkPosition+mainCateName+", '"+xlSheetValue+"' label is display in the correct place.","Expected label of the '"+xlSheetValue+"' label is "+xlSheetValue,"Actual label of the '"+xlSheetValue+"' label is "+xlSheetValue ,"",true,false);
									
								}
								
								
								
								
								
								
								
								else if(!pageTitle.equalsIgnoreCase(xlSheetTitle))
									// {
										 
									// }
									
								//if(!pageTitle.equalsIgnoreCase(xlSheetTitle))
								{											
									writeTestResults("Verify the "+linkPosition+mainCateName+", '"+xlSheetValue+"' link is redirects to the correct content page.","Expected url of the '"+xlSheetValue+"' link is "+xlSheetTitle,"Actual url of the '"+xlSheetValue+"' is "+pageTitle,"",false,false);
									
									if(sheetName.equalsIgnoreCase("Footer Links"))
									{
										failCountInFooter++;
									}
								}
								
								
								
								else
								{									
									writeTestResults("Verify the "+linkPosition+mainCateName+", '"+xlSheetValue+"' link is redirects to the correct content page.","Expected url of the '"+xlSheetValue+"' link is "+xlSheetTitle,"Actual url of the '"+xlSheetValue+"' link is "+pageTitle ,"",true,false);
								}
							}								
						}
						goBack();	
						goBackToHomePage();						
					}
				}
			}			
		}
		
		if(failCount > 0 )
		{
			writeTestResultsToUJ("42735b0a",false);
		}
		else
		{
			writeTestResultsToUJ("42735b0a",true);
		}
		
		if(failCountInFooter > 0)
		{
			writeTestResultsToUJ("4272465c",true);
		}
		else
		{
			writeTestResultsToUJ("4272465c",false);
		}
		
		goBackToHomePage();
	}	
	
	private void checkSocialMediaLinks(String pageType,String linkXpath,String title,String uID) throws Exception
	{	
		String winHandleBefore ="";
		String pageTitle = "";
		String pageURL = "";
		
		if(!linkXpath.isEmpty())
		{
			if(isElementPresent(linkXpath))
			{
				openPage(common.siteUrl);				
			}
				
			
			if(!isElementPresent(linkXpath))
			{
				writeTestResults("Verify the "+ pageType +"xpath present in the page",pageType+" xpath should be displayed in the page" ,linkXpath+" xpath is not displayed",uID,false,false);	
			}
			else
			{
				clickAndWait(linkXpath);
				
				Thread.sleep(100);
				
				if(getCurrentUrl().indexOf("/#")>-1 && driver.getWindowHandles().size() <= 1)
				{
					writeTestResults("Verify the "+pageType+"' link is redirects to the correct content page.","Expected title of the '"+pageType+"' link is "+facebookTitle,"Link is not working",uID,false,false);
				}
				else
				{									
					 pageTitle = getTitle();
					 pageURL = getCurrentUrl();
					
					 if(driver.getWindowHandles().size()>1)
					 {
						 winHandleBefore = driver.getWindowHandle();
						 for(String winHandle : driver.getWindowHandles())
						 {
							 driver.switchTo().window(winHandle);
							 Thread.sleep(3000);
						     pageTitle = getTitle();
							 pageURL =getCurrentUrl();											 
						 }		
						  driver.close();
							 
						  driver.switchTo().window(winHandleBefore);
					 }
				
				}
				if(!pageTitle.equalsIgnoreCase(title))
				{										
					writeTestResults("Verify the "+pageType+"' link is redirects to the correct content page.","Expected title of the '"+pageType+" is "+title,"Actual title of the '"+pageType+" is '"+pageTitle+"' and page url is "+pageURL ,uID,false,false);
					
				}
				else
				{									
					writeTestResults("Verify the "+pageType+"' link is redirects to the correct content page.","Expected title of the '"+pageType+"' link is "+title,"Actual title of the '"+pageType+"' link is "+pageTitle ,uID,true,false);
				}
			 		
				goBack();
				
				goBackToHomePage();
				
			}			
		}		
	}
	
	public void verifySocialMediaLinks() throws Exception 
	{	
		goBackToHomePage();

		
		checkSocialMediaLinks("facebook",lnfacebook,facebookTitle,"427248f0");
		

		
	}
	
	public void copyrightMessage()throws Exception 
	{	
		goBackToHomePage();
		
		if(!isElementPresent(copyright))
		{
			writeTestResults("Verify copyright message", "Copyright message should be displayed" , "Copyright message xpath is not present or xpath("+copyright+") is incorrect in Property file",
					"426f1086",false,true);
		}
		else
		{
			
			if(getText(copyright).indexOf(copyrightMessage) < 0)
			{				
				writeTestResults("Verify copyright message", "Expected 'Copyright' message is '"+copyrightMessage+"'" , "Actual 'Copyright' message is '"+getText(copyright)+"'",
						"426f1086",false,true);
			}			
			else
			{				
				writeTestResults("Verify copyright message", "Expected 'Copyright' message is '"+copyrightMessage+"'" , "Actual 'Copyright' message is '"+copyrightMessage+"'" ,
						"426f1086",true,false);				
			}
		}		
	}
	
	public void wistlistGuestPrDetails() throws Exception
	{
		common.clickLogo();
		
		if(!common.selectBrand())
		{
			writeTestResults("Verify product add to wishlist When 'add to wishlist' button is clicked from Product details page",
					"User can select the product", "Selected brand is not dipalyed or page is not loaded properly","",false,true);
	
		}	
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
		{
			common.selectProductFromLeftCategory();			
		}	
		
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			common.productGrid();
		}
		
		
		
		common.navigateToProductDetailsPage();
		
		if(isElementPresent(wishListLink) || isElementPresent("//a[@class='action towishlist']"))
		{
			beforeClickUrl = getCurrentUrl();
			
			clickAndWait(wishListLink);
			
			if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
			{
				clickAndWait("//*[@id='product_addtocart_form']/div[2]/div[2]/div[1]/div[2]/ul/li/a");
			}
			
			if(!wishListErrorXpath.isEmpty())
			{
				if(isDisplayed(wishListErrorXpath))
				{
					if(getText(wishListErrorXpath).equalsIgnoreCase(wishListErrorMsg))
					{
						writeTestResults("Verify product add to wishlist When 'add to wishlist' button is clicked from Product detail page",	wishListErrorMsg+" should be displayed", wishListErrorMsg+" is displayed" ,"",true,false);
					}
					else
					{
						writeTestResults("Verify product add to wishlist When 'add to wishlist' button is clicked from Product detail page",	wishListErrorMsg+" should be displayed", getText(wishListErrorXpath)+" is displayed" ,"",true,false);
					}
				}
				else
				{
					writeTestResults("Verify product add to wishlist When 'add to wishlist' button is clicked from Product detail page","Expected page title is "+myAccountTestData.titleMyAccount, "Login page is not displayed.Following page is displayed and page title is '"+getTitle()+"'","",false,true);
				}					
			}
			else if(getTitle().equalsIgnoreCase(myAccountTestData.titleUserLogin))
			{
				writeTestResults("Verify product add to wishlist When 'add to wishlist' button is clicked from Product detail page",	"Should Navigate to Login Page", "Login page is displayed" ,"",true,false);				
			}
			else
			{
				
				writeTestResults("Verify product add to wishlist When 'add to wishlist' button is clicked from Product detail page","Expected page title is "+myAccountTestData.titleMyAccount, "Login page is not displayed.Following page is displayed and page title is '"+getTitle()+"'","",false,true);
			}
		}
		else
		{
			writeTestResults("Verify 'add to wishlist' link in 'Product detial' page","Wishlist link should be displayed", "Wishlist link is not present in product detail page","",false,true);
		}
		common.clickLogo();		
	}
	
	public void wistlistGuestPrListing() throws Exception
	{
		common.clickLogo();
		
		if(!common.selectBrand())
		{
			writeTestResults("Verify product add to wishlist When 'add to wishlist' button is clicked from Product Listing page",
					"User can select the product", "Selected brand is not dipalyed or page is not loaded properly","",false,true);	
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
		{
			common.selectProductFromLeftCategory();			
		}	
		
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			common.productGrid();
		}
				
		if(isElementPresent(wishListLink))
		{
			beforeClickUrl = getCurrentUrl();
			
			clickAndWait(wishListLink);			
			
			if(getTitle().equalsIgnoreCase(myAccountTestData.titleMyAccount))
			{
				writeTestResults("Verify product add to wishlist When 'add to wishlist' button is clicked from Product Listing page",	"Should Navigate to Login Page", "Login page is displayed" ,"",true,false);
				
			}
			else
			{
				writeTestResults("Verify product add to wishlist When 'add to wishlist' button is clicked from Product detail page","Expected page title is "+myAccountTestData.titleMyAccount, "Login page is not displayed.Following page is displayed and page title is '"+getTitle()+"'","",false,true);				
			}
		}
		else
		{
			writeTestResults("Verify 'add to wishlist' link in 'Product Listing' page","Wishlist link should be displayed", "Wishlist link is not present in product listing page or xpath("+wishListLink+") is incorrect","",false,true);
		}		
		common.clickLogo();
		
		clearCookies();		
	}	
	
	public void addToWishlistLoggedUserPrDetails() throws Exception
	{
		common.clickLogo();
		
		if(!common.navigateToLoginPage())
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
					"User can navigate to login page", "User Can't navigate to login page.log link is not displayed or link path ("+myAccountTestData.loginLink+") is incorrect","",false,true);
		}	
		
		if(!common.login().equalsIgnoreCase("Element present"))
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
					"User can login to My account", "Following element not present or xpath ("+common.notFoundElement+") is incorrect","",false,true);	
		}
	
		if(common.passwordMatch().equalsIgnoreCase(""))
		{
			common.clickLogo();
			
			if(!common.selectBrand())
			{
				writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
						"User can select the product", "Selected brand is not dipalyed or page is not loaded properly","",false,true);
			}
			
			if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
			{
				common.selectProductFromLeftCategory();
			}	
			
			if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
			{
				common.productGrid();
			}
			
			
			
			common.navigateToProductDetailsPage();
			
			if(isElementPresent(wishListLink) || isElementPresent("//span[text()='Add to Wishlist']"))
			{
				beforeClickUrl = getCurrentUrl();
				
				clickAndWait(wishListLink);	
				
				if(beforeClickUrl.equalsIgnoreCase(getCurrentUrl()))
				{
					clickAndWait("//span[text()='Add to Wishlist']");	
				}
				
				
				
				if(isDisplayed("//div[contains(text(),'has been added to your')]"))
				{
					writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
							"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is displayed in 'My Wishlist' page" ,"",true,false);
					
					if(getPageSource(common.productName))
					{
						writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
									"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is not displayed in 'My Wishlist' page" ,"",true,false);
					}
					
					int rowCount = getRowCountClassName(wishListTable,"product-item");
					i=1;
					while(i < rowCount+1)
					{
						mouseMove(wishListTable+"/li["+i+"]/div[@class='product-item-info']");
						
						if(isElementPresent(rmvItemFromWishList))
						{
							clickAndWait(rmvItemFromWishList);
							
							if(isAlertPresent())
							{
								//logFile("Aleart present");
								acceptAlert();
							}									
						}
						else
						{
							break;
						}
						i++;
					}
				}
				else
				{
					writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
							"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is not displayed in 'My Wishlist' page","",false,false);
					
				}
					
			}
			else
			{
					writeTestResults("Verify 'add to wishlist' link in 'Product detail' page","Wishlist link should be displayed", "Wishlist link is not present in product details page or xpath("+wishListLink+") is incorrect","",false,true);
			}
			if(getPageSource("An error occurred while adding item to wishlist:") && getPageSource("already in wishlist")){
				writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
						"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is already added to 'Wishlist' and product is displayed in 'My Wishlist'" ,"",true,false);
			}
			
			common.userLogOut();
		}
		else
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product details page",
					"User can login to the My Account section", "User Can't login to My Account section.Following error got it in user login page ("+common.passwordMatch()+")","",false,true);
		}
	}
	
	public void addToWishlistLoggedUserPrListing() throws Exception
	{		
		String loginFieldsValidation = "";
		
		if(!common.navigateToLoginPage())
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product Listing page",
					"User can navigate to login page", "User Can't navigate to login page.log link is not displayed or link path ("+myAccountTestData.loginLink+") is incorrect","",false,true);
		}	
		
		loginFieldsValidation =	common.login();
		
		if(!loginFieldsValidation.equalsIgnoreCase("Element present"))
		{
			writeTestResults("Verify fields in login page","All the field shoud be displayed", "Following field "+loginFieldsValidation+" is not displayed or location is incorrect","",false,true);			
		}
		
		loginFieldsValidation = common.passwordMatch();
		
		if(!loginFieldsValidation.equalsIgnoreCase(""))
		{
			writeTestResults("Verify user can login to my account","My Account should be displayed", "Following error "+loginFieldsValidation+" is displayed","",false,true);
		}
		
		common.clickLogo();
		
		if(!common.selectBrand())
		{
			writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product Listing page",
					"User can select the product", "Selected brand is not dipalyed or page is not loaded properly","",false,true);
		}	
		
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			common.productGrid();
		}		
		
		if(isElementPresent(wishListLink))
		{
			beforeClickUrl = getCurrentUrl();
			
			clickAndWait(wishListLink);
			
			if(getPageSource("has been added to your wishlist") || getPageSource("already in wishlist."))
			{
				writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product Listing page",
						"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is displayed in 'My Wishlist' page" ,"",true,false);
				
				if(!getPageSource(common.productName))
				{
					writeTestResults("Verify product add to wishlist When logged user clicked 'add to wishlist' button from Product Listing page",
								"Should navigate to 'My Wishlist' page in My Account Should display the product selected", "Selected product is not displayed in 'My Wishlist' page" ,"",true,false);
				}				
				
				int rowCount = getRowCount(wishListTable,"tr");
				
				while(i < rowCount)
				{				
					if(isElementPresent(rmvItemFromWishList))
					{
						click(rmvItemFromWishList);
						
						if(isAlertPresent())
						{
							acceptAlert();
						}							
											   
					}
					else
					{
						break;
					}
					i++;
					
				}				
				common.userLogOut();
				clearCookies();				
			}
		}
	}
	
	public void noOfItemsInMiniCartItemCountIsZero() throws Exception
	{		
		common.clearMyCart();
		
		//common.clickLogo();
			
		if(isElementPresent(ckProcess.myCartXpath))
		{
			String totalCartItems = getText(ckProcess.myCartXpath);
			
			//logFile("totalCartItems"+totalCartItems);
			int items = 0;
			if(totalCartItems.indexOf("Items")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("")+0,totalCartItems.indexOf("Items")).trim());
			}
			else if(totalCartItems.indexOf("My Cart (")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("My Cart (")+9,totalCartItems.indexOf(")")).trim());
			}
			else if(totalCartItems.indexOf("My Cart")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("My Cart")+7).trim());
			}
			else if(totalCartItems.indexOf("MY CART")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("MY CART (")+9,totalCartItems.indexOf(")")).trim());
			}
			else if(totalCartItems.indexOf("SHOPPING CART")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("")+0,totalCartItems.indexOf("SHOPPING CART")).trim());
			}
			else if(totalCartItems.indexOf("SHOPPING BAG")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("(")+1,totalCartItems.indexOf(")")).trim());
			}
			else if(totalCartItems.indexOf("CART (")>-1)
			{
				items = Integer.parseInt(totalCartItems.substring(totalCartItems.indexOf("CART (")+6,totalCartItems.indexOf(")")).trim());				
			}
			
			if(items==0)
			{				
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user not added any product", "'0 Items' should be displayed in the 'mini cart'", "'0 Items' is displayed in the 'mini cart'"  ,"",true,false);
			}
			else
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user not added any product", "'0 Items' should be displayed in the'mini cart'", "'"+items+"' Items' is displayed in the 'mini cart'" ,"",false,true);
			}
		}
		else
		{
			writeTestResults("Verify the 'Cart Item' field", "'Cart Item' field should be displayed", "'Cart Item' is not displayed" ,"",false,true);
		}	
	}
	
	public void noOfItemsInMiniCartItemCountIsOne() throws Exception
	{ 
		common.clearMyCart();
		
		//common.clickLogo();
		Thread.sleep(3000);
		if(!common.selectBrand())
		{
			writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", "User can select the product", 
					"Selected brand is not dipalyed or page is not loaded properly" 
					,"",false,true);
		}
		else
		{
			System.out.println("Navigate to category page");
		}
		
		if(findElementInXLSheet(getMethodIsExecuted,"Select Left Category").equalsIgnoreCase("Yes"))
		{
			common.selectProductFromLeftCategory();			
		}	
		
		if(findElementInXLSheet(getMethodIsExecuted,"View Product Grid").equalsIgnoreCase("Yes"))
		{
			if(!common.productGrid())
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product",
						"Grid link should be present","Link is not displayed or xpath ("+ckProcess.productGrid+") is incorrect","",false,true);
			}
		}			
				
		common.navigateToProductDetailsPage();
		Thread.sleep(3000);
		common.addToCart("Verify the cart drop down shows number of items in the shopping cart when user added one product");
		common.pageRefersh();
		Thread.sleep(8000);
		if(isElementPresent(CheckoutTestData.myCartXpath))
		{					
			if(common.totalItemInHeaderCart()==1)
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", 
						"'1 Item' should be displayed in the 'mini cart'", "'1 Item' is displayed in the 'mini cart'"  ,"",true,false);
				
			}
			else
			{
				writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", 
						"'1 Item' should be displayed in the 'mini cart'", "'"+common.totalItemInHeaderCart()+"' Items' is displayed in the 'mini cart'" ,"",false,true);
			}
			
			//common.clearMyCart();
			
			//common.clickLogo();			
		}
		else
		{
			writeTestResults("Verify the cart drop down shows number of items in the shopping cart when user added one product", "'Cart Item' field should be displayed", "'Cart Item' is not displayed" ,"",false,true);
		}		
	}
	
	public void updateDatabase(String newsletterEmail)throws Exception
	{		
		 db.getConnection();
		 Random rand = new Random(); 
		 rand.nextInt(40); 	
		
	    String sql = "UPDATE newsletter_subscriber SET subscriber_email = ? WHERE subscriber_email =?";
		try{
			 java.sql.PreparedStatement st = db.getConnection().prepareStatement(sql);
			 st.setString(1, "test"+rand.nextInt(5000)+"@test.com");
			 st.setString(2, newsletterEmail);
			 st.executeUpdate();			 
			 st.close(); 
		}
		catch (SQLException e) 
		{
			writeTestResults("SQL ERROR", "SQL ERROR", "Error Message is "+ e.getMessage(),"",false,true);
			
			e.printStackTrace();
		  }		
	}
	public void newsLetterPopup(String newsletterEmail) throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"Has newsletter popup"))
		{
			if(isDisplayed(newsletterPopupFNameXpath))
			{
				sendKeys(newsletterPopupFNameXpath,newsletterPopupFName);
			}
			if(isDisplayed(newsletterPopupLNameXpath))
			{
				sendKeys(newsletterPopupLNameXpath,newsletterPopupLName);
			}
			if(isDisplayed(newsletterPopupCEmailXpath))
			{
				sendKeys(newsletterPopupCEmailXpath,newsletterEmail);
			}
			if(isDisplayed(newsletterPopupSubmit))
			{
				clickAndWait(newsletterPopupSubmit);
			}
			else
			{
				writeTestResults("Verify newsletter popup submit button","Submit button should be displayed", "Submit button is not displayed or xpath is incorrect","",false,true);
			}
			
			if(isDisplayed(newsletterPopupFNameErrorXpath))
			{
				writeTestResults("verify the system allows user to subscribe to newsletters with first name ", newsletterSuccessMsg+" message should be displayed", "Following message found :"+getText(newsletterPopupFNameErrorXpath),"",false,true);
			}
			else if(isDisplayed(newsletterPopupLNameErrorXpath))
			{
				writeTestResults("verify the system allows user to subscribe to newsletters with last name ", newsletterSuccessMsg+" message should be displayed", "Following message found :"+getText(newsletterPopupLNameErrorXpath),"",false,true);
			}
			else if(isDisplayed(newsletterPopupCEmailErrorXpath))
			{
				writeTestResults("verify the system allows user to subscribe to newsletters with confirm email", newsletterSuccessMsg+" message should be displayed", "Following message found :"+getText(newsletterPopupCEmailErrorXpath),"",false,true);
			}			
		}		
	}
	
	public void newsLetterSubscription(String userType) throws Exception
	{
		String newsletterEmail ="";
		String newsLetterMsg = "";
		String newsLetterMsgXpath = "";
		String uJId = "";
		
		if(userType.equalsIgnoreCase("Member"))
		{
			newsletterEmail = newsletterEmailForMember;
			newsLetterMsg = newsletterAlreadyMsg;
			newsLetterMsgXpath = newsletterAlreadyXpath;	

			common.updateNewsletterUserAccountInAdmin(newsletterEmail);
			
		}
		else if(userType.equalsIgnoreCase("Guest"))
		{
			Random rn = new Random();
			int rand = rn.nextInt(10000) + 1;
			
			newsletterEmail = rand+"qa@netstarter.com";
			newsLetterMsg = newsletterSuccessMsg;
			newsLetterMsgXpath = newsletterSuccessXpath;
			uJId = "426f0456";
		}
		
		//updateDatabase(newsletterEmail);
		
		goBackToHomePage();
		
		pageRefersh();
		
		if(isDisplayed(newsletterTxtBox))
		{
			sendKeys(newsletterTxtBox,newsletterEmail);
			
			click(newsletterSubmitButton);
			
			newsLetterPopup(newsletterEmail);
			
			if(getText(newsLetterMsgXpath).equalsIgnoreCase(newsLetterMsg))
			{
				writeTestResults("verify the system allows '"+userType+" user' to subscribe to newsletters with valid email", newsLetterMsg+" message should be displayed", 
						newsLetterMsg +" message is displayed" ,uJId,true,false);
				
			}
			else
			{
				if(!getText(newsletterValidationXpath).isEmpty())
				{
					writeTestResults("verify the system allows '"+userType+" user' to subscribe to newsletters with valid email", newsLetterMsg+" message should be displayed", "Following message found :"+getText(newsletterValidationXpath),uJId,false,true);
				}
				else if(!getText(newsletterAlreadyXpath).isEmpty())
				{
					writeTestResults("verify the system allows '"+userType+" user' to subscribe to newsletters with valid email", newsLetterMsg+" message should be displayed", "Following message found :"+getText(newsletterAlreadyXpath),uJId,false,true);
				}
				else if(getText(newsLetterMsgXpath).isEmpty())
				{
					writeTestResults("verify the system allows '"+userType+" user' to subscribe to newsletters with valid email", newsLetterMsg+" message should be displayed", newsLetterMsg+" message is not displayed",uJId,false,true);
				}
				else					
				{
					writeTestResults("verify the system allows '"+userType+" user' to subscribe to newsletters with valid email", newsLetterMsg+" message should be displayed", "Following message found :"+getText(newsLetterMsgXpath),
							uJId,false,true);
				}				
			}
			
		}
		else			
		{
			if(!isDisplayed(newsletterTxtBox))
			{
				writeTestResults("verify the system allows '"+userType+" user' to subscribe to newsletters with valid email","Email text box and submit button should be displayed", "Email text box is not displayed or xpath is incorrect",
						uJId,false,true);
			}
			else if(!isDisplayed(newsletterSubmitButton))
			{
				writeTestResults("verify the system allows '"+userType+" user' to subscribe to newsletters with valid email","Email text box and submit button should be displayed", "submit button is not displayed or xpath is incorrect",
						uJId,false,true);
			}			
		}		
	}
	
	public void newsLetterSubscriptionAlreadyExistsMail() throws Exception
	{
		pageRefersh();
		
		if(isDisplayed(newsletterTxtBox))
		{
			sendKeys(newsletterTxtBox,newsletterAlreadyEmail);
			
			click(newsletterSubmitButton);
			
			newsLetterPopup(newsletterAlreadyEmail);
			
			if(getText(newsletterAlreadyXpath).equalsIgnoreCase(newsletterAlreadyMsg))
			{
				writeTestResults("verify the system allows user to subscribe to newsletters with already exists email ", newsletterAlreadyMsg+" message should be displayed", newsletterAlreadyMsg +" message is displayed" ,
						"426f0726",true,false);
				
			}
			else
			{
				if(!getText(newsletterValidationXpath).isEmpty())
				{
					writeTestResults("verify the system allows user to subscribe to newsletters with exists email ", newsletterAlreadyMsg+" message should be displayed", "Following message found :"+getText(newsletterValidationXpath),
							"426f0726",false,true);
				}
				else if(!getText(newsletterSuccessXpath).isEmpty())
				{
					writeTestResults("verify the system allows user to subscribe to newsletters with exists email ", newsletterAlreadyMsg+" message should be displayed", "Following message found :"+getText(newsletterSuccessXpath),
							"426f0726",false,true);
				}
				else if(getText(newsletterAlreadyXpath).isEmpty())
				{
					writeTestResults("verify the system allows user to subscribe to newsletters with exists email ", newsletterAlreadyMsg+" message should be displayed", newsletterAlreadyMsg+" message is not displayed","426f0726",false,true);
				}
				else
				{
					
					writeTestResults("verify the system allows user to subscribe to newsletters with exists email ", newsletterAlreadyMsg+" message should be displayed", "Following message found :"+getText(newsletterAlreadyXpath),
							"426f0726",false,true);
				}				
			}
		}
		else
		{
			if(!isDisplayed(newsletterTxtBox))
			{
				writeTestResults("verify the system allows user to subscribe to newsletters with exists email ", "Email text box and submit button should be displayed", "Email text box is not displayed or xpath is incorrect",
						"426f0726",false,true);
			}
			else if(!isDisplayed(newsletterSubmitButton))
			{
				writeTestResults("verify the system allows user to subscribe to newsletters with exists email ", "Email text box and submit button should be displayed", "submit button is not displayed or xpath is incorrect",
						"426f0726",false,true);
			}			
		}
	}
	
	public void newsLetterSubscriptionValidation() throws Exception
	{
		pageRefersh();
		
		if(isDisplayed(newsletterTxtBox))
		{
			sendKeys(newsletterTxtBox,"Invalid");
			
			click(newsletterSubmitButton);
			
			if(getText(newsletterValidationXpath).equalsIgnoreCase(newsletterValidationMsg))
			{
				writeTestResults("verify the system allows user to subscribe to newsletters with invalid email ", "Error message shoud be displayed", "Following error found :"+newsletterValidationMsg ,
						"426f0a5a",true,false);
			}
			else
			{
				if(!getText(newsletterSuccessXpath).isEmpty())
				{
					writeTestResults("verify the system allows user to subscribe to newsletters with invalid email ", newsletterAlreadyMsg+" message should be displayed", "Following message found :"+getText(newsletterSuccessXpath),
							"426f0a5a",false,true);
				}
				else if(!getText(newsletterAlreadyXpath).isEmpty())
				{
					writeTestResults("verify the system allows user to subscribe to newsletters with invalid email ", "Error message shoud be displayed", "Following error found :"+getText(newsletterAlreadyXpath),
							"426f0a5a",false,true);
				}				 
				else
				{
					writeTestResults("verify the system allows user to subscribe to newsletters with invalid email ", "Error message shoud be displayed", "Following error found :"+getText(newsletterValidationXpath),
							"426f0a5a",true,false);
				}
			}			
		}
		else
		{
			if(!isDisplayed(newsletterTxtBox))
			{
				writeTestResults("verify the system allows user to subscribe to newsletters with invalid email ", "Email text box and submit button should be displayed", "Email text box is not displayed or xpath is incorrect",
						"426f0a5a",false,true);
			}
			else if(!isDisplayed(newsletterSubmitButton))
			{
				writeTestResults("verify the system allows user to subscribe to newsletters with invalid email ", "Email text box and submit button should be displayed", "submit button is not displayed or xpath is incorrect",
						"426f0a5a",false,true);
			}			
		}
	}	
	
	public boolean ajaxCall(String loader) throws Exception
	{
		int i=0;
		boolean isAjaxLoad = true;
		
		if(isElementPresent(ckProcess.chkoutAjaxLoader))
		{	
			//logFile("aaa"+isDisplayed(chkoutAjaxLoader));
			 while(isDisplayed(ckProcess.chkoutAjaxLoader))
			 {			   	 
			   	 Thread.sleep(1000);
			   	 //logFile("i"+i);
			   	 i++;
			   	 
			   	 if(i==100)
			   	 {
			   		isAjaxLoad = false; 
			   		break;
			   	 }			   	 
			 }			 
		}
		else
		{
			Thread.sleep(2000);
		}		
		return isAjaxLoad;	   
	}	
	
	public void newsLetterSubscriptionPopupClosed() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"has home page popup"))
		{
			clearCookies();
			
			openPage(common.siteUrl);
			
			if(isElementPresent(findElementInXLSheet(getParameterXpath,"home signup popup id")))
			{
				click(findElementInXLSheet(getParameterXpath,"home signup popup id"));
				
				if(isElementPresent(txtPopUp) || isElementPresent(btnPopUpSubmit))
				{
					writeTestResults("Verify 'Newsletter' popup close", "Popup should be closed once click on close icon", "Popup is closed once click on close icon" ,"",true,false);
					
				}
				else
				{
					writeTestResults("Verify 'Newsletter' popup close", "Popup should be closed once click on close icon", "Popup is not closed once click on close icon","",false,true);
				}
			}			
		}		
	}
	
	public void newsLetterSubscriptionPopupWithoutEmail() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"has home page popup"))
		{
			clearCookies();			
					
			Thread.sleep(1000);
			
			if(isElementPresent(txtPopUp) && isElementPresent(btnPopUpSubmit))
			{				
				sendKeys(txtPopUp,"");
				
				click(btnPopUpSubmit);
				
				Thread.sleep(1000);
				
				if(isDisplayed(newsLetterpopUpEmptyValidation))
				{
					writeTestResults("Verify 'Newsletter' popup validation messages when user click submit button without email", "If email address not entered error message should fire.", "Following message is displayed. "+getText(newsLetterpopUpEmptyValidation) ,"",true,false);
				}
				else
				{
					writeTestResults("Verify 'Newsletter' popup validation messages when user click submit button without email", "If email address not entered error message should fire.", "Error message is not displayed or error xpath is incorrect","",false,true);
				}
			}
		}		
	}
	
	public void newsLetterSubscriptionPopupWithInValidEmail() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"has home page popup"))
		{
			clearCookies();
			
			openPage(common.siteUrl);
			
			Thread.sleep(1000);
			
			if(isElementPresent(txtPopUp) && isElementPresent(btnPopUpSubmit))
			{
				writeTestResults("Verify 'Newsletter' popup", "Newsletter popup should displayed.", "Newsletter popup is displayed." ,"426ef8ee",true,false);
				
				click(txtPopUp);
				
				sendKeys(txtPopUp,"test");
				
				click(btnPopUpSubmit);
				
				Thread.sleep(2000);
				
				if(isDisplayed(newsLetterpopUpInvalidEmail))
				{
					writeTestResults("Verify 'Newsletter' popup validation messages when user click submit button with wrong email", "If email address wrong error message should fire.", 
							"Following message is displayed. "+getText(newsLetterpopUpInvalidEmail) ,"426efdbc",true,false);
				}
				else
				{
					writeTestResults("Verify 'Newsletter' popup validation messages when user click submit button with wrong email", "If email address wrong error message should fire.", 
							"Error message is not displayed or error xpath is incorrect","426efdbc",false,true);
				}
			}
			else
			{
				writeTestResults("Verify 'Newsletter' popup", "Newsletter popup should displayed.", "Newsletter popup is not displayed." ,"426ef8ee",false,true);
			}
		}
	}	
	
	public void newsLetterSubscriptionPopupWithValidEmail() throws Exception
	{
		if(isExecuted(getMethodIsExecuted,"has home page popup"))
		{
			clearCookies();
			
			openPage(common.siteUrl);
			
			Thread.sleep(1000);
			
			if(isElementPresent(txtPopUp) && isElementPresent(btnPopUpSubmit))
			{
				click(txtPopUp);
				
				sendKeys(txtPopUp,"Test@test123.com");
				
				click(btnPopUpSubmit);
				
				Thread.sleep(2000);
				
				if(isDisplayed(newsLetterpopUpSuccess))
				{
					writeTestResults("Verify 'Newsletter' popup validation messages when user click submit button with correct email", "Following message should be displayed.- "+getText(newsLetterpopUpSuccess), "Following message is displayed. "+getText(newsLetterpopUpSuccess) ,"",true,false);
				}
				else
				{
					writeTestResults("Verify 'Newsletter' popup validation messages when user click submit button with correct email", "Thank you message should be displayed.", "Success message is not displayed or error xpath is incorrect","",false,true);
				}
			}
		}
	}
	
	public void verifyErrorPage() throws Exception
	{
		String pageUrl = common.siteUrl+"/error";
		
		if(common.urlStatus(pageUrl))
		{
			if(common.getResponseStatus == 404)
			{
			
					writeTestResults("Verify 404 page content", "Respose code should be 404 and correct page should be displayed", "Respose code is 404 and correct page is displayed" ,"42713dfc",true,false);
			}
			else
			{
				if(getTitle().indexOf("404") >= 0)
				{
					
					writeTestResults("Verify 404 page content", "Respose code should be 404 and correct page should be displayed", "Respose code is 404 and correct page is displayed" ,"42713dfc",true,false);
				}
				else
				{
					writeTestResults("Verify 404 page content", "Respose code should be 404 and 404 page should be displayed", "Following page is displayed. " +
							"Page title is "+getTitle(),"42713dfc",false,true);
				}
				
				
			}
		}
		else
		{
			writeTestResults("Verify 404 page content", "Respose code should be 404 and 404 page should be displayed", "Respose code is "+common.getResponseStatus,"42713dfc",false,true);
		}			
	}
	
	public void verifyHeaderLogin() throws Exception
	{
		account.login();
		
		if(!account.mouseMoveToMyAccount.isEmpty())
		{
			mouseMove(account.mouseMoveToMyAccount);	
		}		
		
		if(isElementPresent(account.linkLogOut))
		{
			writeTestResults("Verify the system allows user to log in to the system","User should login to the system and My Account Section should be enabled for the user" , "User can login to the system.",
					"426f35fc",true,false);
		
		}
	}
	
	
	public void totalTime(java.util.Date startTime) throws Exception
	{		
		java.util.Date endTime;		
		Calendar cal = Calendar.getInstance(); 		
		endTime = cal.getTime();
		String totalTime = common.totalTime(startTime, endTime);		   
	    writeTestResults("Total time to execute "+writeFileName, "Total time =",totalTime ,"",true,false);		
	}
	
	
			
}