<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:template match="/">
<html lang="en">
	
<head>
	<title>Summary Results</title>
	<meta charset="utf-8"></meta>
	<meta name="viewport" content="width=device-width, initial-scale=1"></meta>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"></link>
	<link rel="stylesheet" href="../css/style.css" type="text/css"></link>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	
	<xsl:for-each select="test_results/total_summary">  

	 <script>
	 
		$('#backToTopBtn').click(function(){
			$('html,body').animate({scrollTop:0},'slow');return false;
		});	  
		$(function(){
		$("#pieChart").drawPieChart([
		{ title: "PASSED",              value : <xsl:value-of select="passed"/>,  color: "#36B503" },
		{ title: "FAILED",    value:  <xsl:value-of select="failed"/>,   color: "rgba(234, 10, 10, 0.96)" },
		{ title: "SKIPPED",          value:  <xsl:value-of select="skip"/>,   color: "#fff100" }
    
		]);
	});	
	</script>	
	</xsl:for-each>	
</head>
	<body>	
	    <div id="w1" class="navbar-inverse navbar-fixed-top navbar" role="navigation"><div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span></button><a class="navbar-brand" href="/dashboard/web">Netstarter</a></div>					
				<div id="w1-collapse" class="collapse navbar-collapse">
					<ul id="w2" class="navbar-nav navbar-right nav">
						<li><a href="/dashboard/web/site/login">Login</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="wrap">
				<div class="container">			
			<div class="row header_middle">BBQ AUTOMATED REGRESSION TEST REPORT <br><xsl:value-of select="test_results/executed_time"/></br></div>	

			<xsl:for-each select="test_results/test_results_summary">  
				<div class="summary_heading"><h3><xsl:value-of select="@id"/></h3></div>
					<div class="row">
						<div class="col-sm-8">
						   <table class="table table-bordered table-striped-column">
							  <tr style="background-color: #ffffff">
								 <th style="width:25%">Scenario</th>
								 <th style="width:10%"># Cases</th>
								 <th style="width:12%"># Passed</th>
								 <th style="width:12%"># Failed</th>
								 <th style="width:12%"># Skipped</th>
								 <th style="width:15%">Duration(m:s)</th>
								 <th style="width:15%">Success%</th>
							  </tr>	
							  <xsl:for-each select="*">
							   <tr>								 
									<xsl:variable name="NAME"><xsl:value-of select="scenario"/></xsl:variable>	
									<td class ="scenario"><a href="#{$NAME}"><xsl:value-of select="scenario"/></a></td>
									<td class = "mid"><xsl:value-of select="case"/></td>
									<td class = "mid_pass"><xsl:value-of select="passed"/></td>
							  
									<xsl:if test="failed > 0">
										<td class = "mid_fail"><a class="fail_senario" href="#{$NAME} Failed Scenarios"><xsl:value-of select="failed"/></a></td>
									</xsl:if>
									<xsl:if test="failed = 0">
										<td class = "mid_fail"><xsl:value-of select="failed"/></td>
									</xsl:if>
									<td class = "mid"><xsl:value-of select="skip"/></td>
									<td class = "mid"><xsl:value-of select="time"/></td>
									<td class = "mid"><xsl:value-of select="rate"/></td>
							  </tr>
							  </xsl:for-each>	
						</table>
					</div>
					<div class="col-sm-4 chart" id="pieChart" >						
						<script src="../js/index.js"></script>	
					</div>
				</div>
			</xsl:for-each>	

			<!--<xsl:for-each select="test_results/test_results_skip">  
				<div class="summary_heading"><h3><xsl:value-of select="@id"/></h3></div>
					<div class="row">
						<div class="col-sm-8">
						   <table class="table table-bordered table-striped-column">
							  <tr style="background-color: #ffffff">
								 <th style="width:25%">Main Scenario</th>
								 <th style="width:10%"># Scenario</th>
								 <th style="width:12%"># Status</th>
							  </tr>	
							  <xsl:for-each select="*">
							   <tr>									
									<td><xsl:value-of select="main_senario"/></td>
									<td><xsl:value-of select="scenario"/></td>
									<td><xsl:value-of select="status"/></td>
							  </tr>
							  </xsl:for-each>	
						  </table>
						</div>
					</div>

			</xsl:for-each>				
			-->
			
			<xsl:for-each select="test_results/test_scenario"> 
				<xsl:variable name="NAME"><xsl:value-of select="@id"/></xsl:variable>	
				<div id='{$NAME}'>
					<div class="summary_heading"><h3><xsl:value-of select="@id" /></h3></div>
				</div>
				<div>
					<a href="#top" class="btn btn-link" role="button"><span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span> TOP</a>
				</div>
				
				<div class="table-responsive">
					<table class="table table-bordered table-striped-column">
					   <tr style="background-color: #ffffff">
						  <th style="width:25%" >Test Case</th>
						  <th style="width:25%" >Expected Results</th>
						  <th style="width:25%" >Actual Results</th>
						  
						   <xsl:if test="status = 'PASS'">
								<th style="width:10%" >Status</th>
						  </xsl:if>
						  <xsl:if test="status = 'FAIL'">
								<th style="width:10%" >Status</th>
						  </xsl:if>
						  <xsl:if test="status = ''">
									<td class="fail"><font color="#C80000">FAIL</font></td>
								</xsl:if>
						  
						  
						
					   </tr>
						<xsl:for-each select="*">
							<tr>
								<td class ="tc"><xsl:value-of select="case"/></td>
								<td class ="tc"><xsl:value-of select="expected"/></td>
								<td class ="tc"><xsl:value-of select="actual"/></td>
								<xsl:if test="status = 'FAIL'">
									<xsl:variable name="FAILURL"><xsl:value-of select="failUrl"/></xsl:variable>	
									<td class="fail"><a href="#" onclick="event.preventDefault();loadImage('../screenshots/BBQ/{$FAILURL}');"><font color="#C80000"><xsl:value-of select="status"/></font></a></td>
								</xsl:if>
								<xsl:if test="status = 'PASS'">
										<td class="pass"><xsl:value-of select="status"/></td>
								</xsl:if>
							</tr>						
						</xsl:for-each>	
					</table>
				</div>
			</xsl:for-each>
			
			<xsl:for-each select="test_results/test_failed_scenario"> 
				<xsl:variable name="NAME"><xsl:value-of select="@id"/></xsl:variable>	
					<div id='{$NAME} Failed Scenarios'>
						<div class="summary_heading"><h3><xsl:value-of select="@id" /> Failed Scenarios</h3></div>
					</div>
					<div>
						<a href="#top" class="btn btn-link" role="button"><span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span> TOP</a>
					</div>
					<div class="table-responsive">
						<table class="table table-bordered table-striped-column">
						   <tr style="background-color: #ffffff">
						  <th style="width:25%" >Test Case</th>
						  <th style="width:25%" >Expected Results</th>
						  <th style="width:25%" >Actual Results</th>
						  <th style="width:10%" >Status</th>
					   </tr>
						<xsl:for-each select="*">
							<xsl:if test="status = 'FAIL'">
								<tr>
									<td class ="tc"><xsl:value-of select="case"/></td>
									<td class ="tc"><xsl:value-of select="expected"/></td>
									<td class ="tc"><xsl:value-of select="actual"/></td>
									<xsl:if test="status = 'FAIL'">
										<xsl:variable name="FAILURL"><xsl:value-of select="failUrl"/></xsl:variable>	
										<td class="fail"><a href="#" onclick="event.preventDefault();loadImage('../screenshots/BBQ/{$FAILURL}');"><font color="#C80000"><xsl:value-of select="status"/></font></a></td>
									</xsl:if>
									<xsl:if test="status = 'PASS'">
										<td class="pass"><xsl:value-of select="status"/></td>
									</xsl:if>
								</tr>
							</xsl:if>							
						</xsl:for-each>	
						</table>
					</div>
				</xsl:for-each>		
		</div>
		</div>

	</body>
</html>
</xsl:template>
</xsl:stylesheet> 		
